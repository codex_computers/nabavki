object frmPartneriPonuduvaci: TfrmPartneriPonuduvaci
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088#1080' '
  ClientHeight = 457
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 564
    Height = 49
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 0
    object cxLabel1: TcxLabel
      Left = 13
      Top = 14
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088#1080' '#1079#1072' '#1082#1086#1080' '#1080#1084#1072' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1085#1091#1076#1080' '#1074#1086
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clHighlight
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      AnchorY = 24
    end
    object RibbonComboBox1: TcxComboBox
      Left = 393
      Top = 15
      TabStop = False
      BeepOnEnter = False
      Properties.Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      Properties.OnEditValueChanged = RibbonComboBox1PropertiesEditValueChanged
      Style.Color = 16775924
      StyleDisabled.Color = 16113353
      StyleDisabled.TextColor = clBackground
      StyleFocused.Color = 16113353
      StyleFocused.TextColor = clWindowText
      TabOrder = 1
      Width = 88
    end
    object cxLabel2: TcxLabel
      Left = 487
      Top = 14
      Caption = #1075#1086#1076#1080#1085#1072
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clHighlight
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      AnchorY = 24
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 564
    Height = 408
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 560
      Height = 385
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsPartneriPonuduvaci
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 49
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 442
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object StatusBar1: TStatusBar
      Left = 2
      Top = 387
      Width = 560
      Height = 19
      Panels = <
        item
          Text = 'F10 - '#1055#1077#1095#1072#1090#1080
          Width = 75
        end
        item
          Text = 'F1 - '#1055#1086#1084#1086#1096', Esc - '#1048#1079#1083#1077#1079
          Width = 100
        end>
    end
  end
  object ActionList1: TActionList
    Left = 424
    Top = 112
    object aIzlez: TAction
      Caption = 'aIzlez'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aPomos: TAction
      Caption = 'aPomos'
      ShortCut = 112
      OnExecute = aPomosExecute
    end
    object aPecati: TAction
      Caption = 'aPecati'
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aDizajn: TAction
      Caption = 'aDizajn'
      ShortCut = 24697
      OnExecute = aDizajnExecute
    end
  end
end
