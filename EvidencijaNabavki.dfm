﻿object frmNabavki: TfrmNabavki
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1080
  ClientHeight = 724
  ClientWidth = 1051
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1051
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar10'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 701
    Width = 1051
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1051
    Height = 575
    Align = alClient
    Color = clCream
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = False
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    OnPageChanging = cxPageControl1PageChanging
    ClientRectBottom = 575
    ClientRectRight = 1051
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 57
        Width = 1051
        Height = 494
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid1DBTableView1KeyDown
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsNabavki
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00 , .'
              Kind = skSum
              Column = cxGrid1DBTableView1Column1
            end
            item
              Format = '0.00 , .'
              Kind = skSum
              Column = cxGrid1DBTableView1Iznos
            end
            item
              Format = '0.00 , .'
              Column = cxGrid1DBTableView1IZNOS_DENARI
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          object cxGrid1DBTableView1NazivStatus: TcxGridDBColumn
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'NAZIVSTATUS'
            Width = 90
          end
          object cxGrid1DBTableView1BrNabavka: TcxGridDBColumn
            DataBinding.FieldName = 'BROJNABAVKA'
            Width = 144
          end
          object cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_DOGOVOR'
            Width = 100
          end
          object cxGrid1DBTableView1RabEdinica: TcxGridDBColumn
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            DataBinding.FieldName = 'REIDNAZIV'
            Width = 197
          end
          object cxGrid1DBTableView1TIP_NABAVKA: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
            DataBinding.FieldName = 'TIP_NABAVKA_NAZIV'
            Width = 146
          end
          object cxGrid1DBTableView1PREDMET: TcxGridDBColumn
            Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
            DataBinding.FieldName = 'PREDMET'
            Width = 254
          end
          object cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
            DataBinding.FieldName = 'DATUM_KREIRANA'
            Width = 136
          end
          object cxGrid1DBTableView1Iznos: TcxGridDBColumn
            Caption = #1055#1088#1077#1076#1074#1080#1076#1077#1085' '#1080#1079#1085#1086#1089
            DataBinding.FieldName = 'IZNOS'
            Width = 113
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            Caption = #1048#1079#1085#1086#1089' '
            DataBinding.FieldName = 'IZNOSCENA'
            Width = 84
          end
          object cxGrid1DBTableView1Broj: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Visible = False
          end
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object cxGrid1DBTableView1RE: TcxGridDBColumn
            DataBinding.FieldName = 'RE'
            Visible = False
          end
          object cxGrid1DBTableView1MESEC: TcxGridDBColumn
            DataBinding.FieldName = 'MESEC'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1DBColumn: TcxGridDBColumn
            DataBinding.FieldName = 'mesecNaziv'
            Width = 100
          end
          object cxGrid1DBTableView1GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'GODINA'
          end
          object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_VAZENJE'
            Visible = False
          end
          object cxGrid1DBTableView1DATUM_PONUDA_OD: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_PONUDA_OD'
            Visible = False
          end
          object cxGrid1DBTableView1DATUM_PONUDA_DO: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_PONUDA_DO'
            Visible = False
          end
          object cxGrid1DBTableView1TIP_NABAVKA1: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_NABAVKA'
            Visible = False
          end
          object cxGrid1DBTableView1OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Visible = False
          end
          object cxGrid1DBTableView1TIP_KONTAKT: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_KONTAKT'
            Visible = False
          end
          object cxGrid1DBTableView1KONTAKT: TcxGridDBColumn
            DataBinding.FieldName = 'KONTAKT'
            Visible = False
          end
          object cxGrid1DBTableView1STATUS: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS'
            Visible = False
          end
          object cxGrid1DBTableView1KREIRAL: TcxGridDBColumn
            DataBinding.FieldName = 'KREIRAL'
            Visible = False
          end
          object cxGrid1DBTableView1VALUTA: TcxGridDBColumn
            DataBinding.FieldName = 'VALUTA'
            Visible = False
          end
          object cxGrid1DBTableView1KURS: TcxGridDBColumn
            DataBinding.FieldName = 'KURS'
            Visible = False
          end
          object cxGrid1DBTableView1PRIORITET: TcxGridDBColumn
            DataBinding.FieldName = 'PRIORITET'
            Visible = False
          end
          object cxGrid1DBTableView1USLOVI: TcxGridDBColumn
            DataBinding.FieldName = 'USLOVI'
            Visible = False
          end
          object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
            DataBinding.FieldName = 'ZABELESKA'
            Visible = False
          end
          object cxGrid1DBTableView1VREME: TcxGridDBColumn
            DataBinding.FieldName = 'VREME'
            Visible = False
          end
          object cxGrid1DBTableView1PERIOD_VAZENJE: TcxGridDBColumn
            DataBinding.FieldName = 'PERIOD_VAZENJE'
            Visible = False
          end
          object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNERNAZIV'
            Visible = False
          end
          object cxGrid1DBTableView1VALUTANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'VALUTANAZIV'
            Visible = False
          end
          object cxGrid1DBTableView1PRIORITETNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PRIORITETNAZIV'
            Visible = False
          end
          object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RENAZIV'
            Visible = False
          end
          object cxGrid1DBTableView1POTEKLO: TcxGridDBColumn
            DataBinding.FieldName = 'POTEKLO'
            Visible = False
          end
          object cxGrid1DBTableView1IZNOS_DENARI: TcxGridDBColumn
            DataBinding.FieldName = 'IZNOS_DENARI'
            Visible = False
          end
          object cxGrid1DBTableView1ODOBRIL: TcxGridDBColumn
            DataBinding.FieldName = 'ODOBRIL'
            Width = 100
          end
          object cxGrid1DBTableView1DATUM_ODOBRUVANJE: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1114#1077
            DataBinding.FieldName = 'DATUM_ODOBRUVANJE'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1051
        Height = 57
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        ParentBackground = False
        TabOrder = 1
        object Label1: TLabel
          Left = 638
          Top = 11
          Width = 97
          Height = 13
          Caption = #1044#1072#1090#1091#1084' '#1082#1088#1077#1080#1088#1072#1085#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 255
          Top = 11
          Width = 72
          Height = 13
          Caption = #1058#1080#1087' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 765
          Top = 11
          Width = 94
          Height = 13
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 21
          Top = 11
          Width = 77
          Height = 13
          Caption = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 431
          Top = 11
          Width = 122
          Height = 13
          Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RabEdinica: TcxLookupComboBox
          Left = 21
          Top = 30
          BeepOnEnter = False
          Properties.DropDownAutoSize = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Caption = #1064#1080#1092#1088#1072
              Width = 55
              FieldName = 'ID'
            end
            item
              Caption = #1053#1072#1079#1080#1074
              Width = 250
              FieldName = 'NAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dmMat.dsRE
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleFocused.TextColor = clWindowText
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 0
          OnKeyDown = Prebaraj
          Width = 228
        end
        object TipNabavka: TcxLookupComboBox
          Left = 255
          Top = 30
          BeepOnEnter = False
          Properties.DropDownAutoSize = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Width = 400
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dm.dsTipNabavki
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 1
          OnKeyDown = Prebaraj
          Width = 170
        end
        object PredmetNabavka: TcxTextEdit
          Left = 431
          Top = 30
          BeepOnEnter = False
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 2
          OnKeyDown = Prebaraj
          Width = 201
        end
        object BrojNabavka: TcxTextEdit
          Left = 765
          Top = 30
          BeepOnEnter = False
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 4
          OnKeyDown = Prebaraj
          Width = 100
        end
        object DatumKreiran: TcxDateEdit
          Left = 638
          Top = 30
          BeepOnEnter = False
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 3
          OnKeyDown = Prebaraj
          Width = 121
        end
        object ButtonClear: TButton
          Left = 880
          Top = 26
          Width = 75
          Height = 25
          Action = aIscistiButton
          TabOrder = 5
          TabStop = False
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1051
        Height = 551
        Align = alClient
        Enabled = False
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          1051
          551)
        object Label15: TLabel
          Left = 64
          Top = 28
          Width = 76
          Height = 13
          Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 115
          Top = 82
          Width = 25
          Height = 13
          Caption = #1041#1088#1086#1112
          Color = clCream
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label7: TLabel
          Left = 98
          Top = 55
          Width = 42
          Height = 13
          Caption = #1043#1086#1076#1080#1085#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 18
          Top = 190
          Width = 122
          Height = 13
          Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 51
          Top = 163
          Width = 89
          Height = 13
          Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label16: TLabel
          Left = 60
          Top = 217
          Width = 80
          Height = 13
          Caption = #1050#1086#1085#1090#1072#1082#1090' '#1083#1080#1094#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label17: TLabel
          Left = 99
          Top = 109
          Width = 41
          Height = 13
          Caption = #1057#1090#1072#1090#1091#1089
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label25: TLabel
          Left = 77
          Top = 136
          Width = 63
          Height = 13
          Caption = #1055#1088#1077#1086#1088#1080#1090#1077#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label24: TLabel
          Left = 386
          Top = 109
          Width = 42
          Height = 13
          Caption = #1044#1072#1090#1091#1084' '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 328
          Top = 82
          Width = 100
          Height = 13
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
          Color = clCream
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object DPredmet: TcxDBTextEdit
          Tag = 1
          Left = 146
          Top = 187
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'PREDMET'
          DataBinding.DataSource = dm.dsNabavki
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 9
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 409
        end
        object DBroj: TcxDBTextEdit
          Tag = 1
          Left = 146
          Top = 79
          TabStop = False
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ'
          DataBinding.DataSource = dm.dsNabavki
          Enabled = False
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 3
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object DRabEdinID: TcxDBTextEdit
          Tag = 1
          Left = 146
          Top = 25
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'RE'
          DataBinding.DataSource = dm.dsNabavki
          Properties.OnEditValueChanged = DRabEdinIDPropertiesEditValueChanged
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 41
        end
        object DRabEdinica: TcxDBLookupComboBox
          Tag = 1
          Left = 187
          Top = 25
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'RE'
          DataBinding.DataSource = dm.dsNabavki
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'ID'
            end
            item
              Caption = #1053#1072#1079#1080#1074
              FieldName = 'NAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dmMat.dsRE
          Properties.OnEditValueChanged = DRabEdinicaPropertiesEditValueChanged
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 368
        end
        object DGodina: TcxDBComboBox
          Tag = 1
          Left = 146
          Top = 52
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'GODINA'
          DataBinding.DataSource = dm.dsNabavki
          Properties.Items.Strings = (
            '2000'
            '2001'
            '2002'
            '2003'
            '2004'
            '2005'
            '2006'
            '2007'
            '2008'
            '2009'
            '2010'
            '2011'
            '2012'
            '2013'
            '2014'
            '2015'
            '2016'
            '2017'
            '2018'
            '2019'
            '2020'
            '2021'
            '2022'
            '2023'
            '2024'
            '2025'
            '2026'
            '2027'
            '2028'
            '2029'
            '2030'
            '2031'
            '2032'
            '2033'
            '2034'
            '2035'
            '2036'
            '2037'
            '2038'
            '2039'
            '2040'
            '2041'
            '2042'
            '2043'
            '2044'
            '2045'
            '2046'
            '2047'
            '2048'
            '2049'
            '2050')
          Properties.OnEditValueChanged = DGodinaPropertiesEditValueChanged
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object DTipNabavkaID: TcxDBTextEdit
          Tag = 1
          Left = 146
          Top = 160
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'TIP_NABAVKA'
          DataBinding.DataSource = dm.dsNabavki
          Properties.OnEditValueChanged = DTipNabavkaIDPropertiesEditValueChanged
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 7
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 41
        end
        object DTipNabavka: TcxDBLookupComboBox
          Tag = 1
          Left = 187
          Top = 160
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'TIP_NABAVKA'
          DataBinding.DataSource = dm.dsNabavki
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Width = 30
              FieldName = 'ID'
            end
            item
              Width = 180
              FieldName = 'Naziv'
            end
            item
              Caption = #1054#1087#1080#1089
              Width = 180
              FieldName = 'OPIS'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsTipNabavki
          Properties.OnEditValueChanged = DTipNabavkaPropertiesEditValueChanged
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 368
        end
        object DStatus: TcxDBLookupComboBox
          Tag = 1
          Left = 146
          Top = 106
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'STATUS'
          DataBinding.DataSource = dm.dsNabavki
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Caption = #1053#1072#1079#1080#1074
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dm.dsStatusNabavka
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object DPrioritet: TcxDBLookupComboBox
          Tag = 1
          Left = 146
          Top = 133
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'PRIORITET'
          DataBinding.DataSource = dm.dsNabavki
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Caption = #1053#1072#1079#1080#1074
              FieldName = 'NAZIV'
            end
            item
              Caption = #1042#1088#1077#1084#1077
              FieldName = 'VREME'
            end>
          Properties.ListSource = dm.dsPrioritet
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object DTipKontakt: TcxDBTextEdit
          Left = 146
          Top = 214
          BeepOnEnter = False
          DataBinding.DataField = 'TIP_KONTAKT'
          DataBinding.DataSource = dm.dsNabavki
          Enabled = False
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 10
          OnKeyDown = EnterKakoTab
          Width = 41
        end
        object DKontakt: TcxDBTextEdit
          Tag = 1
          Left = 187
          Top = 214
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'KONTAKT'
          DataBinding.DataSource = dm.dsNabavki
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 11
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 41
        end
        object DPartner: TcxExtLookupComboBox
          Tag = 1
          Left = 228
          Top = 214
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          Properties.DropDownAutoSize = True
          Properties.View = cxGridViewRepository1DBTableView1
          Properties.KeyFieldNames = 'TIP_PARTNER; ID'
          Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 12
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 327
        end
        object GroupBox1: TGroupBox
          Left = 13
          Top = 241
          Width = 292
          Height = 82
          Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1087#1088#1080#1084#1072#1114#1077' '#1087#1086#1085#1091#1076#1080
          TabOrder = 13
          object Label12: TLabel
            Left = 92
            Top = 56
            Width = 58
            Height = 13
            Caption = #1044#1072#1090#1091#1084' '#1076#1086
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label10: TLabel
            Left = 92
            Top = 29
            Width = 58
            Height = 13
            Caption = #1044#1072#1090#1091#1084' '#1086#1076
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DDatumPonudaDo: TcxDBDateEdit
            Left = 156
            Top = 53
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_PONUDA_DO'
            DataBinding.DataSource = dm.dsNabavki
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DDatumPonudaOd: TcxDBDateEdit
            Left = 156
            Top = 26
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_PONUDA_OD'
            DataBinding.DataSource = dm.dsNabavki
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
        end
        object GroupBox2: TGroupBox
          Left = 13
          Top = 329
          Width = 292
          Height = 166
          Caption = #1048#1079#1085#1086#1089
          TabOrder = 14
          object Label26: TLabel
            Left = 20
            Top = 103
            Width = 130
            Height = 13
            Caption = #1055#1088#1077#1076#1074#1080#1076#1077#1085' '#1074#1086' '#1076#1077#1085#1072#1088#1080
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label19: TLabel
            Left = 83
            Top = 76
            Width = 67
            Height = 13
            Caption = #1055#1088#1077#1076#1074#1080#1076#1077#1085
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label20: TLabel
            Left = 108
            Top = 22
            Width = 42
            Height = 13
            Caption = #1042#1072#1083#1091#1090#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label21: TLabel
            Left = 123
            Top = 49
            Width = 27
            Height = 13
            Caption = #1050#1091#1088#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 13
            Top = 130
            Width = 137
            Height = 13
            Caption = #1052#1086#1084#1077#1085#1090#1072#1083#1077#1085' '#1074#1086' '#1076#1077#1085#1072#1088#1080
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DIznosDenari: TcxDBTextEdit
            Left = 156
            Top = 100
            BeepOnEnter = False
            DataBinding.DataField = 'IZNOS_DENARI'
            DataBinding.DataSource = dm.dsNabavki
            Enabled = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 3
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DIznosPotrosen: TcxDBTextEdit
            Left = 156
            Top = 127
            BeepOnEnter = False
            DataBinding.DataField = 'IZNOSCENA'
            DataBinding.DataSource = dm.dsNabavki
            Enabled = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 4
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DIznos: TcxDBTextEdit
            Left = 156
            Top = 73
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'IZNOS'
            DataBinding.DataSource = dm.dsNabavki
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DValuta: TcxDBLookupComboBox
            Left = 156
            Top = 19
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'VALUTA'
            DataBinding.DataSource = dm.dsNabavki
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsValuta
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DKurs: TcxDBTextEdit
            Left = 156
            Top = 46
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'KURS'
            DataBinding.DataSource = dm.dsNabavki
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
        end
        object GroupBox3: TGroupBox
          Left = 311
          Top = 241
          Width = 584
          Height = 254
          Caption = #1054#1087#1080#1089' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          object Label9: TLabel
            Left = 13
            Top = 77
            Width = 109
            Height = 13
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label33: TLabel
            Left = 25
            Top = 50
            Width = 97
            Height = 13
            Caption = #1044#1072#1090#1091#1084' '#1082#1088#1077#1080#1088#1072#1085#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 95
            Top = 104
            Width = 27
            Height = 13
            Caption = #1054#1087#1080#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label23: TLabel
            Left = 57
            Top = 154
            Width = 65
            Height = 13
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label22: TLabel
            Left = 81
            Top = 204
            Width = 41
            Height = 13
            Caption = #1059#1089#1083#1086#1074#1080
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label18: TLabel
            Left = 74
            Top = 23
            Width = 48
            Height = 13
            Caption = #1050#1088#1077#1080#1088#1072#1083
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DDatumVazenje: TcxDBDateEdit
            Left = 128
            Top = 74
            OnFocusChanged = cxDBTextEditAllEnter
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_VAZENJE'
            DataBinding.DataSource = dm.dsNabavki
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DDatumKreirana: TcxDBTextEdit
            Left = 128
            Top = 47
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_KREIRANA'
            DataBinding.DataSource = dm.dsNabavki
            Enabled = False
            Style.BorderColor = clWindowFrame
            Style.Color = clWindow
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = clInactiveCaptionText
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 1
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DOpis: TcxDBMemo
            Left = 128
            Top = 101
            OnFocusChanged = cxDBTextEditAllEnter
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsNabavki
            Properties.WantReturns = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 44
            Width = 441
          end
          object DZabeleska: TcxDBMemo
            Left = 128
            Top = 151
            OnFocusChanged = cxDBTextEditAllEnter
            DataBinding.DataField = 'ZABELESKA'
            DataBinding.DataSource = dm.dsNabavki
            Properties.WantReturns = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 44
            Width = 441
          end
          object DUslovi: TcxDBMemo
            Left = 128
            Top = 201
            OnFocusChanged = cxDBTextEditAllEnter
            DataBinding.DataField = 'USLOVI'
            DataBinding.DataSource = dm.dsNabavki
            Properties.WantReturns = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 44
            Width = 441
          end
          object DKreiral: TcxDBTextEdit
            Left = 128
            Top = 20
            BeepOnEnter = False
            DataBinding.DataField = 'KREIRAL'
            DataBinding.DataSource = dm.dsNabavki
            Enabled = False
            Style.BorderColor = clWindowFrame
            Style.Color = clWindow
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = clInactiveCaptionText
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnKeyDown = EnterKakoTab
            Width = 121
          end
        end
        object DATUM_ODOBRUVANJE: TcxDBDateEdit
          Left = 434
          Top = 106
          OnFocusChanged = cxDBTextEditAllEnter
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM_ODOBRUVANJE'
          DataBinding.DataSource = dm.dsNabavki
          Style.Color = 16775924
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object OtkaziStavka: TcxButton
          Left = 926
          Top = 512
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 17
        end
        object ZapisiButton: TcxButton
          Left = 845
          Top = 512
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 16
        end
        object BrojDogovor: TcxDBTextEdit
          Left = 434
          Top = 79
          TabStop = False
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ_DOGOVOR'
          DataBinding.DataSource = dm.dsNabavki
          Style.LookAndFeel.SkinName = ''
          StyleDisabled.Color = 16113353
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clBackground
          StyleFocused.BorderStyle = ebsUltraFlat
          StyleFocused.Color = 16113353
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 18
          OnKeyDown = EnterKakoTab
          Width = 121
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 157
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 832
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 972
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxStatus'
        end
        item
          Visible = True
          ItemName = 'cxGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      CaptionButtons = <>
      DockedLeft = 629
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1048#1079#1074#1077#1096#1090#1072#1080
      CaptionButtons = <>
      DockedLeft = 707
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112
      CaptionButtons = <>
      DockedLeft = 494
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = #1053#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 246
      DockedTop = 0
      FloatLeft = 907
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar10: TdxBar
      Caption = #1057#1090#1072#1074#1082#1080
      CaptionButtons = <>
      DockedLeft = 360
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cxGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
    end
    object cxStatus: TcxBarEditItem
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      OnChange = cxStatusChange
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dm.dsStatus
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aBaranjeZaPonuda
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton1: TdxBarButton
      Action = aNajdobriPonudi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aNalozi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aOdobriOtvori
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPonudi
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aPonudiZaNabavka
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = PonudiZaNabavkaPoArtikli
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aStavki
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPoNabavkaStavki
      Category = 0
      SyncImageIndex = False
      ImageIndex = 27
    end
    object dxBarButton4: TdxBarButton
      Action = aRealizacijaKolicina
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = аSpecifikacijaZaOdbranaNabavka
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 600
    Top = 336
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBaranjeZaPonuda: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 45
      OnExecute = aBaranjeZaPonudaExecute
    end
    object aIsprazniStatus: TAction
      Caption = 'aIsprazniStatus'
    end
    object aNajdobriPonudi: TAction
      Caption = #1053#1072#1112#1076#1086#1073#1088#1080' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aNajdobriPonudiExecute
    end
    object aNalozi: TAction
      Caption = #1053#1072#1083#1086#1079#1080
      ImageIndex = 8
      OnExecute = aNaloziExecute
    end
    object aOdobriOtvori: TAction
      Caption = #1054#1090#1074#1086#1088#1080'/'#1054#1076#1086#1073#1088#1080
      ImageIndex = 6
      OnExecute = aOdobriOtvoriExecute
    end
    object aPoNabavkaStavki: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 26
      OnExecute = aPoNabavkaStavkiExecute
    end
    object aPonudi: TAction
      Caption = #1055#1086#1085#1091#1076#1080
      ImageIndex = 2
      OnExecute = aPonudiExecute
    end
    object aPonudiZaNabavka: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aPonudiZaNabavkaExecute
    end
    object PonudiZaNabavkaPoArtikli: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
      ImageIndex = 19
      OnExecute = PonudiZaNabavkaPoArtikliExecute
    end
    object aRealizacijaKolicina: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      ImageIndex = 19
      OnExecute = aRealizacijaKolicinaExecute
    end
    object aStavki: TAction
      Caption = #1044#1086#1076#1072#1076#1080'/'#1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 26
      OnExecute = aStavkiExecute
    end
    object aIscistiButton: TAction
      Caption = #1048#1089#1095#1080#1089#1090#1080
      ImageIndex = 23
      OnExecute = aIscistiButtonExecute
    end
    object аSpecifikacijaZaOdbranaNabavka: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1086#1076#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = аSpecifikacijaZaOdbranaNabavkaExecute
    end
    object aDizajnPonudiZaNabavka: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      OnExecute = aDizajnPonudiZaNabavkaExecute
    end
    object aDizajnPonudiZaNabavkaPoArtikli: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
      OnExecute = aDizajnPonudiZaNabavkaPoArtikliExecute
    end
    object aDizajnNajdobriPonudi: TAction
      Caption = #1053#1072#1112#1076#1086#1073#1088#1080' '#1087#1086#1085#1091#1076#1080
      OnExecute = aDizajnNajdobriPonudiExecute
    end
    object aDizajn: TAction
      Caption = 'aDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDizajnExecute
    end
    object aDizajnSpecifikacija: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1086#1076#1073#1088#1072#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      OnExecute = aDizajnSpecifikacijaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44228.496486122680000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 784
    Top = 416
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 472
    Top = 288
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER; ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 42
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 48
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        Caption = #1053#1072#1079#1080#1074
        DataBinding.FieldName = 'NAZIV'
        Width = 221
      end
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 696
    Top = 384
    object N5: TMenuItem
      Action = aDizajnSpecifikacija
    end
    object N3: TMenuItem
      Action = aDizajnPonudiZaNabavka
    end
    object N4: TMenuItem
      Action = aDizajnPonudiZaNabavkaPoArtikli
    end
    object N2: TMenuItem
      Action = aDizajnNajdobriPonudi
    end
  end
end
