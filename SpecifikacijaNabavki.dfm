object FrmSpecifikacijaNabavka: TFrmSpecifikacijaNabavka
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1080
  ClientHeight = 740
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 473
    Top = 126
    Width = 527
    Height = 585
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 2
      Top = 2
      Width = 523
      Height = 49
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      ParentBackground = False
      TabOrder = 0
      object RadioGroup1: TRadioGroup
        Left = 23
        Top = 4
        Width = 477
        Height = 39
        Caption = #1048#1079#1073#1086#1088' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
        TabOrder = 0
      end
    end
    object cxGrid2: TcxGrid
      Left = 2
      Top = 51
      Width = 523
      Height = 532
      Align = alClient
      TabOrder = 1
      TabStop = False
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid2DBTableView1KeyDown
        OnKeyPress = cxGrid2DBTableView1KeyPress
        DataController.DataSource = dm.dstblListaArtikliFiltrirani
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid2DBTableView1ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
          Width = 61
        end
        object cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVIDNAZIV'
          Width = 154
        end
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 58
        end
        object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 165
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Width = 69
        end
        object cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Width = 71
        end
        object cxGrid2DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid2DBTableView1TARIFA: TcxGridDBColumn
          DataBinding.FieldName = 'TARIFA'
          Width = 39
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid2DBTableView1BRPAK: TcxGridDBColumn
          DataBinding.FieldName = 'BRPAK'
          Width = 86
        end
        object cxGrid2DBTableView1KATALOG: TcxGridDBColumn
          DataBinding.FieldName = 'KATALOG'
          Width = 57
        end
        object cxGrid2DBTableView1BARKOD: TcxGridDBColumn
          DataBinding.FieldName = 'BARKOD'
          Width = 56
        end
        object cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'PROZVODITEL'
          Width = 91
        end
        object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Width = 89
        end
        object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Width = 111
        end
        object cxGrid2DBTableView1KLASIFIKACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'KLASIFIKACIJA'
          Width = 132
        end
      end
      object cxGrid2DBTableView2: TcxGridDBTableView
        OnKeyDown = cxGrid2DBTableView2KeyDown
        OnKeyPress = cxGrid2DBTableView2KeyPress
        DataController.DataSource = dm.dsPoPlanStavki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid2DBTableView2ARTSIF: TcxGridDBColumn
          DataBinding.FieldName = 'ARTSIF'
        end
        object cxGrid2DBTableView2NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 199
        end
        object cxGrid2DBTableView2ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
        end
        object cxGrid2DBTableView2ARTVIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVIDNAZIV'
          Width = 148
        end
        object cxGrid2DBTableView2KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid2DBTableView2CENA: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid2DBTableView2DDV: TcxGridDBColumn
          Caption = #1044#1044#1042
          DataBinding.FieldName = 'DDV'
          SortIndex = 0
          SortOrder = soAscending
          Width = 50
        end
        object cxGrid2DBTableView2MERKAID: TcxGridDBColumn
          DataBinding.FieldName = 'MERKAID'
          Width = 76
        end
        object cxGrid2DBTableView2MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Width = 96
        end
        object cxGrid2DBTableView2PODGRUPAID: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPAID'
          Width = 86
        end
        object cxGrid2DBTableView2PODGRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPANAZIV'
          Width = 138
        end
      end
      object cxGrid2DBTableView3: TcxGridDBTableView
        OnKeyDown = cxGrid2DBTableView3KeyDown
        OnKeyPress = cxGrid2DBTableView3KeyPress
        DataController.DataSource = dm.dsPlanStavkiRabEd
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid2DBTableView3ARTSIF: TcxGridDBColumn
          DataBinding.FieldName = 'ARTSIF'
        end
        object cxGrid2DBTableView3NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 196
        end
        object cxGrid2DBTableView3ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
        end
        object cxGrid2DBTableView3ARTVIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVIDNAZIV'
          Width = 132
        end
        object cxGrid2DBTableView3KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid2DBTableView3CENA: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid2DBTableView3DDV: TcxGridDBColumn
          DataBinding.FieldName = 'DDV'
          Width = 51
        end
        object cxGrid2DBTableView3MERKAID: TcxGridDBColumn
          DataBinding.FieldName = 'MERKAID'
          Width = 69
        end
        object cxGrid2DBTableView3MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Width = 106
        end
        object cxGrid2DBTableView3PODGRUPAID: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPAID'
          Width = 88
        end
        object cxGrid2DBTableView3PODGRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPANAZIV'
          Width = 112
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object RadioButton1: TRadioButton
      Left = 40
      Top = 19
      Width = 169
      Height = 17
      Caption = #1054#1076' '#1087#1083#1072#1085' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080#1086#1090' '#1089#1077#1082#1090#1086#1088
      Checked = True
      Color = clCream
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 377
      Top = 19
      Width = 98
      Height = 17
      Caption = #1054#1076' '#1096#1080#1092#1088#1072#1088#1085#1080#1082
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      OnClick = RadioButton2Click
    end
    object RadioButton3: TRadioButton
      Left = 215
      Top = 19
      Width = 148
      Height = 17
      Caption = #1054#1076' '#1087#1083#1072#1085' '#1079#1072' '#1089#1080#1090#1077' '#1089#1077#1082#1090#1086#1088#1080
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      OnClick = RadioButton3Click
    end
    object Panel6: TPanel
      Left = 10
      Top = 189
      Width = 297
      Height = 257
      ParentBackground = False
      TabOrder = 5
      Visible = False
      object Bevel2: TBevel
        Left = 8
        Top = 8
        Width = 281
        Height = 241
        Align = alCustom
        Shape = bsFrame
      end
      object Label7: TLabel
        Left = 16
        Top = 193
        Width = 54
        Height = 13
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 16
        Top = 149
        Width = 30
        Height = 13
        Caption = #1062#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 158
        Top = 149
        Width = 76
        Height = 13
        Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 95
        Top = 149
        Width = 50
        Height = 13
        Caption = #1044#1044#1042'(%)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 16
        Top = 15
        Width = 76
        Height = 13
        Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 16
        Top = 61
        Width = 95
        Height = 13
        Caption = #1054#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 95
        Top = 193
        Width = 50
        Height = 13
        Caption = #1045#1076'. '#1084#1077#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ARabEdinica: TcxExtLookupComboBox
        Tag = 1
        Left = 60
        Top = 34
        BeepOnEnter = False
        Properties.DropDownAutoSize = True
        Properties.View = cxGridViewRepository1DBTableView1
        Properties.KeyFieldNames = 'ID'
        Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
        Properties.OnEditValueChanged = ARabEdinicaPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnKeyDown = EnterPanel6
        Width = 221
      end
      object cxDBTextEdit1: TcxDBTextEdit
        Left = 95
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'MERKA'
        DataBinding.DataSource = dm.dstblListaArtikliFiltrirani
        Enabled = False
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clInactiveCaptionText
        TabOrder = 7
        OnKeyDown = EnterPanel6
        Width = 60
      end
      object AKolicina: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 6
        OnKeyDown = EnterPanel6
        Width = 73
      end
      object ACena: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 3
        OnExit = ACenaExit
        OnKeyDown = EnterPanel6
        Width = 73
      end
      object ADDV: TcxDBLookupComboBox
        Tag = 1
        Left = 95
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'DDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'TARIFA'
        Properties.ListColumns = <
          item
            FieldName = 'TARIFA'
          end>
        Properties.ListSource = dm.dsDDV
        Properties.OnEditValueChanged = ADDVPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 4
        OnKeyDown = EnterPanel6
        Width = 57
      end
      object ACenaSoDDV: TcxDBTextEdit
        Tag = 1
        Left = 158
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENADDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 5
        OnExit = ACenaSoDDVExit
        OnKeyDown = EnterPanel6
        Width = 76
      end
      object AOpis: TcxDBMemo
        Left = 16
        Top = 80
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnKeyDown = EnterPanel6
        Height = 63
        Width = 265
      end
      object ARabEdinicaID: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 34
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.OnEditValueChanged = ARabEdinicaIDPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnKeyDown = EnterPanel6
        Width = 43
      end
    end
    object Panel5: TPanel
      Left = 289
      Top = 233
      Width = 297
      Height = 257
      ParentBackground = False
      TabOrder = 6
      Visible = False
      object Bevel1: TBevel
        Left = 8
        Top = 8
        Width = 281
        Height = 241
        Align = alCustom
        Shape = bsFrame
      end
      object Label4: TLabel
        Left = 16
        Top = 193
        Width = 54
        Height = 13
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 16
        Top = 149
        Width = 30
        Height = 13
        Caption = #1062#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 158
        Top = 149
        Width = 76
        Height = 13
        Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 95
        Top = 149
        Width = 50
        Height = 13
        Caption = #1044#1044#1042'(%)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 16
        Top = 15
        Width = 76
        Height = 13
        Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label23: TLabel
        Left = 16
        Top = 61
        Width = 95
        Height = 13
        Caption = #1054#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 95
        Top = 193
        Width = 50
        Height = 13
        Caption = #1045#1076'. '#1084#1077#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PRabEdinica: TcxExtLookupComboBox
        Tag = 1
        Left = 59
        Top = 34
        BeepOnEnter = False
        Properties.DropDownAutoSize = True
        Properties.View = cxGridViewRepository1DBTableView1
        Properties.KeyFieldNames = 'ID'
        Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
        Properties.OnEditValueChanged = PRabEdinicaPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnKeyDown = EnterPanel5
        Width = 222
      end
      object cxDBTextEdit2: TcxDBTextEdit
        Left = 95
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'MERKAID'
        DataBinding.DataSource = dm.dsPoPlanStavki
        Enabled = False
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clInactiveCaptionText
        TabOrder = 7
        Width = 60
      end
      object PKolicina: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 6
        OnKeyDown = EnterPanel5
        Width = 73
      end
      object PCena: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 3
        OnExit = PCenaExit
        OnKeyDown = EnterPanel5
        Width = 73
      end
      object PDDV: TcxDBLookupComboBox
        Tag = 1
        Left = 95
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'DDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'TARIFA'
        Properties.ListColumns = <
          item
            FieldName = 'TARIFA'
          end>
        Properties.ListSource = dm.dsDDV
        Properties.OnEditValueChanged = PDDVPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 4
        OnKeyDown = EnterPanel5
        Width = 57
      end
      object PCenaSoDDV: TcxDBTextEdit
        Tag = 1
        Left = 158
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENADDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 5
        OnExit = PCenaSoDDVExit
        OnKeyDown = EnterPanel5
        Width = 76
      end
      object POpis: TcxDBMemo
        Left = 16
        Top = 80
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnKeyDown = EnterPanel5
        Height = 63
        Width = 265
      end
      object PRabEdinicaID: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 34
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.OnEditValueChanged = PRabEdinicaIDPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnKeyDown = EnterPanel5
        Width = 43
      end
    end
    object Panel8: TPanel
      Left = 218
      Top = 42
      Width = 297
      Height = 209
      ParentBackground = False
      TabOrder = 7
      Visible = False
      object Bevel4: TBevel
        Left = 8
        Top = 8
        Width = 281
        Height = 193
        Align = alCustom
        Shape = bsFrame
      end
      object Label1: TLabel
        Left = 16
        Top = 145
        Width = 54
        Height = 13
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 16
        Top = 101
        Width = 30
        Height = 13
        Caption = #1062#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 158
        Top = 101
        Width = 76
        Height = 13
        Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 95
        Top = 101
        Width = 50
        Height = 13
        Caption = #1044#1044#1042'(%)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label25: TLabel
        Left = 16
        Top = 13
        Width = 95
        Height = 13
        Caption = #1054#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 95
        Top = 147
        Width = 50
        Height = 13
        Caption = #1045#1076'. '#1084#1077#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxDBTextEdit3: TcxDBTextEdit
        Left = 95
        Top = 164
        BeepOnEnter = False
        DataBinding.DataField = 'MERKAID'
        DataBinding.DataSource = dm.dsPlanStavkiRabEd
        Enabled = False
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clInactiveCaptionText
        TabOrder = 5
        Width = 60
      end
      object SKolicina: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 164
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 4
        OnKeyDown = EnterPanel8
        Width = 73
      end
      object SCena: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 120
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnExit = SCenaExit
        OnKeyDown = EnterPanel8
        Width = 73
      end
      object SDDV: TcxDBLookupComboBox
        Tag = 1
        Left = 95
        Top = 120
        BeepOnEnter = False
        DataBinding.DataField = 'DDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'TARIFA'
        Properties.ListColumns = <
          item
            FieldName = 'TARIFA'
          end>
        Properties.ListSource = dm.dsDDV
        Properties.OnEditValueChanged = SDDVPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnKeyDown = EnterPanel8
        Width = 57
      end
      object SCenaSoDDV: TcxDBTextEdit
        Tag = 1
        Left = 158
        Top = 120
        BeepOnEnter = False
        DataBinding.DataField = 'CENADDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 3
        OnExit = SCenaSoDDVExit
        OnKeyDown = EnterPanel8
        Width = 76
      end
      object SOpis: TcxDBMemo
        Left = 16
        Top = 32
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnKeyDown = EnterPanel8
        Height = 63
        Width = 265
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 473
    Height = 585
    Align = alLeft
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 1
    object Panel3: TPanel
      Left = 2
      Top = 2
      Width = 469
      Height = 49
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      ParentBackground = False
      TabOrder = 0
      object Label6: TLabel
        Left = 8
        Top = 17
        Width = 213
        Height = 16
        Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CNabavka_ID: TcxDBLabel
        Left = 347
        Top = 22
        DataBinding.DataField = 'ID'
        DataBinding.DataSource = dm.dsNabavki
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clHighlight
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Visible = False
        Height = 21
        Width = 35
      end
      object CBrojNabavka: TcxDBLabel
        Left = 227
        Top = 15
        AutoSize = True
        DataBinding.DataField = 'BROJNABAVKA'
        DataBinding.DataSource = dm.dsNabavki
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clHighlight
        Style.Font.Height = -13
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
    end
    object cxGrid1: TcxGrid
      Left = 2
      Top = 51
      Width = 469
      Height = 532
      Align = alClient
      TabOrder = 1
      TabStop = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnKeyPress = cxGrid1DBTableView1KeyPress
        DataController.DataSource = dm.dsPoNabavkaStavki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.00 , .'
            Kind = skSum
            Column = cxGrid1DBTableView1Iznos
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        object cxGrid1DBTableView1ArtSifra: TcxGridDBColumn
          DataBinding.FieldName = 'ARTSIF'
          Options.Editing = False
          Width = 61
        end
        object cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIVARTIKAL'
          Options.Editing = False
          Width = 162
        end
        object cxGrid1DBTableView1RABEDINIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABEDINIDNAZIV'
          Options.Editing = False
          Width = 190
        end
        object cxGrid1DBTableView1Cena: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid1DBTableView1DDV: TcxGridDBColumn
          DataBinding.FieldName = 'DDV'
        end
        object cxGrid1DBTableView1CenaDDV: TcxGridDBColumn
          DataBinding.FieldName = 'CENADDV'
          Options.Editing = False
          Width = 91
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid1DBTableView1Iznos: TcxGridDBColumn
          DataBinding.FieldName = 'IIZNOS'
          Width = 104
        end
        object cxGrid1DBTableView1Opis: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 239
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel7: TPanel
      Left = 71
      Top = 150
      Width = 297
      Height = 257
      ParentBackground = False
      TabOrder = 2
      Visible = False
      object Bevel3: TBevel
        Left = 8
        Top = 8
        Width = 281
        Height = 241
        Align = alCustom
        Shape = bsFrame
      end
      object Label15: TLabel
        Left = 16
        Top = 193
        Width = 54
        Height = 13
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 16
        Top = 149
        Width = 30
        Height = 13
        Caption = #1062#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 158
        Top = 149
        Width = 76
        Height = 13
        Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 95
        Top = 149
        Width = 50
        Height = 13
        Caption = #1044#1044#1042'(%)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 16
        Top = 15
        Width = 76
        Height = 13
        Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label24: TLabel
        Left = 16
        Top = 61
        Width = 95
        Height = 13
        Caption = #1054#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label29: TLabel
        Left = 95
        Top = 193
        Width = 50
        Height = 13
        Caption = #1045#1076'. '#1084#1077#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RabEdinica: TcxExtLookupComboBox
        Tag = 1
        Left = 60
        Top = 34
        BeepOnEnter = False
        Properties.DropDownAutoSize = True
        Properties.View = cxGridViewRepository1DBTableView1
        Properties.KeyFieldNames = 'ID'
        Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
        Properties.OnEditValueChanged = RabEdinicaPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 1
        OnKeyDown = EnterPanel7
        Width = 221
      end
      object cxDBTextEdit4: TcxDBTextEdit
        Left = 95
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'MERKA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Enabled = False
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = clInactiveCaptionText
        TabOrder = 7
        Width = 60
      end
      object Kolicina: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 212
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 6
        OnKeyDown = EnterPanel7
        Width = 73
      end
      object Cena: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 3
        OnExit = CenaExit
        OnKeyDown = EnterPanel7
        Width = 73
      end
      object DDV: TcxDBLookupComboBox
        Tag = 1
        Left = 95
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'DDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'TARIFA'
        Properties.ListColumns = <
          item
            FieldName = 'TARIFA'
          end>
        Properties.ListSource = dm.dsDDV
        Properties.OnEditValueChanged = DDVPropertiesEditValueChanged
        Style.Color = 16775924
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 4
        OnKeyDown = EnterPanel7
        Width = 57
      end
      object CenaSoDDV: TcxDBTextEdit
        Tag = 1
        Left = 158
        Top = 168
        BeepOnEnter = False
        DataBinding.DataField = 'CENADDV'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 5
        OnExit = CenaSoDDVExit
        OnKeyDown = EnterPanel7
        Width = 76
      end
      object Opis: TcxDBMemo
        Left = 16
        Top = 80
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 2
        OnKeyDown = EnterPanel7
        Height = 63
        Width = 265
      end
      object RabEdinicaID: TcxDBTextEdit
        Tag = 1
        Left = 16
        Top = 34
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsPoNabavkaStavki
        Properties.OnEditValueChanged = RabEdinicaIDPropertiesEditValueChanged
        StyleDisabled.Color = 16113353
        StyleDisabled.TextColor = clWindowText
        StyleFocused.BorderStyle = ebsThick
        StyleFocused.Color = 16113353
        TabOrder = 0
        OnKeyDown = EnterPanel7
        Width = 43
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 711
    Width = 1000
    Height = 29
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072',  F8 - '#1048#1079#1073#1088#1080#1096#1080' '#1089#1090#1072#1082#1072
        Width = 220
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Enter - '#1042#1085#1077#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080', Insert - '#1064#1080#1092#1088#1072#1088#1085#1080#1082' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080' '
        Width = 290
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Ctrl+Alt+S - '#1047#1072#1095#1091#1074#1072#1112' '#1080#1079#1075#1083#1077#1076' , Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
        Width = 280
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F1 - '#1055#1086#1084#1086#1096', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079#1080' '
        Width = 250
      end>
    LookAndFeel.SkinName = 'Office2007Blue'
    Color = clMenuHighlight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1000
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 7
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = aAzuriraj
            ImageIndex = 12
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aBrisi
            ImageIndex = 11
            ShortCut = 119
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
      end
      item
        Items = <
          item
            Caption = #1043#1086#1076#1080#1085#1072
            CommandStyle = csComboBox
            CommandProperties.Width = 150
          end>
      end
      item
        Items = <
          item
            Action = aAzuriraj
            ImageIndex = 12
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aBrisi
            ImageIndex = 11
            ShortCut = 119
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aZacuvajVoExcel
            Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' &Excel'
            ImageIndex = 17
            ShortCut = 16453
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            Action = aZacuvajIzgled
            Caption = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
            ImageIndex = 9
            ShortCut = 49235
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aPomos
            ImageIndex = 24
            ShortCut = 112
            CommandProperties.ButtonSize = bsLarge
          end
          item
            Action = aIzlez
            ImageIndex = 20
            CommandProperties.ButtonSize = bsLarge
          end>
      end
      item
        Items = <
          item
            ChangesAllowed = [caModify]
            Items = <
              item
                Caption = '&ActionClientItem0'
              end>
            Caption = '&ActionClientItem0'
            KeyTip = 'F'
          end>
        AutoSize = False
      end>
    LargeImages = dm.cxLargeImages
    Images = dm.cxSmallImages
    Left = 408
    Top = 352
    StyleName = 'Platform Default'
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 20
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aESCExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aZacuvajIzgled: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 9
      ShortCut = 49235
      OnExecute = aZacuvajIzgledExecute
    end
    object aZacuvajVoExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 17
      ShortCut = 16453
      OnExecute = aZacuvajVoExcelExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      ShortCut = 112
      OnExecute = aPomosExecute
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 840
    Top = 64
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dm.dsRabEdinica
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      Styles.ContentEven = dm.cxStyle8
      Styles.ContentOdd = dm.cxStyle8
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 51
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 219
      end
      object cxGridViewRepository1DBTableView1SPISOK: TcxGridDBColumn
        DataBinding.FieldName = 'SPISOK'
        Width = 98
      end
      object cxGridViewRepository1DBTableView1POTEKLO: TcxGridDBColumn
        DataBinding.FieldName = 'POTEKLO'
        Width = 68
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 424
    Top = 176
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 96
    Top = 232
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 480
    Top = 232
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 69
      FloatClientHeight = 150
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 133
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 92
      FloatClientHeight = 64
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 340
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 96
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 168
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 184
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40772.411942407410000000
      OptionsFormatting.UseNativeStyles = True
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
end
