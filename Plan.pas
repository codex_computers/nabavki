unit Plan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, DBCtrls, cxDBLabel, cxLabel, dxRibbonSkins, cxPCdxBarPopupMenu,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmPlan = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1Bar5: TdxBar;
    cxGodina: TcxBarEditItem;
    cxRabEdinica: TcxBarEditItem;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ1: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1DatumOd: TcxGridDBColumn;
    cxGrid1DBTableView1DatumDo: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RabEdinica: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ODOBRUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1ODOBRIL: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    Panel8: TPanel;
    Label12: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    POpis: TcxDBMemo;
    PGodina: TcxDBComboBox;
    PBroj: TcxDBTextEdit;
    PRabEdinica: TcxDBLookupComboBox;
    PRabEdinicaID: TcxDBTextEdit;
    Status: TcxDBLookupComboBox;
    DatumOdobruvanje: TcxDBDateEdit;
    cxGroupBox1: TcxGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    PDatumOd: TcxDBDateEdit;
    PDatumDo: TcxDBDateEdit;
    Panel4: TPanel;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1Artvid: TcxGridDBColumn;
    cxGrid2DBTableView1ArtvidNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1Cena: TcxGridDBColumn;
    cxGrid2DBTableView1DDV: TcxGridDBColumn;
    cxGrid2DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1IIZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1MerkaID: TcxGridDBColumn;
    cxGrid2DBTableView1MerkaNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1PodgrupaID: TcxGridDBColumn;
    cxGrid2DBTableView1PodgrupaNaziv: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    cxTabSheet4: TcxTabSheet;
    Panel9: TPanel;
    GroupBox2: TGroupBox;
    Label21: TLabel;
    Label20: TLabel;
    Label31: TLabel;
    Label30: TLabel;
    Label22: TLabel;
    SKolicina: TcxDBTextEdit;
    SCena: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCenaSoDDV: TcxDBTextEdit;
    SIznos: TcxDBTextEdit;
    GroupBox1: TGroupBox;
    Label25: TLabel;
    Label18: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    SArtvid: TcxDBTextEdit;
    SArtvidNaziv: TcxDBTextEdit;
    SSifra: TcxDBTextEdit;
    SNaziv: TcxDBTextEdit;
    SPodgrupaId: TcxDBTextEdit;
    SPodgrupaNaziv: TcxDBTextEdit;
    SMerkaId: TcxDBTextEdit;
    SMerkaNaziv: TcxDBTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aNovaStavka: TAction;
    aAzurirajStavka: TAction;
    aBrisiStavka: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton20: TdxBarLargeButton;
    aZacuvajStavkiExcel: TAction;
    OtkaziStavka: TcxButton;
    ZapisiStavka: TcxButton;
    aZapisiStavki: TAction;
    aOtkaziStavka: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    aStavkiZaOdbranPlan: TAction;
    aRealizacijaKolSektor: TAction;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    aOtvoriOdobri: TAction;
    aPlanZaIzbraniotSektor: TAction;
    aPlanZaSiteSektori: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    aPecatiStavki: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    aGodisenPlan: TAction;
    PopupMenu2: TPopupMenu;
    aDizajnPlanZaIzbraniotSektor: TAction;
    aDizajnPlanZaSiteSektori: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    aPopupDizajn: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxRabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure cxRabEdinicaChange(Sender: TObject);
    procedure cxGodinaChange(Sender: TObject);
    procedure aNovaStavkaExecute(Sender: TObject);
    procedure aAzurirajStavkaExecute(Sender: TObject);
    procedure aBrisiStavkaExecute(Sender: TObject);
    procedure aZacuvajStavkiExcelExecute(Sender: TObject);
    procedure aZapisiStavkiExecute(Sender: TObject);
    procedure aOtkaziStavkaExecute(Sender: TObject);
    procedure Iznos (Sender: TObject);
    procedure PGodinaExit(Sender: TObject);
    procedure aStavkiZaOdbranPlanExecute(Sender: TObject);
    procedure aRealizacijaKolSektorExecute(Sender: TObject);
    procedure aOtvoriOdobriExecute(Sender: TObject);
    procedure aPlanZaIzbraniotSektorExecute(Sender: TObject);
    procedure aPlanZaSiteSektoriExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure aPecatiStavkiExecute(Sender: TObject);
    procedure aGodisenPlanExecute(Sender: TObject);
    procedure aDizajnPlanZaIzbraniotSektorExecute(Sender: TObject);
    procedure aPopupDizajnExecute(Sender: TObject);
    procedure aDizajnPlanZaSiteSektoriExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmPlan: TfrmPlan;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmMaticni,
  IzborNaArtikli, RealizacijaPlanSektor, PregledGodisenPlan;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPlan.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode


procedure TfrmPlan.Iznos (Sender: TObject);
begin
     if (SCenaSoDDV.Text <> '')and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
        begin
           dm.tblSpecifikacijaZaPlanIIZNOS.Value:=dm.tblSpecifikacijaZaPlanKOLICINA.Value * dm.tblSpecifikacijaZaPlanCENADDV.Value;
        end;
end;

procedure TfrmPlan.aNovaStavkaExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
     begin
      if (cxGrid1DBTableView1.Controller.SelectedRecordCount = 1) then
        begin
           frmIzborArtikli:=TfrmIzborArtikli.Create(Application);
           frmIzborArtikli.ShowModal;
           frmIzborArtikli.Free;
           cxPageControl2.ActivePage:=cxTabSheet3;
           cxgrid2.SetFocus;
           StateActive:=dsBrowse;
        end
     else  ShowMessage('����������� ���� !!!!!');
     end
   else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPlan.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
  begin
     cxPageControl1.ActivePage:=cxTabSheet2;
     cxTabSheet1.Enabled:= false;
     dxRibbon1.Enabled:=False;
     cxPageControl2.Enabled:=False;
     Panel8.Enabled:= true;
     PBroj.Enabled:=true;
     PGodina.Enabled:= true;
     PDatumOd.SetFocus;

     cxGrid1DBTableView1.DataController.DataSet.Insert;
     StateActive:=dsInsert;

     if avtomatsko_odobruvanje = '1' then
        begin
          dm.tblPlanSektorSTATUS.Value:=odobren_plan;
          dm.tblPlanSektorDATUM_ODOBRUVANJE.Value:=now;
          dm.tblPlanSektorODOBRIL.Value:=dmKon.user;
        end
     else if avtomatsko_odobruvanje = '0' then
        dm.tblPlanSektorSTATUS.Value:=otvoren_plan;

     dm.tblPlanSektorGODINA.Value:=cxGodina.EditValue;
     PRabEdinica.EditValue:=cxRabEdinica.EditValue;
     dm.tblPlanSektorRE.Value:=cxRabEdinica.EditValue;
     dm.tblPlanSektorBROJ.Value:=dm.zemiMaxPlusEden3(dm.MaxBrPlan,'GODINA',Null,Null,StrToInt(PGodina.EditText), Null,Null, 'MAXBR');

  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPlan.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
  begin
    if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           cxPageControl1.ActivePage:=cxTabSheet2;
           cxTabSheet1.Enabled:= false;
           dxRibbon1.Enabled:=false;
           cxPageControl2.Enabled:=false;

           Panel8.Enabled:=true;

           PBroj.Enabled:=false;
           PGodina.Enabled:= false;
           PDatumOd.SetFocus;

           dm.tblPlanSektor.Edit;
           StateActive:=dsEdit;
        end
     else  ShowMessage('����������� ���� !!!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPlan.aAzurirajStavkaExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
  begin
    if (cxGrid2DBTableView1.Controller.SelectedRecordCount = 1) and (cxPageControl2.Enabled = true) then
         begin
            cxPageControl2.ActivePage:=cxTabSheet4;
            cxTabSheet3.Enabled:=False;
            dxRibbon1.Enabled:=False;
            cxPageControl1.Enabled:=False;
            Panel9.Enabled:= true;
            SKolicina.SetFocus;

            dm.tblSpecifikacijaZaPlan.Edit;
            StateActive:=dsEdit;
          end
      else ShowMessage('����������� ������!!!!!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPlan.aBrisiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
   begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              dm.tblPlanSektor.Delete;
        end
     else ShowMessage('����������� ���� !!!');
   end;
end;

//	����� �� ���������� �� ����������
procedure TfrmPlan.aRealizacijaKolSektorExecute(Sender: TObject);
begin
     frmGrafikRealPlan:=TfrmGrafikRealPlan.Create(Application);
     frmGrafikRealPlan.ShowModal;
     frmGrafikRealPlan.Free;
end;

procedure TfrmPlan.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPlan.aIzlezExecute(Sender: TObject);
begin
 if StateActive=dsBrowse then
        if (cxPageControl1.ActivePage = cxTabSheet2) and (cxPageControl1.Enabled = true) then
           begin
              cxPageControl1.ActivePage:=cxTabSheet1;
           end
         else if cxPageControl2.ActivePage = cxTabSheet4 then
           begin
               cxTabSheet3.Enabled:=true;
               cxPageControl2.ActivePage:=cxTabSheet3;
           end
         else
           begin
               Close;
           end;

     if StateActive in [dsedit,dsInsert] then
        if  (cxPageControl1.ActivePage = cxTabSheet2) and (cxPageControl1.Enabled = true) then
           begin
              frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
              if frmDaNe.ShowModal =mrYes then
                begin
                   cxGrid1DBTableView1.DataController.DataSet.Cancel;
                   StateActive:=dsBrowse;
                   dm.RestoreControls(Panel8);
                   PBroj.Enabled:=true;
                   PGodina.Enabled:=true;
                   Panel8.Enabled:= false;
                   cxTabSheet1.Enabled:= true;
                   cxPageControl1.ActivePage:=cxTabSheet1;
                   cxGrid1.SetFocus;

                   dxRibbon1.Enabled:=true;
                   cxPageControl2.Enabled:=true;
              end;
           end
        else
        if cxPageControl2.ActivePage = cxTabSheet4 then
           begin
              frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
              if frmDaNe.ShowModal =mrYes then
                begin
                   cxGrid2DBTableView1.DataController.DataSet.Cancel;
                   StateActive:=dsBrowse;
                   dm.RestoreControls(Panel9);
                   Panel9.Enabled:= false;
                   cxTabSheet3.Enabled:=true;
                   cxPageControl2.ActivePage:= cxTabSheet3;
                   dxRibbon1.Enabled:=True;
                   cxPageControl1.Enabled:=True;
                   cxGrid2.SetFocus;
                end;
           end;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPlan.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPlan.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPlan.aZacuvajStavkiExcelExecute(Sender: TObject);
begin
    zacuvajVoExcel(cxGrid2, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPlan.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPlan.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPlan.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if sender = PGodina then
       begin
         if (StateActive in [dsEdit,dsInsert]) then
             dm.tblPlanSektorBROJ.Value:=dm.zemiMaxPlusEden3(dm.MaxBrPlan,'GODINA',Null,Null,StrToInt(PGodina.Text),Null,Null, 'MAXBR');
         end;
    if (sender = PRabEdinica) or (Sender = PRabEdinicaID) then
       begin

       end;

    if sender = SCena then
       begin
         if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '') then
         begin
            dm.tblSpecifikacijaZaPlanCENADDV.Value:= dm.tblSpecifikacijaZaPlanCENA.Value + (SDDV.EditValue*dm.tblSpecifikacijaZaPlanCENA.Value/100);
            dm.tblSpecifikacijaZaPlanIIZNOS.Value:=dm.tblSpecifikacijaZaPlanKOLICINA.Value * dm.tblSpecifikacijaZaPlanCENADDV.Value;
         end;
       end;
    if sender = SCenaSoDDV then
      if (StateActive in [dsEdit]) and (SCenaSoDDV.Text <> '') then
       begin
          dm.tblSpecifikacijaZaPlanCENA.Value:=dm.tblSpecifikacijaZaPlanCENADDV.Value*100/(100 + SDDV.EditValue);
          dm.tblSpecifikacijaZaPlanIIZNOS.Value:=dm.tblSpecifikacijaZaPlanKOLICINA.Value * dm.tblSpecifikacijaZaPlanCENADDV.Value;
       end;
    if sender = SDDV then
      if ((SKolicina.Text <> '') and (SCena.Text <> '') and (StateActive in [dsedit])) then
         begin
            dm.tblSpecifikacijaZaPlanCENADDV.Value:=dm.tblSpecifikacijaZaPlanCENA.Value  + (SDDV.EditValue*dm.tblSpecifikacijaZaPlanCENA.Value/100);
         end;

end;

procedure TfrmPlan.cxGodinaChange(Sender: TObject);
begin
     dm.tblPlanSektor.ParamByName('godina').Value:= cxGodina.EditValue;
     dm.tblPlanSektor.FullRefresh;

     cxGrid1.SetFocus;

     dm.tblSpecifikacijaZaPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblSpecifikacijaZaPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblSpecifikacijaZaPlan.FullRefresh;
end;

procedure TfrmPlan.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           dm.tblSpecifikacijaZaPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
           dm.tblSpecifikacijaZaPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
           dm.tblSpecifikacijaZaPlan.FullRefresh;
        end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPlan.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPlan.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPlan.PGodinaExit(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPlan.prefrli;
begin
end;

procedure TfrmPlan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmPlan.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

   dm.tblPlanSektor.Open;
   dm.tblSpecifikacijaZaPlan.Open;
   dm.DDV.Open;
   dmMat.tblRE.Open;
   dm.IzbraniSektoriZaPlan.close;
   dm.IzbraniSektoriZaPlan.ParamByName('poteklo').Value:=intToStr(dmKon.re) + ',';
   dm.IzbraniSektoriZaPlan.ParamByName('spisok').Value:=intToStr(dmKon.re) + ','+'%';
   dm.IzbraniSektoriZaPlan.Open;
   dm.Firmi.close;
   dm.Firmi.ParamByName('re').Value:=dmKon.re;
   dm.Firmi.Open;

   dm.tblReportDizajn.Open;
   dm.tblPlanZaSiteSektori.Open;
   dm.tblPlanZaIzbraniotSektor.Open;
   dm.tblStavkiZaOdbranPlan.Open;

   if not odobriPlan then
      begin
        Status.Enabled:=false;
      end;
end;

//------------------------------------------------------------------------------

procedure TfrmPlan.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    dxComponentPrinter1Link2.ReportTitle.Text := '��������� �� ������ �� ����';

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);

     dm.frxReport.Script.Clear;
     dm.frxReport.Clear;

     cxPageControl1.ActivePage:=cxTabSheet1;
     cxPageControl2.ActivePage:=cxTabSheet3;

     dmmat.tblRE.Locate('ID',dmkon.UserRE,[]);

     dm.tblPlanSektor.ParamByName('sektor').Value:=dmKon.re;
     dm.tblPlanSektor.ParamByName('godina').Value:=dmKon.godina ;
     dm.tblPlanSektor.Open;

     cxGodina.EditValue:= IntToStr(dmKon.godina);
     cxRabEdinica.EditValue:=dmKon.re;

     dm.tblSpecifikacijaZaPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblSpecifikacijaZaPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblSpecifikacijaZaPlan.FullRefresh;
     dm.tag:=1;
     cxGrid1.SetFocus;
     StateActive:=dsBrowse;
end;
//------------------------------------------------------------------------------

procedure TfrmPlan.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPlan.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       case key of
     VK_INSERT: begin
         if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
            begin
               frmIzborArtikli:=TfrmIzborArtikli.Create(Application);
               frmIzborArtikli.ShowModal;
               frmIzborArtikli.Free;
               cxPageControl2.ActivePage:=cxTabSheet3;
               cxGrid2.SetFocus;
               StateActive:=dsBrowse;
            end
        else
          begin
             ShowMessage('����������� ���� !!!!!');
          end;
       end;
       end;
end;

procedure TfrmPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPlan.cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case key of
      VK_INSERT: begin
         if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
          begin
             frmIzborArtikli:=TfrmIzborArtikli.Create(Application);
             frmIzborArtikli.ShowModal;
             frmIzborArtikli.Free;
             cxPageControl2.ActivePage:=cxTabSheet3;
             cxgrid2.SetFocus;
             StateActive:=dsBrowse;
          end
        else
          begin
             ShowMessage('����������� ���� !!!!!');
          end;
       end;
      end;
end;

procedure TfrmPlan.cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmPlan.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
           cxGodina.Enabled:=False;
           cxRabEdinica.Enabled:=False;
        end;
        if (cxPageControl1.ActivePage = cxTabSheet2) then
        begin
            cxGodina.Enabled:=true;
            cxRabEdinica.Enabled:=true;
        end;
end;

procedure TfrmPlan.cxRabEdinicaChange(Sender: TObject);
begin
     dm.tblPlanSektor.ParamByName('sektor').Value:=cxRabEdinica.EditValue;
     dm.tblPlanSektor.FullRefresh;

     cxGrid1.SetFocus;

     dm.tblSpecifikacijaZaPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblSpecifikacijaZaPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblSpecifikacijaZaPlan.FullRefresh;
end;

procedure TfrmPlan.cxRabEdinicaPropertiesEditValueChanged(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmPlan.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
         if dm.Validacija(Panel8) = false then
          begin
              if StateActive in [dsInsert] then
              begin
                 cxGrid1DBTableView1.DataController.DataSet.Post;
                 dm.tblPlanSektor.ParamByName('sektor').Value:=PRabEdinica.EditValue;
                 dm.tblPlanSektor.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                 dm.tblPlanSektor.FullRefresh;
              end;

              if StateActive in [dsEdit] then
              begin
                  cxGrid1DBTableView1.DataController.DataSet.Post;
                  dm.tblPlanSektor.ParamByName('sektor').Value:=PRabEdinica.EditValue;
                  dm.tblPlanSektor.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                  dm.tblPlanSektor.FullRefresh;
              end;

              StateActive:=dsBrowse;
              dm.RestoreControls(Panel8);

              PBroj.Enabled:=true;
              PGodina.Enabled:=true;

              Panel8.Enabled:= false;
              cxTabSheet1.Enabled:= true;
              cxPageControl1.ActivePage:=cxTabSheet1;

              cxRabEdinica.EditValue:=dm.tblPlanSektorRE.Value;
              cxGodina.EditValue:=dm.tblPlanSektorGODINA.Value;
              dxRibbon1.Enabled:=true;
              cxPageControl2.Enabled:=true;

              cxGrid1.SetFocus;
              dm.tblPlanSektor.Locate('GODINA; BROJ', VarArrayOf([StrToInt(PGodina.Text), StrToInt(PBroj.Text)]) , []);
       end;
  end;
end;

procedure TfrmPlan.aZapisiStavkiExecute(Sender: TObject);
var pom1, pom2, pom3, pom4 :integer;
  st: TDataSetState;
begin
   ZapisiStavka.SetFocus;
//
  st := cxGrid2DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
     pom1:=dm.tblSpecifikacijaZaPlanARTVID.Value;
     pom2:=dm.tblSpecifikacijaZaPlanARTSIF.Value;
     pom3:=dm.tblSpecifikacijaZaPlanBROJ.Value;
     pom4:=dm.tblSpecifikacijaZaPlanGODINA.Value;
     if dm.Validacija(Panel9) = false then
        begin
           dm.tblSpecifikacijaZaPlan.Post;
           StateActive:=dsBrowse;

           dm.RestoreControls(Panel9);

           dm.tblSpecifikacijaZaPlan.FullRefresh;

           Panel9.Enabled:= false;
           cxTabSheet3.Enabled:= true;
           cxPageControl2.ActivePage:=cxTabSheet3;
           dxRibbon1.Enabled:=True;
           cxPageControl1.Enabled:=True;

           cxGrid2.SetFocus;
           dm.tblSpecifikacijaZaPlan.Locate('ARTVID; ARTSIF; BROJ; GODINA', VarArrayOf([pom1, pom2, pom3, pom4]) , []);
        end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmPlan.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      StateActive:=dsBrowse;
      dm.RestoreControls(panel8);

      PBroj.Enabled:=true;
      PGodina.Enabled:=true;
      Panel8.Enabled:= false;
      cxTabSheet1.Enabled:= true;
      cxPageControl1.ActivePage:=cxTabSheet1;
      cxGrid1.SetFocus;

      dxRibbon1.Enabled:=true;
      cxPageControl2.Enabled:=true;
  end;
end;

procedure TfrmPlan.aOtkaziStavkaExecute(Sender: TObject);
begin
  if (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      dm.tblSpecifikacijaZaPlan.Cancel;
      StateActive:=dsBrowse;

      dm.RestoreControls(Panel9);

      Panel9.Enabled:= false;
      cxTabSheet3.Enabled:=true;
      cxPageControl2.ActivePage:= cxTabSheet3;
      dxRibbon1.Enabled:=True;
      cxPageControl1.Enabled:=True;

      cxGrid2.SetFocus;
  end;
end;

procedure TfrmPlan.aOtvoriOdobriExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
          if odobriPlan then
             begin
               if dm.tblPlanSektorSTATUS.Value = otvoren_plan then
                  begin
                    frmDaNe:=TfrmDaNe.Create(self,'������� !', '���� ��� ������� ���� ������ �� �� �������� ������?',1);
                    if frmDaNe.ShowModal =mrYes then
                       begin
                          dm.tblPlanSektor.Edit;
                          dm.tblPlanSektorSTATUS.Value:=odobren_plan;
                          dm.tblPlanSektorODOBRIL.Value:=dmKon.user;
                          dm.tblPlanSektorDATUM_ODOBRUVANJE.Value:=Now;
                          dm.tblPlanSektor.Post;
                       end
                  end
               else
                  begin
                    frmDaNe:=TfrmDaNe.Create(self,'������� !', '���� ��� ������� ���� ������ �� �� �������� ������?',1);
                    if frmDaNe.ShowModal =mrYes then
                       begin
                          dm.tblPlanSektor.Edit;
                          dm.tblPlanSektorSTATUS.Value:=otvoren_plan;
                          dm.tblPlanSektorODOBRIL.Value:=dmKon.user;
                          dm.tblPlanSektorDATUM_ODOBRUVANJE.Value:=Now;
                          dm.tblPlanSektor.Post;
                       end
                  end
             end
          else ShowMessage('������ ����� �� ��������/���������� ���� !!!');
        end
     else  ShowMessage('����������� ���� !!!!');
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPlan.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPlan.aPecatiStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmPlan.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPlan.aPlanZaIzbraniotSektorExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4002;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=cxGodina.EditValue;
     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=cxRabEdinica.EditValue;
     dm.tblPlanZaIzbraniotSektor.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmPlan.aPlanZaSiteSektoriExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4001;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaSiteSektori.ParamByName('poteklo').Value:=(IntToStr(dmKon.re))+',';
     dm.tblPlanZaSiteSektori.ParamByName('spisok').Value:=(IntToStr(dmKon.re))+','+'%';
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=cxgodina.EditValue;
     dm.tblPlanZaSiteSektori.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPlan.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPlan.aPopupDizajnExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500 );
end;

procedure TfrmPlan.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPlan.aStavkiZaOdbranPlanExecute(Sender: TObject);
var RptStream:TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4003;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblStavkiZaOdbranPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblStavkiZaOdbranPlan.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPlan.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPlan.aBrisiStavkaExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)then
   begin
      if (cxGrid2DBTableView1.Controller.SelectedRecordCount = 1) and (cxPageControl2.Enabled = true) then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              dm.tblSpecifikacijaZaPlan.Delete;
        end
     else ShowMessage('����������� ������!!!!!!');
   end;
end;

procedure TfrmPlan.aDizajnPlanZaIzbraniotSektorExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4002;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=cxGodina.EditValue;
     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=cxRabEdinica.EditValue;
     dm.tblPlanZaIzbraniotSektor.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmPlan.aDizajnPlanZaSiteSektoriExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4001;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaSiteSektori.ParamByName('poteklo').Value:=(IntToStr(dmKon.re))+',';
     dm.tblPlanZaSiteSektori.ParamByName('spisok').Value:=(IntToStr(dmKon.re))+','+'%';
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=cxgodina.EditValue;
     dm.tblPlanZaSiteSektori.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPlan.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmPlan.aGodisenPlanExecute(Sender: TObject);
begin
     frmGodisenPlan:=TfrmGodisenPlan.Create(Application);
     frmGodisenPlan.ShowModal;
     frmGodisenPlan.Free;
end;

procedure TfrmPlan.aHelpExecute(Sender: TObject);
begin
     Application.HelpContext(118);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPlan.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPlan.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
