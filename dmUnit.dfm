object dm: Tdm
  OldCreateOrder = False
  Height = 1699
  Width = 1613
  object tblListaArtikli: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    NAZIV = :NAZIV,'
      '    MERKA =:MERKA,'
      '    CENA = :CENA,'
      '    BRPAK = :BRPAK,'
      '    KATALOG = :KATALOG,'
      '    BARKOD = :BARKOD,'
      '    PROZVODITEL =:PROZVODITEL,'
      '    GRUPA =:GRUPA,'
      '    KLASIFIKACIJA =:KLASIFIKACIJA,'
      '    TARIFA =:TARIFA'
      '    '
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_ARTIKAL'
      'WHERE'
      '        ARTVID = :OLD_ARTVID'
      '        and ID = :OLD_ID')
    InsertSQL.Strings = (
      'INSERT INTO MTR_ARTIKAL('
      '    ARTVID,'
      '    ID,'
      '    NAZIV,'
      '    MERKA,'
      '    CENA,'
      '    BRPAK,'
      '    KATALOG,'
      '    BARKOD,'
      '    PROZVODITEL,'
      '    GRUPA,'
      '    KLASIFIKACIJA,'
      '    TARIFA'
      ')'
      'VALUES('
      '    :ARTVID,'
      '    :ID,'
      '    :NAZIV,'
      '    :MERKA,'
      '    :CENA,'
      '    :BRPAK,'
      '    :KATALOG,'
      '    :BARKOD,'
      '    :PROZVODITEL,'
      '    :GRUPA,'
      '    :KLASIFIKACIJA,'
      '    :TARIFA'
      ')')
    RefreshSQL.Strings = (
      'select m.artvid,'
      '       m.id,'
      '       m.naziv,'
      '       m.merka,'
      '       cast(m.cena as numeric(15, 4)) as cena,'
      '       m.brpak,'
      '       m.katalog,'
      '       m.barkod,'
      '       m.prozvoditel,'
      '       m.grupa,'
      '       m.klasifikacija,'
      '       cast(m.kolicina as numeric(15,4)) as kolicina,'
      '       m.uninaziv,'
      '       m.unifont,'
      '       m.oblik,'
      '       m.jacina,'
      '       m.tarifa,'
      '       ma.opis as ArtVidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       mr.naziv as MerkaNaziv,'
      '       mp.naziv as ProizvoditelNaziv'
      'from mtr_artikal m  '
      'inner join mtr_artvid ma on m.artvid= ma.id'
      
        'left outer join mtr_artgrupa mg on m.grupa= mg.id and ma.id=mg.m' +
        'tr_artvid_id'
      'left outer join mat_merka mr on mr.id = m.merka'
      'left outer join mat_proizvoditel mp on mp.id = m.prozvoditel'
      ''
      ' WHERE '
      '        M.ARTVID = :OLD_ARTVID'
      '    and M.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select m.artvid,'
      '       m.id,'
      '       m.naziv,'
      '       m.merka,'
      '       cast(m.cena as numeric(15, 4)) as cena,'
      '       m.brpak,'
      '       m.katalog,'
      '       m.barkod,'
      '       m.prozvoditel,'
      '       m.grupa,'
      '       m.klasifikacija,'
      '       cast(m.kolicina as numeric(15,4)) as kolicina,'
      '       m.uninaziv,'
      '       m.unifont,'
      '       m.oblik,'
      '       m.jacina,'
      '       m.tarifa,'
      '       ma.opis as ArtVidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       mr.naziv as MerkaNaziv,'
      '       mp.naziv as ProizvoditelNaziv'
      'from mtr_artikal m  '
      'inner join mtr_artvid ma on m.artvid= ma.id'
      
        'left outer join mtr_artgrupa mg on m.grupa= mg.id and ma.id=mg.m' +
        'tr_artvid_id'
      'left outer join mat_merka mr on mr.id = m.merka'
      'left outer join mat_proizvoditel mp on mp.id = m.prozvoditel')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 576
    object tblListaArtikliARTVID: TFIBIntegerField
      DefaultExpression = '0'
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblListaArtikliID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblListaArtikliNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072'(ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliBRPAK: TFIBIntegerField
      DisplayLabel = #1041#1088'. '#1087#1072#1082
      FieldName = 'BRPAK'
    end
    object tblListaArtikliKATALOG: TFIBStringField
      DisplayLabel = #1050#1072#1090#1072#1083#1086#1075
      FieldName = 'KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliBARKOD: TFIBStringField
      DisplayLabel = #1041#1072#1088#1082#1086#1076
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' (ID)'
      FieldName = 'PROZVODITEL'
    end
    object tblListaArtikliGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliKLASIFIKACIJA: TFIBStringField
      DisplayLabel = #1050#1083#1072#1089#1080#1092#1080#1082#1072#1094#1080#1112#1072
      FieldName = 'KLASIFIKACIJA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliUNINAZIV: TFIBStringField
      FieldName = 'UNINAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliUNIFONT: TFIBStringField
      FieldName = 'UNIFONT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliOBLIK: TFIBIntegerField
      FieldName = 'OBLIK'
    end
    object tblListaArtikliJACINA: TFIBFloatField
      FieldName = 'JACINA'
    end
    object tblListaArtikliTARIFA: TFIBFloatField
      DefaultExpression = '0'
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
    object tblListaArtikliARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblListaArtikliKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
  end
  object dsListaArtikli: TDataSource
    DataSet = tblListaArtikli
    Left = 32
    Top = 512
  end
  object tblPlanSektor: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_PLAN'
      'SET '
      '    RE = :RE,'
      '    OPIS = :OPIS,'
      '    VREME = :VREME,'
      '    DATUM_KREIRAN = :DATUM_KREIRAN,'
      '    KREIRAL = :KREIRAL,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    STATUS = :STATUS,'
      '    ODOBRIL = :ODOBRIL,'
      '    DATUM_ODOBRUVANJE = :DATUM_ODOBRUVANJE'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_PLAN'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_PLAN('
      '    BROJ,'
      '    GODINA,'
      '    RE,'
      '    OPIS,'
      '    VREME,'
      '    DATUM_KREIRAN,'
      '    KREIRAL,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    STATUS,'
      '    ODOBRIL,'
      '    DATUM_ODOBRUVANJE'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :GODINA,'
      '    :RE,'
      '    :OPIS,'
      '    :VREME,'
      '    :DATUM_KREIRAN,'
      '    :KREIRAL,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :STATUS,'
      '    :ODOBRIL,'
      '    :DATUM_ODOBRUVANJE'
      ')')
    RefreshSQL.Strings = (
      'select'
      '       p.broj,'
      '       p.godina,'
      '       p.re,'
      '       p.opis,'
      '       p.vreme,'
      '       p.datum_kreiran,'
      '       p.kreiral,'
      '       p.datum_od,'
      '       p.datum_do,'
      '       mr.naziv,'
      '       p.broj ||'#39'/'#39'|| p.godina as  BrojGodina,'
      '       p.re ||'#39'. '#39'|| mr.naziv as RabEdIDNaziv,'
      '       p.status,'
      '       p.odobril,'
      '       p.datum_odobruvanje,'
      '       s.naziv as StatusNaziv'
      'from po_plan p'
      'inner join mat_re mr on mr.id = p.re'
      'inner join mat_status s on s.id = p.status'
      'where(  p.re = :sektor and p.godina=:godina'
      '     ) and (     P.BROJ = :OLD_BROJ'
      '    and P.GODINA = :OLD_GODINA'
      '     )'
      '    '
      'order by p.broj desc')
    SelectSQL.Strings = (
      'select'
      '       p.broj,'
      '       p.godina,'
      '       p.re,'
      '       p.opis,'
      '       p.vreme,'
      '       p.datum_kreiran,'
      '       p.kreiral,'
      '       p.datum_od,'
      '       p.datum_do,'
      '       mr.naziv,'
      '       p.broj ||'#39'/'#39'|| p.godina as  BrojGodina,'
      '       p.re ||'#39'. '#39'|| mr.naziv as RabEdIDNaziv,'
      '       p.status,'
      '       p.odobril,'
      '       p.datum_odobruvanje,'
      '       s.naziv as StatusNaziv'
      'from po_plan p'
      'inner join mat_re mr on mr.id = p.re'
      'inner join mat_status s on s.id = p.status'
      'where p.re = :sektor and p.godina=:godina'
      'order by p.broj desc')
    AfterInsert = tblPlanSektorAfterInsert
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 112
    Top = 576
    object tblPlanSektorGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblPlanSektorBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ'
    end
    object tblPlanSektorRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' (ID)'
      FieldName = 'RE'
    end
    object tblPlanSektorOPIS: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorDATUM_KREIRAN: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
      FieldName = 'DATUM_KREIRAN'
    end
    object tblPlanSektorKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorVREME: TFIBDateTimeField
      FieldName = 'VREME'
    end
    object tblPlanSektorDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblPlanSektorDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblPlanSektorBROJGODINA: TFIBStringField
      FieldName = 'BROJGODINA'
      Size = 18
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorRABEDIDNAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RABEDIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' (ID)'
      FieldName = 'STATUS'
    end
    object tblPlanSektorODOBRIL: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1086#1076#1086#1073#1088#1080#1083' '#1087#1083#1072#1085#1086#1090
      FieldName = 'ODOBRIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorSTATUSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanSektorDATUM_ODOBRUVANJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1114#1077'/'#1086#1090#1074#1072#1088#1072#1114#1077
      FieldName = 'DATUM_ODOBRUVANJE'
    end
  end
  object dsPlanSektor: TDataSource
    AutoEdit = False
    DataSet = tblPlanSektor
    Left = 112
    Top = 512
  end
  object tblSpecifikacijaZaPlan: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_PLAN_STAVKI'
      'SET '
      '    KOLICINA = :KOLICINA,'
      '    CENA = :CENA,'
      '    DDV = :DDV,'
      '    CENADDV = :CENADDV'
      ''
      'WHERE'
      '    BROJ = :OLD_BROJ and GODINA = :OLD_GODINA'
      '    and ARTSIF = :OLD_ARTSIF and ARTVID = :OLD_ARTVID')
    DeleteSQL.Strings = (
      'DELETE FROM PO_PLAN_STAVKI'
      '    '
      'WHERE'
      '    BROJ = :OLD_BROJ and GODINA = :OLD_GODINA'
      '    and ARTSIF = :OLD_ARTSIF and ARTVID = :OLD_ARTVID'
      '        ')
    InsertSQL.Strings = (
      'INSERT INTO PO_PLAN_STAVKI('
      '    BROJ,'
      '    GODINA,'
      '    ARTVID,'
      '    ARTSIF,'
      '    KOLICINA,'
      '    CENA,'
      '    DDV,'
      '    CENADDV,'
      '    KREIRAL,'
      '    VREME'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :GODINA,'
      '    :ARTVID,'
      '    :ARTSIF,'
      '    :KOLICINA,'
      '    :CENA,'
      '    :DDV,'
      '    :CENADDV,'
      '    :KREIRAL,'
      '    :VREME'
      ')')
    RefreshSQL.Strings = (
      'select    d.broj,'
      '          d.godina,'
      '          d.artvid,'
      '          d.artsif,'
      '          d.kolicina,'
      '          d.cena,'
      '          d.ddv,'
      '          d.cenaddv,'
      '          d.naziv,'
      '          d.vreme,'
      '          d.kreiral,       '
      '          d.iiznos,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      'from  VIEW_PO__PLANSTAVKI d'
      'where d.broj = :broj and d.godina=:godina')
    SelectSQL.Strings = (
      'select    d.broj,'
      '          d.godina,'
      '          d.artvid,'
      '          d.artsif,'
      '          d.kolicina,'
      '          d.cena,'
      '          d.ddv,'
      '          d.cenaddv,'
      '          d.naziv,'
      '          d.vreme,'
      '          d.kreiral,       '
      '          d.iiznos,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      'from  VIEW_PO__PLANSTAVKI d'
      'where d.broj = :broj and d.godina=:godina')
    BeforePost = tblSpecifikacijaZaPlanBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 200
    Top = 576
    object tblSpecifikacijaZaPlanBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblSpecifikacijaZaPlanGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblSpecifikacijaZaPlanARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblSpecifikacijaZaPlanARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblSpecifikacijaZaPlanDDV: TFIBFloatField
      Alignment = taCenter
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblSpecifikacijaZaPlanCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object tblSpecifikacijaZaPlanNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblSpecifikacijaZaPlanKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanIIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblSpecifikacijaZaPlanMERKAID: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKAID'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanPODGRUPAID: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072'(ID)'
      FieldName = 'PODGRUPAID'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanPODGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'PODGRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaZaPlanKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object tblSpecifikacijaZaPlanCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsSpecifikacijaPlan: TDataSource
    DataSet = tblSpecifikacijaZaPlan
    Left = 200
    Top = 512
  end
  object CenaStavki: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_PREDLOGCENA(?ARTSIF, ?ARTVID)')
    Left = 1048
    Top = 496
  end
  object DDVStavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_PREDLOGDDV(?ARTSIF, ?ARTVID)')
    Left = 936
    Top = 256
  end
  object MaxBrPlan: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXBROJPLAN(?GODINA)')
    Left = 864
    Top = 256
  end
  object tblNabavi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_NABAVKA'
      'SET '
      '    RE = :RE,'
      '    BROJ = :BROJ,'
      '    GODINA = :GODINA,'
      '    PREDMET = :PREDMET,'
      '    DATUM_KREIRANA = :DATUM_KREIRANA,'
      '    DATUM_VAZENJE = :DATUM_VAZENJE,'
      '    DATUM_PONUDA_OD = :DATUM_PONUDA_OD,'
      '    DATUM_PONUDA_DO = :DATUM_PONUDA_DO,'
      '    TIP_NABAVKA = :TIP_NABAVKA,'
      '    OPIS = :OPIS,'
      '    TIP_KONTAKT = :TIP_KONTAKT,'
      '    KONTAKT = :KONTAKT,'
      '    STATUS = :STATUS,'
      '    KREIRAL = :KREIRAL,'
      '    IZNOS = :IZNOS,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS,'
      '    PRIORITET = :PRIORITET,'
      '    USLOVI = :USLOVI,'
      '    ZABELESKA = :ZABELESKA,'
      '    VREME = :VREME,'
      '    ODOBRIL = :ODOBRIL,'
      '    DATUM_ODOBRUVANJE = :DATUM_ODOBRUVANJE,'
      '    BROJ_DOGOVOR = :BROJ_DOGOVOR'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_NABAVKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_NABAVKA('
      '    ID,'
      '    RE,'
      '    BROJ,'
      '    GODINA,'
      '    PREDMET,'
      '    DATUM_KREIRANA,'
      '    DATUM_VAZENJE,'
      '    DATUM_PONUDA_OD,'
      '    DATUM_PONUDA_DO,'
      '    TIP_NABAVKA,'
      '    OPIS,'
      '    TIP_KONTAKT,'
      '    KONTAKT,'
      '    STATUS,'
      '    KREIRAL,'
      '    IZNOS,'
      '    VALUTA,'
      '    KURS,'
      '    PRIORITET,'
      '    USLOVI,'
      '    ZABELESKA,'
      '    VREME,'
      '    ODOBRIL,'
      '    DATUM_ODOBRUVANJE,'
      '    BROJ_DOGOVOR'
      ')'
      'VALUES('
      '    :ID,'
      '    :RE,'
      '    :BROJ,'
      '    :GODINA,'
      '    :PREDMET,'
      '    :DATUM_KREIRANA,'
      '    :DATUM_VAZENJE,'
      '    :DATUM_PONUDA_OD,'
      '    :DATUM_PONUDA_DO,'
      '    :TIP_NABAVKA,'
      '    :OPIS,'
      '    :TIP_KONTAKT,'
      '    :KONTAKT,'
      '    :STATUS,'
      '    :KREIRAL,'
      '    :IZNOS,'
      '    :VALUTA,'
      '    :KURS,'
      '    :PRIORITET,'
      '    :USLOVI,'
      '    :ZABELESKA,'
      '    :VREME,'
      '    :ODOBRIL,'
      '    :DATUM_ODOBRUVANJE,'
      '    :BROJ_DOGOVOR'
      ')')
    RefreshSQL.Strings = (
      'select   p.id,'
      '         p.re,'
      '         p.broj,'
      '         p.godina,'
      '         p.predmet,'
      '         p.datum_kreirana,'
      '         p.datum_vazenje,'
      '         p.datum_ponuda_od,'
      '         p.datum_ponuda_do,'
      '         p.tip_nabavka,'
      '         p.opis,'
      '         p.tip_kontakt,'
      '         p.kontakt,'
      '         p.status,'
      '         p.kreiral,'
      '         p.iznos,'
      '         p.valuta,'
      '         p.kurs,'
      '         p.prioritet,'
      '         p.uslovi,'
      '         p.zabeleska,'
      '         p.vreme,'
      '         p.tip_nabavka ||'#39'.'#39'||tn.naziv  as tip_nabavka_naziv,'
      
        '         p.datum_kreirana ||'#39' - '#39'||p.datum_vazenje as PERIOD_VAZ' +
        'ENJE,'
      '         ms.naziv as nazivstatus,'
      '         p.re ||'#39'. '#39'|| mr.naziv  as REIDNAZIV,'
      '         mp.naziv PartnerNaziv,'
      '         mv.naziv as valutaNaziv,'
      '         sp.naziv PrioritetNaziv,'
      '         mr.naziv as RENAZIV,'
      '         p.broj || '#39'/'#39' || p.re ||'#39'/'#39' ||p.godina as BrojNabavka,'
      '         mr.poteklo,'
      '         (select sum(ps.kolicina * ps.cenaddv) '
      '         from po_nabavka_stavki ps'
      '         where ps.nabavka_id = p.id'
      '         group by ps.nabavka_id'
      '         )iznoscena,'
      '         cast(p.iznos_denari as numeric(15,4)) as iznos_denari,'
      '         p.odobril,'
      '         p.datum_odobruvanje,'
      '         p.broj_dogovor,'
      '         extractmonth(p.datum_kreirana) as mesec,'
      
        '         case when extractmonth(p.datum_kreirana) = 1 then '#39#1032#1072#1085#1091 +
        #1072#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 2 then '#39#1092#1077#1074#1088 +
        #1091#1072#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 3 then '#39#1052#1072#1088#1090 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 4 then '#39#1040#1087#1088#1080 +
        #1083#39
      '              when extractmonth(p.datum_kreirana) = 5 then '#39#1052#1072#1112#39
      
        '              when extractmonth(p.datum_kreirana) = 6 then '#39#1032#1091#1085#1080 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 7 then '#39#1032#1091#1083#1080 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 8 then '#39#1040#1074#1075#1091 +
        #1089#1090#39
      
        '              when extractmonth(p.datum_kreirana) = 9 then '#39#1057#1077#1087#1090 +
        #1077#1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 10 then '#39#1054#1082#1090 +
        #1086#1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 11 then '#39#1053#1086#1077 +
        #1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 12 then '#39#1044#1077#1082 +
        #1077#1084#1074#1088#1080#39
      '         end "mesecNaziv"'
      ''
      ''
      'from  po_nabavka p'
      'inner join po_tip_nabavka tn on tn.id = p.tip_nabavka'
      'inner join mat_status ms on ms.id = p.status'
      'inner join mat_re mr on mr.id = p.re'
      
        'inner join mat_partner mp on mp.id=p.kontakt and p.tip_kontakt=m' +
        'p.tip_partner'
      'left outer join mat_valuta mv on mv.id=p.valuta'
      'inner join sys_prioritet sp on sp.id=p.prioritet'
      ''
      'where p.godina = :godina and p.status like :status'
      '      and  ( mr.id in (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or mr.id in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      'order by p.broj desc'
      '')
    SelectSQL.Strings = (
      'select   p.id,'
      '         p.re,'
      '         p.broj,'
      '         p.godina,'
      '         p.predmet,'
      '         p.datum_kreirana,'
      '         p.datum_vazenje,'
      '         p.datum_ponuda_od,'
      '         p.datum_ponuda_do,'
      '         p.tip_nabavka,'
      '         p.opis,'
      '         p.tip_kontakt,'
      '         p.kontakt,'
      '         p.status,'
      '         p.kreiral,'
      '         p.iznos,'
      '         p.valuta,'
      '         p.kurs,'
      '         p.prioritet,'
      '         p.uslovi,'
      '         p.zabeleska,'
      '         p.vreme,'
      '         p.tip_nabavka ||'#39'.'#39'||tn.naziv  as tip_nabavka_naziv,'
      
        '         p.datum_kreirana ||'#39' - '#39'||p.datum_vazenje as PERIOD_VAZ' +
        'ENJE,'
      '         ms.naziv as nazivstatus,'
      '         p.re ||'#39'. '#39'|| mr.naziv  as REIDNAZIV,'
      '         mp.naziv PartnerNaziv,'
      '         mv.naziv as valutaNaziv,'
      '         sp.naziv PrioritetNaziv,'
      '         mr.naziv as RENAZIV,'
      '         p.broj || '#39'/'#39' || p.re ||'#39'/'#39' ||p.godina as BrojNabavka,'
      '         mr.poteklo,'
      '         (select sum(ps.kolicina * ps.cenaddv) '
      '         from po_nabavka_stavki ps'
      '         where ps.nabavka_id = p.id'
      '         group by ps.nabavka_id'
      '         )iznoscena,'
      '         cast(p.iznos_denari as numeric(15,4)) as iznos_denari,'
      '         p.odobril,'
      '         p.datum_odobruvanje,'
      '         p.broj_dogovor,'
      '         extractmonth(p.datum_kreirana) as mesec,'
      
        '         case when extractmonth(p.datum_kreirana) = 1 then '#39#1032#1072#1085#1091 +
        #1072#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 2 then '#39#1092#1077#1074#1088 +
        #1091#1072#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 3 then '#39#1052#1072#1088#1090 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 4 then '#39#1040#1087#1088#1080 +
        #1083#39
      '              when extractmonth(p.datum_kreirana) = 5 then '#39#1052#1072#1112#39
      
        '              when extractmonth(p.datum_kreirana) = 6 then '#39#1032#1091#1085#1080 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 7 then '#39#1032#1091#1083#1080 +
        #39
      
        '              when extractmonth(p.datum_kreirana) = 8 then '#39#1040#1074#1075#1091 +
        #1089#1090#39
      
        '              when extractmonth(p.datum_kreirana) = 9 then '#39#1057#1077#1087#1090 +
        #1077#1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 10 then '#39#1054#1082#1090 +
        #1086#1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 11 then '#39#1053#1086#1077 +
        #1084#1074#1088#1080#39
      
        '              when extractmonth(p.datum_kreirana) = 12 then '#39#1044#1077#1082 +
        #1077#1084#1074#1088#1080#39
      '         end "mesecNaziv"'
      ''
      ''
      'from  po_nabavka p'
      'inner join po_tip_nabavka tn on tn.id = p.tip_nabavka'
      'inner join mat_status ms on ms.id = p.status'
      'inner join mat_re mr on mr.id = p.re'
      
        'inner join mat_partner mp on mp.id=p.kontakt and p.tip_kontakt=m' +
        'p.tip_partner'
      'left outer join mat_valuta mv on mv.id=p.valuta'
      'inner join sys_prioritet sp on sp.id=p.prioritet'
      ''
      'where p.godina = :godina and p.status like :status'
      '      and  ( mr.id in (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or mr.id in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      'order by p.broj desc'
      '')
    AfterInsert = tblNabaviAfterInsert
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 440
    object tblNabaviID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblNabaviRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072' (ID)'
      FieldName = 'RE'
    end
    object tblNabaviGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblNabaviPREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090
      FieldName = 'PREDMET'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviDATUM_KREIRANA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1082#1088#1077#1080#1088#1072#1085#1072
      FieldName = 'DATUM_KREIRANA'
      DisplayFormat = 'dd.mm.yy'
    end
    object tblNabaviDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblNabaviDATUM_PONUDA_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1087#1086#1085#1091#1076#1072' '#1086#1076
      FieldName = 'DATUM_PONUDA_OD'
    end
    object tblNabaviDATUM_PONUDA_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1087#1086#1085#1091#1076#1072' '#1076#1086
      FieldName = 'DATUM_PONUDA_DO'
    end
    object tblNabaviTIP_NABAVKA: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'TIP_NABAVKA'
    end
    object tblNabaviOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviTIP_KONTAKT: TFIBIntegerField
      FieldName = 'TIP_KONTAKT'
    end
    object tblNabaviKONTAKT: TFIBIntegerField
      FieldName = 'KONTAKT'
    end
    object tblNabaviSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblNabaviKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviIZNOS: TFIBBCDField
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblNabaviVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviKURS: TFIBFloatField
      FieldName = 'KURS'
    end
    object tblNabaviPRIORITET: TFIBSmallIntField
      FieldName = 'PRIORITET'
    end
    object tblNabaviUSLOVI: TFIBStringField
      FieldName = 'USLOVI'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblNabaviTIP_NABAVKA_NAZIV: TFIBStringField
      FieldName = 'TIP_NABAVKA_NAZIV'
      Size = 107
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviPERIOD_VAZENJE: TFIBStringField
      FieldName = 'PERIOD_VAZENJE'
      Size = 38
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviNAZIVSTATUS: TFIBStringField
      FieldName = 'NAZIVSTATUS'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviREIDNAZIV: TFIBStringField
      FieldName = 'REIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviVALUTANAZIV: TFIBStringField
      FieldName = 'VALUTANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviPRIORITETNAZIV: TFIBStringField
      FieldName = 'PRIORITETNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblNabaviRENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviBROJNABAVKA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJNABAVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviIZNOSCENA: TFIBFloatField
      DisplayLabel = #1052#1086#1084#1077#1085#1090#1072#1083#1077#1085' '#1080#1079#1085#1086#1089
      FieldName = 'IZNOSCENA'
      DisplayFormat = '0.00 , .'
    end
    object tblNabaviIZNOS_DENARI: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1074#1086' '#1076#1077#1085#1072#1088#1080
      FieldName = 'IZNOS_DENARI'
      DisplayFormat = '0.00 , .'
    end
    object tblNabaviODOBRIL: TFIBStringField
      DisplayLabel = #1054#1076#1086#1073#1088#1080#1083
      FieldName = 'ODOBRIL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviDATUM_ODOBRUVANJE: TFIBDateField
      FieldName = 'DATUM_ODOBRUVANJE'
    end
    object tblNabaviBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'BROJ_DOGOVOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabaviMESEC: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1077#1094
      FieldName = 'MESEC'
    end
    object tblNabavimesecNaziv: TFIBStringField
      DisplayLabel = #1052#1077#1089#1077#1094
      FieldName = 'mesecNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNabavki: TDataSource
    AutoEdit = False
    DataSet = tblNabavi
    Left = 32
    Top = 384
  end
  object TipNabavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_TIP_NABAVKA'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_TIP_NABAVKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_TIP_NABAVKA('
      '    ID,'
      '    NAZIV,'
      '    OPIS'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS'
      ')')
    RefreshSQL.Strings = (
      'select  p.id,'
      '        p.naziv,'
      '        p.opis'
      ''
      'from po_tip_nabavka p'
      ''
      ' WHERE '
      '        P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select  p.id,'
      '        p.naziv,'
      '        p.opis'
      ''
      'from po_tip_nabavka p')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 96
    Top = 440
    object TipNabavkiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TipNabavkiNAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object TipNabavkiOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipNabavki: TDataSource
    DataSet = TipNabavki
    Left = 104
    Top = 384
  end
  object DDV: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.tarifa'
      'from  mat_tarifa m')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 16
    Top = 192
    object DDVTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
  end
  object dsDDV: TDataSource
    DataSet = DDV
    Left = 16
    Top = 136
  end
  object Podgrupa: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      #9'MTR_ARTGRUPA.ID,'
      #9'MTR_ARTGRUPA.NAZIV,'
      #9'MTR_ARTGRUPA.MTR_ARTVID_ID,'
      #9'MTR_ARTGRUPA.KOREN,'
      #9'MTR_ARTGRUPA.POTEKLO'
      'from mtr_artgrupa'
      'where(  MTR_ARTGRUPA.MTR_ARTVID_ID like :artvidID'
      '     ) and (     MTR_ARTGRUPA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      #9'MTR_ARTGRUPA.ID,'
      #9'MTR_ARTGRUPA.NAZIV,'
      #9'MTR_ARTGRUPA.MTR_ARTVID_ID,'
      #9'MTR_ARTGRUPA.KOREN,'
      #9'MTR_ARTGRUPA.POTEKLO'
      'from mtr_artgrupa'
      'where MTR_ARTGRUPA.MTR_ARTVID_ID like :artvidID')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 72
    Top = 192
    object PodgrupaID: TFIBStringField
      FieldName = 'ID'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PodgrupaNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PodgrupaMTR_ARTVID_ID: TFIBIntegerField
      FieldName = 'MTR_ARTVID_ID'
    end
    object PodgrupaKOREN: TFIBStringField
      FieldName = 'KOREN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PodgrupaPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPodgrupa: TDataSource
    DataSet = Podgrupa
    Left = 72
    Top = 136
  end
  object Prioritet: TpFIBDataSet
    SelectSQL.Strings = (
      'select sp.id, sp.naziv, sp.vreme'
      'from sys_prioritet sp')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 208
    Top = 192
    object PrioritetID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PrioritetNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrioritetVREME: TFIBIntegerField
      FieldName = 'VREME'
    end
  end
  object dsPrioritet: TDataSource
    DataSet = Prioritet
    Left = 208
    Top = 136
  end
  object StatusNabavka: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where(  msg.naziv = '#39'PO_NABAVKA'#39
      '     ) and (     MS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where msg.naziv = '#39'PO_NABAVKA'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 136
    Top = 192
    object StatusNabavkaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object StatusNabavkaGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object StatusNabavkaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStatusNabavka: TDataSource
    DataSet = StatusNabavka
    Left = 136
    Top = 136
  end
  object MaxIDNabavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXNABAVKAID')
    Left = 944
    Top = 368
  end
  object tblPoNabavkaStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_NABAVKA_STAVKI'
      'SET '
      '    ARTVID = :ARTVID,'
      '    ARTSIF = :ARTSIF,'
      '    KOLICINA = :KOLICINA,'
      '    RE = :RE,'
      '    VREME = :VREME,'
      '    KREIRAL = :KREIRAL,'
      '    CENA = :CENA,'
      '    DDV = :DDV,'
      '    CENADDV = :CENADDV,'
      '    OPIS = :OPIS'
      'WHERE'
      '    NABAVKA_ID = :OLD_NABAVKA_ID'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_NABAVKA_STAVKI'
      'WHERE'
      '        NABAVKA_ID = :OLD_NABAVKA_ID'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_NABAVKA_STAVKI('
      '    NABAVKA_ID,'
      '    ID,'
      '    ARTVID,'
      '    ARTSIF,'
      '    KOLICINA,'
      '    RE,'
      '    VREME,'
      '    KREIRAL,'
      '    CENA,'
      '    DDV,'
      '    CENADDV,'
      '    OPIS'
      ')'
      'VALUES('
      '    :NABAVKA_ID,'
      '    :ID,'
      '    :ARTVID,'
      '    :ARTSIF,'
      '    :KOLICINA,'
      '    :RE,'
      '    :VREME,'
      '    :KREIRAL,'
      '    :CENA,'
      '    :DDV,'
      '    :CENADDV,'
      '    :OPIS'
      ')')
    RefreshSQL.Strings = (
      'select ps.nabavka_id,'
      '       ps.id,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.re,'
      '       ps.vreme,'
      '       ps.kreiral,'
      '       cast(ps.cena as numeric(15,2)) as cena,'
      '       ps.DDV,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       ps.opis,'
      '       pn.broj as brojnabavka,'
      '       mr.naziv as nazivre,'
      '       ma.naziv as nazivartikal,'
      '       mtra.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ps.artvid ||'#39'. '#39'|| mtra.opis as artvidIDNaziv,'
      '       mg.id || '#39'. '#39'|| mg.naziv as grupaIdNaziv,'
      '       ps.re || '#39'. '#39'|| mr.naziv as RabEdinIDNaziv,'
      
        '       cast((ps.cenaddv * ps.kolicina) as numeric(15, 2)) as iiz' +
        'nos,'
      '       mr.poteklo,'
      
        '       pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNaNabavk' +
        'a,'
      '       mm.naziv as merkaNaziv,'
      '       ma.merka'
      'from po_nabavka_stavki ps'
      ''
      'inner join po_nabavka pn on pn.id = ps.nabavka_id'
      'inner join mat_re mr on mr.id= ps.re'
      
        'inner join mtr_artikal ma on ma.artvid=ps.artvid and ma.id=ps.ar' +
        'tsif'
      
        'inner join mtr_artvid mtra on mtra.id=ma.artvid and ma.artvid=ps' +
        '.artvid and ma.id=ps.artsif'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.artvi' +
        'd=ps.artvid and ma.id=ps.artsif'
      
        'inner join mat_merka mm on ma.merka = mm.id and  ma.artvid=ps.ar' +
        'tvid and ma.id=ps.artsif'
      ''
      'where(  ps.nabavka_id  =:NabavkaId'
      '     ) and (     PS.NABAVKA_ID = :OLD_NABAVKA_ID'
      '    and PS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ps.nabavka_id,'
      '       ps.id,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.re,'
      '       ps.vreme,'
      '       ps.kreiral,'
      '       cast(ps.cena as numeric(15,2)) as cena,'
      '       ps.DDV,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       ps.opis,'
      '       pn.broj as brojnabavka,'
      '       mr.naziv as nazivre,'
      '       ma.naziv as nazivartikal,'
      '       mtra.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ps.artvid ||'#39'. '#39'|| mtra.opis as artvidIDNaziv,'
      '       mg.id || '#39'. '#39'|| mg.naziv as grupaIdNaziv,'
      '       ps.re || '#39'. '#39'|| mr.naziv as RabEdinIDNaziv,'
      
        '       cast((ps.cenaddv * ps.kolicina) as numeric(15, 2)) as iiz' +
        'nos,'
      '       mr.poteklo,'
      
        '       pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNaNabavk' +
        'a,'
      '       mm.naziv as merkaNaziv,'
      '       ma.merka'
      'from po_nabavka_stavki ps'
      ''
      'inner join po_nabavka pn on pn.id = ps.nabavka_id'
      'inner join mat_re mr on mr.id= ps.re'
      
        'inner join mtr_artikal ma on ma.artvid=ps.artvid and ma.id=ps.ar' +
        'tsif'
      
        'inner join mtr_artvid mtra on mtra.id=ma.artvid and ma.artvid=ps' +
        '.artvid and ma.id=ps.artsif'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.artvi' +
        'd=ps.artvid and ma.id=ps.artsif'
      
        'inner join mat_merka mm on ma.merka = mm.id and  ma.artvid=ps.ar' +
        'tvid and ma.id=ps.artsif'
      ''
      'where ps.nabavka_id  =:NabavkaId'
      'order by ps.id desc')
    BeforePost = tblPoNabavkaStavkiBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 184
    Top = 440
    object tblPoNabavkaStavkiNABAVKA_ID: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' (ID)'
      FieldName = 'NABAVKA_ID'
    end
    object tblPoNabavkaStavkiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPoNabavkaStavkiARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblPoNabavkaStavkiARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPoNabavkaStavkiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072' (ID)'
      FieldName = 'RE'
    end
    object tblPoNabavkaStavkiVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblPoNabavkaStavkiKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblPoNabavkaStavkiCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoNabavkaStavkiBROJNABAVKA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJNABAVKA'
    end
    object tblPoNabavkaStavkiNAZIVRE: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'NAZIVRE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiARTVIDIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiGRUPAIDNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPAIDNAZIV'
      Size = 1076
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiRABEDINIDNAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RABEDINIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiIIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoNabavkaStavkiPOTEKLO: TFIBStringField
      DisplayLabel = #1055#1086#1090#1077#1082#1083#1086
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiBROJNANABAVKA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJNANABAVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNabavkaStavkiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoNabavkaStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object tblPoNabavkaStavkiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPoNabavkaStavki: TDataSource
    DataSet = tblPoNabavkaStavki
    Left = 192
    Top = 384
  end
  object MaxBrojNabavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXNABAVKABROJ(?GODINA, ?RE)')
    Left = 848
    Top = 312
  end
  object PartnerVraboten: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    p.adresa,'
      '    p.tel,'
      '    p.fax,'
      '    p.danocen,'
      '    p.mesto,'
      '    p.ime,'
      '    p.prezime,'
      '    p.tatko,'
      '    p.re,'
      '    tp.naziv tippartner_naziv,'
      '    m.naziv mestonaziv,'
      
        '    cast(case when m.id is null then p.naziv else (p.naziv || '#39' ' +
        '- '#39' || m.naziv) end as varchar(150)) nazivsomesto'
      'from mat_partner p'
      'left outer join mat_mesto m on p.mesto = m.id'
      'inner join mat_tip_partner tp on p.tip_partner = tp.id'
      ''
      'where(  p.tip_partner = :vraboten'
      '     ) and (     P.TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and P.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    p.adresa,'
      '    p.tel,'
      '    p.fax,'
      '    p.danocen,'
      '    p.mesto,'
      '    p.ime,'
      '    p.prezime,'
      '    p.tatko,'
      '    p.re,'
      '    tp.naziv tippartner_naziv,'
      '    m.naziv mestonaziv,'
      
        '    cast(case when m.id is null then p.naziv else (p.naziv || '#39' ' +
        '- '#39' || m.naziv) end as varchar(150)) nazivsomesto'
      'from mat_partner p'
      'left outer join mat_mesto m on p.mesto = m.id'
      'inner join mat_tip_partner tp on p.tip_partner = tp.id'
      ''
      'where p.tip_partner = :vraboten')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 272
    Top = 192
    object PartnerVrabotenTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object PartnerVrabotenID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PartnerVrabotenNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenTEL: TFIBStringField
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenFAX: TFIBStringField
      FieldName = 'FAX'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenDANOCEN: TFIBStringField
      FieldName = 'DANOCEN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object PartnerVrabotenIME: TFIBStringField
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenTATKO: TFIBStringField
      FieldName = 'TATKO'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object PartnerVrabotenTIPPARTNER_NAZIV: TFIBStringField
      FieldName = 'TIPPARTNER_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenMESTONAZIV: TFIBStringField
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PartnerVrabotenNAZIVSOMESTO: TFIBStringField
      FieldName = 'NAZIVSOMESTO'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPartner: TDataSource
    DataSet = PartnerVraboten
    Left = 272
    Top = 136
  end
  object tblPoPlanStavki: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '          d.artvid,'
      '          d.artsif,'
      '          sum(d.kolicina) kolicina,'
      '          (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=d.artvid and pp.artsif = d.artsif'
      '            order by pp.vreme desc)as cena ,'
      '          d.ddv,'
      '          d.naziv,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      ''
      'from  VIEW_PO__PLANSTAVKI d'
      
        'where d.godina like :godina and d.status = :odobren and( d.re in' +
        ' (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or d.re in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      ''
      'group by d.artvid,'
      '         d.artsif ,'
      '         d.naziv,'
      '         d.ddv,    '
      '         d.merkaid,'
      '         d.merkanaziv,'
      '         d.artvidnaziv,'
      '         d.podgrupaid,'
      '         d.podgrupanaziv')
    SelectSQL.Strings = (
      'select'
      '          d.artvid,'
      '          d.artsif,'
      '          sum(d.kolicina) kolicina,'
      '          (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=d.artvid and pp.artsif = d.artsif'
      '            order by pp.vreme desc)as cena ,'
      '          d.ddv,'
      '          d.naziv,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      ''
      'from  VIEW_PO__PLANSTAVKI d'
      
        'where d.godina like :godina and d.status = :odobren and( d.re in' +
        ' (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or d.re in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      ''
      'group by d.artvid,'
      '         d.artsif ,'
      '         d.naziv,'
      '         d.ddv,    '
      '         d.merkaid,'
      '         d.merkanaziv,'
      '         d.artvidnaziv,'
      '         d.podgrupaid,'
      '         d.podgrupanaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 352
    Top = 440
    object tblPoPlanStavkiARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblPoPlanStavkiARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPoPlanStavkiDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1074
      FieldName = 'DDV'
    end
    object tblPoPlanStavkiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiMERKAID: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKAID'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiPODGRUPAID: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'PODGRUPAID'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiPODGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'PODGRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPlanStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object tblPoPlanStavkiCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsPoPlanStavki: TDataSource
    DataSet = tblPoPlanStavki
    Left = 344
    Top = 384
  end
  object MaxNabavkaStavkaID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXNABAVKASTAVKAID(?NABAVKAID)')
    Left = 944
    Top = 496
  end
  object ViewListaArtikli: TpFIBDataSet
    RefreshSQL.Strings = (
      'select  va.artvid,'
      '        va.id,'
      '        va.naziv,'
      '        va.merka,'
      '        va.cena,'
      '        va.brpak,'
      '        va.katalog,'
      '        va.barkod,'
      '        va.prozvoditel,'
      '        va.grupa,'
      '        va.klasifikacija,'
      '        va.kolicina,'
      '        va.uninaziv,'
      '        va.unifont,'
      '        va.oblik,'
      '        va.jacina,'
      '        va.tarifa,'
      '        va.artvidnaziv,'
      '        va.grupanaziv,'
      '        va.merkanaziv,'
      '        va.proizvoditelnaziv'
      'from view_po_listaartikli va')
    SelectSQL.Strings = (
      'select  va.artvid,'
      '        va.id,'
      '        va.naziv,'
      '        va.merka,'
      '        va.cena,'
      '        va.brpak,'
      '        va.katalog,'
      '        va.barkod,'
      '        va.prozvoditel,'
      '        va.grupa,'
      '        va.klasifikacija,'
      '        va.kolicina,'
      '        va.uninaziv,'
      '        va.unifont,'
      '        va.oblik,'
      '        va.jacina,'
      '        va.tarifa,'
      '        va.artvidnaziv,'
      '        va.grupanaziv,'
      '        va.merkanaziv,'
      '        va.proizvoditelnaziv'
      'from view_po_listaartikli va')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 296
    Top = 576
    object ViewListaArtikliARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object ViewListaArtikliID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object ViewListaArtikliNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object ViewListaArtikliBRPAK: TFIBIntegerField
      DisplayLabel = #1041#1088'. '#1087#1072#1082
      FieldName = 'BRPAK'
    end
    object ViewListaArtikliKATALOG: TFIBStringField
      DisplayLabel = #1050#1072#1090#1072#1083#1086#1075
      FieldName = 'KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliBARKOD: TFIBStringField
      DisplayLabel = #1041#1072#1088#1082#1086#1076
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' (ID)'
      FieldName = 'PROZVODITEL'
    end
    object ViewListaArtikliGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliKLASIFIKACIJA: TFIBStringField
      DisplayLabel = #1050#1083#1072#1089#1080#1092#1080#1082#1072#1094#1080#1112#1072
      FieldName = 'KLASIFIKACIJA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object ViewListaArtikliUNINAZIV: TFIBStringField
      FieldName = 'UNINAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliUNIFONT: TFIBStringField
      FieldName = 'UNIFONT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliOBLIK: TFIBIntegerField
      FieldName = 'OBLIK'
    end
    object ViewListaArtikliJACINA: TFIBFloatField
      FieldName = 'JACINA'
    end
    object ViewListaArtikliTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
    object ViewListaArtikliARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ViewListaArtikliPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsViewListaArtikli: TDataSource
    DataSet = ViewListaArtikli
    Left = 296
    Top = 512
  end
  object tblPonudi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_PONUDA'
      'SET '
      '    NABAVKA_ID = :NABAVKA_ID,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    DATUM = :DATUM,'
      '    DATUM_VAZENJE = :DATUM_VAZENJE,'
      '    KONTAKT = :KONTAKT,'
      '    ZABELESKA = :ZABELESKA,'
      '    USLOVI = :USLOVI,'
      '    STATUS = :STATUS,'
      '    NACIN_PLAKJANJE = :NACIN_PLAKJANJE,'
      '    KREIRAL = :KREIRAL,'
      '    VREME = :VREME,'
      '    PREDMET = :PREDMETPONUDA,'
      '    RE = :RE'
      'WHERE'
      '    GODINA = :OLD_GODINA'
      '    and BROJ = :OLD_BROJ'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_PONUDA'
      'WHERE'
      '        GODINA = :OLD_GODINA'
      '    and BROJ = :OLD_BROJ'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_PONUDA('
      '    GODINA,'
      '    BROJ,'
      '    NABAVKA_ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    DATUM,'
      '    DATUM_VAZENJE,'
      '    KONTAKT,'
      '    ZABELESKA,'
      '    USLOVI,'
      '    STATUS,'
      '    NACIN_PLAKJANJE,'
      '    KREIRAL,'
      '    VREME,'
      '    PREDMET,'
      '    RE'
      ')'
      'VALUES('
      '    :GODINA,'
      '    :BROJ,'
      '    :NABAVKA_ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :DATUM,'
      '    :DATUM_VAZENJE,'
      '    :KONTAKT,'
      '    :ZABELESKA,'
      '    :USLOVI,'
      '    :STATUS,'
      '    :NACIN_PLAKJANJE,'
      '    :KREIRAL,'
      '    :VREME,'
      '    :PREDMETPONUDA,'
      '    :RE'
      ')')
    RefreshSQL.Strings = (
      'select '
      ' pp.godina,'
      ' pp.broj,'
      ' pp.nabavka_id,'
      ' pp.tip_partner,'
      ' pp.partner,'
      ' pp.datum,'
      ' pp.datum_vazenje,'
      ' pp.kontakt,'
      ' pp.zabeleska,'
      ' pp.uslovi,'
      ' pp.status,'
      ' pp.nacin_plakjanje,'
      ' pp.kreiral,'
      ' pp.vreme,'
      ' pn.broj as NBroj,'
      ' pn.re  as NRe,'
      ' pn.godina as NGodina,'
      ' mp.naziv PNaziv,'
      ' mk.naziv KNaziv,'
      ' ms.naziv SNaziv,'
      ' sp.naziv PlakanjeNaziv,'
      ' pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNabavka,'
      ' pp.broj||'#39'/'#39'||pp.godina as PonudaBroj,'
      ' pn.predmet as PredmetNabavka,'
      ' pp.predmet as PredmetPonuda,'
      ' pp.re,'
      ' pn.broj_dogovor'
      ' '
      'from po_ponuda pp'
      'left outer join po_nabavka pn on pp.nabavka_id = pn.id'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id= pp.partner'
      'left outer join mat_kontakt mk on mk.id=pp.kontakt'
      'inner join mat_status ms on ms.id=pp.status'
      
        'left outer join sys_tip_placanje sp on sp.id = pp.nacin_plakjanj' +
        'e'
      'inner join sys_user su on su.username=pp.kreiral'
      ''
      
        'where(  pp.godina = :godina and COALESCE(pp.nabavka_id,0) like :' +
        'nabavkaID and pp.status like :status and pp.re = :re'
      '     ) and (     PP.GODINA = :OLD_GODINA'
      '    and PP.BROJ = :OLD_BROJ'
      '     )'
      '    '
      ''
      'order by PonudaBroj')
    SelectSQL.Strings = (
      'select '
      ' pp.godina,'
      ' pp.broj,'
      ' pp.nabavka_id,'
      ' pp.tip_partner,'
      ' pp.partner,'
      ' pp.datum,'
      ' pp.datum_vazenje,'
      ' pp.kontakt,'
      ' pp.zabeleska,'
      ' pp.uslovi,'
      ' pp.status,'
      ' pp.nacin_plakjanje,'
      ' pp.kreiral,'
      ' pp.vreme,'
      ' pn.broj as NBroj,'
      ' pn.re  as NRe,'
      ' pn.godina as NGodina,'
      ' mp.naziv PNaziv,'
      ' mk.naziv KNaziv,'
      ' ms.naziv SNaziv,'
      ' sp.naziv PlakanjeNaziv,'
      ' pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNabavka,'
      ' pp.broj||'#39'/'#39'||pp.godina as PonudaBroj,'
      ' pn.predmet as PredmetNabavka,'
      ' pp.predmet as PredmetPonuda,'
      ' pp.re,'
      ' pn.broj_dogovor'
      ' '
      'from po_ponuda pp'
      'left outer join po_nabavka pn on pp.nabavka_id = pn.id'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id= pp.partner'
      'left outer join mat_kontakt mk on mk.id=pp.kontakt'
      'inner join mat_status ms on ms.id=pp.status'
      
        'left outer join sys_tip_placanje sp on sp.id = pp.nacin_plakjanj' +
        'e'
      'inner join sys_user su on su.username=pp.kreiral'
      ''
      
        'where pp.godina = :godina and COALESCE(pp.nabavka_id,0) like :na' +
        'bavkaID and pp.status like :status and pp.re = :re'
      ''
      'order by PonudaBroj')
    AfterInsert = tblPonudiAfterInsert
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 320
    object tblPonudiNABAVKA_ID: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' (ID)'
      FieldName = 'NABAVKA_ID'
    end
    object tblPonudiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object tblPonudiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' (ID)'
      FieldName = 'PARTNER'
    end
    object tblPonudiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
    end
    object tblPonudiDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
      FieldName = 'DATUM_VAZENJE'
    end
    object tblPonudiKONTAKT: TFIBIntegerField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090
      FieldName = 'KONTAKT'
    end
    object tblPonudiZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiUSLOVI: TFIBStringField
      DisplayLabel = #1059#1089#1083#1086#1074#1080
      FieldName = 'USLOVI'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblPonudiNACIN_PLAKJANJE: TFIBIntegerField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'NACIN_PLAKJANJE'
    end
    object tblPonudiKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblPonudiNBROJ: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' ('#1073#1088#1086#1112')'
      FieldName = 'NBROJ'
    end
    object tblPonudiNRE: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' ('#1088#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072')'
      FieldName = 'NRE'
    end
    object tblPonudiNGODINA: TFIBSmallIntField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' ('#1075#1086#1076#1080#1085#1072')'
      FieldName = 'NGODINA'
    end
    object tblPonudiPNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiKNAZIV: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' '#1085#1072#1079#1080#1074
      FieldName = 'KNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072#1079#1080#1074
      FieldName = 'SNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiPLAKANJENAZIV: TFIBStringField
      DisplayLabel = #1055#1083#1072#1116#1072#1114#1077' '#1085#1072#1079#1080#1074
      FieldName = 'PLAKANJENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiBROJNABAVKA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJNABAVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPonudiBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblPonudiPONUDABROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'PONUDABROJ'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiPREDMETNABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMETNABAVKA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiPREDMETPONUDA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'PREDMETPONUDA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblPonudiBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudi: TDataSource
    DataSet = tblPonudi
    Left = 24
    Top = 264
  end
  object MaxPonudaID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXPONUDAID(?GODINA)')
    Left = 832
    Top = 432
  end
  object Plakanje: TpFIBDataSet
    SelectSQL.Strings = (
      'select sp.id, sp.naziv'
      'from sys_tip_placanje sp')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 344
    Top = 192
    object PlakanjeID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PlakanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPlakanje: TDataSource
    DataSet = Plakanje
    Left = 344
    Top = 136
  end
  object StatusPonuda: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where(  msg.naziv = '#39'PO_PONUDA'#39
      '     ) and (     MS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where msg.naziv = '#39'PO_PONUDA'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 408
    Top = 192
    object StatusPonudaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object StatusPonudaGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object StatusPonudaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStatusPonuda: TDataSource
    DataSet = StatusPonuda
    Left = 408
    Top = 136
  end
  object BrStavkiNabavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_BRSTAVKINABAVKA(?RE,?ID,?ARTSIF,?ARTVI' +
        'D)')
    Left = 1144
    Top = 432
  end
  object RabEdinica: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV,'
      '    MAT_RE.TIP_PARTNER,'
      '    MAT_RE.PARTNER,'
      '    MAT_RE.KOREN,'
      '    MAT_RE.SPISOK,'
      '    MAT_RE.RE,'
      '    MAT_RE.POTEKLO,'
      '    MAT_RE.RAKOVODITEL,'
      '    MAT_RE.R,'
      '    MAT_RE.M,'
      '    MAT_RE.T'
      'from mat_re'
      'where  MAT_RE.SPISOK like :spisok'
      'union'
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV,'
      '    MAT_RE.TIP_PARTNER,'
      '    MAT_RE.PARTNER,'
      '    MAT_RE.KOREN,'
      '    MAT_RE.SPISOK,'
      '    MAT_RE.RE,'
      '    MAT_RE.POTEKLO,'
      '    MAT_RE.RAKOVODITEL,'
      '    MAT_RE.R,'
      '    MAT_RE.M,'
      '    MAT_RE.T'
      'from mat_re'
      'where  MAT_RE.POTEKLO like :poteklo or mat_re.id like :poteklo')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 280
    Top = 440
    object RabEdinicaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object RabEdinicaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object RabEdinicaTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object RabEdinicaPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object RabEdinicaKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object RabEdinicaSPISOK: TFIBStringField
      DisplayLabel = #1057#1087#1080#1089#1086#1082
      FieldName = 'SPISOK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object RabEdinicaRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object RabEdinicaPOTEKLO: TFIBStringField
      DisplayLabel = #1055#1086#1090#1077#1082#1083#1086
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object RabEdinicaRAKOVODITEL: TFIBStringField
      FieldName = 'RAKOVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object RabEdinicaR: TFIBSmallIntField
      FieldName = 'R'
    end
    object RabEdinicaM: TFIBSmallIntField
      FieldName = 'M'
    end
    object RabEdinicaT: TFIBSmallIntField
      FieldName = 'T'
    end
  end
  object dsRabEdinica: TDataSource
    DataSet = RabEdinica
    Left = 280
    Top = 384
  end
  object PlanStavkiRabEd: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '          d.artvid,'
      '          d.artsif,'
      '          sum(d.kolicina) kolicina,'
      '          (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=d.artvid and pp.artsif = d.artsif'
      '            order by pp.vreme desc) as cena,'
      '          d.ddv,'
      '          d.naziv,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      ''
      'from  VIEW_PO__PLANSTAVKI d'
      
        'where d.godina like :godina and d.re = :RE and d.status = :odobr' +
        'en'
      'group by d.artvid,'
      '         d.artsif ,'
      '         d.naziv,'
      '         d.ddv,    '
      '         d.merkaid,'
      '         d.merkanaziv,'
      '         d.artvidnaziv,'
      '         d.podgrupaid,'
      '         d.podgrupanaziv'
      ''
      '')
    SelectSQL.Strings = (
      'select'
      '          d.artvid,'
      '          d.artsif,'
      '          sum(d.kolicina) kolicina,'
      '          (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=d.artvid and pp.artsif = d.artsif'
      '            order by pp.vreme desc) as cena,'
      '          d.ddv,'
      '          d.naziv,'
      '          d.merkaid,'
      '          d.merkanaziv,'
      '          d.artvidnaziv,'
      '          d.podgrupaid,'
      '          d.podgrupanaziv'
      ''
      'from  VIEW_PO__PLANSTAVKI d'
      
        'where d.godina like :godina and d.re = :RE and d.status = :odobr' +
        'en'
      'group by d.artvid,'
      '         d.artsif ,'
      '         d.naziv,'
      '         d.ddv,    '
      '         d.merkaid,'
      '         d.merkanaziv,'
      '         d.artvidnaziv,'
      '         d.podgrupaid,'
      '         d.podgrupanaziv'
      ''
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 432
    Top = 440
    object PlanStavkiRabEdARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object PlanStavkiRabEdARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object PlanStavkiRabEdDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object PlanStavkiRabEdNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdMERKAID: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKAID'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdPODGRUPAID: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'PODGRUPAID'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdPODGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'PODGRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PlanStavkiRabEdKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object PlanStavkiRabEdCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsPlanStavkiRabEd: TDataSource
    DataSet = PlanStavkiRabEd
    Left = 424
    Top = 384
  end
  object KontaktPartner: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      #9'MAT_KONTAKT.ID,'
      #9'MAT_KONTAKT.TIP_PARTNER,'
      #9'MAT_KONTAKT.PARTNER,'
      #9'MAT_KONTAKT.NAZIV,'
      #9'MAT_KONTAKT.MAIL,'
      #9'MAT_KONTAKT.MOBILEN,'
      #9'MAT_KONTAKT.TEL,'
      #9'MAT_KONTAKT.OPIS'
      'from mat_kontakt'
      
        'where(  MAT_KONTAKT.TIP_PARTNER =:tippartner and MAT_KONTAKT.PAR' +
        'TNER =:partner'
      '     ) and (     MAT_KONTAKT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      #9'MAT_KONTAKT.ID,'
      #9'MAT_KONTAKT.TIP_PARTNER,'
      #9'MAT_KONTAKT.PARTNER,'
      #9'MAT_KONTAKT.NAZIV,'
      #9'MAT_KONTAKT.MAIL,'
      #9'MAT_KONTAKT.MOBILEN,'
      #9'MAT_KONTAKT.TEL,'
      #9'MAT_KONTAKT.OPIS'
      'from mat_kontakt'
      
        'where MAT_KONTAKT.TIP_PARTNER =:tippartner and MAT_KONTAKT.PARTN' +
        'ER =:partner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 88
    Top = 320
    object KontaktPartnerID: TFIBIntegerField
      FieldName = 'ID'
    end
    object KontaktPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object KontaktPartnerPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER'
    end
    object KontaktPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object KontaktPartnerMAIL: TFIBStringField
      DisplayLabel = 'eMail'
      FieldName = 'MAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object KontaktPartnerMOBILEN: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object KontaktPartnerTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083'.'
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object KontaktPartnerOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKontaktPartner: TDataSource
    DataSet = KontaktPartner
    Left = 88
    Top = 264
  end
  object tblPoPonudaStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_PONUDA_STAVKI'
      'SET '
      '    ARTVID = :ARTVID,'
      '    ARTSIF = :ARTSIF,'
      '    KOLICINA = :KOLICINA,'
      '    STATUS = :STATUS,'
      '    KOLICINA_PRIFATENA = :KOLICINA_PRIFATENA,'
      '    OPIS = :OPIS,'
      '    CENA = :CENA,'
      '    DDV = :DDV,'
      '    CENADDV = :CENADDV,'
      '    RABAT = :RABAT,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS,'
      '    KREIRAL = :KREIRAL,'
      '    VREME = :VREME'
      'WHERE'
      '    ID = :OLD_ID'
      '    and BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_PONUDA_STAVKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    and BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_PONUDA_STAVKI('
      '    ID,'
      '    BROJ,'
      '    GODINA,'
      '    ARTVID,'
      '    ARTSIF,'
      '    KOLICINA,'
      '    STATUS,'
      '    KOLICINA_PRIFATENA,'
      '    OPIS,'
      '    CENA,'
      '    DDV,'
      '    CENADDV,'
      '    RABAT,'
      '    VALUTA,'
      '    KURS,'
      '    KREIRAL,'
      '    VREME'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :GODINA,'
      '    :ARTVID,'
      '    :ARTSIF,'
      '    :KOLICINA,'
      '    :STATUS,'
      '    :KOLICINA_PRIFATENA,'
      '    :OPIS,'
      '    :CENA,'
      '    :DDV,'
      '    :CENADDV,'
      '    :RABAT,'
      '    :VALUTA,'
      '    :KURS,'
      '    :KREIRAL,'
      '    :VREME'
      ')')
    RefreshSQL.Strings = (
      'select ps.id,'
      '       ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.status,'
      
        '       cast(ps.kolicina_prifatena as numeric(15,4)) as kolicina_' +
        'prifatena,'
      '       ps.opis,'
      '       cast(ps.cena as numeric(15,2)) as cena,'
      '       ps.ddv,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       cast(ps.rabat as numeric(15,2)) as rabat,'
      '       ps.valuta,'
      '       ps.kurs,'
      '       ps.kreiral,'
      '       ps.vreme,'
      '       ma.naziv as artnaziv,'
      '       ma.merka as merka,'
      '       ma.grupa as artgrupa,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as artgrupanaziv,'
      '       mm.naziv as MerkaNaziv,'
      '       mval.naziv as valutanaziv,'
      '       cast(ps.krajna_cena as numeric(15,2)) as krajna_cena,'
      
        '       cast((ps.krajna_cena* ps.kolicina_prifatena) as numeric(1' +
        '5,2)) as iiznos,'
      '       ms.naziv as statusnaziv'
      'from po_ponuda_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      
        'inner join mtr_artvid mv on mv.id = ma.artvid and ma.id = ps.art' +
        'sif and ma.artvid = ps.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'ps.artsif and ma.artvid = ps.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = ps.artsi' +
        'f and ma.artvid = ps.artvid'
      'inner join mat_valuta mval on mval.id=ps.valuta'
      'inner join mat_status ms on ms.id=ps.status'
      'where(  ps.godina = :godina and ps.broj = :broj'
      '     ) and (     PS.ID = :OLD_ID'
      '    and PS.BROJ = :OLD_BROJ'
      '    and PS.GODINA = :OLD_GODINA'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.status,'
      
        '       cast(ps.kolicina_prifatena as numeric(15,4)) as kolicina_' +
        'prifatena,'
      '       ps.opis,'
      '       cast(ps.cena as numeric(15,2)) as cena,'
      '       ps.ddv,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       cast(ps.rabat as numeric(15,2)) as rabat,'
      '       ps.valuta,'
      '       ps.kurs,'
      '       ps.kreiral,'
      '       ps.vreme,'
      '       ma.naziv as artnaziv,'
      '       ma.merka as merka,'
      '       ma.grupa as artgrupa,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as artgrupanaziv,'
      '       mm.naziv as MerkaNaziv,'
      '       mval.naziv as valutanaziv,'
      '       cast(ps.krajna_cena as numeric(15,2)) as krajna_cena,'
      
        '       cast((ps.krajna_cena* ps.kolicina_prifatena) as numeric(1' +
        '5,2)) as iiznos,'
      '       ms.naziv as statusnaziv'
      'from po_ponuda_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      
        'inner join mtr_artvid mv on mv.id = ma.artvid and ma.id = ps.art' +
        'sif and ma.artvid = ps.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'ps.artsif and ma.artvid = ps.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = ps.artsi' +
        'f and ma.artvid = ps.artvid'
      'inner join mat_valuta mval on mval.id=ps.valuta'
      'inner join mat_status ms on ms.id=ps.status'
      'where ps.godina = :godina and ps.broj = :broj')
    BeforePost = tblPoPonudaStavkiBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 168
    Top = 320
    object tblPoPonudaStavkiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPoPonudaStavkiBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblPoPonudaStavkiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPoPonudaStavkiARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblPoPonudaStavkiARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPoPonudaStavkiSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblPoPonudaStavkiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoPonudaStavkiDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblPoPonudaStavkiCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1076#1076#1074
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoPonudaStavkiVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072' (ID)'
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiKURS: TFIBFloatField
      DisplayLabel = #1050#1091#1088#1089
      FieldName = 'KURS'
    end
    object tblPoPonudaStavkiKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblPoPonudaStavkiARTNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiARTGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTGRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiARTGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'ARTGRUPANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = 'Merka'
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiVALUTANAZIV: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072
      FieldName = 'VALUTANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiSTATUSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoPonudaStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1055#1086#1085#1091#1076#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.0000'
    end
    object tblPoPonudaStavkiKOLICINA_PRIFATENA: TFIBBCDField
      DisplayLabel = #1055#1088#1080#1092#1072#1090#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA_PRIFATENA'
      DisplayFormat = '0.0000'
    end
    object tblPoPonudaStavkiRABAT: TFIBBCDField
      DisplayLabel = #1056#1072#1073#1072#1090
      FieldName = 'RABAT'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPoPonudaStavkiKRAJNA_CENA: TFIBBCDField
      DisplayLabel = #1050#1088#1072#1112#1085#1072' '#1094#1077#1085#1072
      FieldName = 'KRAJNA_CENA'
      Size = 2
    end
    object tblPoPonudaStavkiIIZNOS: TFIBBCDField
      FieldName = 'IIZNOS'
      Size = 2
    end
  end
  object dsPoPonudaStavki: TDataSource
    DataSet = tblPoPonudaStavki
    Left = 160
    Top = 264
  end
  object CountPartner: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_COUNTPARTNER (?TIP_PARTNER,?ID)')
    Left = 720
    Top = 368
  end
  object ponudaStavkaID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_PONUDASTAVKAID(?BROJ, ?GODINA)')
    Left = 1040
    Top = 432
  end
  object CountStavkiPonuda: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_COUNTSTAVKIPONUDA (?BROJ, ?GODINA, ?AR' +
        'TSIF, ?ARTVID)')
    Left = 1040
    Top = 368
  end
  object tblNalog: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_NALOG'
      'SET '
      '    PONUDA_BROJ = :PONUDA_BROJ,'
      '    PONUDA_GODINA = :PONUDA_GODINA,'
      '    NABAVKA_ID = :NABAVKA_ID,'
      '    DATUM = :DATUM,'
      '    DATUM_ISPORAKA = :DATUM_ISPORAKA,'
      '    STATUS = :STATUS,'
      '    KREIRAL = :KREIRAL,'
      '    TIP_KONTAKT = :TIP_KONTAKT,'
      '    KONTAKT = :KONTAKT,'
      '    PRIMAC = :PRIMAC,'
      '    PRIMAC_ADRESA = :PRIMAC_ADRESA,'
      '    PRIMAC_KONTAKT = :PRIMAC_KONTAKT,'
      '    TIP_PLAKJANJE = :TIP_PLAKJANJE,'
      '    VID_PREVOZ = :VID_PREVOZ,'
      '    PREDMET_NABAVKA = :PREDMET_NABAVKA,'
      '    ZABELESKA = :ZABELESKA,'
      '    TEZINA = :TEZINA,'
      '    VREME = :VREME,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    RE = :RE'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_NALOG'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_NALOG('
      '    BROJ,'
      '    GODINA,'
      '    PONUDA_BROJ,'
      '    PONUDA_GODINA,'
      '    NABAVKA_ID,'
      '    DATUM,'
      '    DATUM_ISPORAKA,'
      '    STATUS,'
      '    KREIRAL,'
      '    TIP_KONTAKT,'
      '    KONTAKT,'
      '    PRIMAC,'
      '    PRIMAC_ADRESA,'
      '    PRIMAC_KONTAKT,'
      '    TIP_PLAKJANJE,'
      '    VID_PREVOZ,'
      '    PREDMET_NABAVKA,'
      '    ZABELESKA,'
      '    TEZINA,'
      '    VREME,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    RE'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :GODINA,'
      '    :PONUDA_BROJ,'
      '    :PONUDA_GODINA,'
      '    :NABAVKA_ID,'
      '    :DATUM,'
      '    :DATUM_ISPORAKA,'
      '    :STATUS,'
      '    :KREIRAL,'
      '    :TIP_KONTAKT,'
      '    :KONTAKT,'
      '    :PRIMAC,'
      '    :PRIMAC_ADRESA,'
      '    :PRIMAC_KONTAKT,'
      '    :TIP_PLAKJANJE,'
      '    :VID_PREVOZ,'
      '    :PREDMET_NABAVKA,'
      '    :ZABELESKA,'
      '    :TEZINA,'
      '    :VREME,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :RE'
      ')')
    RefreshSQL.Strings = (
      'select pn.broj,'
      '       pn.godina,'
      '       pn.ponuda_broj,'
      '       pn.ponuda_godina,'
      '       pn.nabavka_id,'
      '       pn.datum,'
      '       pn.datum_isporaka,'
      '       pn.status,'
      '       pn.kreiral,'
      '       pn.tip_kontakt,'
      '       pn.kontakt,'
      '       pn.primac,'
      '       pn.primac_adresa,'
      '       pn.primac_kontakt,'
      '       pn.tip_plakjanje,'
      '       pn.vid_prevoz,'
      '       pn.predmet_nabavka,'
      '       pn.zabeleska,'
      '       pn.tezina,'
      '       pn.vreme,'
      '       pn.ponuda_broj ||'#39'/'#39'|| pn.ponuda_godina as BrojNaPonuda,'
      
        '       pnab.broj || '#39'/'#39'|| pnab.re || '#39'/'#39'|| pnab.godina as BrojNa' +
        'Nabavka,'
      '       pnab.broj_dogovor,'
      '       ms.naziv as StatusNaziv,'
      '       mp.naziv as KontaktNaziv,'
      '       tp.naziv as PlakanjeNaziv,'
      '       mv.naziv as PrevozNaziv,'
      '       pn.broj ||'#39'/'#39'|| pn.godina as BrojNaNalog,'
      '       ptn.naziv as tip_nabavkanaziv,'
      '       pn.tip_partner,'
      '       pn.partner,'
      
        '       (select mp.naziv  from mat_partner mp where mp.tip_partne' +
        'r = pn.tip_partner and pn.partner = mp.id) as partnerNaziv,'
      
        '       (select mp.adresa  from mat_partner mp where mp.tip_partn' +
        'er = pn.tip_partner and pn.partner = mp.id) as partnerAdresa,'
      
        '       (select mm.naziv  from mat_mesto mm where /*$$IBEC$$ mp.t' +
        'ip_partner = pn.tip_partner and pn.partner = mp.id and $$IBEC$$*' +
        '/ mm.id = mp.mesto) as partnerMesto,'
      '       pponuda.datum  datumponuda,'
      '       pn.re'
      ''
      'from po_nalog pn'
      'left outer join po_nabavka pnab on pnab.id = pn.nabavka_id'
      'left outer join po_tip_nabavka ptn on ptn.id = pnab.tip_nabavka'
      'inner join mat_status ms on ms.id = pn.status'
      
        'left outer join mat_partner mp on mp.tip_partner = pn.tip_kontak' +
        't and mp.id = pn.kontakt'
      'left outer join sys_tip_placanje tp on tp.id = pn.tip_plakjanje'
      'left outer join mat_vid_prevoz mv on mv.id = pn.vid_prevoz'
      
        'left outer join po_ponuda pponuda on pponuda.broj = pn.ponuda_br' +
        'oj and pponuda.godina = pn.ponuda_godina'
      ''
      
        'where(  pn.godina = :godina and COALESCE(pn.nabavka_id,0) like :' +
        'nabavkaID and pn.status like :status'
      '      and COALESCE(pn.ponuda_broj,0) like :ponudaBroj '
      '      and COALESCE(pn.ponuda_godina,0) like :ponudaGodina'
      '      and pn.re = :re'
      '     ) and (     PN.BROJ = :OLD_BROJ'
      '    and PN.GODINA = :OLD_GODINA'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select pn.broj,'
      '       pn.godina,'
      '       pn.ponuda_broj,'
      '       pn.ponuda_godina,'
      '       pn.nabavka_id,'
      '       pn.datum,'
      '       pn.datum_isporaka,'
      '       pn.status,'
      '       pn.kreiral,'
      '       pn.tip_kontakt,'
      '       pn.kontakt,'
      '       pn.primac,'
      '       pn.primac_adresa,'
      '       pn.primac_kontakt,'
      '       pn.tip_plakjanje,'
      '       pn.vid_prevoz,'
      '       pn.predmet_nabavka,'
      '       pn.zabeleska,'
      '       pn.tezina,'
      '       pn.vreme,'
      '       pn.ponuda_broj ||'#39'/'#39'|| pn.ponuda_godina as BrojNaPonuda,'
      
        '       pnab.broj || '#39'/'#39'|| pnab.re || '#39'/'#39'|| pnab.godina as BrojNa' +
        'Nabavka,'
      '       pnab.broj_dogovor,'
      '       ms.naziv as StatusNaziv,'
      '       mp.naziv as KontaktNaziv,'
      '       tp.naziv as PlakanjeNaziv,'
      '       mv.naziv as PrevozNaziv,'
      '       pn.broj ||'#39'/'#39'|| pn.godina as BrojNaNalog,'
      '       ptn.naziv as tip_nabavkanaziv,'
      '       pn.tip_partner,'
      '       pn.partner,'
      
        '       (select mp.naziv  from mat_partner mp where mp.tip_partne' +
        'r = pn.tip_partner and pn.partner = mp.id) as partnerNaziv,'
      
        '       (select mp.adresa  from mat_partner mp where mp.tip_partn' +
        'er = pn.tip_partner and pn.partner = mp.id) as partnerAdresa,'
      
        '       (select mm.naziv  from mat_mesto mm where /*$$IBEC$$ mp.t' +
        'ip_partner = pn.tip_partner and pn.partner = mp.id and $$IBEC$$*' +
        '/ mm.id = mp.mesto) as partnerMesto,'
      '       pponuda.datum  datumponuda,'
      '       pn.re'
      ''
      'from po_nalog pn'
      'left outer join po_nabavka pnab on pnab.id = pn.nabavka_id'
      'left outer join po_tip_nabavka ptn on ptn.id = pnab.tip_nabavka'
      'inner join mat_status ms on ms.id = pn.status'
      
        'left outer join mat_partner mp on mp.tip_partner = pn.tip_kontak' +
        't and mp.id = pn.kontakt'
      'left outer join sys_tip_placanje tp on tp.id = pn.tip_plakjanje'
      'left outer join mat_vid_prevoz mv on mv.id = pn.vid_prevoz'
      
        'left outer join po_ponuda pponuda on pponuda.broj = pn.ponuda_br' +
        'oj and pponuda.godina = pn.ponuda_godina'
      ''
      
        'where pn.godina = :godina and COALESCE(pn.nabavka_id,0) like :na' +
        'bavkaID and pn.status like :status'
      '      and COALESCE(pn.ponuda_broj,0) like :ponudaBroj '
      '      and COALESCE(pn.ponuda_godina,0) like :ponudaGodina'
      '      and pn.re = :re'
      '')
    AfterInsert = tblNalogAfterInsert
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 248
    Top = 320
    object tblNalogBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblNalogGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblNalogPONUDA_BROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'PONUDA_BROJ'
    end
    object tblNalogPONUDA_GODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'PONUDA_GODINA'
    end
    object tblNalogNABAVKA_ID: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072' (ID)'
      FieldName = 'NABAVKA_ID'
    end
    object tblNalogDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
    end
    object tblNalogDATUM_ISPORAKA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      FieldName = 'DATUM_ISPORAKA'
    end
    object tblNalogSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblNalogKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTIP_KONTAKT: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TIP_KONTAKT'
    end
    object tblNalogKONTAKT: TFIBIntegerField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090
      FieldName = 'KONTAKT'
    end
    object tblNalogPRIMAC: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1072#1095
      FieldName = 'PRIMAC'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPRIMAC_ADRESA: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1072#1095' - '#1040#1076#1088#1077#1089#1072
      FieldName = 'PRIMAC_ADRESA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPRIMAC_KONTAKT: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1087#1088#1080#1077#1084
      FieldName = 'PRIMAC_KONTAKT'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTIP_PLAKJANJE: TFIBSmallIntField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' - (ID)'
      FieldName = 'TIP_PLAKJANJE'
    end
    object tblNalogVID_PREVOZ: TFIBSmallIntField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1088#1077#1074#1086#1079' (ID)'
      FieldName = 'VID_PREVOZ'
    end
    object tblNalogPREDMET_NABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET_NABAVKA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTEZINA: TFIBStringField
      DisplayLabel = #1058#1077#1078#1080#1085#1072
      FieldName = 'TEZINA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblNalogBROJNAPONUDA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'BROJNAPONUDA'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogBROJNANABAVKA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJNANABAVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogSTATUSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogKONTAKTNAZIV: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' - '#1053#1072#1079#1080#1074
      FieldName = 'KONTAKTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPLAKANJENAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'PLAKANJENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPREVOZNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1077#1074#1086#1079
      FieldName = 'PREVOZNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogBROJNANALOG: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1083#1086#1075
      FieldName = 'BROJNANALOG'
      Size = 18
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTIP_NABAVKANAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'TIP_NABAVKANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object tblNalogPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - (ID)'
      FieldName = 'PARTNER'
    end
    object tblNalogPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1053#1072#1079#1080#1074
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPARTNERADRESA: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1072#1076#1088#1077#1089#1072
      FieldName = 'PARTNERADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPARTNERMESTO: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' - '#1084#1077#1089#1090#1086
      FieldName = 'PARTNERMESTO'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogDATUMPONUDA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'DATUMPONUDA'
    end
    object tblNalogRE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RE'
    end
    object tblNalogBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'BROJ_DOGOVOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNalog: TDataSource
    DataSet = tblNalog
    Left = 248
    Top = 264
  end
  object PoPonudaStavkIStatus: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where(  msg.naziv = '#39'PO_PONUDA_STAVKI'#39
      '     ) and (     MS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where msg.naziv = '#39'PO_PONUDA_STAVKI'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 504
    Top = 192
    object PoPonudaStavkIStatusID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PoPonudaStavkIStatusGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object PoPonudaStavkIStatusNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPoPonudaStavkiStatus: TDataSource
    DataSet = PoPonudaStavkIStatus
    Left = 504
    Top = 136
  end
  object NalogStatus: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where(  msg.naziv = '#39'PO_NALOG'#39
      '     ) and (     MS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ms.id, ms.grupa, ms.naziv'
      'from mat_status ms'
      'inner join mat_status_grupa msg on msg.id = ms.grupa'
      'where msg.naziv = '#39'PO_NALOG'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 592
    Top = 192
    object NalogStatusID: TFIBIntegerField
      FieldName = 'ID'
    end
    object NalogStatusGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object NalogStatusNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNalogStatus: TDataSource
    DataSet = NalogStatus
    Left = 600
    Top = 136
  end
  object Prevoz: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAT_VID_PREVOZ'
      'SET '
      '    NAZIV = :NAZIV'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_VID_PREVOZ'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAT_VID_PREVOZ('
      '    ID,'
      '    NAZIV'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV'
      ')')
    RefreshSQL.Strings = (
      'select mv.id, mv.naziv'
      'from mat_vid_prevoz mv'
      ''
      ' WHERE '
      '        MV.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select mv.id, mv.naziv'
      'from mat_vid_prevoz mv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 648
    Top = 192
    object PrevozID: TFIBSmallIntField
      FieldName = 'ID'
    end
    object PrevozNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPrevoz: TDataSource
    DataSet = Prevoz
    Left = 656
    Top = 136
  end
  object MaxNalogID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_MAXNALOGID(?GODINA)')
    Left = 1136
    Top = 368
  end
  object tblPoNalogStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_NALOG_STAVKI'
      'SET '
      '    ARTVID = :ARTVID,'
      '    ARTSIF = :ARTSIF,'
      '    DDV = :DDV,'
      '    OPIS = :OPIS,'
      '    KREIRAL = :KREIRAL,'
      '    VREME = :VREME,'
      '    kolicina = :kolicina,'
      '    cena = :cena,'
      '    cenaddv = :cenaddv'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_NALOG_STAVKI'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_NALOG_STAVKI('
      '    BROJ,'
      '    GODINA,'
      '    ID,'
      '    ARTVID,'
      '    ARTSIF,'
      '    DDV,'
      '    OPIS,'
      '    KREIRAL,'
      '    VREME,'
      '    kolicina,'
      '    cena,'
      '    cenaddv'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :GODINA,'
      '    :ID,'
      '    :ARTVID,'
      '    :ARTSIF,'
      '    :DDV,'
      '    :OPIS,'
      '    :KREIRAL,'
      '    :VREME,'
      '    :kolicina,'
      '    :cena,'
      '    :cenaddv'
      ')')
    RefreshSQL.Strings = (
      'select pn.broj,'
      '       pn.godina,'
      '       pn.id,'
      '       pn.artvid,'
      '       pn.artsif,'
      '       cast(pn.kolicina as numeric(15, 4)) as kolicina,'
      '       cast(pn.cena as numeric(15, 2)) as cena,'
      '       pn.ddv,'
      '       cast(pn.cenaddv as numeric(15, 2)) as cenaddv,'
      '       pn.opis,'
      '       pn.kreiral,'
      '       pn.vreme,'
      '       ma.naziv as NazivArtikal,'
      '       mv.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ma.grupa,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      
        '       cast((pn.cenaddv* pn.kolicina) as numeric(15, 2)) as iizn' +
        'os'
      'from po_nalog_stavki pn'
      
        'inner join mtr_artikal ma on ma.id = pn.artsif and ma.artvid = p' +
        'n.artvid'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and ma.id = pn.art' +
        'sif and ma.artvid = pn.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'pn.artsif and ma.artvid = pn.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = pn.artsi' +
        'f and ma.artvid = pn.artvid'
      'where(  pn.broj = :broj and pn.godina =:godina'
      '     ) and (     PN.BROJ = :OLD_BROJ'
      '    and PN.GODINA = :OLD_GODINA'
      '    and PN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pn.broj,'
      '       pn.godina,'
      '       pn.id,'
      '       pn.artvid,'
      '       pn.artsif,'
      '       cast(pn.kolicina as numeric(15, 4)) as kolicina,'
      '       cast(pn.cena as numeric(15, 2)) as cena,'
      '       pn.ddv,'
      '       cast(pn.cenaddv as numeric(15, 2)) as cenaddv,'
      '       pn.opis,'
      '       pn.kreiral,'
      '       pn.vreme,'
      '       ma.naziv as NazivArtikal,'
      '       mv.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ma.grupa,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      
        '       cast((pn.cenaddv* pn.kolicina) as numeric(15, 2)) as iizn' +
        'os'
      'from po_nalog_stavki pn'
      
        'inner join mtr_artikal ma on ma.id = pn.artsif and ma.artvid = p' +
        'n.artvid'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and ma.id = pn.art' +
        'sif and ma.artvid = pn.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'pn.artsif and ma.artvid = pn.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = pn.artsi' +
        'f and ma.artvid = pn.artvid'
      'where pn.broj = :broj and pn.godina =:godina')
    BeforePost = tblPoNalogStavkiBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 312
    Top = 320
    object tblPoNalogStavkiBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblPoNalogStavkiGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPoNalogStavkiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPoNalogStavkiARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblPoNalogStavkiARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPoNalogStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.0000'
    end
    object tblPoNalogStavkiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblPoNalogStavkiDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblPoNalogStavkiCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object tblPoNalogStavkiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiKREIRAL: TFIBStringField
      DisplayLabel = #1050#1088#1077#1080#1088#1072#1083
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiVREME: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077
      FieldName = 'VREME'
    end
    object tblPoNalogStavkiNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072'(ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoNalogStavkiIIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsPoNalogStavki: TDataSource
    DataSet = tblPoNalogStavki
    Left = 312
    Top = 264
  end
  object CountStavkiNalog: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_COUNTSTAVKINALOG (?BROJ, ?GODINA, ?ART' +
        'SIF, ?ARTVID)')
    Left = 952
    Top = 312
  end
  object NalogStavkiID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_NALOGSTAVKAID(?BROJ, ?GODINA)')
    Left = 1048
    Top = 312
  end
  object IzborStavkiOdNabavka: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.artvid,'
      '       ps.nabavka_id,'
      '       ps.artsif,'
      '       sum(ps.kolicina) sumaKolicina,'
      '       sum(ps.cena) as sumaCena,'
      '       ps.DDV,'
      '       sum(ps.cenaddv)as SumaCenaDDv,'
      '       ma.naziv as nazivartikal,'
      '       mtra.opis as ArtvidNaziv,'
      '       ma.grupa || '#39'. '#39'|| mg.naziv as grupaIdNaziv ,'
      '       mm.naziv as merkaNaziv,'
      '       ma.merka'
      'from po_nabavka_stavki ps'
      ''
      'inner join po_nabavka pn on pn.id = ps.nabavka_id'
      
        'inner join mtr_artikal ma on ma.artvid=ps.artvid and ma.id=ps.ar' +
        'tsif'
      
        'inner join mtr_artvid mtra on mtra.id=ma.artvid and ma.artvid=ps' +
        '.artvid and ma.id=ps.artsif'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.artvi' +
        'd=ps.artvid and ma.id=ps.artsif'
      
        'inner join mat_merka mm on ma.merka = mm.id and  ma.artvid=ps.ar' +
        'tvid and ma.id=ps.artsif'
      'where ps.nabavka_id like :NabavkaId'
      
        'group by ps.artsif, ps.artvid, mtra.opis, ma.grupa,  mg.naziv, p' +
        's.ddv, ma.naziv, ps.nabavka_id , ma.merka, mm.naziv'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 416
    Top = 320
    object IzborStavkiOdNabavkaARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072'(ID)'
      FieldName = 'ARTVID'
    end
    object IzborStavkiOdNabavkaARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object IzborStavkiOdNabavkaSUMAKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'SUMAKOLICINA'
    end
    object IzborStavkiOdNabavkaSUMACENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'SUMACENA'
      DisplayFormat = '0.00 , .'
    end
    object IzborStavkiOdNabavkaDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object IzborStavkiOdNabavkaSUMACENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'SUMACENADDV'
      DisplayFormat = '0.00 , .'
    end
    object IzborStavkiOdNabavkaNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborStavkiOdNabavkaARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborStavkiOdNabavkaGRUPAIDNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPAIDNAZIV'
      Size = 72
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborStavkiOdNabavkaNABAVKA_ID: TFIBIntegerField
      FieldName = 'Nabavka_Id'
    end
    object IzborStavkiOdNabavkaMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' '
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborStavkiOdNabavkaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID) '
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborStavkiOdNabavka: TDataSource
    DataSet = IzborStavkiOdNabavka
    Left = 416
    Top = 264
  end
  object MaxIDTipNabavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_TIPNABAVKAID ')
    Left = 1032
    Top = 256
  end
  object countPonudaStavkaPrifatena: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_PONUDASTAVKAPRIFATENA(?PRIFATENA_PONUD' +
        'A,?GODINA, ?BROJ)')
    Left = 848
    Top = 368
  end
  object countPonudaStavkaStatus: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_COOUNTPONUDASTAVKI (?GODINA, ?BROJ)')
    Left = 752
    Top = 312
  end
  object countPonudaStavkaOtfrlena: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_PONUDASTAVKAOTFRLENA(?OTFRLENA_PONUDA,' +
        '?GODINA, ?BROJ)')
    Left = 760
    Top = 256
  end
  object countPonudaStavkaOtvorena: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_PONUDASTAVKAOTVORENA(?OTVORENA_PONUDA,' +
        ' ?GODINA, ?BROJ)')
    Left = 816
    Top = 496
  end
  object MaxVidPrevozID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_VIDPREVOZID')
    Left = 1136
    Top = 496
  end
  object SumaPrifatenaKolicina: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE  PROC_PO_SUMKOLICINA(?BROJ,?GODINA,?ARTSIF, ?A' +
        'RTVID)')
    Left = 936
    Top = 432
  end
  object tblKontakt: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAT_KONTAKT'
      'SET '
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    NAZIV = :NAZIV,'
      '    MAIL = :MAIL,'
      '    MOBILEN = :MOBILEN,'
      '    TEL = :TEL,'
      '    OPIS = :OPIS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_KONTAKT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAT_KONTAKT('
      '    ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    NAZIV,'
      '    MAIL,'
      '    MOBILEN,'
      '    TEL,'
      '    OPIS'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :NAZIV,'
      '    :MAIL,'
      '    :MOBILEN,'
      '    :TEL,'
      '    :OPIS'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    mk.ID,'
      '    mk.TIP_PARTNER,'
      '    mk.PARTNER,'
      '    mk.NAZIV,'
      '    mk.MAIL,'
      '    mk.MOBILEN,'
      '    mk.TEL,'
      '    mk.OPIS,'
      '    mp.naziv as PartnerNaziv'
      'from mat_kontakt mk'
      
        'inner join mat_partner mp on mp.id =mk.partner and mp.tip_partne' +
        'r = mk.tip_partner'
      
        'where(  mk.TIP_PARTNER like :tipPartner and  mk.PARTNER like :Pa' +
        'rtner'
      '     ) and (     MK.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    mk.ID,'
      '    mk.TIP_PARTNER,'
      '    mk.PARTNER,'
      '    mk.NAZIV,'
      '    mk.MAIL,'
      '    mk.MOBILEN,'
      '    mk.TEL,'
      '    mk.OPIS,'
      '    mp.naziv as PartnerNaziv'
      'from mat_kontakt mk'
      
        'inner join mat_partner mp on mp.id =mk.partner and mp.tip_partne' +
        'r = mk.tip_partner'
      
        'where mk.TIP_PARTNER like :tipPartner and  mk.PARTNER like :Part' +
        'ner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 16
    Top = 88
    object tblKontaktID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblKontaktTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object tblKontaktPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' (ID)'
      FieldName = 'PARTNER'
    end
    object tblKontaktNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktMAIL: TFIBStringField
      DisplayLabel = 'eMail'
      FieldName = 'MAIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktMOBILEN: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKontakt: TDataSource
    DataSet = tblKontakt
    Left = 16
    Top = 32
  end
  object tblPartner: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAT_PARTNER'
      'SET '
      '    NAZIV = :NAZIV,'
      '    ADRESA = :ADRESA,'
      '    TEL = :TEL,'
      '    FAX = :FAX,'
      '    DANOCEN = :DANOCEN,'
      '    MESTO = :MESTO,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    TATKO = :TATKO,'
      '    LOGO = :LOGO,'
      '    RE = :RE,'
      '    LOGOTEXT = :LOGOTEXT,'
      '    STATUS = :STATUS,'
      '    "LIMIT" = :"LIMIT",'
      '    MAIL = :MAIL,'
      '    URL = :URL'
      'WHERE'
      '    TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_PARTNER'
      'WHERE'
      '        TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAT_PARTNER('
      '    TIP_PARTNER,'
      '    ID,'
      '    NAZIV,'
      '    ADRESA,'
      '    TEL,'
      '    FAX,'
      '    DANOCEN,'
      '    MESTO,'
      '    IME,'
      '    PREZIME,'
      '    TATKO,'
      '    LOGO,'
      '    RE,'
      '    LOGOTEXT,'
      '    STATUS,'
      '    "LIMIT",'
      '    MAIL,'
      '    URL'
      ')'
      'VALUES('
      '    :TIP_PARTNER,'
      '    :ID,'
      '    :NAZIV,'
      '    :ADRESA,'
      '    :TEL,'
      '    :FAX,'
      '    :DANOCEN,'
      '    :MESTO,'
      '    :IME,'
      '    :PREZIME,'
      '    :TATKO,'
      '    :LOGO,'
      '    :RE,'
      '    :LOGOTEXT,'
      '    :STATUS,'
      '    :"LIMIT",'
      '    :MAIL,'
      '    :URL'
      ')')
    RefreshSQL.Strings = (
      'select mp.tip_partner,'
      '       mp.id,'
      '       mp.naziv,'
      '       mp.adresa,'
      '       mp.tel,'
      '       mp.fax,'
      '       mp.danocen,'
      '       mp.mesto,'
      '       mp.ime,'
      '       mp.prezime,'
      '       mp.tatko,'
      '       mp.logo,'
      '       mp.re,'
      '       mp.logotext,'
      '       mp.status,'
      '       mp.limit,'
      '       mtp.naziv as tip_partner_naziv,'
      '       mm.naziv as mestonaziv,'
      '       mr.naziv as rab_edin_naziv,'
      '       mp.mail,'
      '       mp.url'
      'from mat_partner mp'
      'inner join mat_tip_partner mtp on mtp.id = mp.tip_partner'
      'left outer join mat_mesto mm on mm.id = mp.mesto'
      'left outer join mat_re mr on mr.id = mp.re'
      ''
      'where(  mp.tip_partner like :tipPartner'
      '     ) and (     MP.TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and MP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mp.tip_partner,'
      '       mp.id,'
      '       mp.naziv,'
      '       mp.adresa,'
      '       mp.tel,'
      '       mp.fax,'
      '       mp.danocen,'
      '       mp.mesto,'
      '       mp.ime,'
      '       mp.prezime,'
      '       mp.tatko,'
      '       mp.logo,'
      '       mp.re,'
      '       mp.logotext,'
      '       mp.status,'
      '       mp.limit,'
      '       mtp.naziv as tip_partner_naziv,'
      '       mm.naziv as mestonaziv,'
      '       mr.naziv as rab_edin_naziv,'
      '       mp.mail,'
      '       mp.url'
      'from mat_partner mp'
      'inner join mat_tip_partner mtp on mtp.id = mp.tip_partner'
      'left outer join mat_mesto mm on mm.id = mp.mesto'
      'left outer join mat_re mr on mr.id = mp.re'
      ''
      'where mp.tip_partner like :tipPartner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 72
    Top = 88
    object tblPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object tblPartnerID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerFAX: TFIBStringField
      DisplayLabel = #1060#1072#1082#1089
      FieldName = 'FAX'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerDANOCEN: TFIBStringField
      DisplayLabel = #1044#1072#1085#1086#1095#1077#1085
      FieldName = 'DANOCEN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' (ID)'
      FieldName = 'MESTO'
    end
    object tblPartnerIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerTATKO: TFIBStringField
      DisplayLabel = #1058#1072#1090#1082#1086
      FieldName = 'TATKO'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerLOGO: TFIBBlobField
      DisplayLabel = #1051#1086#1075#1086
      FieldName = 'LOGO'
      Size = 8
    end
    object tblPartnerRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072' (ID)'
      FieldName = 'RE'
    end
    object tblPartnerLOGOTEXT: TFIBBlobField
      DisplayLabel = #1058#1077#1082#1089#1090' '#1085#1072' '#1083#1086#1075#1086
      FieldName = 'LOGOTEXT'
      Size = 8
    end
    object tblPartnerSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblPartnerLIMIT: TFIBBCDField
      DisplayLabel = #1051#1080#1084#1080#1090
      FieldName = 'LIMIT'
    end
    object tblPartnerTIP_PARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1090#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerMESTONAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerRAB_EDIN_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RAB_EDIN_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerMAIL: TFIBStringField
      DisplayLabel = 'eMail'
      FieldName = 'MAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerURL: TFIBStringField
      FieldName = 'URL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTblPartner: TDataSource
    DataSet = tblPartner
    Left = 80
    Top = 24
  end
  object frxDBPlan1: TfrxDBDataset
    UserName = 'frxDBPlan1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GODINA=GODINA'
      'GRUPA=GRUPA'
      'PODGRUPA=PODGRUPA'
      'ARTSIF=ARTSIF'
      'NAZIV=NAZIV'
      'MERKA=MERKA'
      'SUMAKOLICINA=SUMAKOLICINA'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'GRUPANAZIV=GRUPANAZIV')
    DataSource = dsPlanZaSiteSektori
    BCDToCurrency = False
    Left = 16
    Top = 736
  end
  object frxDesignerPlan: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnSaveReport = frxDesignerPlanSaveReport
    Left = 200
    Top = 656
  end
  object tblPlanZaSiteSektori: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina) as numeric(15, 2)) sumakolicina ,'
      '       (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cena,'
      '       ps.ddv,'
      '       (select first 1  (pp.cenaddv)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cenaddv,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as grupanaziv'
      ''
      '         '
      'from  po_plan_stavki ps'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and ma.artvi' +
        'd = ps.artvid and ma.id = ps.artsif'
      
        'inner join po_plan pp on pp.broj = ps.broj and pp.godina = ps.go' +
        'dina'
      'where pp.godina = :godina'
      '     and ( pp.re in (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or pp.re in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      
        'group by ps.artvid, ps.artsif, ma.naziv , mv.opis, ma.merka, ps.' +
        'godina, ma.grupa, mg.naziv,9,10'
      'order by ps.artvid, ma.grupa, ps.artsif'
      '')
    SelectSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina) as numeric(15, 2)) sumakolicina ,'
      '       (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cena,'
      '       ps.ddv,'
      '       (select first 1  (pp.cenaddv)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cenaddv,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as grupanaziv'
      ''
      '         '
      'from  po_plan_stavki ps'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and ma.artvi' +
        'd = ps.artvid and ma.id = ps.artsif'
      
        'inner join po_plan pp on pp.broj = ps.broj and pp.godina = ps.go' +
        'dina'
      'where pp.godina = :godina'
      '     and ( pp.re in (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or pp.re in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      ''
      
        'group by ps.artvid, ps.artsif, ma.naziv , mv.opis, ma.merka, ps.' +
        'godina, ma.grupa, mg.naziv,9,10'
      'order by ps.artvid, ma.grupa, ps.artsif'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 248
    Top = 736
    object tblPlanZaSiteSektoriGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPlanZaSiteSektoriGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaSiteSektoriPODGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072'(ID)'
      FieldName = 'PODGRUPA'
      Size = 102
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaSiteSektoriARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPlanZaSiteSektoriNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaSiteSektoriMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaSiteSektoriSUMAKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'SUMAKOLICINA'
      Size = 2
    end
    object tblPlanZaSiteSektoriCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblPlanZaSiteSektoriDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblPlanZaSiteSektoriCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object tblPlanZaSiteSektoriARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaSiteSektoriGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPlanZaSiteSektori: TDataSource
    DataSet = tblPlanZaSiteSektori
    Left = 352
    Top = 736
  end
  object frxReport: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40104.540193680600000000
    ReportOptions.LastChange = 40106.465330416660000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 432
    Top = 656
    Datasets = <
      item
        DataSet = frxDBFirma
        DataSetName = 'frxDBFirma'
      end
      item
        DataSet = frxDBNabavka1
        DataSetName = 'frxDBNabavka1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 136.063080000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 192.756030000000000000
          Top = 34.015770000000000000
          Width = 287.244280000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBFirma."PNAZIVMESTO"]')
          ParentFont = False
        end
        object SysMemo3: TfrxSysMemoView
          Left = 623.622450000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 7.559060000000000000
          Width = 725.000000000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '________________________________________________________________' +
              '_________________________________________________________')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Top = 83.149660000000000000
          Width = 309.921460000000000000
          Height = 45.354360000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            #1057#1077#1082#1090#1086#1088': [frxDBNabavka1."RABEDINIDNAZIV"]'
            #1053#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112': [frxDBNabavka1."BROJNANABAVKA"]')
          ParentFont = False
        end
        object Rich1: TfrxRichView
          Left = 529.134200000000000000
          Top = 64.252010000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'LOGOTEXT'
          DataSet = frxDBFirma
          DataSetName = 'frxDBFirma'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235315C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313037317B5C666F6E7474626C7B5C66
            305C666E696C5C6663686172736574323034205461686F6D613B7D7D0D0A7B5C
            2A5C67656E657261746F7220526963686564323020362E322E393230307D5C76
            6965776B696E64345C756331200D0A5C706172645C66305C667331365C706172
            0D0A7D0D0A00}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Height = 49.133890000000000000
        ParentFont = False
        Top = 506.457020000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 532.913730000000000000
          Top = 34.015770000000000000
          Width = 181.417440000000000000
          Height = 15.118120000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1057#1090#1088#1072#1085#1072' [Page#] '#1086#1076'  [TOTALPAGES#]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Top = 15.118120000000000000
          Width = 721.890230000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            
              '________________________________________________________________' +
              '________________________________________________________________' +
              '_____________________________________________')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Top = 34.015770000000000000
          Width = 173.858380000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            'CodEx Computers - '#1050#1072#1074#1072#1076#1072#1088#1094#1080)
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 177.637910000000000000
        Width = 718.110700000000000000
        object Memo6: TfrxMemoView
          Width = 117.165383620000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '  '#1064#1080#1092#1088#1072)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Width = 68.031498500000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1050#1086#1083#1080#1095#1080#1085#1072)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 359.055350000000000000
          Width = 52.913420000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Memo.UTF8W = (
            #1052#1077#1088#1082#1072)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 30.236240000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1044#1044#1042)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 578.268090000000000000
          Width = 68.031496060000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 480.000310000000000000
          Width = 68.031496060000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1062#1077#1085#1072)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 117.165430000000000000
          Width = 241.889920000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Memo.UTF8W = (
            #1053#1072#1079#1080#1074)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 646.299630000000000000
          Width = 71.811070000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1048#1079#1085#1086#1089)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Height = 18.897650000000000000
        ParentFont = False
        Top = 359.055350000000000000
        Width = 718.110700000000000000
        DataSet = frxDBNabavka1
        DataSetName = 'frxDBNabavka1'
        RowCount = 0
        Stretched = True
        object Memo4: TfrxMemoView
          Left = 411.968770000000000000
          Width = 71.810999210000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          DataSet = frxDBPlan1
          DataSetName = 'frxDBPlan1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBNabavka1."KOLICINA"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 359.055350000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          DataSet = frxDBPlan1
          DataSetName = 'frxDBPlan1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDBNabavka1."MERKA"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 480.000310000000000000
          Width = 71.810999210000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBNabavka1."CENA"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 578.268090000000000000
          Width = 803.000000000000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 578.268090000000000000
          Width = 68.031493620000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBNabavka1."CENADDV"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 117.165430000000000000
          Width = 241.889920000000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDBNabavka1."NAZIVARTIKAL"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDBNabavka1."ARTSIF"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 548.031850000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBNabavka1."DDV"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 646.299630000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBNabavka1."IIZNOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 45.354360000000000000
        ParentFont = False
        Top = 400.630180000000000000
        Width = 718.110700000000000000
        object SysMemo1: TfrxSysMemoView
          Left = 646.299630000000000000
          Top = 15.118120000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBNabavka1."IIZNOS">,MasterData1,2)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clDefault
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '________________________________________________________________' +
              '_______________________________________________')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDBNabavka1."ARTVID"'
        object Memo22: TfrxMemoView
          Top = 3.779530000000020000
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Fill.BackColor = 15000804
          Memo.UTF8W = (
            #1043#1088#1091#1087#1072': [frxDBNabavka1."ARTVIDIDNAZIV"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        Condition = 'frxDBNabavka1."GRUPA"'
        object Memo23: TfrxMemoView
          Left = 30.236240000000000000
          Top = 3.779530000000020000
          Width = 687.874460000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Fill.BackColor = 15000804
          Memo.UTF8W = (
            #1055#1086#1076#1075#1088#1091#1087#1072':[frxDBNabavka1."GRUPAIDNAZIV"]')
          ParentFont = False
        end
      end
    end
  end
  object tblReportDizajn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_REPORT'
      'SET '
      '    KOREN = :KOREN,'
      '    NASLOV = :NASLOV,'
      '    "SQL" = :"SQL",'
      '    DATA = :DATA,'
      '    XML = :XML,'
      '    USLOVI = :USLOVI,'
      '    NASLOVI = :NASLOVI,'
      '    VIDLIVOST = :VIDLIVOST,'
      '    POJAVIUSLOVI = :POJAVIUSLOVI,'
      '    USLOVITEXT = :USLOVITEXT,'
      '    DINAMICKIUSLOVI = :DINAMICKIUSLOVI,'
      '    TABELA_GRUPA = :TABELA_GRUPA,'
      '    BR = :BR,'
      '    REDOSLED = :REDOSLED'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_REPORT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_REPORT('
      '    ID,'
      '    KOREN,'
      '    NASLOV,'
      '    "SQL",'
      '    DATA,'
      '    XML,'
      '    USLOVI,'
      '    NASLOVI,'
      '    VIDLIVOST,'
      '    POJAVIUSLOVI,'
      '    USLOVITEXT,'
      '    DINAMICKIUSLOVI,'
      '    TABELA_GRUPA,'
      '    BR,'
      '    REDOSLED'
      ')'
      'VALUES('
      '    :ID,'
      '    :KOREN,'
      '    :NASLOV,'
      '    :"SQL",'
      '    :DATA,'
      '    :XML,'
      '    :USLOVI,'
      '    :NASLOVI,'
      '    :VIDLIVOST,'
      '    :POJAVIUSLOVI,'
      '    :USLOVITEXT,'
      '    :DINAMICKIUSLOVI,'
      '    :TABELA_GRUPA,'
      '    :BR,'
      '    :REDOSLED'
      ')')
    RefreshSQL.Strings = (
      'select sr.id,'
      '       sr.koren,'
      '       sr.naslov,'
      '       sr.sql,'
      '       sr.data,'
      '       sr.xml,'
      '       sr.uslovi,'
      '       sr.naslovi,'
      '       sr.vidlivost,'
      '       sr.pojaviuslovi,'
      '       sr.uslovitext,'
      '       sr.dinamickiuslovi,'
      '       sr.tabela_grupa,'
      '       sr.br,'
      '       sr.redosled'
      'from sys_report sr'
      'where(  sr.id = :id'
      '     ) and (     SR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select sr.id,'
      '       sr.koren,'
      '       sr.naslov,'
      '       sr.sql,'
      '       sr.data,'
      '       sr.xml,'
      '       sr.uslovi,'
      '       sr.naslovi,'
      '       sr.vidlivost,'
      '       sr.pojaviuslovi,'
      '       sr.uslovitext,'
      '       sr.dinamickiuslovi,'
      '       sr.tabela_grupa,'
      '       sr.br,'
      '       sr.redosled'
      'from sys_report sr'
      'where sr.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 656
    object tblReportDizajnID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblReportDizajnKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblReportDizajnNASLOV: TFIBStringField
      FieldName = 'NASLOV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReportDizajnSQL: TFIBBlobField
      FieldName = 'SQL'
      Size = 8
    end
    object tblReportDizajnDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object tblReportDizajnXML: TFIBBlobField
      FieldName = 'XML'
      Size = 8
    end
    object tblReportDizajnUSLOVI: TFIBBlobField
      FieldName = 'USLOVI'
      Size = 8
    end
    object tblReportDizajnNASLOVI: TFIBBlobField
      FieldName = 'NASLOVI'
      Size = 8
    end
    object tblReportDizajnVIDLIVOST: TFIBStringField
      FieldName = 'VIDLIVOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReportDizajnPOJAVIUSLOVI: TFIBIntegerField
      FieldName = 'POJAVIUSLOVI'
    end
    object tblReportDizajnUSLOVITEXT: TFIBBlobField
      FieldName = 'USLOVITEXT'
      Size = 8
    end
    object tblReportDizajnDINAMICKIUSLOVI: TFIBBlobField
      FieldName = 'DINAMICKIUSLOVI'
      Size = 8
    end
    object tblReportDizajnTABELA_GRUPA: TFIBStringField
      FieldName = 'TABELA_GRUPA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReportDizajnBR: TFIBIntegerField
      FieldName = 'BR'
    end
    object tblReportDizajnREDOSLED: TFIBIntegerField
      FieldName = 'REDOSLED'
    end
  end
  object dsReportDizajn: TDataSource
    DataSet = tblReportDizajn
    Left = 120
    Top = 656
  end
  object tblPlanZaIzbraniotSektor: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina) as numeric(15, 2)) sumakolicina ,'
      '       (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cena,'
      '       ps.ddv,'
      '       (select first 1  (pp.cenaddv)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cenaddv,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as grupanaziv,'
      '       mr.naziv as renaziv'
      ''
      '         '
      'from  po_plan_stavki ps'
      
        'inner join po_plan pp on pp.godina= ps.godina and pp.broj = ps.b' +
        'roj'
      'inner join mat_re mr on mr.id = pp.re'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and ma.artvi' +
        'd = ps.artvid and ma.id = ps.artsif'
      'where ps.godina = :godina  and pp.re = :re'
      
        'group by ps.artvid, ps.artsif, ma.naziv , mv.opis, ma.merka, ps.' +
        'godina, ma.grupa,mr.naziv,mg.naziv,9,10')
    SelectSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina) as numeric(15, 2)) sumakolicina ,'
      '       (select first 1  (pp.cena)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cena,'
      '       ps.ddv,'
      '       (select first 1  (pp.cenaddv)'
      '            from po_plan_stavki pp'
      '            where pp.artvid=ps.artvid and pp.artsif = ps.artsif'
      '            order by pp.vreme desc)as cenaddv,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as grupanaziv,'
      '       mr.naziv as renaziv'
      ''
      '         '
      'from  po_plan_stavki ps'
      
        'inner join po_plan pp on pp.godina= ps.godina and pp.broj = ps.b' +
        'roj'
      'inner join mat_re mr on mr.id = pp.re'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and ma.artvi' +
        'd = ps.artvid and ma.id = ps.artsif'
      'where ps.godina = :godina  and pp.re = :re'
      
        'group by ps.artvid, ps.artsif, ma.naziv , mv.opis, ma.merka, ps.' +
        'godina, ma.grupa,mr.naziv,mg.naziv,9,10'
      'order by ps.artvid, ma.grupa, ps.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 472
    Top = 736
    object tblPlanZaIzbraniotSektorGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPlanZaIzbraniotSektorGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072'(ID)'
      FieldName = 'GRUPA'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorPODGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072'(ID)'
      FieldName = 'PODGRUPA'
      Size = 102
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPlanZaIzbraniotSektorNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorSUMAKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'SUMAKOLICINA'
      Size = 2
    end
    object tblPlanZaIzbraniotSektorCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblPlanZaIzbraniotSektorDDV: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
    end
    object tblPlanZaIzbraniotSektorCENADDV: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object tblPlanZaIzbraniotSektorARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanZaIzbraniotSektorRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPlanZaIzbraniotSektor: TDataSource
    DataSet = tblPlanZaIzbraniotSektor
    Left = 600
    Top = 736
  end
  object frxDBPlan2: TfrxDBDataset
    UserName = 'frxDBPlan2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GODINA=GODINA'
      'GRUPA=GRUPA'
      'PODGRUPA=PODGRUPA'
      'ARTSIF=ARTSIF'
      'NAZIV=NAZIV'
      'MERKA=MERKA'
      'SUMAKOLICINA=SUMAKOLICINA'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'GRUPANAZIV=GRUPANAZIV'
      'RENAZIV=RENAZIV')
    DataSource = dsPlanZaIzbraniotSektor
    BCDToCurrency = False
    Left = 80
    Top = 736
  end
  object frxDBFirma: TfrxDBDataset
    UserName = 'frxDBFirma'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'NAZIV=NAZIV'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'KOREN=KOREN'
      'SPISOK=SPISOK'
      'POTEKLO=POTEKLO'
      'RE=RE'
      'LOGO=LOGO'
      'PARTNERNAZIV=PARTNERNAZIV'
      'MESTONAZIV=MESTONAZIV'
      'ADRESA=ADRESA'
      'TEL=TEL'
      'FAX=FAX'
      'DANOCEN=DANOCEN'
      'LOGOTEXT=LOGOTEXT')
    DataSource = dsFirmi
    BCDToCurrency = False
    Left = 288
    Top = 656
  end
  object Firmi: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    VIEW_MAT_FIRMI.ID,'
      '    VIEW_MAT_FIRMI.NAZIV,'
      '    VIEW_MAT_FIRMI.TIP_PARTNER,'
      '    VIEW_MAT_FIRMI.PARTNER,'
      '    VIEW_MAT_FIRMI.KOREN,'
      '    VIEW_MAT_FIRMI.SPISOK,'
      '    VIEW_MAT_FIRMI.POTEKLO,'
      '    VIEW_MAT_FIRMI.RE,'
      '    VIEW_MAT_FIRMI.LOGO,'
      '    VIEW_MAT_FIRMI.PARTNERNAZIV,'
      '    VIEW_MAT_FIRMI.MESTONAZIV,'
      '    VIEW_MAT_FIRMI.ADRESA,'
      '    VIEW_MAT_FIRMI.TEL,'
      '    VIEW_MAT_FIRMI.FAX,'
      '    VIEW_MAT_FIRMI.DANOCEN,'
      '    VIEW_MAT_FIRMI.LOGOTEXT,'
      
        '        VIEW_MAT_FIRMI.ADRESA ||'#39', '#39'|| VIEW_MAT_FIRMI.MESTONAZIV' +
        ' as adresamesto'
      'from view_mat_firmi'
      'where VIEW_MAT_FIRMI.ID = :re')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 336
    Top = 656
    object FirmiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object FirmiNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object FirmiPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object FirmiKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object FirmiSPISOK: TFIBStringField
      FieldName = 'SPISOK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object FirmiLOGO: TFIBBlobField
      FieldName = 'LOGO'
      Size = 8
    end
    object FirmiPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiMESTONAZIV: TFIBStringField
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiTEL: TFIBStringField
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiFAX: TFIBStringField
      FieldName = 'FAX'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiDANOCEN: TFIBStringField
      FieldName = 'DANOCEN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object FirmiLOGOTEXT: TFIBBlobField
      FieldName = 'LOGOTEXT'
      Size = 8
    end
    object FirmiADRESAMESTO: TFIBStringField
      FieldName = 'ADRESAMESTO'
      Size = 152
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsFirmi: TDataSource
    DataSet = Firmi
    Left = 384
    Top = 656
  end
  object tblStavkiZaOdbranPlan: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina)as numeric(15, 2)) as sumakolicina,'
      '       cast(sum (ps.cena)as numeric(15, 2)) as sumacena,'
      '       ps.ddv,'
      '       cast(sum(ps.cenaddv)as numeric(15, 2)) as sumasenaddv,'
      '       pp.re,'
      '       mr.naziv reNaziv,'
      '       pp.broj ||'#39'/'#39'||pp.godina as planBroj,'
      '       pp.datum_od,'
      '       pp.datum_do,'
      
        '       cast((ps.kolicina * ps.cenaddv) as numeric (15,2)) as izn' +
        'os,'
      '       pp.opis'
      '         '
      'from  po_plan_stavki ps'
      
        'inner join po_plan pp on pp.godina= ps.godina and pp.broj = ps.b' +
        'roj'
      
        'inner join mat_re mr on mr.id = pp.re and pp.godina= ps.godina a' +
        'nd pp.broj = ps.broj'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and  ma.artv' +
        'id = ps.artvid and ma.id = ps.artsif'
      'where(  ps.godina = :godina  and ps.broj =:broj'
      '     ) and (     PS.GODINA = :OLD_GODINA'
      '    and PS.ARTSIF = :OLD_ARTSIF'
      '     )'
      '    '
      
        'group by ps.artvid, ps.artsif, ps.ddv, ma.naziv , mv.opis, ma.me' +
        'rka, ps.godina, pp.re, mg.naziv, ma.grupa, mr.naziv, pp.broj, pp' +
        '.godina, 14, 15, 16, 17')
    SelectSQL.Strings = (
      'select'
      '       ps.godina,'
      '       ps.artvid ||'#39'. '#39'|| mv.opis as grupa,'
      '       ps.artsif,'
      '       ma.naziv,'
      '       ma.grupa ||'#39'. '#39'|| mg.naziv as podgrupa,'
      '       ma.merka,'
      '       cast(sum (ps.kolicina)as numeric(15, 2)) as sumakolicina,'
      '       cast(sum (ps.cena)as numeric(15, 2)) as sumacena,'
      '       ps.ddv,'
      '       cast(sum(ps.cenaddv)as numeric(15, 2)) as sumasenaddv,'
      '       pp.re,'
      '       mr.naziv reNaziv,'
      '       pp.broj ||'#39'/'#39'||pp.godina as planBroj,'
      '       pp.datum_od,'
      '       pp.datum_do,'
      
        '       cast((ps.kolicina * ps.cenaddv) as numeric (15,2)) as izn' +
        'os,'
      '       pp.opis'
      '         '
      'from  po_plan_stavki ps'
      
        'inner join po_plan pp on pp.godina= ps.godina and pp.broj = ps.b' +
        'roj'
      
        'inner join mat_re mr on mr.id = pp.re and pp.godina= ps.godina a' +
        'nd pp.broj = ps.broj'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ma.id = p' +
        's.artsif'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and  ma.artvid = p' +
        's.artvid and ma.id = ps.artsif'
      
        'left outer join mtr_artgrupa mg on ma.grupa = mg.id and  ma.artv' +
        'id = ps.artvid and ma.id = ps.artsif'
      'where ps.godina = :godina  and ps.broj =:broj'
      
        'group by ps.artvid, ps.artsif, ps.ddv, ma.naziv , mv.opis, ma.me' +
        'rka, ps.godina, pp.re, mg.naziv, ma.grupa, mr.naziv, pp.broj, pp' +
        '.godina, 14, 15, 16, 17'
      'order by ps.artvid, ma.grupa, ps.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 736
    object tblStavkiZaOdbranPlanGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblStavkiZaOdbranPlanGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblStavkiZaOdbranPlanNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanPODGRUPA: TFIBStringField
      FieldName = 'PODGRUPA'
      Size = 72
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanSUMAKOLICINA: TFIBBCDField
      FieldName = 'SUMAKOLICINA'
      Size = 2
    end
    object tblStavkiZaOdbranPlanSUMACENA: TFIBBCDField
      FieldName = 'SUMACENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblStavkiZaOdbranPlanDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object tblStavkiZaOdbranPlanSUMASENADDV: TFIBBCDField
      FieldName = 'SUMASENADDV'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblStavkiZaOdbranPlanRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblStavkiZaOdbranPlanRENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanPLANBROJ: TFIBStringField
      FieldName = 'PLANBROJ'
      Size = 18
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranPlanDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblStavkiZaOdbranPlanDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblStavkiZaOdbranPlanIZNOS: TFIBBCDField
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblStavkiZaOdbranPlanOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStavkiZaOdbranPlan: TDataSource
    DataSet = tblStavkiZaOdbranPlan
    Left = 750
    Top = 736
  end
  object frxDBPlan3: TfrxDBDataset
    UserName = 'frxDBPlan3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GODINA=GODINA'
      'GRUPA=GRUPA'
      'ARTSIF=ARTSIF'
      'NAZIV=NAZIV'
      'PODGRUPA=PODGRUPA'
      'MERKA=MERKA'
      'SUMAKOLICINA=SUMAKOLICINA'
      'SUMACENA=SUMACENA'
      'DDV=DDV'
      'SUMASENADDV=SUMASENADDV'
      'RE=RE'
      'RENAZIV=RENAZIV'
      'PLANBROJ=PLANBROJ'
      'DATUM_OD=DATUM_OD'
      'DATUM_DO=DATUM_DO'
      'IZNOS=IZNOS'
      'OPIS=OPIS')
    DataSource = dsStavkiZaOdbranPlan
    BCDToCurrency = False
    Left = 152
    Top = 736
  end
  object frxDBNabavka1: TfrxDBDataset
    UserName = 'frxDBNabavka1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NABAVKA_ID=NABAVKA_ID'
      'ID=ID'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'KOLICINA=KOLICINA'
      'RE=RE'
      'VREME=VREME'
      'KREIRAL=KREIRAL'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'BROJNABAVKA=BROJNABAVKA'
      'NAZIVRE=NAZIVRE'
      'NAZIVARTIKAL=NAZIVARTIKAL'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'GRUPANAZIV=GRUPANAZIV'
      'ARTVIDIDNAZIV=ARTVIDIDNAZIV'
      'GRUPAIDNAZIV=GRUPAIDNAZIV'
      'RABEDINIDNAZIV=RABEDINIDNAZIV'
      'IIZNOS=IIZNOS'
      'POTEKLO=POTEKLO'
      'BROJNANABAVKA=BROJNANABAVKA'
      'MERKANAZIV=MERKANAZIV'
      'MERKA=MERKA'
      'GRUPA=GRUPA'
      'BROJ_DOGOVOR=BROJ_DOGOVOR')
    DataSource = dsSpecifikacijaZaNabavka
    BCDToCurrency = False
    Left = 32
    Top = 808
  end
  object dsStavkiZaOdbranaPonuda: TDataSource
    DataSet = tblStavkiZaOdbranaPonuda
    Left = 360
    Top = 880
  end
  object tblStavkiZaOdbranaPonuda: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,2))as kolicina,'
      '       ps.status,'
      '       ps.kolicina_prifatena,'
      '       ps.opis,'
      '       cast(ps.cena as numeric (15,2))as cena,'
      '       ps.ddv,'
      '       cast(ps.cenaddv as numeric(15,2))as cenaddv,'
      '       ps.rabat,'
      '       ps.valuta,'
      '       ps.kurs,'
      '       ma.naziv as artnaziv,'
      '       ma.merka as merka,'
      '       ma.grupa as artgrupa,'
      '       ps.artvid ||'#39'. '#39'||mv.opis as artvidnaziv,'
      '       ma.grupa ||'#39'. '#39'||mg.naziv as artgrupanaziv,'
      '       mval.naziv as valutanaziv,'
      '       ps.krajna_cena,'
      
        '       cast((ps.krajna_cena* ps.kolicina_prifatena) as numeric(1' +
        '5, 2)) as iiznos,'
      '       ms.naziv as statusnaziv,'
      '       ps.broj ||'#39'/'#39'|| ps.godina as ponudabroj'
      'from po_ponuda_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      
        'inner join mtr_artvid mv on mv.id = ma.artvid and ma.id = ps.art' +
        'sif and ma.artvid = ps.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'ps.artsif and ma.artvid = ps.artvid'
      'inner join mat_valuta mval on mval.id=ps.valuta'
      'inner join mat_status ms on ms.id=ps.status'
      'where ps.godina = :godina and ps.broj = :broj'
      ''
      'order by ps.artvid,'
      '         ma.grupa,          '
      '         ps.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 224
    Top = 880
    object tblStavkiZaOdbranaPonudaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblStavkiZaOdbranaPonudaBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object tblStavkiZaOdbranaPonudaGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblStavkiZaOdbranaPonudaARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblStavkiZaOdbranaPonudaARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblStavkiZaOdbranaPonudaKOLICINA: TFIBBCDField
      FieldName = 'KOLICINA'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object tblStavkiZaOdbranaPonudaSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblStavkiZaOdbranaPonudaKOLICINA_PRIFATENA: TFIBFloatField
      DefaultExpression = '0'
      FieldName = 'KOLICINA_PRIFATENA'
    end
    object tblStavkiZaOdbranaPonudaOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblStavkiZaOdbranaPonudaDDV: TFIBFloatField
      DefaultExpression = '0'
      FieldName = 'DDV'
    end
    object tblStavkiZaOdbranaPonudaCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblStavkiZaOdbranaPonudaRABAT: TFIBFloatField
      DefaultExpression = '0'
      FieldName = 'RABAT'
    end
    object tblStavkiZaOdbranaPonudaVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaKURS: TFIBFloatField
      DefaultExpression = '1'
      FieldName = 'KURS'
    end
    object tblStavkiZaOdbranaPonudaARTNAZIV: TFIBStringField
      FieldName = 'ARTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaARTGRUPA: TFIBStringField
      FieldName = 'ARTGRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaARTVIDNAZIV: TFIBStringField
      FieldName = 'ARTVIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaARTGRUPANAZIV: TFIBStringField
      FieldName = 'ARTGRUPANAZIV'
      Size = 72
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaVALUTANAZIV: TFIBStringField
      FieldName = 'VALUTANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblStavkiZaOdbranaPonudaIIZNOS: TFIBBCDField
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblStavkiZaOdbranaPonudaSTATUSNAZIV: TFIBStringField
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranaPonudaPONUDABROJ: TFIBStringField
      FieldName = 'PONUDABROJ'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBPonuda1: TfrxDBDataset
    UserName = 'frxDBPonuda1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'KOLICINA=KOLICINA'
      'STATUS=STATUS'
      'KOLICINA_PRIFATENA=KOLICINA_PRIFATENA'
      'OPIS=OPIS'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'RABAT=RABAT'
      'VALUTA=VALUTA'
      'KURS=KURS'
      'ARTNAZIV=ARTNAZIV'
      'MERKA=MERKA'
      'ARTGRUPA=ARTGRUPA'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'ARTGRUPANAZIV=ARTGRUPANAZIV'
      'VALUTANAZIV=VALUTANAZIV'
      'KRAJNA_CENA=KRAJNA_CENA'
      'IIZNOS=IIZNOS'
      'STATUSNAZIV=STATUSNAZIV'
      'PONUDABROJ=PONUDABROJ')
    DataSource = dsStavkiZaOdbranaPonuda
    BCDToCurrency = False
    Left = 24
    Top = 880
  end
  object tblSpecifikacijaNaNabavka: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ps.nabavka_id,'
      '       ps.id,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,2)) as kolicina,'
      '       ps.re,'
      '       ps.vreme,'
      '       ps.kreiral,'
      '       cast(ps.cena as numeric(15,2))as cena,'
      '       ps.DDV,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       pn.broj as brojnabavka,'
      '       mr.naziv as nazivre,'
      '       ma.naziv as nazivartikal,'
      '       mtra.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ps.artvid ||'#39'. '#39'|| mtra.opis as artvidIDNaziv,'
      '       mg.id || '#39'. '#39'|| mg.naziv as grupaIdNaziv,'
      '       ps.re || '#39'. '#39'|| mr.naziv as RabEdinIDNaziv,'
      
        '       cast((ps.cenaddv * ps.kolicina) as numeric(15, 2)) as iiz' +
        'nos,'
      '       mr.poteklo,'
      
        '       pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNaNabavk' +
        'a,'
      '       mm.naziv as merkaNaziv,'
      '       ma.merka,'
      '       ma.grupa'
      'from po_nabavka_stavki ps'
      ''
      'inner join po_nabavka pn on pn.id = ps.nabavka_id'
      'inner join mat_re mr on mr.id= ps.re'
      
        'inner join mtr_artikal ma on ma.artvid=ps.artvid and ma.id=ps.ar' +
        'tsif'
      
        'inner join mtr_artvid mtra on mtra.id=ma.artvid and ma.artvid=ps' +
        '.artvid and ma.id=ps.artsif'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.artvi' +
        'd=ps.artvid and ma.id=ps.artsif'
      
        'inner join mat_merka mm on ma.merka = mm.id and  ma.artvid=ps.ar' +
        'tvid and ma.id=ps.artsif'
      ''
      'where(  ps.nabavka_id like :NabavkaId'
      '     ) and (     PS.NABAVKA_ID = :OLD_NABAVKA_ID'
      '    and PS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ps.nabavka_id,'
      '       ps.id,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,2)) as kolicina,'
      '       ps.re,'
      '       ps.vreme,'
      '       ps.kreiral,'
      '       cast(ps.cena as numeric(15,2))as cena,'
      '       ps.DDV,'
      '       cast(ps.cenaddv as numeric(15,2)) as cenaddv,'
      '       pn.broj as brojnabavka,'
      '       mr.naziv as nazivre,'
      '       ma.naziv as nazivartikal,'
      '       mtra.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ps.artvid ||'#39'. '#39'|| mtra.opis as artvidIDNaziv,'
      '       mg.id || '#39'. '#39'|| mg.naziv as grupaIdNaziv,'
      '       ps.re || '#39'. '#39'|| mr.naziv as RabEdinIDNaziv,'
      
        '       cast((ps.cenaddv * ps.kolicina) as numeric(15, 2)) as iiz' +
        'nos,'
      '       mr.poteklo,'
      
        '       pn.broj || '#39'/'#39' || pn.re ||'#39'/'#39' ||pn.godina as BrojNaNabavk' +
        'a,'
      '       mm.naziv as merkaNaziv,'
      '       ma.merka,'
      '       ma.grupa,'
      '       pn.broj_dogovor'
      'from po_nabavka_stavki ps'
      ''
      'inner join po_nabavka pn on pn.id = ps.nabavka_id'
      'inner join mat_re mr on mr.id= ps.re'
      
        'inner join mtr_artikal ma on ma.artvid=ps.artvid and ma.id=ps.ar' +
        'tsif'
      
        'inner join mtr_artvid mtra on mtra.id=ma.artvid and ma.artvid=ps' +
        '.artvid and ma.id=ps.artsif'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.artvi' +
        'd=ps.artvid and ma.id=ps.artsif'
      
        'inner join mat_merka mm on ma.merka = mm.id and  ma.artvid=ps.ar' +
        'tvid and ma.id=ps.artsif'
      ''
      'where ps.nabavka_id like :NabavkaId'
      'order by ps.re, ps.artvid, ma.grupa, ps.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 240
    Top = 808
    object tblSpecifikacijaNaNabavkaNABAVKA_ID: TFIBIntegerField
      FieldName = 'NABAVKA_ID'
    end
    object tblSpecifikacijaNaNabavkaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblSpecifikacijaNaNabavkaARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblSpecifikacijaNaNabavkaARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblSpecifikacijaNaNabavkaKOLICINA: TFIBBCDField
      FieldName = 'KOLICINA'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object tblSpecifikacijaNaNabavkaRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblSpecifikacijaNaNabavkaVREME: TFIBDateTimeField
      FieldName = 'VREME'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblSpecifikacijaNaNabavkaKREIRAL: TFIBStringField
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblSpecifikacijaNaNabavkaDDV: TFIBFloatField
      DefaultExpression = '0'
      FieldName = 'DDV'
    end
    object tblSpecifikacijaNaNabavkaCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblSpecifikacijaNaNabavkaBROJNABAVKA: TFIBIntegerField
      FieldName = 'BROJNABAVKA'
    end
    object tblSpecifikacijaNaNabavkaNAZIVRE: TFIBStringField
      FieldName = 'NAZIVRE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaNAZIVARTIKAL: TFIBStringField
      FieldName = 'NAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaARTVIDNAZIV: TFIBStringField
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaGRUPANAZIV: TFIBStringField
      FieldName = 'GRUPANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaARTVIDIDNAZIV: TFIBStringField
      FieldName = 'ARTVIDIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaGRUPAIDNAZIV: TFIBStringField
      FieldName = 'GRUPAIDNAZIV'
      Size = 1076
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaRABEDINIDNAZIV: TFIBStringField
      FieldName = 'RABEDINIDNAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaIIZNOS: TFIBBCDField
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblSpecifikacijaNaNabavkaPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaBROJNANABAVKA: TFIBStringField
      FieldName = 'BROJNANABAVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaMERKANAZIV: TFIBStringField
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSpecifikacijaNaNabavkaBROJ_DOGOVOR: TFIBStringField
      FieldName = 'BROJ_DOGOVOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSpecifikacijaZaNabavka: TDataSource
    DataSet = tblSpecifikacijaNaNabavka
    Left = 360
    Top = 808
  end
  object frxDBNabavka2: TfrxDBDataset
    UserName = 'frxDBNabavka2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'RE=RE'
      'GODINA=GODINA'
      'PREDMET=PREDMET'
      'DATUM_KREIRANA=DATUM_KREIRANA'
      'DATUM_VAZENJE=DATUM_VAZENJE'
      'DATUM_PONUDA_OD=DATUM_PONUDA_OD'
      'DATUM_PONUDA_DO=DATUM_PONUDA_DO'
      'TIP_NABAVKA=TIP_NABAVKA'
      'OPIS=OPIS'
      'TIP_KONTAKT=TIP_KONTAKT'
      'KONTAKT=KONTAKT'
      'STATUS=STATUS'
      'KREIRAL=KREIRAL'
      'IZNOS=IZNOS'
      'VALUTA=VALUTA'
      'KURS=KURS'
      'PRIORITET=PRIORITET'
      'USLOVI=USLOVI'
      'ZABELESKA=ZABELESKA'
      'VREME=VREME'
      'TIP_NABAVKA_NAZIV=TIP_NABAVKA_NAZIV'
      'PERIOD_VAZENJE=PERIOD_VAZENJE'
      'NAZIVSTATUS=NAZIVSTATUS'
      'REIDNAZIV=REIDNAZIV'
      'PARTNERNAZIV=PARTNERNAZIV'
      'VALUTANAZIV=VALUTANAZIV'
      'PRIORITETNAZIV=PRIORITETNAZIV'
      'BROJ=BROJ'
      'RENAZIV=RENAZIV'
      'BROJNABAVKA=BROJNABAVKA'
      'POTEKLO=POTEKLO'
      'IZNOSCENA=IZNOSCENA'
      'IZNOS_DENARI=IZNOS_DENARI'
      'ODOBRIL=ODOBRIL'
      'DATUM_ODOBRUVANJE=DATUM_ODOBRUVANJE'
      'BROJ_DOGOVOR=BROJ_DOGOVOR'
      'MESEC=MESEC'
      'mesecNaziv=mesecNaziv')
    DataSource = dsNabavki
    BCDToCurrency = False
    Left = 128
    Top = 808
  end
  object frxDBPonuda2: TfrxDBDataset
    UserName = 'frxDBPonuda2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NABAVKA_ID=NABAVKA_ID'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'DATUM=DATUM'
      'DATUM_VAZENJE=DATUM_VAZENJE'
      'KONTAKT=KONTAKT'
      'ZABELESKA=ZABELESKA'
      'USLOVI=USLOVI'
      'STATUS=STATUS'
      'NACIN_PLAKJANJE=NACIN_PLAKJANJE'
      'KREIRAL=KREIRAL'
      'VREME=VREME'
      'NBROJ=NBROJ'
      'NRE=NRE'
      'NGODINA=NGODINA'
      'PNAZIV=PNAZIV'
      'KNAZIV=KNAZIV'
      'SNAZIV=SNAZIV'
      'PLAKANJENAZIV=PLAKANJENAZIV'
      'BROJNABAVKA=BROJNABAVKA'
      'GODINA=GODINA'
      'BROJ=BROJ'
      'PONUDABROJ=PONUDABROJ'
      'PREDMETNABAVKA=PREDMETNABAVKA'
      'PREDMETPONUDA=PREDMETPONUDA')
    DataSource = dsPonudi
    BCDToCurrency = False
    Left = 112
    Top = 880
  end
  object frxDBNAlog1: TfrxDBDataset
    UserName = 'frxDBNalog1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ID=ID'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'KOLICINA=KOLICINA'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'OPIS=OPIS'
      'KREIRAL=KREIRAL'
      'VREME=VREME'
      'NAZIVARTIKAL=NAZIVARTIKAL'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'GRUPANAZIV=GRUPANAZIV'
      'GRUPA=GRUPA'
      'MERKA=MERKA'
      'MERKANAZIV=MERKANAZIV'
      'IIZNOS=IIZNOS')
    DataSource = dsStavkiZaOdbranNalog
    BCDToCurrency = False
    Left = 24
    Top = 952
  end
  object tblStavkiZaOdbranNalog: TpFIBDataSet
    SelectSQL.Strings = (
      'select pn.broj,'
      '       pn.godina,'
      '       pn.id,'
      '       pn.artvid,'
      '       pn.artsif,'
      '       pn.kolicina,'
      '       pn.cena,'
      '       pn.ddv,'
      '       pn.cenaddv,'
      '       pn.opis,'
      '       pn.kreiral,'
      '       pn.vreme,'
      '       ma.naziv as NazivArtikal,'
      '       mv.opis as ArtvidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       ma.grupa,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      
        '       cast((pn.cenaddv* pn.kolicina) as numeric(15, 2)) as iizn' +
        'os'
      'from po_nalog_stavki pn'
      
        'inner join mtr_artikal ma on ma.id = pn.artsif and ma.artvid = p' +
        'n.artvid'
      
        'inner join mtr_artvid mv on ma.artvid = mv.id and ma.id = pn.art' +
        'sif and ma.artvid = pn.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'pn.artsif and ma.artvid = pn.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = pn.artsi' +
        'f and ma.artvid = pn.artvid'
      'where pn.broj = :broj and pn.godina =:godina'
      'order by pn.artvid, ma.grupa, pn.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 208
    Top = 952
    object tblStavkiZaOdbranNalogBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object tblStavkiZaOdbranNalogGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblStavkiZaOdbranNalogID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblStavkiZaOdbranNalogARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblStavkiZaOdbranNalogARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblStavkiZaOdbranNalogKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblStavkiZaOdbranNalogCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblStavkiZaOdbranNalogDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object tblStavkiZaOdbranNalogCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object tblStavkiZaOdbranNalogOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogKREIRAL: TFIBStringField
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogVREME: TFIBDateTimeField
      FieldName = 'VREME'
    end
    object tblStavkiZaOdbranNalogNAZIVARTIKAL: TFIBStringField
      FieldName = 'NAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogARTVIDNAZIV: TFIBStringField
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogGRUPANAZIV: TFIBStringField
      FieldName = 'GRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogMERKANAZIV: TFIBStringField
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiZaOdbranNalogIIZNOS: TFIBBCDField
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
  end
  object dsStavkiZaOdbranNalog: TDataSource
    DataSet = tblStavkiZaOdbranNalog
    Left = 360
    Top = 952
  end
  object frxDBNalog2: TfrxDBDataset
    UserName = 'frxDBNalog2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'PONUDA_BROJ=PONUDA_BROJ'
      'PONUDA_GODINA=PONUDA_GODINA'
      'NABAVKA_ID=NABAVKA_ID'
      'DATUM=DATUM'
      'DATUM_ISPORAKA=DATUM_ISPORAKA'
      'STATUS=STATUS'
      'KREIRAL=KREIRAL'
      'TIP_KONTAKT=TIP_KONTAKT'
      'KONTAKT=KONTAKT'
      'PRIMAC=PRIMAC'
      'PRIMAC_ADRESA=PRIMAC_ADRESA'
      'PRIMAC_KONTAKT=PRIMAC_KONTAKT'
      'TIP_PLAKJANJE=TIP_PLAKJANJE'
      'VID_PREVOZ=VID_PREVOZ'
      'PREDMET_NABAVKA=PREDMET_NABAVKA'
      'ZABELESKA=ZABELESKA'
      'TEZINA=TEZINA'
      'VREME=VREME'
      'BROJNAPONUDA=BROJNAPONUDA'
      'BROJNANABAVKA=BROJNANABAVKA'
      'STATUSNAZIV=STATUSNAZIV'
      'KONTAKTNAZIV=KONTAKTNAZIV'
      'PLAKANJENAZIV=PLAKANJENAZIV'
      'PREVOZNAZIV=PREVOZNAZIV'
      'BROJNANALOG=BROJNANALOG'
      'TIP_NABAVKANAZIV=TIP_NABAVKANAZIV'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'PARTNERNAZIV=PARTNERNAZIV'
      'PARTNERADRESA=PARTNERADRESA'
      'PARTNERMESTO=PARTNERMESTO'
      'DATUMPONUDA=DATUMPONUDA')
    DataSource = dsNalog
    BCDToCurrency = False
    Left = 96
    Top = 952
  end
  object frxRichObject1: TfrxRichObject
    Left = 496
    Top = 656
  end
  object tblGenerika: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    NAZIV = :NAZIV,'
      '    MERKA = :MERKA,'
      '    CENA = :CENA,'
      '    KOLICINA = :KOLICINA,'
      '    TARIFA = :TARIFA,'
      '    GRUPA = :GRUPA'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_ARTIKAL'
      'WHERE'
      '        ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_ARTIKAL('
      '    ARTVID,'
      '    ID,'
      '    NAZIV,'
      '    MERKA,'
      '    CENA,'
      '    KOLICINA,'
      '    TARIFA,'
      '    GRUPA'
      ')'
      'VALUES('
      '    :ARTVID,'
      '    :ID,'
      '    :NAZIV,'
      '    :MERKA,'
      '    :CENA,'
      '    :KOLICINA,'
      '    :TARIFA,'
      '    :GRUPA'
      ')')
    RefreshSQL.Strings = (
      'select ma.artvid,'
      '       ma.id,'
      '       ma.naziv,'
      '       ma.merka,'
      '       ma.cena,'
      '       ma.kolicina,'
      '       ma.tarifa,'
      '       ma.grupa,'
      '       mg.naziv as PodgrupaNaziv'
      'from mtr_artikal ma'
      'inner join mtr_artvid mv on ma.artvid= mv.id'
      
        'left outer join mtr_artgrupa mg on ma.grupa= mg.id and mv.id=mg.' +
        'mtr_artvid_id'
      'where(  ma.artvid = :artvid'
      '     ) and (     MA.ARTVID = :OLD_ARTVID'
      '    and MA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ma.artvid,'
      '       ma.id,'
      '       ma.naziv,'
      '       ma.merka,'
      '       ma.cena,'
      '       ma.kolicina,'
      '       ma.tarifa,'
      '       ma.grupa,'
      '       mg.naziv as PodgrupaNaziv'
      'from mtr_artikal ma'
      'inner join mtr_artvid mv on ma.artvid= mv.id'
      
        'left outer join mtr_artgrupa mg on ma.grupa= mg.id and mv.id=mg.' +
        'mtr_artvid_id'
      'where ma.artvid = :artvid')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 136
    Top = 88
    object tblGenerikaARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID'
    end
    object tblGenerikaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblGenerikaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGenerikaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGenerikaCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblGenerikaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblGenerikaTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
    object tblGenerikaGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGenerikaPODGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'PODGRUPANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsGenerika: TDataSource
    DataSet = tblGenerika
    Left = 136
    Top = 32
  end
  object GenArtVid: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_GENARTVID')
    Left = 744
    Top = 432
  end
  object GenerikaID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_GENERIKAID(?ARTVID)')
    Left = 712
    Top = 496
  end
  object tblMapiranje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_GENERIKA_ARTIKAL'
      'SET '
      'WHERE'
      '    VID_GENERIKA = :OLD_VID_GENERIKA'
      '    and GENERIKA = :OLD_GENERIKA'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_GENERIKA_ARTIKAL'
      'WHERE'
      '        VID_GENERIKA = :OLD_VID_GENERIKA'
      '    and GENERIKA = :OLD_GENERIKA'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_GENERIKA_ARTIKAL('
      '    VID_GENERIKA,'
      '    GENERIKA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL'
      ')'
      'VALUES('
      '    :VID_GENERIKA,'
      '    :GENERIKA,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL'
      ')')
    RefreshSQL.Strings = (
      'select mg.vid_generika,'
      '       mg.generika,'
      
        '       (select ma.naziv from mtr_artikal ma where ma.id = mg.gen' +
        'erika and ma.artvid = mg.vid_generika) as opst_naziv,'
      '       mg.vid_artikal,'
      '       mg.artikal,'
      '       ma.naziv'
      'from mtr_generika_artikal mg'
      
        'inner join mtr_artikal ma on ma.artvid = mg.vid_artikal and ma.i' +
        'd = mg.artikal'
      'where(  mg.generika = :generika'
      '     ) and (     MG.VID_GENERIKA = :OLD_VID_GENERIKA'
      '    and MG.GENERIKA = :OLD_GENERIKA'
      '    and MG.VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and MG.ARTIKAL = :OLD_ARTIKAL'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mg.vid_generika,'
      '       mg.generika,'
      
        '       (select ma.naziv from mtr_artikal ma where ma.id = mg.gen' +
        'erika and ma.artvid = mg.vid_generika) as opst_naziv,'
      '       mg.vid_artikal,'
      '       mg.artikal,'
      '       ma.naziv'
      'from mtr_generika_artikal mg'
      
        'inner join mtr_artikal ma on ma.artvid = mg.vid_artikal and ma.i' +
        'd = mg.artikal'
      'where mg.generika = :generika')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 200
    Top = 88
    object tblMapiranjeVID_GENERIKA: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1075#1077#1085#1077#1088#1080#1082#1072
      FieldName = 'VID_GENERIKA'
    end
    object tblMapiranjeGENERIKA: TFIBIntegerField
      DisplayLabel = #1043#1077#1085#1077#1088#1080#1082#1072' (ID)'
      FieldName = 'GENERIKA'
    end
    object tblMapiranjeOPST_NAZIV: TFIBStringField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'OPST_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMapiranjeVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'VID_ARTIKAL'
    end
    object tblMapiranjeARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblMapiranjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsMapiranje: TDataSource
    DataSet = tblMapiranje
    Left = 200
    Top = 32
  end
  object DetailStavkiNabavka: TpFIBDataSet
    SelectSQL.Strings = (
      'select a.artvid as vidartikal_gen,'
      '       a.id as sifraartikal_gen,'
      '       a.naziv opstnazivartikal,'
      '       a1.artvid as vidartikal,'
      '       a1.id as sifraartikal,'
      
        '       (case when a1.naziv is null then a.naziv else a1.naziv en' +
        'd)naziv'
      'from po_nabavka_stavki s'
      ''
      
        'left outer join mtr_generika_artikal ga on ga.vid_generika=s.art' +
        'vid and ga.generika=s.artsif'
      'inner join mtr_artikal a on  a.artvid=s.artvid and a.id=s.artsif'
      
        'left outer join mtr_artikal a1 on a1.artvid=ga.vid_artikal and  ' +
        'a1.id=ga.artikal'
      'where s.nabavka_id like :NabavkaId and a1.artvid is not null'
      'order by a.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 528
    Top = 320
    object DetailStavkiNabavkaVIDARTIKAL_GEN: TFIBIntegerField
      FieldName = 'VIDARTIKAL_GEN'
    end
    object DetailStavkiNabavkaSIFRAARTIKAL_GEN: TFIBIntegerField
      FieldName = 'SIFRAARTIKAL_GEN'
    end
    object DetailStavkiNabavkaOPSTNAZIVARTIKAL: TFIBStringField
      FieldName = 'OPSTNAZIVARTIKAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object DetailStavkiNabavkaVIDARTIKAL: TFIBIntegerField
      FieldName = 'VIDARTIKAL'
    end
    object DetailStavkiNabavkaSIFRAARTIKAL: TFIBIntegerField
      FieldName = 'SIFRAARTIKAL'
    end
    object DetailStavkiNabavkaNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDetailStavkiNabavka: TDataSource
    DataSet = DetailStavkiNabavka
    Left = 528
    Top = 264
  end
  object PromenaStatusStavka: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_PO_PROMENASTAVKASTATUS (?PRIFATENA, ?STAT' +
        'USSTAVKA, ?STATUS, ?BROJ, ?GODINA)')
    StoredProcName = 'PROC_PO_PROMENASTAVKASTATUS'
    Left = 848
    Top = 560
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object CenaDDVStavka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_PREDLOGCENADDV(?ARTSIF, ?ARTVID)')
    Left = 936
    Top = 560
  end
  object GrupaArtikal: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      #9'MTR_ARTVID.ID,'
      #9'MTR_ARTVID.OPIS'
      'from mtr_artvid '
      'where(  MTR_ARTVID.ID <> :artvidID'
      '     ) and (     MTR_ARTVID.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      #9'MTR_ARTVID.ID,'
      #9'MTR_ARTVID.OPIS'
      'from mtr_artvid '
      'where MTR_ARTVID.ID <> :artvidID')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 704
    Top = 192
    object GrupaArtikalID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object GrupaArtikalOPIS: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsGrupaArtikal: TDataSource
    DataSet = GrupaArtikal
    Left = 720
    Top = 136
  end
  object IzbraniSektoriZaPlan: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV'
      'from mat_re'
      'where(   MAT_RE.SPISOK like :spisok'
      
        '       and ('#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and' +
        ' P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mat_re.id || '#39','#39 +
        '  || '#39'%'#39')'
      '     ) and (     MAT_RE.ID = :OLD_ID'
      '     )'
      '    '
      ''
      'union'
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV'
      ''
      'from mat_re'
      
        'where ( (MAT_RE.POTEKLO like :poteklo) or( mat_re.id like :potek' +
        'lo))'
      
        '        and ('#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' an' +
        'd P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mat_re.id || '#39',' +
        #39'  || '#39'%'#39')'
      '')
    SelectSQL.Strings = (
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV'
      'from mat_re'
      'where  MAT_RE.SPISOK like :spisok'
      
        '       and ('#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and' +
        ' P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mat_re.id || '#39','#39 +
        '  || '#39'%'#39')'
      ''
      'union'
      'select'
      '    MAT_RE.ID,'
      '    MAT_RE.NAZIV'
      ''
      'from mat_re'
      
        'where ( (MAT_RE.POTEKLO like :poteklo) or( mat_re.id like :potek' +
        'lo))'
      
        '        and ('#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' an' +
        'd P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mat_re.id || '#39',' +
        #39'  || '#39'%'#39')'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 288
    Top = 88
    object IzbraniSektoriZaPlanID: TFIBIntegerField
      FieldName = 'ID'
    end
    object IzbraniSektoriZaPlanNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzbraniSektoriZaPlan: TDataSource
    DataSet = IzbraniSektoriZaPlan
    Left = 288
    Top = 32
  end
  object SetupPlanSektor: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_SETUP'
      'SET '
      '    P1 = :P1,'
      '    P2 = :P2,'
      '    V1 = :V1'
      'where p1 = '#39'PO'#39' and p2 = '#39'SEKTORI_PLAN'#39
      '    ')
    RefreshSQL.Strings = (
      'select ss.p1, ss.p2, ss.v1'
      'from sys_setup ss'
      'where ss.p1 = '#39'PO'#39' and ss.p2 = '#39'SEKTORI_PLAN'#39)
    SelectSQL.Strings = (
      'select ss.p1, ss.p2, ss.v1'
      'from sys_setup ss'
      'where ss.p1 = '#39'PO'#39' and ss.p2 = '#39'SEKTORI_PLAN'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 392
    Top = 88
    object SetupPlanSektorP1: TFIBStringField
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object SetupPlanSektorP2: TFIBStringField
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object SetupPlanSektorV1: TFIBStringField
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSetupPlanSektor: TDataSource
    DataSet = IzbraniSektoriZaPlan
    Left = 400
    Top = 32
  end
  object SetupGrupaArtikalPlan: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_SETUP'
      'SET '
      '    P1 = :P1,'
      '    P2 = :P2,'
      '    V1 = :V1'
      'where p1 = '#39'PO'#39' and p2 = '#39'GRUPI_PLAN'#39)
    RefreshSQL.Strings = (
      'select ss.p1, ss.p2, ss.v1'
      'from sys_setup ss'
      'where ss.p1 = '#39'PO'#39' and ss.p2 = '#39'GRUPI_PLAN'#39)
    SelectSQL.Strings = (
      'select ss.p1, ss.p2, ss.v1'
      'from sys_setup ss'
      'where ss.p1 = '#39'PO'#39' and ss.p2 = '#39'GRUPI_PLAN'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 504
    Top = 88
    object SetupGrupaArtikalPlanP1: TFIBStringField
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object SetupGrupaArtikalPlanP2: TFIBStringField
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object SetupGrupaArtikalPlanV1: TFIBStringField
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object cxLargeImages: TcxImageList
    SourceDPI = 96
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 76546928
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000D0401112E1001450D040110000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000210B
          012E903001E78F3001FF903101EC511A017B270D013904020106000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002010101752C
          01C6722C0BFF792E06FF942C01FF9C3101FF9A3201FF943001F1762801B64D1A
          0175230D01330301010400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003A16015C9331
          01FE584E49FF646D72FF76706DFF886754FF945535FF983B0BFF992B01FF992E
          01FF9A3201FF903001ED722501B04919016E1D0B012D02010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000903010C8C2E01E4882A
          01FF5B6063FF686969FF747576FF818486FF8F9598FF9AA3A8FF9C9A98FFA17F
          6CFFA4673FFFA94C0DFFA93B01FFA33801FF9C3501FF852F01E96D2401AA4517
          01691D0A01280101010100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000571C0186A43E01FF8429
          01FF555B5FFF676868FF757575FF828282FF929292FF9B9B9CFF9D9FA0FFA0A3
          A6FFA4AAAEFFA7B0B6FFABAAA7FFB39178FFBA7D46FFBE6211FFB94B01FFAC41
          01FFA13601FE902D01E4692001A4411501631306011D00000000000000000000
          00000000000000000000000000000000000019090123953101F7D16B01FF8D2A
          01FF43413DFF5B6266FF707477FF7F8081FF8E8E8FFF979797FF9A9A9AFFA0A0
          A0FFA4A4A4FFA7A8A8FFACAEAEFFAEB1B4FFB2B8BDFFB5BEC5FFBAB9B6FFC5A5
          82FFD0924CFFD37815FFCA5F01FF9F3A01FF6B2501B100000000000000000000
          000000000000000000000000000000000000702301B0B95401FFE37E01FFC75F
          01FF853309FF984B14FF8A5938FF826E62FF84898EFF8A9196FF909497FF9698
          9AFF9B9C9DFFA0A0A1FFA5A5A5FFAAAAAAFFB0B0B0FFB5B6B6FFB9BBBBFFBCC0
          C3FFC0C6CCFFC3CDD6FFC7BDB6FF742001C10A03010E00000000000000000000
          000000000000000000000000000031110146A03A01FEDD7801FFEF8901FFFFA4
          06FFBB5401FFC56619FFBC5401FFB44801FFAC4201FFA1501CFF996644FF937C
          6EFF909396FF959CA1FF9B9FA2FFA0A3A4FFA5A6A7FFAAAAABFFAFAFAFFFB6B6
          B6FFBABABAFFC0C2C2FF9F9793CF0803010B0000000000000000000000000000
          00000000000000000000040201057D2701D4CB6601FFE98301FFFF9A03FFDE7C
          0DFFB75A10FFD0761EFFC66101FFC55F01FFC35C01FFBC5501FFB64C01FFAF43
          01FFA83C01FFA24D18FF9C6544FF9A8070FF9A9D9FFF9FA6ABFFA4A9ADFFA9AC
          AEFFAEAFB0FFB5B5B5FF868888AA000000000000000000000000000000000000
          0000000000000000000048170170AB4601FFE68101FFF99301FFFFA015FFAF4A
          01FFD37E28FFCC6A07FFC86301FFC66101FFC45F01FFC35E01FFC15C01FFC05A
          01FFBF5901FFB85101FFB14801FFAC4001FFA53901FFA14A15FF9F6543FF9F83
          71FFA3A2A2FFA8B1B6FF919699C5000000000000000000000000000000000000
          00000000000010050115913101EEDB7601FFF48D01FFFFA30EFFCD6C0DFFC76D
          1DFFD57916FFCE6901FFCC6701FFCA6501FFC76201FFC56001FFC35E01FFC15C
          01FFBF5A01FFBD5801FFBC5701FFBB5601FFB85201FFB44D01FFAC4401FFA73C
          01FFA33501FFA04512FF9B6141F43D1501580402010600000000000000000000
          00000000000069260199C05B01FFF08901FFFF9B06FFF5981FFFB45106FFDD88
          27FFD47003FFD16C01FFCF6A01FFCC6701FFCB6601FFC96401FFC66101FFC45F
          01FFC25D01FFC05B01FFBE5901FFBC5701FFBA5501FFB85301FFB75201FFB550
          01FFB34D01FFAF4801FFA74001FF9B3701F31709012000000000000000000000
          000027100132A94301FBEB8501FFFC9601FFFFAA23FFBF5B06FFD68128FFDB7C
          0FFFD67101FFD46F01FFD26D01FFD06B01FFCE6901FFCC6701FFCA6501FFC863
          01FFC66101FFC35E01FFC15C01FFBF5A01FFBD5801FFBB5601FFB95401FFB752
          01FFB65101FFB24D01FF9F3B01F7321301450000000000000000000000000201
          01017E3601C2D46F01FFFA9301FFFFA416FFE38720FFC26513FFE58D21FFDC78
          01FFDA7401FFD87201FFD57001FFD36E01FFD16C01FFCF6A01FFCD6801FFCB66
          01FFC96401FFC76201FFC56001FFC25D01FFC05B01FFBE5901FFBC5701FFBB56
          01FFB95401FFA44001FB3F180154000000000000000000000000000000002F16
          0145B85301FEF79001FFFF9F0CFFFFAA32FFB95402FFE6922EFFE3810AFFDE79
          01FFD37B1CFFD88326FFD7770BFFD76E01FFD56D01FFD26A01FFD06A01FFCE69
          01FFCC6701FFCA6501FFC86301FFC66101FFC45F01FFC15C01FFC05B01FFBF5A
          01FFA94301FD4C1D016500000000000000000000000000000000000000006A2E
          0198DA7401FFFF9B04FFFFAC2DFFD57719FFD57D22FFEC8F1AFFE78001FFD16B
          01FFDEA562FFE1A969FFE0AC6FFFE2AA6CFFE19E53FFE09337FFDE841BFFDA74
          01FFD56D01FFCE6701FFCA6401FFC96401FFC76201FFC66101FFC45F01FFAD47
          01FE592301760000000000000000000000000000000000000000000000009241
          01CCF58D01FFFFA61FFFF5A038FFBF5C08FFF29D2DFFEB8705FFDF7701FFD187
          3AFFE3A963FFE1A761FFE2A865FFE3AB6BFFE5AE71FFE4B176FFE6B37DFFE8B6
          81FFE8AC6CFFE9A24EFFE88E1CFFD36D01FFCA6501FFCA6501FFB24C01FE6729
          018800000000000000000000000000000000000000000000000000000000A449
          01E3FFA015FFFFB342FFC46109FFE7922EFFF39413FFF08901FFC36614FFE6B0
          66FFE5AB61FFE5AA62FFE3AB65FFE4AB68FFE4AC6BFFE5AE6FFFE6AF72FFE6B1
          76FFE7B47DFFE6B076FFDA7704FFCF6A01FFCF6A01FFB75201FF7030019A0101
          010100000000000000000000000000000000000000000000000000000000A24B
          01D8FFAB34FFE58E2EFFD07319FFFBA426FFF38E03FFE68101FFC2630EFFC573
          26FFCD813BFFDB9953FFE6AF6AFFE8B16FFFE7B26FFFE8B170FFE7B272FFE7B1
          74FFE6B175FFD87407FFD57001FFD57001FFBC5701FF773301AA030101030000
          0000000000000000000000000000000000000000000000000000000000006C36
          018CD97B18FFC35F01FFEC9526FFFB9A0EFFFB9401FFF89101FFF79001FFF48C
          01FFEA8101FFD97101FFCE6701FFCD6D0FFFCD7928FFD3893FFFDE9F5AFFE6B1
          74FFD37614FFD97401FFDB7601FFC25D01FF823601B805030107000000000000
          0000000000000000000000000000000000000000000000000000000000000603
          01064F28015B773A018C9E4C01BFB35C01EFC86501FFD46F01FFDE7901FFE882
          01FFF38D01FFF48E01FFF28B01FFEF8701FFEC8501FFE47D01FFDA7101FFD069
          01FFDE7801FFE17B01FFCA6501FF8C3D01C60A04010C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000020101011E100123482301556E3401879647
          01B8AA5501E8C15E01FECE6901FFD67201FFDF7A01FFEA8401FFED8701FFE983
          01FFE88201FFD26D01FF954001D20F0701120000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000002010101180C011D411E014E673001808F4001B2B15001E3BF5801FDC863
          01FFCD6801FF9F4701DC160A011A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000015090117391B
          01485E2A01761D0E012200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFFFFF803FFFFF0003FFFF00
          003FFE000003FE000001FC000003FC000007F8000007F0000007F0000003E000
          0003E0000007C0000007C000000F8000001F8000003F8000007F800000FF8000
          01FFC00003FFFE0007FFFFF80FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000803
          010A5E1F01998D2E01E4652201A71D0B012D0201010200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000006421
          0193872B01FF752101FF942D01FF9B3201FE933201E9752801AD4C1A016F210C
          0131040101030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000250D01329E31
          01FB54433AFF676464FF815D4BFF90502EFF993907FF982801FF992B01FF9930
          01FF8F3101EE762701B3501B0175260D01370502010500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020101027F2D01C98C2A
          01FF595D5FFF686B6DFF727679FF80878CFF8E9AA0FF9A9D9FFF9C8172FFA066
          43FFA44A15FFA53401FFA13201FF9E3101FE922C01F27A2701B9521A017B2A0F
          013C070201080000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000471A01699F3901FE8628
          01FF535D61FF696B6BFF767676FF838383FF939494FF9C9D9FFF9DA3A5FFA0A9
          ADFFA5B0B8FFA7B1B5FFAE9988FFB48156FFBA6A22FFB84A01FFAE3F01FFA236
          01FF932D01F57B2601BF561B01812E0F01420803010B00000000000000000000
          000000000000000000000000000000000000120601178D2D01EECD6701FF8D26
          01FF3F3F3FFF596165FF717172FF808080FF909090FF979797FF9C9C9CFFA0A0
          A0FFA4A5A5FFA8AAAAFFACB0B3FFAEB5BBFFB1BCC5FFB5C2C9FFBFB09FFFC99C
          6AFFD28633FFCE6501FFC15301FF9F3801FF7D2B01CD00000000000000000000
          0000000000000000000000000000000000006B2201A3B14D01FFE58001FFC35A
          01FF7E2E0AFF825537FF6F7A81FF787B7CFF868686FF8D8D8DFF929292FF9696
          96FF9B9B9BFFA2A2A2FFA7A7A7FFADADADFFB0B0B0FFB5B7B7FFB9BCC0FFBCC3
          CAFFBFC9D3FFC4D1DEFFCFCABCFF832301D91708012200000000000000000000
          00000000000000000000000000002F1001429C3401FDDC7701FFF08A01FFFFA8
          06FFAF4902FFB94901FF9C5021FF82888EFF84888BFF878888FF8B8B8BFF8E8E
          8EFF909090FF969696FF9B9B9BFFA0A0A0FFA5A5A5FFAAAAAAFFAFAFAFFFB4B4
          B4FFBBBBBBFFC1C2C4FFAEAAA9DC100401160000000000000000000000000000
          00000000000000000000060201077E2801D6C76201FFEB8401FFFF9E03FFCD6C
          0BFFB75201FFEA8301FFC45901FFA34006FF9C9694FF8F969BFF888A8AFF8E8E
          8EFF949494FF919191FF929292FF949494FF989898FF9D9D9DFFA3A3A3FFA8A8
          A8FFADADADFFB3B4B4FF999C9DBE000000000000000000000000000000000000
          00000000000000000000521B017AA94401FFE88201FFFC9601FFF59412FFA23C
          01FFE37E01FFE27D01FFE58001FFD16801FFA53701FFB69B8AFFBCC7CEFFA3A4
          A4FF939393FFAEAEAEFFC1C1C1FFB3B3B3FFA4A4A4FF9B9B9BFF9C9C9CFF9E9E
          9EFFA2A2A2FFA4A4A4FFA0A0A0DA000000000000000000000000000000000000
          0000000000001A090122973401F5DF7A01FFF79001FFFFA711FFAC4905FFD46F
          01FFE68101FFE27D01FFE27D01FFE47F01FFDC7501FFA93701FFB27B59FFD4E0
          E8FFCCCFD1FFA9A9A9FF989898FFC7C7C7FFE5E5E5FFDFDFDFFFD2D2D2FFC4C4
          C4FFB8B8B8FFAFAFAFFFB1B1B1FC2D2D2D330000000000000000000000000000
          0000000000007B2C01B4C45F01FFF48D01FFFFA309FFD27212FFBB5501FFEB85
          01FFE47F01FFE37E01FFE27D01FFE17C01FFE37E01FFE47D01FFB34501FFA956
          23FFD2D7DCFFDCE0E4FFCFD0D0FFA3A3A3FFA8A8A8FFDDDDDDFFE6E6E6FFEAEA
          EAFFF3F3F3FFD4D4D4FF959595D10E0E0E100000000000000000000000000000
          000041190152AF4701FEF08901FFFF9C03FFF59A20FFA74101FFEB8501FFE983
          01FFE68101FFE58001FFE47F01FFE37E01FFE27D01FFE27D01FFE78101FFC45A
          01FFA13B01FFCFC5C0FFDEE7ECFFE2E3E3FFC6C6C6FF9E9E9EFFD0D0D0FFF0F0
          F0FFD6D6D6FF9A9A9ADD1616161A000000000000000000000000000000000A04
          010C983C01E3DF7901FFFE9801FFFFAC20FFB34F08FFDA7501FFED8701FFE983
          01FFE78101FFE68101FFE58001FFE58001FFE47F01FFE37E01FFE37E01FFE781
          01FFD36B01FFA03001FFC6A28DFFE4EFF8FFE6E8E9FFDFDFDFFFA9A9A9FFC0C0
          C0FF9D9D9DE71C1C1C250000000000000000000000000000000000000000471F
          0166C05B01FFFD9501FFFFA717FFD67A1BFFBF5A01FFF28C01FFEB8501FFEA84
          01FFE98301FFE88201FFE78101FFE68101FFE58001FFE47F01FFE37E01FFE27D
          01FFE47F01FFDF7801FFA83701FFB97852FFE8F3FAFFEAEEEFFFECEDEDFFBABA
          BAFF797979B00303030300000000000000000000000000000000000000007B35
          01B1E27C01FFFFA40DFFF8A02EFFAE4701FFF18B01FFF08A01FFED8701FFED87
          01FFE98201FFED8601FFEA8401FFE88201FFE78101FFE68101FFE58001FFE47F
          01FFE47F01FFE58001FFE78001FFB54801FFAA511BFFE8E9EBFFF0F6F9FFF9FA
          FAFFC2C2C2FF6B6B6B8300000000000000000000000000000000000000009B44
          01DAFB9705FFFFB232FFB95709FFDF7901FFF58F01FFEF8901FFEF8901FFED84
          01FFD47B1BFFDB7802FFED8401FFEA8301FFE88201FFE78101FFE68101FFE580
          01FFE47F01FFE37E01FFE37E01FFE78101FFC75D01FF9E3501FFE3D2CAFFE3ED
          F3FFA7A9AAE72D2D2D310000000000000000000000000000000000000000A449
          01E0FFAA26FFDC8124FFC45E01FFF99301FFF28C01FFF28C01FFF48C01FFCF6C
          05FFEBB770FFEAB269FFDC7909FFEB8301FFEB8401FFE98301FFE88201FFE781
          01FFE68101FFE58001FFE47F01FFE37E01FFE78101FFD87001FFA13001FFA074
          5CF0202123270000000000000000000000000000000000000000000000008C42
          01B7E58A25FFB85301FFF89101FFF89201FFF58F01FFF89101FFD26A01FFE4A7
          5CFFECB468FFECB56CFFEFBA74FFDE831DFFE98001FFED8601FFEB8501FFE983
          01FFE88201FFE78101FFE68101FFE58001FFE58001FFE68101FFE47D01FFB043
          01FE6C2301A50A03010B00000000000000000000000000000000000000002613
          012BA14B01D1CC6701FFFF9901FFF99301FFF79101FFE47D01FFCD853EFFF0BA
          6BFFECB263FFECB366FFEEB76CFFF1BF7BFFE29135FFE77C01FFED8501FFEA84
          01FFE98301FFE88201FFE78101FFE68101FFE58001FFE47F01FFE68101FFED87
          01FFAD4901FF5823017300000000000000000000000000000000000000000000
          00000603010678360190C65E01FDF58F01FFFD9701FFFB9301FFD16801FFD790
          45FFEFBA6BFFEDB566FFEDB469FFEEB76EFFF2BF7AFFE8A24FFFE47A01FFEE86
          01FFEC8501FFEA8401FFE98301FFE88201FFE78101FFE88201FFEB8501FFB853
          01FE6E2E01A00502010500000000000000000000000000000000000000000000
          000000000000000000004C230154B45201F0E68101FFFF9A01FFFE9701FFE279
          01FFCA7828FFEFBA6DFFEFB76AFFEEB56AFFF0B86CFFF1BE77FFEBAE63FFE37C
          01FFEE8601FFEB8501FFEA8401FFE98301FFEA8401FFEC8601FFB95001FE6B2D
          0190020101020000000000000000000000000000000000000000000000000000
          000000000000000000000000000022100125994601CFD46F01FFFF9901FFFD96
          01FFF38901FFC7680DFFE9B065FFF1B96CFFEEB569FFEEB66BFFF1BD76FFEDB7
          71FFE37A01FFEE8801FFEC8601FFEE8801FFED8701FFB54E01FE652A017D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000A050109803A019CC86001FEF892
          01FFFE9801FFFB9301FFD16901FFDE9B50FFF1BC6FFFF0BA6DFFEDB66EFFDD7B
          0CFFF08801FFF08A01FFF28C01FFEC8601FFB44C01FC5825016C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000056270160B853
          01F4EB8501FFFF9B01FFFE9701FFE27701FFD38333FFEBB36AFFD77203FFF48C
          01FFF28C01FFF48E01FFEB8501FFB24B01F94B20015B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002914
          012EA14901D8D87301FFFF9B01FFFE9701FFF08801FFD46C01FFF89101FFF690
          01FFF89201FFEA8401FFAE4B01F5401A014B0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000D06010E893F01A8CA6201FEFC9601FFFF9901FFFC9501FFF99301FFFC96
          01FFE88301FFAB4A01EF3417013D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000612C016DBA5501F8EF8901FFFF9901FFFE9801FFE681
          01FFA74701E72B13013000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000032180137A44A01DFDE7901FFE68101FFA147
          01DE210F01260000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000010070113843C01B4994701D3180B
          011C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFC7FFFFFF807FFFFF8007FFFF00007FFE000007FE000001FC00
          0003FC000007F8000007F0000007F0000007E0000007C000000FC000001F8000
          001F8000000F8000001F8000003F8000001FC000000FE000001FF000003FFC00
          007FFE0000FFFF0001FFFFC007FFFFE00FFFFFF01FFFFFFC3FFFFFFE7FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00003412014D872C01DB862C01DB451701711D0B012F03010104000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001F0A
          0129942F01F5643219FF8D3001FF9D2D01FF9B2D01FE963001F1802A01BE5B1E
          01853512014D1106011600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050201058129
          01CB752F0BFF596871FF69757CFF7F7E7FFF90776AFF9A6449FF9A491FFF9C31
          01FF9C2B01FF9C2C01FD902A01DB6D2101A34917016A230C0132050201050000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000581D01849E33
          01FF5F301AFF5D6569FF707171FF818283FF919598FF9DA4A8FF9EA9AEFFA2AF
          B6FFA8A099FFAE8B6FFFB07244FFAF5513FFAA3A01FFA13101FE932801F37C24
          01C05A1C01883713015012060118000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A0E013A9A3401FBC85E
          01FF662209FF434E59FF696C76FF797D84FF8A8F94FF94989AFF9B9D9DFFA2A3
          A3FFA7AAABFFAAB0B4FFAEB7BCFFB2BFC6FFB9C1C6FFC0B09CFFC99D6CFFC57E
          3AFFBF5E09FFA13601FF983301EE070201090000000000000000000000000000
          0000000000000000000000000000000000000903010D822901DCCC6701FFFF95
          01FFCD5204FF834C01FF5E5801FF9C541FFFAA6445FF9C7A6EFF949196FF959E
          A6FF9BA2A8FFA1A7AAFFA7ABADFFAEB0B0FFB5B6B7FFB9BDC0FFBEC5CBFFC3CC
          D4FFCCDFECFF8E4A29D92009012C000000000000000000000000000000000000
          0000000000000000000000000000000000006620019CB24D01FFF18901FFFFA6
          0BFF75760AFF016001FF015F01FF016701FF1F5D01FF535101FF824501FFAD46
          07FFB55830FFAB7159FFA58C86FFA2A7AFFFA6B0B8FFADB6BAFFB3B9BCFFBBBE
          C0FFC3C8CBFE1916141F00000000000000000000000000000000000000000000
          00000000000000000000000000003913014F9D3601FEEA8301FFFF9E05FFCC88
          15FF015E01FF3F5142FF466A46FF2D6E2EFF076C0DFF016301FF016501FF0165
          01FF015E01FF355201FF664401FF943901FFB64413FFB3613FFFAF806EFFB0A2
          9FFFB5C0C7FE1E21222500000000000000000000000000000000000000000000
          00000000000000000000130701178D3001E9D87301FFFF9601FFFF9D1BFF0161
          01FF015304FF6D5F6DFF746F74FF888088FF9E929EFFA79FA7FF829382FF5A8A
          5DFF2E8135FF01750DFF016D01FF016A01FF016101FF195401FF4A4701FF7937
          01FFA53201FF912801CA360F0141000000000000000000000000000000000000
          000000000000000000007C2F01B2C25C01FFFA9201FFFFA818FF3D6705FF016F
          06FF0A4B0BFF5F535FFF6D6A6DFF7D7B7DFF8E8D8EFF9D9B9DFFA4A0A4FFADA6
          ACFFB5ABB5FFBEB0BDFFAEAFB0FF86A88DFF5CA068FF29913DFF01810EFF0174
          01FF016801FF035E01FF0B4A01BB010F011E0000000000000000000000000000
          00000000000050200164B54E01FFF58D01FFFFA50EFF9A7F14FF016B06FF1EAA
          43FF015001FF364632FF5A6A55FF81767EFF938593FF999099FF9C979CFF9F9C
          9FFFA3A2A3FFA9A9A9FFB0AFB0FFBAB5BAFFC1B8C0FFCBBECAFFD2C4D1FFB3C3
          B9FF90C4A1FF308E3BFF015101C7010A01140000000000000000000000000000
          000008030108A04301E9E78101FFFF9F05FFEF9722FF015901FF1EA641FF33CB
          64FF1FAB34FF017E07FF017725FF017D01FF137C03FF387F2EFF5F845BFF8A8E
          88FFA899A7FFACA0ACFFB0A7B0FFB1ADB1FFB5B4B5FFBABABAFFBFBFBFFFC6C3
          C5FFD9CDD8FF3459349D01030107000000000000000000000000000000000000
          000031160144C05B01FFFF9C01FFFFA926FF0E5D01FF0D922DFF2DC05BFF3CDA
          68FF047E44FF0160ADFF0170B5FF0166A2FF01726DFF01773FFF017C11FF017D
          01FF017501FF207B13FF4A8443FF789275FFA7A4A6FFC0B1C0FFC2B8C2FFC5BE
          C5FFCCC6CCFF2827282900000000000000000000000000000000000000000000
          00004F24016CDB7301FFFFAF20FF62700BFF017812FF2CBE59FF3DD96FFF149B
          35FF015DA1FF375E6DFF418196FF2890B5FF0588C1FF017DC3FF016DB7FF0161
          A8FF016D84FF017358FF01772AFF017B01FF017301FF037001FF307F26FF6090
          5BFF96A794FF3135314600000000000000000000000000000000000000000000
          00004822015FE5780AFFC38E1FFF016801FF27B954FF39D26CFF2ABE3FFF0169
          94FF015B8BFF6C5D54FF736B68FF857A75FF9B8A85FF9E9C9AFF7AA2AEFF51A6
          C0FF279AC6FF018AC9FF017AC1FF0167B2FF016499FF016F71FF017242FF0175
          13FF017501FF016801FA012E015B000000000000000000000000000000000000
          00000E06010EA34B01C5016901FF1DAA43FF35CD68FF3BD55BFF01726CFF0178
          B7FF03547AFF5D4C46FF6E6867FF7C7979FF8C8B8AFF9D9A99FFA59E9CFFACA2
          9FFFB6A7A3FFBFABA7FFA7B2B3FF7BB7C6FF4CB2D3FF1C9FD1FF0188CCFF0175
          BDFF0160ABFF015D80FD0140679F012639520000000000000000000000000000
          000000000000015A01B110992AFF33CB68FF43E071FF088745FF0177BCFF01B7
          EDFF016B9DFF2B4551FF476A78FF727271FF92817CFF9D918DFF9C9492FF9F9A
          99FFA3A2A1FFA8A6A6FFAFABAAFFB9B1AFFFC2B6B3FFCCBCB7FFD1C1BCFFABC7
          CDFF79C9DEFF4EBEE0FF016BA2FE014467980000000000000000000000000000
          000001270147058708FE34C968FF43DE76FF1CA83AFF016DAFFF01A8DEFF01CC
          FFFF20CCF5FF0179B1FF018FC7FF017CB4FF0173A5FF23769CFF487E95FF7187
          92FF9B9492FFAC9D98FFAEA3A0FFB0A9A6FFB2AEACFFB6B6B5FFBBBAB9FFC5C1
          BFFFD0C5C2FFD6CBC7F801355065000000000000000000000000000000000000
          0000015101A31FAE3FFF3FDA75FF32C848FF016E92FF0197D0FF01C0F4FF1DDF
          FFFF0B85B4FF15A5D2FF04B6E8FF01B4E7FF01ADE1FF019FD4FF018FC5FF0181
          B8FF0170A9FF056F9FFF2F7A9CFF59879CFF86969FFFB3AAA5FFBFB2ACFFC2B9
          B4FFC4BDBAFFB5ADAACC00000000000000000000000000000000000000000000
          0000017001D938D16CFF45DE65FF017C6DFF0187C6FF01B6EBFF0BD7FFFF17A1
          CDFF0F8FC0FF0CBAEBFF01B0E4FF01AFE3FF01AEE1FF01AEE1FF01AEE1FF01AF
          E2FF01B0E3FF01A6DAFF0197CCFF0187BDFF0176AEFF0167A0FF106F9DFF3D7F
          9EFF6D92A6FF9DA2A3E701030505000000000000000000000000000000000000
          0000017701DF4CE577FF0A9347FF017CC2FF01AEE3FF01CCFFFF1EC0E8FF0880
          B1FF1CC1EEFF01B2E5FF01B1E4FF01B0E3FF01AFE2FF01AEE1FF01ADE0FF01AC
          DFFF01ACDFFF01AADDFF01ABDEFF01ABDEFF01ACDFFF01ABDEFF019ED3FF0190
          C6FF0181B7FF0172A8FF016BA0F30110161C0000000000000000000000000000
          00000149017D019301FD017ABBFF01A1D8FF01C2F6FF1BD2FDFF027CB0FF28C0
          EAFF02B7EAFF01B4E7FF01B3E6FF01B2E5FF01B1E4FF01B0E3FF01AEE1FF01AD
          E0FF01ACDFFF01ABDEFF01AADDFF01A9DCFF01A8DBFF01A8DBFF01A7DAFF01AA
          DDFF01A2D5FF0171A4F901334B63000000000000000000000000000000000000
          000001030404016B9DD20196CDFF01B9EEFF10D7FFFF0488BBFF2AB5E0FF0FBF
          F0FF01B6EAFF01B3E7FF01B3E7FF01B2E6FF01B2E5FF01B1E4FF01B0E3FF01AF
          E2FF01AEE1FF01ADE0FF01ACDFFF01AADDFF01A9DCFF01A8DBFF01ADE0FF0199
          CCFF01699BEE0124344400000000000000000000000000000000000000000000
          0000012D3D54018ABDFF01B1E4FF05D2FFFF0BA0CFFF22A3CFFF20C9F5FF01B9
          EEFF14B5E3FF6DCCE6FF57C7E7FF40C4E9FF29C0EBFF0EBCEBFF01B5E9FF01B2
          E7FF01AEE4FF01ABE0FF01ACE0FF01ACDFFF01ACDFFF01B0E3FF0192C5FF0165
          91DE0118212A0000000000000000000000000000000000000000000000000000
          00000152709B019FD2FF01C9FCFF0EB9E7FF1391C1FF34D0F8FF01C0F4FF01AE
          E2FF61C8E5FF64CAE6FF68CCE8FF6ECFE9FF75D1EBFF7DD4EEFF82D6EFFF6FD4
          F0FF55D1F3FF3DCCF3FF01BDF1FF01AEE1FF01B3E6FF018DC0FF015E85C7010D
          1317000000000000000000000000000000000000000000000000000000000000
          0000016286B701B5E9FF0BCBFAFF0789BBFF42D0F6FF10C6F6FF01AEE3FF41B9
          DCFF69CCE5FF6BCEE8FF6CCEE9FF6DD0EBFF6FD1ECFF72D2ECFF74D2EFFF7BD7
          F0FF8AD9F0FF26C4EEFF01B2E6FF01B3E6FF0187BCFF015177AB010608090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000014F6A8E02A8DBFF0193C6FF44C2E9FF23D4FFFF01C7FBFF01C2F5FF01B8
          ECFF01B0E5FF07B1E3FF20B4E0FF36BAE3FF51C4E7FF6DCFEAFF7DD6EFFF81D7
          EEFF19BCEAFF01B3E8FF01B2E6FF0186BAFE0149698901020202000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001090C0D014C6777016D94A70184B1D3018DC2F70195C8FF019ED1FF01A9
          DCFF01B2E5FF01BDF0FF01C3F7FF01C1F6FF01B9EEFF01B4E9FF01B4E6FF08B7
          E6FF01B8ECFF01B1E4FF0180B4FA013D55660000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001050606011B242901364954014E
          6C7E01678EA9017DACD40186BAF7018FC2FE0199CCFF01A3D6FF01ACDFFF01B6
          EAFF01ADE0FF017CADEF012A3D46000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000010506070118202A012F405401435D8001597BAA016D
          98D4016F9BD8011A242C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFE3FFFFFFC03FFFFF8001FFFF00000FFF000007FE00000FFC00
          001FFC00001FF800000FF0000007E0000007E000000FE000001FC000001FC000
          0007E0000003E0000003E0000007C000000FC000000FC0000007C0000007E000
          001FC000003FC000007FC00000FFC00001FFE00003FFFF000FFFFFFC1FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000E0E0E1B3234356F1414142939393984646464E4363636780101
          0101000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001C1D
          1D30515559A1867E74F8AD814DFF858282FEA1A5A8FFCBCBCBFF9B9B9BFF4E4E
          4EB2090909120000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000020202022B2B2C4A64676CBB9A8B
          79FDCD9A59FFEAA345FFE2973CFFD5740BFF9C8B78FFC7C9CBFFCFCFCFFFB1B1
          B1FF626262DE1919193400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000070707093A3B3D65717578D4AB967BFEDEA661FFEEA9
          53FFE7A551FFE3A14CFFE2A357FFD47308FF997044FFCBCFD3FFC6C6C6FFC9C9
          C9FFC1C1C1FF787878F731313169000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000F0F0F164B4D4F7F868786E8BDA080FFECB16AFFF1B162FFEBAC5FFFE7A9
          5AFFE5A656FFE4A44FFFE6AF6DFFD67B11FF9A764CFFD2D5DAFFC4C4C4FFC5C5
          C5FFC5C5C5FFC9C9C9FF919191FE464646A50707070C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000003333
          344F9D9790F6CFAD84FFF6BA74FFF4B76FFFEEB46CFFEDB068FFEAAE63FFE9AB
          5FFFE7A95AFFE5A553FFEABC82FFDA8017FF99774FFFD5D9DDFFC3C3C3FFC2C2
          C2FFC1C1C1FFC2C2C2FFC8C8C8FFA6A6A6FF595959D41414142A000000000000
          0000000000000000000000000000000000000000000000000000000000004C4E
          5081E8B886FFFABE7DFFF3BB79FFF2B775FFEFB570FFEDB26CFFECB068FFEAAE
          63FFE9AA5CFFE7A553FFEFC895FFDC871EFF977853FFD7DBDFFFC0C0C0FFC0C0
          C0FFC0C0C0FFBFBFBFFFC0C0C0FFC2C2C2FFB7B7B7FF707070F2262626530000
          0000000000000000000000000000000000000000000000000000000000005759
          5C93EDBC8AFFF8C081FFF4BC7EFFF2BA79FFF0B774FFEFB56FFFEEB26BFFEFB2
          67FFF8CA8EFFF2CE9DFFCDAD8CFFF5B666FF9A7A51FFDBDEE2FFBEBEBEFFBFBF
          BFFFBEBEBEFFBDBDBDFFBEBEBEFFBBBBBBFFC0C0C0FFC4C4C4FF5C5C5CCB0000
          0000000000000000000000000000000000000000000000000000000000006266
          69A6F7C28BFFF8C186FFF5BE81FFF4BC7DFFF4BB7AFFF8C588FFFAD39FFFD3A8
          81FF965C34FF5F2307FF4C1C06FF623E20FF948A7EFFE0E1E3FFBDBDBDFFBDBD
          BDFFBCBCBCFFBDBDBDFFB8B8B8FFADADADFFB3B3B3FFBFBFBFFF6A6A6ADB0000
          0000000000000000000000000000000000000000000000000000000000007073
          77B8FFC78EFFFBC48BFFFAC68CFFFDD19EFFE9B683FFBC713FFF882B01FF731F
          01FF56240CFF6D5B54FF9EA0A4FFDDE0E3FFE9EAECFFDEDFDFFFBABABAFFBBBB
          BBFFBCBCBCFFBDBDBDFFACACACFF9E9E9EFFA5A5A5FFB1B1B1FF7A7A7AEA0000
          0000000000000000000000000000000000000000000000000000000000008082
          83CAFFD097FFF8C58DFFE09E61FFBB5F17FF9F3501FF732401FF785748FF9E9F
          A0FFDDE1E4FFEDEFF0FFE9EAEAFFE6E6E6FFE7E7E7FFE3E3E3FFB7B7B7FFBABA
          BAFFBBBBBBFFA7A7A7FF818181FF848484FF7E7E7EFF696969FF7F7F7FF90101
          010100000000000000000000000000000000000000000000000000000000797A
          7AABC6B6A7FEE1AB70FF9B5A19FF88674CFF9C9E9EFFD8E0E4FFEDF1F2FFEBEC
          EDFFE8E9EAFFE9EBEEFFECF0F3FFEEF0F1FFECECECFFEDEDEDFFB5B5B5FFB8B8
          B8FFBDBDBDFF6E6E6EFF989898FF939393FF878787FF6B6B6BFF707070FE0606
          060A000000000000000000000000000000000000000000000000000000000101
          01018F9192C9DADEE3FFD1D6DCFFEDF1F4FFECEDEEFFEAEBEBFFEBEFF1FFEFF4
          F7FFEEF3F7FFDEC9B2FFAD7456FFC4BDBAFFF4F7F8FFFBFBFBFFB3B3B3FFB6B6
          B6FFBCBCBCFF555555FFC6C6C6FFA0A0A0FF848484FF707070FF7A7B7BFF0D0D
          0D1A000000000101010106010108000000000000000000000000000000000000
          00009C9C9CCCEDEEEEFFEBEDEFFFEEF2F6FFF1F6F9FFF0F2F2FFCDAA91FF9B6B
          53FFE3CBABFFEEAC59FFDA8C38FF8C3A01FFF8FFFFFFFEFEFEFFC3C3C3FFB4B4
          B4FFB9B9B9FF757575FF848484FFACACACFF7C7C7CFF767777FF8F9799FF2116
          1344510C01937D1D02E8892A04FE761F01CE1305011F00000000000000000000
          0000A3A3A3D0F3F4F6FFEFEBE6FFC2916FFF96674EFFEDC188FFEDAC5AFFBE5B
          07FFCF8E4FFFF3B872FFEEB367FFC35B01FFDBE1E7FFFFFFFFFFDADADAFFB0B0
          B0FFB3B3B3FFB5B5B5FFA7A8A9FFA9B0B2FF7C7D80FF869095FF957067FF8421
          01FB92420AFFA6560AFFB55E05FFB05809FF652101AC00000000000000000000
          00009D9D9DCBF9FDFFFFEFBC7DFFECAB57FFB95001FFEFB270FFF1B670FFE094
          3DFFC38140FFF7BC7AFFEFB165FFD58D42FFF9FFFFFFFFFFFFFFF9F9F9FFABAB
          ABFFB1B1B1FFB1B1B2FFB9BFC2FF9F461DFF812D01FF973705FF9D4001FFB45A
          02FFC86401FFD16E09FFD87D1FFFE4923DFFA85217F000000000000000000000
          0000898989B2F9FDFFFFF4C286FFEEB46BFFDB8528FFD69B5FFFF4B873FFEAA0
          4FFFE2D1C1FFFFF4E8FFFBEAD7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF
          BFFFADADADFFB5B6B7FFBAA9A5FFAD4101FFA3560BFFCF6601FFD3700CFFD97D
          20FFDE8B38FFE69B50FFEFAC6BFFFDC58CFFB26726EA00000000000000000000
          00006A6A6A86EAEDEFFFFEECD8FFF3B46EFFEDAE6DFFF0EFEFFFFFF9F0FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F8F7FFE0D7CEFFFFFFFFFFFFFFFFFFEDED
          EDFFA7A7A7FFBABBBBFFD2D0CEFFBE5301FFBC7122FFE48D36FFDA8738FFE398
          50FFF3B77AFFFDCA96FFFBC691FFE29F56FE6940177900000000000000000000
          000035353540D2D2D2FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE4C3A2FFD8CBBDFFFFFFFFFFE9A45AFFD5730BFFA46225FFF3F9FFFFFEFE
          FEFFC5C5C5FFA8A8A9FFEEF4F9FFC98E59FFD28538FFC18D5AFFA59281FF7D58
          32BA8E571CA0A36825B49261249F4D34144E0000000000000000000000000000
          000002020202A5A5A5D6FCFCFCFFFFFFFFFFFDF6F0FFD29150FFC19E7EFFF5E0
          C8FFE89E48FFCB6401FFB58155FFF6BA73FFEFB468FFC76811FFBCA497FFF2F4
          F5FFF6F6F6FFB3B3B3FFA2A3A4FFA5A9ADFFAAA6A2FFA6ABB0FF9FA2A6FF3F41
          4484000000000000000000000000000000000000000000000000000000000000
          00000000000047474751D3D3D3FEFFFFFFFFF6D1A4FFEDAD5EFFD07417FFB26E
          33FFF6BA73FFF0B062FFA25015FFF8D6AFFFF3B468FFDF9B51FFE2E1E2FFEAEA
          EBFFE8E8E8FFF3F3F3FFCBCBCBFFA6A6A6FFA6A6A7FFA7A7A7FFA2A2A2FF4B4B
          4B94000000000000000000000000000000000000000000000000000000000000
          0000000000000000000079797995DBDCDEFEF7EEE2FFF3B871FFEFAF5DFFC297
          7AFFF3DEC4FFF0C085FFE0D0C2FFEAF0F6FFE8E7E4FFE9ECEEFFE1E3E6FFD8D8
          D8FFCBCBCCFFC0C0C0FFB1B1B1FFA4A4A4FFA4A4A4FFA4A4A4FFA3A3A3FF5353
          53A3000000000000000000000000000000000000000000000000000000000000
          00000000000000000000030303037B7B7B98D2D4D7FEECEAE8FFEBDCCAFFEAEF
          F5FFEAEDF2FFE8EBF1FFE5E6E9FFDDDDDEFFD6D6D7FFD1D2D2FFCBCBCBFFB5B5
          B5FF9D9D9DFFC7C7C7FFADADADFFA4A4A4FFA3A3A3FFA2A2A2FFA5A5A5FF5A5A
          5AB2000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004A4A4A57ABACACE9ECEDF2FFC8C8
          C8FFB7B7B7FFA1A1A1FF8C8C8CFF797979FF616161FF454545FF323232FF2F2F
          2FFF242424FF8C8C8CFFB7B7B9FFA7A7A7FFA1A1A1FFA0A0A0FFA4A4A4FF6464
          64C2000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000008F8F8FB8DBDBDBFF6262
          62FF5C5C5CFF5A5A5AFF575757FF555555FF515151FF4F4F4FFF4C4C4CFF4747
          47FF3D3D3DFF838383FFC0C0C0FFACACACFF9E9E9EFF9F9F9FFFA1A1A1FF6E6E
          6ED1000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000959595BAD4D4D4FF6B6B
          6BFF737373FF6D6D6DFF696969FF656565FF616161FF5D5D5DFF585858FF5454
          54FF4A4A4AFF767676FFC4C4C4FFB3B3B3FF9D9D9DFF9D9D9DFFA0A0A0FF7878
          78E0000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000999999BFCBCBCBFF7474
          74FF818181FF7B7B7BFF747474FF6C6C6CFF646464FF5E5E5EFF5C5C5CFF5F5F
          5FFF6A6A6AFF828282FFC4C4C4FFB6B6B6FF969696FF999999FF9C9C9CFF7F7F
          7FEE010101010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000009B9B9BC1C6C6C6FF4E4E
          4EFF6B6B6BFF7A7A7AFF8B8B8BFFA0A0A0FFB4B4B4FFC8C8C8FFDADADAFFEAEA
          EAFFF1F1F1FFF6F6F6FFFDFDFDFFFDFDFDFFE4E4E5FFC7C7C7FFACACACFF7E7E
          7EF9030303050000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000009A9A9AC3E7E7E7FFE6E6
          E6FFE8E8E8FFEAEAEAFFEAEAEAFFE9E9E9FFE9E9E9FFE8E8E8FFE3E3E3FFDDDD
          DDFFD5D5D5FFCCCCCCFFC2C2C2FFB7B7B7FFAEAEAEFCA1A1A1EE8B8B8BD76060
          60B6030303040000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001919191F3A3A3A545151
          51786767679D7F7F7FC19A9A9AE5939393DF7E7E7EC8696969B0595959984A4A
          4A803A3A3A682C2C2C501E1E1E38121212200606060A01010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFF47FFFFFC03FFFFF001FFFF80007FFE00003FFC00001FF8000
          007F8000007F8000007F8000007F8000007F8000007FC000007FC0000043C000
          0001C0000001C0000001C0000001E0000007E000003FE000003FF000003FF800
          003FFC00003FFE00003FFE00003FFE00003FFE00003FFE00003FFF000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000E0B080D80604184B0691FCFAB651FC8A25F1DBE995B1CB39156
          1BA887511A9E7E4C1993764617886B40167D633B15735A3714685232125D472C
          11523F270F4836220D3D2D1C0C3224170A281A11071D110B05130A06030A0403
          0203000000000000000000000000000000000000000000000000000000008960
          64C2589065C48BC676E9FFC28FFFD57D23FED87F25FFD87F26FFD88026FFD980
          27FFD98127FFD98128FFDA8129FFDA822CFFDA832CFFDB842FFFDC852FFFDC86
          31FFDD8733FFDE8A35FFDE8A37FFDF8B39FFE18E3CFFE08F40FEDE8F3FFCE091
          41F8ED9A47F1E69947E6DB9347DBD18D46D181592C8000000000000000009367
          6EEE30F47BFF9AD889FFFFD1A5FFD57D20FFD77D23FFD77E23FFD77E23FFD87E
          24FFD87F25FFD88026FFD98027FFD98128FFDA812AFFDA822BFFDB832DFFDB85
          2FFFDC8631FFDD8733FFDE8935FFDE8A39FFDF8C3AFFE08D3CFFE18F3EFFE190
          41FFE29243FFE39446FFE49548FFE5974CFF976432A600000000000000009365
          6DEA37F27EFF9ED98AFFFFD5ACFFD1781AFFD67C20FFD67C20FFD67C21FFD77C
          21FFD77D22FFD77E23FFD87F25FFD88026FFD98128FFD98129FFDA822BFFDB83
          2DFFDC852FFFDA8631FFDB8631FFDF8936FFDF8B38FFDF8C3AFFE08E3DFFE18F
          3FFFE29142FFE39344FFE49447FFE49649FF986431A900000000000000009265
          6DE839F681FF9DD98BFFFFD5ABFFCF7615FFD5791AFFD57A1CFFD57A1DFFD678
          1BFFD6791DFFD67C21FFD77D22FFD87E24FFD87F26FFD98127FFDA8129FFE381
          2BFFE2812DFFD87E22FFD8852DFFDD8834FFDE8A37FFDF8B39FFE08D3BFFE08E
          3EFFE19040FFE29243FFE39346FFE49548FF996632AA00000000000000008D65
          6CE43DF882FF9FDE8FFFFFD2A8FFDB812AFFE1822DFFDD7C23FFD57313FFE39E
          54FFD68731FFD47414FFD67B1DFFD77D22FFD77E23FFDA7F25FFE97E26FFBE8A
          2EFF19BC43FFE9893AFFE79246FFDD8C39FFDD8934FFDE8A38FFDF8C3AFFE08E
          3DFFE18F3FFFE29142FFE39344FFE49447FF9C6731AE00000000000000008D65
          6BE145FC88FFA1D888FFEBA35DFFCEA152FFB39A42FF98942FFFFEBC84FFFFCA
          9EFFFFC795FFEAB06EFFD7852FFFD47517FFDA7B1EFFEC7821FF96932FFF0CBE
          41FF0EBD42FF74AE4AFFFCA260FFEAA864FFDC842EFFDE8936FFDF8B39FFE08D
          3BFFE18F3EFFE19041FFE29243FFE39446FF9C6632AE00000000000000008A62
          6DDE51FF8FFF2FCF5CFF01AA20FF01AF29FF03B12BFF02B12CFF01AF28FF6AB9
          55FFECC388FFFFBE8EFFFFB881FFF4A260FFF2802EFF699D2EFF08BC3DFF1CB7
          3EFF1DB73FFF09BB3FFFE7B372FFFFBC85FFE7A25BFFDC862FFFDE8A38FFDF8C
          3AFFE08E3DFFE19040FFE29142FFE39345FFA16932B400000000000000008A62
          6BDB56FF93FF3BCF5EFF10A922FF14AC29FF14AC29FF13AC29FF12AD26FF09AD
          27FF01AD24FF45B140FFCAB269FFA6AF59FF24B43AFF0CB536FF1BB538FF1DB5
          3AFF1EB63DFF16B73DFF4BBB51FFFFCDA2FFFFD3A2FFDD8A36FFDE8A35FFDF8B
          39FFE08D3CFFE18F3FFFE29142FFE39344FFA16932B400000000000000008861
          6AD859FF96FF3DD160FF0DA61EFF11AA24FF0BA71EFF1EB63AFF23BB45FF12AA
          25FF14AD2AFF0CAD2AFF04AE28FF08AF2CFF13B031FF19B134FF1AB236FF1CB4
          38FF1DB53BFF1DB63DFF08B638FFC7C784FFFFD1A4FFF4BC81FFDB822BFFDF8B
          39FFE08D3BFFE18F3EFFE19041FFE29244FFA66B33B800000000000000008461
          6BD55DFF9AFF3AD05DFF09A418FF01A410FF50BD5DFF33D870FF3FD876FF15AD
          28FF12AA25FF14AC29FF15AD2BFF16AE2DFF17AF2FFF18B031FF19B134FF1BB3
          36FF1CB439FF1DB53BFF19B73CFF23B943FFFFCEA4FFFFD4A5FFE29A4DFFDE89
          34FFDF8C3BFFE08E3EFFE19040FFE29243FFA66D32BA00000000000000008361
          68D261FF9CFF38D159FF019A01FF83C784FFFFEFFFFF7EDE9AFF39DB71FF3AD2
          68FF0CA41BFF13AB26FF14AC28FF15AD2BFF16AE2DFF17AF2FFF18B032FF1AB2
          34FF1BB337FF1CB439FF1EB63CFF0DB739FF96C271FFFFD2A8FFFDC993FFDE89
          34FFDF8A36FFE08E3DFFE19040FFE29244FFA86E32BD00000000000000008061
          68CF58FF99FF23CE4AFFA7CFA2FFFFEDFCFFFBE8EDFFFEE8EEFF41DF77FF4AE6
          81FF27BF44FF0FA71EFF13AB26FF14AC28FF15AD2BFF16AE2DFF17AF30FF19B1
          33FF1AB235FF1BB338FF1DB53BFF1BB63EFF12B93CFFCDC07CFFFFB882FFFFB8
          7EFFECA059FFE08D3AFFE18E3DFFE29142FFAB7033C000000000000000007760
          64CCBAF7CBFFF1ECE9FFFFECF5FFF7EAECFFF5E8E8FFFFEAF0FFC2DFC5FF3CE7
          77FF53EC88FF11A921FF10A822FF13AB26FF14AC29FF15AD2BFF17AF2EFF16AF
          2EFF1AB033FF1AB334FF1CB439FF1EB63CFF1BB73FFF0CB93CFF33BB4AFFBABB
          70FFFFB680FFFBAE73FFE89A50FFE29140FFAC6F34C10000000000000000745F
          60C9F7E7EBFFFFF2F3FFFBF0F0FFFAF0F0FFFAF0F0FFFDF1F2FFF2D6E2FF7BE2
          9CFF50F187FF48E074FF09A116FF12AA24FF11AB26FF08AC21FF0EAF26FF24C4
          50FF3AD270FF26BB45FF1BB335FF1DB53BFF1FB73EFF20B841FF1CBA44FF0EBC
          43FF33BD4FFFB5B467FFFFAA6FFFF4A15EFFB47739C800000000000000007262
          62C6C0B8B8FFC4BDBDFFC4BBBBFFC4BDBDFFC6BDBDFFC8C1C1FFB7ABAEFFC4B2
          B7FF54F288FF5EFB95FF26C142FF01A10EFF01A916FF56A75CFFE2C4D0FF6BCF
          8CFF2ED167FF3FD875FF1FB53BFF1DB538FF1EB63DFF20B840FF21B943FF23BB
          46FF1EBC49FF13C04BFF32BF52FF6B5D348719120A1800000000000000006F5F
          5FC3F0E8E8FFFFF6F6FFFCF3F3FFFCF3F3FFFCF1F1FFFDF4F4FFE4D9D9FFF3E1
          E7FFCDF1D4FF46FD84FF69FB99FF6EC172FFFFEDF3FFDCC4CFFFFFEEF3FFFFE6
          F0FF4CDA7CFF3DDB74FF46DE77FF19B134FF1EB63BFF1FB73FFF21B942FF22BA
          46FF24BC49FF26BE4CFF21C04FFF083F19480000000000000000000000006C60
          60C0EDE3E3FFFAF1F1FFF6EEEEFFF6EEEEFFF6EDEDFFF9F0F0FFDFD6D6FFE9DF
          DFFFFFECF2FFFDEBF0FFFFEAF5FFFFECF5FFFCEEF1FFCCC0C1FFF7EAEBFFF9E6
          EAFFEDE4E2FF3DE175FF4CE781FF44DC71FF18B033FF1FB73FFF21B942FF22BA
          45FF24BC48FF25BD4BFF27BF4EFF0E3F1B490000000000000000000000006A5E
          5EBDEBE3E3FFFAF2F2FFF7EFEFFFF7EFEFFFF7EFEFFFF9F1F1FFE0D8D8FFE8E0
          E0FFF7EEEEFFF6EBEBFFF6EBEBFFF4EAEAFFF9EDEDFFCBC1C1FFF8ECECFFF2E6
          E7FFFDE5ECFFCBE4CCFF3EEA7AFF58F38EFF3DD667FF1AB337FF20B841FF22BA
          45FF24BC48FF25BD4BFF27BF4EFF0F441C4E0000000000000000000000006A5D
          5DBAE9E2E2FFFBF5F5FFF8F1F1FFF8F0F0FFF7F0F0FFFAF2F2FFE1DBDBFFE9E1
          E1FFF8EFEFFFF6ECECFFF5EBEBFFF5EBEBFFF8EFEFFFCCC2C2FFF7EBEBFFF3E6
          E6FFF3E5E6FFFFE4EDFFA2EAB5FF49F885FF55F58CFF27C451FF20BA41FF1FB7
          3FFF21B944FF25BD4AFF27BF4EFF0F441C4F000000000000000000000000685F
          5FB7EAE3E3FFFCF6F6FFF8F3F3FFF9F3F3FFF9F2F2FFFCF5F5FFE4DDDDFFE9E2
          E2FFFBF1F1FFF8EEEEFFF8EDEDFFF7ECECFFFBF0F0FFCFC6C6FFFAEEEEFFF5E9
          E9FFF6E8E8FFF6E7E8FFFFE7EFFFBBD2BBFF85CE98FF5CD684FF31D96DFF30D9
          6AFF2ED261FF28CA56FF21C24DFF0D481E53000000000000000000000000675E
          5EB4E9E2E2FFFCF8F8FFF8F1F1FFF6F0F0FFF5EFEFFFF6EFEFFFDED8D8FFE1DB
          DBFFF0E9E9FFEDE4E4FFEBE3E3FFEAE1E1FFEDE4E4FFC3BBBBFFEADFDFFFE6DB
          DBFFE5D8D8FFE3D7D8FFE5D6D7FFD3C2C6FFDDC5CCFFF3D3DEFFEFD0D8FFBFCD
          BBFF8CCF9DFF75D08FFF628763C8061B0D1A000000000000000000000000655E
          5EB1C2BCBCFFCECACAFFCDCACAFFD1CBCBFFD2CDCDFFD6D1D1FFC2BEBEFFCAC5
          C5FFDAD4D4FFD9D3D3FFDBD3D3FFDDD5D5FFE1D8D8FFBBB3B3FFE3D9D9FFDFD6
          D6FFE1D7D7FFE1D7D7FFE4D9D9FFD2C5C5FFD5C9C9FFE6D9D9FFE6D6D6FFECD6
          D9FFF4D8DEFFE1BEC7FF754D53AE00000000000000000000000000000000625C
          5CAEEAE4E4FFFFFFFFFFFFFCFCFFFFFAFAFFFFFAFAFFFFFBFBFFE8E1E1FFEEE8
          E8FFFEF7F7FFFAF3F3FFFAF2F2FFF9F1F1FFFDF3F3FFD2C9C9FFFCF1F1FFF7EC
          ECFFF5EBEBFFF6E8E8FFF6E9E9FFE1D4D4FFE3D5D5FFF3E4E4FFF2E1E1FFF0E0
          E0FFF4E4E4FFDCC7C7FF715353AE00000000000000000000000000000000615B
          5BABE5E0E0FFFFFBFBFFFBF7F7FFFAF6F6FFFAF6F6FFFDF8F8FFE6E1E1FFEBE6
          E6FFFAF5F5FFF8F1F1FFF7EFEFFFF7EEEEFFFAF2F2FFCFC8C8FFF9F0F0FFF4E9
          E9FFF4E8E8FFF3E7E7FFF4E9E9FFDFD4D4FFE2D6D6FFF4E5E5FFF0E0E0FFEFDF
          DFFFF2E2E2FFDDC6C6FF705353AE000000000000000000000000000000005F5B
          5BA9E5E1E1FFFFFEFEFFFBF8F8FFFBF8F8FFFBF7F7FFFDF9F9FFE8E2E2FFEBE6
          E6FFFAF4F4FFF8F1F1FFF7F0F0FFF7EFEFFFFAF1F1FFCFCACAFFF9EFEFFFF4EA
          EAFFF4E8E8FFF3E7E7FFF6E8E8FFDFD3D3FFE2D5D5FFF3E4E4FFF0E1E1FFEFDF
          DFFFF2E2E2FFDDC8C8FF705353AE000000000000000000000000000000005E5B
          5BA5E4E1E1FFFFFEFEFFFCF9F9FFFBF9F9FFFBF8F8FFFCF8F8FFE7E3E3FFEAE5
          E5FFFCF6F6FFF8F2F2FFF8F0F0FFF7EFEFFFFAF3F3FFD1C9C9FFF9EFEFFFF5EA
          EAFFF4E9E9FFF3E7E7FFF6E8E8FFE1D3D3FFE4D5D5FFF3E4E4FFF0E1E1FFF0E0
          E0FFF2E2E2FFDDC8C8FF705353AE000000000000000000000000000000005B5A
          5AA3E3E2E2FFFFFFFFFFFEFDFDFFFFFCFCFFFEFBFBFFFFFDFDFFEBE7E7FFEFEA
          EAFFFFFAFAFFFDF8F8FFFEF7F7FFFDF5F5FFFFF9F9FFD6D1D1FFFFF8F8FFFDF2
          F2FFFCF2F2FFFBF1F1FFFEF2F2FFE7DDDDFFEBE0E0FFFDEFEFFFF9EDEDFFFAEC
          ECFFFCEEEEFFE3D0D0FF725454AE000000000000000000000000000000005C5A
          5A9FE2E1E1FFFFFFFFFFF8F7F7FFF3F0F0FFEEECECFFECE7E7FFD7D2D2FFD5CF
          CFFFDED6D6FFD9CECEFFD4CACAFFD1C4C4FFCDC1C1FFB6A8A8FFC5B5B5FFC1B0
          B0FFBDA9A9FFB7A3A3FFB59F9FFFAB9595FFA99090FFA98E8EFFA58A8AFFA183
          83FFA08080FF9D7979FE755A5AAC000000000000000000000000000000005554
          548D807D7DDF777474D6736E6ECE706969C66B6464BE676161B6635B5BAD5F58
          58A55951519E554D4D955248488D4D444485483F3F7C443B3B753F36366C3B31
          3164362D2D5D322929542C26264C29212144231D1D3B1F1919341A15152C1610
          1023110E0E1B0C0A0A1307060609000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF0003FFF80000001800000018000000180000001800000018000
          0001800000018000000180000001800000018000000180000001800000018000
          0001800000038000000780000007800000078000000780000003800000078000
          000780000007800000078000000780000007800000078000000780000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000115012C01340178014B01B3015A01D9016101EC016101ED0159
          01DD014C01BB0135018301190139010101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000125
          0148015B01C4087101FE147701FF1F7901FF257901FF277901FF297701FF2874
          01FF257101FF1A6D01FF0B6901FE015D01D4012C015D01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010B0114015201AC0777
          01FE128301FF138B01FF108A01FF0F8901FF138501FF1A8101FF247A01FF3072
          01FF317201FF307001FF2F6F01FF276F01FF0E6901FE015601C5011101240000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000118012C016701E0038901FF039B
          01FF019B01FF019801FF019901FF019701FF019701FF019401FF019501FF0191
          01FF128501FF297601FF386C01FF346E01FF326F01FF226D01FF016101EF0120
          0145000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001170129016D01EA039505FF039E07FF039C
          07FF039B06FF039B05FF029A02FF019901FF019801FF019701FF019601FF0195
          01FF019301FF019301FF0C8801FF337101FF386D01FF346D01FF296D01FF0265
          01F7012001440000000000000000000000000000000000000000000000000000
          0000000000000000000001090110016901DB06980CFF06A10DFF069E0DFF069E
          0DFF069E0DFF049C09FF0EAC24FF12B12EFF0AA518FF019901FF019903FF0198
          01FF019601FF019501FF019201FF019201FF247A01FF3B6A01FF356C01FF2A6D
          01FF016101EF0111012400000000000000000000000000000000000000000000
          000000000000000000000151019F0B9310FF0DA516FF0AA313FF0AA214FF0AA2
          14FF09A314FF1EBF4AFF85DCA4FFAAE7C1FF5ED98AFF15B536FF019902FF039B
          06FF019902FF019701FF019501FF019201FF019201FF1F7C01FF3B6B01FF346D
          01FF206D01FF015601C501010102000000000000000000000000000000000000
          00000000000001200137018304FD25B031FF0BA419FF0DA51AFF0DA51AFF0CA6
          1AFF13B738FFC2E5CBFFFFF6FEFFFFF8FDFFFFFDFFFF69DC92FF15B536FF039B
          04FF049C08FF029A04FF019801FF019601FF019401FF019301FF287901FF386D
          01FF336F01FF0C6A01FE012C015E000000000000000000000000000000000000
          000000000000015C01AF21A42AFF1FAE2FFF0FA720FF10A820FF0FA920FF0CAF
          29FFBAE0C4FFFBF2F8FFF3F2F2FFF5F5F5FFFCFAFCFFFFFFFFFF68DB91FF16B6
          38FF049C08FF059D0AFF039B06FF019902FF019601FF019301FF019201FF376E
          01FF356E01FF286F01FF015C01D5010101010000000000000000000000000000
          0000010D0117018101FA44C054FF16AC29FF13AB26FF12AA26FF02A718FFB5DB
          BBFFF7EEF5FFEFEEEEFFF2F2F2FFF5F5F5FFF8F8F8FFFEFCFDFFFFFFFFFF67DA
          91FF16B638FF049C09FF059D0BFF039B06FF019902FF019601FF019501FF1385
          01FF396E01FF327101FF0A6C01FE011901390000000000000000000000000000
          00000130015C14981CFF4FC360FF13AD2AFF14AD2BFF019F07FFADD4AFFFF3EB
          F2FFEBEAEBFFEEEEEEFFF1F1F1FFF4F4F4FFF7F7F7FFFAFAFAFFFFFEFFFFFFFF
          FFFF67DA91FF17B739FF059D0AFF059D0BFF039B06FF019902FF019501FF0195
          01FF337101FF327101FF1A7001FF013701850000000000000000000000000000
          0000014B01962AAB38FF54C567FF12AE2CFF019901FFA8CFAAFFEFE7EFFFE7E6
          E7FFE9E9E9FFEFEEEFFFFBF3FAFFFFF8FFFFFCF9FCFFFAFAFAFFFDFDFDFFFFFF
          FFFFFFFFFFFF67DA91FF17B739FF059D09FF059D0AFF039B05FF019801FF0196
          01FF1B8101FF327201FF257301FF014F01BD0000000000000000000000000000
          0000015D01BA3CB94FFF5CC970FF05A216FF6DB66AFFF1E5F1FFE3E2E3FFE5E5
          E5FFEBE9EBFFFBF0F9FF6EC87AFF42BC54FFC2E5C8FFFFFBFFFFFBFAFBFFFEFE
          FEFFFFFFFFFFFFFFFFFF67DA8FFF16B639FF049C09FF049C09FF029A04FF0198
          01FF098E01FF347201FF2A7701FF015E01E10000000000000000000000000000
          0000016601C849C25FFF64CD7AFF11AB28FF36A133FFF6E6F6FFE3E2E3FFE7E5
          E7FFFAECF9FF44B54BFF07AB20FF15B232FF07AB22FFC5E6CAFFFFFCFFFFFDFC
          FDFFFEFEFEFFFFFFFFFFFFFFFFFF66D98EFF16B638FF039B07FF049C07FF019A
          02FF019801FF317501FF2C7801FF016801F30000000000000000000000000000
          0000016501C74EC464FF68CF80FF2DC050FF019202FF6EB56BFFF7E8F7FFF9EA
          F8FF3CAD3EFF08A91CFF23BC46FF21B941FF1CB53AFF08AB20FFC5E6CAFFFFFC
          FFFFFBFAFBFFFDFCFCFFFFFEFFFFFFFFFFFF66DA8FFF15B537FF029A05FF029A
          05FF019A01FF287E01FF2A7A01FF016701F10000000000000000000000000000
          0000015E01B649C05FFF6BD283FF41C561FF24BF4CFF029A10FF13920FFF1D9A
          18FF07A71AFF27C050FF24BC4AFF22BA45FF20B843FF1CB53AFF06AA1FFFC5E7
          C9FFFFFBFFFFFAF9FAFFFAFAFAFFFEFCFEFFFFFFFFFF64D88BFF14B436FF0199
          02FF019C02FF228201FF277C01FF015F01DD0000000000000000000000000000
          0000014B018E3AB64EFF6CD487FF5BD07AFF26C14FFF2DC559FF27C150FF25BE
          4DFF2BC457FF28C051FF27BF4EFF25BD4BFF23BB48FF22BA43FF1CB53AFF07A9
          1EFFC4E6C8FFFFFAFFFFF8F7F8FFF8F8F8FFFBF9FAFFFFFDFFFF64D78BFF14B4
          33FF019A01FF1F8501FF1F7B01FF014F01B60000000000000000000000000000
          0000012C015221A52EFF6DD88BFF78D993FF2CC659FF2DC55AFF2DC55CFF2DC5
          5CFF2EC65BFF2BC357FF2AC254FF28C050FF25BD4BFF23BB46FF20B842FF1BB4
          38FF06A91CFFC2E5C8FFFFF8FFFFF6F6F6FFF5F5F5FFF8F6F8FFFFF9FFFF5DD5
          85FF08A71BFF1D8501FF157A01FF0136017B0000000000000000000000000000
          0000010A010E018701F46ED98FFF77DA93FF58D17DFF2DC85FFF31C963FF31C9
          61FF30C861FF2EC65DFF2CC459FF2AC254FF28C051FF25BD4AFF22BA46FF1FB8
          40FF1AB337FF05A81CFFC1E3C6FFFDF5FBFFF4F3F3FFF3F3F3FFFCF4FAFFAAE3
          BBFF0EB12BFF1F8701FF087501FE0116012D0000000000000000000000000000
          000000000000015A019E44BD5AFF77DC98FF80DF9EFF37CE6BFF33CB66FF33CB
          67FF34CC66FF31C963FF2FC75FFF2CC459FF29C153FF26BE4DFF23BB48FF20B8
          43FF1DB53BFF18B132FF05A71AFFC0E1C3FFFDF3FBFFF7F1F6FFFFF2FFFF46C8
          69FF08A312FF1B8301FF016001C5000000000000000000000000000000000000
          00000000000001190126058F07F975E09AFF7BDE9BFF73DD97FF30CB63FF34CC
          67FF34CD67FF34CC67FF32CA63FF2EC65CFF2BC356FF28C050FF25BD4AFF21B9
          44FF1EB63DFF1BB337FF15AF2EFF06A71AFF9CD4A4FFB3DDBCFF3EC05AFF0DAC
          23FF0D9807FF087B02FE01270148000000000000000000000000000000000000
          00000000000000000000014F01862FAF40FF7BE1A0FF7EDF9EFF6BD98EFF2ECB
          63FF34CC67FF34CC68FF32CA65FF2FC75EFF2CC458FF29C152FF25BD4BFF22BA
          45FF1FB73FFF1CB438FF18B032FF14AD29FF08A71CFF08A91DFF0BA71AFF07A1
          10FF0E8907FF015701AE00000000000000000000000000000000000000000000
          0000000000000000000001040106016D01C64CC264FF7AE09EFF7FDF9DFF6FDB
          91FF34CD67FF32CC66FF33CC65FF30C85FFF2CC459FF29C154FF26BE4CFF22BA
          46FF1FB73FFF1CB439FF19B132FF15AD2CFF12AA25FF0FA71EFF0CA618FF089A
          0FFF016A01E0010B011400000000000000000000000000000000000000000000
          000000000000000000000000000001100117017501D74CC265FF79E19FFF7ADE
          9AFF7BDD9BFF4ED27BFF2FCB63FF2EC85EFF2CC459FF29C152FF26BE4CFF22BA
          47FF1FB73FFF1CB43AFF19B132FF15AD2CFF12AA25FF10AA21FF0A9D15FF0173
          01EA0119012C0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001100117016D01C633B142FF77E1
          9CFF77DE9AFF7ADE9BFF72DA94FF4ECF76FF33C75EFF25C14FFF22BD48FF20BA
          43FF1EB73CFF19B337FF17B030FF16AD2CFF1CB12FFF0A9713FF016B01DB0119
          012A000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001040106014F01850993
          0BF94AC264FF74DF9BFF70DC95FF6FD88FFF6DD68AFF61D280FF51CB6EFF44C5
          61FF3DC259FF3BC055FF3CC051FF23A932FF058806FD015501A0010A01100000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000119
          0125015D019E048C04F42AAB39FF43BE5BFF52C86FFF56CD75FF53CA6EFF46C1
          5EFF33B245FF1A9E25FF028604FA016001AF0121013600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000010A010E012D0151014D018E016001B5016801C7016901C80160
          01BA014E01960131015D010E0117000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFF00FFFFFC001FFFF0000FFFE00007FFC00003FF800
          001FF000000FF0000007E0000007E0000007C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003E0000007E0000007F000000FF000
          000FF800001FFC00003FFE00007FFF0000FFFFC003FFFFF00FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000013A0166016801D1016D01DF014E019E010A011000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000001010101015D
          0194058F08FE12A925FF13AD27FF089310FF016601CD010C0113000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001010101015F01950691
          08FF16B02DFF14AD29FF12AB27FF15B028FF099412FF016701CE010C01120000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000101010101620195059509FE18B2
          31FF16AE2DFF14AC29FF13AB28FF13AB26FF14AF27FF099512FF016801CD010C
          0113000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101010164019506980AFF1AB436FF18B0
          31FF16AE2DFF15AD2CFF15AD2AFF14AC28FF13AB26FF14AF29FF099512FF0166
          01CD010C01130000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000101010101650194059B0AFF1DB539FF1AB235FF18B0
          31FF17AF30FF16AE2EFF16AE2CFF15AD2AFF14AC29FF13AB27FF14AF29FF0995
          12FF016801CD010C011200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000101010101670194079F0BFE1FB73EFF1CB439FF1AB235FF19B1
          33FF18B032FF18B030FF15AF2CFF13AE2BFF15AD2BFF14AC29FF13AB27FF14AF
          2AFF099513FF016801CD010C0112000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001010101016A019406A00BFF20B943FF1EB63DFF1CB439FF1BB337FF1AB2
          35FF19B134FF17B132FF3EBE52FF35BA4AFF0FAC27FF13AD29FF14AC2AFF15AD
          28FF16B02AFF099513FF016701CC010C01120000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          0101046E079409A512FF22BA46FF20B840FF1EB63DFF1DB53BFF1CB439FF1BB3
          39FF1CB538FF20B53BFF21AD33FF76D285FF76CF83FF14AE2BFF12AD2AFF15AD
          2AFF14AC28FF15B02CFF099513FF016701CC010C011200000000000000000000
          000000000000000000000000000000000000000000000000000000000000065A
          0C7F12AB20FE2BBE50FF21BA43FF20B841FF1FB73FFF1EB63DFF1DB53DFF1DB5
          3DFF1DB539FF019402FC017301C2019401FE65CD79FF79D287FF11AE2AFF14AE
          2AFF15AD2BFF14AC29FF15B02BFF099613FF016701CC010C0112000000000000
          0000000000000000000000000000000000000000000000000000000000000798
          14ED60D07DFF20BC45FF22BA45FF21B943FF20B841FF1FB73FFF1FB741FF1FB7
          3FFF029803FC0145016400000000015E018D019401FE67CD7AFF77D085FF11AE
          2AFF13AE2BFF15AD2BFF14AC29FF15B02DFF099613FF016701CC010C01120000
          0000000000000000000000000000000000000000000000000000000000000998
          17EA6DD587FF3EC45EFF1EBA43FF22BA45FF21B943FF21B945FF20B841FF029B
          04FD0147016400000000000000000000000001600191019401FF67CD7BFF77D2
          85FF13AE2CFF13AE2BFF15AD2CFF16AE2AFF17B12DFF0A9613FF016701CC010A
          0111000000000000000000000000000000000000000000000000000000000B5F
          158D21B539FF92E0A7FF31C254FF1AB83FFF20BA46FF21B943FF049E09FD014C
          0169000000000000000000000000000000000101010101630193019601FF67CD
          7AFF76D185FF12AE2AFF13AE2DFF16AE2CFF15AD2AFF16B12EFF0A9614FF0167
          01CB010C01110000000000000000000000000000000000000000000000000109
          020B0D891CCB26B740FF8ADDA2FF83DC9BFF4DCA6CFF07A411FD034F056A0000
          0000000000000000000000000000000000000000000001010101016301940195
          01FE67CE7BFF76D085FF12AE2CFF15AF2DFF16AE2DFF15AD2BFF16B12EFF0A96
          14FF016701CB010A011100000000000000000000000000000000000000000000
          0000020C030D0B74199B0AA91CFA11AE28FF09A317F9054E0967000000000000
          000000000000000000000000000000000000000000000000000001010101015E
          0197019302FF67CE7BFF74D085FF12AE2BFF15AF2EFF16AE2DFF15AD2BFF16B1
          2FFF0A9614FF016701CB010A0111000000000000000000000000000000000000
          000000000000000000000215041806320A410217051C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000102
          0102015A0199019302FF68CE7CFF74D085FF12AF2BFF15AF2EFF16AE2EFF15AD
          2CFF18B32FFF0A9714FF016701CB010A01110000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001020102015A019A019301FE68CF7CFF74CF85FF10AF2BFF15AF2FFF17AF
          2EFF16AE2CFF18B22FFF0A9714FF016801CA010A011100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001020102015E019D019603FF6ACF7EFF73CF83FF12AF2DFF17B0
          2FFF17AF2EFF16AE2EFF18B230FF0A9613FF015201A100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000001020103015F019F019403FF67CF7CFF73CF83FF12AF
          2CFF17B030FF17AF2FFF16AF2EFF18B12FFF017301E500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001020102015E01A0019503FE67CF7EFF72CF
          81FF0FAF2BFF13AF2DFF17B030FF15AB29FF016901CC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001020102015F01A2019604FF69CF
          7EFF7DD58DFF49C25FFF1EB73BFF058E0AFE012E016000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001030104015E01A50197
          07FF45C35FFF35BB4DFF049509FF015401A40102010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000001030103014B
          0187017E01E4017501D5013E016E010201020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFFFFF81FFFFFF00
          FFFFFE007FFFFC003FFFF8001FFFF0000FFFE00007FFC00003FF800001FF8008
          00FF801C007F803E003FC07F001FE0FF800FFFFFC007FFFFE003FFFFF001FFFF
          F801FFFFFC01FFFFFE01FFFFFF03FFFFFF87FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000846251FF866251FF846150FF846150FF846150FF846150FF836150FF8361
          50FF83604FFF82604FFF82604FFF81604FFF815F4EFF815F4EFF815E4FFF805E
          4DFF805E4EFF7F5D4CFF7E5C4CFF7F5E4EFF7C5C4CFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DECCFFF2DECCFFF2DF
          CCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DECAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DA
          C4FFF1D9C2FFF2D8C0FFF1D6BDFFF5D9BDFF7D5C4CFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000866251FFFFFDF2FFFAF3E9FFFAF3E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4
          E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1
          E2FFF9F0E0FFF9EFDEFFF9EDD9FFF4DCC4FF7C5B4AFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000876452FFFFFEF7FFFCF6ECFFDEC4B7FFDEC5B7FFDEC5B7FFDDC3B5FFDDC3
          B5FFDDC2B3FFDDC1B2FFDDC1B0FFDDBFAEFFDDBEABFFDDBDAAFFDDBCA8FFDDBA
          A5FFDDBAA5FFDFBEA9FFFAF0E1FFF3DFC9FF7D5C4BFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000886352FFFFFFF7FFFCF6EEFFDBC0B4FFDBC0B4FFDBC0B4FFDBC0B3FFDBBF
          B2FFDBBEB1FFDBBDAEFFDABBACFFDABAAAFFDAB9A8FFDAB7A5FFDAB6A2FFDAB5
          A0FFDAB5A0FFDDBAA4FFFBF3E4FFF5E0CCFF7E5D4CFF7E3F53FF7D3E51FF7D3E
          51FF7C3D50FF7A3D50FF7C3F52FF783D4FFF0000000000000000000000000000
          0000896453FFFFFFFAFFFCF7EFFFDBC2B8FFDBC2B8FFDBC2B8FFDBC2B6FFDBC0
          B5FFDBBFB2FFDBBEB1FFDBBDAFFFDBBCACFFDABAAAFFDAB9A6FFDAB7A4FFDAB6
          A1FFDAB6A1FFDDBBA5FFFBF4E7FFF4E2CEFF7F5C4DFFDFB0B9FFDDAEB4FFDDAC
          B4FFDEAAB3FFDDA7B0FFE1A7AFFF7A3D50FF0000000000000000000000000000
          00008A6554FFFFFFFAFFFCF8F1FFDBC3BBFFDBC4BBFFDBC4BBFFDBC3B9FFDBC2
          B8FFDBC0B6FFDBBFB3FFDBBEB1FFDBBDAEFFDBBCACFFDAB9A8FFDAB8A5FFDAB7
          A3FFDAB7A3FFDDBBA7FFFCF5E8FFF6E3D2FF815E4CFFE3CCCCFFE2CCCBFFE2C9
          C9FFE3C6C6FFE3C1C1FFE0ADB4FF783B4EFF0000000000000000000000000000
          00008B6654FFFFFFFBFFFCF8F2FFDBC6BEFFDBC6BFFFDBC6BFFFDBC4BCFFDBC3
          BAFFDBC1B7FFDBC0B5FFDBBEB3FFDBBDAFFFDBBCACFFDBBBAAFFDBBAA8FFDAB7
          A4FFDAB7A4FFDDBDA8FFFCF4E9FFF5E4D3FF805D4EFFCE92A2FFCE8F9FFFCE8F
          9FFFCF93A2FFE3CACBFFDFB2B7FF793C4FFF0000000000000000000000000000
          00008C6855FFFFFFFEFFFDF9F4FFDBC7C2FFDBC7C3FFDBC7C3FFDBC5BFFFDBC4
          BCFFDBC3BAFFDBC1B7FFDBBFB3FFDBBEB1FFDBBCAFFFDBBBABFFDBBAA8FFDBB8
          A5FFDBB8A5FFDDBDAAFFFCF6EAFFF5E4D2FF815F4DFFCB8C9EFFCB8A9CFFCB8A
          9CFFCE8F9FFFE4CECDFFE1B5BCFF7A3D50FF0000000000000000000000000000
          00008F6756FFFFFFFFFFFDFAF5FFDBC8C3FFDBC8C4FFDBC8C4FFDBC6C0FFDBC4
          BDFFDBC3BAFFDBC2B8FFDBC0B5FFDBBEB2FFDBBDAFFFDBBBACFFDBBAA9FFDBB9
          A7FFDBB9A7FFDEBEAAFFFCF5EBFFF5E4D3FF825E4DFFCB8E9FFFCB8C9CFFCB8C
          9CFFCE8F9DFFE4CFCFFFDFB7BCFF7B3E52FF0000000000000000000000000000
          00008F6855FFFFFFFFFFFDFBF7FFDBC7C3FFDBC7C4FFDBC7C4FFDBC6C0FFDBC5
          BEFFDBC4BBFFDBC2B8FFDBC0B5FFDBBFB3FFDBBDAFFFDBBCACFFDBBAAAFFDBB8
          A6FFDBB8A6FFDEBEABFFFCF5ECFFF5E4D4FF81604FFFCB90A1FFCB8E9FFFCB8E
          9FFFCE90A1FFE5D0CFFFE1BBC1FF7E3D51FF0000000000000000000000000000
          0000926A58FFFFFFFFFFFEFCFAFFDCC6C0FFDCC6C0FFDCC6C0FFDCC5BFFFDCC4
          BDFFDBC3BAFFDBC2B7FFDBC0B5FFDBBEB2FFDBBDAFFFDBBCADFFDBBAAAFFDBB9
          A7FFDBB9A7FFDEBDABFFFCF6EDFFF5E5D5FF83614EFFCC92A2FFCB8E9FFFCB8E
          9FFFCE92A1FFE5D1D2FFE0BCC1FF7D3E52FF0000000000000000000000000000
          0000936B57FFFFFFFFFFFEFDFBFFDCC6C0FFDCC6C0FFDCC6C0FFDCC4BEFFDCC4
          BBFFDCC3BAFFDBC1B7FFDBC0B4FFDBBEB2FFDBBDAFFFDBBCADFFDBBBAAFFDBB9
          A7FFDBB9A7FFDEBDAAFFFCF6EEFFF5E5D7FF846050FFCC92A2FFCC8FA0FFCC8F
          A0FFCE94A3FFE5D2D1FFE0BCC0FF7E3D4FFF0000000000000000000000000000
          0000946D58FFFFFFFFFFFEFDFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFD
          FBFFFEFCFBFFFEFCFAFFFDFBF9FFFDFBF7FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8
          F2FFFCF8F1FFFCF7EFFFFDF8EFFFF7E6D6FF84604FFFCC92A4FFCC90A2FFCC90
          A2FFCE94A3FFE5D3D4FFE0BCC1FF7F3D52FF0000000000000000000000000000
          0000966D59FFFFFFFFFFFFFFFFFFE0CFCEFFE0D0CFFFE0CFCDFFDFCDCAFFDFCB
          C8FFDFC9C5FFDFC8C2FFDFC7BDFFDFC5BBFFDFC3B8FFDFC1B4FFDFC0B1FFDFBF
          ACFFDFBFACFFE1C2B0FFFDF7F0FFF6E6D8FF856151FFCC94A5FFCC90A2FFCC90
          A2FFCE96A4FFE5D3D4FFE0BDC2FF7F4052FF0000000000000000000000000000
          0000976E5AFFFFFFFFFFFFFFFFFFDDCBC9FFDDCBCBFFDDCAC9FFDDC9C6FFDCC7
          C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9
          A7FFDBB9A7FFDEBEABFFFDF7F0FFF6E6D7FF866150FFCC94A5FFCC90A2FFCC90
          A2FFCE96A6FFE5D5D6FFE0BDC2FF803E51FF0000000000000000000000000000
          0000986F5CFFFFFFFFFFFFFFFFFFDDCBC9FFDDCBCBFFDDCAC9FFDDC9C6FFDCC7
          C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9
          A7FFD5B3A0FFD3B29EFFE6DACFFFDBC4B4FF866251FFCC94A4FFCC90A2FFCC90
          A2FFCE94A4FFE4D6D7FFE0BFC5FF814056FF0000000000000000000000000000
          00009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFEFFFEFCFAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACC
          C5FFD5C5BEFFC8B6ADFFBBA49AFF977666FFA1857AFFE5DCDDFFE4D9DAFFE4D9
          D9FFE5D7D7FFE5D7D7FFE1BFC4FF813F54FF0000000000000000000000000000
          00009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFDFFFEFCFAFFFFFFFFFFBBA295FFA18173FFA78979FFA583
          70FFA38068FFB08867FF936D55FF9A736AFFCB9BA9FFCF9AABFFCF97A5FFCF97
          A5FFD09AA9FFE5D8D9FFE1C0C6FF814157FF0000000000000000000000000000
          00009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFEFEFEFFFEFEFDFFFEFCFAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1
          BBFFF8D69CFFC59E6FFF99736AFFC899A8FFCB99A9FFCC94A5FFCC90A2FFCC90
          A2FFCE96A4FFE5D8D9FFE1C0C5FF834056FF0000000000000000000000000000
          00009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
          FEFFFEFEFEFFFEFDFCFFFEFCFAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF
          96FFC0986BFF956F67FFC89CAAFFCB9CACFFCB99A9FFCC94A5FFCC90A2FFC78A
          9BFFC68899FFD3B8BDFFCB9EA9FF824056FF0000000000000000000000000000
          00009C735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFE
          FEFFFEFDFDFFFEFDFCFFFEFCF9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F
          6FFF9B7F73FFE1D9D8FFE6E2E3FFD3BFC6FFC9ABB2FFCEB4BCFFC9AEB6FFC6A7
          B1FFBB97A2FFB18492FF935266FF5E2F3DB60000000000000000000000000000
          00009D725EFFFFFFFFFFFEFEFEFFFEFEFEFFFFFFFFFFFEFEFEFFFEFEFEFFFEFD
          FDFFFEFDFCFFFEFCFBFFFDFBF8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF9D7E
          72FFE1DCDAFFE6E1E1FFE6E6E6FFB17F8EFF9C5E73FFA26476FFA05B6DFF9F52
          64FFAA5263FF904357FF683445C40804050E0000000000000000000000000000
          00009B725CFFFFFFFFFFFEFDFDFFFEFEFDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFD
          FCFFFEFCFBFFFEFCFAFFFDFBF7FFFFFEFBFFCEBAB1FFA27A5BFF9D7E6EFFDDDA
          D8FFE6E5E4FFE6E1E1FFE6E6E6FFBD96A2FFB98895FFE6CCCBFFE4A5A7FFE587
          87FFBC5B64FF653343C10705060C000000000000000000000000000000000000
          00009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB39789FF9E7D6EFFDEDADAFFE4E5
          E6FFE6E2E3FFE6E1E1FFE6E5E4FFBE99A3FFB47681FFE1A2A2FFE18385FFB757
          60FF6C374BCF0804050E00000000000000000000000000000000000000000000
          00009A725EFF9D7360FF9C725EFF9A725EFF9A725EFF99715CFF98705BFF976E
          5CFF966F5AFF956D5BFF936B59FF936D5AFFA68A7EFFDDDBD9FFE4E5E6FFE6E4
          E5FFE6E2E3FFE6E1E1FFE6E4E4FFC6AAB2FFAB5E6DFFE38687FFBD5B64FF6C36
          49CC0C0608120000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000994A
          64FFE6E6E6FFE4E5E6FFE4E5E6FFE6E6E6FFE4E5E6FFE4E5E6FFE6E4E5FFE6E2
          E3FFE6E2E3FFE5E0E0FFE6E3E2FFC4A2ABFFB05764FFC05C66FF6D374ACD0B05
          0711000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009649
          5FFFE6E6E6FFE6E4E5FFE6E5E4FFE6E5E4FFE6E5E4FFE6E2E3FFE6E2E3FFE6E2
          E3FFE6E1E1FFE5DEDEFFE6E2E2FFC09AA7FF9D4858FF753B4CD610080A190000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000994A
          64FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6
          E6FFE6E6E6FFE6E6E6FFE6E6E6FFAB7484FF74384CD40F080B17000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000964B
          63FF994C65FF994A64FF964B63FF964B63FF954960FF934960FF924962FF9248
          5DFF914961FF90475FFF8F485DFF653243B90D07081600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFC00001FFC00001FFC00001FFC00001FFC0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0007C000000FC000001FC000003FFF80007FFF8000FFFF8001FFFF8003FFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00003412014D872C01DB862C01DB451701711D0B012F03010104000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001F0A
          0129942F01F5643219FF8D3001FF9D2D01FF9B2D01FE963001F1802A01BE5B1E
          01853512014D1106011600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050201058129
          01CB752F0BFF596871FF69757CFF7F7E7FFF90776AFF9A6449FF9A491FFF9C31
          01FF9C2B01FF9C2C01FD902A01DB6D2101A34917016A230C0132050201050000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000581D01849E33
          01FF5F301AFF5D6569FF707171FF818283FF919598FF9DA4A8FF9EA9AEFFA2AF
          B6FFA8A099FFAE8B6FFFB07244FFAF5513FFAA3A01FFA13101FE932801F37C24
          01C05A1C01883713015012060118000000000000000000000000000000000000
          000000000000000000000000000000000000000000002A0E013A9A3401FBC85E
          01FF662209FF434E59FF696C76FF797D84FF8A8F94FF94989AFF9B9D9DFFA2A3
          A3FFA7AAABFFAAB0B4FFAEB7BCFFB2BFC6FFB9C1C6FFC0B09CFFC99D6CFFC57E
          3AFFBF5E09FFA13601FF983301EE070201090000000000000000000000000000
          0000000000000000000000000000000000000903010D822901DCCC6701FFFF95
          01FFCD5204FF834C01FF5E5801FF9C541FFFAA6445FF9C7A6EFF949196FF959E
          A6FF9BA2A8FFA1A7AAFFA7ABADFFAEB0B0FFB5B6B7FFB9BDC0FFBEC5CBFFC3CC
          D4FFCCDFECFF8E4A29D92009012C000000000000000000000000000000000000
          0000000000000000000000000000000000006620019CB24D01FFF18901FFFFA6
          0BFF75760AFF016001FF015F01FF016701FF1F5D01FF535101FF824501FFAD46
          07FFB55830FFAB7159FFA58C86FFA2A7AFFFA6B0B8FFADB6BAFFB3B9BCFFBBBE
          C0FFC3C8CBFE1916141F00000000000000000000000000000000000000000000
          00000000000000000000000000003913014F9D3601FEEA8301FFFF9E05FFCC88
          15FF015E01FF3F5142FF466A46FF2D6E2EFF076C0DFF016301FF016501FF0165
          01FF015E01FF355201FF664401FF943901FFB64413FFB3613FFFAF806EFFB0A2
          9FFFB5C0C7FE1E21222500000000000000000000000000000000000000000000
          00000000000000000000130701178D3001E9D87301FFFF9601FFFF9D1BFF0161
          01FF015304FF6D5F6DFF746F74FF888088FF9E929EFFA79FA7FF829382FF5A8A
          5DFF2E8135FF01750DFF016D01FF016A01FF016101FF195401FF4A4701FF7937
          01FFA53201FF912801CA360F0141000000000000000000000000000000000000
          000000000000000000007C2F01B2C25C01FFFA9201FFFFA818FF3D6705FF016F
          06FF0A4B0BFF5F535FFF6D6A6DFF7D7B7DFF8E8D8EFF9D9B9DFFA4A0A4FFADA6
          ACFFB5ABB5FFBEB0BDFFAEAFB0FF86A88DFF5CA068FF29913DFF01810EFF0174
          01FF016801FF035E01FF0B4A01BB010F011E0000000000000000000000000000
          00000000000050200164B54E01FFF58D01FFFFA50EFF9A7F14FF016B06FF1EAA
          43FF015001FF364632FF5A6A55FF81767EFF938593FF999099FF9C979CFF9F9C
          9FFFA3A2A3FFA9A9A9FFB0AFB0FFBAB5BAFFC1B8C0FFCBBECAFFD2C4D1FFB3C3
          B9FF90C4A1FF308E3BFF015101C7010A01140000000000000000000000000000
          000008030108A04301E9E78101FFFF9F05FFEF9722FF015901FF1EA641FF33CB
          64FF1FAB34FF017E07FF017725FF017D01FF137C03FF387F2EFF5F845BFF8A8E
          88FFA899A7FFACA0ACFFB0A7B0FFB1ADB1FFB5B4B5FFBABABAFFBFBFBFFFC6C3
          C5FFD9CDD8FF3459349D01030107000000000000000000000000000000000000
          000031160144C05B01FFFF9C01FFFFA926FF0E5D01FF0D922DFF2DC05BFF3CDA
          68FF047E44FF0160ADFF0170B5FF0166A2FF01726DFF01773FFF017C11FF017D
          01FF017501FF207B13FF4A8443FF789275FFA7A4A6FFC0B1C0FFC2B8C2FFC5BE
          C5FFCCC6CCFF2827282900000000000000000000000000000000000000000000
          00004F24016CDB7301FFFFAF20FF62700BFF017812FF2CBE59FF3DD96FFF149B
          35FF015DA1FF375E6DFF418196FF2890B5FF0588C1FF017DC3FF016DB7FF0161
          A8FF016D84FF017358FF01772AFF017B01FF017301FF037001FF307F26FF6090
          5BFF96A794FF3135314600000000000000000000000000000000000000000000
          00004822015FE5780AFFC38E1FFF016801FF27B954FF39D26CFF2ABE3FFF0169
          94FF015B8BFF6C5D54FF736B68FF857A75FF9B8A85FF9E9C9AFF7AA2AEFF51A6
          C0FF279AC6FF018AC9FF017AC1FF0167B2FF016499FF016F71FF017242FF0175
          13FF017501FF016801FA012E015B000000000000000000000000000000000000
          00000E06010EA34B01C5016901FF1DAA43FF35CD68FF3BD55BFF01726CFF0178
          B7FF03547AFF5D4C46FF6E6867FF7C7979FF8C8B8AFF9D9A99FFA59E9CFFACA2
          9FFFB6A7A3FFBFABA7FFA7B2B3FF7BB7C6FF4CB2D3FF1C9FD1FF0188CCFF0175
          BDFF0160ABFF015D80FD0140679F012639520000000000000000000000000000
          000000000000015A01B110992AFF33CB68FF43E071FF088745FF0177BCFF01B7
          EDFF016B9DFF2B4551FF476A78FF727271FF92817CFF9D918DFF9C9492FF9F9A
          99FFA3A2A1FFA8A6A6FFAFABAAFFB9B1AFFFC2B6B3FFCCBCB7FFD1C1BCFFABC7
          CDFF79C9DEFF4EBEE0FF016BA2FE014467980000000000000000000000000000
          000001270147058708FE34C968FF43DE76FF1CA83AFF016DAFFF01A8DEFF01CC
          FFFF20CCF5FF0179B1FF018FC7FF017CB4FF0173A5FF23769CFF487E95FF7187
          92FF9B9492FFAC9D98FFAEA3A0FFB0A9A6FFB2AEACFFB6B6B5FFBBBAB9FFC5C1
          BFFFD0C5C2FFD6CBC7F801355065000000000000000000000000000000000000
          0000015101A31FAE3FFF3FDA75FF32C848FF016E92FF0197D0FF01C0F4FF1DDF
          FFFF0B85B4FF15A5D2FF04B6E8FF01B4E7FF01ADE1FF019FD4FF018FC5FF0181
          B8FF0170A9FF056F9FFF2F7A9CFF59879CFF86969FFFB3AAA5FFBFB2ACFFC2B9
          B4FFC4BDBAFFB5ADAACC00000000000000000000000000000000000000000000
          0000017001D938D16CFF45DE65FF017C6DFF0187C6FF01B6EBFF0BD7FFFF17A1
          CDFF0F8FC0FF0CBAEBFF01B0E4FF01AFE3FF01AEE1FF01AEE1FF01AEE1FF01AF
          E2FF01B0E3FF01A6DAFF0197CCFF0187BDFF0176AEFF0167A0FF106F9DFF3D7F
          9EFF6D92A6FF9DA2A3E701030505000000000000000000000000000000000000
          0000017701DF4CE577FF0A9347FF017CC2FF01AEE3FF01CCFFFF1EC0E8FF0880
          B1FF1CC1EEFF01B2E5FF01B1E4FF01B0E3FF01AFE2FF01AEE1FF01ADE0FF01AC
          DFFF01ACDFFF01AADDFF01ABDEFF01ABDEFF01ACDFFF01ABDEFF019ED3FF0190
          C6FF0181B7FF0172A8FF016BA0F30110161C0000000000000000000000000000
          00000149017D019301FD017ABBFF01A1D8FF01C2F6FF1BD2FDFF027CB0FF28C0
          EAFF02B7EAFF01B4E7FF01B3E6FF01B2E5FF01B1E4FF01B0E3FF01AEE1FF01AD
          E0FF01ACDFFF01ABDEFF01AADDFF01A9DCFF01A8DBFF01A8DBFF01A7DAFF01AA
          DDFF01A2D5FF0171A4F901334B63000000000000000000000000000000000000
          000001030404016B9DD20196CDFF01B9EEFF10D7FFFF0488BBFF2AB5E0FF0FBF
          F0FF01B6EAFF01B3E7FF01B3E7FF01B2E6FF01B2E5FF01B1E4FF01B0E3FF01AF
          E2FF01AEE1FF01ADE0FF01ACDFFF01AADDFF01A9DCFF01A8DBFF01ADE0FF0199
          CCFF01699BEE0124344400000000000000000000000000000000000000000000
          0000012D3D54018ABDFF01B1E4FF05D2FFFF0BA0CFFF22A3CFFF20C9F5FF01B9
          EEFF14B5E3FF6DCCE6FF57C7E7FF40C4E9FF29C0EBFF0EBCEBFF01B5E9FF01B2
          E7FF01AEE4FF01ABE0FF01ACE0FF01ACDFFF01ACDFFF01B0E3FF0192C5FF0165
          91DE0118212A0000000000000000000000000000000000000000000000000000
          00000152709B019FD2FF01C9FCFF0EB9E7FF1391C1FF34D0F8FF01C0F4FF01AE
          E2FF61C8E5FF64CAE6FF68CCE8FF6ECFE9FF75D1EBFF7DD4EEFF82D6EFFF6FD4
          F0FF55D1F3FF3DCCF3FF01BDF1FF01AEE1FF01B3E6FF018DC0FF015E85C7010D
          1317000000000000000000000000000000000000000000000000000000000000
          0000016286B701B5E9FF0BCBFAFF0789BBFF42D0F6FF10C6F6FF01AEE3FF41B9
          DCFF69CCE5FF6BCEE8FF6CCEE9FF6DD0EBFF6FD1ECFF72D2ECFF74D2EFFF7BD7
          F0FF8AD9F0FF26C4EEFF01B2E6FF01B3E6FF0187BCFF015177AB010608090000
          0000000000000000000000000000000000000000000000000000000000000000
          0000014F6A8E02A8DBFF0193C6FF44C2E9FF23D4FFFF01C7FBFF01C2F5FF01B8
          ECFF01B0E5FF07B1E3FF20B4E0FF36BAE3FF51C4E7FF6DCFEAFF7DD6EFFF81D7
          EEFF19BCEAFF01B3E8FF01B2E6FF0186BAFE0149698901020202000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001090C0D014C6777016D94A70184B1D3018DC2F70195C8FF019ED1FF01A9
          DCFF01B2E5FF01BDF0FF01C3F7FF01C1F6FF01B9EEFF01B4E9FF01B4E6FF08B7
          E6FF01B8ECFF01B1E4FF0180B4FA013D55660000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001050606011B242901364954014E
          6C7E01678EA9017DACD40186BAF7018FC2FE0199CCFF01A3D6FF01ACDFFF01B6
          EAFF01ADE0FF017CADEF012A3D46000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000010506070118202A012F405401435D8001597BAA016D
          98D4016F9BD8011A242C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFE3FFFFFFC03FFFFF8001FFFF00000FFF000007FE00000FFC00
          001FFC00001FF800000FF0000007E0000007E000000FE000001FC000001FC000
          0007E0000003E0000003E0000007C000000FC000000FC0000007C0000007E000
          001FC000003FC000007FC00000FFC00001FFE00003FFFF000FFFFFFC1FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000160C071981370BC0A34209FF9F3C09FF9A3605FF7377
          79FF8F8C8AFF939091FF989696FF9D9A9BFFA19F9FFFA4A09EFF9C9896FF9490
          8EFF8D8886FF84817EFF7C7877FF747473FF653520FF9E3705FF9A3708FF9A36
          08FF9A3506FF96360AF65D250A96060403070000000000000000000000000000
          000000000000150C0517953F02D6B85F14FFCA7E2DFFCA7D2CFFC87A28FF878E
          95FFC0BCB7FFCEB69BFFD7C2A9FFE2CCB4FFEDD6B8FFFEFEFEFFFFFFFFFFF7F7
          F8FFEAEAEBFFDCDCDDFFCFCFCFFFC5C9CDFF7D5D35FFDE9437FFD89139FFD992
          38FFE1A75AFFDA943BFFA94A0CFF5F2910930000000000000000000000000000
          00001E1309209B4502D4BC6315FFC6792BFFC67728FFC67629FFC67626FF8188
          91FFB1A090FFCC761BFFCB7C25FFCF8128FFD4821FFFE6E6E3FFF4F5F6FFFDFD
          FDFFF1F1F1FFE3E3E3FFD6D6D6FFCED2D5FF7D5B35FFDA8D34FFD38A35FFD48A
          31FFE8BE8CFFD9933DFFD78F37FF91360DED0000000000000000000000000000
          0000B05C11E2BF6614FFC37529FFC37428FFC37327FFC37427FFC47224FF848C
          94FFA59484FFC16B1BFFC27226FFC7792BFFCD7C22FFDAD9D7FFE6E6E7FFF4F4
          F4FFFDFDFDFFF2F2F2FFE4E4E4FFDCDFE3FF7D5B35FFD88A33FFD28834FFD387
          2FFFE6BC87FFD7913AFFD99239FF9E3E14FF0000000000000000000000000000
          0000CB6E16FFC17026FFC17126FFC17025FFC17125FFC37126FFC06F22FF929A
          A2FFA9988BFFB95D13FFBA661FFFC06E24FFC7731DFFCECDCBFFDADBDCFFE6E6
          E6FFF3F3F3FFFDFDFDFFF2F2F2FFECEFF3FF7E5B37FFD58831FFD18733FFD285
          2EFFE5B986FFD68E39FFD88F37FF9D3E14FF0000000000000000000000000000
          0000D07316FFBF6C22FFBF6D23FFBF6E24FFBF6E24FFC06F24FFBF6D20FF9DA5
          ADFFB3A094FFAE4C09FFB35B18FFBA651FFFC36C1AFFC1C0BFFFCDCECFFFD9D9
          D9FFE5E5E5FFF3F3F3FFFDFDFDFFFAFEFFFF7E5B36FFD48630FFCF8532FFD184
          2DFFE4B887FFD58C38FFD58E36FF9D3E14FF0000000000000000000000000000
          0000D37516FFBE6A21FFBD6B23FFBD6B22FFBE6B22FFBE6C23FFBD691FFFA8B1
          B9FFBCA99EFFA23C01FFAD5012FFB65F1AFFC16717FFB5B1B0FFBFBFC0FFCCCC
          CCFFD8D8D8FFE5E5E5FFF2F2F2FFFFFFFFFF7F5D37FFD3832FFFCE8331FFCE82
          2CFFE3B886FFD48A37FFD48C35FF9E3E14FF0000000000000000000000000000
          0000D27416FFBB681EFFBB6820FFBC6820FFBC6921FFBC6A21FFB9671BFFB0B9
          C1FFC6B2A8FF9F3E06FFAB5319FFB36020FFBC6A1FFFA6A5A1FFB2B3B4FFC0C0
          C0FFCBCBCBFFD9D9D9FFE5E5E5FFFAFEFFFF815D39FFD5832FFFD08330FFCF82
          2CFFE5B888FFD28A37FFD38B34FF9E3E14FF0000000000000000000000000000
          0000D27416FFBB661DFFBA6620FFBA661FFFBA671FFFBC6720FFB8631BFFBBC6
          CFFFE4EBEFFFD3DEE5FFC5D0D8FFB3BEC7FFA5AEB7FF9A9EA3FFA4A7AAFFB3B7
          BAFFC0C3C6FFCFD2D6FFDCDFE3FFF1F7FEFF725638FFB36E26FFB07029FFAF6F
          27FFD79F61FFD18834FFD28933FF9E3E14FF0000000000000000000000000000
          0000D27515FFB8621DFFB8631DFFB8641DFFB9641EFFB9651EFFBA651EFF9A77
          5BFFA07B5BFFA57F5DFFA58060FFA27F5DFFA07C5AFF9C7856FF987350FF916E
          4CFF8F6A47FF886442FF83613EFF805E3CFF91612FFFAA6925FFA86A27FFA76A
          27FFCA802DFFCF8432FFD18833FF9E3F14FF0000000000000000000000000000
          0000D37515FFB5611AFFB6611DFFB7611CFFB7621CFFB8621DFFB9631DFFBC64
          1BFFBB6419FFBC641AFFBE651BFFBF681CFFC0691EFFC16C20FFC36E21FFC46F
          22FFC77123FFC87425FFCA7726FFCB7928FFCC7B2AFFCB7D2CFFCD7F2DFFCE81
          2EFFCD812FFFCE8431FFD08631FF9E3F14FF0000000000000000000000000000
          0000D37515FFB55D1AFFB55E1AFFB55E1AFFB55F1BFFB6601BFFB6611CFFB762
          1CFFB8631DFFB9641EFFBA651FFFBB671FFFBB6820FFBD6A21FFBE6C22FFBF6D
          23FFC06F25FFC27126FFC37327FFC47528FFC67729FFC7792BFFC87B2CFFCA7E
          2DFFCB802EFFCD8130FFD18532FF9E3F14FF0000000000000000000000000000
          0000D37615FFB25B17FFB35B1AFFB35C19FFB45C16FFB45913FFB55911FFB75A
          12FFB65D14FFB75E13FFB85F14FFBA6215FFBB6217FFBC6518FFBD6719FFC069
          1AFFC16A1BFFC26C1EFFC56E1FFFC57220FFC87421FFC97724FFCA7925FFCB7C
          29FFCA7E2EFFCC812FFFCF8430FF9E3F14FF0000000000000000000000000000
          0000D37615FFB05716FFB15917FFB25814FFB1703FFFAF9784FFAD927FFFAE93
          7EFFAE937EFFAE9480FFAE947FFFAE947DFFAE937DFFAE937EFFAD947CFFAD92
          7DFFAC937BFFAA917AFFAB917BFFA98F79FFA89078FFA88F77FFA78F7CFFB984
          50FFCC7D2AFFCB7F2EFFCE832FFF9E3F15FF0000000000000000000000000000
          0000D37615FFB05614FFB05617FFB05512FFB07952FFBFC8CDFFD7D9DCFFD6D9
          DBFFD8DADCFFD7DADCFFD7D9DBFFD8DADCFFD6D8DAFFD5D7D9FFD4D7D9FFD1D4
          D6FFCFD2D4FFCDD0D2FFCBCDD0FFC9CBCEFFC7C9CCFFC7CACCFFAFB4BBFFB18A
          62FFCB7B28FFCA7E2DFFCC812EFF9E3F15FF0000000000000000000000000000
          0000D37615FFAC5213FFAE5414FFAE510FFFAF774FFFC8CED1FFE9E9E9FFE9E9
          E9FFE9E9E9FFEBEBEBFFECECECFFEDEDEDFFEBEBEBFFEAEAEAFFE9E9E9FFE7E7
          E7FFE4E4E4FFE2E2E2FFE0E0E0FFDDDDDDFFDADADAFFD9D9D9FFBABDC1FFB088
          5FFFCA7B26FFC97D2EFFCB802FFF9E4015FF0000000000000000000000000000
          0000D37615FFAC4F11FFAC5112FFAD4F0EFFAF774EFFCBD0D4FFEBEBEBFFEAEA
          EAFFEEEEEEFFD2D2D2FFCECECEFFCECECEFFCECECEFFCECECEFFCECECEFFCFCF
          CFFFCDCDCDFFCECECEFFCDCDCDFFDCDCDCFFDADADAFFDADADAFFBABDC1FFB088
          61FFC97827FFCA7C2CFFCC7F2DFF9E4015FF0000000000000000000000000000
          0000D37615FFA94E10FFAB4F11FFAB4D0BFFAE764EFFCED4D7FFEEEEEEFFEDED
          EDFFF1F1F1FFCFCFCFFFC9C9C9FFCACACAFFCACACAFFCACACAFFCACACAFFCACA
          CAFFCBCBCBFFCBCBCBFFCACACAFFDDDDDDFFDDDDDDFFDCDCDCFFBDC0C3FFB288
          60FFC87725FFC87B2BFFCA7E2EFF9E4015FF0000000000000000000000000000
          0000D37615FFA74A0EFFA94C0FFFA94A0BFFAE764EFFD0D6D9FFF1F1F1FFF0F0
          F0FFF4F4F4FFEEEEEEFFECECECFFEDEDEDFFEBEBEBFFEAEAEAFFE8E8E8FFE6E6
          E6FFE5E5E5FFE1E1E1FFDFDFDFFFE1E1E1FFDDDDDDFFDDDDDDFFBDC1C4FFB187
          60FFC97624FFC77A2CFFC97F2CFF9E4015FF0000000000000000000000000000
          0000D47716FFA7480DFFA74A0EFFA84808FFB07650FFD4D9DDFFF4F4F4FFF3F3
          F3FFF7F7F7FFE7E7E7FFE6E6E6FFE7E7E7FFE5E5E5FFE4E4E4FFE3E3E3FFE0E0
          E0FFE0E0E0FFDDDDDDFFDADADAFFE3E3E3FFE0E0E0FFDFDFDFFFBEC2C5FFB187
          62FFC77425FFC8792AFFCA7D2DFF9E4015FF0000000000000000000000000000
          0000D47716FFA4450BFFA6470CFFA64507FFAF754FFFD5DBDEFFF7F7F7FFF6F6
          F6FFFBFBFBFFD0D0D0FFC9C9C9FFC9C9C9FFC9C9C9FFC9C9C9FFCACACAFFCACA
          CAFFCACACAFFCBCBCBFFC9C9C9FFE2E2E2FFE0E0E0FFE0E0E0FFC0C3C6FFB187
          61FFC77523FFC6782BFFC87C2BFF9E4015FF0000000000000000000000000000
          0000D47716FFA3430AFFA4450BFFA54305FFAF754FFFD9DFE2FFF9F9F9FFF9F9
          F9FFFEFEFEFFDDDDDDFFD9D9D9FFDADADAFFD8D8D8FFD7D7D7FFD8D8D8FFD7D7
          D7FFD6D6D6FFD5D5D5FFD3D3D3FFE3E3E3FFE2E2E2FFE0E0E0FFC1C4C7FFB187
          61FFC67323FFC77929FFC87B2CFF9E4015FF0000000000000000000000000000
          0000D47816FFA14108FFA24209FFA34104FFB0744FFFDCE1E5FFFBFBFBFFFCFC
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFFF8F8
          F8FFF4F4F4FFF1F1F1FFECECECFFE6E6E6FFE1E1E1FFE1E1E1FFC2C5C9FFB388
          61FFC77424FFC7792AFFC87B2AFFA04215FF0000000000000000000000000000
          0000D47816FF9F3D07FFA14008FFA23D03FFAE744FFFDCE2E5FFFCFCFCFFFDFD
          FDFFFFFFFFFFDCDCDCFFD6D6D6FFD6D6D6FFD7D7D7FFD6D6D6FFD5D5D5FFD4D4
          D4FFD3D3D3FFD4D4D4FFD1D1D1FFE5E5E5FFE3E3E3FFE1E1E1FFC2C5C8FFB489
          62FFB4681FFFB56C25FFCB7C2AFFA54715FF0000000000000000000000000000
          0000D57917FF9E3B06FF9F3E07FFA03B02FFAE7350FFDEE4E7FFFCFCFCFFFCFC
          FCFFFFFFFFFFCFCFCFFFC7C7C7FFC9C9C9FFC9C9C9FFC9C9C9FFC9C9C9FFC9C9
          C9FFCACACAFFCACACAFFC9C9C9FFE4E4E4FFE3E3E3FFE1E1E1FFC2C5C8FFB78A
          63FF7D4714FF824F1DFFCC7C2BFFAA4C16FF0000000000000000000000000000
          0000C3690EEB9E3B04FF9E3C05FF9E3A01FFAE714EFFDEE4E7FFFBFBFBFFFBFB
          FBFFFFFFFFFFF4F4F4FFF4F4F4FFF4F4F4FFF2F2F2FFF0F0F0FFECECECFFEBEB
          EBFFE7E7E7FFE5E5E5FFE1E1E1FFE6E6E6FFE1E1E1FFE1E1E1FFC2C5C9FFB38A
          63FFA15D1AFFA66422FFC87829FF9D4309E90000000000000000000000000000
          000077410D8EBF5A01FF9E3A04FF9A3401FFAD6F4EFFDDE3E6FFFBFBFBFFFCFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFCFCFDFFF9F9F9FFF6F6
          F6FFF2F2F2FFEFEFEFFFECECECFFE7E7E7FFE3E4E4FFE2E3E3FFC2C7CAFFB288
          63FFC87325FFC77828FFB75407FF602E098A0000000000000000000000000000
          000007060407753F0A8EC46A0DEFD06B04FFCD8E50FFC8CACDFFC8C6C3FFC9C7
          C5FFCAC7C5FFC9C6C4FFC7C3C1FFC3C2BFFFC2BEBBFFBEBDBAFFBBB9B6FFBAB6
          B4FFB6B5B1FFB3B1AEFFB0AEABFFADABAAFFACA8A6FFA9A7A4FFA6A8A9FFB683
          54FFC35D02FFB1560BEF67330A8E070504070000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFF8000007F0000003E0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003E0000007FFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFFB9A69DFF019301FF019301FF019301FF0193
          01FF019301FFB7A59CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF7EBDFFF019301FF36CE60FF32CA5AFF1EB6
          35FF019301FFB7A59CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFFCF7EFFF019301FF55ED92FF50E88AFF2FC7
          51FF019301FFB7A49BFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFDF8F3FFFDF8F2FFFDF9F1FFFCF8F1FF019301FF54EC8EFF4DE584FF2CC5
          4CFF019301FFB8A59CFF00000000000000000000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFDEC4B7FFDEC5
          B7FFDEC5B7FFDEC4B7FFDDC3B5FFDDC3B5FFDDC2B3FFDDC1B2FFDDC1B0FFECDC
          D3FF019301FF019301FF019301FF019301FF019301FF54EC8DFF4EE684FF2AC2
          48FF019301FF019301FF019301FF019301FF019301FF00000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFDBC0B4FFDBC0
          B4FFDBC0B4FFDBC0B4FFDBC0B3FFDBBFB2FFDBBEB1FFDBBDAEFFDABBACFFEBD9
          D0FF019301FF43DB6EFF3AD260FF38D05BFF3DD565FF51E986FF4DE581FF40D8
          6BFF28C045FF2BC34BFF2DC550FF1EB635FF019301FF00000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFDBC2B8FFDBC2
          B8FFDBC2B8FFDBC2B7FFDBC2B6FFDBC0B5FFDBBFB2FFDBBEB1FFDBBDAFFFEBDA
          D1FF019301FF66FEA4FF59F190FF57EF8EFF53EB88FF50E882FF4DE581FF4DE5
          81FF4EE685FF4DE584FF4FE78AFF34CC5BFF019301FF00000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFDBC3BBFFDBC4
          BBFFDBC4BBFFDBC3BBFFDBC3B9FFDBC2B8FFDBC0B6FFDBBFB3FFDBBEB1FFEBDB
          D3FF019301FF6EFFAFFF62FA9CFF5FF798FF58F08FFF53EB86FF50E882FF51E9
          86FF54EC8CFF54EC8DFF54EC91FF37CF60FF019301FF00000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFDBC6BEFFDBC6
          BFFFDBC6BFFFDBC4BEFFDBC4BCFFDBC3BAFFDBC1B7FFDBC0B5FFDBBEB3FFEBDB
          D3FF019301FF019301FF019301FF019301FF019301FF59F190FF53EB88FF3AD2
          60FF019301FF019301FF019301FF019301FF019301FF00000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFDBC7C2FFDBC7
          C3FFDBC7C3FFDBC6C2FFDBC5BFFFDBC4BCFFDBC3BAFFDBC1B7FFDBBFB3FFEDDF
          D8FFEBDAD3FFEBDAD1FFEBD9CFFFEBD8CEFF019301FF61F99BFF59F18FFF30C8
          4EFF019301FFB9A79DFF00000000000000000000000000000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFDBC8C3FFDBC8
          C4FFDBC8C4FFDBC8C2FFDBC6C0FFDBC4BDFFDBC3BAFFDBC2B8FFDBC0B5FFDBBE
          B2FFDBBDAFFFDBBBACFFDBBAA9FFEBD9CFFF019301FF63FB9EFF5AF292FF34CD
          54FF019301FFBAA69DFF00000000000000000000000000000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFDBC7C3FFDBC7
          C4FFDBC7C4FFDBC7C3FFDBC6C0FFDBC5BEFFDBC4BBFFDBC2B8FFDBC0B5FFDBBF
          B3FFDBBDAFFFDBBCACFFDBBAAAFFEBD8CEFF019301FF6DFFAEFF65FDA3FF3AD2
          5EFF019301FFBAA79EFF00000000000000000000000000000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFDCC7C2FFDBC6
          C3FFDBC6C3FFDBC6C1FFDBC5C0FFDBC4BDFFDBC3BBFFDBC1B9FFDBC0B5FFDBBF
          B2FFDBBEB0FFDBBCADFFDBBAAAFFEBD8CFFF019301FF019301FF019301FF0193
          01FF019301FFBBA79DFF00000000000000000000000000000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6C0FFDCC5BFFFDCC4BDFFDBC3BAFFDBC2B7FFDBC0B5FFDBBE
          B2FFDBBDAFFFDBBCADFFDBBAAAFFEDDDD3FFEBD9CFFFEDDBD1FFFDFAF5FFFDFA
          F5FFFAF1E8FFB9A59BFF00000000000000000000000000000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6BEFFDCC4BEFFDCC4BBFFDCC3BAFFDBC1B7FFDBC0B4FFDBBE
          B2FFDBBDAFFFDBBCADFFDBBBAAFFDBB9A7FFDBB9A7FFDEBDAAFFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8F2FFFCF8F1FFFCF7EFFFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCF9FFFDFB
          F8FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFE0CFCEFFE0D0
          CFFFE0CFCDFFDFCDCAFFDFCDCAFFDFCBC8FFDFC9C5FFDFC8C2FFDFC7BDFFDFC5
          BBFFDFC3B8FFDFC1B4FFDFC0B1FFDFBFACFFDFBFACFFE1C2B0FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF000000FF000000FF000000FF000000FF0000001F0000001F000
          0001F0000001F0000001F000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000070F9ED401017CB2BAA69DFF9E8174FF856251FF846150FF927364FFBAA6
          9CFFBAA69CFFB9A59CFF977B6DFF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF0000000000000000000000000000000000000000060F
          92C8112ED5FF1F4FFAFF0304A6FF8A84C9FFF2E5DFFFF5E5D7FFB9B0D7FF050D
          B2FF030EA9FF03039BFFA0A1D8FFF4E4D4FFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000010107070101
          97F12251F7FF2461FFFF225EFFFF0A1ABDFF4241A9FFB7B4DBFF0409ACFF1441
          EAFF1550FFFF104BFFFF0504A6FFFCF9F2FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF9EFDEFFF8ECDBFFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000101
          537C1026C8FF2961FFFF2459FFFF255FFFFF0E23C7FF010199FF1845EAFF184E
          FFFF144AFFFF1243EAFF0516B6FFFDFAF3FFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFFAF1E5FFF9F0E1FFF9EFE0FFFAF0
          DEFFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000910B5FF204AEAFF2B62FFFF255AFFFF245BFFFF1B45EAFF1E52FFFF1B50
          FFFF1744EAFF081CC2FF5E59B1FFE6D3C9FFDDC2B3FFDDC1B2FFDDC1B0FFDDBF
          AEFFDDBEABFFDDBDAAFFDDBCA8FFDDBAA5FFDDBAA5FFDFBEA9FFFAF1E2FFFAF0
          E1FFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000101273501018CF01A39DBFF2D64FFFF2658FFFF2355FFFF2054FFFF2056
          FFFF060DACFF544FAEFFE2D1CEFFDCC0B3FFDBBEB1FFDBBDAEFFDABBACFFDABA
          AAFFDAB9A8FFDAB7A5FFDAB6A2FFDAB5A0FFDAB5A0FFDDBAA4FFFAF2E5FFFBF3
          E4FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          0000000000000101151F1B1886FF1F43E5FF295EFFFF2658FFFF2559FFFF1838
          DCFF2A2895FFE8DAD4FFDCC3B7FFDBC0B5FFDBBFB2FFDBBEB1FFDBBDAFFFDBBC
          ACFFDABAAAFFDAB9A6FFDAB7A4FFDAB6A1FFDAB6A1FFDDBBA5FFFBF3E6FFFBF4
          E7FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000000000
          0000000000000000000009088DFF3369FFFF2E63FFFF2B61FFFF285EFFFF265F
          FFFF0406A1FF948CC7FFE2CFC7FFDBC2B8FFDBC0B6FFDBBFB3FFDBBEB1FFDBBD
          AEFFDBBCACFFDAB9A8FFDAB8A5FFDAB7A3FFDAB7A3FFDDBBA7FFFBF3E7FFFCF5
          E8FFF6E3D2FF815E4CFF00000000000000000000000000000000000000000000
          00000000000001012A3D0E1BADFF3972FFFF366FFFFF152DCCFF2657FDFF2A63
          FFFF2155FFFF0E0D97FFCEC2D7FFDDC6BEFFDBC1B7FFDBC0B5FFDBBEB3FFDBBD
          AFFFDBBCACFFDBBBAAFFDBBAA8FFDAB7A4FFDAB7A4FFDDBDA8FFFBF4E8FFFCF4
          E9FFF5E4D3FF805D4EFF00000000000000000000000000000000000000000000
          00000000000001018ADA2950EAFF3D76FFFF1C38D4FF302F9AFF070692FF1E48
          EAFF2964FFFF1434DEFF5752ABFFE5D4CFFFDBC3BAFFDBC1B7FFDBBFB3FFDBBE
          B1FFDBBCAFFFDBBBABFFDBBAA8FFDBB8A5FFDBB8A5FFDDBDAAFFFBF4E9FFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000000000000000
          0000000000000609A7FE3767EAFF2B55EAFF111198FFE4E2F0FFEBE0DDFF0303
          9FFF1F4BEAFF276AFFFF050CA6FFEBDEDAFFDBC3BAFFDBC2B8FFDBC0B5FFDBBE
          B2FFDBBDAFFFDBBBACFFDBBAA9FFDBB9A7FFDBB9A7FFDEBEAAFFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF00000000000000000000000000000000000000000000
          000000000000010166A51F36D1FF111FBBFF8F8ED7FFFDFCF9FFDBC7C3FFEBE0
          DEFF080EB4FF1D49F8FF0711B8FFEBDEDBFFDBC4BBFFDBC2B8FFDBC0B5FFDBBF
          B3FFDBBDAFFFDBBCACFFDBBAAAFFDBB8A6FFDBB8A6FFDEBEABFFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000000000000000
          000000000000000000006A5EA9FF3838BCFFF9F8FAFFFDFBF8FFDCC7C2FFDBC6
          C3FFEBDFDDFF3F3CBEFF7D77B8FFDFCECAFFDBC3BBFFDBC1B9FFDBC0B5FFDBBF
          B2FFDBBEB0FFDBBCADFFDBBAAAFFDBB8A7FFDBB8A7FFDEBDABFFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000000000
          00000000000000000000A8877AFFFFFFFFFFFEFCF9FFFEFCFAFFDCC6C0FFDCC6
          C0FFDBC5BFFFE7D9D5FFE3D1CCFFDCC4BDFFDBC3BAFFDBC2B7FFDBC0B5FFDBBE
          B2FFDBBDAFFFDBBCADFFDBBAAAFFDBB9A7FFDBB9A7FFDEBDABFFFCF6EDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6BEFFDCC4BEFFDCC4BBFFDCC3BAFFDBC1B7FFDBC0B4FFDBBE
          B2FFDBBDAFFFDBBCADFFDBBBAAFFDBB9A7FFDBB9A7FFDEBDAAFFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8F2FFFCF8F1FFFCF7EFFFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCF9FFFDFB
          F8FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFE0CFCEFFE0D0
          CFFFE0CFCDFFDFCDCAFFDFCDCAFFDFCBC8FFDFC9C5FFDFC8C2FFDFC7BDFFDFC5
          BBFFDFC3B8FFDFC1B4FFDFC0B1FFDFBFACFFDFBFACFFE1C2B0FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFC000000F8000000F8000000F8000000FC000000FE000000FF000
          000FF000000FF000000FE000000FE000000FE000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF1DCCBFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFF8F2E6FFE3DCD3FFE6DFD5FFF1E9DFFFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF9EFDEFFF8ECDBFFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFE7E3D8FFC8C2BBFFCEC8BFFFDCD5CDFFF5EF
          E3FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFFAF1E5FFF9F0E1FFF9EFE0FFFAF0
          DEFFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFDEC4B7FFDEC5
          B7FFDEC5B7FFDEC4B7FFDDC3B5FFB09B90FF97847BFF9F8B81FFBFA798FFDDBF
          AEFFDDBEABFFDDBDAAFFDDBCA8FFDDBAA5FFDDBAA5FFDFBEA9FFFAF1E2FFFAF0
          E1FFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFDBC0B4FFDBC0
          B4FFDBC0B4FFDBC0B4FFDBC0B3FF97847CFF7F6F67FF9F897FFFD8BAABFFDABA
          AAFFDAB9A8FFDAB7A5FFDAB6A2FFDAB5A0FFDAB5A0FFDDBAA4FFFAF2E5FFFBF3
          E4FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFDBC2B8FFDBC2
          B8FFDBC2B8FFDBC2B7FFDBC2B6FF6F6B69FF665D58FFB9A59CFFDABCAEFFDBBC
          ACFFDABAAAFFDAB9A6FFDAB7A4FFDAB6A1FFDAB6A1FFDDBBA5FFFBF3E6FFFBF4
          E7FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFDBC3BBFFDBC4
          BBFFDBC4BBFFDBC3BBFFDBC3B9FF82807FFF6A7C87FF5C626EFF656472FF828A
          76FFB6AF92FFDAB9A8FFDAB8A5FFDAB7A3FFDAB7A3FFDDBBA7FFFBF3E7FFFCF5
          E8FFF6E3D2FF815E4CFF00000000000000000000000000000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFDBC6BEFFDBC6
          BFFFDBC6BFFFDBC4BEFFDBC4BCFF9BA1A8FF90B7DBFF60917FFF169228FF20A4
          35FF158C20FF8EA773FFDBBAA8FFDAB7A4FFDAB7A4FFDDBDA8FFFBF4E8FFFCF4
          E9FFF5E4D3FF805D4EFF00000000000000000000000000000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFDBC7C2FFDBC7
          C3FFDBC7C3FFDBC6C2FFDBC5BFFFB5B6BDFF6BABABFF22B742FF5DF188FF4AD3
          6DFF36B44FFF198A23FF9FAC7DFFDBB8A5FFDBB8A5FFDDBDAAFFFBF4E9FFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFDBC8C3FFDBC8
          C4FFDBC8C4FFDBC8C2FFDBC6C0FFC8C0C4FF30AC58FF67FF99FF61F791FF56E5
          80FF45CC67FF33B14CFF1A8724FFACAE86FFDBB9A7FFDEBEAAFFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFDBC7C3FFDBC7
          C4FFDBC7C4FFDBC7C3FFDBC6C0FFD8C6C2FF3BC868FF64FF93FF66FF99FF5FF2
          8CFF53E17CFF43C964FF30AD48FF1F8626FFBAB08EFFDEBEABFFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFDCC7C2FFDBC6
          C3FFDBC6C3FFDBC6C1FFDBC5C0FFDBC4BDFF91CE95FF4CE276FF63FD93FF66FE
          98FF5DF18BFF53E07BFF41C45FFF2EA843FF25842AFFC6B899FFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6C0FFDCC5BFFFDCC4BDFFDBC3BAFF77C983FF53E880FF63FC
          93FF66FE98FF5CEF89FF51DC77FF3EC25DFF2BA33EFF30862FFFEAEEDDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6BEFFDCC4BEFFDCC4BBFFDCC3BAFFD7C2B4FF5DC275FF5AF2
          86FF64FD93FF65FD97FF5CED87FF4EDB76FF3CBE59FF279E39FF419746FFF2F1
          E5FFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFEFCFBFFFEFCFAFFEEF9EDFF51D1
          78FF5FF68AFF65FD95FF65FB94FF5BEB85FF4CD772FF3ABC57FF239A35FF53A0
          56FFF3E5D3FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCF9FFDCF7
          E2FF45D672FF60F98CFF65FD97FF64F993FF58E983FF4CD46FFF38B752FF1F96
          30FF5FA95BFF84624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFE0CFCEFFE0D0
          CFFFE0CFCDFFDFCDCAFFDFCDCAFFDFCBC8FFDFC9C5FFDFC8C2FFDFC7BDFFDFC5
          BBFFADCCA1FF45DC71FF60F98CFF66FF97FF63F892FF57E881FF44D76AFF37AC
          4EFF49734DFF796E61FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FF91CF90FF4DE779FF60F88EFF65FF98FF59FF8DFF62B178FF9A8A
          97FF9C959BFF757373FF33332F67000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFF74CA81FF51EA7EFF57FF8AFF84B695FFDFD0DAFFE2DF
          DFFFBFBDB4FF9B9A90FF444988F904062D390000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFAFDF6FF5FD983FF85C597FFF4E5F0FFFFFFFFFFFFFF
          F1FF9FA7D4FF203CD3FF1A30CAFF050B99E70101151F00000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFD1D5C4FFCBC1C6FFEFECEDFFFFFFFCFF98AA
          F0FF2E5BF2FF3B63EEFF2941D0FF121EB2FF010171BC00000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFB49889FFD2CFC5FFABB6E6FF2F5F
          F5FF4D7DFFFF3C61EAFF2740CDFF121FB2FF02017FD500000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF0DAB7FF264BE5FF4F82
          FFFF4672F9FF3554E1FF2438C8FF0B10AAFE0101436400000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF494393E63655
          E4FE395EE7FF2944D3FF0F19B0FE010167910000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C090812070B
          58770B128ED5020374BE01013754000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF0000007F0000007F0000003F0000001F000
          0001F0000001F0000003F0000087F00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF987B6DFF7B74ABFF1A24B7FF0C16
          B3FF0C16B0FF0C14AEFF0C13AAFF0C12A6FF0C10A4FF0C0F9FFF0C0D9CFF0C0C
          9AFF0C0C9BFF0C0C9BFF010191F001018AE40101355800000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF8ECE2FF1C2BC4FF354DDFFF011A
          D2FF011BCEFF0117CCFF0119CBFF0116C4FF0114C0FF0112BCFF010FB8FF010F
          B8FF0108AEFF010AACFF0108A7FF01049FFF01018BE600000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFCF8F2FF1A2ECDFF3858EFFF0123
          E5FF0120E2FF1A45F6FF557CF9FF011DDDFF011ED6FF011CD2FF0112CDFF678B
          FBFF395EECFF0108BAFF010FB8FF0107A5FF010193F300000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFDF9F4FF1C33D0FF3E61F4FF0122
          EDFF0536F9FFCBD1E4FFF5F2E1FF5177F9FF011BDFFF0117D8FF587CF9FFFFFF
          F9FFF5FAFFFF3155EAFF0108BAFF0109A9FF010294F300000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFDEC4B7FFDEC5
          B7FFDEC5B7FFDEC4B7FFDDC3B5FFDDC3B5FFECDED6FF1F34D4FF4568FCFF012E
          FAFFC5CAE1FFE7E5DFFFE7E7E4FFF9F5EAFF4C73F8FF436BF9FFFFFFF9FFFFFE
          FBFFFFFFFFFFF6FBFFFF385DEDFF0105ABFF010294F300000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFDBC0B4FFDBC0
          B4FFDBC0B4FFDBC0B4FFDBC0B3FFDBBFB2FFEBDCD4FF2038D7FF4970FFFF3C5D
          ECFFF5EFD9FFE4E4E1FFE6E6E6FFEEEEEBFFF5F4F0FFF5F4F6FFFEFDFAFFFFFF
          FFFFFFFFFFFFFFFFFFFF81A1FFFF0210B6FF010296F300000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFDBC2B8FFDBC2
          B8FFDBC2B8FFDBC2B7FFDBC2B6FFDBC0B5FFEBDCD5FF223DDCFF5781FFFF012C
          FFFF4664EDFFF5F1E1FFEAE9E7FFECECECFFF1F1F1FFF7F7F6FFFDFDFDFFFFFF
          FEFFFFFFFFFF7594FDFF0118D1FF010BB5FF01059AF300000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFDBC3BBFFDBC4
          BBFFDBC4BBFFDBC3BBFFDBC3B9FFDBC2B8FFEBDDD7FF263FDFFF638BFFFF0341
          FFFF0136FFFF4465F0FFF1EFE7FFEEEEEDFFF2F2F2FFF7F7F7FFFDFDFCFFFFFF
          FFFF6485FEFF0119DAFF011BD2FF0110B9FF01079EF300000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFDBC6BEFFDBC6
          BFFFDBC6BFFFDBC4BEFFDBC4BCFFDBC3BAFFEBDDD8FF2644E2FF6D97FFFF064A
          FFFF0447FFFF133CEEFFE3E5E8FFEDEDECFFF2F2F2FFF7F7F7FFFEFEFDFFFFFF
          FFFF3963FAFF011ADDFF011ED6FF0112BDFF0109A4F300000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFDBC7C2FFDBC7
          C3FFDBC7C3FFDBC6C2FFDBC5BFFFDBC4BCFFEBDED9FF2948E6FF77A3FFFF044F
          FFFF1B3CDFFFE8E6E1FFEAEAE6FFEDEDECFFF1F1F1FFF6F6F6FFFCFCFCFFFFFF
          FEFFFFFFFFFF4169FBFF011ADBFF0114C1FF010AA7F300000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFDBC8C3FFDBC8
          C4FFDBC8C4FFDBC8C2FFDBC6C0FFDBC4BDFFEBDED9FF2A4CEBFF7DA8FFFF1E39
          CFFFE4E2DAFFE6E4E0FFE6E6E5FFEDEDEAFFF7F5EFFFFDFDF4FFFCFCF9FFFEFE
          FEFFFFFFFFFFFFFFFFFF4D74FDFF0116C6FF010CABF300000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFDBC7C3FFDBC7
          C4FFDBC7C4FFDBC7C3FFDBC6C0FFDBC5BEFFEBDFDAFF2D4EEEFF85B3FFFF1B35
          C9FFE0DCD8FFE5E4E0FFE6E6E3FFFCF8E8FF3B5AEDFF4B6AF3FFFFFFF6FFFDFD
          FAFFFFFFFEFFFFFFFFFF3A63FDFF0118C9FF010EAFF300000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFDCC7C2FFDBC6
          C3FFDBC6C3FFDBC6C1FFDBC5C0FFDBC4BDFFEBDEDAFF2F51F1FF92C0FFFF1069
          FFFF1933C9FFE4E0DAFFF8F3E3FF4158DDFF013EFDFF0135FEFF506DF2FFFFFF
          F6FFFFFFFAFF214DFCFF011EE3FF011ACCFF0110B3F300000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6C0FFDCC5BFFFDCC4BDFFEBDED9FF3156F4FF9CC9FFFF1472
          FFFF116AFFFF1E37CAFF4251CDFF0146FCFF074BFFFF0342FFFF012BFDFF536F
          F1FF1D48F8FF0121EDFF0123E5FF0119CEFF0112B8F300000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6BEFFDCC4BEFFDCC4BBFFEBDDD8FF214BF5FFD2EEFFFF8ABE
          FFFF7FB4FFFF73A7FFFF6691F9FF6496FFFF5C8BFFFF5281FFFF4974FFFF3961
          FEFF365CFAFF3255F3FF2D4DEDFF2D43D9FF0719B0E400000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFEFCFCFFABB8FBFF2C54F9FF375B
          F6FF3559F4FF3357F1FF3254EFFF2F50ECFF2F4EE9FF2E4BE5FF2D49E4FF2844
          E1FF2741DCFF2038D2FF0D24BFE7051BB0DB0108405100000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFCFFFEFDFBFFFEFD
          FBFFFEFDFAFFFEFCFAFFFEFCF9FFFEFCF8FFFDFBF7FFFDFAF7FFFDFAF6FFFEFA
          F6FFFAF1E9FFBAA69BFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFE0CFCEFFE0D0
          CFFFE0CFCDFFDFCDCAFFDFCDCAFFDFCBC8FFDFC9C5FFDFC8C2FFDFC7BDFFDFC5
          BBFFDFC3B8FFDFC1B4FFDFC0B1FFDFBFACFFDFBFACFFE1C2B0FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF0000001F0000001F0000001F0000001F0000001F0000001F000
          0001F0000001F0000001F0000001F0000001F0000001F0000001F0000001F000
          0001F0000001F000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000846251FF866251FF846150FF846150FF8361
          50FF836150FF836150FF82604FFF815F4EFF815F4EFF815E4FFF805E4DFF805E
          4EFF7F5D4CFF7F5D4DFF7E5C4CFF7F5E4EFF7C5C4CFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000C3B1A9FFFAECE0FFF6E8DBFFF6E8DBFFF2DE
          CCFFF2DECCFFF2DFCCFFF2DDCBFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9
          C2FFF2D8C0FFF0D7BEFFF1D6BDFFF5D9BDFF7D5C4CFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000115013101300176014301A5317A2BFF398737FFBCD3B4FFFBF7F0FFFAF3
          E9FFFAF3E8FFFAF4E8FFFAF2E7FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0
          E0FFF9EFDEFFF8ECDBFFF9EDD9FFF4DCC4FF7C5B4AFF00000000000000000000
          0000000000000000000000000000000000000000000000000000011D01430154
          01CD016802FE07800AFF047405FF136F12FFC6DDC1FFFCF6ECFFFCF6ECFFFCF6
          ECFFFBF5ECFFFBF5ECFFFBF5EAFFFBF4E9FFFBF4E8FFFBF3E8FFFBF3E7FFFAF2
          E6FFFAF2E6FFFAF1E2FFFAF0E1FFF3DFC9FF7D5C4BFF00000000000000000000
          0000000000000000000000000000000000000000000001320171016901FD0F93
          16FF18B325FF16AB21FF016501F6A0A68AFFFFFFF7FFFCF6EEFFFCF6EEFFFCF6
          EEFFFCF6EDFFFCF6EDFFFBF5ECFFFBF5EAFFFBF4EAFFFBF4E9FFFBF3E8FFFBF3
          E7FFFAF3E6FFFAF2E5FFFBF3E4FFF5E0CCFF7E5D4CFF00000000000000000000
          0000000000000000000000000000000000000124014B026E02FD1BAE29FF20B9
          30FF21BB31FF0F9016FF014201978A6554FFFFFFFAFFFCF8F1FFFCF8F1FFFCF8
          F1FFFCF8F0FFFCF7F0FFFCF7EFFFFCF6EDFFFBF5ECFFFBF5EBFFFBF5EAFFFBF4
          E9FFFBF4E8FFFBF3E7FFFCF5E8FFF6E3D2FF815E4CFF00000000000000000000
          000000000000000000000000000000000000015F01D219A325FF27C03AFF27BF
          3AFF2AC33EFF0A8610FF012B015A8B6654FFFFFFFBFFFDF9F2FFFDF9F2FFFCF8
          F2FFFCF8F2FFFCF8F2FFFCF7F0FFFCF7EEFFFCF6EDFFFCF6EDFFFBF5ECFFFBF5
          EBFFFBF4E9FFFBF4E8FFFCF4E9FFF5E4D3FF805D4EFF00000000000000000000
          00000000000000000000000000000116012A027902FE30C947FF2EC645FF2FC7
          46FF31CB4AFF0A870EFF012401478F6756FFFFFFFFFFFDFAF5FFFDFAF5FFFDFA
          F5FFFDFAF5FFFDF9F5FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFCF6EEFFFCF6
          EDFFFBF5EBFFFBF5EAFFFCF5EBFFF5E4D3FF825E4DFF00000000000000008462
          51FF866251FF846150FF846150FF94BA8CFF0D8F13FF37D153FF36CE50FF36CE
          51FF3AD356FF0C8D11FF9FBF96FF916956FFFFFFFFFFFDFBF8FFFEFCF9FFFDFB
          F8FFFDFBF8FFFDFBF7FFFDFAF5FFFDF9F3FFFCF8F2FFFCF8F1FFFCF7F0FFFCF7
          EFFFFCF6EDFFFBF5ECFFFCF7EDFFF5E5D6FF835F4EFF00000000000000008562
          51FFFAECE0FFF6E8DBFFF6E8DBFF94C083FF139B1DFF40D95FFF3DD55CFF3ED6
          5CFF41DA61FF119718FF9CBD80FFCAB7ACFFFFFFFFFFFEFEFDFFFEFEFDFFFEFD
          FDFFFEFDFCFFFEFDFBFFFDFBF9FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8F2FFFCF8
          F1FFFCF7EFFFFCF6EEFFFDF8EFFFF7E6D6FF84604FFF00000000000000008662
          51FFA1D499FF65B65EFF5FB259FF389B34FF119A19FF48E06BFF45DD67FF46DE
          68FF48E16DFF19A524FF1D901BFF1F8E18FF269F26FF84C884FFFFFFFFFFFEFE
          FEFFFEFEFEFFFEFDFDFFFEFCF9FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8
          F1FFFCF7F0FFFCF7EFFFFDF7EFFFF6E6D8FF85624FFF00000000000000008764
          52FFF2F8EAFF3DA239FF038C04FF2CBE42FF34C74DFF4DE674FF4DE573FF4DE5
          74FF4FE777FF43DA65FF3ED25EFF14A31DFF37A437FFF8FCF8FFFFFFFFFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFCFAFFFDFBF8FFFDFAF6FFFDFAF5FFFDF9F3FFFCF8
          F2FFFCF8F1FFFCF7EFFFFDF7F0FFF7E7D9FF866251FF00000000000000008863
          52FFFFFFF7FFF1F2E4FF44AB40FF16A620FF57EF82FF55ED80FF55ED7FFF55ED
          80FF56EE80FF5AF285FF3AD158FF12930FFFE2F5E2FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFDFCFFD4EDCFFF73C270FFFEFDF8FFFFFDF7FFFEFB
          F7FFFDF8F3FFF9F5EDFFF5F0E8FFE9D6C8FF866150FC00000000000000008A65
          54FFFFFFFAFFFCF8F1FFF4F4E9FF4BB148FF13A81CFF5DF58AFF5DF58BFF5DF5
          8AFF5FF78EFF52EA7BFF059905FFA1C391FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0F8EEFF28A727FF079B0AFF54AB4BFFE6DED8FFD9CB
          C4FFD5C5BEFFC8B6ADFFBBA49AFF977666FF60473BB600000000000000008B66
          54FFFFFFFBFFFDF9F2FFFDF9F2FFF6F6ECFF52B84FFF12AB1AFF64FC95FF67FF
          9AFF66FE97FF0BA30FFF85CF7EFF9A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFEFF52BA51FF1FB82EFF69FF9CFF11AA18FF44A139FFDCD3
          CAFFA38068FFB08867FF936D55FF6B4E41C40806050E00000000000000008F67
          56FFFFFFFFFFFDFAF5FFFDFAF5FFFDFAF5FFF9F8F1FF59BC57FF11AA18FF69FF
          9CFF1FB82EFF51B94DFFFBF5EDFF9B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF87D587FF0BA20FFF66FE97FF67FF9AFF64FC95FF12AB1AFF52B5
          48FFF4E1B6FFC59E6FFF684C40C10805050C0000000000000000000000009169
          56FFFFFFFFFFFDFBF8FFFEFCF9FFFDFBF8FFFDFBF8FFFAF9F4FF61BD5EFF079C
          0AFF28A726FFEEF3E4FFFCF7F0FF9C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBBE7BBFF059A05FF52EA7BFF5FF78EFF5DF58AFF5DF58BFF5DF58AFF13A8
          1CFF3F9E2DFFB3A89FE80906050E00000000000000000000000000000000946D
          58FFFFFFFFFFFEFEFDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFCFBF8FF72C0
          6FFFD2EBCCFFFDF9F4FFFCF8F2FF9C735DFFFFFFFFFFFFFFFFFFFFFFFFFFE2F5
          E2FF169816FF3AD158FF5AF285FF56EE80FF55ED80FF55ED7FFF55ED80FF57EF
          82FF16A620FF046C03C00108010B00000000000000000000000000000000956C
          5AFFFFFFFFFFFEFEFEFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFCF9FFFDFB
          F7FFFDFAF6FFFDF9F4FFFDF9F3FF9D725EFFFFFFFFFFFEFEFEFFF8FCF8FF37A3
          37FF14A31DFF3ED25EFF43DA65FF4FE777FF4DE574FF4DE573FF4DE674FF34C7
          4DFF2CBE42FF038C04FF016701C20108010D000000000000000000000000986F
          5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFDFB
          F9FFFDFBF7FFFDFAF6FFFDF9F4FF9B725CFFFFFFFFFFFEFEFDFF84C884FF269E
          26FF2B9F2BFF1E921EFF19A524FF48E16DFF46DE68FF45DD67FF48E06BFF119A
          19FF016501C70157019F015501990135015E0000000000000000000000009971
          5BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFFFD
          F9FFFEFDF9FFFFFDF9FFFFFDF7FF9C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA5D5A5FF119718FF41DA61FF3ED65CFF3DD55CFF40D95FFF139B
          1DFF013601660000000000000000000000000000000000000000000000009A70
          5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFFFD
          FBFFE6DCD7FFD9CBC2FFE0D3CCFF9A725EFF9D7360FF9A725EFF9A725EFF9871
          5DFF98705BFF9BB78DFF0C8D11FF3AD356FF36CE51FF36CE50FF37D153FF0D8F
          13FF012D01570000000000000000000000000000000000000000000000009B71
          5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFFFF
          FFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D55FF6B4E
          41C40806050E012401470A870EFF31CB4AFF2FC746FF2EC645FF30C947FF0279
          02FE0116012A0000000000000000000000000000000000000000000000009C73
          5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFFFFFF
          FFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C40C10805
          050C00000000012B015A0A8610FF2AC33EFF27BF3AFF27C03AFF19A325FF015F
          01D2000000000000000000000000000000000000000000000000000000009E73
          5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFFFF
          FCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906050E0000
          000000000000014201970F9016FF21BB31FF20B930FF1BAE29FF026D02FD0124
          014B000000000000000000000000000000000000000000000000000000009C73
          5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFCFAFFFFFE
          FCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C090812000000000000
          00000116012F016401F616AB21FF18B325FF0F9316FF016801FD013201710000
          0000000000000000000000000000000000000000000000000000000000009D72
          5EFFFFFFFFFFFEFEFEFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFCFAFFFFFF
          FBFFD3C2B9FFB7906BFFCBA170FF705144CD0B08061100000000000000000119
          0139015C01E9047405FF07800AFF016802FE015401CD011D0143000000000000
          0000000000000000000000000000000000000000000000000000000000009B72
          5CFFFFFFFFFFFEFEFDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFDFBF9FFFFFE
          FBFFCEBAB1FFA27A5BFF795949D6100C0A190000000000000000011B01400150
          01C7014E01C0014301A501300176011501310000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009C72
          5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB39789FF775647D40F0B0A17000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009A72
          5EFF9D7360FF9A725EFF9A725EFF98715DFF98705BFF976E5CFF946C5AFF936D
          5AFF684B3EB90E0A081600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFF80001FFF80001FFE00001FF800001FE000001FE000001FC00
          0001FC0800018000000180000001800000018000000180000001800000018000
          0003800000078000000F8000000F80000007800000038000001F8000001F8000
          303F8000603F8000E07F8001E07F8003C1FF800787FF800FFFFF801FFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000D0D
          0D0C5C5C5C5B828282827C7C7C7B484848470404040300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF8F6F5FFFBBAA
          A3FF706C6BFF4B4F51FF4A4B4EFF78716EFFB9A69DFF836251FF82604FFF8160
          4FFF815F4EFF81604FFF8F7265FF927667FF836253FF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF3E1D0FFECE4DCFF7476
          77FF89786DFFC3987EFFC59E87FF7C7977FF706F6EFFF7EBE0FFF2DDCBFFF2DE
          CAFFF3DECEFFF5E9DEFFD1C9C2FFC5BDB6FFEDE3D9FFF4DFCCFFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF5EAFFF0ECE8FF767776FF8379
          71FFF9C08DFFF3BF8EFFF0BB8BFFEBB589FF6C6A6AFFBDBBB8FFFBF4EBFFFBF4
          E9FFECE8E3FF737575FF5E5F60FF68686AFF4E5254FFBCBAB5FFFAF1E4FFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF6FFF1F0EAFF696868FF707376FFCAA7
          87FFFFDAA9FFFFD7ABFFFFD5A9FFFFD29CFFC9AA8EFF787A7BFFFDF9F4FFE9E6
          E1FF727576FF978170FFE5AB81FFE0A87EFFC8A791FF515759FFD6D2CDFFFBF2
          E1FFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000947564FFEFEFEBFF696867FF6E6E6EFF909396FFCAA9
          91FFFFE7C5FFFFE3CBFFFFE2C5FFFFDBB4FFF8CBA0FF494E52FFABA9A6FF5D5D
          5DFF7A7876FFFFCF97FFFFD09DFFFECE99FFFCC490FFB39F8CFF757575FFFCF5
          ECFFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000000000025252524B8A9A2FF5E5E5EFF707070FF949494FFA4A7A8FFAC99
          92FFFFE6D3FFFFFAEFFFFFF0E0FFFFE7C8FFE3C09FFF3D4144FF3D3D3DFF8889
          8BFFA2948BFFFFE1B4FFFFDDBDFFFFDCBAFFFFD8ABFFF4C89DFF52565AFFFDF9
          F1FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          00002F2F2F2E8080809D525150FF424242FF959595FFAAAAAAFFBBBBBCFFBAB9
          BCFFB5978EFFF2E8E6FFFFF6E9FFFFF1D2FF84796FFF3C3E3FFF919191FFAAAB
          ACFFA09896FFFFE1C5FFFFF1DFFFFFECD8FFFFE2C3FFFFD3A7FF53575AFFFDF9
          F3FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000003A3A
          3A397D7D7DA33E3E3EF6616161FF525252FF848484FFC7C7C7FFD3D3D3FFE4E5
          E5FFA8A8A9FF957E7CFFA58981FF81706CFF4F5153FF545455FFB6B6B6FFBFBF
          BFFFBABDBFFFC3A89CFFFFF9F5FFFFFFF7FFFFF4D7FFCAAE95FF838484FFFDF9
          F0FFF6E3D2FF815E4CFF00000000000000000000000000000000252525247777
          77A9393939FA9A9A9AFFEEEEEEFF7C7C7CFF505050FFBBBBBBFFF4F4F4FFFFFF
          FFFFFFFFFFFF909393FF676A6AFF727474FF686868FF313131FFCECECEFFD4D4
          D4FFE6E8E8FFB4B1B2FFA98E89FFD4B5A8FFC5A594FF6B6A6BFFE0DED9FFFCF5
          EBFFF5E4D3FF805D4EFF00000000000000000000000000000000747474734C4C
          4CEB3A3A3AFFEDEDEDFF848484FFB5B5B5FFC2C2C2FF5E5E5EFF909090FFB3B3
          B3FF5B5B5BFF404040FF525252FF6B6B6BFF5E5E5EFF616161FFEDEDEDFFF4F4
          F4FFFFFFFFFFFFFFFFFFBABDBEFF6F6F70FF9F9E9FFFE5E3DFFFFCF6EDFFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000888888884949
          49FC494949FF8B8B8BFFBCBCBCFFCCCCCCFFCACACAFFC7C7C7FF9A9A9AFF7676
          76FF5F5F5FFF505050FF4E4E4EFF3E3E3EFF3B3B3BFF7D7D7DFFBEBEBEFFD8D8
          D8FFC1C1C1FF807F7FFF616160FFD2D0CDFFFDFAF5FFFBF6EDFFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF000000000000000000000000000000006F6F6F6E5A5A
          5AE6515151FF797979FFD6D6D6FFCBCBCBFFBEBEBEFFB6B6B6FFB2B2B2FFAFAF
          AFFFABABABFFA3A3A3FF959595FF8A8A8AFF7F7F7FFF727272FF6A6A6AFF3636
          36FF464646FFC0BFBDFFFDFBF7FFFCF8F1FFFCF6EEFFFCF6ECFFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000212121207F7F
          7FA55E5E5EFA5A5A5AFF575757FF898989FFA5A5A5FFB1B1B1FFAFAFAFFFA6A6
          A6FF9E9E9EFF9E9E9EFF9F9F9FFFA2A2A2FFA8A8A8FF919191FF575757FFA3A2
          A1FFF9F7F4FFFDF9F4FFFCF8F1FFFCF7F0FFFCF7EFFFFCF6EDFFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000002F2F
          2F2E86868699797979C39D938FFFD2D2D2FFB0AFAEFF8D8C8CFF777777FF6C6C
          6CFF747474FF7C7C7CFF808080FF7E7E7EFF5E5E5EFF818180FFEBEAE8FFFEFB
          F8FFFDF9F4FFFDF9F3FFFCF8F2FFFCF8F1FFFCF7EFFFFCF6EEFFFCF6EDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000000
          00001414141345454544AF9182FFFFFFFFFFFEFDFCFFFEFEFDFFFFFEFEFFF1F0
          F0FF9B9A9AFF5E5E5EFF696969FFADADACFFE0E0DEFFFEFDFBFFFDFBF7FFFDFA
          F6FFFDFAF5FFFDF9F4FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFEFFFFFEFEFFFEFEFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8F2FFFCF8F1FFFCF7EFFFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCF9FFFDFB
          F8FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFDFBF8FFFDFAF6FFFDFAF5FFFDF9F3FFFCF8F2FFFCF8F1FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFDFBF8FFFDFBF7FFFDFAF5FFFDF9F4FFFDF9F2FFFCF8F1FFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFDFBF9FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFC7FFFFF000000FF000000FF000000FF000000FF000000FF000000FE000
          000FC000000F8000000F0000000F0000000F0000000F8000000FC000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF9EFDEFFF8ECDBFFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFFAF1E5FFF9F0E1FFF9EFE0FFFAF0
          DEFFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFDEC4B7FFDEC5
          B7FFDEC5B7FFDEC4B7FFDDC3B5FFDDC3B5FFDDC2B3FFDDC1B2FFDDC1B0FFDDBF
          AEFFDDBEABFFDDBDAAFFDDBCA8FFDDBAA5FFDDBAA5FFDFBEA9FFFAF1E2FFFAF0
          E1FFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFE1CBC1FFDDC3
          B8FFDBC0B4FFDBC0B4FFDBC0B3FFDBBFB2FFDBBEB1FFDBBDAEFFDABBACFFDABA
          AAFFDAB9A8FFDAB7A5FFDAB6A2FFDAB5A0FFDAB5A0FFDDBAA4FFFAF2E5FFFBF3
          E4FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFDF8F2FF8FB880FFCFD2
          BEFFDFC8BFFFDBC2B7FFDBC2B6FFDBC0B5FFDBBFB2FFDBBEB1FFDBBDAFFFDBBC
          ACFFDABAAAFFDAB9A6FFDAB7A4FFDAB6A1FFDAB6A1FFDDBBA5FFFBF3E6FFFBF4
          E7FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFDFAF4FF74B46CFF2C9D
          31FFD1D6C1FFDFC9C2FFDBC3B9FFDBC2B8FFDBC0B6FFDBBFB3FFDBBEB1FFDBBD
          AEFFDBBCACFFDAB9A8FFDAB8A5FFDAB7A3FFDAB7A3FFDDBBA7FFFBF3E7FFFCF5
          E8FFF6E3D2FF815E4CFF00000000000000000000000000000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFDFAF5FF75B46DFF3FC2
          6FFF2A9C30FFD0D6C2FFDFCAC3FFDBC3BAFFDBC1B7FFDBC0B5FFDBBEB3FFDBBD
          AFFFDBBCACFFDBBBAAFFDBBAA8FFDAB7A4FFDAB7A4FFDDBDA8FFFBF4E8FFFCF4
          E9FFF5E4D3FF805D4EFF00000000000000000000000000000000000000000000
          00000000000000000000B1988CFFFFFFFEFFFEFBF7FFFEFBF8FF75B26EFF3EBF
          6EFF3FBE6DFF2A9F2FFFD1D8C4FFDFCAC3FFDBC3BAFFDBC1B7FFDBBFB3FFDBBE
          B1FFDBBCAFFFDBBBABFFDBBAA8FFDBB8A5FFDBB8A5FFDDBDAAFFFBF4E9FFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000000000000250
          02A9045E09B2045E07B140953AFF53B055FF50B151FF4FB053FF208E1FFF48E6
          80FF46D979FF40BD6FFF2CA22FFFD2D9C4FFDFC9C1FFDBC2B8FFDBC0B5FFDBBE
          B2FFDBBDAFFFDBBBACFFDBBAA9FFDBB9A7FFDBB9A7FFDEBEAAFFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF00000000000000000000000000000000000000001182
          1FEB3DC270FF39BD69FF3ABE6BFF3CBD6CFF3DBD6DFF3FBE6EFF3EBA6CFF42CB
          72FF4CE880FF4ADA7BFF43BC70FF2DA530FFD2D9C2FFDFC8BFFFDBC0B5FFDBBF
          B3FFDBBDAFFFDBBCACFFDBBAAAFFDBB8A6FFDBB8A6FFDEBEABFFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000000000000E83
          1EED4FE183FF3DD871FF40DB74FF44DF78FF45E079FF49E47DFF4BE67FFF4CE6
          80FF4DE681FF4FEA83FF4EDD80FF46BC72FF2CA831FFD2DAC1FFDFC6BCFFDBBF
          B2FFDBBEB0FFDBBCADFFDBBAAAFFDBB8A7FFDBB8A7FFDEBDABFFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000000D82
          1DEE5FE38EFF3DD771FF41D974FF44DC77FF47DF7AFF49E17CFF4BE37EFF4DE5
          80FF50E882FF51E983FF55EF88FF52E182FF48BD74FF2DAA2FFFD1D5BFFFDDC1
          B6FFDBBDAFFFDBBCADFFDBBAAAFFDBB9A7FFDBB9A7FFDEBDABFFFCF6EDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000E86
          1EF16CE998FF3CD870FF43DB76FF46DE79FF49E17CFF4BE37EFF4EE681FF50E8
          82FF52EA84FF54EC86FF56EE88FF59F48CFF57E488FF4EC179FF48AF42FFE7D3
          CBFFDBBDAFFFDBBCADFFDBBBAAFFDBB9A7FFDBB9A7FFDEBDAAFFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000C87
          1CF27CEBA3FF3ADA6FFF42DD76FF45E079FF48E37DFF4AE57EFF4DE881FF4FEA
          83FF55ED87FF57EF89FF59F18BFF5FF993FF5DED92FF32AF36FFE5F3E3FFFDFB
          F7FFFDFAF6FFFDFAF5FFFDF9F4FFFCF8F2FFFCF8F1FFFCF7EFFFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000D88
          1DF28EF0B2FF5DE088FF64E38DFF68E792FF6BE894FF6EEC97FF71ED9AFF76F1
          9EFF5DF08DFF5AF38CFF5FF793FF64FD9AFF37BA3CFFE5F6E4FFFEFCFAFFFDFB
          F8FFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFCF7F0FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF0000000000000000000000000000000000000000118C
          24EF73F5A6FF75F0A2FF79F3A6FF7DF4A9FF7FF8ABFF83F9B1FF82FBADFF9FF8
          BEFF5BF28DFF5FF792FF65FF9BFF33B337FFD8DDCBFFE2CDC7FFDFC7BDFFDFC5
          BBFFDFC3B8FFDFC1B4FFDFC0B1FFDFBFACFFDFBFACFFE1C2B0FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000153
          01A1026208AE026505AE409D3AFF51B956FF50BB55FF51BB55FF249523FF80F7
          A8FF5CF891FF65FF9BFF32B038FFD5DBC9FFDFCAC5FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000B99D90FFFFFFFFFFFFFFFFFFFFFFFFFF75B570FF77FD
          A7FF64FE9AFF33B038FFD5DCC9FFDFCCC8FFDCC5C0FFDCC3BCFFDCC2B9FFDCC0
          B5FFDBBEB2FFDBBDAFFFDBBAAAFFDBB9A7FFDBB9A7FFDEBEABFFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFF7EC77EFF6AFF
          A1FF35B33CFFE5F6E5FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFF7ECC7EFF36B4
          41FFE6F6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFA2DCA2FFE5F4
          E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000F8000000F8000000F8000000F8000000F8000
          000F8000000F8000000F8000000F8000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF9EFDEFFF8ECDBFFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFFAF1E5FFF9F0E1FFF9EFE0FFFAF0
          DEFFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFDEC4B7FFDEC5
          B7FFDEC5B7FFDEC4B7FFDDC3B5FFDDC3B5FFDDC2B3FFDDC1B2FFDDC1B0FFDDBF
          AEFFDDBEABFFDDBDAAFFDDBCA8FFDDBAA5FFDDBAA5FFDFBEA9FFFAF1E2FFFAF0
          E1FFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFDBC0B4FFDBC0
          B4FFDBC0B4FFDBC0B4FFDBC0B3FFDBBFB2FFDBBEB1FFDBBDAEFFDABBACFFDABA
          AAFFDAB9A8FFDAB7A5FFDAB6A2FFDAB5A0FFE0C1AFFFDFBEA9FFFAF2E5FFFBF3
          E4FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFDBC2B8FFDBC2
          B8FFDBC2B8FFDBC2B7FFDBC2B6FFDBC0B5FFDBBFB2FFDBBEB1FFDBBDAFFFDBBC
          ACFFDABAAAFFDAB9A6FFDAB7A4FFE0C2B0FF8EB274FFCECCB1FFFBF4E8FFFBF4
          E7FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFDBC3BBFFDBC4
          BBFFDBC4BBFFDBC3BBFFDBC3B9FFDBC2B8FFDBC0B6FFDBBFB3FFDBBEB1FFDBBD
          AEFFDBBCACFFDAB9A8FFDAB8A5FFE2C7B7FF73B064FF2C9C2FFFE0ECD6FFFCF6
          EAFFF6E3D2FF815E4CFF00000000000000000000000000000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFDBC6BEFFDBC6
          BFFFDBC6BFFFDBC4BEFFDBC4BCFFDBC3BAFFDBC1B7FFDBC0B5FFDBBEB3FFDBBD
          AFFFDBBCACFFDBBBAAFFDBBAA8FFE2C7B8FF74AE65FF3FC26FFF2CA033FFDFED
          D6FFF6E7D7FF805D4EFF00000000000000000000000000000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFDBC7C2FFDBC7
          C3FFDBC7C3FFDBC6C2FFDBC5BFFFDBC4BCFFDBC3BAFFE5D3CCFFE6D2CAFFE6D1
          C8FFE6D0C7FFE6D0C4FFE6CFC3FFE8D2C6FF74AD65FF3EBF6EFF3FBE6DFF2CA3
          32FFDDE5CBFF8B6E5DFF00000000000000000000000000000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFDBC8C3FFDBC8
          C4FFDBC8C4FFDBC8C2FFDBC6C0FFDBC4BDFFE5D4CEFF519A4AFF4BA04AFF4CA0
          48FF4CA247FF4DA447FF4BA745FF49A646FF208C1CFF48E680FF46D979FF40BD
          6FFF2EA430FFA3A387FF00000000000000000000000000000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFDBC7C3FFDBC7
          C4FFDBC7C4FFDBC7C3FFDBC6C0FFDBC5BEFFE9DBD6FF239230FF3DC270FF39BD
          69FF3ABE6BFF3CBD6CFF3DBD6DFF3FBE6EFF3EBA6CFF42CB72FF4CE880FF4ADA
          7BFF43BC70FF259C26FF0114011C000000000000000000000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFDCC7C2FFDBC6
          C3FFDBC6C3FFDBC6C1FFDBC5C0FFDBC4BDFFE9DBD6FF1F922DFF4FE183FF3DD8
          71FF40DB74FF44DF78FF45E079FF49E47DFF4BE67FFF4CE680FF4DE681FF4FEA
          83FF4EDD80FF46BC72FF0A8811DA0115011C0000000000000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6C0FFDCC5BFFFDCC4BDFFEADBD6FF1D902BFF5FE38EFF3DD7
          71FF41D974FF44DC77FF47DF7AFF49E17CFF4BE37EFF4DE580FF50E882FF51E9
          83FF55EF88FF52E182FF48BD74FF0B8B10DB0112011C00000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFDCC6C0FFDCC6
          C0FFDCC6C0FFDCC6BEFFDCC4BEFFDCC4BBFFEADBD6FF1B912AFF6CE998FF3CD8
          70FF43DB76FF46DE79FF49E17CFF4BE37EFF4EE681FF50E882FF52EA84FF54EC
          86FF56EE88FF59F48CFF57E488FF4EC179FF016C01B200000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFDFDFFFEFDFCFFFEFDFBFFFEFDFDFF199429FF7CEBA3FF3ADA
          6FFF42DD76FF45E079FF48E37DFF4AE57EFF4DE881FF4FEA83FF55ED87FF57EF
          89FF59F18BFF5FF993FF5DED92FF0A870ED70110011900000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFEFDFF1A952AFF8EF0B2FF5DE0
          88FF64E38DFF68E792FF6BE894FF6EEC97FF71ED9AFF76F19EFF5DF08DFF5AF3
          8CFF5FF793FF64FD9AFF0E9213D6011301190000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFE0CFCEFFE0D0
          CFFFE0CFCDFFDFCDCAFFDFCDCAFFDFCBC8FFECDFDCFF209A31FF73F5A6FF75F0
          A2FF79F3A6FF7DF4A9FF7FF8ABFF83F9B1FF82FBADFF9FF8BEFF5BF28DFF5FF7
          92FF65FF9BFF2BA92CFF01110118000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFE5D5D1FF58A451FF4DA74DFF4CAA
          49FF4BAA48FF4BAE48FF4AAF46FF4BAF46FF249320FF80F7A8FF5CF891FF65FF
          9BFF34B339FFA7A38AFF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFDDCBC9FFDDCB
          CBFFDDCAC9FFDDC9C6FFDDC9C6FFDCC7C3FFDCC5C0FFE5D3CEFFE6D4CEFFE6D3
          CBFFE6D1C9FFE6D1C7FFE6CFC4FFE8D3C7FF74AF65FF77FDA7FF64FE9AFF35B4
          3BFFE1EAD1FF906F5FFF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF9FF7EC67CFF6AFFA1FF34B23BFFE0EE
          D9FFEBD9CDFF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE7DDD7FF73BD6DFF33AF3BFFC9D0BBFFC1AB
          A2FF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFB59C8DFF78A05EFFB3AE91FFB79274FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFFAE6C6FFF8D8A0FFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF0000007F0000003F000
          0001F0000003F0000007F000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFF7F5D4DFF7E5C
          4CFF7F5E4EFF7C5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF0D7BEFFF1D6
          BDFFF5D9BDFF7D5C4CFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF9EFDEFFF8ECDBFFF9ED
          D9FFF4DCC4FF7C5B4AFF00000000000000000000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFFAF1E5FFF9F0E1FFF9EFE0FFFAF0
          DEFFF4DEC6FF7E5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFFCF6ECFFFCF6
          ECFFFCF6ECFFFCF6ECFFFBF5ECFFF0DBC6FFCA8B56FFC97126FFC86C1EFFC275
          39FFDCB293FFFAF3E7FFFBF3E8FFFBF3E7FFFAF2E6FFFAF2E6FFFAF1E2FFFAF0
          E1FFF3DFC9FF7D5C4BFF00000000000000000000000000000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFFCF6EEFFFCF6
          EEFFFCF6EEFFFCF6EEFFE9BE97FFD47B25FFFFA547FFFFA642FFFFA43AFFFFA4
          33FFEC821DFFCA814DFFFAEFE4FFFBF3E8FFFBF3E7FFFAF3E6FFFAF2E5FFFBF3
          E4FFF5E0CCFF7E5D4CFF00000000000000000000000000000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFEAC59EFFE0A9
          73FFFBF4EAFFEBC8A4FFDE8730FFFFB15BFFFFAA50FFF69A3CFFCE7522FFD086
          49FFD1803CFFC5610DFFCD8F66FFFBF4E9FFFBF4E8FFFBF3E7FFFBF3E6FFFBF4
          E7FFF4E2CEFF7F5C4DFF00000000000000000000000000000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFD58B3CFFF9B7
          75FFD49A5FFFD78834FFFFBD76FFFFB160FFF09A42FFD29659FFF9F0E4FFFCF6
          EEFFFCF6EDFFF0D8C4FFB4632AFFF5E3D2FFFBF4E9FFFBF4E8FFFBF3E7FFFCF5
          E8FFF6E3D2FF815E4CFF00000000000000000000000000000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFD9974DFFFFD4
          A2FFE6A255FFFCC286FFFFC385FFFDB46BFFD49249FFFBF6EFFFFCF7F0FFFCF7
          EFFFFCF7EEFFFCF6EDFFF5E5D8FFF7ECE0FFFBF5EBFFFBF4E9FFFBF4E8FFFCF4
          E9FFF5E4D3FF805D4EFF00000000000000000000000000000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFDBA25DFFFFD9
          AFFFFFD1A1FFFFCB98FFFFCA92FFDB8F38FFF7EADAFFFCF8F2FFFCF8F1FFFCF8
          F0FFFCF7F0FFFCF7EFFFFCF6EEFFFCF6EDFFFBF5ECFFFBF5EAFFFBF4E9FFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFE3B783FFFBD5
          AEFFFFD5ABFFFFD09FFFFFCD97FFD4852AFFDBA366FFF9EAD8FFFDF9F3FFFCF8
          F2FFFCF8F1FFFCF7F0FFF9EFE6FFF0DACBFFF5E4D7FFFBF5EBFFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFEED0ABFFF4CC
          9DFFFFE2C5FFFFD3A8FFFFCE9CFFFFCC93FFFFB971FFDBA66EFFF2DECEFFD48D
          54FFD3864CFFD48345FFCD844EFFD8996AFFC1743FFFF4E3D5FFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFFAF1E4FFE3A1
          47FFEFBD7BFFE6A753FFECA444FFE9A34AFFE2A052FFF4E0C9FFD5A177FFFFC6
          88FFFFD6A2FFFFD6A6FFFFDAB0FFFFDEB6FFECB486FFDAB39CFFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFFEFCFAFFFDF2
          E2FFFAE8D0FFFEF6EDFFFEFCF9FFFEFCF9FFFDFBF8FFFDFBF7FFF7E9DDFFD297
          67FFC2722BFFFFCD9AFFFFD0A0FFFFD3A6FFFCCC9AFFCA9378FFFCF6EDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFFEFDFCFFFEFD
          FCFFFEFDFCFFFEFDFBFFFEFCFBFFFEFCFAFFFEFCF9FFFDFBF8FFFDFBF7FFF8EC
          E3FFCD813AFFFFCB92FFFFCA95FFFFCD99FFFFD39EFFC38156FFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFDFAF4FFFCF2E6FFFEFDFCFFFEFDFBFFFEFCFBFFFEFCFAFFFDFBF9FFD794
          54FFFAB269FFFFC283FFFDC083FFDA9051FFFFD194FFC37A45FFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFCF4EAFFD58A2DFFF9E8D1FFFEFDFDFFFEFDFCFFFCF6F2FFDDA267FFEF98
          3EFFFFB05EFFFFBB70FFCD7D33FFC68760FFF4B373FFC47132FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFECB977FFDC800DFFE79C42FFE2A153FFDA8425FFF6993AFFFFA8
          4DFFFFAE55FFDF8530FFE4BE9EFFFCF7F0FFD69C74FFDFB597FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFDFBFFEAAA57FFF58F20FFFFA13BFFFFA038FFFFA341FFFFAF
          58FFDB822BFFE7BA93FFFDFAF5FFFDF9F4FFFDF9F2FFFCF8F1FFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFF0CEA3FFDB9741FFDA8A2EFFD78A33FFD494
          53FFF2DDCAFFFDFBF7FFFDFAF6FFFDF9F4FFFDF9F3FFFCF8F1FFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFDF9FFFEFDF9FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000846251FF866251FF846150FF846150FF846150FF8461
          50FF846150FF836150FF836150FF836150FF83604FFF82604FFF82604FFF8160
          4FFF815F4EFF815F4EFF815E4FFF805E4DFF805E4EFF7F5D4CFFAB958AFFBCAF
          A6FF8C8982FF8C817BFF2525255E020202020000000000000000000000000000
          00000000000000000000856251FFF8E4D3FFF2DECCFFF2DECCFFF2DECCFFF2DE
          CCFFF2DECCFFF2DECCFFF2DECCFFF2DFCCFFF2DDCCFFF2DDCBFFF2DDCBFFF2DE
          CAFFF2DCCAFFF2DCC7FFF3DBC7FFF1DAC4FFF1D9C2FFF2D8C0FFF3E3D5FF5D7D
          EFFF8A93A4FFCDC7C2FF888887FF2A2A2A690000000000000000000000000000
          00000000000000000000866251FFFFFDF2FFFAF4E8FFFAF3E9FFFAF3E9FFFAF3
          E9FFFAF3E9FFFAF3E9FFFAF3E8FFFAF4E8FFFAF2E8FFFAF2E7FFFAF2E7FFFAF3
          E6FFFAF1E6FFFAF2E5FFFAF2E4FFF9F1E2FFF9F0E0FFF6ECDEFF5E81F2FF2B77
          FFFF4CADFFFFE0E6E4FFCFCBC7FF323232790000000000000000000000000000
          00000000000000000000866251FFFFFEF5FFFBF6EAFFFBF5ECFFFBF5EBFFFBF5
          EBFFFBF5EBFFFBF5ECFFFBF5EAFFFBF6EAFFFBF4EBFFFBF4E9FFFBF4EAFFFBF5
          E8FFFBF3E9FFFBF3E7FFFCF4E6FFFAF2E5FFF6EDE5FF5A81F5FF2F7AFFFF57B4
          FFFF69D5FFFF5CBDFFFFAEAEAEFF74736ED30000000000000000000000000000
          00000000000000000000876452FFFFFEF7FFFBF5ECFFFCF6ECFFFCF6ECFFFCF6
          ECFFFCF6ECFFFCF6ECFFFBF5ECFFFBF5ECFFFBF5EBFFFBF5EBFFFBF5EAFFFBF4
          EAFFFBF4E9FFFBF4E8FFFBF3E8FFF6EFE7FF577DF7FF307CFFFF5AB7FFFF6DD6
          FFFF60C0FFFF3C8FFFFF1545D0D22E2C28570000000000000000000000000000
          00000000000000000000886352FFFFFFF7FFFCF6EDFFFCF6EEFFFCF6EEFFFCF6
          EEFFFCF6EEFFFCF6EEFFFCF6EDFFFCF6EDFFFCF6EDFFFCF6ECFFFBF5ECFFFBF5
          EBFFFBF5EAFFFBF4EAFFEAE3D8FF506DE9FF317BFCFF5BB9FFFF6DD6FFFF5FBF
          FFFF3F8EFFFF2D5CE9FF01051514000000000000000000000000000000000000
          00000000000000000000896453FFFFFFFAFFFCF7EFFFFCF7EFFFFCF7EFFFFCF7
          EFFFFCF7EFFFFCF7EFFFFEFCF8FFFEFCF8FFFEFBF8FFFDF9F4FFFDF9F3FFFDF9
          F3FFFCF8F3FFF7F3EDFF97938AFF5C7397FF4BABF4FF6ED9FFFF5EBEFFFF3E8D
          FFFF3F6BF5FFBBB0B8FF00000000000000000000000000000000000000000000
          000000000000000000008A6554FFFFFFFAFFFCF8F0FFFCF8F1FFFCF8F1FFFCF8
          F1FFFCF8F1FFFDFAF6FFF4F1ECFFB4B2B0FF898A8EFF83878EFF83878CFF8183
          86FFAEACAAFFE5E2DDFF8D8D8BFFE2D9D2FF7AA4B5FF4EB1F8FF3B8BFFFF4472
          FAFFEAE2E4FFAC968AFF00000000000000000000000000000000000000000000
          000000000000000000008B6654FFFFFFFBFFFCF8F2FFFCF8F2FFFDF9F2FFFDF9
          F2FFFDF9F2FFDAD7D3FF7B8085FFB5A995FFE9CB99FFF5CD8FFFF5CC8FFFF5D2
          A1FFC0B5A4FF4E545BFF909092FFEEEDECFFEAE1D8FF4C71AAFF3E68EDFFEFE9
          EAFFF8EDE2FF805D4EFF00000000000000000000000000000000000000000000
          000000000000000000008C6855FFFFFFFEFFFDF9F3FFFDF9F4FFFDF9F4FFFDF9
          F4FFDEDBD8FF828387FFE6C693FFF8CF8EFFF1CC92FFEFCA93FFF0CF9BFFF3D7
          A6FFF4CA8DFFF2D09CFF65686EFF9A9B9CFF959492FF9F9A8CFFE9E5E3FFFCF6
          EAFFF5E4D2FF815F4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6756FFFFFFFFFFFDFAF5FFFDFAF5FFFDFAF5FFF7F5
          F0FF878A8EFFE6C898FFF4D297FFEECF98FFEECF97FFEECE96FFEDCC8EFFEDC7
          89FFF1D2A2FFF4D59EFFF2CF9EFF4E535BFFDBD8D3FFE5E0D7FFFBF5EAFFFCF5
          EBFFF5E4D3FF825E4DFF00000000000000000000000000000000000000000000
          000000000000000000008F6855FFFFFFFFFFFDFAF6FFFDFBF7FFFDFBF7FFC8C7
          C6FFB1A48FFFFAD9A1FFF1D5A3FFF0D8A3FFF0D5A4FFF0D5A1FFEFD29CFFEECF
          96FFEDC98BFFF1D3A6FFF5CF90FFB3AB9CFFC0BEBCFFFDF9F3FFFBF5EBFFFCF5
          ECFFF5E4D4FF81604FFF00000000000000000000000000000000000000000000
          00000000000000000000916956FFFFFFFFFFFDFBF8FFFDFBF8FFFDFBF8FFA3A4
          A8FFDEC99EFFF4DCABFFF3DDAFFFF3DDAFFFF3DDAEFFF3DCACFFF1DAA8FFEFD3
          9FFFEECF98FFEDCB8DFFF6DAB0FFEFD0A2FF8F8F90FFFDF9F3FFFBF5ECFFFCF7
          EDFFF5E5D6FF835F4EFF00000000000000000000000000000000000000000000
          00000000000000000000926A58FFFFFFFFFFFEFCF9FFFEFCFAFFFEFCFAFF9092
          97FFF7DFAAFFF7E6BCFFF6E5BDFFF6E8C4FFF5E7BFFFF6E4B7FFF3DEAFFFF1DB
          A8FFEFD39FFFEECE94FFF1D5A7FFF6D299FF8B8E92FFFDF9F4FFFCF6EDFFFCF6
          EDFFF5E5D5FF83614EFF00000000000000000000000000000000000000000000
          00000000000000000000936B57FFFFFFFFFFFEFCFBFFFEFDFBFFFEFDFCFF9396
          9CFFFBE4B4FFF9EDCCFFFBF0D4FFFBEFCFFFF8EDCBFFF6E9C2FFF5E3B9FFF3DE
          AFFFF1DAA7FFEFD29BFFF2D6A4FFF5D09AFF8D9093FFFDFAF5FFFCF6EDFFFCF6
          EEFFF5E5D7FF846050FF00000000000000000000000000000000000000000000
          00000000000000000000946D58FFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFB0B1
          B4FFDFCFA9FFFFF6DCFFFCF6DDFFFBF9EBFFFCF5E0FFFBEFCEFFF7EBC6FFF5E1
          B6FFF2DBABFFEFD49FFFF4D7A4FFE7CC9FFF979798FFFCF7EFFFFCF6EEFFFDF8
          EFFFF7E6D6FF84604FFF00000000000000000000000000000000000000000000
          00000000000000000000956C5AFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFDADA
          DBFFB0A998FFFFFCE0FFFEFDF1FFFEFDFAFFFEFEF3FFFCF4D6FFF8EECEFFF5E4
          B9FFF3DDAEFFF1D5A3FFFADAA0FFABA191FFC5C3C0FFFCF7F0FFFCF7EFFFFDF7
          EFFFF6E6D8FF85624FFF00000000000000000000000000000000000000000000
          00000000000000000000966D59FFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFCFC
          FCFFA5A4A8FFE7E0C2FFFFFFEAFFFFFFF6FFFCFBEBFFFBF5DCFFF8EECBFFF6E5
          BBFFF3DDAFFFF6DCA3FFE6CD9DFF808388FFF8F4EFFFFCF8F1FFFCF7EFFFFDF7
          F0FFF6E6D8FF856151FF00000000000000000000000000000000000000000000
          00000000000000000000976E5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF5F5F5FF9D9D9CFFEAE3C9FFFFFFE9FFFFFCE8FFFBF5DAFFFAEEC9FFFAE6
          B9FFFDE3AEFFE5CDA1FF828386FFE0DDD9FFFDF9F2FFFCF8F1FFFCF7EFFFFDF7
          F0FFF6E6D7FF866150FF00000000000000000000000000000000000000000000
          00000000000000000000986F5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF3F3F3FFA3A3A7FFB2AE9EFFE3D8B8FFFFF0C3FFFDEABCFFE2D0
          AAFFB0A796FF84888BFFDFDDDAFFFDF9F4FFFDF9F3FFFCF8F1FFFDF8F1FFFDF8
          F1FFF7E7D9FF866251FF00000000000000000000000000000000000000000000
          0000000000000000000099715BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFAFAFAFFD3D3D4FFA6A7ABFF8B8D90FF87898EFF9B9D
          A0FFBFBEBEFFF7F6F2FFFFFDF9FFFFFDF7FFFEFBF7FFFDF8F3FFF9F5EDFFF5F0
          E8FFE9D6C8FF866150FC00000000000000000000000000000000000000000000
          000000000000000000009A705DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFEFC
          FAFFFFFDFBFFE6DCD7FFD9CBC2FFE0D3CCFFDACCC5FFD5C5BEFFC8B6ADFFBBA4
          9AFF977666FF60473BB600000000000000000000000000000000000000000000
          000000000000000000009B715CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFBBA295FFA18173FFA78979FFA58370FFA38068FFB08867FF936D
          55FF6B4E41C40806050E00000000000000000000000000000000000000000000
          000000000000000000009C735EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFDFCFFFEFC
          FAFFFFFFFFFFCBB7ADFFC5AD9FFFFFF5E0FFF9E1BBFFF8D69CFFC59E6FFF684C
          40C10805050C0000000000000000000000000000000000000000000000000000
          000000000000000000009E735DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFCFFFEFDFBFFFEFC
          FAFFFFFFFCFFCCBAB0FFBEA18BFFF6DFB8FFF3CF96FFC0986BFF6F5045CF0906
          050E000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C735DFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFAFFFEFC
          F9FFFFFEFCFFD7C9C1FFB28D73FFF6D39AFFC89F6FFF6E5145CC0C0908120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D725EFFFFFFFFFFFEFEFDFFFEFEFEFFFEFEFEFFFFFF
          FFFFFEFEFEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFCFFFEFCFBFFFEFCFAFFFDFB
          F8FFFFFFFBFFD3C2B9FFB7906BFFCBA170FF705144CD0B080611000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009B725CFFFFFFFFFFFEFDFCFFFEFDFDFFFEFEFDFFFEFE
          FDFFFEFEFDFFFEFEFDFFFEFDFCFFFEFDFCFFFEFCFBFFFEFCFAFFFDFBF9FFFDFB
          F7FFFFFEFBFFCEBAB1FFA27A5BFF795949D6100C0A1900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009C725EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFB39789FF775647D40F0B0A170000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009A725EFF9D7360FF9B725EFF9C725EFF9A725EFF9A72
          5EFF99715CFF98715DFF98705BFF976E5CFF966F5AFF956D5BFF946C5AFF936B
          59FF936D5AFF684B3EB90E0A0816000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFF0000007F0000003F0000003F0000003F0000003F000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          001FF000003FF000007FF00000FFF00001FFF00003FFF00007FFF0000FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000101050601096990010AA4E7010CAAF1010BA8F1010BA6F1010AA4F10109
          A4F10109A1F101089FF101079DF101069CF1010599F1010499F1010396F10103
          94F1010292F1010191F1010192F1010192F1010192F1010191F1010191F10101
          91F1010191F101018AE60101548D010104050000000000000000000000000000
          0000010A6C930E23C7FF2D42D4FF0116C7FF0116C5FF0117C3FF0115C3FF0114
          C0FF0113BEFF0112BCFF0111B9FF010FB7FF010EB5FF010DB3FF010CB1FF010B
          AFFF010AADFF0109ABFF0108A9FF0108A7FF0107A5FF0106A3FF0105A2FF0104
          A0FF01039FFF01039DFF01029CFF0101548D0000000000000000000000000000
          00000110B1EC435AE0FF233EDAFF011DD5FF011FD3FF011DD2FF011CD1FF011B
          CFFF0420D2FF021DCFFF0119CAFF0117C9FF0116C5FF0115C3FF0114C1FF0113
          C0FF0112BCFF0111BAFF010CB5FF0A1ABEFF010AB1FF010CB0FF010AADFF0109
          ACFF0107A6FF01039FFF01039EFF01018BE80000000000000000000000000000
          0000091CBAF64B66EEFF2242E4FF0123E0FF0123DFFF0123DEFF0122DDFF0122
          DEFF2455FFFF2455FEFF011ED9FF011ED5FF011DD4FF011DD1FF011AD0FF0119
          CCFF0118CAFF010FC3FF3058EFFF4B7DFFFF071CC6FF010FBAFF010FB7FF010F
          B5FF010DB1FF0106A5FF01039EFF010194F60000000000000000000000000000
          0000081BBFF6506CF1FF2445E9FF0124E5FF0126E4FF0125E3FF0125E4FF1044
          FFFFB2BFE9FFA9B9ECFF1A4DFDFF011FDAFF011FD7FF011ED5FF011DD2FF011B
          CFFF0112C9FF2A54EFFF95B2FEFFE9EDF8FF4778FFFF051AC5FF010EB9FF010F
          B7FF010FB5FF0108A9FF0104A0FF010194F60000000000000000000000000000
          0000091DC2F65772F6FF2749EDFF0127EAFF0128E9FF0128E9FF073CFFFFA4B3
          E8FFEBE8DEFFF0ECE1FFA2B3F0FF1A4DFDFF011FDBFF0120D9FF011FD6FF0117
          D1FF224FF1FF8DAAFEFFFFFFF7FFFFFFF9FFE1E9FCFF4676FFFF041AC5FF010E
          B9FF010FB8FF0109ABFF0105A2FF010194F60000000000000000000000000000
          0000091FC3F65C79F8FF294EF2FF0128EFFF012AEFFF0133FFFF9FAFE8FFEAE7
          DDFFE1E0E0FFE4E4E3FFF2EEE5FFA4B5F2FF194BFCFF0120DDFF011CD8FF1C48
          F3FF86A4FEFFFFFFF7FFFBFBF9FFFBFBFBFFFFFFFDFFE4ECFFFF4475FFFF0419
          C4FF010FBAFF010BADFF0106A3FF010194F60000000000000000000000000000
          00000A21C6F66280FDFF2C52F6FF0129F4FF0128FFFF9BABE6FFE8E5DBFFE1E0
          DFFFE1E1E1FFE4E4E4FFE9E8E7FFF7F4E9FFA3B6F4FF1346FBFF113FF2FF7D9C
          FEFFFFFFF6FFFBFBF9FFFBFBFBFFFDFDFDFFFEFEFEFFFFFFFFFFE4EDFFFF4373
          FFFF0319C4FF010AAFFF0107A5FF010194F60000000000000000000000000000
          00000B22C8F66885FFFF2F56F9FF0126F9FF9BA9E3FFE9E5D9FFDFDEDDFFDFDF
          DFFFE2E2E2FFE5E5E5FFE8E8E8FFEBEAEAFFFCF8EDFF9DB1F7FF6A8CFCFFFFFF
          F5FFFBFAF8FFFAFAFAFFFDFDFDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFEAF1
          FFFF4879FFFF0817BAFF0107A7FF010194F60000000000000000000000000000
          00000B24CAF66D8CFFFF315AFEFF0125F6FF7488E7FFEEEADAFFDEDEDDFFE0E0
          E0FFE3E3E3FFE6E6E6FFE9E9E9FFEBEBEBFFEFEFEEFFF9F7F1FFFDFCF4FFFAF9
          F7FFF9F9F9FFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3C7
          FFFF3661F4FF010DB4FF0108A9FF010296F60000000000000000000000000000
          00000C25CDF67393FFFF3762FFFF0135FFFF0122F7FF7588E8FFF0ECDDFFE3E3
          E1FFE4E4E4FFE6E6E6FFE9E9E9FFECECECFFEFEFEFFFF2F2F2FFF5F5F5FFF8F8
          F8FFFAFAFAFFFDFDFDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFAFC5FFFF2F5D
          F5FF0111C5FF010FB7FF0109ABFF010398F60000000000000000000000000000
          00000C26CFF67C9BFFFF3A67FFFF013AFFFF043BFFFF0123F7FF758AE9FFF3EF
          E0FFE5E5E4FFE7E7E7FFEAEAEAFFEDEDEDFFF0F0F0FFF3F3F3FFF5F5F5FFF8F8
          F8FFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABC1FFFF2755F7FF0115
          CBFF0118C9FF0110B9FF010AADFF01049BF60000000000000000000000000000
          00000C27D1F681A2FFFF406DFFFF0540FFFF0741FFFF063FFFFF0122F5FF7A8E
          EBFFF6F2E3FFE8E8E7FFEAEAEAFFEDEDEDFFF0F0F0FFF3F3F3FFF6F6F6FFF9F9
          F9FFFCFCFCFFFEFEFEFFFFFFFFFFFFFFFFFFA9BFFFFF1F4FF7FF0119D2FF011B
          CFFF0119CCFF0111BCFF010BAFFF01059DF60000000000000000000000000000
          00000D29D2F689ABFFFF4373FFFF0545FFFF0A46FFFF0A46FFFF0743FFFF0122
          F6FF7A8EECFFF1EFE7FFEAEAEAFFEDEDEDFFF0F0F0FFF3F3F3FFF6F6F6FFF9F9
          F9FFFCFCFCFFFEFEFEFFFFFFFFFFA0B6FFFF1446F9FF011CD9FF011FD6FF011D
          D2FF011AD0FF0113BEFF010CB1FF01069FF60000000000000000000000000000
          00000D2BD4F68FB1FFFF4979FFFF094AFFFF0C4CFFFF0B4AFFFF0A4BFFFF012F
          F7FF274AEEFFF0EFE7FFEAEAEAFFEDEDEDFFF0F0F0FFF3F3F3FFF6F6F6FFF9F9
          F9FFFCFCFCFFFEFEFEFFFFFFFFFF6F8FFFFF0C3AF3FF011DDAFF0120D9FF011E
          D5FF011DD1FF0114C0FF010DB3FF0107A1F60000000000000000000000000000
          00000D2CDAF696B7FFFF4C80FFFF0A4FFFFF1051FFFF0F52FFFF0131F0FF2E4A
          E4FFF4F1E4FFEAEAE7FFEAEAEAFFEDEDEDFFF0F0F0FFF3F3F3FFF6F6F6FFF8F8
          F8FFFBFBFBFFFFFFFEFFFFFFFFFFFFFFFFFF7897FFFF0E3DF4FF011DDAFF011F
          D7FF011DD5FF0115C3FF010EB5FF0108A4F60000000000000000000000000000
          00000E30DBF69DBDFFFF5083FFFF0E54FFFF125BFFFF0132EBFF2B42DBFFEFED
          E1FFE8E7E4FFE6E6E6FFE9E9E9FFECECECFFEFEFEFFFF2F2F2FFF5F5F5FFF8F8
          F8FFFBFBFBFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF7594FFFF0D3CF3FF011C
          D7FF011ED6FF0116C5FF010FB7FF0109A6F60000000000000000000000000000
          00000E30DDF6A4C3FFFF568AFFFF105EFFFF0134E7FF2A3DD1FFECE9DDFFE4E3
          E0FFE3E3E3FFE6E6E6FFE9E9E9FFECECECFFEEEEEEFFF1F1F1FFF4F4F4FFF7F7
          F7FFFAFAFAFFFCFCFCFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF7595FFFF0E3C
          F2FF011CD6FF0117C7FF0110B9FF010BA8F60000000000000000000000000000
          00000F32E0F6AACBFFFF5B93FFFF0136E2FF2835C6FFE9E7DBFFE1E0DDFFE1E1
          E0FFE2E2E2FFE5E5E5FFE8E8E8FFEBEBEBFFEEEEEDFFF9F6F0FFFCFAF3FFF8F8
          F6FFF8F8F8FFFBFBFBFFFDFDFDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFF7293
          FFFF1340F2FF0116C8FF0111BBFF010CABF60000000000000000000000000000
          00000F33E2F6B1D0FFFF5B8CF9FF0101B5FFCCCBD5FFE3E2DAFFDCDCDCFFDFDF
          DFFFE1E1E1FFE4E4E4FFE7E7E7FFECECEAFFFCF9ECFF8C9EEFFF8EA0F2FFFFFF
          F4FFF9F9F7FFF9F9F9FFFBFBFBFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
          FFFF265AFFFF0825D2FF0112BDFF010DADF60000000000000000000000000000
          00001136E4F6B8D5FFFF639DFFFF0E53F0FF010FB8FFD0CFD8FFE1E1DCFFDEDE
          DEFFE0E0E0FFE3E3E3FFE8E8E6FFF8F5E9FF95A3E8FF011BEEFF011EF2FF99AA
          F2FFFFFFF5FFF8F8F7FFF9F9F9FFFDFDFDFFFEFEFDFFFFFFFEFFFEFEFFFF2A59
          FFFF042CE7FF0118CBFF0114BFFF010DB0F60000000000000000000000000000
          00001136E6F6BDDAFFFF659EFFFF1B71FFFF1255EEFF0210B8FFD1D0D9FFE4E3
          DEFFDFDFDFFFE3E3E2FFF3F1E5FF98A1E2FF0118E2FF0948FFFF0742FFFF011D
          F1FF99AAF3FFFFFFF5FFF8F8F7FFF9F9F9FFFFFFFBFFFBFCFDFF2252FFFF012A
          EBFF0120DDFF011BCEFF0114C1FF010EB2F60000000000000000000000000000
          00001138EAF6C3E0FFFF69A3FFFF1B73FFFF2078FFFF1355EEFF0310B8FFD2D2
          DBFFE6E5DFFFF0EFE3FF939DDCFF0113D9FF0D4FFFFF0B4BFFFF0A46FFFF063E
          FFFF011CF0FFA0AEF2FFFFFFF5FFFFFDF7FFF6F8F9FF1748FFFF012AEFFF0124
          E3FF0124DFFF011CD0FF0115C3FF010FB4F60000000000000000000000000000
          0000123AEBF6C8E3FFFF6DA8FFFF1D78FFFF2377FFFF2078FFFF1254EEFF040F
          B8FFDFDEDCFF999ED6FF010FCEFF1056FFFF1053FFFF0C4CFFFF0A46FFFF0742
          FFFF033BFFFF011CF0FFA3B0F2FFFFFEF5FF1241FEFF0129F3FF0128E9FF0126
          E4FF0123E0FF011CD1FF0116C5FF0111B7F60000000000000000000000000000
          0000143DEDF6CDE8FFFF7DB3FFFF1A79FFFF207AFFFF1F76FFFF1E74FFFF0E50
          EDFF0712B7FF010EC4FF115BFFFF1157FFFF0D51FFFF0C4CFFFF0847FFFF0743
          FFFF033DFFFF0135FFFF0121EFFF123AF2FF0128F4FF012AEFFF0129EAFF0126
          E5FF0124E1FF011CD2FF0116C6FF0112B9F60000000000000000000000000000
          0000012CE6EDCEEBFFFFC2DEFFFF5EA1FFFF3F8DFFFF3E89FFFF3B84FFFF3A83
          FFFF2D62ECFF3071FBFF2F72FFFF2C6AFFFF2765FFFF255FFFFF205AFFFF1E54
          FFFF1A4EFFFF174AFFFF1442FDFF103CF6FF123CF5FF113AF0FF1035ECFF0E33
          E7FF0D31E2FF122DD6FF2438CFFF0313B2E90000000000000000000000000000
          0000011992964F7CFBFFCDEBFFFFCDE7FFFFCAE4FFFFC5E1FFFFC0DBFFFFBAD6
          FFFFB3D3FFFFADCCFFFFA6C5FFFF9FBFFFFF98B8FFFF92B2FFFF8AABFFFF83A3
          FFFF7E9CFFFF7594FFFF6F8CFFFF6886FEFF6280FCFF5C7AF7FF5773F3FF516C
          F0FF4C66EBFF455ADFFF182DCEFF010D70900000000000000000000000000000
          00000102080701199396012DE7ED0F3AEEF60E39ECF60D37EBF60D35EAF60D34
          E9F60D32E7F60D32E4F60C31E4F60C2FE1F60C2FDFF60B2CDCF60A2ADCF60A2A
          DAF60928D6F60927D5F60A26D2F60926D2F60824D0F60722CEF60722CCF60620
          CBF6081FCAF60218BEEC010E7492010106060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFE0000007C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003E0000007FFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001010101010228350103537F010276B801028ADC010293ED010191EC0101
          86DB01016FB701014C7E01012333000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010102010104
          4258010699D00911ADFE161CABFF2123AAFF2827A7FF2929A6FF2B27A2FF2A25
          9EFF241F9AFF191398FF08089DFE01018ACE0101405501010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001021C20010A81BF0915
          B1FE1428C7FF132BCFFF1029CFFF1027CDFF1326C5FF1C26B8FF2727A4FF3327
          92FF332793FF312595FF312694FF271E95FF0B099CFE010180BE0101171E0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001043940010EAAED0424DBFF022E
          F2FF012BF2FF012AF0FF0129EFFF0129EDFF0128EBFF0126EBFF0125EAFF0124
          E3FF1424C3FF2E259BFF392688FF33248EFF342690FF1F1894FF010193EB0101
          2D3E000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001053A410110B9F5012EF2FF0130FAFF012E
          F6FF012EF4FF012EF3FF012DF2FF012CF0FF012BEEFF012AECFF0129E9FF0127
          E7FF0125E8FF0124E9FF0D24CAFF37258EFF392687FF33268EFF2A1F92FF0101
          98F401012C3D0000000000000000000000000000000000000000000000000000
          00000000000000000000010320220111B5ED0232FAFF0134FFFF0132FDFF0132
          FDFF0132FBFF0132FAFF0131FAFF0130F7FF012FF5FF012DF3FF012CF0FF012A
          EDFF0129E9FF0127E7FF0125E7FF0123E6FF2926A2FF3C2682FF34258DFF2A20
          92FF010193EB0101181F00000000000000000000000000000000000000000000
          00000000000001010202010F9AC30C36F2FF0538FFFF0437FFFF0437FFFF0437
          FFFF0437FFFF0336FEFF0235FFFF0134FDFF0132FDFF0131F9FF012FF6FF012E
          F3FF012CF0FF012AECFF0128E8FF0126E5FF0123E9FF2425AAFF3C2783FF3526
          8EFF1F1896FF010180BF01010101000000000000000000000000000000000000
          000000000000010A525C0825D8FE1C4FFFFF093BFFFF0A3CFFFF0A3CFFFF0A3C
          FFFF093CFFFF083BFFFF073AFFFF0638FFFF0437FFFF0235FEFF0133FEFF0131
          FAFF012FF6FF012DF2FF012BEDFF0128E9FF0126E5FF0124EAFF2C279DFF3A25
          88FF352590FF0A0A9EFE01014058000000000000000000000000000000000000
          000001010201010FA6D5315BFCFF1043FFFF0E40FFFF0F41FFFF0F41FFFF0F41
          FFFF0E41FFFF0E40FFFF0C3FFFFF0B3DFFFF093CFFFF073AFFFF0437FFFF0235
          FFFF0132FDFF0130F8FF012EF3FF012BEEFF0129EAFF0126E6FF0124E5FF3A28
          89FF35278FFF281F96FF01018DD1010101010000000000000000000000000000
          000001072E3B0726D7FE4972FFFF0F42FFFF1446FFFF1446FFFF1446FFFF1446
          FFFF1446FFFF1345FFFF1143FFFF0E40FFFF0B3EFFFF093BFFFF0639FFFF0336
          FFFF0133FEFF012FFEFF012EF9FF012AF4FF0126EFFF0125E9FF0122E9FF1424
          C1FF3A2889FF322895FF0809A2FE010124350000000000000000000000000000
          0000010B6D86274CEFFF4670FFFF1446FFFF184AFFFF1648FFFF1246FFFF1147
          FFFF1449FFFF184CFFFF194EFFFF1D50FFFF1F53FFFF2053FFFF2053FFFF1E52
          FFFF1C4FFFFF194EFEFF174BFFFF174BFBFF1748F8FF1A49F7FF2653F6FF0127
          EEFF37288FFF332795FF18179EFF010152820000000000000000000000000000
          0000010F9DC1446BFBFF4972FFFF194BFFFF1B4DFFFF2351FAFF94A6E3FF8EA2
          E8FF94A6EBFF9BADEDFFA4B6EFFFA7BAF1FFADBFF3FFB4C3F6FFB8CAF8FFBBCE
          FAFFBECEFCFFC0D1FEFFC5D5FEFFC7D6FFFFC7D6FFFFD3DFFFFF6B95FFFF0128
          EFFF1D27B7FF332994FF25219DFF010175BB0000000000000000000000000000
          00000113C0E55B81FFFF4F76FFFF1E50FFFF1B4FFFFF2751F6FFF4EDCFFFE4E1
          D8FFE8E5DCFFECE9E0FFEFECE4FFF5F2EAFFF8F5EEFFFCF9F2FFFFFEF6FFFFFF
          F9FFFFFFFDFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7298FFFF0128
          EEFF0A28D8FF352994FF2C289FFF010290E00000000000000000000000000000
          00000119CFF5658AFFFF5D81FFFF2155FFFF2153FDFF2044E8FFECE8D4FFDADA
          DAFFDEDEDEFFE3E3E3FFE7E7E7FFEBEBEBFFEFEFEFFFF4F4F4FFF8F8F8FFFDFD
          FDFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F96FFFF012A
          F0FF0127EFFF342997FF2D2AA3FF01059CF20000000000000000000000000000
          0000011AD2F46B8FFFFF6E8EFFFF2758FFFF2755FDFF1F39D6FFEEEBD7FFDBDB
          DBFFDFDFDFFFE4E4E4FFE8E8E8FFECECECFFF1F1F1FFF5F5F5FFF9F9F9FFFDFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F96FFFF012B
          F3FF012AF5FF2C2BA8FF2C2CA8FF01069EF10000000000000000000000000000
          00000115C4E36E90FFFF7997FFFF3867FFFF2A58FBFF2433C4FFFBF7DAFFEAE9
          DDFFF0EEE2FFF3F2E5FFF9F7EAFFFDFBEDFFFFFEF1FFFFFFF6FFFFFFFAFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7197FFFF012F
          F6FF012AF6FF242AB5FF292BACFF010594DE0000000000000000000000000000
          00000113A3BD6083FCFF7B9BFFFF5680FFFF2F5BF8FF090EA7FF767BC5FF6D76
          CDFF717AD4FF7380DCFF7585E4FF778BECFF7A90F3FF7E97FBFF849BFCFF89A1
          FEFF8CA4FFFF90A8FFFF94ACFFFF98AFFFFF9CB2FFFFAABEFFFF5681FFFF0132
          FAFF012DF7FF212CBCFF2126B0FF01057CB80000000000000000000000000000
          0000010F73824467F5FF7E9DFFFF7B99FFFF3566FCFF213CD6FF1B3DDEFF193D
          E1FF183FE7FF163EEBFF1440EFFF123FF3FF0E3FF7FF0C3FFBFF0940FFFF0A3E
          FFFF083EFFFF093FFFFF093DFFFF083EFFFF093EFFFF093EFFFF1447FEFF0132
          FCFF012EFAFF202EC0FF151EB3FF0105577E0000000000000000000000000000
          000001083535183DEFFE83A4FFFF8AA8FFFF5480FFFF4678FFFF4B7BFFFF4A7B
          FFFF4979FFFF4576FFFF4373FFFF3E6EFFFF3969FFFF3565FFFF305FFFFF2A5B
          FFFF2455FFFF1E4FFFFF194BFFFF1446FFFF0F41FFFF093BFFFF0436FFFF0133
          FCFF012FFDFF2431C1FF0713B7FE010325300000000000000000000000000000
          0000010101010119CDCE7293FDFF88A6FFFF87A6FFFF4475FFFF4B79FFFF4C7A
          FFFF4B79FFFF4875FFFF4371FFFF3E6DFFFF3A69FFFF3564FFFF2F60FFFF2A5B
          FFFF2556FFFF2051FFFF1B4CFFFF1547FFFF1042FFFF0B3EFFFF0639FFFF0134
          FFFF0131FBFF1D2BBFFF010A9FCA000000000000000000000000000000000000
          000000000000010E54532347F2FE8FAFFFFF90ADFFFF7194FFFF4676FFFF4C7A
          FFFF4B79FFFF4B7AFFFF4676FFFF4170FFFF3C6BFFFF3766FFFF3161FFFF2C5C
          FFFF2657FFFF2152FFFF1C4DFFFF1748FFFF1143FFFF0C3EFFFF073AFFFF0236
          FFFF0B35EEFF0819BEFE0106464E000000000000000000000000000000000000
          000000000000000000000117B9B96082F8FF8EACFFFF93AFFFFF688EFFFF4676
          FFFF4C7AFFFF4B79FFFF4A78FFFF4572FFFF3E6DFFFF3868FFFF3363FFFF2D5E
          FFFF2858FFFF2253FFFF1D4EFFFF1749FFFF1244FFFF0D3FFFFF083AFFFF0238
          FFFF0C28D7FF010C93B500000000000000000000000000000000000000000000
          0000000000000000000001061C1B0121D9E77A99FEFF8CAAFFFF92AEFFFF7093
          FFFF4374FFFF4B7AFFFF4A79FFFF4673FFFF3F6EFFFF3969FFFF3563FFFF2E5E
          FFFF2859FFFF2354FFFF1D4FFFFF184AFFFF1344FFFF0D3FFFFF083DFFFF0433
          F5FF0111AFE50102171900000000000000000000000000000000000000000000
          0000000000000000000000000000010937360126E1F07998FEFF8DABFFFF8FAB
          FFFF84A3FFFF5581FFFF4575FFFF4372FFFF3F6EFFFF3969FFFF3363FFFF2E5E
          FFFF2859FFFF2354FFFF1D4FFFFF184AFFFF1344FFFF0D42FFFF0737F9FF0114
          BCEE01062F330000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010936350121D5E45C7EF8FF8EAF
          FFFF89A9FFFF8DAAFFFF7A9BFFFF5781FFFF3B6CFFFF3264FFFF3060FFFF2A5B
          FFFF2656FFFF1F51FFFF1B4DFFFF184AFFFF1C4EFFFF0B34EEFF0114B6E30105
          2F32000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010418170118B0B01D44
          F3FE7091FDFF8BACFFFF83A5FFFF819FFFFF7D9CFFFF688BFFFF557EFFFF4972
          FFFF426DFFFF3F6CFFFF406DFFFF2953F8FF0524DCFE01119BAE010316160000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000010D
          47460118C1C11239F0FD4065F3FF5E81FCFF6C8FFFFF6E92FFFF688CFFFF597F
          FFFF4167F9FF2248ECFF0425DFFD0114AFBE0109454400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001062625010E646E011398A90115B8CE0117C3DE0117C2DE0113
          B1CE011291A9010C5E6E01052224000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFF00FFFFF8001FFFF0000FFFE00007FFC00003FF800
          001FF000000FE0000007E0000007E0000007C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003E0000007E0000007E000000FF000
          000FF800001FFC00003FFE00007FFF0000FFFFC003FFFFF00FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000909090F3434347E515151D75D5D
          5DFB525252E91A1A1A46019301FF019301FF019301FF019301FF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001D1D1D35636363E2989898FFAEAEAEFF9A9A
          9AFF6A6A6AFF414141AB019301FF36CE60FF32CA5AFF1EB635FF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000D0D0D16676767E9E2E2E2FFDBDBDBFFB3B3B3FF9393
          93FF696969FF464646B7019301FF55ED92FF50E88AFF2FC751FF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000003B3B3B78B7B7B7FFFFFFFFFFCFCFCFFFB0B0B0FFA3A3
          A3FF939393FF4D4D4DC2019301FF54EC8EFF4DE584FF2CC54CFF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000515151AAE1E1E1FFFAFAFAFFCFCFCFFF019301FF0193
          01FF019301FF019301FF019301FF54EC8DFF4EE684FF2AC248FF019301FF0193
          01FF019301FF019301FF019301FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000575757BAEAEAEAFFF9F9F9FFCFCFCFFF019301FF43DB
          6EFF3AD260FF38D05BFF3DD565FF51E986FF4DE581FF40D86BFF28C045FF2BC3
          4BFF2DC550FF1EB635FF019301FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000005C5C5CC8F5F5F5FFF7F7F7FFCFCFCFFF019301FF66FE
          A4FF59F190FF57EF8EFF53EB88FF50E882FF4DE581FF4DE581FF4EE685FF4DE5
          84FF4FE78AFF34CC5BFF019301FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000636363D6FEFEFEFFF8F8F8FFD2D2D2FF019301FF6EFF
          AFFF62FA9CFF5FF798FF58F08FFF53EB86FF50E882FF51E986FF54EC8CFF54EC
          8DFF54EC91FF37CF60FF019301FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000676767E5FFFFFFFFE4E4E4FFB9B9B9FF019301FF0193
          01FF019301FF019301FF019301FF59F190FF53EB88FF3AD260FF019301FF0193
          01FF019301FF019301FF019301FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000003535355C7F7F7FFEE7E7E7FFD3D3D3FFCECECEFFBFBFBFFFB1B1
          B1FFABABABFFA1A1A1FE019301FF61F99BFF59F18FFF30C84EFF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001E1E1E307A7A7AF4E6E6E6FFE1E1E1FFD0D0D0FFCDCDCDFFB5B5B5FF9999
          99FF818181FFA6A6A6FF019301FF63FB9EFF5AF292FF34CD54FF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000C0C
          0C12717171DDD5D5D5FFE9E9E9FFD8D8D8FFCECECEFFCCCCCCFFB7B7B7FFA0A0
          A0FF8A8A8AFF9E9E9EFF019301FF6DFFAEFF65FDA3FF3AD25EFF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020202036767
          67B7BFBFBFFFF1F1F1FFDFDFDFFFD6D6D6FFCDCDCDFFCCCCCCFFBBBBBBFFA5A5
          A5FF919191FFA4A4A4FF019301FF019301FF019301FF019301FF019301FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000057575785ABAB
          ABFFF7F7F7FFE4E4E4FFDBDBDBFFD3D3D3FFCCCCCCFFCCCCCCFFBDBDBDFFA9A9
          A9FF979797FFA9A9A9FF9F9F9FFF969696FF8D8D8DFE32323273000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000003B3B3B519E9E9EFDF6F6
          F6FFEAEAEAFFE0E0E0FFD9D9D9FFD2D2D2FFCCCCCCFFCCCCCCFFBDBDBDFFADAD
          ADFF9C9C9CFF8C8C8CFF7F7F7FFF737373FF686868FF5B5B5BFA1C1C1C430000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000020202028949494F1ECECECFFEEEE
          EEFFE4E4E4FFDDDDDDFFD7D7D7FFD0D0D0FFCCCCCCFFCDCDCDFFBEBEBEFFAFAF
          AFFFA0A0A0FF929292FF848484FF797979FF6F6F6FFF646464FF555555EA0E0E
          0E20000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000C0C0C0E888888D6DDDDDDFFF3F3F3FFE7E7
          E7FFE1E1E1FFDBDBDBFFD5D5D5FFCFCFCFFFCCCCCCFFCECECEFFC0C0C0FFB1B1
          B1FFA3A3A3FF969696FF898989FF7F7F7FFF757575FF6D6D6DFF616161FF4949
          49CD050505090000000000000000000000000000000000000000000000000000
          0000000000000000000002020201717171ADCACACAFFF8F8F8FFEDEDEDFFE6E6
          E6FFE2E2E2FFDCDCDCFFD6D6D6FFCECECEFFCACACAFFC9C9C9FFBBBBBBFFABAB
          ABFFA1A1A1FF969696FF8B8B8BFF818181FF787878FF717171FF6A6A6AFF5E5E
          5EFF3F3F3FA20000000000000000000000000000000000000000000000000000
          000000000000000000005C5C5C7ABEBEBEFEFFFFFFFFF0F0F0FFE4E4E4FFD8D8
          D8FFCCCCCCFFC3C3C3FFBEBEBEFFBEBEBEFFBFBFBFFFC1C1C1FFC4C4C4FFC6C6
          C6FFC0C0C0FFB8B8B8FFB0B0B0FF999999FF7E7E7EFF6B6B6BFF666666FF6363
          63FF5A5A5AFE2F2F2F6F00000000000000000000000000000000000000000000
          0000000000003F3F3F48BDBDBDFCDADADAFFBDBDBDFFABABABFF989898FF9494
          94FF969696FF9E9E9EFFA9A9A9FFB5B5B5FFBFBFBFFFCBCBCBFFD2D2D2FFDEDE
          DEFFEAEAEAFFF1F1F1FFF4F4F4FFFAFAFAFFF4F4F4FFDCDCDCFFB2B2B2FF8686
          86FF5E5E5EFF515151FA19191940000000000000000000000000000000000000
          000021212122B1B1B1ECB4B4B4FF999999FF838383FF7F7F7FFF888888FF9393
          93FF9B9B9BFFA4A4A4FFADADADFFB7B7B7FFC0C0C0FFC9C9C9FFD0D0D0FFD9D9
          D9FFE2E2E2FFECECECFFF4F4F4FFFEFEFEFFFFFFFFFFFCFCFCFFF9F9F9FFF1F1
          F1FFD7D7D7FF8D8D8DFF515151E80C0C0C1E0000000000000000000000000A0A
          0A0B9B9B9BCFA4A4A4FF767676FF767676FF808080FF878787FF8E8E8EFF9696
          96FF9E9E9EFFA6A6A6FFAFAFAFFFB8B8B8FFC1C1C1FFCBCBCBFFCFCFCFFFD8D8
          D8FFE1E1E1FFE9E9E9FFF1F1F1FFFAFAFAFFFFFFFFFFF8F8F8FFF3F3F3FFF1F1
          F1FFF1F1F1FFF1F1F1FFB7B7B7FF4E4E4ECB0505050A00000000000000005555
          5575A2A2A2FF6A6A6AFF777777FF7E7E7EFF848484FF8A8A8AFF919191FF9999
          99FFA1A1A1FFA9A9A9FFB1B1B1FFB9B9B9FFC2C2C2FFCACACAFFCFCFCFFFD8D8
          D8FFE0E0E0FFE7E7E7FFEFEFEFFFF6F6F6FFFFFFFFFFFAFAFAFFF6F6F6FFF1F1
          F1FFEDEDEDFFEBEBEBFFF1F1F1FFA0A0A0FF3636368C00000000000000009595
          95CA767676FF747474FF7C7C7CFF818181FF878787FF8E8E8EFF959595FF9C9C
          9CFFA3A3A3FFAAAAAAFFB2B2B2FFBABABAFFC2C2C2FFCACACAFFCFCFCFFFD7D7
          D7FFDFDFDFFFE6E6E6FFEDEDEDFFF4F4F4FFFCFCFCFFFDFDFDFFF8F8F8FFF3F3
          F3FFEFEFEFFFECECECFFECECECFFEDEDEDFF535353E800000000000000008F8F
          8FC17F7F7FFF777777FF7F7F7FFF848484FF8A8A8AFF909090FF979797FF9E9E
          9EFFA5A5A5FFACACACFFB4B4B4FFBBBBBBFFC3C3C3FFCACACAFFCFCFCFFFD6D6
          D6FFDEDEDEFFE5E5E5FFEBEBEBFFF2F2F2FFF9F9F9FFFFFFFFFFF9F9F9FFF5F5
          F5FFF1F1F1FFEEEEEEFFEFEFEFFFE9E9E9FF515151DE00000000000000004747
          4754BABABAFE7A7A7AFF7F7F7FFF878787FF8D8D8DFF939393FF999999FFA0A0
          A0FFA7A7A7FFAEAEAEFFB5B5B5FFBCBCBCFFC3C3C3FFCACACAFFCFCFCFFFD6D6
          D6FFDDDDDDFFE4E4E4FFEAEAEAFFF0F0F0FFF7F7F7FFFDFDFDFFFBFBFBFFF7F7
          F7FFF3F3F3FFF6F6F6FFFBFBFBFF888888FE3131317100000000000000000000
          00006D6D6D76C5C5C5FC949494FF838383FF8C8C8CFF969696FF9C9C9CFFA2A2
          A2FFA8A8A8FFAFAFAFFFB6B6B6FFBDBDBDFFC4C4C4FFCACACAFFCECECEFFD5D5
          D5FFDCDCDCFFE3E3E3FFE9E9E9FFEFEFEFFFF5F5F5FFFCFCFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFE0E0E0FF7C7C7CFE4646468E0101010100000000000000000000
          00000000000037373739B1B1B1C4C8C8C8FEA8A8A8FF989898FF9B9B9BFFA3A3
          A3FFAAAAAAFFB1B1B1FFB7B7B7FFBEBEBEFFC4C4C4FFCACACAFFCECECEFFD6D6
          D6FFDCDCDCFFE5E5E5FFEAEAEAFFF3F3F3FFFCFCFCFFFFFFFFFFFFFFFFFFD0D0
          D0FF909090FE616161D127272749000000000000000000000000000000000000
          00000000000000000000000000003737373883838395AEAEAEE2BABABAFEB2B2
          B2FFB0B0B0FFB2B2B2FFB8B8B8FFC0C0C0FFC9C9C9FFD0D0D0FFD3D3D3FFDBDB
          DBFFDFDFDFFFDFDFDFFFD6D6D6FFC8C8C8FFB2B2B2FF959595FE6E6E6EE85151
          519E252525430101010100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202011E1E1E284646
          465E6565658C7B7B7BB08C8C8CCD959595E19B9B9BF19B9B9BF6969696F69090
          90F2808080E3737373D0606060B44B4B4B90343434631919192F020202030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFE107FFFFC007FFFF8007FFFF0007FFFF00007FFF00007FFF0
          0007FFF00007FFF00007FFE0007FFFE0007FFFC0007FFF80007FFF0000FFFE00
          00FFFE00007FFC00003FF800001FF000000FF000000FE0000007C00000038000
          0001800000018000000180000001C0000003F000000FFC00003FFF8001FFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000070F9ED401017CB200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000909090F3434347E515151D75D5D
          5DFB525252E91A1A1A4600000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000060F
          92C8112ED5FF1F4FFAFF0203A6FE01014B73010105050000000001013041040C
          B2FE030EA9FF010199FD070F4C604747475B636363E2989898FFAEAEAEFF9A9A
          9AFF6A6A6AFF414141AB00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010107070101
          97F12251F7FF2461FFFF225EFFFF0A1ABDFF01016BBD01012B460308ACFE1441
          EAFF1550FFFF104BFFFF0403A6FEB2B2B2F4E2E2E2FFDBDBDBFFB3B3B3FF9393
          93FF696969FF464646B700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          537C1026C8FF2961FFFF2459FFFF255FFFFF0E23C7FF010199FF1845EAFF184E
          FFFF144AFFFF1243EAFF0516B6FFDBDBDBFFFFFFFFFFCFCFCFFFB0B0B0FF9292
          92FF686868FF4D4D4DC200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000910B5FF204AEAFF2B62FFFF255AFFFF245BFFFF1B45EAFF1E52FFFF1B50
          FFFF1744EAFF081CC2FF43439FEEEAEAEAFFFAFAFAFFCFCFCFFFB0B0B0FF9292
          92FF6A6A6AFF555555CD00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000101273501018CF01A39DBFF2D64FFFF2658FFFF2355FFFF2054FFFF2056
          FFFF060DACFF010164A59A9AA0D9EAEAEAFFF9F9F9FFCFCFCFFFB0B0B0FF9292
          92FF696969FF5B5B5BD800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000101151F010171DC1F43E5FF295EFFFF2658FFFF2559FFFF1838
          DCFF010170D2010102025C5C5CC8F5F5F5FFF7F7F7FFCFCFCFFFB0B0B0FF9292
          92FF696969FF626262E300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000010186F43369FFFF2E63FFFF2B61FFFF285EFFFF265F
          FFFF0305A1FE01013F5E636363D6FEFEFEFFF8F8F8FFD2D2D2FFB3B3B3FF9595
          95FF686868FF696969ED00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001012A3D0E1BADFF3972FFFF366FFFFF152DCCFF2657FDFF2A63
          FFFF2155FFFF01018BF19C9CB4F4FFFFFFFFE4E4E4FFB9B9B9FF919191FF7474
          74FF626262FF656565F502020203000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001018ADA2950EAFF3D76FFFF1C38D4FF01016DD001018DF91E48
          EAFF2964FFFF1434DEFF4747A1FFEFEFEFFFD3D3D3FFCECECEFFB3B3B3FF9292
          92FF737373FF626262FE1E1E1E47000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000609A7FE3767EAFF2B55EAFF010189EF0101111A000000000101
          9EFD1F4BEAFF276AFFFF050CA6FFF0F0F0FFD0D0D0FFCDCDCDFFB5B5B5FF9999
          99FF818181FF6D6D6DFF5A5A5AEC0F0F0F220000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000010166A51F36D1FF111FBBFF01014A6F00000000000000008686
          8689080EB4FF1D49F8FF0711B8FFECECECFFCECECEFFCCCCCCFFB7B7B7FFA0A0
          A0FF8A8A8AFF787878FF676767FF4F4F4FD00505050A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000001015374010185C80101050500000000020202036767
          67B7DFDFDFFF4242C5FF8080C2FFDDDDDFFFCDCDCDFFCCCCCCFFBBBBBBFFA5A5
          A5FF919191FF808080FF717171FF636363FF444444A601010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000057575785ABAB
          ABFFF6F6F6FFEEEEEEFFE3E3E3FFD3D3D3FFCCCCCCFFCCCCCCFFBDBDBDFFA9A9
          A9FF979797FF868686FF797979FF6C6C6CFF5F5F5FFE32323273000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000003B3B3B519E9E9EFDF6F6
          F6FFEAEAEAFFE0E0E0FFD9D9D9FFD2D2D2FFCCCCCCFFCCCCCCFFBDBDBDFFADAD
          ADFF9C9C9CFF8C8C8CFF7F7F7FFF737373FF686868FF5B5B5BFA1C1C1C430000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000020202028949494F1ECECECFFEEEE
          EEFFE4E4E4FFDDDDDDFFD7D7D7FFD0D0D0FFCCCCCCFFCDCDCDFFBEBEBEFFAFAF
          AFFFA0A0A0FF929292FF848484FF797979FF6F6F6FFF646464FF555555EA0E0E
          0E20000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000C0C0C0E888888D6DDDDDDFFF3F3F3FFE7E7
          E7FFE1E1E1FFDBDBDBFFD5D5D5FFCFCFCFFFCCCCCCFFCECECEFFC0C0C0FFB1B1
          B1FFA3A3A3FF969696FF898989FF7F7F7FFF757575FF6D6D6DFF616161FF4949
          49CD050505090000000000000000000000000000000000000000000000000000
          0000000000000000000002020201717171ADCACACAFFF8F8F8FFEDEDEDFFE6E6
          E6FFE2E2E2FFDCDCDCFFD6D6D6FFCECECEFFCACACAFFC9C9C9FFBBBBBBFFABAB
          ABFFA1A1A1FF969696FF8B8B8BFF818181FF787878FF717171FF6A6A6AFF5E5E
          5EFF3F3F3FA20000000000000000000000000000000000000000000000000000
          000000000000000000005C5C5C7ABEBEBEFEFFFFFFFFF0F0F0FFE4E4E4FFD8D8
          D8FFCCCCCCFFC3C3C3FFBEBEBEFFBEBEBEFFBFBFBFFFC1C1C1FFC4C4C4FFC6C6
          C6FFC0C0C0FFB8B8B8FFB0B0B0FF999999FF7E7E7EFF6B6B6BFF666666FF6363
          63FF5A5A5AFE2F2F2F6F00000000000000000000000000000000000000000000
          0000000000003F3F3F48BDBDBDFCDADADAFFBDBDBDFFABABABFF989898FF9494
          94FF969696FF9E9E9EFFA9A9A9FFB5B5B5FFBFBFBFFFCBCBCBFFD2D2D2FFDEDE
          DEFFEAEAEAFFF1F1F1FFF4F4F4FFFAFAFAFFF4F4F4FFDCDCDCFFB2B2B2FF8686
          86FF5E5E5EFF515151FA19191940000000000000000000000000000000000000
          000021212122B1B1B1ECB4B4B4FF999999FF838383FF7F7F7FFF888888FF9393
          93FF9B9B9BFFA4A4A4FFADADADFFB7B7B7FFC0C0C0FFC9C9C9FFD0D0D0FFD9D9
          D9FFE2E2E2FFECECECFFF4F4F4FFFEFEFEFFFFFFFFFFFCFCFCFFF9F9F9FFF1F1
          F1FFD7D7D7FF8D8D8DFF515151E80C0C0C1E0000000000000000000000000A0A
          0A0B9B9B9BCFA4A4A4FF767676FF767676FF808080FF878787FF8E8E8EFF9696
          96FF9E9E9EFFA6A6A6FFAFAFAFFFB8B8B8FFC1C1C1FFCBCBCBFFCFCFCFFFD8D8
          D8FFE1E1E1FFE9E9E9FFF1F1F1FFFAFAFAFFFFFFFFFFF8F8F8FFF3F3F3FFF1F1
          F1FFF1F1F1FFF1F1F1FFB7B7B7FF4E4E4ECB0505050A00000000000000005555
          5575A2A2A2FF6A6A6AFF777777FF7E7E7EFF848484FF8A8A8AFF919191FF9999
          99FFA1A1A1FFA9A9A9FFB1B1B1FFB9B9B9FFC2C2C2FFCACACAFFCFCFCFFFD8D8
          D8FFE0E0E0FFE7E7E7FFEFEFEFFFF6F6F6FFFFFFFFFFFAFAFAFFF6F6F6FFF1F1
          F1FFEDEDEDFFEBEBEBFFF1F1F1FFA0A0A0FF3636368C00000000000000009595
          95CA767676FF747474FF7C7C7CFF818181FF878787FF8E8E8EFF959595FF9C9C
          9CFFA3A3A3FFAAAAAAFFB2B2B2FFBABABAFFC2C2C2FFCACACAFFCFCFCFFFD7D7
          D7FFDFDFDFFFE6E6E6FFEDEDEDFFF4F4F4FFFCFCFCFFFDFDFDFFF8F8F8FFF3F3
          F3FFEFEFEFFFECECECFFECECECFFEDEDEDFF535353E800000000000000008F8F
          8FC17F7F7FFF777777FF7F7F7FFF848484FF8A8A8AFF909090FF979797FF9E9E
          9EFFA5A5A5FFACACACFFB4B4B4FFBBBBBBFFC3C3C3FFCACACAFFCFCFCFFFD6D6
          D6FFDEDEDEFFE5E5E5FFEBEBEBFFF2F2F2FFF9F9F9FFFFFFFFFFF9F9F9FFF5F5
          F5FFF1F1F1FFEEEEEEFFEFEFEFFFE9E9E9FF515151DE00000000000000004747
          4754BABABAFE7A7A7AFF7F7F7FFF878787FF8D8D8DFF939393FF999999FFA0A0
          A0FFA7A7A7FFAEAEAEFFB5B5B5FFBCBCBCFFC3C3C3FFCACACAFFCFCFCFFFD6D6
          D6FFDDDDDDFFE4E4E4FFEAEAEAFFF0F0F0FFF7F7F7FFFDFDFDFFFBFBFBFFF7F7
          F7FFF3F3F3FFF6F6F6FFFBFBFBFF888888FE3131317100000000000000000000
          00006D6D6D76C5C5C5FC949494FF838383FF8C8C8CFF969696FF9C9C9CFFA2A2
          A2FFA8A8A8FFAFAFAFFFB6B6B6FFBDBDBDFFC4C4C4FFCACACAFFCECECEFFD5D5
          D5FFDCDCDCFFE3E3E3FFE9E9E9FFEFEFEFFFF5F5F5FFFCFCFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFE0E0E0FF7C7C7CFE4646468E0101010100000000000000000000
          00000000000037373739B1B1B1C4C8C8C8FEA8A8A8FF989898FF9B9B9BFFA3A3
          A3FFAAAAAAFFB1B1B1FFB7B7B7FFBEBEBEFFC4C4C4FFCACACAFFCECECEFFD6D6
          D6FFDCDCDCFFE5E5E5FFEAEAEAFFF3F3F3FFFCFCFCFFFFFFFFFFFFFFFFFFD0D0
          D0FF909090FE616161D127272749000000000000000000000000000000000000
          00000000000000000000000000003737373883838395AEAEAEE2BABABAFEB2B2
          B2FFB0B0B0FFB2B2B2FFB8B8B8FFC0C0C0FFC9C9C9FFD0D0D0FFD3D3D3FFDBDB
          DBFFDFDFDFFFDFDFDFFFD6D6D6FFC8C8C8FFB2B2B2FF959595FE6E6E6EE85151
          519E252525430101010100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202011E1E1E284646
          465E6565658C7B7B7BB08C8C8CCD959595E19B9B9BF19B9B9BF6969696F69090
          90F2808080E3737373D0606060B44B4B4B90343434631919192F020202030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFCFFE1FFF83800FFF81000FFF80000FFFC0000FFFE0000FFFF010
          0FFFF0000FFFF0000FFFE0000FFFE18007FFE18003FFF38001FFFF0000FFFE00
          00FFFE00007FFC00003FF800001FF000000FF000000FE0000007C00000038000
          0001800000018000000180000001C0000003F000000FFC00003FFF8001FFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000030303062121214D3C3C3C974D4D4DD05A5A5AF3606060FE5D5D5DFD5252
          52F0404040CB2E2E2E9116161644020202030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000050505063939
          3971666666E5838383FF949494FFA2A2A2FFA3A3A3FFA0A0A0FF9D9D9DFF9A9A
          9AFF919191FF7F7F7FFF656565FF434343DC2424246301010103000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001D1D1D32666666D58F8F
          8FFFAFAFAFFFB0B0B0FFABA8ABFFA9A2A8FFACA0AAFFB0A3AEFFACA0AAFFA096
          A0FF999398FF929192FF939393FF8C8C8CFF676767FF404040C70E0E0E250000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000033333352808080F5B1B1B1FFBCBC
          BCFFB8B3B7FFC7B8C5FFCDC9CDFF96CB9DFF73C97DFF5DC467FF5FC66AFF7CCD
          83FFA3CEA7FFD2C8D1FFA99DA8FF918E91FF949394FF7F7F7FFF4A4A4AEE1717
          1740000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000363636528E8E8EFAC4C4C4FFC3C1C3FFCBBD
          C9FFC1CCC3FF4BBD5AFF01A912FF03A917FF06A819FF08A91BFF07A71AFF02A5
          15FF01A40EFF01A60DFF6CCA74FFCECCCEFF9B919AFF939193FF8B8B8BFF4F4F
          4FF51616163D0000000000000000000000000000000000000000000000000000
          000000000000000000002121212F8D8D8DF5CBCBCBFFCCC9CCFFDBC9D8FF7AC3
          84FF01AB1BFF0CAC24FF13AD2BFF12AB28FF0BA722FF09A621FF09A61FFF0CA7
          20FF11AB23FF0EA81EFF03A512FF03A712FFAED8B2FFAEA0ADFF949294FF8C8C
          8CFF4A4A4AEB0C0C0C1E00000000000000000000000000000000000000000000
          00000000000005050505797979D1C5C5C5FFD2D0D2FFE0CDDDFF5CBC6CFF05AE
          20FF16B02EFF18B030FF13AF2EFF2C9A3AFF619060FF5A8B58FF5C8C5AFF4E93
          50FF09A51FFF12AB24FF10A822FF0BA71CFF01A105FF9BD69FFFADA0ABFF9796
          97FF818181FF3D3D3DB900000000000000000000000000000000000000000000
          00000000000047474769B0B0B0FFD9D7D8FFDED1DCFF75BE81FF06AF25FF19B2
          35FF1AB235FF1AB235FF11B02EFF6AA86EFFFFF4FFFFFFEDF5FFFFF1FBFFE0CE
          C9FF019D18FF14AE28FF12AA25FF11A922FF0CA71CFF01A004FFB6D9B8FF9E96
          9EFF9E9D9EFF666666FE1E1E1E4E000000000000000000000000000000000000
          0000030303038A8A8ADED7D7D7FFDCD6DBFFBBC7BDFF07B125FF1BB438FF1CB4
          39FF1CB43AFF1DB63AFF13B333FF6EB273FFFFF9FFFFFFF3FFFFFFF5FFFFE4D6
          D1FF02A11DFF15AE2DFF14AC29FF12AA26FF11A922FF0AA71BFF0AA919FFD4CB
          D3FF9B999BFF919191FF454545C7000000000000000000000000000000000000
          00002C2C2C43B3B3B3FFDFDFDFFFE5D5E2FF41B658FF15B536FF1EB63DFF1FB7
          3EFF1FB73FFF1FB73FFF1DB63CFF20B13EFF2AA441FF209D38FF249E3BFF29AA
          3FFF12AF2DFF17AF2FFF16AE2CFF14AC29FF12AA25FF10A822FF01A410FF81CF
          87FFA9A0A9FFA3A3A3FF646464FE0F0F0F280000000000000000000000000000
          00005D5D5D8DCCCCCCFFE4E1E3FFC9C9C9FF08B52BFF20B840FF21B942FF21B9
          43FF21B943FF21B943FF21BC43FF19A238FF91B393FF82A786FF8BAC8CFF3EA1
          50FF14B233FF19B233FF17AF2FFF15AD2CFF14AC28FF12AA24FF0CA81CFF10AB
          1EFFD8C6D6FFA4A3A4FF818181FF272727720000000000000000000000000000
          0000828282C5DEDEDEFFEBE3E9FF83BB90FF13B937FF22BA44FF23BB46FF23BB
          48FF24BC48FF24BC48FF22BE47FF1F9B3DFFFFFFFFFFFFFFFFFFFFFFFFFF74B4
          7DFF12B432FF1AB437FF19B232FF17AF2FFF15AD2BFF13AB27FF11A923FF01A3
          0AFFBECEC0FFA9A6A9FF979797FF393939A80000000000000000000000000000
          00009C9C9CE7E9E9E9FFEFE4EDFF5AB46DFF1ABB40FF24BC49FF25BD4BFF26BE
          4CFF26BE4DFF26BE4DFF25C14CFF1E9C3DFFFFFFFFFFFFFEFEFFFFFFFFFF79A3
          7FFF018314FF13AF32FF1AB535FF18B131FF16AE2DFF14AC29FF12AA25FF01A5
          12FF96CB9BFFB1AAB0FFA8A8A8FF474747CB0000000000000000000000000000
          0000ADADADF6EDEDEDFFF0E2EDFF4AB462FF1FBE46FF26BE4DFF27BF4FFF28C0
          51FF29C153FF29C153FF27C251FF24AC48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE3E0DDFF388645FF089C24FF18B333FF17AF2FFF15AD2BFF13AB27FF02A6
          14FF81C989FFB9B0B8FFB4B4B4FF505050DD0000000000000000000000000000
          0000AFAFAFF4EEEEEEFFF2E4EFFF4BB563FF20BE48FF28C051FF29C153FF2AC2
          55FF2BC356FF2BC356FF2BC455FF23C04FFF10AD39FF57BB70FFF4F6F3FFFFFF
          FFFFFFFFFFFFFFFFFFFF6C9E71FF08A225FF18B331FF16AE2DFF14AC28FF03A7
          17FF83C98AFFBDB4BCFFB8B8B8FF535353DA0000000000000000000000000000
          0000A3A3A3E1F1F1F1FFF9EEF6FF5CB273FF20C14AFF2AC254FF2BC357FF2DC5
          5AFF2DC55BFF2DC55BFF2DC65BFF2CC559FF29C354FF1DBF48FF0BA331FFE6F0
          E8FFFFFFFFFFFFFDFEFFFFFFFFFF228D33FF15B22FFF17B02EFF14AC2AFF01A7
          15FF98C99DFFC1B9BFFFB4B4B4FF4C4C4CC50000000000000000000000000000
          0000888888B9E9E9E9FFFBF4F9FF8AB996FF1BC149FF2BC357FF2DC55CFF2FC7
          5FFF30C960FF2EC75FFF2FCA5EFF2DC85DFF2DC658FF2AC554FF1ABC45FF64B2
          78FFFFFFFFFFFFFBFBFFFFFFFFFF87B68AFF0CAC28FF17AF2FFF15AD2AFF01A7
          10FFC6CFC7FFC4C1C4FFAAAAAAFF4141419D0000000000000000000000000000
          00005E5E5E7DDEDEDEFFF9F6F8FFD0CACDFF17C246FF2BC459FF2FC75DFF32CA
          61FF2DC961FF4EB370FF188C3FFF19AF48FF22BF51FF22C04EFF0BA935FF6CA7
          7AFFFFFFFFFFFFFDFDFFFFFCFFFFAAC6A6FF09A926FF17AF31FF10AD28FF1CAF
          30FFE3D2E1FFC9C9C9FF979797FF2B2B2B620000000000000000000000000000
          00002929292FD0D0D0FEFAFAFAFFF9ECF5FF4EB36BFF26C655FF2FC75FFF33CC
          65FF26C45AFFACD2B9FFFFFFFFFF9EBCA8FF59986BFF4A925FFF78A584FFFFFF
          FFFFFFFFFFFFFFFCFCFFFFFFFFFF85BC89FF0EAE2BFF17AF30FF04AA1CFF86C4
          8FFFD7CED6FFD2D2D2FF7B7B7BFA0D0D0D170000000000000000000000000000
          000000000000AAAAAAC6F1F1F1FFFFFBFEFFC3C3C2FF1CC44EFF2DC85FFF33CC
          64FF25C45AFFA3CEB1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFFFFFFF6FFFF15A12FFF17B333FF11AF2AFF1AB030FFE3D4
          E0FFD3D1D3FFB9B9B9FF585858AC000000000000000000000000000000000000
          00000000000047474749D6D6D6FEFFFEFFFFFFF7FEFF81B28FFF1DC651FF2FC9
          60FF29C75DFF81CE99FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFDAE5D8FF24A83FFF13B333FF16B231FF03AC1FFFBBCBBDFFDBD5
          DBFFDBDADBFF8A8A8AFC1E1E1E30000000000000000000000000000000000000
          00000000000000000000979797AEE9E9E9FFFFFEFFFFFDEEF8FF6FB081FF1DC7
          50FF2BC85BFF24C555FF12B444FF39BA60FF65C781FF7DCC91FF80CC92FF68C3
          7DFF2AAE48FF08AD2DFF18B539FF16B433FF06AF23FFA3C4A8FFE5DBE3FFDEDD
          DEFFADADADFF5353539200000000000000000000000000000000000000000000
          0000000000000000000015151514B4B4B4DFF5F5F5FFFFFFFFFFFFF0FAFF88B3
          95FF1EC24EFF24C754FF2AC659FF28C454FF22C14DFF1DBE48FF1BBC43FF1BBB
          42FF1FBA41FF1BB73DFF0EB52FFF1CB136FFB6C7B8FFE8DEE7FFE1E0E1FFC6C6
          C6FF717171CF0707070A00000000000000000000000000000000000000000000
          00000000000000000000000000002828282ABDBDBDE8F5F5F5FFFFFFFFFFFFFD
          FFFFD2CBD0FF5EB075FF1DBD4AFF17C144FF1CC047FF1DBE46FF1ABC41FF14BA
          3AFF0BB631FF1DB33CFF77B983FFE5D6E2FFEBE5EAFFE7E7E7FFCBCBCBFF7F7F
          7FDD1313131B0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000026262627B0B0B0DAE8E8E8FFFFFF
          FFFFFFFFFFFFFFF9FFFFE6D6E0FFABC2AFFF7BB78AFF65B278FF68B378FF84BA
          90FFBAC7BDFFEBD9E8FFF4EAF3FFEDEAECFFEAEAEAFFBDBDBDFF797979CD1414
          141B000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001010100F8A8A8A9ED4D4
          D4FDF0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFBFFFFFFF8FFFFFDF6
          FCFFF8F4F7FFF3F2F3FFF1F1F1FFD5D5D5FFA9A9A9FB6060608D070707080000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003434
          3435989898AECACACAF9DEDEDEFFEBEBEBFFF4F4F4FFF7F7F7FFF5F5F5FFEDED
          EDFFDEDEDEFFCACACAFFADADADF6757575A22121212A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000141414164747475B72727294898989B9929292C88F8F8FC88282
          82B6656565903B3B3B540D0D0D10000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFF00FFFFF8001FFFF0000FFFC00007FF800003FF800
          001FF000000FE000000FE0000007E0000007C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003E0000007E0000007F000000FF000
          000FF800001FFC00003FFE00007FFF0000FFFFC003FFFFF00FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002B2B
          2B57515151B4494949A14040408B373737762E2E2E612626264C1B1B1B361111
          11210606060C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000005050509454545997B7B
          7BFEC6C6C6FFC5C5C5FFB7B7B7FFACACACFFA1A1A1FF979797FF8F8F8FFF8686
          86FF7F8080FE7E7976F5A34B01E2904106CD7E3805B86E2E05A25D28078D4E20
          06783D200E622A2B2B4C1F1F1F360A0A0A100000000000000000000000000000
          00000000000000000000000000000000000014141429595959D1909090FFC7C7
          C7FFE4E4E4FEEDEDEDFFEBEBEBFFEAEAEAFFE8E8E8FFE7E7E7FFE4E4E4FFE3E3
          E3FFE2E2E3FFD9D6D3FFB55401FFD98A1DFFD28019FFCB7816FFC56F12FFB95E
          0AFF9E4B1BFF9CA1A4FF919191FE4848487E0000000000000000000000000000
          00000000000000000000000000002E2E2E5F696969F3A7A7A7FFC6C6C6FFC1C1
          C1FFE4E4E4FEEEEEEEFFEAEAEAFFE8E8E8FFE6E6E6FFE4E4E4FFE2E2E2FFDFDF
          DFFFDDDEDEFFD9D5D1FFB95801FFE59C32FFE59930FFE49A2FFFE49A2DFFDA8A
          20FFA85523FFD3D9DCFFB2B2B2FF4F4F4F8B0000000000000000000000000000
          0000000000000707070C474747A17A7A7AFEB7B7B7FFBFBFBFFFBDBDBDFFBCBC
          BCFFE7E7E7FFF0F0F0FFECECECFFEAEAEAFFE8E8E8FFE6E6E6FFE4E4E4FFE1E1
          E1FFDFE0E0FFDCD8D3FFBC5D01FFEAA541FFE7A13FFFE79F3DFFE69E39FFDA8B
          29FFAB5C27FFCFD5D9FFB4B4B4FE575757980000000000000000000000000000
          00001616162D5A5A5AD68D8D8DFFBCBCBCFFB9B9B9FFB9B9B9FFBABABAFFB9B9
          B9FFE9E9E9FEF2F2F2FFEFEFEFFFECECECFFEAEAEAFFE8E8E8FFE5E5E5FFE3E3
          E3FFE1E2E2FFDDD9D5FFC16201FFD48735FFD17C25FFEAA54AFFE9A446FFDD92
          34FFAE6025FFD0D5D9FFB8B8B8FF5F5F5FA40000000000000000000000000000
          00004C4C4CA8979797FFBABABAFFB6B6B6FFB5B5B5FFB6B6B6FFB7B7B7FFB5B5
          B5FFEDEDEDFEF4F4F4FFF6F6F6FFF5F5F5FFF3F3F3FFF0F0F0FFEEEEEEFFE9E9
          E9FFE4E4E5FFDFDBD7FFC46801FFD78B40FFD07A27FFEDAC57FFEDAA54FFE097
          3DFFB26426FFD2D7DBFFBEBEBEFE656565B10000000000000000000000000000
          0000505050B3A4A4A4FFB4B4B4FFB2B2B2FFB2B2B2FFB2B2B2FFB3B3B3FFB2B2
          B2FFF3F3F3FFFAFAFAFFA4A4A4FF818181FF878787FF8D8D8DFF949494FFA6A6
          A6FFE9E9EAFFE1DED9FFC76B01FFF4B96DFFF3B669FFF0B165FFF0B160FFE29A
          44FFB76C2BFFD4D9DDFFC3C3C3FF6E6E6EBD0000000000000000000000000000
          0000555555BCA3A3A3FFAFAFAFFFAEAEAEFFAFAFAFFFAFAFAFFFB1B1B1FFAFAF
          AFFFF5F5F5FEFDFDFDFF9C9C9CFFACACACFFBFBFBFFFC0C0C0FFBFBFBFFF9494
          94FFEDEDEEFFE3E0DBFFCA7001FFF7BC79FFF4B974FFF5B972FFF3B56BFFE59E
          4CFFBA702CFFD5DADFFFC8C8C8FE767676CA0000000000000000000000000000
          00005B5B5BC6A3A3A3FFACACACFFABABABFFABABABFFACACACFFADADADFFABAB
          ABFFFAFAFAFEFFFFFFFF959595FFA7A7A7FFB8B8B8FFBCBCBCFFBDBDBDFF9191
          91FFEFF0F0FFE6E2DCFFCE7301FFF9C486FFF9C182FFF8BD7EFFF5BB75FFE7A2
          52FFBD742BFFD7DCE1FFCECECEFF7E7E7ED60000000000000000000000000000
          0000606060CFA4A4A4FFA9A9A9FFA8A8A8FFA8A8A8FFA9A9A9FFABABABFFA9A9
          A9FFFEFEFEFFFFFFFFFF8F8F8FFFA3A3A3FFB2B2B2FFB6B6B6FFB8B8B8FF8C8C
          8CFFF3F3F4FFE8E4DEFFD27A01FFFFCD9AFFFECC99FFFCC88EFFFAC382FFE9A6
          58FFC07B32FFDADFE4FFD3D3D3FE868686E30000000000000000000000000000
          0000646464D9A3A3A3FFA6A6A6FFA4A4A4FFA5A6A6FFA6A8A9FFA8ABAEFFA8A7
          A8FEEEF0F1FFFFFFFFFF888888FFA1A1A1FFB1B1B1FFB5B5B5FFB8B8B8FF8A8A
          8AFFF5F5F6FFEAE5DFFFCF7201FFCF7601FFCD7201FFC96F01FFC86B01FFC466
          01FFC57D2CFFDBE0E4FFD9D9D9FF8E8E8EEF0000000000000000000000000000
          00006B6B6BE3A2A2A2FFA1A1A1FFA1A2A3FFA0A5AAFFA69181FFAC5B15FFAA4C
          01FED1CCC9FFF5F7F8FF7D7D7DFF6F6F6FFF767676FF7D7D7DFF808080FF8282
          82FFF6F6F7FFEFEFF0FFEBEDF0FFEAE9EAFFE6E6E7FFE5E3E1FFE1E1DEFFDFDB
          D6FFDDD9D6FFDCDDDEFFDDDDDDFF919191F80303030400000000000000000000
          0000717171ECA1A1A2FF9DA1A6FFA1968EFFAD6A2AFFB95901FFD98B2BFFD180
          22FFAC550EFFD4D3D2FFE2E3E5FFECEDEDFFE9E9E9FFE3E3E3FFE0E0E0FFDFDF
          DFFFF4F4F4FFF0F0F0FFEDEDEDFFEBEBECFFE9EAEAFFE8E8E8FFE4E5E5FFE2E3
          E3FFE0E1E1FFDEDEDEFFE0E1E2FF93979BFC0808080C00000000000000000101
          010174787DF4A1A09FFFAD773FFFBB5C01FFD68527FFEBA647FFEAA446FFEBA6
          47FFCD7A20FFAE5B18FFD5D8D9FFF2F3F5FFFFFFFFFFFDFDFDFFFBFBFBFFF7F7
          F7FFF4F4F4FFF2F2F2FFF0F0F0FFEDEDEDFFEBEBEBFFE9E9E9FFE6E6E6FFE4E4
          E4FFE2E2E2FFE1E2E3FFE0E5EAFEB8752CFE874201B800000000000000000906
          020C946C41FABE6301FFD3811EFFEAA54AFFECA84EFFEBA64EFFEAA64DFFEAA6
          4CFFECA94EFFC9741BFFAF6223FFD8DBDEFFF3F4F5FFFFFFFFFFFBFBFBFFF9F9
          F9FFF6F6F6FFF4F4F4FFF2F2F2FFEFEFEFFFEDEDEDFFEBEBEBFFE9E9E9FFE6E6
          E6FFE5E6E8FFE2E2E2FECE8B30FEBD5E01FE6030017C0000000000000000A95B
          01DCD07C16FFE9A247FFEDAA52FFEDAA53FFECAA54FFECAB55FFECAB57FFECAA
          56FFEDA954FFEFAD53FFC77018FFB26A30FFDADFE3FFF4F4F5FFFFFFFFFFFBFB
          FBFFF8F8F8FFF6F6F6FFF4F4F4FFF2F2F2FFEFEFEFFFEDEDEDFFEBEBECFFE9EC
          EFFFE4DDD3FFC77614FEB75A01FA512701630000000000000000000000006336
          0178CC7710FEECAC55FFEEAC58FFEEAD5AFFEEAE5DFFEFAF5EFFEFAF5EFFEEAE
          5DFFEEAD5BFFEDAB57FFEFAE57FFC36912FFB5733EFFDCE1E6FFF6F6F7FFFFFF
          FFFFFCFCFCFFF8F8F8FFF6F6F6FFF4F4F4FFF1F1F1FFEFF0F0FFEFF3F7FFE2D0
          BCFFC06601FFB15801F53E1E014D000000000000000000000000000000000000
          0000723E0184CD7914FEF0B163FFF1B263FFF1B265FFF1B368FFF1B368FFF1B2
          65FFF0B163FFEFAF5FFFEFAE5BFFEFAE58FFBF630BFFB77D4DFFDEE3E9FFF7F7
          F8FFFFFFFFFFFAFAFAFFF8F8F8FFF6F6F6FFF5F5F6FFF4FAFFFFDCBB9AFFB959
          01FFAB5301EC2F17013A00000000000000000000000000000000000000000000
          0000000000007443018FCF7E1AFFF4B86FFFF4B76EFFF3B76FFFF3B76FFFF3B6
          6FFFF2B56CFFF1B366FFEFB061FFEFAE5CFFEEAD56FFBB5D05FFBB885FFFDFE5
          EAFFF8F8F9FFFDFDFDFFFAFAFAFFF9F9FAFFFAFFFFFFD19F72FFB35201FFA24E
          01E0211001290000000000000000000000000000000000000000000000000000
          000000000000020101017845019BD28222FEF7BF7CFFF5BC79FFF5BB78FFF5BA
          76FFF4B872FFF3B66DFFF1B368FFEFB160FFEEAD5AFFECAA52FFB85801FFBD93
          6FFFE1E6EBFFF9FAFAFFFEFFFFFFFDFFFFFFC37F4AFFB14E01FF964901D2160B
          011C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000003020103834701A6D6882BFFFAC487FFF9C081FFF7BE
          7FFFF6BD7AFFF4B974FFF2B56CFFF0B264FFEEAE5DFFEDAB56FFEAA64BFFB554
          01FFC19D80FFE4EBF0FFF8F5F3FFB56127FFB04F01FF8A4301C10E0601110000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000004030104884901B0D98F36FFFEC990FFFAC2
          85FFF7BE80FFF5BB79FFF3B770FFF1B368FFEFAF5FFFEDAB57FFECA951FFE7A1
          43FFB45301FFBF9677FFA4460DFFAF4F01FF7B3C01AD08030108000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000008040107904E01B9DE9541FFFECB
          95FFFAC388FFF8BF81FFF7BC78FFF5B870FFF2B567FFF0B25EFFECAA53FFE8A2
          46FFDC8F30FFB15101FFB04F01FE703601970302010300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000A05010B985401C2DB8F
          37FFDC943FFFD6882FFFCF7F22FFC97415FFC16B0AFFBD6102FDB65901F0A74E
          01DC954701C7864001B25D2B0176000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000D08010F6738
          017C5B32016F4A27015A381E014527150130160C011B07030107000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFF81FFFFFF00001FFE000003F8000003F000
          0003E0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000001C0000001800000038000000FC000001FE000003FF000
          007FF80000FFFC0001FFFE0003FFFF0007FFFF8FFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000019301FF019301FF019301FF0193
          01FF019301FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000100701155524
          017B8E3A01D29C4101E9983E01E48B3901CF773001B25F27018E431C0164250F
          01370804010B000000000000000000000000019301FF36CE60FF32CA5AFF1EB6
          35FF019301FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E06010F642B0187A84601F3BC57
          01FFCA6501FFCE6901FFCD6801FFC96401FFC25D01FFBC5701FFB65101FFB04B
          01FFA74201F9973A01D3712C019E451B0161019301FF55ED92FF50E88AFF2FC7
          51FF019301FF0000000000000000000000000000000000000000000000000000
          00000000000000000000010101014D21015FA14501E7BF5A02FFD16C01FFD46F
          01FFD26D01FFD06B01FFCF6A01FFCF6A01FFCE6901FFCE6901FFCC6701FFE3AF
          7DFFE2AE7DFFDDA97DFFD9A47DFFD5A17DFF019301FF54EC8EFF4DE584FF2CC5
          4CFF019301FF0000000000000000000000000000000000000000000000000000
          00000000000011080113863801B3BA5403FED4740EFFD9780BFFD67205FFD46F
          01FFD46F01FFD36E01FFD36E01FFD26D01FFD06B01FFCF6A01FFCD6801FFE4B0
          7DFF019301FF019301FF019301FF019301FF019301FF54EC8DFF4EE684FF2AC2
          48FF019301FF019301FF019301FF019301FF019301FF00000000000000000000
          00001F0E01249C4201DDC7680FFFDF8720FFDD811BFFDB7E15FFDA7B0FFFD977
          08FFD77301FFD67101FFD67101FFD57001FFD36E01FFD26D01FFD06B01FFE6B1
          7DFF019301FF43DB6EFF3AD260FF38D05BFF3DD565FF51E986FF4DE581FF40D8
          6BFF28C045FF2BC34BFF2DC550FF1EB635FF019301FF00000000000000000603
          0107984101D8D3781DFFE39131FFE28C2AFFE28924FFE0861FFFDF8418FFDE7F
          12FFDB7704FFDA7501FFD97401FFD87301FFD67101FFD57001FFD36E01FFE7B3
          7DFF019301FF66FEA4FF59F190FF57EF8EFF53EB88FF50E882FF4DE581FF4DE5
          81FF4EE685FF4DE584FF4FE78AFF34CC5BFF019301FF00000000000000002E13
          013FC46815FFE99D40FFE69537FFE59433FFE5932EFFE48E28FFE48B21FFE287
          1AFFDF7C06FFDD7801FFDC7701FFDB7601FFD97401FFD77201FFD57001FFE8B4
          7DFF019301FF6EFFAFFF62FA9CFF5FF798FF58F08FFF53EB86FF50E882FF51E9
          86FF54EC8CFF54EC8DFF54EC91FF37CF60FF019301FF00000000000000003A19
          0151CA711EFFEBA249FFE99F42FFE99C3FFFE99938FFE89631FFE89229FFE690
          23FFE28008FFE07B01FFDF7A01FFDE7901FFDC7701FFDA7501FFD87301FFEAB5
          7DFF019301FF019301FF019301FF019301FF019301FF59F190FF53EB88FF3AD2
          60FF019301FF019301FF019301FF019301FF019301FF0000000000000000461F
          015FCF7A26FFEFAA54FFECA54EFFEDA347FFECA141FFEC9D3AFFEB9931FFEA95
          2CFFE58308FFE37E01FFE27D01FFE07B01FFDF7A01FFDD7801FFDA7501FFEBB7
          7DFFE9B57DFFE8B37DFFE7B37DFFE6B17DFF019301FF61F99BFF59F18FFF30C8
          4EFF019301FFD6A27DFFD39F7DFFD09C7DFF876551A600000000000000004F24
          016CD48130FFF2B361FFF0AE57FFF0AC51FFF0A74BFFEFA443FFEF9F39FFEF9E
          32FFE88406FFE68101FFE58001FFE37E01FFE17C01FFDF7A01FFDC7701FFDA75
          01FFD77201FFD46F01FFD16C01FFE6B17DFF019301FF63FB9EFF5AF292FF34CD
          54FF019301FFD7A27DFFAC4701FFA54001FF5721018A00000000000000005A28
          017AD98B3BFFF5B96DFFF2B461FFF3B15AFFF2AE53FFF2AB4AFFF2A53FFFF1A3
          39FFEB8702FFEA8401FFE88201FFE68101FFE47F01FFE17C01FFDE7901FFDC77
          01FFD97401FFD67101FFD36E01FFE7B37DFF019301FF6DFFAEFF65FDA3FF3AD2
          5EFF019301FFD8A47DFFAF4A01FFA74201FF5F2401960000000000000000642C
          0188E09548FFF8C176FFF5BB6CFFF5B862FFF5B35AFFF5B04FFFF6AB44FFF5A7
          3CFFEE8601FFED8701FFEB8501FFE98301FFE68101FFE37E01FFE07B01FFDD78
          01FFDA7501FFD77201FFD46F01FFE7B37DFF019301FF019301FF019301FF0193
          01FF019301FFD9A47DFFAF4A01FFA84301FF682701A300000000000000006F32
          0196E4A054FFFAC681FFF7C074FFF7BB69FFF7B760FFF7B454FFF7AF4BFFF7A5
          34FFF28C01FFF08A01FFEF8901FFEB8501FFE88201FFE58001FFE27D01FFDF7A
          01FFDB7601FFD87301FFD57001FFE8B37DFFE6B27DFFE4B07DFFE2AD7DFFDFAA
          7DFFDCA87DFFD9A47DFFB04B01FFA94401FF702A01B000000000000000007936
          01A4EAAC64FFFACB87FFF8C47AFFF8BE6EFFF8BA64FFFAB758FFFCB550FFFBA3
          24FFF99101FFF68F01FFF28C01FFF08A01FFEF8901FFE98301FFE68101FFE37E
          01FFDE7901FFDB7601FFD77201FFD46F01FFCF6A01FFCC6701FFC76201FFC15C
          01FFBC5701FFB65101FFB14C01FFAA4501FF782D01BD0000000000000000843B
          01B2EEB570FFFBCE8DFFF9C57DFFFAC171FFFCC068FFFFC05FFFED9E38FFD66D
          01FFC95C01FFC05601FFBD5501FFBD5001FFBF5101FFC05301FFC55901FFC85D
          01FFCA6101FFCE6701FFD16A01FFD46F01FFD36E01FFCF6A01FFCB6601FFC35E
          01FFBC5701FFB75201FFB14C01FFAC4701FF813001CA00000000000000008F3E
          01BFF0B972FFFBCB89FFFBC77FFFFEC776FFE99A3CFFD06501FFBD6A1DFFAD8D
          70FF515457FFD3DBE4FFC9D1D9FFC7CED3FFC5C4C4FFC4B7AEFFC2A896FFBD97
          7BFFBB865DFFB8723DFFB35D1DFFAF4701FFAF4201FFB54901FFB94E01FFBA52
          01FFBC5501FFB95401FFB24D01FFAC4701FF893301D700000000000000009A43
          01CDF4BC72FFFECE87FFEEA752FFCF6601FFCC7E32FF716458FF99A1A9FFBFC3
          C6FF5F6061FFDDDEDEFFD7D8D8FFD6D7D7FFD4D5D6FFD4D6D7FFD1D4D7FFCFD3
          D6FFCFD4D8FFCCD3D8FFCCD4DAFFCAD5DAFFC6C3C1FFC1A999FFBA8B6BFFB16C
          3DFFA94809FFA63801FFAA4001FFA94301FF913601E40000000000000000A449
          01DBF5B96DFFD57717FFC86910FF705A46FFDCE4EDFF818284FFB8B8B8FFD6D6
          D6FF7A7A7AFFE1E1E1FFE8E8E8FFE5E5E5FFE2E2E2FFE2E2E2FFDFDFDFFFDFDF
          DFFFDCDCDCFFDCDCDCFFD9D9D9FFD9D9D9FFD6D8D9FFD4D7D9FFD4D9DCFFD2D9
          DDFFD3DBE1FFD2D0CFFF876E5DFFA14A09FF993901F00000000000000000B152
          01E9C96201FFA27144FFD4DDE4FF66696CFFECECECFF959595FFCFCFCFFFEDED
          EDFF979797FFDADADAFFF1F1F1FFEFEFEFFFEFEFEFFFECECECFFEBEBEBFFEBEB
          EBFFEAEAEAFFE8E8E8FFE7E7E7FFE6E6E6FFE5E5E5FFE4E4E4FFE1E1E1FFE1E1
          E1FFDEDFDFFFE4E6E6FF929A9EFF864A21F1742C01A00000000000000000A84F
          01DEC96301FF919CA8FFE0E1E2FF7E7E7EFFF5F5F5FFAAAAAAFFE5E5E5FFFFFF
          FFFFC9C9C9FF969696FF989898FFA7A7A7FFB6B6B6FFC5C5C5FFD2D2D2FFE4E4
          E4FFEFEFEFFFF0F0F0FFEFEFEFFFECECECFFEAEAEAFFEAEAEAFFEAEAEAFFE9E9
          E9FFE9E9E9FFEEEEEEFFB2B3B3FF3B3E407F0000000000000000000000001008
          011349311A66A5A9AEFFE7E7E7FF999999FFFFFFFFFFC2C2C2FF949494FF9B9B
          9BFFADADADFFBFBFBFFFC1C1C1FFBEBEBEFFB8B8B8FFB4B4B4FFACACACFF9898
          98FF8E8E8EFF9C9C9CFFADADADFFE0E0E0FFEAEAEAFFE9E9E9FFE9E9E9FFE9E9
          E9FFEAEAEAFFF2F2F2FFC9C9C9FF545454960000000000000000000000000000
          00002D2E3047B6B7B8FFF7F7F7FF919191FF969696FFAAAAAAFFBBBBBBFFC1C1
          C1FFC2C2C2FFCCCCCCFFC0C0C0FFB3B3B3FFA7A7A7FF959595FFD6D6D6FFFCFC
          FCFFEFEFEFFFDFDFDFFFA6A6A6FFCCCCCCFFECECECFFE9E9E9FFE9E9E9FFE9E9
          E9FFE9E9E9FFADADADFFACACACFF707070AE0000000000000000000000000000
          00003A3A3A589E9E9EFFB6B6B6FFD6D6D6FFF2F2F2FFD9D9D9FFC8C8C8FFB8B8
          B8FF999999FFD1D1D1FFF4F4F4FFF0F0F0FFF0F0F0FFB6B6B6FFD6D6D6FFFFFF
          FFFFFFFFFFFFFFFFFFFFC1C1C1FFC4C4C4FFECECECFFE9E9E9FFE9E9E9FFE9E9
          E9FFEAEAEAFF8A8A8AEE3C3C3C5C2C2C2C3D0000000000000000000000000000
          0000030303031D1D1D2C4848487AB0B0B0FFFFFFFFFFFCFCFCFFF7F7F7FFF7F7
          F7FFACACACFFE2E2E2FFF9F9F9FFF3F3F3FFF5F5F5FFB3B3B3FFDBDBDBFFFFFF
          FFFFFFFFFFFFFFFFFFFFCECECEFFBEBEBEFFF1F1F1FFECECECFFECECECFFECEC
          ECFFF0F0F0FF989898F400000000000000000000000000000000000000000000
          000000000000000000003A3A3A5ABFBFBFFFFFFFFFFFFEFEFEFFFEFEFEFFFFFF
          FFFF9E9E9EFFF5F5F5FFFFFFFFFFFDFDFDFFFFFFFFFFB1B1B1FFE5E5E5FFFFFF
          FFFFFFFFFFFFFFFFFFFFDBDBDBFFA3A3A3FFC4C4C4FFCACACAFFD2D2D2FFD9D9
          D9FFE8E8E8FF9C9C9CFD07070709000000000000000000000000000000000000
          0000000000000000000049494972CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF929292FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBCFF979797FFA6A6
          A6FFB1B1B1FFBDBDBDFFC2C2C2FF9E9E9EFF767676D5616161A46E6E6EAC7B7B
          7BC1888888D6949494E20A0A0A0E000000000000000000000000000000000000
          0000000000000000000056565685A9A9A9FFC1C1C1FFCDCDCDFFDCDCDCFFF0F0
          F0FF979797FF939393FE999999FEA2A2A2FFADADADFFBBBBBBFFACACACFF5959
          59952C2C2C453C3C3C5B4A4A4A725A5A5A892B2B2B3C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000C0C0C112C2C2C483D3D3D674F4F4F855F5F5FA37070
          70C2868686E0676767AC0B0B0B11171717262424243A2F2F2F4F3D3D3D641717
          1722000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFC1FFF807C1FFE00001FF800001FF0000001E0000001C000
          0001C00000018000000180000001800000018000000180000001800000018000
          0001800000018000000180000001800000018000000180000003C0000003E000
          0003C0000007F000000FF000000FF000000FF00023FFFC0F7FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000011110E24797871E6313030742525255E0202020200000000000000000000
          0000000000000000000000000000000000000000000000000000100701155524
          017B8E3A01D29C4101E9983E01E48B3901CF773001B25F27018E431C0164250F
          01370804010B0000000000000000000000000000000000000000000000000101
          03020128A0A08A93A4FFCDC7C2FF888887FF2A2A2A6900000000000000000000
          0000000000000000000000000000000000000E06010F642B0187A84601F3BC57
          01FFCA6501FFCE6901FFCD6801FFC96401FFC25D01FFBC5701FFB65101FFB04B
          01FFA74201F9973A01D3712C019E451B01611B0A01220000000001010403042D
          A3A32B77FFFF4CADFFFFE0E6E4FFCFCBC7FF3232327900000000000000000000
          00000000000000000000010101014D21015FA14501E7BF5A02FFD16C01FFD46F
          01FFD26D01FFD06B01FFCF6A01FFCF6A01FFCE6901FFCE6901FFCC6701FFC964
          01FFC76201FFBD5801FFB55001FFAE4901FFA94606FEC69278EE4363D5EE2F7A
          FFFF57B4FFFF69D5FFFF5CBDFFFFAEAEAEFF74736ED300000000000000000000
          00000000000011080113863801B3BA5403FED4740EFFD9780BFFD67205FFD46F
          01FFD46F01FFD36E01FFD36E01FFD26D01FFD06B01FFCF6A01FFCD6801FFCB66
          01FFC86301FFC66101FFC25D01FFC36615FFD7A27BFF4D65D7FF307CFFFF5AB7
          FFFF6DD6FFFF60C0FFFF3C8FFFFF1545D0D22E2C285700000000000000000000
          00001F0E01249C4201DDC7680FFFDF8720FFDD811BFFDB7E15FFDA7B0FFFD977
          08FFD77301FFD67101FFD67101FFD57001FFD36E01FFD26D01FFD06B01FFCE69
          01FFCC6701FFC96401FFCA6E17FFD4A881FF4757CAFF317BFCFF5BB9FFFF6DD6
          FFFF5FBFFFFF3F8EFFFF355EE8FFAE8E8ADB3936353B00000000000000000603
          0107984101D8D3781DFFE39131FFE28C2AFFE28924FFE0861FFFDF8418FFDE7F
          12FFDB7704FFDA7501FFD97401FFD97709FFE0933FFFE5A764FFE7AF74FFE5AD
          72FFE0A161FFD88B3FFFE1B182FF969187FF5C7397FF4BABF4FF6ED9FFFF5EBE
          FFFF3E8DFFFF395EE6FFC49788FFA74A14FF3413015300000000000000002E13
          013FC46815FFE99D40FFE69537FFE59433FFE5932EFFE48E28FFE48B21FFE287
          1AFFDF7C06FFDD7801FFE28C2AFFE5B47DFFAB9276FF857C73FF817F80FF817E
          7DFF7C7267FFA1866CFFD3A87EFF8A847EFFE2D9D2FF7AA4B5FF4EB1F8FF3B8B
          FFFF3C61E4FFC89C87FFAD4E11FFA13C01FF3E17016300000000000000003A19
          0151CA711EFFEBA249FFE99F42FFE99C3FFFE99938FFE89631FFE89229FFE690
          23FFE28008FFE7993DFFD1AC82FF7A7A7AFFB4A58CFFE8C58FFFF3C784FFF3C6
          85FFF3CC97FFBFB09BFF4E5359FF909092FFEEEDECFFEAE1D8FF4C71AAFF3757
          D4FFCD9E85FFB2520FFFAA4501FFA23D01FF471A01700000000000000000461F
          015FCF7A26FFEFAA54FFECA54EFFEDA347FFECA141FFEC9D3AFFEB9931FFEA95
          2CFFE9952DFFD5B085FF818182FFE4C088FFF6C881FFEFC584FFEDC386FFEEC8
          8FFFF1D19CFFF2C482FFF0CB94FF65686DFF9A9B9CFF94918FFF9F998BFFCFA5
          88FFBA5C14FFAF4A01FFAA4501FFA43F01FF4F1D017D00000000000000004F24
          016CD48130FFF2B361FFF0AE57FFF0AC51FFF0A74BFFEFA443FFEF9F39FFEFA0
          37FFEEBD80FF868481FFE5C28CFFF3CA88FFEDC788FFEDC788FFECC687FFEBC4
          80FFEBBE7CFFEFCC97FFF2CF94FFF0CA96FF4E525AFFC69E7AFFD0A883FFC56E
          24FFB65101FFB14C01FFAC4701FFA54001FF5721018A00000000000000005A28
          017AD98B3BFFF5B96DFFF2B461FFF3B15AFFF2AE53FFF2AB4AFFF2A53FFFF4B7
          64FFC3A882FFB0A189FFF8D190FFF0CD92FFEFCF93FFEECD93FFEECC90FFEDC9
          8CFFECC687FFEBC07DFFEFCD9BFFF3C985FFB2A998FFB09072FFD1843EFFBE59
          01FFB85301FFB34E01FFAF4A01FFA74201FF5F2401960000000000000000642C
          0188E09548FFF8C176FFF5BB6CFFF5B862FFF5B35AFFF5B04FFFF6AB44FFF9C8
          84FFA19587FFDDC391FFF3D499FFF1D49DFFF1D49DFFF1D49DFFF1D29AFFEFD1
          97FFEDCB8FFFECC688FFEBC380FFF4D4A6FFEECB99FF86786AFFD89456FFBF5A
          01FFBA5501FFB44F01FFAF4A01FFA84301FF682701A300000000000000006F32
          0196E4A054FFFAC681FFF7C074FFF7BB69FFF7B760FFF7B454FFF7AF4BFFFBCE
          8FFF8F8B87FFF6D79AFFF6DEAAFFF4DDABFFF5E0B3FFF4DEAEFFF4DAA5FFF1D5
          9DFFEFD197FFEDCB8FFFECC584FFEFCE9BFFF4CC8EFF86817CFFDDA169FFC05B
          01FFBB5601FFB55001FFB04B01FFA94401FF702A01B000000000000000007936
          01A4EAAC64FFFACB87FFF8C47AFFF8BE6EFFF8BA64FFFAB758FFFCB550FFFDCC
          86FF938F8CFFFBDCA2FFF8E6BBFFFAE9C4FFFAE7BEFFF7E4B8FFF5DFB0FFF4D9
          A7FFF1D59DFFEFD096FFEDC98BFFEFCE97FFF3CA8FFF88827DFFDEA26AFFC15C
          01FFBC5701FFB65101FFB14C01FFAA4501FF782D01BD0000000000000000843B
          01B2EEB570FFFBCE8DFFF9C57DFFFAC171FFFCC068FFFFC05FFFED9E38FFE5A3
          61FFA99C93FFDBC59AFFFBECCDFFF9ECCFFFF8F2E0FFF9EBD1FFF6E3BBFFF3DF
          B4FFF1D5A3FFEFD099FFECCA8EFFF2CF96FFE5C795FF908173FFDD9957FFC35E
          01FFBC5701FFB75201FFB14C01FFAC4701FF813001CA00000000000000008F3E
          01BFF0B972FFFBCB89FFFBC77FFFFEC776FFE99A3CFFD06501FFBD6A1DFFBFA6
          8FFFA9AAACFFAFA897FFFCF9DDFFFCFBEFFFFCFBF9FFFCFBF0FFF8EECFFFF3E7
          C6FFF0DBAEFFEED3A0FFECCB94FFF6CF91FFA99D8DFFB08E79FFC66F30FFBA52
          01FFBC5501FFB95401FFB24D01FFAC4701FF893301D700000000000000009A43
          01CDF4BC72FFFECE87FFEEA752FFCF6601FFCC7E32FF716458FF99A1A9FFC1C4
          C7FFAEAEAFFFA4A3A6FFE5DEC0FFFDFDE8FFFDFDF4FFFAF9E9FFF8F3DAFFF5EB
          C9FFF2E2B9FFEFDAADFFF3DAA2FFE3CC9CFF7D8186FFDBCEC7FFBB8C6DFFB16C
          3DFFA94809FFA63801FFAA4001FFA94301FF913601E40000000000000000A449
          01DBF5B96DFFD57717FFC86910FF705A46FFDCE4EDFF818284FFB8B8B8FFD6D6
          D6FF8F8F8FFFEBEBEBFF9D9D9CFFE8E1C7FFFDFDE7FFFDFBE7FFF9F3D9FFF8EC
          C7FFF7E4B7FFFBE1ACFFE3CBA0FF818386FFD2D3D4FFD9DCDEFFD4D9DCFFD2D9
          DDFFD3DBE1FFD2D0CFFF876E5DFFA14A09FF993901F00000000000000000B152
          01E9C96201FFA27144FFD4DDE4FF66696CFFECECECFF959595FFCFCFCFFFEDED
          EDFF979797FFE4E4E4FFEEEEEEFFA2A2A6FFB2AD9DFFE1D7B7FFFDEEC2FFFCE9
          BBFFE1CFA9FFAFA796FF83878AFFD7D7D8FFEBEBEBFFE4E4E4FFE1E1E1FFE1E1
          E1FFDEDFDFFFE4E6E6FF929A9EFF864A21F1742C01A00000000000000000A84F
          01DEC96301FF919CA8FFE0E1E2FF7E7E7EFFF5F5F5FFAAAAAAFFE5E5E5FFFFFF
          FFFFC9C9C9FF969696FFABABABFFD1D1D1FFC1C1C3FFA1A2A6FF8A8B8FFF8688
          8EFF999CA0FFBBBCBDFFF0F0F0FFEFEFEFFFEAEAEAFFEAEAEAFFEAEAEAFFE9E9
          E9FFE9E9E9FFEEEEEEFFB2B3B3FF3B3E407F0000000000000000000000001008
          011349311A66A5A9AEFFE7E7E7FF999999FFFFFFFFFFC2C2C2FF949494FF9B9B
          9BFFADADADFFBFBFBFFFC1C1C1FFC1C1C1FFCBCBCBFFD3D3D3FFD5D5D5FFCACA
          CAFFBDBDBDFFB5B5B5FFB0B0B0FFE0E0E0FFEAEAEAFFE9E9E9FFE9E9E9FFE9E9
          E9FFEAEAEAFFF2F2F2FFC9C9C9FF545454960000000000000000000000000000
          00002D2E3047B6B7B8FFF7F7F7FF919191FF969696FFAAAAAAFFBBBBBBFFC1C1
          C1FFC2C2C2FFCCCCCCFFC0C0C0FFB3B3B3FFA7A7A7FF959595FFD6D6D6FFFCFC
          FCFFEFEFEFFFDFDFDFFFA6A6A6FFCCCCCCFFECECECFFE9E9E9FFE9E9E9FFE9E9
          E9FFE9E9E9FFADADADFFACACACFF707070AE0000000000000000000000000000
          00003A3A3A589E9E9EFFB6B6B6FFD6D6D6FFF2F2F2FFD9D9D9FFC8C8C8FFB8B8
          B8FF999999FFD1D1D1FFF4F4F4FFF0F0F0FFF0F0F0FFB6B6B6FFD6D6D6FFFFFF
          FFFFFFFFFFFFFFFFFFFFC1C1C1FFC4C4C4FFECECECFFE9E9E9FFE9E9E9FFE9E9
          E9FFEAEAEAFF8A8A8AEE3C3C3C5C2C2C2C3D0000000000000000000000000000
          0000030303031D1D1D2C4848487AB0B0B0FFFFFFFFFFFCFCFCFFF7F7F7FFF7F7
          F7FFACACACFFE2E2E2FFF9F9F9FFF3F3F3FFF5F5F5FFB3B3B3FFDBDBDBFFFFFF
          FFFFFFFFFFFFFFFFFFFFCECECEFFBEBEBEFFF1F1F1FFECECECFFECECECFFECEC
          ECFFF0F0F0FF989898F400000000000000000000000000000000000000000000
          000000000000000000003A3A3A5ABFBFBFFFFFFFFFFFFEFEFEFFFEFEFEFFFFFF
          FFFF9E9E9EFFF5F5F5FFFFFFFFFFFDFDFDFFFFFFFFFFB1B1B1FFE5E5E5FFFFFF
          FFFFFFFFFFFFFFFFFFFFDBDBDBFFA3A3A3FFC4C4C4FFCACACAFFD2D2D2FFD9D9
          D9FFE8E8E8FF9C9C9CFD07070709000000000000000000000000000000000000
          0000000000000000000049494972CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF929292FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBCFF979797FFA6A6
          A6FFB1B1B1FFBDBDBDFFC2C2C2FF9E9E9EFF767676D5616161A46E6E6EAC7B7B
          7BC1888888D6949494E20A0A0A0E000000000000000000000000000000000000
          0000000000000000000056565685A9A9A9FFC1C1C1FFCDCDCDFFDCDCDCFFF0F0
          F0FF979797FF939393FE999999FEA2A2A2FFADADADFFBBBBBBFFACACACFF5959
          59952C2C2C453C3C3C5B4A4A4A725A5A5A892B2B2B3C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000C0C0C112C2C2C483D3D3D674F4F4F855F5F5FA37070
          70C2868686E0676767AC0B0B0B11171717262424243A2F2F2F4F3D3D3D641717
          1722000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFE3FF807FC1FE000381F8000001F0000001E0000003C000
          0001C00000018000000180000001800000018000000180000001800000018000
          0001800000018000000180000001800000018000000180000003C0000003E000
          0003C0000007F000000FF000000FF000000FF00023FFFC0F7FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000011E2326017080A2018FAADE0192
          B2F00181A1E4015E7AB1011F2839000000000000000000000000000000000000
          0000000000000000000000000000000000000404040631313168535353BE5F5F
          5FE75A5A5AEF4C4C4CDE343434A8151515420000000000000000000000000000
          0000000000000000000000000000010A0B0B01B4C6E801C9DFFF01CCE5FF01BE
          DDFF01B5D6FF019EC5FF017EA9F701161C250000000001191D1E01515D6D016F
          829B01758DAF016E88AE015B759701394D656B6462BD8A8A8AFF9C9C9CFFA4A4
          A4FFA0A0A0FF8F8F8FFF727272FF4F4F4FFD2121216E00000000000000000000
          00000000000000000000000000000123252501DEECFF01D5E4FF01CEE0FF01C8
          DCFF01B2D4FF01CDE6FF01A4CBFF012736530167717D01B8CFFA01B9D2FF01B9
          D4FF01B6D4FF01ADCFFF01A1C7FF198FB0FFADA29FFFC1C1C1FFBBBBBBFFBDBD
          BDFFB4B4B4FFA0A0A0FFB3B3B3FF7F7F7FFF434343E400000000000000000000
          00000000000000000000010306060D252D3936E7EBF63FF8FCFF0FE0EBFF01D2
          E3FF01D1E7FF01C7E0FF0192B9F401353C4C01CCDDFE01C6DDFF01C0DBFF01C9
          E0FF01C1DAFF01C0D9FF01BAD8FF22A7C4FFD3C5C3FFBFBFBFFFC9C9C9FF9090
          90FFBDBDBDFF999999FFAFAFAFFF8F8F8FFF5B5B5BEF00000000000000000000
          000001010101013E5972016F9DEC2F8CB8FFD5E1EBFFE9F0F4FF48DBE9FF01CA
          E0FF01B5D2FF0193BCFF016B9BF21681A5E727EFF6FF21D9E6FF06E9F4FF01DE
          ECFF01E0F1FF01B3D4FF01C4E5FF01E3F4FFD5CACAFFCDCCCCFF9F9F9FFFACAC
          ACFF939393FFA7A7A7FFACACACFF9D9C9CFF464646C300000000000000000000
          000001486784017AAEFE017CAEFF017DB1FF0581B3FF0381B3FF017BB0FF017B
          B0FF017CAFFF017CB0FF1788B7FF6CB6D1FFE9FFFFFFB6FFFFFF45F1F4FF5597
          9DFF786B6FFF7A6966FF6F5E5BFF555152FF9DA3A6FFFFFFFFFFFFFFFFFFEAE0
          DEFFC0B6B5FFB9AFADFFA09491FF5D5B5CD70E0C0C2300000000000000000115
          1F29017AADFB017FB1FF0181B4FF0184B7FF0186B9FF0187BAFF0187BAFF0187
          BAFF0186B9FF0185B8FF1C90BEFF69B3D1FFD6E7F0FFA2FCFEFFABB0B0FFB0A8
          A7FFB6B4B4FFBBBBBBFFA6A6A6FF9B9A9AFF868686FF7B7372FF61C6D3FF5BC4
          D0FF4DACBAFF36A0B4FF1094B3FF0182ACF8012E415F00000000000000000143
          5E890180B2FF0184B7FF0187BAFF018ABDFF018CBFFF018EC1FF018EC1FF018E
          C1FF018DC0FF018CBFFF018ABCFF0F8DBEFF2193C0FF1588B7FFCEC1BEFFC0C0
          C0FFC4C4C4FFA0A0A0FFB8B8B8FFA4A4A4FF9A9A9AFF746665FF04B1BDFF01CB
          E3FF01D3E6FF01B6D4FF01B8DCFF01B3D4FF01688DD200000000000000000162
          8AC60184B7FF018BBEFF018FC2FF0191C4FF0193C6FF0195C8FF0195C8FF0195
          C8FF0194C7FF0192C5FF018FC2FF018CBFFF0187BBFF0181B7FFC3C9CBFFE9E6
          E6FFBDBCBCFFB3B2B2FF929090FFB2B0B0FF9F9D9DFF696666FF14E9EFFF0AE4
          EFFF01BFD6FF01C9DEFF01B7D6FF01B9DAFF016A8BC200000000000000000173
          A0E4018BBEFF018FC2FF0193C6FF0197CAFF079BCDFF0F9ED0FF12A0D1FF119F
          D1FF0C9DCFFF0499CCFF0196C9FF0192C5FF018DC0FF1795C3FF91CBE0FFFFFF
          FFFFE4E9EBFFC3BDBAFFA8A19FFF7D868BFF5F5D5CB996B8B9C5F5FDFDFDBBF7
          FBFF32E2F0FF01C7E2FF01B9D9FE018CA7C8011B222D00000000000000000178
          A7EB0190C3FF0194C7FF059ACCFF15A1D3FF22A6D6FF2BAAD9FF2FACDAFF2EAB
          DAFF29AAD8FF1DA4D4FF0E9ECFFF0197CAFF0192C5FF018DC1FF1490BFFF2796
          C1FF1387B7FF0174ABFF016FA3FF016CA2FF031822302D2D2D2C484848473A4D
          4D4C1041464A012F373E0113181A000000000000000000000000000000000171
          9DDB0193C6FF059ACCFF19A3D3FF2DACD9FF3DB1DEFF47B6E2FF4CB8E3FF4BB9
          E3FF43B4E2FF37B0DCFF26A9D7FF119FD0FF0197CAFF0191C4FF018BBEFF0185
          B8FF0180B2FF017AACFF0174A6FF0170A4FE01090E1100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000161
          86B80193C7FF15A2D3FF2DACD9FF43B5E0FF56BDE6FF64C3EBFF6CC6EDFF69C4
          EEFF5FC1E9FF4EBAE4FF3AB0DDFF24A8D6FF0B9DCEFF0195C8FF018EC1FF0188
          BBFF0182B5FF017CAEFF0176A8FF016C9CE30000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000145
          5F800792C4FF23A8D8FF3DB1DEFF56BDE6FF6CC6EFFF80CEF5FF88D2F8FF86D1
          F7FF78CBF2FF63C2ECFF4BB7E3FF32AEDCFF17A2D2FF0197CAFF0191C4FF018A
          BDFF0183B6FF017EB0FF0177A9FF015379AD0000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000011F
          2A34018EC0FE2EADDCFF47B7E3FF63C1ECFF7FCDF3FF96D7FCFF9FDCFFFF9DDA
          FFFF8DD4F9FF74C9F1FF56BCE7FF3CB2DEFF1FA5D5FF059ACCFF0192C5FF018B
          BEFF0185B8FF0180B2FF0176A8FF013147630000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000179A6CF25A6D4FF4CBAE4FF68C4ECFF85D1F7FF9EDCFFFF9EDCFFFF9FDC
          FFFF96D7FCFF7ACCF3FF5CC0E8FF3EB2DFFF22A6D6FF079BCDFF0193C6FF018C
          BFFF0186B9FF017EB0FF016B9BE901080A0D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001364A540794C5FE4DBAE4FF66C3ECFF81CFF5FF99DAFDFF9EDBFFFF9FDB
          FFFF91D5FAFF75CAF2FF59BDE7FF3CB1DEFF20A7D5FF069ACCFF0192C5FF018D
          C0FF0186B9FF0178ABFE012F4458000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001678CAB189DCEFF5FC2EBFF73C9F1FF84D0F7FF90D5FAFF8DD4
          F9FF7ECDF4FF67C4EDFF4FBAE4FF35AEDDFF19A3D3FF0298CBFF0192C5FF018B
          BEFF017CAFFE01425F7B00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000107090901668BA90C97C9FE54BAE5FF6DC7F0FF71C7EFFF71C8
          F0FF65C3ECFF53BCE7FF3FB3DFFF26A8D7FF0E9ECFFF0196CAFF018BBEFF0179
          AAF7013951680000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000013C515C0186B8F03FB2DFFF56BDE8FF51BA
          E5FF49B8E2FF3CB1DEFF2BABD8FF15A2D3FF029ACDFF0188BBFF016C98DB011F
          2B37000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001243238018BBFF935ADDCFF35AD
          DBFF2DA8D9FF20A4D4FF109DCFFF0199CBFF0185BAFF01597FB201090D0E0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001547593119FD1FF2FC2
          ECFF30C9F3FF2CCAF4FF27C4EFFF1AABD9FF015881B601030304000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000004161656490EDFEFE5EE2
          FFFF48DCFFFF42DAFFFF4EDEFFFF73E7FEFE7FBCC4C402020201000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000005B70706F8CE6FCFF3CBB
          E1FF21A4D0FE1E99C6FF2A9CC8FF5FC3E2FF87C5CACA02020201000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002C2C2C2B147CA7EF018F
          C4FF018BBFFF0181B4FF0176A9FF016599FF1B5673A90106080D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000012434450181B5FE0299
          CCFF018CBFFF0182B5FF0179ABFF0170A2FF01689BFF015D8DE8012F3F640101
          0201000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010B10110179A5E11BA1D2FF0295
          C9FF0185B8FF017DAFFF0177A9FF0170A2FF01689AFF016799FF016698FE014B
          66A4000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000015F809B1E9FD0FF1198CAFF0182
          B5FF0179ABFF0178AAFF017CAEFF0177A9FF016EA0FF016799FF016799FF0168
          9DFD01111A220000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010304030190BFF20391C2F7016D9AD10164
          97FB016FA1FF0177A9FF017EB0FF0183B7FF0186B9FC01517298012C4165011D
          2B3C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000132444F01171E22000000000110
          182301426199016593E2016F9FED015476AB011D283400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFE0FFE07FC070001FC000001FC040001E0000001C0000003C000
          0001800000018000000180000003800003FF800003FF800003FF800003FFC000
          03FFC00007FFC00007FFE0000FFFF0001FFFF8007FFFFE00FFFFFE01FFFFFE01
          FFFFFE01FFFFFF01FFFFFF007FFFFE003FFFFC003FFFFC007FFFFFC3FFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010A0D0F01202F3F012A3F5601283B520117
          1E2B010102010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000010B0E110151759F0172A5F40B83B1FB0280B0FA0177A9FA0171
          9DFA016785DB0134416501010201000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001293944016291D22EA4C8F836C0DFFC15A5D2FF0195C8FF028EC0FF017A
          ACFF016498FC016E9DFD014C66A7010202020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000012E
          41520C8EBDFB48C6E0FE3EC3E1FE2BB6DBFF1FA6CFFF108AB8FF01669CFF1F8F
          B8FF2BA0C4FF0176A9FE0176A8FE012A425D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000159
          81B77DEAF4FE88F5FAFF83F0F7FF77E8F2FF6EE2F1FF69E2F2FF5FD7EAFF2F9C
          BFFF098DBDFF39C1E1FF0389BCFE01385585010B0F13012232430126374A011C
          29370107090C0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000016C
          98D091FBFDFF75E6F2FF6ADFEEFF60D8EBFF56D2E8FF4CCBE4FF44C7E2FF3CC4
          E2FF31B4D7FF1DACD6FF139BC8FF015F8FE00A83B1FB038ABCFF017FB4FF0172
          A6FF016A9DF7014F73A701131D25000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000017A
          A6DB83F1F9FF68DEEDFF5DD6EAFF53D0E7FF4ACAE3FF40C3E0FF36BDDDFF2DB6
          DAFF25B1D8FF1CADD7FF0B95C5FF55CEE4FF53D4EBFF44C8E4FF3CBDDCFF34AC
          CFFF1B82ACFF016CA2FF01699AED0111181F0000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000014D
          6B8A3BBCDCFF65DEEFFF54D0E8FF48C9E2FF3DC1DFFF34BBDCFF2AB4D9FF20B0
          D7FF18A9D5FF028EC0FF57C9E0FF8CF8FDFF7DECF5FF70E2F0FF65DCEEFF57D6
          EBFF52D5EBFF4ECBE4FF0A8AB9FF012C415C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000103
          04050160859C1AA6D1FE3DC0DEFF3BC0DFFF31B9DBFF28B2D9FF20AFD8FF12A5
          D1FF0487BAFF016594D962D7E9FF76E9F3FF65DBECFF59D4E9FF4ECCE5FF43C5
          E1FF38BEDDFF2FB9DDFF22A4CDFF013049630000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000012D3E4A0184BAFD3FBFDCFF38BEDEFF10A4D1FF018DC0FF0177
          AAFF016A9DFE0174A9FE44C5E1FF6EE2F0FF57D3E7FF4BCAE4FF40C3E0FF35BD
          DCFF2AB5DAFF21B3DBFF0C8EC9FE012838560125015001050109000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000012D3F4C4DC2DCFF6DE2F1FF32B9DCFF029BCDFF0187BCFF0177
          AAFF016799FF0181B5FE0186BBFE31B5D9FF4CCCE5FF43C7E3FF36BDE1FF2AB7
          E1FF1BABDEFF088ED5FF037173FD077001F6057F08FE025A03D4011901300000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000013E586859CDE2FE72E6F1FF60D8EBFF3CC1E0FF12A3D0FF0186
          B8FF016C9FFF0186B9FE016B9BF601314558015A89A70178B5D6048ABDFA0284
          A7FF037F7AFF098A30FF089B02FF05A307FF0DA418FE017B01FE026A07F80135
          0275010201020000000000000000000000000000000000000000000000000000
          0000000000000150718563D8E9FF8AF6FAFF63DAECFF31B9DCFF17A6D2FF1EA6
          CFFF128CB9FF016398FF0178B0FF014B66AD084605750F7610D2149315FE11A0
          13FF0EA90FFF0CA612FF08A210FF049C08FF17AF28FE018001FF018001FE0270
          04FE014D02BD010F011E00000000000000000000000000000000000000000000
          0000000000000157788B4BC6E0FE59D5EAFF1FADD5FF0191C7FF0180B5FF016F
          A3FF1C8CB5FF3CC1DFFF0C9BD2FF349D4FFF8B9B88FF38A348FE10B023FF10AA
          21FF0DA51BFF0BA314FF059D0BFF019601FF17B228FE018101FF018001FF017E
          01FE017502FD016001ED012B0158000000000000000000000000000000000000
          000000000000017FAFC77FEDF6FF50CDE5FF3DC1E0FF28B2D7FF23A0C8FF0A79
          A9FF015C92FF0185B9FF0BB1E3FFA2A5AEFFBBA4B5FF37A445FF0EAC22FF0EA6
          1CFF08A012FF079F0FFF20B835FF39D160FF49E282FF3BD167FF068905FF017C
          01FF017F01FF017A01FD016901FD014801A30107010D00000000000000000000
          0000000000000BA5D4EC98FEFFFF82EFF7FF74E6F2FF67DDEDFF5BD8ECFF58D7
          EDFF49C3DEFF0C99C7FF73ACBEFFB9ADABFFBAABB7FF32A33FFF09A81AFF25BD
          3EFF3AD261FF49E17EFF3CD473FF3CD470FF34CD68FF30C863FF3DD471FF1BA6
          2FFF017A01FF017D01FF017B01FE016C01FD012E017000000000000000000000
          0000010404030BA0D1F688F2FCFE6ADEF1FF5BD5E9FF4ECCE5FF42C4E1FF35BC
          DDFF26B3D9FF15B0DBFFCFB7B1FFB4B1B1FFB7B3B6FF62B979FF3BE977FF3ADA
          73FF3BD671FF48E07BFF5AF28CFF68FF9AFF68FF9AFF46DB78FF2BBB59FF31C0
          62FF2EBB54FF048506FF017B01FF017601FF013C019700000000000000000000
          000000000000016684942EB99CFE57D4E1FF4ECAF5FF3EC2EDFF31B6E7FF1FAC
          E2FF0EA1D9FF6FAFC9FFD8D0CDFFD9DEDAFFDEE1DFFFD6C8D2FF94A198FF5DCC
          82FF59FD91FF60FB97FF62FA97FF58F08AFF4EE77AFF46DF6CFF2EC54BFF0FA2
          22FF09A112FF23B044FF11931FFE017901FE014B01BD00000000000000000000
          000000000000010102011C9D29D330CB55FF2DC65DFF2BC16EFF30C47FFF37D1
          7EFF6EE189FFFFF9F9FFF1E5EDFFC7C4C7FFB1B1B1FFA7A6A6FFA39EA1FFAA96
          A4FF679B70FF0DB024FF09A118FF079F11FF049C0DFF049C0CFF059D0AFF059E
          09FF019C01FF149926FF21A442FE159629FE015B01E300000000000000000000
          0000000000000000000027B248E933CD65FC33C860F63BCF66F939D065FE31C9
          5BFF24C34FFF12BD3CFF38B454FFB6ABB4FFACACACFFAAAAAAFFA8A8A8FFAEA5
          ACFF6A9E71FF07AC1CFF11A922FF0EA61DFF0CA419FF0AA214FF08A010FF069E
          0BFF019901FF129C20FE097512FE045305BE0138027B00000000000000000000
          0000000000000102010127A44EE934CC66FE30C860FF2EC65CFF2CC456FF29C1
          53FF27BF4EFF1FBD45FF4BBA64FFC6B7C3FFB4B4B4FFB2B2B2FFB0B0B0FFB6AD
          B4FF6FA276FF04AA17FF0EA61CFF09A115FF069E10FF049C08FF049C08FF0BA3
          12FF12AC1FFF22B33CFF037305FE021D04370000000000000000000000000000
          0000000000000000000024A54AD334CD67FF30C860FF2EC65CFF2BC357FF28C1
          53FF25BE4CFF1CBA41FF4EBA66FFCABCC7FFB9B9B9FFB6B6B6FFB3B3B3FFB7B1
          B5FF7BAA83FF1BC641FF2FC954FF34CD5DFF3AD467FF3FD974FF44DC7CFF43DC
          7CFF41DA7AFF42DB78FF017801FF054D0B940000000000000000000000000000
          0000000000000000000029B853EA32C963FE2CC45CFF28C056FF29C254FE2CC6
          58FE31CE5EFF30D45FFF65D686FFEFDBEAFFE1E0E1FFE5E5E5FFE9E9E9FFEEED
          EEFFEAE5EAFF63DB8CFF65F493FF6BF597FF6EF79AFF71FC9EFF73FFA0FF6AFE
          9BFF34CE68FF3CD674FF149F21FF06660DC70000000000000000000000000000
          000000000000010201012AB359F44BE27DFC5EF590FC5CF591FD7AFAA2FD81FA
          A5FF87FBA9FF8DFBABFF8AFCA9FFCCFFD7FFFFF5FEFFF2F0F2FFECEBECFFE7E7
          E7FFE7E2E5FFD8D6D7FF83F9A4FF83FFA9FF7FFFA5FF7CFFA3FF77FFA1FF76FF
          A4FF46DF79FF3AD470FF2EC454FE057509ED0101010100000000000000000000
          000000000000000000000B2D1533249846CE44D774FE60FA93FE77FA9EFFC9FF
          CBFFC4FFC8FFBAFFC3FFB2FFC0FFA7FFB7FFBBFCC9FFF6EDF2FFECE8EBFFE3E3
          E3FFE0DFE0FFE2DBE0FFC0CEC3FF83FFA7FF89FFAEFF83FFAAFF78FFA2FF70FF
          9FFF5AF18CFF40DD78FF43E37FFD087E10FB010D021800000000000000000000
          0000000000000000000000000000010602051865307A2FC45FF751EC86FE5FF8
          92FFB2FFBFFFCAFFCDFFBFFFC6FFAEFFBEFF9CFFB3FF97FCB1FFEDE8EDFFF6E6
          F2FFF1E0EBFFF0DDEAFFF4D9EDFF98C6A8FF2ECB61FF2CBE57FF26B34DFF20A6
          41FE1C9B37FE178F2BFC0F841DED087412D50111031F00000000000000000000
          0000000000000000000000000000000000000000000009261328269446C341DA
          74FE4EEA82FF47E27DFF3ADB71FF36D46BFF31CD64FE28C75CFA33C660EC49B9
          69D732A252C21F8C3EAD10762C980E6525820F54216D0B411958082F10430520
          092D020F06180105010500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010302021652
          285D1762306D134F26580E3B1C430A29142D05150A1802070307000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFE7FFFFFF00FFFFFE007FFFF8003FFFF8003FFFF8000
          0FFF800007FF800003FFC00003FFF00001FFF00000FFE000003FE000001FE000
          0007E0000003E0000001E0000001E0000001F0000001F0000001F0000007F000
          0003F0000003F0000003F8000003FC000003FF0000FFFF8FFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000606060A2E2E2E605C5C5CC65D5D5DCA3535356F0C0C0C180000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001717
          17284848488F717171ECA4A4A4FFE4E4E4FFA3A3A3FF7C7C7CFF757575FA5959
          59B52B2B2B590505050A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404062F2F2F56616161BE8989
          89FDC0C0C0FFF8F8F8FFFFFFFFFFFFFFFFFFA9A9A9FF848484FF848484FF7F7F
          7FFF7A7A7AFF6E6E6EF14C4C4C9F202020420202020300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001313132148484885737373E7A4A4A4FFDCDCDCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFCFCFCFFFEFEFEFFABABABFF898989FF838383FF8383
          83FF868686FF848484FF7F7F7FFF767676FE666666E2404040891616162C0000
          0000000000000000000000000000000000000000000000000000000000000303
          03032B2B2B4C636363B48B8B8BFBBFBFBFFFF5F5F5FFFFFFFFFFFFFFFFFFFEFE
          FEFFFBFBFBFFF9F9F9FFF7F7F7FFF7F7F7FFAEAEAEFF8E8E8EFF7F7F7FFF6F6F
          6FFF757575FF7E7E7EFF868686FF888888FF838383FF7B7B7BFF737373FE5E5E
          5ECE343434720D0D0D1900000000000000000000000000000000000000006A6A
          6AC7A5A5A5FEDADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFBFBFBFFF9F9
          F9FFF7F7F7FFF5F5F5FFF4F4F4FFF7F7F7FFAEAEAEFF949494FF8B8B8BFF7575
          75FF676767FF676767FF6B6B6BFF747474FF808080FF878787FF888888FF8282
          82FF797979FF6E6E6EFB535353B82A2A2A5C0606060C00000000000000007878
          78E3FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFBFBFBFFF9F9F9FFF7F7F7FFF7F7
          F7FFF5F5F5FFEDEDEDFFCBCBCBFFA3A3A3FF818181FF767676FF818181FF9393
          93FF959595FF838383FF727272FF626262FF646464FF686868FF747474FF8181
          81FF898989FF878787FF7F7F7FFF757575FF606060E500000000000000007979
          79E3FCFCFCFFFEFEFEFFFBFBFBFFFAFAFAFFF8F8F8FFF8F8F8FFF2F2F2FFD7D7
          D7FFB7B7B7FFA1A1A1FFBBBBBBFFDBDBDBFFB5B5B5FF8F8F8FFF7B7B7BFF6666
          66FF6E6E6EFF808080FF929292FF959595FF818181FF6E6E6EFF5E5E5EFF5F5F
          5FFF666666FF747474FF858585FF898989FF696969F500000000000000007A7A
          7AE3F9F9F9FFFAFAFAFFF9F9F9FFF7F7F7FFE3E3E3FFC6C6C6FFADADADFFBDBD
          BDFFD6D6D6FFEAEAEAFFE9E9E9FFE7E7E7FFB8B8B8FFA2A2A2FFA0A0A0FFA0A0
          A0FF8F8F8FFF7C7C7CFF686868FF6D6D6DFF7F7F7FFF919191FF959595FF8080
          80FF6A6A6AFF5A5A5AFF7B7B7BFF8D8D8DFF686868F500000000000000007C7C
          7CE3F6F6F6FFECECECFFD4D4D4FFBBBBBBFFC2C2C2FFD6D6D6FFEAEAEAFFEBEB
          EBFFE8E8E8FFE4E4E4FFE2E2E2FFE2E2E2FFC4C4C4FFB2B2B2FFACACACFFA6A6
          A6FFA2A2A2FFA0A0A0FFA0A0A0FF929292FF7D7D7DFF696969FF6C6C6CFF7E7E
          7EFF909090FF939393FF8D8D8DFF929292FF696969F500000000000000007B7B
          7BE0C8C8C8FFC8C8C8FFD8D8D8FFEAEAEAFFEDEDEDFFE9E9E9FFE6E6E6FFE4E4
          E4FFE2E2E2FFE0E0E0FFDEDEDEFFDBDBDBFFD2D2D2FFC8C8C8FFC0C0C0FFB9B9
          B9FFB3B3B3FFACACACFFA6A6A6FFA2A2A2FFA2A2A2FFA0A0A0FF939393FF8080
          80FF6B6B6BFF6B6B6BFF7D7D7DFF8D8D8DFF6B6B6BF500000000000000007B7B
          7BDDE4E4E4FFEFEFEFFFEBEBEBFFE8E8E8FFE6E6E6FFE4E4E4FFE4E4E4FFE0E0
          E0FFDEDEDEFFDBDBDBFFD9D9D9FFDEDEDEFFE8E8E8FFE1E1E1FFD8D8D8FFD0D0
          D0FFC8C8C8FFC1C1C1FFBABABAFFB3B3B3FFADADADFFA7A7A7FFA2A2A2FFA1A1
          A1FFA1A1A1FF949494FF818181FF6C6C6CFF6B6B6BF500000000000000007C7C
          7CDDE6E6E6FFE8E8E8FFE6E6E6FFE5E5E5FFE4E4E4FFE0E0E0FFDEDEDEFFDBDB
          DBFFD9D9D9FFD6D6D6FFDBDBDBFFEEEEEEFFEBEBEBFFECECECFFEAEAEAFFE8E8
          E8FFE1E1E1FFDADADAFFD1D1D1FFC9C9C9FFC1C1C1FFBBBBBBFFB7B5B7FFAEAA
          ADFFACA7ABFFA7A4A7FFA1A0A1FFA1A1A1FF6D6D6DF500000000000000007C7C
          7CDDE1E1E1FFE6E6E6FFE3E3E3FFE1E1E1FFDEDEDEFFDBDBDBFFD9D9D9FFD6D6
          D6FFD3D3D3FFDEDEDEFFF1F1F1FFF0F0F0FFEEEEEEFFEDEDEDFFECECECFFEBEB
          EBFFECECECFFEAEAEAFFE7E7E7FFE2E2E2FFD9D9D9FFD3D3D3FFAAABA9FF5CBC
          6FFF4DC260FF73BF7CFFB9B7B8FFA9A9A9FF6A6A6AF300000000000000007C7C
          7CDDE0E0E0FFE2E2E2FFDEDEDEFFDBDBDBFFD9D9D9FFD6D6D6FFD4D4D4FFD6D6
          D6FEEBEBEBFEEDEDEDFFF3F3F3FFF4F4F4FFF1F1F1FFF1F1F1FFEFEFEFFFEEEE
          EEFFEDEDEDFFECECECFFECECECFFECECECFFECECECFFECEBECFFA0A4A1FF43D4
          66FF35D653FF1AC030FF85C28AFFB1AFB1FD5B5B5BCF00000000000000007676
          76CECCCCCCFFE0E0E0FFDBDBDBFFD6D6D6FFD2D2D2FED7D7D7FCDBDBDBFDBDBD
          BDFEA9A9A9FFB0B0B0FFBDBDBDFFD0D0D0FFE1E1E1FFEDEDEDFFF4F4F4FFF2F2
          F2FFF2F2F2FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFEDEDEDFFEDECEDFFACAB
          ACFF84AF8CFF90BD96FFDAD9D9FE898889FD2F2F2F6400000000000000002323
          233A7D7D7DDAA1A1A1FFB4B4B4FFD2D2D2FEF7F7F7FEFFFFFFFF797979FF5F60
          5FFF606263FF6E6F70FF818181FF959595FFA7A7A7FFB2B2B2FFC1C1C1FFD6D6
          D6FFE4E4E4FFF0F0F0FFF3F3F3FFF2F2F2FFF0F0F0FFEEEEEEFFEEEEEEFFF0EF
          EFFFF5F2F4FFEEECEEFF969596FF4C4C4CA20101010100000000000000000000
          0000030303042727273F4C4C4C7F6C6C6CBC878787F0A5A5A5FE6B6D6EFFFFE0
          C2FFDDC3ACFFB4A392FF8A817AFF656565FF636565FF727374FF838384FF9999
          99FFAAAAAAFFB6B6B6FFC7C7C7FFD7D7D7FFE5E5E5FFF1F1F1FFF4F4F4FFF4F4
          F4FFC3C3C3FF7C7C7CFA3E3E3E82020202020000000000000000000000000000
          00000000000000000000000000000000000003030304242424447B7978FEFFE8
          CDFFFFE3C6FFFFE4C5FFFFE4C2FFFFE1C0FFE3C6ABFFB9A592FF90867CFF6967
          66FF646667FF737475FF848585FFC5C5C5FFEDEDEDFFE3E3E3FFB7B7B7FF8181
          81FC555555AE1818182C00000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002525254C9C948EFFFFE9
          D1FFFFE4CBFFFFE3C9FFFFE1C6FFFFE0C4FFFFE1C1FFFFE0BFFFFFE1BEFFFFDF
          BBFFE9C8AAFFC0A892FF847C73FF9C9D9EFF878787FD696969D3414141801313
          1324000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000046464690C2B6ACFFFFEC
          D7FFFFE7D1FFFFE6CFFFFFE5CCFFFFE3CAFFFFE2C7FFFFE1C4FFFFDFC1FFFFDF
          BEFFFFDDBDFFFFE1BDFFB7A590FB282929500F0F0F1A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010101016C6C6CDCF2E3D5FFFFED
          DDFFFFEAD8FFFFE9D5FFFFE8D2FFFFE6D0FFFFE5CDFFFFE4CBFFFFE3C8FFFFE1
          C5FFFFE0C2FFFFE2C2FF91857ADB000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000027272745989593FEFFF7E9FFFFEF
          E1FFFFEEDEFFFFECDCFFFFEBD9FFFFEAD6FFFFE8D3FFFFE7D1FFFFE6CEFFFFE4
          CBFFFFE3CAFFFFE8CBFF4F4C4A9B000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000636364BBD7D1CBFFFFF6ECFFFFF2
          E7FFFFF1E5FFFFF0E2FFFFEEDFFFFFEDDCFFFFEBDAFFFFEAD7FFFFE9D4FFFFE7
          D2FFFFE8D0FFE1CCB8FF1F1F204C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000028282843979897FEFFFDF8FFFFF7F0FFFFF6
          EEFFFFF4EBFFFFF3E8FFFFF2E6FFFFF0E3FFFFEFE0FFFFEDDDFFFFECDBFFFFEC
          D8FFFFEFDAFF908881DF02020204000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000808080B767676DAE6E5E4FFFFFFFDFFFFFAF7FFFFF9
          F4FFFFF8F1FFFFF6EFFFFFF5ECFFFFF3E9FFFFF2E7FFFFF1E4FFFFEFE1FFFFF0
          E0FFEBDBCCFF2C2C2C6900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002020202626262A6C0C0C0FFFFFFFFFFFFFFFFFFFFFFFEFFFFFC
          FCFFFFFBF8FFFFFAF5FFFFF8F2FFFFF7F0FFFFF5EDFFFFF4EAFFFFF4E9FFFFF8
          EBFF77736FCB0202020400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000004C4C4C73A5A5A5FEFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFEFFFFFBFAFFFFFAF6FFFFF9F3FFFFF7F2FFFFFDF4FFAFAA
          A4F21111112A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000D0D0D12292929443D3D3D685656568E7B7B7BB3A1A1A1D9BEBE
          BEF8D2D2D2FEE7E7E7FFFDFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFC3C0BDFA2122
          224F000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000404
          0407141414262424244C353535724F4F4F97747474BD8A8989D7212121510000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFF0FFFFFFC01FFFFE0007FFF80000FFE000001F800000038000
          0001800000018000000180000001800000018000000180000001800000018000
          000180000001C0000003F0000007FF00001FFF00007FFE0003FFFE0007FFFE00
          07FFFC000FFFFC000FFFF8000FFFF0001FFFE0003FFFF8007FFFFFF07FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000676767FF676767FF676767FF676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000676767FFCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000676767FFCCCCCCFF343434FF979797FF343434FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000676767FFCCCCCCFFFFFFFFFF343434FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000000000
          00000000000023180F27874016C4994313E8973D0FE8784B35F491989CFFC0C1
          C4FF676767FFCCCCCCFFFFFFFFFF343434FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000000000
          000032231831A84A07ECAB4401FFB45A15FFB7560EFF845430FFB4BFC8FFD6C0
          A6FF676767FFCCCCCCFFFFFFFFFF343434FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000003625
          1739AE5004EBB44F01FFC5782BFFC77A2BFFCE7C27FF986E43FF9DAAB8FFDCAF
          81FF676767FFCCCCCCFFFFFFFFFF343434FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF0000000000000000BB63
          11F1BE5801FFC37329FFC37428FFC27226FFC97324FF996E46FF9DACBAFFD4A7
          80FF676767FFCCCCCCFF343434FF979797FF343434FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF0000000000000000CA67
          07FFC16A1AFFC07127FFC06F24FFC07025FFC67020FF9C7047FFB0C0CDFFCF9E
          7CFF676767FFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCC
          CCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCC
          CCFFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFF676767FF0000000000000000D06C
          08FFC26918FFBE6C24FFBE6C23FFBF6D23FFC36E1DFF9D7147FFC3D4E1FFCB97
          78FF676767FF676767FF676767FF676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF0000000000000000CF6D
          08FFC16815FFBC6923FFBC6A21FFBD6A21FFC06A1CFF9C7049FFD7E7F1FFD1A4
          8CFFD4A186FFD09469FFDA9D68FFCFC8C4FFC8C9CBFFD3D3D3FFDDDDDDFFE8E8
          E8FFFCFCFCFFD1D4DAFFE9C194FFE8C49DFFE8C39AFFE8C298FFE6BC89FFE1BA
          96FFDAB3A2FF000000000000000000000000000000000000000000000000CF6D
          08FFC06614FFBA6621FFBB6720FFBB6720FFBE671AFF9B6F4AFFEBF7FFFFC8B4
          AAFFBDA99DFFB4A194FFAC9685FF9E9F9FFFACAFB1FFBBBDBFFFCBCDCFFFDBDD
          DFFFF3F6F8FF9BA4AFFFCF7A20FFCD812FFFCC802BFFE1AF7AFFD6903EFFC16D
          22FF983406FF000000000000000000000000000000000000000000000000CF6D
          09FFBF6513FFB96420FFB9641EFFB9651EFFBD661CFFA06F47FFAFADACFFAEA9
          A4FFAAA8A4FFA49F9BFF999591FF8E8782FF8A827EFF8A837CFF89827BFF8882
          7BFF8A857EFF6A6866FF8F5518FF8D5921FF8A5820FFBD7728FFD38732FFBF6D
          21FF983406FF000000000000000000000000000000000000000000000000CF6D
          09FFBE6312FFB7611DFFB7621CFFB7621DFFB8631DFFBC6218FFBB5F10FFBC5F
          10FFBE6211FFBF6313FFC26816FFC46917FFC66C19FFC76E1AFFC8701BFFC972
          1CFFCD7520FFCF7924FFD07F2BFFD0812FFFD18230FFCF822FFFD18834FFBE6C
          23FF983406FF000000000000000000000000000000000000000000000000D06E
          09FFBD6111FFB55E1BFFB55F1BFFB6601BFFB6601CFFB7611CFFB8631DFFB964
          1EFFBA651FFFBB671FFFBC6820FFBD6A21FFBE6C23FFBF6E24FFC17025FFC272
          26FFC47428FFC57729FFC7792AFFC87B2CFFCA7D2DFFCB802EFFD08533FFBF6B
          21FF993406FF000000000000000000000000000000000000000000000000D06E
          09FFBB5F10FFB35C1BFFB35C19FFB45A12FFB6580EFFB5580FFFB6590EFFB75A
          0FFFB95D10FFBA5F12FFBB6013FFBC6214FFBF6515FFC06717FFC16A19FFC46C
          1AFFC5701BFFC7721EFFCA741FFFCB7622FFCC7C26FFCA7E2DFFCF8532FFBD6A
          22FF993406FF000000000000000000000000000000000000000000000000D06E
          09FFBA5C0FFFB15918FFB35510FFB07F5AFFAD9584FFAD917DFFAC927EFFAB91
          7EFFAB937DFFAB927DFFAB927DFFAB927CFFAA907CFFAA907BFFA9907AFFA88F
          79FFA78F79FFA58E78FFA68D77FFA5907AFFB08864FFCC7D28FFCE8231FFBC69
          20FF993406FF000000000000000000000000000000000000000000000000D06F
          09FFB95C0EFFAE5616FFB0510BFFAD8F7AFFD1DAE1FFDCE0E3FFDBE0E3FFDDE1
          E5FFDDE1E4FFDDE2E5FFDCE0E3FFDBDFE3FFD8DCE0FFD8DCE0FFD5D9DDFFD3D7
          DBFFD0D3D7FFCED1D5FFCBCFD3FFC1C7CFFFA2917EFFCC7823FFCD812FFFBD68
          1FFF993407FF000000000000000000000000000000000000000000000000D06F
          09FFB8590DFFAE5315FFAE4D07FFAD8C75FFDCE0E4FFEAEAEAFFEBEBEBFFEBEB
          EBFFECECECFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE6E6E6FFE4E4E4FFE1E1
          E1FFDEDEDEFFDCDCDCFFDBDBDBFFCED2D6FFA38F7AFFCD7720FFCC8130FFBB68
          1FFF993507FF000000000000000000000000000000000000000000000000D06F
          09FFB7570CFFAC5113FFAC4907FFAE8D76FFDDE3E6FFEDEDEDFFEDEDEDFFDEDE
          DEFFC7C7C7FFC9C9C9FFC9C9C9FFC9C9C9FFC9C9C9FFC9C9C9FFCACACAFFCACA
          CAFFD0D0D0FFDFDFDFFFDBDBDBFFCED1D4FFA48F7BFFCB761FFFCC802FFFBC67
          1FFF993507FF000000000000000000000000000000000000000000000000D06F
          09FFB4550BFFA94E10FFAB4704FFAF8E77FFE0E6E9FFEFEFEFFFF2F2F2FFE2E2
          E2FFCACACAFFCDCDCDFFCDCDCDFFCDCDCDFFCCCCCCFFCCCCCCFFCDCDCDFFCBCB
          CBFFD3D3D3FFE0E0E0FFDBDBDBFFD0D3D6FFA3907CFFC97520FFCB7F2DFFBA66
          1EFF993507FF000000000000000000000000000000000000000000000000D06F
          09FFB3530AFFA74B10FFA94301FFB28F79FFE4E9ECFFF2F2F2FFF3F3F3FFF8F8
          F8FFFDFDFDFFFFFFFFFFFBFBFBFFFBFBFBFFF7F7F7FFF5F5F5FFF0F0F0FFECEC
          ECFFE6E6E6FFE0E0E0FFDEDEDEFFD2D6DAFFA4907DFFC8731EFFCA7E2EFFBB66
          1EFF993507FF000000000000000000000000000000000000000000000000D06F
          09FFB25209FFA5490DFFA64101FFB3917AFFE7ECEFFFF5F5F5FFF9F9F9FFE7E7
          E7FFCBCBCBFFCECECEFFCECECEFFCECECEFFCDCDCDFFCECECEFFCCCCCCFFCCCC
          CCFFD4D4D4FFE4E4E4FFDEDEDEFFD3D6DAFFA6917DFFC9721CFFCA7C2CFFB965
          1CFF993507FF000000000000000000000000000000000000000000000000D170
          09FFB14F07FFA5450DFFA33E01FFB4917BFFEBF0F4FFF9F9F9FFFCFCFCFFE7E7
          E7FFC3C3C3FFC6C6C6FFC6C6C6FFC6C6C6FFC7C7C7FFC8C8C8FFC8C8C8FFC8C8
          C8FFD2D2D2FFE4E4E4FFDFDFDFFFD4D8DCFFA6917FFFC7711CFFC97B2DFFB963
          1DFF993507FF000000000000000000000000000000000000000000000000D170
          09FFB04D06FFA2420AFFA33C01FFB4927DFFEDF2F6FFFAFAFAFFFCFCFCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFBFBFBFFF5F5F5FFF2F2F2FFEDED
          EDFFE8E8E8FFE4E4E4FFE1E1E1FFD4D7DBFFA8927EFFC8711DFFC97B2BFFBA65
          1DFF993607FF000000000000000000000000000000000000000000000000D170
          09FFAF4B05FF9F400AFFA03701FFB5927CFFEEF3F6FFFDFDFDFFFFFFFFFFF6F6
          F6FFE6E6E6FFE8E8E8FFE6E6E6FFE4E4E4FFE1E1E1FFDFDFDFFFDFDFDFFFDCDC
          DCFFDEDEDEFFE4E4E4FFE2E2E2FFD5D9DDFFA8937FFFC46F1BFFC5792BFFBD66
          1EFF9D3A07FF000000000000000000000000000000000000000000000000D170
          09FFAE4A04FF9F3E07FF9F3501FFB5927FFFF0F5F8FFFDFDFDFFFFFFFFFFE8E8
          E8FFC2C2C2FFC5C5C5FFC6C6C6FFC6C6C6FFC6C6C6FFC7C7C7FFC8C8C8FFC7C7
          C7FFD1D1D1FFE5E5E5FFE2E2E2FFD5D8DCFFAC957FFF8A4B11FF814F1DFFC26A
          1EFFA23F06FF000000000000000000000000000000000000000000000000CE70
          11FAAF4C02FF993707FF9C3201FFB5927FFFEEF3F7FFFCFCFCFFFFFFFFFFF8F8
          F8FFEBEBEBFFEEEEEEFFEDEDEDFFEAEAEAFFE9E9E9FFE5E5E5FFE3E3E3FFE0E0
          E0FFE1E1E1FFE5E5E5FFE3E3E3FFD6D9DDFFAB9581FFA05A15FF9D6125FFBF66
          19FFA6470EF8000000000000000000000000000000000000000000000000834A
          119FCC6201FFB44E01FFAE4201FFBC987DFFEDF4F9FFFDFEFFFFFEFEFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFF8F8F9FFF3F4F4FFF0F1
          F1FFEBECECFFE6E6E7FFE4E5E6FFD7DCDFFFA7917DFFC66309FFC16514FFAE43
          01FF7037109F0000000000000000000000000000000000000000000000000A0A
          080A7F481299C87117F1D57008FFCDA881FFCBCAC9FFCBC5BFFFCAC4BFFFCAC4
          BFFFC7C2BEFFC5C0BAFFC3BEB9FFC0BBB5FFBDB8B2FFBAB6B1FFB9B3ADFFB5B0
          AAFFB2ADA7FFAFAAA6FFAFA9A3FFACABA8FFB5987EFFC76207FFB55D15F1723C
          12990A09080A0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFC00001FFC00001FFC00001FFC00001F0000001E0000001C000
          00018000000180000001800000018000001F8000001F8000001F8000001F8000
          001F8000001F8000001F8000001F8000001F8000001F8000001F8000001F8000
          001F8000001F8000001F8000001F8000001F8000001F8000001FC000003FFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000606
          061E101010551414147F191919A51A1A1ABF1A1A1AD2181818DB161616DE1313
          13D7111111CB0D0D0DB509090993060606650303032B01010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000A0A0A272020208A2D2D2DD93636
          36FE3D3D3DFF434343FF474747FF4B4B4BFF323232FF4C4C4CFF464646FF4040
          40FF383838FF2F2F2FFF262626FF1B1B1BFF0C0C0CFE0A0A0ADF050505820101
          0118000000000000000000000000000000000000000000000000000000000000
          00000000000000000000020202052B2B2B9D404040FB393939FF595959FF6060
          60FF5F5F5FFF5F5F5FFF5C5C5CFF5B5C5CFF313333FF535353FF4C4C4CFF4646
          46FF424242FF3C3C3CFF363636FF303030FF161616FF222222FF131313FF0909
          09F3030303710000000000000000000000000000000000000000000000000000
          000000000000000000000B0B0B23464646FF626262FF4C4C4CFF666666FF6565
          65FF656565FF646464FF656565FF515252FF664B4BFF363838FF545454FF4D4D
          4DFF464646FF404040FF393939FF343434FF1F1F1FFF262626FF1F1F1FFF1616
          16FF070707E40000000000000000000000000000000000000000000000000000
          0000000000000000000008080817484848FE686868FF5C5C5CFF6C6C6CFF6D6D
          6DFF6D6D6DFF6F6F6FFF575858FF454141FFAC7272FF4A3B3BFF404141FF5454
          54FF4A4A4AFF444444FF3D3D3DFF353535FF2A2A2AFF282828FF222222FF1919
          19FF0A0A0ACA0000000000000000000000000000000000000000000000000000
          0000000000000000000001010103494949F16D6D6DFF6F6F6FFF747474FF7575
          75FF787878FF595959FF636464FF695858FFAA7373FF7F5B5BFF3D3F3FFF4040
          40FF515151FF474747FF404040FF393939FF323232FF2B2B2BFF232323FF1919
          19FF0A0A0A9E0000000000000000000000000000000000000000000000000000
          00000000000000000000000000003A3A3ABC686868FF777777FF7A7A7AFF7F7F
          7FFF6B6B6BFF595959FF707272FF826666FFAD7878FFA17373FF575A5AFF4545
          45FF3B3B3BFF4D4D4DFF444444FF3C3C3CFF343434FF2D2D2DFF272727FF1919
          19FF0707075E0000000000000000000000000000000000000000000000000000
          00000000000000000000000000002323236B5D5D5DFF7F7F7FFF818181FF8686
          86FF4A4A4AFF8E8E8EFF5F6161FFAA9595FFB37E7EFFA47B7BFF7E8181FF6161
          61FF424242FF363636FF484848FF3F3F3FFF363636FF2F2F2FFF282828FF1717
          17F5020202110000000000000000000000000000000000000000000000000000
          00000000000000000000000000000505050F4B4B4BEF818181FF878787FF8E8E
          8EFF707070FF8F8F8FFF585959FFCDC6C6FFB78181FFA37F7FFFB1B3B3FF5F5F
          5FFF585858FF3C3C3CFF363636FF404040FF383838FF313131FF242424FF1111
          1198000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000027272774606060FF8F8F8FFF9797
          97FF727272FF909090FF575757FFD9DBDBFFC08D8DFF9D7D7DFFECEFEFFF5656
          56FF575757FF4D4D4DFF363636FF424242FF393939FF303030FF1F1F1FF00404
          041A000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000020202033C3C3CBE727272FF7A7A
          7AFF606060FF6F6F6FFF5A5A5AFFDFE2E2FFD1A9A9FF9D8181FFFCFEFEFF7D7D
          7DFF585858FF343434FF3C3C3CFF414141FF383838FF292929FD0E0E0E590000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000006060612414141CB5656
          56FF919191FF929292FF5E5E5EFFB0B2B2FFDCBBBBFF9D7878FFDBDDDDFF9797
          97FF5B5B5BFF434343FF333333FF3A3A3AFF2E2E2EFD15151572000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000404040C3737
          37A2646464FE838383FF414141FF3D3D3DFF121111FF0C0E0EFF080909FF3131
          31FF575755FF4B4C4CFF303232FF2A2E2EEA1212125300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001616143F2B2B29DE232222FF2B2926FF24201AFF1F1B14FF1C1915FF100F
          0DFF2A2E38FF292820DC6E5654EE442E2E710000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020204031D1D16E52D2C29FF334E75FF4D8AE3FF4B85DEFF365A92FF1310
          09FF243668FF2B4D9696633E3890966867D20202020200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000264C85852C313EFF2E2B26FF3E6699FF487CC2FF4779BFFF2D4363FF150F
          08FF2D456CFF4E85FFFF2D448992B07970F1462E2E6A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000D19
          2A294484FAFA4570A1FF2B231AFF302922FF2C241BFF251D13FF1C140BFF232A
          37FF5290EFFF5497FFFF4279F7F756424F9BAC7976E805040408000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002C57
          A0A0529AFFFF5EB2FFFF508ECDFF4673A0FF4471A0FF487EB8FF559BEDFF5FAD
          FFFF5AA3FFFF579BFFFF4F8AFFFF3051ABABB87D75FC2719193A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020408073F80
          F1F162B8FFFF64BDFFFF64BEFFFF5CB2FFFF62B7FFFF56A3F1FF3B6EA7FF427B
          BCFF4781CDFF4A87DBFF579BFFFF3C73EFEFB27972F9281B1B3C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000F203534438A
          F6FF26445BFF111F2BFF010101FF0B151DFF67C6FFFF366995FF1A1209FF0101
          01FF010101FF010101FF589EFFFF4783FFFF9B7D97F70A07060E000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000018355A593D81
          DEFF282118FF121110FF010101FF010101FF63BFFFFF2E5372FF403D3AFF0D0D
          0DFF010101FF010101FF4F8EE7FF4480EDFF312D44B100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001C3F68673B81
          D9FF2E241AFF1F1915FF010101FF010101FF1A3142FF131E24FF252626FF1210
          0FFF010101FF010101FF2E5385FF1D3556FF010101B500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000112849611C3B
          60FF4A91C1FF55AADDFF60BFF4FF6BD0FFFF68CBFFFF65C7FFFF63C0FFFF62BB
          FFFF65BDFFFF5195E8FF010101FF020202FF010101BE00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000C1C333D3A83
          D8FF4B92C1FF3B7597FF68CFFFFF3E7B9DFF60BCF9FF67C7FFFF64BEFFFF63BA
          FFFF2E547DFF090E17FF010101FF010101FF010101A900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020306190101
          01FE16100DFF140F0BFF010306FF070101FF010406FF010609FF03070CFF55A0
          ECFF122334FF0A0806FF050505FF010101FF0101017200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          01B7373737FF666665FF4D4C4CFF656565FF555554FF4D4C4BFF373534FF0101
          02FF04080DFF1C1B1BFF0C0C0CFF010101F90101011E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          0122010101F05D5D5DFF7B7B7BFF767676FF6E6E6EFF626262FF565656FF3A3A
          39FF141414FF272727FF080808FF0101018D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001010142010101F1595959FF919191FF818181FF707070FF5E5E5EFF4E4E
          4EFF3C3C3CFF101010FF010101BD010101080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010129010101BD0F0F0FFE3A3A3AFF434343FF373737FF1D1D
          1DFF020202F70101018B01010107000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000101012A0101016901010187010101830101
          015C010101150000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFC003FFFE00007FF800001FF800001FF800001FF800001FF800
          001FF800003FFC00003FFC00007FFE00007FFF0000FFFF8001FFFFE003FFFFE0
          03FFFFC001FFFFC001FFFF8001FFFF8001FFFF8001FFFF0001FFFF0001FFFF00
          01FFFF8001FFFF8001FFFF8003FFFFC003FFFFE007FFFFF00FFFFFFC3FFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          00070000000D000000140000001A0000001E0000002000000022000000220000
          00220000001F0000001C00000017000000100000000800000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000900000015000000230000
          00350000004A0000005B0000006B000000760000007D00000082000000830000
          00800000007B0000007200000063000000500000003A00000024000000140000
          0007000000010000000000000000000000000000000000000000000000000000
          00000000000000000000000000060000001A0000003A0000005F030303800909
          09960E0E0EA7121212B9151515CA161616D7151515DF131313E10F0F0FDB0E0E
          0ED20A0A0AC4060606B2030303A20101019A00000094000000800000005C0000
          0033000000140000000400000000000000000000000000000000000000000000
          000000000000000000020000001A0606066A171717A8282828DD353535FE3C3C
          3CFF424242FF464646FF4A4A4AFF313131FF4B4B4BFF454545FF3F3F3FFF3737
          37FF2E2E2EFF252525FF1A1A1AFF0B0B0BFE080808E2020202AD0000009E0000
          00860000004D0000001200000000000000000000000000000000000000000000
          00000000000001010109272727A43E3E3EFB383838FF585858FF5F5F5FFF5E5E
          5EFF5E5E5EFF5B5B5BFF5A5B5BFF303232FF525252FF4B4B4BFF454545FF4141
          41FF3B3B3BFF353535FF2F2F2FFF151515FF212121FF121212FF080808F30101
          01A6000000780000001F00000000000000000000000000000000000000000000
          0000000000000A0A0A26454545FF616161FF4B4B4BFF656565FF646464FF6464
          64FF636363FF646464FF505151FF654A4AFF353737FF535353FF4C4C4CFF4545
          45FF3F3F3FFF383838FF333333FF1E1E1EFF252525FF1E1E1EFF151515FF0505
          05E6000000790000001F00000000000000000000000000000000000000000000
          00000000000007070718484848FE676767FF5B5B5BFF6B6B6BFF6C6C6CFF6C6C
          6CFF6E6E6EFF565757FF444040FFAC7171FF493A3AFF3F4040FF535353FF4949
          49FF434343FF3C3C3CFF343434FF292929FF272727FF212121FF181818FF0707
          07D1000000670000001800000000000000000000000000000000000000000000
          00000000000000000004474747F16C6C6CFF6E6E6EFF737373FF747474FF7777
          77FF585858FF626363FF685757FFAA7272FF7E5A5AFF3C3E3EFF3F3F3FFF5050
          50FF464646FF3F3F3FFF383838FF313131FF2A2A2AFF222222FF181818FF0707
          07B50000004D0000000F00000000000000000000000000000000000000000000
          00000000000000000000383838BD676767FF767676FF797979FF7E7E7EFF6A6A
          6AFF585858FF6F7171FF826565FFAD7777FFA17272FF565959FF444444FF3A3A
          3AFF4C4C4CFF434343FF3B3B3BFF333333FF2C2C2CFF262626FF181818FF0404
          04950000002F0000000500000000000000000000000000000000000000000000
          000000000000000000002222226D5C5C5CFF7E7E7EFF818181FF868686FF4949
          49FF8E8E8EFF5E6060FFAA9595FFB37D7DFFA47A7AFF7D8080FF606060FF4141
          41FF353535FF474747FF3E3E3EFF353535FF2E2E2EFF272727FF151515F50101
          0167000000180000000100000000000000000000000000000000000000000000
          00000000000000000000040404104A4A4AEF808080FF878787FF8E8E8EFF6F6F
          6FFF8F8F8FFF575858FFCDC6C6FFB78080FFA37E7EFFB1B3B3FF5E5E5EFF5757
          57FF3B3B3BFF353535FF3F3F3FFF373737FF303030FF232323FF0D0D0DAE0000
          0035000000080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000262626755F5F5FFF8F8F8FFF979797FF7171
          71FF909090FF565656FFD9DBDBFFC08D8DFF9D7C7CFFECEFEFFF555555FF5656
          56FF4C4C4CFF353535FF414141FF383838FF2F2F2FFF1D1D1DF1020202590000
          0013000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010101033A3A3ABF717171FF797979FF5F5F
          5FFF6E6E6EFF595959FFDFE2E2FFD1A9A9FF9D8181FFFCFEFEFF7C7C7CFF5757
          57FF333333FF3B3B3BFF404040FF373737FF282828FD0A0A0A7D0000001B0000
          0003000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000006060613404040CB555555FF9191
          91FF929292FF5D5D5DFFB0B2B2FFDCBBBBFF9D7777FFDBDDDDFF979797FF5A5A
          5AFF424242FF323232FF393939FF2E2E2EFD1111118A0000001B000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000303030D363636A36464
          64FE838383FF404040FF3C3C3CFF111010FF0B0D0DFF070808FF303030FF5656
          54FF4A4B4BFF2F3131FF282A2AEB0C0C0C820000002200000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001515
          1344282827DF222121FF2A2825FF231F19FF1E1A13FF1B1814FF0F0E0CFF292D
          37FF24231CDF695250EF312121990000006F0000002800000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010101
          031C1B1B14E62C2B28FF324D74FF4C8AE3FF4A85DEFF355992FF120F08FF2335
          67FF1E366AB5472D28AF875C5CD6010101840000004F00000012000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000092143
          76922B303DFF2D2A25FF3D6599FF477BC2FF4678BFFF2C4262FF140E07FF2C44
          6BFF4D85FFFF1F2F5FB3A9736CF12F1F1E9A0000007500000027000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000010C16263B4382
          F8FA446FA1FF2A2219FF2F2821FF2B231AFF241C12FF1B130AFF222936FF5190
          EFFF5397FFFF3F75F1F73F303AB6A3716FE90202028A00000039000000070000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000003285297A6519A
          FFFF5DB2FFFF4F8ECDFF4572A0FF4370A0FF477DB8FF549BEDFF5EADFFFF59A3
          FFFF569BFFFF4E8AFFFF243D82BFB77C74FC150E0E910000003A000000080000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000103070F3D7EEEF161B8
          FFFF63BDFFFF63BEFFFF5BB2FFFF61B7FFFF55A3F1FF3A6DA7FF417ABCFF4680
          CDFF4987DBFF569BFFFF386DE4F0AF766FF9170F0F8D0000002C000000040000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000E1E323C428AF6FF2543
          5AFF101E2AFF000000FF0A141CFF66C6FFFF356895FF191108FF000000FF0000
          00FF000000FF579EFFFF4683FFFF987993F7050303790000001F000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000163256603C81DEFF2720
          17FF11100FFF000000FF000000FF62BFFFFF2D5271FF3F3C39FF0C0C0CFF0000
          00FF000000FF4E8EE7FF437FEDFF262335C1000000730000001C000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001A3C636C3A81D9FF2D23
          19FF1E1814FF000000FF000000FF193041FF121D23FF242525FF110F0EFF0000
          00FF000000FF2D5285FF1C3455FF000000C4000000740000001C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F2747651B3A5FFF4991
          C1FF54AADDFF5FBFF4FF6AD0FFFF67CBFFFF64C7FFFF62C0FFFF61BBFFFF64BD
          FFFF5095E8FF000000FF010101FF000000C90000006A00000019000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B1B31403983D8FF4A92
          C1FF3A7497FF67CFFFFF3D7A9DFF5FBCF9FF66C7FFFF63BEFFFF62BAFFFF2D53
          7CFF080D16FF000000FF000000FF000000BB0000005400000011000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000102051A000000FE150F
          0CFF130E0AFF000205FF060000FF000305FF000508FF02060BFF54A0ECFF1122
          33FF090705FF040404FF000000FF0000009D0000003500000007000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000B83636
          36FF656564FF4C4B4BFF646464FF545453FF4C4B4AFF363433FF000001FF0307
          0CFF1B1A1AFF0B0B0BFF000000F9000000680000001800000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000230000
          00F05C5C5CFF7A7A7AFF757575FF6D6D6DFF616161FF555555FF393938FF1313
          13FF262626FF070707FF000000A3000000280000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0043000000F1585858FF919191FF808080FF6F6F6FFF5D5D5DFF4D4D4DFF3B3B
          3BFF0F0F0FFF000000C30000002E000000080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000002A000000BE0E0E0EFE393939FF424242FF363636FF1C1C1CFF0101
          01F7000000950000001C00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000002C0000006C0000008B00000088000000640000
          0021000000060000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000505052E0F0F0F9A0C0C0CEA040404C20101010A0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000202020A262626EA272727FF1F1F1FFF929292FF1A1A1A7A0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000020202220202021B0000
          0000000000000A0A0A2B3D3D3DFF363636FF6A6A6AFF6F6F6FFF4D4D4DC90000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000009090944171717CA101010FD1B1B1BF20202
          022A010101012323234A5A5A5AFF575757FF868686FF929292FF8D8D8DE50000
          00000000000000000000000000000000000000000000020202150202021D0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000002C2C2CDE2F2F2FFF313131FFA4A4A4FF5858
          58D47B7B7BDF8C8C8CFE8A8A8AFF828282FF797979FF808080FF8C8C8CFC0C0C
          0C1400000000000000000000000005050529151515D8131313FD070707F70202
          0239000000000000000000000000000000000000000000000000000000000000
          0000000000000000000001010101434343F2565656FF7B7B7BFF919191FF9E9E
          9EFFB3B3B3FFB8B8B8FFC1C1C1FFC1C1C1FFADADADFF8D8D8DFF7A7A7AFF6B6B
          6BEE4A4A4A991F1F1F3B0101010119191986323232FF1A1A1AFF9E9E9EFF4343
          43B7000000000000000000000000000000000000000000000000000000000000
          000000000000000000000F0F0F1E7D7D7DFD999999FFB1B1B1FFCACACAFFD9D9
          D9FFBFBFBFFFACACACFFA0A0A0FFABABABFFCBCBCBFFE7E7E7FFD4D4D4FFABAB
          ABFF838383FF737373FE646464D84C4C4CD04C4C4CFF535353FF8F8F8FFF5D5D
          5DF4010101010000000000000000000000000000000000000000000000000000
          0000000000000000000034343452A6A6A6FFCCCCCCFFC8C8C8FF9C9C9CFF7777
          77FE565656B6373737702C2C2C563838386C555555F6797979FF969696FFC1C1
          C1FFE1E1E1FFD3D3D3FF9F9F9FFF787878FF6D6D6DFF6F6F6FFF8B8B8BFFA0A0
          A0FE0A0A0A0F0000000000000000000000000000000000000000000000000000
          000000000000000000003A3A3A5BBDBDBDFFCECECEFF8A8A8AFF787878FF7B7B
          7BFE313232650B0C0C14000000000505051C2D2E2EFE191919FF858585FF6262
          62EA6C6C6CDF8F8F8FFEBFBFBFFFDEDEDEFFBEBEBEFF888888FF6C6C6CFF6E6E
          6EFF202020370000000000000000000000000000000000000000000000000000
          000000000000000000000A0A0A0D858585DCC2C3C3FFD9DBDBFFB9BCBCFFA0A2
          A2FF908585FF8D7777FB7F6666C95B484898555555FF4D5151FF959797FF5A5D
          5DDD02020202202020394D4D4D94747474EA9C9C9CFFCFCFCFFFBFBFBFFF7A7A
          7AFF3F3F3F940000000000000000000000000000000000000000000000000000
          00000000000000000000000000000B0C0C125F5A5A9EA18585FEBA8F8FFFB17D
          7DFFBE8A8AFFCF9D9DFFBB8989FFAD7979FFAD7777FFA47676FF9E7E7EFF9989
          89F8171010200000000000000000030303052B2C2C566B6B6BE4D0D0D0FFAFAF
          AFFF5A5A5AD40000000000000000000000000000000000000000000000000000
          0000000000000000000000000000261B1B2FAE7B7BECD29F9FFCE2B3B3FCEDC0
          C0FBEDC3C1FCF0C5C3FED9ABA9FFC49090FFBE8C8CFFB58282FFAC7878FFA570
          71FFA16D6DFD8F6161D7614141933221214F0705050B5E6161C6D1D1D1FFABAB
          ABFF545454C00000000000000000000000000000000000000000000000000000
          00000000000000000000030202039D7373D4DAAFAFF2E9C0BFEEEABFBEF2EEC1
          BEF7ECC1C2FBD7B6CCFBBEA5C6FEA07D9CFFA98AB2FFCC9A9AFFCB9798FFC08E
          8EFFAD827DFEAC7979FDA37070FD9D6A6AFE915F5FEF9B8E8EFDD2D4D4FF7C7C
          7CFF2F2F2F680000000000000000000000000000000000000000000000000000
          000000000000000000003024243BC59696FAE9C2C2EDE5BFBEEDD9BAC8F1BBB1
          DDFAB0ADE6FEBDBAE8FFC5B9D6FFB69092FFAD95B9FFB390ACFEB69B8BFEA898
          83FFAB9884FFB19386FDCC9899FDBF8D8DFEB07F7FFF9F6C6CFF979999FF5757
          57C7030303050000000000000000000000000000000000000000000000000000
          00000000000000000000503C3C62CFA2A2F7E7C4C4ECE8C2C0ECC2B8DBF8C5C5
          E8FFD9D5E7FFDCD9EAFFD4CAD7FFB28A8FFFB09DBAFFA78DB1FFAEA78DFFB1AC
          95FFB6AD98FFAA9E87FFB69C8CFFDCA9AAFFD7A6A6FFA97474FF837777FE1C1C
          1C3B000000000000000000000000000000000000000000000000000000000000
          000000000000000000006A4D4D80D4AAAAF4E7C4C4ECE8C3C1ECB8BAE4FCCED1
          EAFFD5D6EAFFD7D7EBFFD1C9DBFFAC848BFFAD99BEFFA995AEFFB2AF95FFB1AC
          95FFB5AC96FFB0AA93FFA69A82FFE1AEB0FFE0B0B0FFB88585FF7B6666EC0202
          0202000000000000000000000000000000000000000000000000000000000000
          000000000000000000008260609ED9B4B4F2E7C6C5ECE7C5C7EDB3C0EDFECFD5
          EDFFD1D6EDFFD5D8EEFFD0CCDDFFA87F87FFAB97BFFFAA9CACFFB1B096FFB1AE
          96FFB7AE99FFAEAB90FFA79D85FFD9ADAAFFE1B2B2FFC29292FF755858C30000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009D7171BCDEBFBFEFE6CAC9EBDCC9D2EFB8C9F1FFCDD7
          F0FFCED8F1FFD2DCF3FFCDD1E3FFA07885FFA996C2FFACA2A7FFB1B298FFB1B0
          97FFB8B29CFFADAA8FFFA6A186FFD2ABA5FFE2B3B4FFCD9D9DFF775353BA0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000B28383DAE4CBCBEDE7CDCCEBD2CDDCF3BED3F7FFCBDD
          F8FFCCE0FBFFD0E2FCFFBEB4C2FF9C6D73FFA793C6FFADABA5FFB2B499FFB1B2
          98FFB6B29CFFAFAB90FFA7A388FFCAAAA1FFE3B4B6FFD9ABABFF895D5DD00000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000003020202BE9090F4E8D5D5EBE9D1D0EBCCD4EAF7BAC4DEFFBBAE
          B9FFC6ABADFFC1A2A0FFAFA7B6FFB8ABBDFFBC9291FFA88B82FFB1B197FFB1B5
          99FFB2B69BFFB5AF95FFA7A587FFC2A99BFFE5B6B7FFE3B6B6FF9B6868EC0101
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000015101018CC9C9CFCECE4E4F2E7D4D0F1A7B8D6FC93BCEBFF81C2
          FFFF63B3FFFF54ADFFFF6CBBFFFF4481FFFF2263FFFF9BE9ADFFBEB5ABFFBF9E
          9AFFADA08DFFB7B096FFA3A785FFB9A896FFE6B8BAFFE8BDBDFFA67272FD0C09
          0911000000000000000000000000000000000000000000000000000000000000
          0000000000000E0A0A0E997272B4C69595F9E2BDB9FB1E94FEFE389CFEFE6EBC
          FEFE7DC0FFFF7CBAFFFF7FB8FFFF4A82FFFF0C51FFFF4CEF75FF43E67BFF69EE
          96FFA3E1B5FFD0C0BCFFB9A596FFB0A08CFFE8B9BBFFE9BDBDFFB07F7FFF2217
          1731000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001812121A664949778D829CD76A8ED2FE3081
          F4FF3685FEFE4280F8F81B76D2FA2588D4FF34A5BEFF52ED7FFF4FE781FF4BE5
          7FFF44E57CFF6ADA80FF46C057FF9EC5A2FFF1D6D9FFE8C4C4FFBC8C8CFF5F4F
          4F9E010101010000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010101013525243A865E
          599BC18981F0E7B7B3FE61CB73FD4EEE7AFF59F486FF54ED86FF52EA84FF51E9
          83FF4AE780FF7AE391FF019E06FF049323FFE1C8CAFFCFB0B1FFB07D7DFF937B
          7BFE5555559F0202020300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A08080B4E35375E48A15DDB4EE981FF5AF28CFF57F08AFF53EE87FF55ED
          89FF84F4A5FFC3FFCDFF58C460FF149C30FF5A4D3FA3806669F0C0B4B4FFBDBC
          BCFF8E8E8EFF4D4D4D8F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001D7D3E914EE881FF62F993FF75F99DFF94FFB2FF9BF3
          ACFF60D47CFF22AE44FD0E7D28C809461471010502071A181A346C6D6DF3DADA
          DAFFBCBCBCFF818181FD0D0D0D16000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001C6C377E3DD36FFF4AD777FE4BD675FE2AC156FF32AF
          55FF529363FF46554BB5463F4595544D52B3625B61D3716F70F1818181FECACA
          CAFFC2C2C2FF878787FE1212121E000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000041209140F3E1E490F42204F1248215A8C988DEEF1E6
          EEFFB0ACAFFF8C898BFF999999FFACACACFFBEBEBEFFCCCCCCFFD3D3D3FFD1D1
          D1FFADADADFF6C6C6CC600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003534354A9F9F
          9FF9DFDFDFFFE4E4E4FFE2E2E2FFDADADAFFCBCBCBFFBEBEBEFFAFAFAFFF9C9C
          9CFF6F6F6FCF1212121E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002929
          293F767676C78E8E8EEB7B7B7BD0676767B75959599F49494984393939672323
          233F020202030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFF8FFFFFFF07FFFFFF07FFFFC707FFFF8007C7FF800183FF800
          003FF000003FF01C003FF800301FFC003C1FFC00061FF800001FF800003FF000
          007FF000007FF000007FF000007FF000007FF000007FF000007FF000007FFC00
          003FFF80001FFFE0000FFFF0030FFFF0000FFFFE000FFFFF801FFFFFC07FFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000103
          2B36010899E401059DEF01059AEF010399EF010298EF010397EF010195EF0102
          94EF010193EF010192EF010192EF01018CE20102243200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000001052F360108
          A7EF1038E9FF174CFFFF174BFFFF1549FFFF1448FFFF1349FFFF1447FFFF1246
          FFFF1145FFFF1044FFFF0F45FFFF092EE4FF010195EC01022732000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001043037010AABEE113A
          EBFF1B50FFFF174AFFFF1848FFFF1649FFFF1547FFFF1446FFFF1345FFFF1244
          FFFF1145FFFF1043FFFF0F43FFFF0F45FFFF0A2EE3FF010195EC010228320000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001043237010CB0EE143CEBFF1D52
          FFFF1A4BFFFF194BFFFF184AFFFF1849FFFF1749FFFF1648FFFF1547FFFF1446
          FFFF1345FFFF1244FFFF1143FFFF1043FFFF1047FFFF092EE3FF010296EC0102
          2832000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001063237010DB3EE153EECFF2054FFFF1C4F
          FFFF1C4DFFFF1B4CFFFF1A4CFFFF194BFFFF194AFFFF1849FFFF1748FFFF1648
          FFFF1546FFFF1445FFFF1244FFFF1143FFFF1042FFFF1046FFFF092EE4FF0102
          98EC010228320000000000000000000000000000000000000000000000000000
          0000000000000000000001053337010FB8EE1740EDFF2255FFFF1E50FFFF1E4F
          FFFF1D4FFFFF1D4EFFFF1C4DFFFF1B4DFFFF1A4CFFFF194BFFFF184AFFFF1749
          FFFF1648FFFF1547FFFF1446FFFF1345FFFF1143FFFF1042FFFF1046FFFF092F
          E4FF010399EC0102283200000000000000000000000000000000000000000000
          000000000000010734360111BCEE1842EFFF2457FFFF2152FFFF2052FFFF2051
          FFFF1F50FFFF1E50FFFF1E4FFFFF1D4EFFFF1C4DFFFF1B4CFFFF1A4BFFFF194A
          FFFF1849FFFF1648FFFF1547FFFF1446FFFF1345FFFF1144FFFF1042FFFF1046
          FFFF092FE5FF01039AEC01022932000000000000000000000000000000000000
          0000010632360111BEEF1B44EFFF2659FFFF2354FFFF2354FFFF2253FFFF2153
          FFFF2152FFFF2051FFFF1F51FFFF1E50FFFF1D4FFFFF1C4EFFFF1B4DFFFF1A4C
          FFFF194BFFFF184AFFFF1748FFFF1547FFFF1446FFFF1345FFFF1143FFFF1042
          FFFF1046FFFF092FE5FF01049BEC010227320000000000000000000000000000
          00000114B9E81B46F1FF2A5CFFFF2556FFFF2556FFFF2455FFFF2455FFFF2354
          FFFF2354FFFF2253FFFF2152FFFF2051FFFF1F50FFFF1E4FFFFF1D4EFFFF1C4D
          FFFF1B4CFFFF194BFFFF184AFFFF1749FFFF1547FFFF1446FFFF1345FFFF1143
          FFFF1042FFFF0F45FFFF082FE6FF010698E60000000000000000000000000000
          00000115C5F52B5DFFFF2759FFFF2758FFFF2456FFFF1A4EFFFF164BFFFF194D
          FFFF2152FFFF2153FFFF1A4DFFFF164AFFFF1C4FFFFF2051FFFF1A4DFFFF0E45
          FFFF0B41FFFF1245FFFF1649FFFF0F43FFFF0B3EFFFF0F42FFFF1446FFFF1244
          FFFF1143FFFF0F44FFFF0F44FFFF0105A0F50000000000000000000000000000
          00000115C6F52D5DFFFF295AFFFF2658FFFF2053FFFF96ADFFFFBFCDFFFF99B0
          FFFF2253FFFF164AFFFF819CFFFFA8BBFFFF3D69FFFF174AFFFF2455FFFFA4B7
          FFFFB9C8FFFF5E81FFFF0137FFFF6F8EFFFFA2B7FFFF3E67FFFF1043FFFF1445
          FFFF1244FFFF1043FFFF1043FFFF0106A2F50000000000000000000000000000
          00000116C8F52D60FFFF2B5DFFFF1C4FFFFFBDCBFFFFFFFFFFFFC9D6FFFFFFFF
          FFFFC6D2FFFF033DFFFFDBE2FFFFFFFFFFFF5D81FFFF053FFFFFEEF2FFFFFFFF
          FFFFECF1FFFFFFFFFFFF305EFFFFBDCCFFFFFFFFFFFF6786FFFF0D41FFFF1546
          FFFF1345FFFF1145FFFF1144FFFF0108A7F50000000000000000000000000000
          00000117CDF53061FFFF2D5DFFFF1C50FFFFF1F3FFFFFFFFFFFF1F52FFFFFFFF
          FFFFDFE6FFFF0138FFFFD2DCFFFFFFFFFFFF587EFFFF0138FFFFFFFFFFFFFFFF
          FFFF87A2FFFFFFFFFFFF6484FFFFB2C1FFFFFFFFFFFF587DFFFF0239FFFF1346
          FFFF1446FFFF1244FFFF1245FFFF0109A8F50000000000000000000000000000
          00000119CEF53263FFFF2F5FFFFF285AFFFF1F53FFFF4B75FFFFE9EEFFFFFFFF
          FFFFB8C9FFFF053FFFFFD1DBFFFFFFFFFFFF597FFFFF0138FFFFFFFFFFFFFFFF
          FFFF8DA6FFFFFFFFFFFF6788FFFFAFC2FFFFFFFFFFFFB4C4FFFF7D98FFFF1143
          FFFF1244FFFF1345FFFF1446FFFF010AA9F50000000000000000000000000000
          00000119D0F53465FFFF3161FFFF2659FFFF809EFFFFFFFFFFFFFFFFFFFFD3DC
          FFFF1E52FFFF1247FFFFD1DDFFFFFFFFFFFF5A80FFFF013BFFFFFFFFFFFFFFFF
          FFFF8EA6FFFFFFFFFFFF6889FFFFAFC1FFFFFFFFFFFFEEF0FFFFFFFFFFFF96AC
          FFFF093EFFFF1446FFFF1347FFFF010BAEF50000000000000000000000000000
          0000011AD4F53668FFFF3364FFFF1D53FFFFF9F9FFFFFFFFFFFF8BA5FFFF6E90
          FFFF537CFFFF0640FFFFCCD8FFFFFFFFFFFF5077FFFF0135FFFFFFFFFFFFFFFF
          FFFF8BA5FFFFFFFFFFFF6888FFFFB2C1FFFFFFFFFFFF6283FFFFFFFFFFFFA2B6
          FFFF063DFFFF1547FFFF1448FFFF010CAFF50000000000000000000000000000
          0000011BD5F53768FFFF3564FFFF2055FFFFF5F7FFFFFFFFFFFF8BA6FFFFFFFF
          FFFFA1B7FFFFAABEFFFFFCFEFFFFFFFFFFFFDFE6FFFF6083FFFFFFFFFFFFFFFF
          FFFFBECCFFFFFFFFFFFF5279FFFFB7C7FFFFFFFFFFFFB7C7FFFFFFFFFFFF9DB2
          FFFF0A3EFFFF1649FFFF1549FFFF010CB2F50000000000000000000000000000
          0000011CD7F5396AFFFF3866FFFF2D60FFFF6B8DFFFFFDFEFFFFFFFFFFFFE5EB
          FFFF3565FFFFDDE4FFFFF4F6FFFFF0F3FFFFFDFEFFFF85A1FFFF577DFFFFFFFE
          FFFFFFFFFFFFBBCAFFFF033CFFFFACBFFFFFF6F8FFFFF3F7FFFFD8E0FFFF2B59
          FFFF1346FFFF1848FFFF1749FFFF010DB6F50000000000000000000000000000
          0000011EDDF53E6EFFFF3969FFFF3867FFFF2E61FFFF2157FFFF1F54FFFF285B
          FFFF3464FFFF3060FFFF295BFFFF2658FFFF2356FFFF2557FFFF1F52FFFF1248
          FFFF0E46FFFF154AFFFF1D50FFFF1448FFFF093FFFFF053DFFFF0A3FFFFF1548
          FFFF194BFFFF174AFFFF174BFFFF010EB8F50000000000000000000000000000
          00000121D6EA2D59F9FF4070FFFF3B69FFFF3968FFFF3867FFFF3A69FFFF3F6B
          FFFF416EFFFF4672FFFF4872FFFF426FFFFF3C68FFFF3463FFFF2D5DFFFF295A
          FFFF2858FFFF2657FFFF2455FFFF2354FFFF2152FFFF1F50FFFF1D4FFFFF1C4D
          FFFF1B4BFFFF1B4FFFFF103BEFFF0113B3E80000000000000000000000000000
          0000010A3C3B0122E0F12C5AFAFF4071FFFF3C6BFFFF3A69FFFF3C6AFFFF406D
          FFFF4470FFFF4975FFFF4E77FFFF4771FFFF3D6AFFFF3664FFFF2F5FFFFF2A5B
          FFFF2959FFFF2758FFFF2556FFFF2354FFFF2153FFFF2051FFFF1E4FFFFF1C4F
          FFFF1D51FFFF123DF0FF0114BFEF010631360000000000000000000000000000
          000000000000010B3C3B0123E2F12D5BF9FF4171FFFF3B6AFFFF3D6BFFFF406F
          FFFF4570FFFF4874FFFF4A74FFFF4470FFFF3E6AFFFF3665FFFF2F5FFFFF2B5C
          FFFF295AFFFF2758FFFF2657FFFF2455FFFF2253FFFF2052FFFF1E51FFFF2052
          FFFF143FF1FF0115C2EE01063636000000000000000000000000000000000000
          00000000000000000000010B3C3B0123E4F12D5DF9FF4172FFFF3C6BFFFF3F6D
          FFFF436FFFFF4370FFFF436FFFFF3F6CFFFF3A68FFFF3463FFFF2E5FFFFF2C5C
          FFFF2A5BFFFF2859FFFF2657FFFF2455FFFF2354FFFF2252FFFF2254FFFF1541
          F3FF0116C6EE0106373600000000000000000000000000000000000000000000
          0000000000000000000000000000010B3C3B0124E6F12D5CFAFF4170FFFF3D6C
          FFFF3E6CFFFF3F6EFFFF3E6DFFFF3B69FFFF3766FFFF3261FFFF2E5EFFFF2C5D
          FFFF2A5BFFFF2959FFFF2758FFFF2556FFFF2354FFFF2456FFFF1743F4FF011A
          CAEE010836370000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010D3C3B0123E6F12D5CFAFF4171
          FFFF3B6AFFFF3B69FFFF3968FFFF3766FFFF3363FFFF3161FFFF2F5FFFFF2D5D
          FFFF2B5BFFFF295AFFFF2758FFFF2556FFFF2658FFFF1845F4FF011ACEEE0108
          3837000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010D3C3B0125E7F12C5A
          FBFF3F70FFFF3869FFFF3766FFFF3564FFFF3364FFFF3161FFFF2F5FFFFF2D5D
          FFFF2B5DFFFF295AFFFF2859FFFF2A5BFFFF1B47F7FF011CD0EE010937360000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010D3C3B0125
          E9F12D5BFBFF3E6DFFFF3A69FFFF3769FFFF3666FFFF3463FFFF3263FFFF3060
          FFFF2D5EFFFF2D5EFFFF2B5DFFFF1B49F6FF011ED6EF01093736000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000010C
          3C3B0125DDEA0124E8F50122E7F50122E4F50122E3F50122E1F50122E0F50121
          DFF50121DEF50120DBF50120DBF50120CEE8010A373600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFC003FFFF8001FFFF0000FFFE00007FFC00003FF800
          001FF000000FE0000007C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003E0000007F000
          000FF800001FFC00003FFE00007FFF0000FFFF8001FFFFC003FFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000856054B5B68375FFB58274FFB48174FFB38173FFB17F73FFB07E
          72FFAF7D72FFAE7C71FFAD7B71FFAC7A70FFAB7970FFA9776FFFA8766EFFA775
          6EFFA6746DFFA5736DFFA4726CFFA3716CFFA16F6BFFA06E6AFF9F6D6AFF9E6C
          69FF9D6B69FF9C6A68FF6F4B4AB5000000000000000000000000000000000000
          0000856054B5C89E90FFEFDBD1FFFFF8F2FFFFFBF7FFFFFEFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE9D9D6FFB38B89FF6F4B4AB50000000000000000000000000000
          0000BE8B79FFEFDACEFFFFF5ECFFFFF8F2FFFFFBF7FFFFFEFDFFFFFFFFFFFFFF
          FFFFF3B25DFFF3B25DFFF3B25DFFF3B25DFFF3B25DFFF3B25DFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE9D9D6FF9F6D6AFF0000000000000000000000000000
          0000C08D7AFFFFFEFEFFFFF5ECFFFFF8F2FFFFFBF7FFFFFDFBFFFFFFFFFFFFFF
          FFFFF3B25DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFA16F6BFF0000000000000000000000000000
          0000C3907BFFFFFFFFFFFFF4EAFFFFF7F0FFFFFAF5FFFFFDFBFFFFFFFFFFFFFF
          FFFFF3B25DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFA4726CFF0000000000000000000000000000
          0000C6937DFFFFFFFFFFFFF3E9FFFFF6EEFFFFF9F4FFFFFCF9FFFFFFFFFFFFFF
          FFFFF3B25DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFA7756EFF0000000000000000000000000000
          0000C9967EFFFFFFFFFFFFF3E9FFFFF6EEFFF3AF58FFF3B05AFFF3B15CFFF3B2
          5DFFF3B25DFFF3B25DFFFFFFFFFF999999FF999999FF999999FF999999FF9999
          99FF999999FF999999FF999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFAA786FFF0000000000000000000000000000
          0000CA977FFFFFFFFFFFFFF2E7FFFFF5ECFFF3AF58FFFFFAF5FFFFFDFBFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FF999999FF999999FF999999FF9999
          99FF999999FF999999FF999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFAC7A70FF0000000000000000000000000000
          0000CC9980FFFFFFFFFFFFF2E5FFFFF4EAFFF3AF58FFFFF9F4FFFFFCF9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFAF7D72FF0000000000000000000000000000
          0000CF9C81FFFFFFFFFFFFF1E3FFFFF3E9FFF3AE56FFFFF8F2FFFFFBF7FFFFFE
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB28073FF0000000000000000000000000000
          0000D19E81FFFFFFFFFFFFF0E1FFFFF2E7FFF3AE55FFFFF7F0FFFFFAF5FFFFFC
          F9FFF3B25DFFF3B25DFFF3B25DFFF3B25DFFF3B25DFFF3B25DFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FF999999FF9999
          99FF999999FFFFFFFFFFFFFFFFFFB48174FF0000000000000000000000000000
          0000D4A183FFFFFFFFFFFFEFE0FFFFF1E3FFF3AD55FFFFF6EEFFFFF8F2FFFFFB
          F7FFF3B15CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FF999999FF9999
          99FF999999FFFFFFFFFFFFFFFFFFB78475FF0000000000000000000000000000
          0000D7A484FFFFFFFFFFFFEDDCFFFFF0E1FFF3AD54FFFFF4EAFFFFF7F0FFFFF9
          F4FFF3B15BFFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBA8777FF0000000000000000000000000000
          0000DAA786FFFFFFFFFFFFECDAFFFFEFE0FFF3AD53FFFFF3E9FFFFF5ECFFFFF8
          F2FFF3B059FFFFFDFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBD8A78FF0000000000000000000000000000
          0000DCA987FFFFFFFFFFFFEAD6FFFFEDDCFFF3AC52FFFFF2E5FFFFF4EAFFFFF6
          EEFFF3AF58FFF3B05AFFF3B15CFFF3B25DFFF3B25DFFF3B25DFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FF999999FF9999
          99FFFFFFFFFFFFFFFFFFFFFFFFFFBF8C79FF0000000000000000000000000000
          0000DEAB88FFFFFFFFFFFFE9D5FFFFEBD8FFF3AC51FFFFF0E1FFFFF2E7FFFFF4
          EAFFF3AF58FFFFF9F4FFFFFBF7FFFFFDFBFFFFFFFFFFFFFFFFFFFFFFFFFF9999
          99FF999999FF999999FF999999FF999999FF999999FF999999FF999999FF9999
          99FFFFFFFFFFFFFFFFFFFFFFFFFFC28F7BFF0000000000000000000000000000
          0000E1AE8AFFFFFFFFFFFFE7D1FFFFEAD6FFF3AB50FFFFEFE0FFFFF1E3FFFFF2
          E7FFF3AE56FFFFF7F0FFFFF9F4FFFFFBF7FFFFFDFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFC5927CFF0000000000000000000000000000
          0000E3B08BFFFFFFFFFFFFE6CFFFFFE8D3FFF3AA4EFFFFEDDCFFFFEFE0FFFFF2
          E5FFF3AD55FFFFF5ECFFFFF7F0FFFFF9F4FFFFFBF7FFFFFDFBFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFC7947DFF0000000000000000000000000000
          0000E6B38CFFFFFFFFFFFFE5CCFFFFE6CFFFF3AA4EFFF3AA4FFFF3AB50FFF3AC
          52FFF3AD54FFF3AD55FFFFF5ECFF999490FF999592FF999794FF999897FF9998
          98FF999999FF999999FF999999FF999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCA977FFF0000000000000000000000000000
          0000E9B68DFFFFFFFFFFFFE3C8FFFFE5CCFFF3A94CFFFFE9D5FFFFEBD8FFFFED
          DCFFFFF0E1FFFFF2E5FFFFF3E9FF99938EFF999490FF999591FF999693FF9997
          95FF999898FF999999FF999999FF999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCC9980FF0000000000000000000000000000
          0000EBB88EFFFFFFFFFFFFE1C4FFFFE4CAFFF3A84BFFFFE7D1FFFFE9D5FFFFEB
          D8FFFFEDDCFFFFF0E1FFFFF2E5FFFFF2E7FFFFF4EAFFFFF6EEFFFFF8F2FFFFF9
          F4FFFFFBF7FFFFFCF9FFFFFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCD9A80FF0000000000000000000000000000
          0000EDBA90FFFFFFFFFFFFDFC1FFFFE2C6FFF3A84AFFFFE5CDFFFFE7D1FFFFE9
          D5FFFFEBD8FFFFEDDCFFFFEFE0FFFFF1E3FFFFF2E7FFFFF4EAFFFFF5ECFFFFF7
          F0FFFFF8F2FFFFFAF5FFFFFBF7FFFFFCF9FFFFFDFBFFFFFEFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD09D81FF0000000000000000000000000000
          0000F0BD91FFFFFFFFFFFFDDBDFFFFE0C2FFF3A748FFF3A84AFFF3A84BFFF3A9
          4CFFF3AA4EFFF3AA4FFFFFEDDCFF998F86FF999087FF999189FF99928CFF9992
          8CFF99948FFF999490FF999591FF999592FF999693FFFFFBF7FFFFFCF9FFFFFD
          FBFFFFFDFBFFFFFEFDFFFFFEFDFFD3A082FF0000000000000000000000000000
          0000F3C093FFFFFFFFFFFFDCB9FFFFDEBFFFF3A647FFFFE2C6FFFFE4CAFFFFE5
          CDFFFFE7D1FFFFE9D5FFFFEAD6FF998E83FF998F85FF999087FF999188FF9991
          8BFF99928CFF99928CFF99938EFF999490FF999591FFFFF8F2FFFFF9F4FFFFFA
          F5FFFFFBF7FFFFFBF7FFFFFBF7FFD6A384FF0000000000000000000000000000
          0000F5C294FFFFFFFFFFFFE6CDFFFFDCB9FFF3A646FFFFE0C2FFFFE2C6FFFFE4
          CAFFFFE5CCFFFFE6CFFFFFE8D3FFFFEAD6FFFFEBD8FFFFEDDCFFFFEFE0FFFFF0
          E1FFFFF1E3FFFFF2E7FFFFF3E9FFFFF4EAFFFFF5ECFFFFF6EEFFFFF6EEFFFFF7
          F0FFFFF8F2FFFFF8F2FFFFF8F2FFD8A585FF0000000000000000000000000000
          0000F8C595FFFFFFFFFFFFF8F2FFFFE6CDFFFFDCB9FFFFDDBDFFFFDFC1FFFFE1
          C4FFFFE3C8FFFFE5CCFFFFE6CFFFFFE7D1FFFFE9D5FFFFEAD6FFFFECDAFFFFED
          DCFFFFEFE0FFFFF0E1FFFFF1E3FFFFF2E5FFFFF2E7FFFFF3E9FFFFF3E9FFFFF4
          EAFFFFF5ECFFFFF5ECFFEEDAD1FFDBA886FF0000000000000000000000000000
          00008D71558FFAC796FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFDFBFFEEDACDFFE7BD9FFF674A428F0000000000000000000000000000
          0000000000008D71558FFBC897FFFAC796FFF9C696FFF8C595FFF7C495FFF5C2
          94FFF4C193FFF3C093FFF2BF92FFF1BE92FFF0BD91FFEEBB90FFEDBA90FFECB9
          8FFFECB98FFFEBB88EFFEAB78EFFE9B68DFFE7B48CFFE6B38CFFE5B28BFFE4B1
          8BFFE3B08BFFE2AF8AFF674A428F000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFE0000007C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003E0000007FFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000119242F015175B201699CED016A9FF0016C9EF0016C9EF0016A9EF00169
          9CF001699DF001699BF001699BF001689CF0016899F0016798EF016293EA0161
          93EA015F92EA015D91EA015D90EA015C8EEA015B8FEA01588AE6015787E40156
          86E4015585E4014E7CD401375590010A0E140000000000000000000000000118
          212D0172A4F262BEDAFF90E2F1FF89E2F1FF7EE2F1FF74E2F1FF6BE2F1FF60E3
          F2FF57E4F2FF4BE7F3FF42E7F4FF37EBF5FF2EEDF6FF23F2F7FF18F2F8FF0EF7
          FBFF03FBFEFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FF
          FFFF01FFFFFF01FCFDFF01B6CFFF015582DD010A101800000000000000000150
          74AE68BEDAFFA4E4F3FF90DDEEFF85DBEEFF7ADBECFF70DBECFF68DBEEFF5DDC
          EDFF53DDEDFF49DEEEFF40E0EFFF37E3F0FF2BE7F3FF15ECFFFF0DF2FFFF0EF1
          F9FF04F1F8FF01F7FAFF01FAFCFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FF
          FFFF01FFFFFF01FFFFFF01FFFFFF01BAD2FF013D5E9E00000000000000000168
          98E098DCEEFF9CDFEFFF8FDCEDFF84DBEDFF7BDBECFF71DBECFF68DBEDFF5EDC
          EDFF54DDEDFF4ADEEEFF41E1EFFF31E2F4FF01DEFFFF1899ABFF1E9EADFF01F1
          FFFF04F3FBFF01F5F9FF01F9FCFF01FDFFFF01FFFFFF01FFFFFF01FFFFFF01FF
          FFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF015B8EF000000000000000000160
          8BCC7CCCE4FFA0E0F0FF90DCEDFF86DBEDFF7DDBECFF72DAECFF69DBECFF5FDB
          EDFF56DCEDFF4CDEEEFF45E1EEFF0FD8FFFF196173FF382521FF463330FF3577
          7FFF01F5FFFF01F3F9FF01F8FBFF01FDFDFF01FEFEFF01FFFFFF01FFFFFF01FF
          FFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF016194F800000000000000000138
          50732198C3FFAAE6F4FF91DCEDFF87DBEDFF7EDBECFF74DAECFF6BDBECFF61DB
          EDFF58DCEDFF4EDDEDFF48E2EEFF07CCFCFF1D2F35FF2E2A28FF3C3636FF434A
          4CFF01EFFFFF03F2F8FF01F5FAFF01F9FCFF01FEFFFF01FEFEFF01FFFFFF01FF
          FFFF01FFFFFF01FFFFFF01FFFFFF01D8E6FF01486EBC00000000000000000105
          0607016A99DB70C9E1FF9BDFEFFF89DBEDFF80DBECFF76DAECFF6DDAECFF64DB
          ECFF5ADCEDFF51DDEDFF4AE0EFFF1ECDF0FF0681A3FF291611FF37211DFF1898
          B6FF04E8FFFF06F0F6FF01F3F8FF01F7FAFF01FCFCFF01FFFEFF01FFFFFF01FF
          FFFF01FFFFFF01FFFFFF01FFFFFF0178A7FD011A263900000000000000000000
          0000012D404D068ABEFE9FE3F2FF8EDBEDFF82DBECFF79DAECFF70DAECFF67DB
          ECFF5DDBEDFF54DCEDFF4ADDEDFF44E3F0FF14C6F0FF01B0E7FF01B9EDFF07DA
          FFFF15ECF5FF0CEFF6FF04F1F7FF01F4F9FF01F8FBFF01FBFEFF01FFFEFF01FF
          FFFF01FFFFFF01FFFFFF01BDD4FF01456BA20000000000000000000000000000
          00000000000001648CB64AB5D6FF98E1F0FF85DBEDFF7CDAECFF73DAECFF6ADA
          ECFF61DBECFF57DCEDFF4EDDEDFF47DFEEFF40E3EFFF12F2FFFF0BF5FFFF22EB
          F4FF1CE9F3FF11EBF5FF09EEF6FF03F1F7FF01F5F9FF01F8FBFF01FCFCFF01FE
          FEFF01FFFFFF01FEFEFF016699F1010D131A0000000000000000000000000000
          000000000000011821270182B7F88DDCEDFF8BDCEEFF80DAECFF76DAECFF6DDA
          ECFF65DBECFF5BDBECFF53DCEDFF4EDDEDFF18E9FFFF1BA1AEFF2D898EFF03F9
          FFFF22E7F2FF18E9F3FF10ECF5FF09EEF6FF03F1F7FF01F4F9FF01F7FAFF01F9
          FDFF01FFFFFF01A2C3FF01375378000000000000000000000000000000000000
          00000000000000000000014F6D8A29A5CEFF99E1F1FF83DCECFF7ADAECFF71DA
          ECFF69DAECFF60DBECFF57DCEDFF53DFEEFF01E8FFFF413A39FF503734FF01E7
          FBFF24E8F5FF1EE7F2FF17E9F3FF11ECF5FF0AEEF6FF05F0F7FF01F3F8FF01F9
          FBFF01E4EEFF015C8CDB01040607000000000000000000000000000000000000
          0000000000000000000001090D0E017AABE571CEE5FF8CDEEDFF7FDAECFF76DA
          ECFF6DDAECFF65DBECFF5FDBECFF3AE0F9FF0AABC9FF3F302EFF483B3AFF1EA6
          B5FF19EDFFFF29E5F1FF1FE7F2FF18E9F3FF13EBF5FF0DEDF5FF09EFF6FF05FC
          FFFF0188B5FE01283B4F00000000000000000000000000000000000000000000
          0000000000000000000000000000013B4A5D0D99C4FE98E2F1FF84DAECFF7BDA
          ECFF72DAECFF6ADAECFF68DCECFF1CDEFFFF1E6674FF393231FF403B3AFF346D
          74FF08EFFFFF31E3F0FF27E5F1FF21E7F2FF1BE8F3FF16EAF4FF13F1F8FF0AC4
          DCFF015580B80000000000000000000000000000000000000000000000000000
          000000000000000000000000000001010201017292C552BFDDFF90DFEFFF80DA
          ECFF77DAECFF6FDAECFF70DDECFF01D7FFFF2B292AFF343333FF3A3A3AFF4639
          38FF01E7FFFF38E2F0FF2FE3F0FF29E4F1FF24E6F2FF1FE8F4FF1CEFF6FF0177
          A9F9011720290000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000001232E33018FC3FC8EDEEFFF88DB
          ECFF7DDAECFF77DAECFF62DBF0FF01AEDDFF2A1B16FF2E2E2EFF363636FF4334
          31FF0EB6D4FF2EE4F7FF39E1EFFF32E2F0FF2DE5F1FF2BECF6FF10ABCCFF0146
          658F000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000015D7F9A34B0D5FF97E0
          EFFF84DAECFF7EDBECFF52D5F1FF0384A8FF241915FF2A2A2AFF323232FF3C33
          30FF1F899DFF22E2FFFF42DFEFFF3AE0EFFF37E3F1FF2EDDECFF016B9CE90109
          0E10000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000010F14150185B7ED7DD4
          E8FF8DDEEDFF85DBECFF4DD1F1FF047391FF1F1512FF262626FF2D2D2DFF3830
          2EFF247283FF1EE1FFFF4CDEEEFF43E0EEFF45E9F5FF0D94BEFE013449650000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000001455D6E16A0
          CEFF9DE2F0FF8CDDECFF60D3EEFF017FA4FF1C0E0BFF222222FF292929FF3429
          26FF1784A0FF2CDCFEFF54DEEDFF4FE0F0FF35C7DEFF01618BCC010203030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010304040177
          A3D262C5E2FF97DEF0FF8CDDEEFF01AAE2FF112328FF1F100BFF281916FF2B2D
          2EFF01BFF4FF56DBF1FF5DDDEEFF5BE3F1FF0584B6FD0122303C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000012C
          3C420196C8FE9EE0F0FF97DFEFFF6BD0EAFF03B3E9FF0197C7FF0195C2FF01BD
          F2FF47D4F2FF6EDDECFF69E1EFFF31B2D2FF01557AA500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001678DAA44B5D8FFA2E2F0FF96DEEEFF95E0EFFF80DBEFFF74D9EFFF80DD
          EEFF79DCECFF73DCEDFF69DAECFF0179AAF30110171C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001141D1F0188BBF492DAECFF9CDFEEFF93DCEDFF8EDBEDFF89DBEDFF83DB
          ECFF7EDAECFF82E1F0FF239FC7FF01435D7B0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000014B687E23A3CEFFB0E5F2FF9BDDEFFF96DCEDFF90DCEDFF8BDB
          EDFF8ADEEFFF6ACCE5FF016C9CDD010608090000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000010709090176A6DD7CCDE4FFABE2EFFF9EDEEEFF99DDEEFF95DC
          EDFF9CE4F2FF0E8EBFFE01304251000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000013347510993C3FEBAE8F4FFACE2F1FFA5DFEFFFA9E5
          F2FF5ABBDAFF01638DBB00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001638AAD2CA0CAFFA1DBEDFFABE2F2FF5EBD
          DBFF017AADF20118212800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010506070145608401709EDE0177A9EF015C
          81B6011B252F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFE0000007C000000380000001800000018000
          000180000001C0000003E0000003E0000007F0000007F000000FF800001FF800
          001FFC00003FFE00003FFE00007FFF00007FFF0000FFFF8001FFFFC001FFFFC0
          03FFFFE003FFFFE007FFFFF007FFFFF00FFFFFF81FFFFFFC3FFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002222224E474747BC606060F86D6D6DFF666666FE4A4A
          4AAB000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020202023E3E3E99787878FEBDBDBDFFCFCFCFFFB0B0B0FF939393FF5D5D
          5DDB000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00003434347F888888FFE9E9E9FFBCBCBCFFA5A5A5FFA3A3A3FF898989FF5151
          51C0000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000909
          0911636363F5E6E6E6FFA7A7A7FFA2A2A2FFA6A6A6FF8A8A8AFF555555D90D0D
          0D1A000000000000000001010102232323460000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002222
          224F8D8D8DFFADADADFFA5A5A5FFA7A7A7FF898989FF545454D80C0C0C1B0000
          00000000000003030306464646A4686868FE2121214D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002F2F
          2F6F858585FEAAAAAAFFA6A6A6FFA8A8A8FF7D7D7DFE474747B40808080F0000
          00000707070C454545B8919191FEB0B0B0F5414141A800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003333
          3378878787FFABABABFFA8A8A8FFA8A8A8FFACACACFE757575FE666666FD5F5F
          5FE2535353DFA1A1A1FFEFEFEFFFA9A9A9FE545454E000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001717172E6060
          60ECA1A1A1FEACACACFFA9A9A9FFA8A8A8FFA9A9A9FFA2A2A2FF7D7D7DFF8282
          82FF878787F6FEFEFEFEAEAEAEFFA2A2A2FF606060F800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001616162D5E5E5EE9BABA
          BAFFBCBCBCFEABABABFFAAAAAAFFA9A9A9FFA8A8A8FFA8A8A8FFA9A9A9FF9C9C
          9CFF909090F9B6B6B6FDA2A2A2FFA2A2A2FF575757E400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001616162A5F5F5FE8BEBEBEFFCCCC
          CCFDACACACFEACACACFFACACACFFABABABFFAAAAAAFFA9A9A9FFA9A9A9FFA8A8
          A8FFA7A7A7FFA5A5A5FFA8A8A8FF8C8C8CFF3E3E3E9D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000015151529616161E7C0C0C0FFD0D0D0FEADAD
          ADFEB5B5B5FFCACACAFFC5C5C5FFB3B3B3FFABABABFFAAAAAAFFA9A9A9FFA8A8
          A8FFA8A8A8FFAAAAAAFF9A9A9AFF5F5F5FEF1010102300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000014141426606060E4C2C2C2FFD6D6D6FEB0B0B0FEB5B5
          B5FFC5C5C5FFBABABAFFC2C2C2FFC7C7C7FFACACACFFAFAFAFFFAEAEAEFFAEAE
          AEFFAFAFAFFF8D8D8DFF5B5B5BE91A1A1A380000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000013131324636363E4C5C5C5FFDBDBDBFEB2B2B2FEB6B6B6FFC0C0
          C0FFBABABAFFBBBBBBFFC7C7C7FFB8B8B8FF9F9F9FFF7E7E7EFF838383FF8181
          81FF6A6A6AFD484848AD0E0E0E1C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000014141424636363E1C7C7C7FFE1E1E1FEB5B5B5FEB6B6B6FFBDBDBDFFB9B9
          B9FFBABABAFFC3C3C3FFB8B8B8FFA0A0A0FF5D5D5DE8252525522828285D2A2A
          2A5F111111240000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001212
          1220646464DFCBCBCBFFE8E8E8FEB8B8B8FEB7B7B7FFB7B7B7FFB8B8B8FFB8B8
          B8FFBEBEBEFFB9B9B9FFA5A5A5FF626262EB1818182F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001212121F6565
          65DFCCCCCCFFEEEEEEFEBBBBBBFEB7B7B7FFAFAFAFFFB7B7B7FFB9B9B9FFBABA
          BAFFBABABAFFA8A8A8FF676767ED181818300000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000303030438383868626262BA6D6D6DCF616161B8686868DECFCF
          CFFFF3F3F3FEBEBEBEFEB9B9B9FFA4A4A4FFB2B2B2FFB5B5B5FFB4B4B4FFBABA
          BAFFABABABFF696969ED1B1B1B34000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B0B0B11606060BF999999FFBEBEBEFFB2B2B2FF939393FFB7B7B7FEFCFC
          FCFEC1C1C1FEB9B9B9FF9B9B9BFFACACACFFB1B1B1FFA9A9A9FFBBBBBBFFAFAF
          AFFF6D6D6DF01C1C1C3600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000707
          070B636363C9B5B5B5FFE3E3E3FFD1D1D1FFC6C6C6FFB0B0B0FFBDBDBDFEC6C6
          C6FEBABABAFF919191FFA8A8A8FFADADADFF9F9F9FFFBBBBBBFFB2B2B2FF7070
          70F01D1D1D370000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003D3D
          3D77AFAFAFFFE9E9E9FFCBCBCBFFC8C8C8FFC8C8C8FFC8C8C8FFC6C6C6FFC7C7
          C7FF929292FF9B9B9BFFA8A8A8FF939393FFBABABAFFB6B6B6FF727272F12121
          213C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000006868
          68CDDBDBDBFFCBCBCBFFCACACAFFC9C9C9FFC8C8C8FFC7C7C7FFC6C6C6FFC7C7
          C7FFB5B5B5FF898989FF8B8B8BFFBABABAFFB9B9B9FF747474F32222223C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008181
          81F6D2D2D2FDCDCDCDFFCDCDCDFFCDCDCDFFCCCCCCFFCACACAFFC8C8C8FFC6C6
          C6FFC8C8C8FFC6C6C6FFC6C6C6FFBDBDBDFF787878F324242440000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009393
          93FED2D2D2FFD1D1D1FFB7B7B7FFA5A5A5FFAEAEAEFFB7B7B7FFC4C4C4FFCACA
          CAFFC7C7C7FFC7C7C7FFC7C7C7FF7A7A7AF62626264300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009898
          98FFD5D5D5FFB7B7B7FF767676DC3C3C3C694C4C4C855D5D5DA5858585FACCCC
          CCFFC8C8C8FFC7C7C7FFC5C5C5FF757575E30000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009B9B
          9BFFBBBBBBFF777777DD1313131D000000000000000000000000646464B5AFAF
          AFFFCBCBCBFFC8C8C8FFCDCDCDFF888888FE0E0E0E1600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008B8B
          8BF37E7E7EDE16161620000000000000000000000000363636578E8E8EFCCDCD
          CDFFCCCCCCFFCBCBCBFFD0D0D0FF7F7F7FF90808080C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003636
          3655161616200000000000000000000000002C2C2C448B8B8BF8D9D9D9FECECE
          CEFCCBCBCBFFCFCFCFFFB2B2B2FF5F5F5FB00000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000F0F0F15898989F1E2E2E2FED5D5D5FDCCCC
          CCFFCFCFCFFFCCCCCCFF818181F61A1A1A2B0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000141414219F9F9FFFEAEAEAFECDCDCDFECFCF
          CFFFB7B7B7FF8F8F8FFE4040406A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000E0E0E15909090FEA1A1A1FE969696FF7F7F
          7FF25A5A5AAC2828284700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFF83FFFFFE03FFFFFC03FFFFFC07FFFFFC0F3FFFF80E1FFFF
          8001FFFF8001FFFF0001FFFE0001FFFC0003FFF80007FFF0000FFFE0003FFFC0
          03FFFF8007FFF0000FFFE0001FFFC0003FFF80007FFF8000FFFF8001FFFF8003
          FFFF8003FFFF8F03FFFF9E03FFFFBE03FFFFFC07FFFFFC07FFFFFC1FFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000071A5000071
          A5000071A5000071A5000071A5000071A5000071A5000071A5000071A5000071
          A500006699000071A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00006699000071
          A5000280B10000A3D60000A3D60000A3D60000A3D60000A3D60000A3D60000A3
          D6000098CC000089BD000071A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00006699000FCD
          FB000066990000AFE10003B7E70000BEF20000BEF20000BEF20000BEF20003B7
          E70000BEF20000AFE10000AFE1000071A500FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00006699000FCD
          FB000FCDFB000066990000BEF20000CBFD0000CBFD0000CBFD0000CBFD0000CB
          FD0000CBFD0000BEF20000BEF20003B7E7000280B100FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000066990024D4
          FB0024D4FB0024D4FB00006699000098CC000098CC0000A3D6000098CC0000A3
          D6000098CC000098CC000098CC000098CC000098CC000280B100FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000066990024D4
          FB0049DBF2005FE8F90070EFFB0070EFFB00D6820000FFE2B600FFE2B600FFE2
          B600FFE2B600FFE2B600D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000066990049DB
          F2005FE8F9008AEEFB007FF5FC0070EFFB00D6820000FFE2B600000102000001
          020000010200FFE2B600D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000066990049DB
          F2005FE8F90070EFFB0070EFFB005FE8F900D6820000FFE2B600FFE2B600FFE2
          B600FFE2B600FFE2B600D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000066990037D3
          EC007FF5FC007FF5FC005FE8F90000669900D6820000FFE2B600000102000001
          020000010200FFE2B600D6820000FFFFFF00FCFDFD00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFDFD000280
          B10029B6D20037D3EC0000669900F0FBFD00D6820000FFE2B600FFE2B600FFE2
          B600FFE2B600FFE2B600D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6820000FFE2B600000102000001
          020000010200FFE2B600D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6820000FFE2B600FFE2B600FFE2
          B600FFE2B600FFE2B600D6820000FFFFFF00FCFDFD00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6820000D6820000D6820000D682
          0000D6820000D6820000D6820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        MaskColor = clWhite
      end>
  end
  object cxSmallImages: TcxImageList
    SourceDPI = 96
    FormatVersion = 1
    DesignInfo = 72352632
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000803010B742301CD6F1F01B83910015E120601190000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000591B017D5B514EFF7E6C62FF985F42FFA03E0EFD9227
          01D2621B018C300E01470803010A000000000000000000000000000000000000
          00000000000011060116A33601F1475058FF72818CFF97A6AEFFA2B3BCFFACB2
          B4FFB5987FFFB87944FFB55001F68E3001BB4E18017000000000000000000000
          00000000000067230195EF8201FF94440DFF8C5A37FF8C7C73FF8E9AA6FF9AA9
          B5FFA8B8C2FFB4C3CEFFC1D3DFFFDADDD5FE4D1A016D00000000000000000000
          00001C080126C65F01FAFFA005FFC56616FFCE6101FFC25001FFB54401FFA452
          18FF996A50FF9C8F89FFA6B5C0FFBCD0DBF40000000000000000000000000000
          00007E3201ADFF9C01FFD37614FFD17412FFCB6601FFC96201FFC45D01FFC158
          01FFBC4F01FFB24001FFA83901FFA85F32FE3D1802570000000000000000290F
          013AE27801FEFD9E17FFCE741CFFD77001FFD26B01FFCD6701FFC96401FFC45F
          01FFC15B01FFBC5601FFBB5401FFAD4501F4260E01350000000000000000893E
          01B6FFAC11FFD3781EFFE58107FFD88326FFDB8424FFD67101FFD16401FFCA60
          01FFC66001FFC56001FFBD5501FB3D18014E000000000000000000000000D272
          01F2F2992CFFE4891DFFDA7706FFE3B475FFE5B478FFE7B882FFEAB67CFFEAAA
          5FFFD87001FFCB6201FE5923016C00000000000000000000000000000000D981
          1CF1E08727FFFF9D07FFD97A0EFFD88934FFD89450FFE3AF73FFEABF8CFFDE8C
          2FFFD46C01FF7131018C00000000000000000000000000000000000000003B1D
          01417D3E018AB15E01BFD27B01F1E58101FFEE8301FFE57A01FFDD7A03FFE179
          01FF833E01AB0201010200000000000000000000000000000000000000000000
          00000000000000000000030101021E100129441E015E70340193A04F01C88F46
          01BD080301080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000F8FF0000F01F0000F0010000E0010000E0030000C001
          0000C00300008007000080070000800F0000C01F0000FC3F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000260E01387D2401E65F19019B330F01530D0401120000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000020101016E2601CB566E7BFF897B73FF9D6B51FFA14C1DFC922C
          01CC60190189310E01460803010A000000000000000000000000000000000000
          0000000000003D17015F9F3F01FF465D6DFF818B92FF9BA5ABFFA6B3BAFFB1BC
          C2FFBDAC96FFC28B5CFFC06923F7742501A70000000000000000000000000000
          00000A03010BAA4B01E4FC9201FFAF4701FF81797BFF828B93FF8D9091FF9799
          9BFFA4A9ADFFB2BCC3FFC5DAE6FE1D0E052E0000000000000000000000000000
          0000591F0182F89202FFCE6B06FFEB8001FFCE5F01FFAB938CFFA6B1BBFFABAC
          AFFFB3B3B3FFA4A4A5FFA7A9ABFE1213131A0000000000000000000000001708
          011ECF6A01F5F08E10FFD67101FFE78101FFE88001FFD36001FFC1927DFFCEDC
          E8FFBFC1C4FFF5F5F5FFB8B8B8DF121212150000000000000000000000007332
          019FFFA60EFFCB6806FFEF8801FFE98101FFE68101FFE78001FFD96801FFBC7A
          50FFE8F6FFFFC0C2C5FE2D2E2E3B00000000000000000000000000000000C86E
          04E7E0821BFFEE8701FFEE8501FFE48106FFEA8101FFE78101FFE78001FFDF70
          01FFBB672CFFEAF8FEFE51565A6000000000000000000000000000000000C16B
          14E2E37E01FFFB9201FFDC8828FFEBBC7CFFE68511FFEC8201FFE78101FFE881
          01FFE87C01FFAD4F01ED1A070122000000000000000000000000000000002712
          012FC26601DCFE9601FFD98B32FFEFBF7AFFF1C07FFFE88E1FFFED8301FFEA83
          01FFEF8901FFCF6901F12D110137000000000000000000000000000000000000
          0000120801139E5101BDFD9301FFE08619FFEEBB78FFF1C889FFE88911FFF58D
          01FFD16B01F13013013B00000000000000000000000000000000000000000000
          00000000000005020104813E0194FD9101FEEA890CFFE1902FFFFB9101FFD571
          01F13114013A0000000000000000000000000000000000000000000000000000
          00000000000000000000000000005D2A0166EE8401F9FFA101FFDB7701F13115
          013A000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000003317013EB65E01DC2E14013A0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F8FF0000F01F0000E0030000E0070000C0070000C0070000800F
          000080070000800F0000C00F0000E01F0000F03F0000F87F0000FEFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A03010E6C2804CB7F2901D559170191380F015C1807
          0127020101020000000000000000000000000000000000000000000000000000
          00000000000000000000762401A754494AFF738699FF999CA1FFA1887CFFA873
          51FFA65A26F08D3601BD631B0189390F01541005011600000000000000000000
          00000000000049170167F77B01FE7C4C07FF576128FFA3735BFFAB8F90FFAEB3
          C4FFB8C4D7FFC0D3E5FFCFD6D3FFAC8765E11E0A013300000000000000000000
          0000230B012FD15F01F6F9A009FF025719FF2D6A38FF107217FF0B5A01FF2346
          01FF504F01FF82612EFFB47E6DFF7C6964A00301010300000000000000000A04
          010DA94801D9FFA209FF087306FF27412CFF8F7286FFB29AAEFFC0ACC3FFAEB0
          B7FF81AA8DFF4E9A60FF22872FFF1D6901FF012A016A00000000000000005E27
          0183FF9E06FF47750AFF1ECD5BFF018136FF01704BFF137A2DFF3E822FFF718E
          5FFFADA69DFFD9BFCEFFEBD0E7FFA29BA3BD010401090000000000000000A74A
          01B299850DFF0DB448FF1CB060FF235081FF5A7E96FF4389B6FF1983B5FF017C
          8DFF01694EFF016324FF17782BFF2F6C21CB0108011500000000000000002326
          01640AA435FF2FC95BFF0183C2FF1F4A68FF716867FFAE8D83FFC19F96FFCFAE
          A8FFBDBBC1FF8DB8D2FF5AA5D1FF3196C4FE013D6C9F00000000000000000174
          11C340DF65FF017D9EFF06D1FFFF0D9ED0FF01A5DEFF0191C9FF1284B0FF3B81
          9DFF6B8697FFA29EA0FFDDBBB2FFEEC9BAEE01080B0F00000000000000001BA6
          10EA0C8E86FF01BAFEFF14A6D4FF0BB4E5FF01B3E7FF01B2E6FF01B1E6FF01B0
          E6FF01A7E0FF0195CFFF0187C0FF1B86B0FC0122335200000000000000000145
          468901A7EDFF0DB4E5FF17B4E4FF05B7E9FF01B5E8FF01B1E7FF01ADE3FF01AB
          E0FF01ACE0FF01B1E5FF01A8DCFE014E71940103040500000000000000000179
          A8CD06C2F6FF2BB9E3FF0BBBEBFF75CEE6FF83D3EAFF81D6EDFF70D5F1FF4ECF
          F3FF01B7ECFF019CCFF701364F6100000000000000000000000000000000016D
          909813AADCF00DBFF1FE01B3E9FF0ABEEEFF1EC2EEFF3AC6EEFF5ACFEFFF08BE
          F0FF0188B9E3011F2C3600000000000000000000000000000000000000000000
          0000000000000110171A0125324201374E6A01527091016D94B90189B8E1016D
          96C1010E12170000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F87F0000F0030000E0030000E0030000C0010000800300008003
          00008001000080030000800100008003000080070000801F0000F83F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000005050509423835A1584C46D15F6163E6414142A90404
          0408000000000000000000000000000000000000000000000000000000000000
          00000E0F0F16514744B1816C5AF2D19F5CFFCB7527FFC4BDB9FFB9BBBBFF5A5A
          5AD4111111220000000000000000000000000000000000000000000000005247
          42BA937B66F9E6B070FFF2B05FFFEBAA56FFD78F46FFC3B8B0FFB9BCBEFFBBBB
          BBFF727272F12424244D00000000000000000000000000000000000000007464
          59E5FFC27FFFF5BC77FFF4BA70FFEFBC79FFCC965CFFC9BFB3FFAFB0B2FFB2B2
          B2FFB9B9B9FF808080FD1010102100000000000000000000000000000000806E
          61EEFFCB8CFFD1925CFF94522DFF6F4634FF847067FFDCDEDFFFA5A5A6FF6D6D
          6DFF6C6D6DFF8C8C8CFF2021216E00000000000000000000000000000000736A
          65DDC8A079FFA28778FFCDD6DEFFECF5FBFFD9D5D7FFF1F3F5FF9A9A9AFF9898
          98FF737475FF818182FF282A2A8F0E0101220201010300000000000000004B4C
          4C8AE8ECF1FFCFC0BBFFDFB589FFD2975FFFCA7013FFF2F4F6FFA1A1A2FF9599
          9AFF615E5FFF7D625DFF7C2201F1993A01FF652201AA0000000000000000494A
          4A84F0DDC5FFD97F1FFFEDB26BFFE6B989FFF8D5AFFFFFFFFFFFC5C6C7FF9C9C
          9DFF982F01FFBA5301FFD4781AFFEEA04DFFB96E30EB00000000000000002C2C
          2D58E5E7E9FFFFF0DDFFFDFFFFFFF1DCC9FFF2E2CFFFD19457FFF5F6FBFF9CA1
          A4FFC26D20FFCA823FFFC68544EDD59750DE5E3C166900000000000000000202
          0201909192D3FEEEDDFFD07D24FFE9AD67FFD59C67FFEDB97DFFEDECEEFFDBDD
          DEFF888B8CFF8B9197FF3B3D40B3000000000000000000000000000000000000
          00001D1D1D219D9EA0E0E8DCCFFFDFE6ECFFC7CDD2FFA7ABB0FF818182FFB9BA
          BBFF909090FF8A8A8AFF444444BD000000000000000000000000000000000000
          00000000000026262630A5A6A7FF5B5B5CFF545454FF4A4A4AFF3C3C3CFF7070
          70FFC2C3C5FF838383FF4D4D4DBD000000000000000000000000000000000000
          0000000000002626262F696969A3828282FF909090FFAEAEAEFFBEBEBEFFD9D9
          D9FFDFDFDFFFB3B3B3FF585858BE000000000000000000000000000000000000
          000000000000111111163B3B3B5E8C8C8CD79F9F9FF1939393E57F7F7FD16969
          69BC555555983F3F3F8022222244000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F87F0000E03F0000801F0000800F000080070000800700008001
          00008001000080010000C0070000E0070000F0070000F0070000F00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002F3D
          316BA68959B1C36B17E6BB6F22DBB26A21D1A96621C6A26120BB985D22B09059
          21A68955229B7F522390774C23866F49237B56391D5E000000000000000055AD
          75F6EAD99BFFD66F0FFFD77B1EFFD77C20FFD88026FFDB8129FFE1822DFFE183
          2FFFDF8936FFDF8C3BFFE19040FFE49346FFC08040D600000000000000005EB0
          7BF2FBD49BFFF57520FFF28130FFE98D3DFFD97316FFE97921FFC3882BFFA197
          39FFEA8B3BFFDE8835FFE08E3DFFE29343FFC37F3CD800000000000000006FB1
          84EF33B83CFF15AA27FF36B744FFC8C883FFFFAA74FFA58F2DFF02BD3EFF12BD
          42FFFFBE8AFFE38835FFDF8A38FFE19041FFC57F3BDB000000000000000076B6
          89EC04B522FF01A818FF11BB3DFF01AA1BFF09AF2DFF05B532FF14B537FF0AB6
          37FF8DC977FFFFBA83FFDF832EFFE18F3FFFC5813CDE00000000000000006BBA
          81E920BB35FFFFE7EFFF4AE484FF0DAE26FF0FA922FF15AE2CFF18B132FF16B4
          35FF07B738FFFFD1A1FFFD9955FFE78939FFC9813AE000000000000000008C8D
          88EFFFF7FFFFFFF1FEFFD0E2D1FF36E770FF019F0CFF01A615FF0BB22BFF1AB8
          3DFF11B233FF08B83CFF76BF62FFF3AC6CFFE18244E300000000000000008C82
          83EEE6DCDDFFE3D9DBFFDEC1CDFF8EF7B2FF3CD25EFF92BE8FFFE7DFE1FF32DC
          6EFF18B739FF17B43BFF14BC44FF0DC34EFF0000000000000000000000009089
          89ECFFFBFBFFFCF4F4FFE9DDDFFFFFF0FBFFFFF0FEFFF1DAE3FFFFECF8FFBEE8
          C7FF3CF079FF12B532FF1AB73DFF20BE4AFF0000000000000000000000008E89
          89EAFFFEFEFFFEF7F7FFE7E0E0FFFCF4F4FFFBF2F2FFE5DBDCFFFBECEEFFFFE8
          F4FFB2E1BDFF6BD88DFF2ACF5CFF22D353FE0000000000000000000000008682
          82E8E9E5E5FFE4E0E0FFD0CBCBFFE3DEDEFFE4DBDBFFD1C8C8FFE4DADAFFE6D8
          D9FFD9C2C9FFF8D5E1FFFFD9EBFFA58C91F30000000000000000000000008C89
          89E7FFFFFFFFFFFCFCFFEBE6E6FFFEF8F8FFFDF5F5FFE6DEDEFFFAF0F0FFF9EC
          ECFFE3D6D7FFF6E8E8FFFBEBECFFA79394F30000000000000000000000008C8A
          8AE5FFFFFFFFFFFFFFFFF0EBEBFFFFFFFFFFFFFCFCFFECE6E6FFFFF8F8FFFFF6
          F6FFE9DFDFFFFEF1F1FFFFF4F4FFA99898F30000000000000000000000007E7D
          7DE4989595F5918E8EF1898686ED8A8585E9847F7FE47E7979E17D7676DD7972
          72DA746B6BD6716969D26F6666CF625555B40000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF00008001000080010000800100008001000080010000800100008001
          000080030000800300008003000080030000800300008003000080030000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000109011001340178035401C6086501EB096501EC0351
          01C80132017B0108011300000000000000000000000000000000000000000000
          00000000000001250147026F01E9098801FF108D01FF148801FF207F01FF3373
          01FF276E01FF076101EC0124014D000000000000000000000000000000000000
          000001260146038205F905A009FF019D01FF01A30EFF019A01FF019701FF0194
          01FF1F7F01FF3A6C01FF106901FB0123014D0000000000000000000000000108
          010E067A07E70CA818FF02A10EFF39BF55FFDCF3E9FF7BDD9BFF019D04FF0199
          01FF019801FF138501FF3B6D01FF066301EC0108011300000000000000000138
          017325A830FF09A91CFF2CB43FFFF6F0F5FFFCF6FAFFFFFFFFFF7EDE9CFF019E
          08FF019B03FF019901FF227E01FF276F01FF0132017C00000000000000000364
          01C03ABF50FF21AA2BFFECE6EAFFF5EEF4FFFFF7FFFFFFFBFFFFFFFFFFFF7EDD
          9CFF019F0AFF019B03FF039401FF377201FF015401C900000000000000000E84
          13E53CBE51FFAAC9A7FFFAEAF9FFD1E1D1FF1FAF34FFDAEFDDFFFFFFFFFFFFFF
          FFFF7CDD9BFF019E08FF019A01FF2A7D01FF086801EE00000000000000001086
          14E45ED27DFF18A326FF8EBF88FF12AB25FF1CB941FF0CAD28FFDBEFDFFFFFFF
          FFFFFFFFFFFF7CDB99FF019E03FF1C8601FF086A01EE00000000000000000167
          01BE78DC95FF2DC55BFF20BB46FF2AC457FF27C050FF23BB46FF0CAC27FFDAEE
          DDFFFFFCFFFFFFFDFFFF79DB96FF148C01FF025901C70000000000000000013B
          017054C66DFF62D889FF2FCA63FF30C962FF2DC55AFF28C04EFF21B943FF0AAA
          23FFDAEDDDFFFFF9FFFFCAECD7FF0F8E09FF0136017900000000000000000109
          010C0D8913E483E5A7FF4DD37AFF2FCB64FF30C961FF2AC355FF23BB47FF1DB5
          3BFF0CA820FF7BCC85FF12AD25FF026F01E90108011000000000000000000000
          0000012A014022A22BF881E3A6FF5FD888FF32CA64FF28C354FF22BC48FF1EB6
          3CFF17B02FFF10A921FF038706F9012801470000000000000000000000000000
          000000000000012A01400E8A13E457CA74FF78E09CFF5AD37FFF44C867FF37C1
          55FF1FAA30FF067D06E801270146000000000000000000000000000000000000
          000000000000000000000109010C013D0170026A02BE128917E40E8615E50368
          02C0013A01730108010E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F81F0000F00F0000E0070000C003000080010000800100008001
          0000800100008001000080010000C0030000E0070000F00F0000F81F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000109010E015D01B5047C06E80132015A000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000109010D016E01C514AC28FF16B32CFF07920FFB013401590000
          0000000000000000000000000000000000000000000000000000000000000000
          00000109010C017101C217AE2FFF18B232FF15AD2BFF14B12AFF07900CFB0133
          0156000000000000000000000000000000000000000000000000000000000108
          010A017501C01AB234FF1DB63BFF19B335FF35BC4EFF18B031FF14B02BFF078F
          0CFA013101520000000000000000000000000000000000000000000000000972
          10B220B73FFF21BA43FF20B841FF12A825FF028202E963C772FF1AB233FF14B1
          2CFF078E0CF9012E014E000000000000000000000000000000000000000025A4
          34E943C866FF20BB47FF16B32DFF01630199010A010F017301C762C872FF1AB1
          32FF14B22DFF078D0CF8012C014A00000000000000000000000000000000074A
          0F6543C256FC37BC4FFF016C069C0101010100000000010C0110017501CC64CA
          73FF18B132FF15B12DFF078D0AF7012D01480000000000000000000000000000
          00000427072E053B0B4E01020102000000000000000000000000010D01120178
          02D064CA73FF18B132FF15B32EFF078B0AF60127014400000000000000000000
          000000000000000000000000000000000000000000000000000000000000010F
          0115017C01D465CB76FF16B132FF17B331FF037D05E800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001100117028004D867CD79FF2BBC47FF017303D300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000111011B017602CB047906D10119012F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFFF0000F8FF0000F07F0000E03F0000C01F0000801F
          0000820F000087070000FF830000FFC10000FFE10000FFF30000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009B7D
          6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFF9B7D6CFF9B7D6CFF997C
          69FF9B7D6CFF0000000000000000000000000000000000000000000000009B78
          67FFFFFFFFFFFAF4E9FFFAF4E9FFFAF3E8FFFAF3E7FFFAF2E6FFFAF1E4FFF8ED
          DAFF977A68FF000000000000000000000000000000000000000000000000A381
          71FFFFFFFFFFDBC3BBFFDBC2B8FFDBBFB3FFDBBDAFFFDABBAAFFDAB8A5FFFAF3
          E6FF997B6BFFB0818FFF975769FF965465FF975769FF0000000000000000A987
          79FFFFFFFFFFDBC7C2FFDBC4BCFFDBC1B7FFDBBFB2FFDBBCADFFDABAA8FFFBF4
          E8FF997C6CFFE3CFD0FFE3CCCDFFE2C2C2FF935364FF0000000000000000AB89
          7BFFFFFFFFFFDBC7C3FFDBC5BEFFDBC2B9FFDBC0B4FFDBBDAFFFDBBAA9FFFBF5
          EAFF9A7D6CFFD4AEB9FFCB90A1FFE3CFCEFF96566AFF0000000000000000AF8E
          80FFFFFFFFFFDCC5C0FFDBC4BDFFDBC2B9FFDBBFB4FFDBBDAFFFDBBAAAFFFBF5
          ECFF9B7D6CFFD5AFBAFFCB91A2FFE4D0D0FF965668FF0000000000000000AF8F
          81FFFFFFFFFFFEFEFEFFFEFDFDFFFEFCFBFFFDFBF8FFFDFAF6FFFCF9F3FFFCF6
          EDFF9B7D6DFFD5B1BCFFCC92A4FFE4D2D2FF975769FF0000000000000000AF8F
          81FFFFFFFFFFDFCECCFFDECAC6FFDEC6C0FFDEC4BAFFDEC1B4FFDEBEADFFF0E8
          E0FF9F8171FFD5B1BCFFCC94A5FFE4D4D5FF975769FF0000000000000000B595
          86FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFA78271FFA78271FFA782
          71FFA78271FFE5DEDFFFE4DBDBFFE5D5D6FF98586CFF0000000000000000B595
          86FFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFA78271FFF5E2D9FFB18E
          7FFFC39C9FFFD4AAB8FFCE98A7FFDBC7CAFF9A5C6FFF0000000000000000B898
          88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA78271FFB18E7FFFCFC3
          BDFFE6E2E2FFA25C72FFA25C72FFA25C72FFA25C72FF0000000000000000B898
          88FFB89888FFB49383FFB08E7EFFB08E7EFFAC8878FFA78271FFCFC3BDFFE6E5
          E4FFE6E1E1FFA25C72FFE0C1CAFFAA6A7FFF3920285800000000000000000000
          0000000000000000000000000000C194A3FFE6E6E6FFE6E6E6FFE6E6E6FFE6E6
          E6FFE6E6E6FFA25C72FFAA6A7FFF392028580000000000000000000000000000
          0000000000000000000000000000AF7385FFAF7385FFAC6E81FFA9697DFFA969
          7DFFA66377FFA25C72FF39202858000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000801F0000801F0000800100008001000080010000800100008001
          00008001000080010000800100008001000080010000F8030000F8070000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000A03010E6C2804CB7F2901D559170191380F015C1807
          0127020101020000000000000000000000000000000000000000000000000000
          00000000000000000000762401A754494AFF738699FF999CA1FFA1887CFFA873
          51FFA65A26F08D3601BD631B0189390F01541005011600000000000000000000
          00000000000049170167F77B01FE7C4C07FF576128FFA3735BFFAB8F90FFAEB3
          C4FFB8C4D7FFC0D3E5FFCFD6D3FFAC8765E11E0A013300000000000000000000
          0000230B012FD15F01F6F9A009FF025719FF2D6A38FF107217FF0B5A01FF2346
          01FF504F01FF82612EFFB47E6DFF7C6964A00301010300000000000000000A04
          010DA94801D9FFA209FF087306FF27412CFF8F7286FFB29AAEFFC0ACC3FFAEB0
          B7FF81AA8DFF4E9A60FF22872FFF1D6901FF012A016A00000000000000005E27
          0183FF9E06FF47750AFF1ECD5BFF018136FF01704BFF137A2DFF3E822FFF718E
          5FFFADA69DFFD9BFCEFFEBD0E7FFA29BA3BD010401090000000000000000A74A
          01B299850DFF0DB448FF1CB060FF235081FF5A7E96FF4389B6FF1983B5FF017C
          8DFF01694EFF016324FF17782BFF2F6C21CB0108011500000000000000002326
          01640AA435FF2FC95BFF0183C2FF1F4A68FF716867FFAE8D83FFC19F96FFCFAE
          A8FFBDBBC1FF8DB8D2FF5AA5D1FF3196C4FE013D6C9F00000000000000000174
          11C340DF65FF017D9EFF06D1FFFF0D9ED0FF01A5DEFF0191C9FF1284B0FF3B81
          9DFF6B8697FFA29EA0FFDDBBB2FFEEC9BAEE01080B0F00000000000000001BA6
          10EA0C8E86FF01BAFEFF14A6D4FF0BB4E5FF01B3E7FF01B2E6FF01B1E6FF01B0
          E6FF01A7E0FF0195CFFF0187C0FF1B86B0FC0122335200000000000000000145
          468901A7EDFF0DB4E5FF17B4E4FF05B7E9FF01B5E8FF01B1E7FF01ADE3FF01AB
          E0FF01ACE0FF01B1E5FF01A8DCFE014E71940103040500000000000000000179
          A8CD06C2F6FF2BB9E3FF0BBBEBFF75CEE6FF83D3EAFF81D6EDFF70D5F1FF4ECF
          F3FF01B7ECFF019CCFF701364F6100000000000000000000000000000000016D
          909813AADCF00DBFF1FE01B3E9FF0ABEEEFF1EC2EEFF3AC6EEFF5ACFEFFF08BE
          F0FF0188B9E3011F2C3600000000000000000000000000000000000000000000
          0000000000000110171A0125324201374E6A01527091016D94B90189B8E1016D
          96C1010E12170000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F87F0000F0030000E0030000E0030000C0010000800300008003
          00008001000080030000800100008003000080070000801F0000F83F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          010267270692983801E67D523AECA1A5A5F2B3ABA5F2C2C1BCF2C3C3C6F2AAAA
          AAF599A2A8FF772C02EB933704E6923A0FE34317027000000000000000007B36
          049FBE6418FFCD7A28FF9E8771FFB9875AFFCE710EFFDCAF7BFFFEFFFFFFFAFA
          FAFFD9E2EAFFAD7125FFDD9436FFECBE76FFA0420BF70000000000000000C969
          0EFFC07129FFC36D1FFFA7876CFFAE7D5BFFBA590AFFC69A6DFFD9DFE5FFF8F8
          F8FFF8FFFFFFA3641EFFD5872EFFE5AF6AFFA5470BFF0000000000000000CB6A
          0DFFBB6924FFBF671AFFB4906FFFB37D61FFAF4A01FFB5875FFFBFC6CDFFDEE0
          E2FFFFFFFFFFA56520FFD4842EFFE4B06DFFA4450AFF0000000000000000C96A
          0BFFB86420FFBC631AFFB08056FFC1BFBDFFB0ACA8FF98918BFF9A8E84FFA397
          8AFFAEA79FFF905920FFA76824FFDA9543FFA6460EFF0000000000000000C968
          0BFFB55E1CFFB65E18FFB85C10FFBA5909FFBE5E0BFFC2630FFFC56811FFC86A
          14FFC96F17FFCE7925FFD0802BFFD38933FFA5460FFF0000000000000000C869
          0AFFB05615FFB16A34FFAF7243FFAE7140FFAF7442FFB17441FFB07743FFB278
          42FFB47B44FFB47E47FFBD8142FFD1842EFFA6470FFF0000000000000000C766
          09FFAC4C0AFFB59C89FFE6F2FBFFE4EDF3FFE6EEF5FFE6EDF4FFE1E9EFFFDDE5
          ECFFD5DEE4FFD1DCE6FFAD9A8AFFD08027FFA4460FFF0000000000000000C866
          0AFFA84404FFB89984FFF4F9FCFFD5D4D2FFBEBDBCFFC0BFBEFFC1C0BFFFC2C0
          BFFFCBCAC9FFDFE3E6FFB19B85FFCF7D23FFA4450EFF0000000000000000C766
          09FFA23D01FFBA9B85FFFAFFFFFFF1F1F1FFECECECFFEBEBEBFFE8E8E8FFE4E4
          E4FFE1E1E1FFE2E5E9FFB29B88FFCD7A21FFA5450EFF0000000000000000C663
          08FF9E3801FFBC9C8AFFFFFFFFFFDDDCDBFFC2C1C0FFC4C3C2FFC4C3C2FFC2C1
          C0FFCECDCCFFE4E7EBFFB39D89FFCD7820FFA4450FFF0000000000000000C563
          07FF9A3201FFBF9D8AFFFFFFFFFFFBFBFBFFEEEEEEFFEFEFEFFFE9E9E9FFE4E4
          E4FFE4E4E4FFE6EAEDFFB5A089FFC2711EFFA9490EFF0000000000000000C05F
          06FA932901FFBE9D8DFFFFFFFFFFE5E4E3FFD0CFCEFFD1D0CFFFCFCECDFFCDCC
          CBFFD4D3D2FFE9EDF1FFB9A38CFF8A5015FFAE4F0FFA00000000000000006C3A
          0584BF5301FBC8A483FFD5D7D7FFD6D3D0FFD6D3D0FFD1CFCCFFCCCAC8FFC7C5
          C3FFC1BEBBFFBDBDBDFFB1947CFFBF5701FB5D2B068400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C001000080010000800100008001000080010000800100008001
          000080010000800100008001000080010000800100008001000080010000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFFBFAB
          A1FF017601FF017101FF016E01FFBFABA1FF0000000000000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFAF4E9FFFAF4E9FFFAF3E8FFFAF3E7FFFCF8
          F1FF017E01FF45DD78FF017301FFBDA99DFF0000000000000000000000000000
          0000A28070FFFFFFFFFFDDC2B5FFDDC2B5FFDCC2B5FFE9D6CDFF018701FF0185
          01FF018101FF49E17CFF017B01FF017601FF017101FF00000000000000000000
          0000A38171FFFFFFFFFFDBC3BBFFDBC3BAFFDBC2B8FFEBDCD5FF018D01FF5FF7
          91FF5BF38DFF54EC86FF49E17CFF46DE79FF017901FF00000000000000000000
          0000A98779FFFFFFFFFFDBC7C2FFDBC6C1FFDBC4BCFFEBDDD7FF019101FF018D
          01FF018B01FF5BF38DFF018301FF018101FF017E01FF00000000000000000000
          0000AB897BFFFFFFFFFFDBC7C3FFDBC7C2FFDBC5BEFFEBDED9FFEBDCD6FFEBDB
          D3FF018F01FF5FF791FF018901FFC8B7AEFF0000000000000000000000000000
          0000AF8E80FFFFFFFFFFDCC5C0FFDCC5BFFFDBC4BDFFDBC2B9FFDBBFB4FFEBDB
          D3FF019301FF019101FF018D01FFBFABA1FF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFFEFCFBFFFDFBF8FFFEFC
          FAFFFDFCF8FFFDFBF7FFFCF6EDFF9B7D6DFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFDFCECCFFDFCDCBFFDECAC6FFDEC6C0FFDEC4BAFFDEC1
          B4FFDEBEADFFDEBEABFFFCF6EEFF9C7D6EFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F8FFFCF9F4FFF9F4EEFFF0E8E0FF9F8070FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFA78271FFA78271FFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0030000C0030000C0010000C0010000C0010000C0030000C003
          0000C0030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000204
          86AF0B15AFFF867FB8FFD4C8C3FF2F2EB3FF2627B8FFB7ACC3FFA5887AFF9B7D
          6CFF9B7D6CFF9A7D6BFF997C69FF9B7D6CFF0000000000000000000000000303
          90D32D73FFFF1635D4FF2236C4FF184FFFFF165DFFFF373BC5FFFDF9F3FFFAF2
          E6FFFAF1E4FFF9EFE0FFF8EDDAFF977A68FF0000000000000000000000000101
          4B601326C5FF2D68FFFF265EFFFF215CFFFF1520BAFFD3C9D8FFDFC3B5FFDCBB
          A9FFDCBAA5FFDCBAA3FFFAF1E2FF987A69FF0000000000000000000000000000
          0000A79EC1FF1123BFFF2E67FFFF1D4AF4FF7572C4FFECDDD6FFDBBDAFFFDABB
          AAFFDAB8A5FFDCB9A5FFFAF3E6FF997B6BFF0000000000000000000000000000
          00002826A6FF407EFFFF1D40DDFF2A62FFFF1324C4FFD2CADAFFDEC4B8FFDBBC
          ADFFDABAA8FFDCBBA7FFFBF4E8FF997C6CFF0000000000000000000000000000
          0000161DC0FF3A6EFFFF8683CCFF3839B4FF2C6EFFFF2A2BBDFFEEE2DCFFDBBD
          AFFFDBBAA9FFDCBCA9FFFBF5EAFF9A7D6CFF0000000000000000000000000000
          0000A59BCAFF4343C4FFF0E6E4FFCAC3DAFF3332BBFFBDB8E5FFE7D4CCFFDBBD
          AFFFDBBAAAFFDDBBA9FFFBF5ECFF9B7D6CFF0000000000000000000000000000
          0000BCA194FFFFFFFFFFFEFEFEFFFEFEFEFFFFFEFEFFFEFDFCFFFDFBF8FFFDFA
          F6FFFCF9F3FFFCF7F0FFFCF6EDFF9B7D6DFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFDFCECCFFDFCDCBFFDFCBC7FFDEC6C0FFDEC4BAFFDEC1
          B4FFDEBEADFFDEBEABFFFCF6EEFF9C7D6EFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F8FFFCF9F4FFF9F4EEFFF0E8E0FF9F8070FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFA78271FFA78271FFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000800300008003000080030000C0030000C0030000C0030000C003
          0000C0030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFF9B7D
          6CFF9B7D6CFF9A7D6BFF997C69FF9B7D6CFF0000000000000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFAF4E9FFEEE9DEFFE8E2D8FFF7F0E4FFFAF2
          E6FFFAF1E4FFF9EFE0FFF8EDDAFF977A68FF0000000000000000000000000000
          0000A28070FFFFFFFFFFDDC2B5FFDDC2B5FFB5A9A4FFB1A19AFFDBC2B4FFDCBB
          A9FFDCBAA5FFDCBAA3FFFAF1E2FF987A69FF0000000000000000000000000000
          0000A38171FFFFFFFFFFDBC3BBFFEADDD7FF706E72FF928B96FFC3CDB9FFE8D6
          CCFFDAB8A5FFDCB9A5FFFAF3E6FF997B6BFF0000000000000000000000000000
          0000A98779FFFFFFFFFFDBC7C2FFE9DDDAFFA8BBCEFF35B357FF2DB445FF72B4
          70FFECDBD2FFDCBBA7FFFBF4E8FF997C6CFF0000000000000000000000000000
          0000AB897BFFFFFFFFFFDBC7C3FFE6D7D4FF99D0A7FF67FF98FF5BEC86FF2FAD
          47FF87BE81FFEAD8CCFFFBF5EAFF9A7D6CFF0000000000000000000000000000
          0000AF8E80FFFFFFFFFFDCC5C0FFDEC9C3FFDBE4D6FF58E280FF6BFF9DFF56E1
          7DFF2BA43DFF9CC494FFFCF8F2FF9B7D6CFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFFFEFEFEFFFEFDFDFFD1F8DCFF55EE83FF69FF
          9BFF51DC78FF259E39FFB7DCB6FFB69F94FF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFDFCECCFFDFCDCBFFDECAC6FFE9D9D5FFADEAB9FF5BF6
          88FF65FF97FF43DA6AFF35873FFFC6BDB6FF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFF91EF
          ACFF56FC88FF9AC1A4FFCDBBB6FF6F6E8CFF0102121800000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFBFC
          FAFFA5C5A6FFFFFFFFFF7992F5FF213EDCFF010379C200000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFDED0C9FF8C99DEFF4378FFFF2E4BD8FF020382D100000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB39384FFC1B9D0FF3142C1F7080D83C90101162500000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF4D423C6800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0030000C0030000C0030000C0030000C0030000C0030000C003
          0000C0030000C0030000C0030000C0010000C0010000C0030000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFFC4B2A7FF434CC2FF0817BEFF0812B7FF080F
          B1FF080FACFF0808A3FF08089FFF08099EFF010172BB00000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFDFAF5FF3148DCFF011EE8FF4367F1FF284C
          ECFF010FD4FF2B4CE2FF627CECFF0103B6FF010198F800000000000000000000
          0000A28070FFFFFFFFFFDDC2B5FFEFE2DCFF3450E5FF3257F2FFF4EFDEFFEBEC
          EBFF3B5FF3FFF8FCFCFFFFFFFFFF637FEFFF010198F800000000000000000000
          0000A38171FFFFFFFFFFDBC3BBFFEEE3DEFF405DECFF1B4AF8FFE7E5E1FFF1F0
          EAFFFFFDF3FFFFFFFEFFFFFFFFFF3C5BE9FF01019EF800000000000000000000
          0000A98779FFFFFFFFFFDBC7C2FFEEE4E2FF4E6CEFFF0140FFFF2A53F3FFF7F5
          EBFFF4F4F4FFFFFFFFFF4165F5FF010ED3FF0109A7F800000000000000000000
          0000AB897BFFFFFFFFFFDBC7C3FFEEE4E2FF587AF2FF124AEEFFD4D4DFFFF2F0
          EAFFFFFCF3FFFFFFFDFFFBFEFFFF1E44EBFF0109ACF800000000000000000000
          0000AF8E80FFFFFFFFFFDCC5C0FFEEE3E1FF5F83F6FF4763D3FFF8F2DEFFE8E6
          E6FF3A5FF2FFFDFBF7FFFFFFFFFF6281FCFF010BB6F800000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFFFFFFFFFF7292FAFF1071FFFF506AD4FF2050
          EAFF013FFFFF204CF9FF5474F8FF011AE6FF0112BCF800000000000000000000
          0000AF8F81FFFFFFFFFFDFCECCFFECE1DFFF6783F3FF6B8FFAFF587EF7FF4F74
          F6FF496BF1FF3B5CEFFF324FE9FF2D47DEFF0C1B92B600000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFFFEFD
          FCFFFEFCFAFFFCFAF7FFF8F4F0FFD0C1B9FF0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFA78271FFA78271FFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0010000C0010000C0010000C0010000C0010000C0010000C001
          0000C0010000C0010000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001110127013A018AAC9989FF9D7F6EFF9C7F
          6EFF9C7E6EFF9B7D6CFF9B7D6CFF997C69FF9B7D6CFF00000000000000000000
          00000000000001020103013F018B087C0BFB0138017CB4998CFFFFFFFFFFFAF4
          E9FFFAF3E8FFFAF2E6FFFAF1E4FFF8EDDAFF977A68FF00000000000000000000
          0000000000000145018E21B432FF17A020FF01140126A28070FFFFFFFFFFFCF6
          EDFFFBF5ECFFFBF4EAFFFBF3E8FFFAF1E2FF987A69FF00000000000000000000
          00000109010F13961AF93EDC5DFF179E21FF0107010DA98779FFFFFFFFFFFDF9
          F4FFFCF8F2FFFCF7EFFFFBF6EDFFFBF4E8FF997C6CFF0000000000000000C6B5
          ABFF9FAE8AFF27B73BFF4BE66FFF1DAB2BFFB6B29EFFD0BCB4FFFFFFFFFFFDFB
          F7FFFDFAF5FFFCF8F2FFFCF7EFFFFBF5EAFF9A7D6CFF00000000000000006FA7
          5FFF39A439FF2EC145FF58F183FF30C347FF31A52DFF619447FFFFFFFFFFFEFE
          FEFFEEF6EBFF58B656FFE8F2E0FFFCF6EDFF9B7D6DFF0000000000000000C1B5
          A6FF3EB441FF5DF68AFF6AFF9FFF5CF387FF49B546FFC0AB9CFFFFFFFFFFF5FC
          F5FF3DB33CFF5BF385FF34B132FFE2E5D3FF9F8171FF0000000000000000A987
          79FFF0FBF0FF34B133FF5BF385FF3CB238FFF1F3E4FFB79787FFFAFDFAFF4AB8
          4BFF5CF387FF6AFF9FFF5DF68AFF2E9E28FFC4B7A6FF0000000000000000AB89
          7BFFFFFFFFFFE9F4E4FF58B656FFECF2E3FFFCF7EFFFB89888FF8CD28CFF32A8
          32FF30C347FF58F183FF2EC145FF2C9124FF215D17A20000000000000000AF8F
          81FFFFFFFFFFFEFEFEFFFEFCFBFFFDFAF6FFFCF9F3FFD7C4BBFFD7C4BBFFD5D6
          C6FF1DAB2BFF4BE66FFF27B73BFF2F402079000000000000000000000000B190
          81FFFFFFFFFFFFFFFFFFFEFEFEFFFDFBF8FFFCF9F4FFF0E8E0FF9F8171FF0107
          010D179E21FF3EDC5DFF139519F90109010F000000000000000000000000B797
          87FFFFFFFFFFFFFFFFFFFEFEFEFFFDFBF9FFA78271FFA78271FFA78271FF0114
          012617A020FF21B432FF0145018E00000000000000000000000000000000B898
          88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA78271FFB18E7FFF3A2E28580138
          017C087B0BFB013F018B0102010300000000000000000000000000000000B898
          88FFB89888FFB49383FFB08E7EFFAC8878FFA78271FF3A2E28580105010C013A
          018A011101270000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FC010000F0010000E2010000E201000080010000800100008001
          00008001000080010000800300008047000080470000800F000080BF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000B49C90FFD3C6BFFF716B66FF72635BFF746B68FFC7B5ACFFC7B5
          ABFFC3B1A8FFC1AFA6FFC6B4ABFF997C69FF9B7D6CFF00000000000000000000
          000000000000D5C5BDFF7D7D80FFD9AF8BFFFFD19BFFC7A386FFCDC8C0FFDBD6
          CDFF7F716AFF81756AFFC0BCB6FFFBF4EAFF977A68FF00000000000000000000
          000000000000695D58FF717375FFEECDB1FFFFF5DBFFFFE1B4FF606263FF6867
          67FFFFDAA4FFFFD9A2FF867D74FFFCF7EEFF987A69FF00000000000000000000
          00002D2D2DAA696969FFABACACFFAA9F9EFFF7E5DDFFBFA593FF5D6063FF9A99
          99FFFFEDD7FFFFFBDFFF887C6EFFFCF8F1FF997B6BFF00000000000000001F1F
          1F8BB5B5B5FF6C6C6CFFCDCECEFFFEFFFFFF676666FF5F6163FF828283FFECEF
          EFFFB1A6A6FF877D78FFB1ADA8FFFDF9F2FF997C6CFF00000000000000003D3D
          3DF7B5B5B5FFD5D5D5FF979797FF6B6B6BFF4B4C4CFF3B3B3BFF999999FFD7D8
          D8FF878888FFCDCBC7FFFCF8F1FFFBF5EAFF9A7D6CFF00000000000000003939
          39AE747474FFABABABFFB6B6B6FFAEAEAEFFA9A9A9FFAAAAAAFF7A7A7AFF8382
          81FFF8F5F2FFFDFAF4FFFCF7EFFFFBF5ECFF9B7D6CFF00000000000000000202
          02041212122FC9BEB8FFBFBFBFFF969696FF6A6A6AFF8B8B8BFFE1E0DFFFFDFC
          FAFFFDFBF8FFFCF9F3FFFCF7F0FFFCF6EDFF9B7D6DFF00000000000000000000
          000000000000AF8F81FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFDFFFEFC
          FAFFFDFBF7FFFDF9F4FFFCF8F1FFFCF6EEFF9C7D6EFF00000000000000000000
          000000000000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFD
          FBFFFDFBF8FFFCF9F4FFF9F4EEFFF0E8E0FF9F8070FE00000000000000000000
          000000000000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFD
          FBFFFDFBF9FFA78271FFA78271FFA78271FFA78271FF00000000000000000000
          000000000000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFC
          FAFFFDFBF9FFA78271FFF5E2D9FFB18E7FFF3A2E285800000000000000000000
          000000000000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA78271FFB18E7FFF3A2E28580000000000000000000000000000
          000000000000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC88
          78FFAC8878FFA78271FF3A2E2858000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000E0010000E0010000E0010000C001000080010000800100008001
          0000E0010000E0010000E0010000E0010000E0010000E0030000E0070000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFF9B7D
          6CFF9B7D6CFF9A7D6BFF997C69FF9B7D6CFF0000000000000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFCF8F1FFFAF4E9FFFAF3E8FFFAF3E7FFFAF2
          E6FFFAF1E4FFF9EFE0FFF8EDDAFF977A68FF0000000000000000000000000000
          0000A28070FFFFFFFFFFE9D8D0FF399F34FFEBDCD5FFDCBFB1FFDCBEAEFFDCBB
          A9FFDCBAA5FFDCBAA3FFFAF1E2FF987A69FF0000000000000000000000000000
          0000CCB9B0FFFFFFFFFFECDFDBFF069006FF53A84DFFEADAD3FFDBBDAFFFDABB
          AAFFDAB8A5FFDCB9A5FFFAF3E6FF997B6BFF0000000000000000000000000289
          03F40E940FFF109712FF139815FF2EC64AFF129C1AFF4EA648FFEBDBD4FFDBBC
          ADFFDABAA8FFDCBBA7FFFBF4E8FF997C6CFF000000000000000000000000058E
          07F84FE781FF4FE781FF4FE781FF4FE781FF4DE57FFF15A01EFF3AA036FFE8D5
          CCFFDBBAA9FFDCBCA9FFFBF5EAFF9A7D6CFF000000000000000000000000068F
          08F857EF89FF57EF89FF57EF89FF57EF89FF55ED87FF16A121FF379F33FFE8D5
          CCFFDBBAAAFFDDBBA9FFFBF5ECFF9B7D6CFF0000000000000000000000000380
          03E4159B1AFF189F1EFF169C1BFF37CF55FF16A321FF3FA73FFFFEFDFBFFFDFA
          F6FFFCF9F3FFFCF7F0FFFCF6EDFF9B7D6DFF0000000000000000000000000000
          0000D2C1B8FFFFFFFFFFEEE5E4FF059006FF4FA74CFFECDFDBFFDEC4BAFFDEC1
          B4FFDEBEADFFDEBEABFFFCF6EEFF9C7D6EFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFF49AC4AFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F8FFFCF9F4FFF9F4EEFFF0E8E0FF9F8070FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFA78271FFA78271FFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0030000C0030000C0030000C003000080030000800300008003
          000080030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFF9B7D
          6CFF9B7D6CFF9A7D6BFF997C69FF9B7D6CFF0000000000000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFAF4E9FFFAF4E9FFFAF3E8FFFAF3E7FFFAF2
          E6FFFAF1E4FFFBF5EBFFF8EDDAFF977A68FF0000000000000000000000000000
          0000A28070FFFFFFFFFFDDC2B5FFDDC2B5FFDCC2B5FFDCBFB1FFDCBEAEFFDCBB
          A9FFEAD5C8FF399E32FFFCF7EFFFA58A7DFF0000000000000000000000000000
          0000A38171FFFFFFFFFFDBC3BBFFDBC3BAFFDBC2B8FFEBDCD5FFEBDBD3FFEBDA
          D0FFEBD9CFFF068F06FF59B156FFC5B3AAFF0000000000000000000000000000
          0000A98779FFFFFFFFFFDBC7C2FFDBC6C1FFEBDFDAFF0C920CFF0F9510FF0F95
          10FF139814FF2EC64AFF129C1BFF429A3BFF0000000000000000000000000000
          0000AB897BFFFFFFFFFFDBC7C3FFDBC7C2FFECE0DCFF0B940DFF4FE781FF4FE7
          81FF4FE781FF4FE781FF4DE57FFF149F1EFF016B02C100000000000000000000
          0000AF8E80FFFFFFFFFFDCC5C0FFDCC5BFFFECDFDBFF0C950EFF57EF89FF57EF
          89FF57EF89FF57EF89FF55ED87FF16A121FF016E02C500000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFFFEFEFEFFFEFEFEFF1E9B1EFF179E1EFF179E
          1EFF159C1BFF37CF55FF16A321FF32972DFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFDFCECCFFDFCDCBFFDECAC6FFECDEDAFFEDDED9FFEDDC
          D5FFEEDDD4FF059005FF54AF53FFC9B7AFFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F8FFFDFBF8FF48AA47FFF6F2EDFFAB9183FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFB29283FFC8B1A6FFB29283FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0030000C0030000C0030000C0030000C0030000C0010000C001
          0000C0030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFFB0988AFF9C7F6EFF9C7F6EFFB0978AFFC0ACA1FFBFAB
          A1FFAF9689FF9A7D6BFF997C69FF9B7D6CFF0000000000000000000000000000
          00009B7867FFFFFFFFFFD88F44FFFAF3E8FFDFBD9DFFC48053FFBA6B35FFB86B
          3DFFCC9577FFF5E7D6FFF8EDDAFF977A68FF0000000000000000000000000000
          0000A28070FFFFFFFFFFCA6D13FFC07224FFD87501FFE88301FFD97301FFB753
          02FFBA5E27FFD6A182FFFAF1E2FF987A69FF0000000000000000000000000000
          0000A38171FFFFFFFFFFD67C1AFFFEA62DFFFFA209FFDE7901FFC67D47FFF3DB
          C7FFFBF5EBFFFBF4E9FFFAF3E6FF997B6BFF0000000000000000000000000000
          0000A98779FFFFFFFFFFD77C21FFFFC56EFFFBA635FFC56F23FFFAF0E7FFFCF7
          EFFFFBF6EDFFFBF5EBFFFBF4E8FF997C6CFF0000000000000000000000000000
          0000AB897BFFFFFFFFFFD47F2AFFD88128FFD97E21FFD4781AFFD37515FFD97F
          22FFD98129FFD47F29FFFBF5EAFF9A7D6CFF0000000000000000000000000000
          0000AF8E80FFFFFFFFFFFEFCFBFFFEFCFAFFFDFCFAFFFBF3EDFFC66F24FFFBA6
          35FFFFC56EFFD87D21FFFBF5ECFF9B7D6CFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFFFEFEFEFFF5E2D5FFC77F4BFFDE7901FFFFA2
          09FFFEA62DFFD77C1BFFFCF6EDFF9B7D6DFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFD9A990FFBB602BFFB85302FFD97301FFE88301FFD875
          01FFC17325FFCB6D13FFFCF6EEFF9C7D6EFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFBF6F4FFCF9D84FFBA6F44FFBB6E3AFFC6835AFFE2C2
          A7FFFCF8F3FFD98F46FFF0E8E0FF9F8070FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFBFFFDFB
          F9FFA78271FFB99B8DFFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFA78271FFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0030000C0030000C0030000C0030000C0030000C0030000C003
          0000C0030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00009B7D6CFF9D7F6EFF9C7F6EFF9C7F6EFF9C7F6EFF9C7E6EFF9C7E6DFF9B7D
          6CFF9B7D6CFFBEABA0FF7B8FC7FF918981FF3C3A367E00000000000000000000
          00009B7867FFFFFFFFFFFAF4E9FFFAF4E9FFFAF4E9FFFAF3E8FFFAF3E7FFFAF2
          E6FFFAF1E4FF9EB8EEFF3A83FCFF80DEFFFF8D867CEC00000000000000000000
          0000A28070FFFFFFFFFFFCF6EDFFFCF6EDFFFBF6EDFFFBF5ECFFFBF5EBFFFBF4
          EAFF9CB6E7FF3582FDFF72E1FFFF3687FAFF1237818200000000000000000000
          0000A38171FFFFFFFFFFFCF8F1FFFCF8F1FFA7A5A3FF7E7F7FFF777675FF8080
          7EFF918F8CFF8CC5D6FF3788FEFF7992D0FF0000000000000000000000000000
          0000A98779FFFFFFFFFFFDF9F4FFA8A7A6FFBFAD93FFFFD184FFFED085FFFED6
          98FF9A948BFF908F8CFF97B4EBFFBDAAA1FF0000000000000000000000000000
          0000AB897BFFFFFFFFFFFDFCF8FF82817EFFFFDD97FFF2D598FFEFCD90FFF1C8
          82FFFED99DFF858584FFFBF5EAFF9A7D6CFF0000000000000000000000000000
          0000AF8E80FFFFFFFFFFFEFDFCFF7C7974FFFFEEBDFFF8E8BBFFF4DDA9FFF0D0
          94FFFFD590FF7B7B79FFFBF5ECFF9B7D6CFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFFEFEFEFF84827EFFFFFFEAFFFFFEEFFFF9EDC2FFF6DA
          A0FFFFDA96FF848383FFFCF6EDFF9B7D6DFF0000000000000000000000000000
          0000AF8F81FFFFFFFFFFFFFFFFFFAAAAABFFDEDAC4FFFFFFF2FFFFF7CBFFFFE7
          A6FFB7AB99FFAAA8A6FFFCF6EEFF9C7D6EFF0000000000000000000000000000
          0000B19081FFFFFFFFFFFFFFFFFFFFFFFFFFABABABFF82817FFF7C7A76FF8181
          7FFFABAAA8FFF9F4EEFFF0E8E0FF9F8070FE0000000000000000000000000000
          0000B79787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFDFCFFFDFC
          FAFFC5ACA0FFB08E7FFFA78271FFA78271FF0000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFDFFFEFCFAFFFDFB
          F9FFB08E7FFFF5E2D9FFB18E7FFF3A2E28580000000000000000000000000000
          0000B89888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA78271FFB18E7FFF3A2E2858000000000000000000000000000000000000
          0000B89888FFB89888FFB49383FFB49383FFB08E7EFFB08E7EFFAC8878FFAC88
          78FFA78271FF3A2E285800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C0010000C0010000C0010000C0030000C0030000C0030000C003
          0000C0030000C0030000C0030000C0030000C0030000C0070000C00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000107
          56760716B0ED010CACF2010BA9F2010AA4F20108A0F201069DF2010499F20104
          96F2010194F2010294F2010293F2010190ED0101467300000000000000001124
          BDF02542E0FF011FD7FF011AD3FF0627DBFF0115CBFF0117C8FF0116C3FF010C
          BCFF0A22C7FF0107B1FF010BAEFF0105A2FF010191EF0000000000000000182E
          C9F5274BEEFF0121E6FF0634F2FFB0C1EFFF1742F1FF011AD8FF0115D2FF1D41
          E5FFCFE1FFFF2245E1FF0108B8FF010AABFF010295F500000000000000001D35
          CDF52A50F7FF0129F8FFC0C7E4FFF1EDE0FFD9DFECFF103CF1FF0D35EBFFE2EB
          FCFFFFFFFBFFEFF8FFFF2144E0FF0105ADFF010295F500000000000000002038
          D3F52953FFFFA0ACE2FFEEEADDFFE4E4E3FFF1EFE9FFD4DCF3FFCFD9F8FFFFFF
          FAFFFFFFFFFFFFFFFFFFDEEDFFFF1028C8FF010195F50000000000000000223F
          D5F53865FFFF0435F7FFD7D7E0FFECEBE5FFEDECEBFFF4F4F1FFFBFAF7FFFEFE
          FDFFFFFFFFFFF8FDFFFF2F55ECFF0109B7FF01059CF500000000000000002843
          D9F54574FFFF013EFFFF0638F8FFD2D6E8FFEEEEEBFFF1F1F1FFF9F9F9FFFFFF
          FEFFEAEFFFFF1E47EFFF0113D1FF0113BFFF01089FF500000000000000002A4A
          DEF54E81FFFF064CFFFF012DF6FFB0BAE8FFF0EFEBFFF1F1F1FFF7F7F7FFFFFF
          FEFFD4DFFFFF022EEDFF011AD8FF0116C5FF010AA5F500000000000000002E4F
          E1F5578EFFFF0132EBFFB6B8DCFFEEEDE4FFEBEBEAFFF3F3F0FFF9F9F6FFFEFE
          FDFFFFFFFFFFE0EAFFFF0834EDFF0114C7FF010CA8F500000000000000003455
          E6F55078ECFF9A98CBFFEDEBDEFFE5E5E3FFF1F0E9FFD3D7EEFFDFE3F4FFFFFF
          F9FFFFFFFEFFFFFFFFFFD0DEFFFF0324D8FF010CADF500000000000000003759
          E9F568A4FFFF1036D2FFD8D4D8FFEEEDE3FFD9D8E4FF0234F0FF0739F6FFEEEE
          F5FFFFFFF9FFFAFBFFFF1D49F7FF0117CFFF0110B4F50000000000000000395C
          EBF56EABFFFF126FFFFF1237D3FFBDB9D4FF0734E3FF054BFFFF013DFFFF0635
          F5FFD6DCF5FF0D3CF9FF011EE6FF011CD3FF0111B7F500000000000000002C53
          E9EFBAE0FFFF5DA0FFFF579AFFFF3B61E4FF4684FFFF4077FFFF376AFFFF2A5B
          FFFF1A45F9FF1F46F6FF1E41EDFF1E39DBFF091DB9EE00000000000000000111
          6C6E274EE0E53455E2EA3152E0EA2F51DCEA2B49DBEA2746D7EA2440D6EA1F3C
          D1EA1C39CFEA1933CCEA152FC6EA0F26BCE50109556C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF00008001000080010000800100008001000080010000800100008001
          000080010000800100008001000080010000800100008001000080010000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000102131901045987040888D30B0D9DF70B0B98F70405
          80D3010151860101101800000000000000000000000000000000000000000000
          000000000000010648580313B6F10B27D4FF0F2BD5FF1529CBFF2029B6FF3328
          97FF2A1F94FF090893F101013D57000000000000000000000000000000000000
          000001074C590120D7FD0133FFFF0131FAFF012EF8FF012CF4FF012AEFFF0126
          E8FF1E26B8FF3A2889FF130F99FD01013D570000000000000000000000000103
          161A0822C9F2083DFFFF073AFFFF0639FEFF0437FFFF0234FFFF0130F9FF012D
          F2FF0128EFFF1026CAFF3B298AFF090993F1010111190000000000000000010C
          6C882752FBFF0F43FFFF0F41FFFF0C40FFFF083BFFFF0237FFFF0131FFFF012B
          FEFF0125F5FF0120F0FF1C24B4FF2B2194FF0101528700000000000000000720
          B6D63D6BFFFF1245FFFF5578F2FF5F81F7FF6789F9FF6E8FFAFF7392FDFF7293
          FFFF7593FFFF7B99FEFF4D72F9FF342793FF050785D300000000000000001737
          DFFA4D78FFFF1643F6FFF3ECD2FFEBE8DEFFF5F2E8FFFCF9F0FFFFFFF8FFFFFF
          FEFFFFFFFFFFFFFFFFFF4E76FEFF2126B1FF0D0F9EF80000000000000000183C
          E2FA678CFFFF1036E7FFF6F1D8FFF0EDE1FFFAF7EAFFFFFFF3FFFFFFFDFFFFFF
          FFFFFFFFFFFFFFFFFFFF4F77FFFF1528C8FF0C11A5F900000000000000000927
          C2D684A5FFFF2445E0FF3F53D1FF3C57E1FF3B5BEBFF3B60F3FF3E64F9FF4167
          F9FF4469F9FF4B70F9FF184AFFFF132DD3FF060C8FD300000000000000000110
          7A897090FDFF6E94FFFF4677FFFF4676FFFF3F6EFFFF3565FFFF295BFFFF1D50
          FFFF1245FFFF083CFFFF0136FFFF112CD3FF0107618800000000000000000105
          1B1B1B40E6F297B4FFFF5D86FFFF4878FFFF4575FFFF3C6AFFFF3060FFFF2556
          FFFF1A4CFFFF0F43FFFF063AFFFF0519BCF20102141A00000000000000000000
          0000010F5C5B365CF3FD95B3FFFF6E91FFFF4776FFFF3B6BFFFF3061FFFF2657
          FFFF1B4CFFFF1146FFFF0426DEFD01074D590000000000000000000000000000
          000000000000010F5C5B1B42E6F27494FCFF86A7FFFF658AFFFF4C77FFFF3C6A
          FFFF2553FAFF0824D2F201095059000000000000000000000000000000000000
          0000000000000000000001051C1B01117D890B2BC4D61C41E7FA193DE5FA0824
          BCD6010F71880103171A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F81F0000E0070000C0030000C003000080010000800100008001
          0000800100008001000080010000C0030000C0030000E0070000F81F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000606060D37373792606060E09B9B
          9BCF017601FF017101FF016E01FF000000000000000000000000000000000000
          0000000000000000000000000000000000006B6B6BAAECECECFFD5D5D5FFD0D0
          D0F3017E01FF45DD78FF017301FF000000000000000000000000000000000000
          000000000000000000000000000000000000919191EAE9E9E9FF018701FF0185
          01FF018101FF49E17CFF017B01FF017601FF017101FF00000000000000000000
          0000000000000000000000000000000000009A9A9AF6E6E6E6FF018D01FF5FF7
          91FF5BF38DFF54EC86FF49E17CFF46DE79FF017901FF00000000000000000000
          000000000000000000000000000012121228909090FEE2E2E2FF019101FF018D
          01FF018B01FF5BF38DFF018301FF018101FF017E01FF00000000000000000000
          000000000000000000000808080D878787D5F5F5F5FFE9E9E9FFE0E0E0FFDBDB
          DBFF018F01FF5FF791FF018901FF747474730000000000000000000000000000
          00000000000001010101686868ADF6F6F6FFDCDCDCFFD0D0D0FFB1B1B1FFD1D1
          D1FF019301FF019101FF018D01FF000000000000000000000000000000000000
          0000000000004B4B4B7AF7F7F7FFEEEEEEFFDBDBDBFFD4D4D4FFB9B9B9FFD6D6
          D6FFD6D6D6FFCFCFCFFF5A5A5A92000000000000000000000000000000000000
          00002F2F2F48E7E7E7FCE3E3E3FFB7B7B7FFA1A1A1FF9E9E9EFF989898FF8686
          86FF676767FF525252FF4A4A4AFB181818440000000000000000000000001A1A
          1A22969696ED6C6C6CFF585858FF6E6E6EFF979797FFC3C3C3FFE0E0E0FFF6F6
          F6FFFFFFFFFFFAFAFAFFAEAEAEFF4B4B4BEB0909092000000000000000006A6A
          6ABE313131FF4E4E4EFF6A6A6AFF858585FFA2A2A2FFC3C3C3FFD4D4D4FFE5E5
          E5FFF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFF545454C700000000000000006F6F
          6FD43D3D3DFF5F5F5FFF787878FF8E8E8EFFA7A7A7FFC5C5C5FFD3D3D3FFE1E1
          E1FFECECECFFF8F8F8FFFFFFFFFFFFFFFFFF7A7A7AE100000000000000002A2A
          2A2E8F8F8FD47B7B7BFF797979FF939393FFADADADFFC9C9C9FFD7D7D7FFE7E7
          E7FFF5F5F5FFFFFFFFFFF2F2F2FF868686DA0D0D0D3600000000000000000000
          0000020202023C3C3C456767678F7E7E7EC0949494DFA3A3A3EEA1A1A1EE9595
          95E0767676C24444449216161649020202030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FE070000FC070000FC010000FC010000FC010000F8030000F007
          0000E0070000E0070000C00300008001000080010000C0030000F00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000204
          86AF0510AAF801013E610101030401038AC91C20B1EF9494B1D96F6F6FE32C2C
          2C9A000000000000000000000000000000000000000000000000000000000303
          90D32D73FFFF1534D3FE1226B5EF184FFFFF165DFFFF363AC5FFD4D4D4FF4747
          47D2000000000000000000000000000000000000000000000000000000000101
          4B601022C2FB2D68FFFF265EFFFF215CFFFF1520BAFFD7D8ECFFA6A6A6FF4D4D
          4DDE000000000000000000000000000000000000000000000000000000000000
          0000202046601123BFFF2E67FFFF1D4AF4FF6E6EC2FEE7E7E7FF919191FF5353
          53EA000000000000000000000000000000000000000000000000000000000000
          0000010183D2407EFFFF1C3FDCFE2A62FFFF1223C3FFC4C5D9FF929292FF5A5A
          5AF90D0D0D210000000000000000000000000000000000000000000000000000
          00000A12B6F23A6EFFFF6A6AB3DE3335B1FD2C6EFFFF292CBEFFD9D9D9FF7474
          74FF474747CF0505050A00000000000000000000000000000000000000000000
          0000010135423131B3EEC0C0C0DDD3D3EDFF3333BDFFBBBBEAFFCACACAFF8484
          84FF656565FF3B3B3BA600000000000000000000000000000000000000000000
          000000000000A6A6A6BDF7F7F7FFF0F0F0FFEEEEEEFFE2E2E2FFB9B9B9FF9191
          91FF737373FF5C5C5CFE2B2B2B73000000000000000000000000000000000000
          00002F2F2F48E7E7E7FCE3E3E3FFB7B7B7FFA2A2A2FF9E9E9EFF989898FF8686
          86FF676767FF525252FF4A4A4AFB181818440000000000000000000000001A1A
          1A22969696ED6C6C6CFF585858FF6E6E6EFF979797FFC3C3C3FFE0E0E0FFF6F6
          F6FFFFFFFFFFFAFAFAFFAEAEAEFF4B4B4BEB0909092000000000000000006A6A
          6ABE313131FF4E4E4EFF6A6A6AFF858585FFA2A2A2FFC3C3C3FFD4D4D4FFE5E5
          E5FFF2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFF545454C700000000000000006F6F
          6FD43D3D3DFF5F5F5FFF787878FF8E8E8EFFA7A7A7FFC5C5C5FFD3D3D3FFE1E1
          E1FFECECECFFF8F8F8FFFFFFFFFFFFFFFFFF7A7A7AE100000000000000002A2A
          2A2E8F8F8FD47B7B7BFF797979FF939393FFADADADFFC9C9C9FFD7D7D7FFE7E7
          E7FFF5F5F5FFFFFFFFFFF2F2F2FF868686DA0D0D0D3600000000000000000000
          0000020202023C3C3C456767678F7E7E7EC0949494DFA3A3A3EEA1A1A1EE9595
          95E0767676C24444449216161649020202030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000883F0000803F0000803F0000C03F0000C03F0000C01F0000E00F
          0000E0070000E0070000C00300008001000080010000C0030000F00F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000B0B0B143535357E535253CA656365EC5D5B5DEB4141
          41C4242424740505050E00000000000000000000000000000000000000000000
          00000000000030303051818081EEB0A6AFFFD5C2D3FFC8C6C7FFC8C3C9FFC7B5
          C5FF898188FF535153E618181840000000000000000000000000000000000000
          000035353551A4A0A3FCDDCEDAFF7CC785FF13B029FF01A210FF01A00DFF1FB3
          2DFF95D099FFBBAAB9FF636162F71616163D0000000000000000000000000F0F
          0F159E9C9EEEE5D6E3FF38B84DFF08AF23FF14AA2DFF149B2AFF139926FF0EA6
          22FF01A50DFF5EC767FFBDACBAFF535153E10404040A00000000000000005150
          5180DDD2DBFF6FBE7EFF0EB42FFF1EB63CFF1CB13BFFFFFFFFFFFFFFFFFF16AB
          2DFF14AC28FF01A40DFF9CCFA0FF8F878EFF2121216700000000000000008D8C
          8DCCECDBE8FF13B636FF20BA42FF22BA45FF21B542FF1DA43BFF199E35FF18A6
          32FF17AC2DFF0DAA20FF2FB73DFFCABAC9FF3D3D3DB40000000000000000B8B6
          B7F1CAD1CCFF0EBA38FF27BE4EFF26B84CFF24B449FFE5F6E9FFE6F5EAFF2FA6
          46FF17A22FFF15AA2AFF01A70EFFE2CFDFFF5A5A5AD80000000000000000BEBB
          BDEFCBD1CCFF13BF41FF2ABE54FF27AD50FF26A74AFF39B259FFBEE5C8FFC8E6
          CFFF55B568FF16A92DFF01A811FFE6D3E3FF5E5E5ED700000000000000009B9A
          9BC8F2E1EDFF1EBE4BFF2DC55DFFE7F7ECFFECF8F0FF5FBE7AFF4AB265FFF9FC
          FAFFF7FCF8FF12AC2BFF2DB540FFE1D2DFFF4C4C4CAF00000000000000005C5C
          5C78FBF1F8FF6EBA84FF24C959FF61D588FFE3F7E9FFFFFFFFFFFFFFFFFFF2FA
          F4FF7AD28BFF02AC1DFF9BCAA1FFBFB6BDFF2B2B2B5F00000000000000000E0E
          0E10C9C7C8E8FBEBF6FF3EBA64FF21C858FF2BC75DFF2AC255FF26BE4CFF1DB9
          40FF08B328FF59B96AFFEFDEEDFF7B797BD90404040600000000000000000000
          000041414144E1DEE0F8FFEEF9FF76BB8AFF23BB4EFF10BC3DFF0AB933FF23B3
          41FF8FC397FFF5E5F2FFA19EA1F2202020320000000000000000000000000000
          0000000000003F3F3F41C3C1C3E4FEF5FBFFFEECFAFFE4DCE1FFE7DDE5FFFCEA
          F8FFE2D9E0FF939293DA24242432000000000000000000000000000000000000
          000000000000000000000B0B0B0B5353536B8E8E8EB6AFAFAFDAA9A9A9D97B7B
          7BB1424242610505050700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F81F0000E00F0000C0070000C003000080010000800100008001
          0000800100008001000080010000C0030000E0070000F00F0000F81F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000454545879D9D9DF2929292E57E7E7ED36A6B6CC05A59
          59AE95612EBB492102723812015C0A0E10460404040D00000000000000000000
          00000808080D6B6B6BB8C0C0C0FEEBEBEBFEF3F3F3FFEEEEEEFFEBECEEFFDFD9
          D9FFE28E1EFFEEA632FFD07509FFCCD8E4FE555555A900000000000000001212
          1221898989E0C0C0C0FFB8B8B8FFE7E7E7FEF7F7F7FFF0F0F0FFEAEBEDFFDBD7
          D3FFD57E1DFFE9A345FFD17816FFD8E4F0FE555555AE00000000000000006C6C
          6CC0BABABAFEB2B2B2FFADADADFFF1F1F1FEA8A8A8FFB1B1B1FFB7B9BBFFE3DE
          D8FFECA247FFF7BB71FFD58226FFE1ECF7FE585858B400000000000000006767
          67B7AFAFAFFFAAAAAAFFA6A8AAFFFBFCFDFEA1A1A1FFBABABAFFADAFB1FFEBE4
          DBFFF1AB56FFFFC98CFFDD8A2EFFE6EFF9FF5E5E5EB90000000000000000696A
          6ABBA7A9ACFF9FA7B2FF9E8676FFEAEDF0FE939496FF919191FF949597FFF4F1
          EDFFE5AC64FFE1AE72FFD19C5CFFE8EEF6FF58595BB200000000000000006D6F
          72C09B9592FFBB7831FFE68F21FFC07632FEE9F3FDFFFBFDFFFFF1F1F1FFF5F7
          F8FFEDF2F8FFE8EEF6FFE5ECF3FFE9F4FEFE5F452CA000000000000000009B64
          22BEE59430FFF2AE51FFEEAC54FFEEA545FFBC7C41FFF3FDFFFFFFFFFFFFF7F7
          F7FFF1F2F2FFEEF0F2FFECF7FFFFD49D56FD5A2A016C0000000000000000AF77
          31BBF2AE57FEF3B568FFF0B166FFF1B160FFECA547FFBB8251FFF6FFFFFFFFFF
          FFFFF8FBFEFFF6FEFFFFC67F33F94D2301590000000000000000000000000000
          0000AF732CB7F9BD74FEF8BE7CFFF3B872FFF3B464FFE99D3DFFBE8F67FFFDFF
          FFFFFFFFFFFFB66218F43E1C0149000000000000000000000000000000000000
          000000000000B27B30C0FBCA90FFFCC688FFF5BC76FFF4B562FFE79833FFC297
          76FFA54B09ED3116013A00000000000000000000000000000000000000000000
          00000000000002010101C8903FDBD6954CEFBD7C32DAA46924C5905616B1743A
          019C2411012E0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000F0070000E0010000C001000080010000800100008001
          0000800100008001000080030000C00F0000E01F0000F03F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000B04010D5B260183A94C01E6B24F01F2A84801E4853701BA4A1C
          0171017601FF017101FF016E01FF000000000000000000000000000000000000
          000036170142B45405E9D36F02FFD57001FFD46F01FFEBBE8FFFE7B88FFFE9C1
          9FFF017E01FF45DD78FF017301FF351401450000000000000000000000003A18
          014ACB670FFCDB7E1BFFDE8113FFD97502FFD77201FFEEC69AFF018701FF0185
          01FF018101FF49E17CFF017B01FF017601FF017101FF00000000000000009249
          0CBEE38F36FFE89836FFE79028FFE07F08FFDD7801FFF1CB9FFF018D01FF5FF7
          91FF5BF38DFF54EC86FF49E17CFF46DE79FF017901FF0000000000000000A652
          0DD5F3B25CFFEFA547FFEE9F39FFDE7A07FFE37E01FFF3CD9FFF019101FF018D
          01FF018B01FF5BF38DFF018301FF018101FF017E01FF0000000000000000B755
          0BEBF9C073FFF5B259FFF4AB48FFDB7305FFE98301FFF5CF9FFFF3CD9FFFF0CA
          9FFF018F01FF5FF791FF018901FFE2BC9FFFCFAF9BED0000000000000000BB5C
          10EFFCCD86FFFBBC66FFFFB851FFD56701FFF48701FFED8101FFE67C01FFF2CB
          9FFF019301FF019101FF018D01FFDEB28FFF913701DD0000000000000000C25F
          13F4FFD791FFED993FFFCB650DFFA8632EFFC1906BFFBD8152FFBE7636FFE6C5
          A6FFE7BD9FFFE7BC9FFFE6BA9AFFE0B18FFF993B01E90000000000000000C663
          12F9D2731CFFB9723AFF8E99A5FF8F959BFFEBF2F9FFE0E8F0FFD9E3ECFFD4E0
          EBFFD0DEEAFFCAC5C5FFC1A08EFFAC724DFF9C3601F50000000000000000BB4F
          01F5ABAFB5FF999DA1FFD2D4D4FFADADADFFC6C6C6FFD6D6D6FFE6E6E6FFF1F1
          F1FFEEEEEFFFEBEDEFFFEBF0F4FFBBC6CCFF4627138C00000000000000003732
          2C6CCED4DAFFA4A4A4FFA2A2A2FFA4A4A4FFA7A7A7FF9F9F9FFFAAAAAAFFA2A2
          A2FFAEAEAEFFEDEDEDFFEFEFEFFFB7B8B9FF3335365C00000000000000001415
          151F727374C7F1F1F1FFC6C6C6FFA1A1A1FFFFFFFFFFB9B9B9FFF8F8F8FFFFFF
          FFFFAAAAAAFFF5F5F5FFF8F8F8FF6A6A6ABA0C0C0C1100000000000000000000
          00004040407BFFFFFFFFFFFFFFFFB5B5B5FFFFFFFFFFAEAEAEFFCBCBCBFFEAEA
          EAFF9D9D9DFF929292DEA8A8A8E86F6F6FAE0000000000000000000000000000
          000025252538767676C3909090DC8C8C8CEE585858A86B6B6BB75353539E2B2B
          2B5D3939396E0101010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F0070000E0070000C00100008001000080010000800100008001
          000080010000800100008001000080010000C0030000C0030000E01F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000B04010D5B260183A94C01E6B24F01F2A84801E4853701BA4A1C
          0171220F0134030101030D2D6C6B847E77E83C3A367E00000000000000000000
          000036170142B45405E9D36F02FFD57001FFD46F01FFD16C01FFC85F01FFC45A
          01FFCF864DFF7E7B9FF83075ECF880DEFFFF8D867CEC00000000000000003A18
          014ACB670FFCDB7E1BFFDE8113FFD97502FFE39B4DFFE1994DFFDE964DFFDB94
          4DFF8A849AFF337CF4FF72E1FFFF3787F9FF404781C600000000000000009249
          0CBEE38F36FFE89836FFE79028FFE9A452FF9C7B54FF7A7063FF756F68FF7A6C
          5DFF8F8881FF8CC5D6FF3583F7FF7C7CA7FF9F6A4DCE0000000000000000A652
          0DD5F3B25CFFEFA547FFF3BC74FF9F8161FFBEA889FFFFD184FFFED085FFFED6
          98FF9A948BFF8F8B85FF8281A1FFD8A581FF7F3101C50000000000000000B755
          0BEBF9C073FFF5B259FFF8CD92FF80766BFFFFDD97FFF2D598FFEFCD90FFF1C8
          82FFFED99DFF7D6C5AFFDFAD81FFB24D01FF873401D10000000000000000BB5C
          10EFFCCD86FFFBBC66FFFFD89FFF7C7872FFFFEEBDFFF8E8BBFFF4DDA9FFF0D0
          94FFFFD590FF787068FFE0AE81FFB55001FF913701DD0000000000000000C25F
          13F4FFD791FFED993FFFEBC5A5FF807B73FFFFFFEAFFFFFEEFFFF9EDC2FFF6DA
          A0FFFFDA96FF7C6A5FFFDFA981FFB74D01FF993B01E90000000000000000C663
          12F9D2731CFFB9723AFFA8B1BAFF909193FFDEDAC4FFFFFFF2FFFFF7CBFFFFE7
          A6FFB4A998FF989696FFC1A08EFFAC724DFF9C3601F50000000000000000BB4F
          01F5ABAFB5FF999DA1FFDADBDBFFC2C2C2FF9E9E9EFF817F7DFF7C7A76FF8180
          7FFFA6A6A7FFEBEDEFFFEBF0F4FFBBC6CCFF4627138C00000000000000003732
          2C6CCED4DAFFA4A4A4FFA2A2A2FFB4B4B4FFBBBBBBFFB6B6B6FFBCBCBCFFB0B0
          B0FFAEAEAEFFEDEDEDFFEFEFEFFFB7B8B9FF3335365C00000000000000001415
          151F727374C7F1F1F1FFC6C6C6FFA1A1A1FFFFFFFFFFB9B9B9FFF8F8F8FFFFFF
          FFFFAAAAAAFFF5F5F5FFF8F8F8FF6A6A6ABA0C0C0C1100000000000000000000
          00004040407BFFFFFFFFFFFFFFFFB5B5B5FFFFFFFFFFAEAEAEFFCBCBCBFFEAEA
          EAFF9D9D9DFF929292DEA8A8A8E86F6F6FAE0000000000000000000000000000
          000025252538767676C3909090DC8C8C8CEE585858A86B6B6BB75353539E2B2B
          2B5D3939396E0101010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F0310000E0010000C00100008001000080010000800100008001
          000080010000800100008001000080010000C0030000C0030000E01F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001475054019AB1D50183A3D6012A3A5801060708012A313F012C
          374B0F1E243B615B58B36F6F6FDF4B4B4BC10E0E0E3500000000000000000000
          00000104060601A6AEB17BFEFEFF01CDE6FF016E86AD01AFC2DF01C4DEFF01BF
          E0FF51ACC0FFD6CBC9FFF0F0F0FFB0B0B0FF424141A600000000000000000114
          1B24016B9BE30175A9FF019AC3FF0181AFFF016FA4FD7BFFFFFF2AC3C7FF488B
          96FF51898EFFC7B8B7FEC2ABA8FFA99A99FE2725255F0000000000000000014F
          6FA00185B8FF018BBEFF018BC0FF018BBFFF017AAEFF0381AFFFEEC3BAFFECE4
          E4FFB39D9BFF357F83FF01CBE3FF01B2D8FF014F6B900000000000000000016C
          96D00192C4FF069BCDFF10A0D2FF0B9DD0FF0194C7FF017DB2FF5CA0BBFF819D
          A9FF697380DD0E8487977BFAFBFB01A9C2C5012B37420000000000000000016C
          95CA11A1D3FF3FB3DFFF55BDE7FF49B7E3FF20A7D6FF0186B9FF017CAFFF016D
          A0FF015784CF0000000000000000000000000000000000000000000000000153
          719835B1DEFF72C9F0FF98D9FEFF82CFF5FF48B8E3FF0A9DCEFF0181B4FF0171
          A3FF014C72B40000000000000000000000000000000000000000000000000122
          303A32AFDDFE86D1F9FFABE1FFFF97D9FDFF54BCE6FF0F9FD1FF018EC2FF016D
          9EF6010D12160000000000000000000000000000000000000000000000000000
          0000035D7C864FBBE8FE85D1F7FF71C7F0FF3DB3E0FF059CCEFF0170A2F50122
          313C000000000000000000000000000000000000000000000000000000000000
          000000000000013D535D2AA9D9FE2AA9D9FF0497CCFF016795DC0112191F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000073DDE4E446DEFEFE5DE6FEFE33525352000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002D90B3CB0181B7FF01699BFF13516EA8010203030000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000012635390E9CCEFB0181B4FF0176A8FF016394FF015A85D70000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001455C6501405A80015882D20174A3E90148679D01151F2D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000E1E30000E0010000C00100008001000080030000801F0000801F
          0000C03F0000C07F0000E0FF0000F0FF0000F0FF0000F07F0000E0FF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000011017200117213501080B0E0000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000010E
          1519025E86B11295C2FE0181B4FF01689CF6013A567400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000969
          8DBF66DEEEFF3EC5E3FF239AC1FF1A92BCFF017EB1FE01314866013047630115
          1F29000000000000000000000000000000000000000000000000000000002194
          B6E07FF0F8FF5BD6EAFF46CAE5FF21B1D9FF23A7CFFF30B4D8FF1596C2FF0171
          A6FD013450670000000000000000000000000000000000000000000000000144
          5F7024AFD7FB35BADBFF1BAAD4FF0187BBFF5DD3E6FF75E9F5FF54D5EBFF34C3
          EBFF01537EB40107010D00000000000000000000000000000000000000000000
          0000127A9FC265DFEFFF0595C8FF01679DFF0996CCFF2CB4E4FF1CA7E4FF098D
          C8FF058732FE036301DF011E013D000000000000000000000000000000000000
          00001A92B9D868E2F1FF0798C8FF1086B6FF088697FF329350FE049E34FF08A3
          01FF019D01FE078B09FE017001FC013B01850103010500000000000000000103
          040439B9DDFA67DFF4FF35B7D9FF0686B9FF62A9C8FF89A57DFF0CB820FF28C2
          4BFF47E17DFF34C863FF088B0CFF017401FF013F019E00000000000000000103
          04041F9A96E157D6D3FF38BFE0FF38B8DCFFE3D5DCFFCCCBCAFF6BC18CFF50ED
          83FF50E97DFF37CE5AFF17AE2DFF139522FE015801D300000000000000000000
          00001770279330CC56FD33CE55FF2ACB4CFF7EBB86FFB0A7ADFFB89FB2FF129D
          20FF019A07FF019501FF019701FF098110FF044907AA00000000000000000000
          00001972328E31C962FF23BE4FFE13BC40FE72C185FFD4C8D1FFD8CAD5FF4DC5
          68FF3AD560FF49DF73FF43DD78FF199F2EFF022A065A00000000000000000000
          000015602A6C41DA72FC76FEA3FEBAFEC6FEB4FFC4FFF7FFF5FFFFEEFBFFF2E0
          EDFF92FFB6FF83FFAEFF66FD9BFF33CA60FE033B088300000000000000000000
          0000000000000C37193D2CB258D864EA8CFF65E589FF46DE74FE7DDA98F773C5
          8AE340A45DCD168933B80F6A1FA20850128D0324064000000000000000000000
          00000000000000000000020804080C2E1638061D0D24030D060F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000C1FF0000807F0000801F0000801F0000C00F0000C003
          0000C0010000C0010000C0010000C0010000C0010000F0030000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000606060A2B2B2B62686868C96D6D6DDB3A3A3A801414
          1425000000000000000000000000000000000000000000000000000000000000
          00001A1A1A2B4C4C4C92989898EFDEDEDEFFFFFFFFFF9C9C9CFF818181FF8181
          81FE656565C73636366B0C0C0C14000000000000000000000000000000007373
          73BCC2C2C2FEFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9BFF7B7B7BFF6D6D
          6DFF787878FF808080FF7A7A7AF9555555B2272727510000000000000000A7A7
          A7F8FFFFFFFFFFFFFFFFE2E2E2FFC4C4C4FFC3C3C3FF929292FF7B7B7BFF7D7D
          7DFF7E7E7EFF717171FF686868FF7D7D7DFF6E6E6EF500000000000000009F9F
          9FF7E0E0E0FFCDCDCDFFDCDCDCFFEBEBEBFFE9E9E9FFBFBFBFFFACACACFFA1A1
          A1FF8A8A8AFF7E7E7EFF7B7B7BFF818181FF747474F500000000000000009C9C
          9CF5F5F5F5FFEAEAEAFFE3E3E3FFDCDCDCFFDCDCDCFFE9E9E9FFDDDDDDFFCCCC
          CCFFBABABAFFB1AEB0FFAFA4ADFF968E95FF6E6B6DF50000000000000000A2A2
          A2F5EEEEEEFFDFDFDFFFDBDBDBFEE7E7E7FEFEFEFEFFF9F9F9FFF3F3F3FFF1F1
          F1FFEBEBEBFFDBD2D9FF2EC04CFF73CC7DFF7B777BEB00000000000000007878
          78B8B8B8B8FFD3D3D4FEA1A2A3FE66696CFF86888BFFB8BABBFFE0E0E1FFF5F5
          F5FFFCFCFCFFFFFCFFFFE7E5E6FFB3B3B3FE3D3C3D7B00000000000000000000
          00001C1C1C29393A3A6677726FF2FFE7C7FFCAB39DFF96887CFF6F6B67FF676A
          6DFFADADAEFFC6C6C6FF818181E13434345F0000000000000000000000000000
          0000000000000B0B0B15A59F99FDFFF3DAFFFFEAD0FFFFEBCCFFFFF0CBFFE4C6
          A7FF404244A92424244002020203000000000000000000000000000000000000
          0000000000003B3C3D73E5DBD1FFFFF2E3FFFFEBDAFFFFE8D4FFFFEED6FFB8AA
          9CFF171717320000000000000000000000000000000000000000000000000000
          00000D0D0D13959494E9FFFFFFFFFFF8EEFFFFF3E8FFFFF3E5FFFFFDEAFF6363
          60D0000000000000000000000000000000000000000000000000000000000000
          0000707070ACEAEAEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A5A1FD1D1E
          1E43000000000000000000000000000000000000000000000000000000000000
          00001010101622222240303030664C4C4C8C727273B2888886D92B2B2B6F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F87F0000E00F0000800100008001000080010000800100008001
          000080010000E0030000F01F0000E03F0000E03F0000C07F0000F07F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF00000000000000000000
          0000000000000000000000000000676767FFCCCCCCFF7A7A7AFF979797FF7A7A
          7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000000F08
          0214833706C19C3B01EAB7BCC0F8676767FFCCCCCCFFFFFFFFFF343434FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF676767FF00000000000000009F4C
          06D1C37428FFCC7720FFBEBCBAFF676767FFCCCCCCFF696969FF7F7F7FFF6969
          69FFCCCCCCFFCCCCCCFFCCCCCCFFCCCCCCFF676767FF0000000000000000C96C
          11FFBE6D26FFC26917FFCAC5C4FF676767FF676767FF676767FF676767FF6767
          67FF676767FF676767FF676767FF676767FF676767FF0000000000000000C96A
          0FFFB96721FFBC6214FFD6DEE8FFD0BEB6FFC4BFBBFFC7CFD6FFDBE2EAFFCAC8
          C6FFDBB892FFE4C194FFD8AB8EFF00000000000000000000000000000000C666
          0FFFB7611DFFB95E15FFB56017FFB9641BFFBB6719FFBB6616FFBA6616FFBE6D
          1DFFC77925FFD18630FFAC4F14FF00000000000000000000000000000000C665
          0CFFB15512FFB0703EFFAF6C37FFB06E37FFB37039FFB4733AFFB6763AFFB879
          3DFFB98146FFD0812CFFAB4F13FF00000000000000000000000000000000C464
          0CFFAA4602FFCDDFEAFFEAF2F8FFE7EFF4FFE7EEF4FFE4EBF1FFE0E6EDFFE1E7
          EDFFB5C6D8FFCE7C20FFAA4E14FF00000000000000000000000000000000C363
          0AFFA33D01FFD7E1E8FFF5F5F6FFCDCDCDFFD1D1D1FFD0D0D0FFCECECEFFE5E5
          E5FFBFCAD3FFCA751BFFAB4D12FF00000000000000000000000000000000C160
          09FF9E3401FFDEE9EEFFFBFBFCFFDFDFDFFFE0E0E0FFDFDFDFFFDBDBDBFFE7E7
          E7FFC2CDD6FFC87219FFA94E13FF00000000000000000000000000000000C05E
          08FF9A2D01FFE4EFF5FFFFFFFFFFF0F0F0FFF1F1F1FFEBEBEBFFE4E4E4FFEBEB
          EBFFC6CFD9FFCA7319FFAA4F12FF00000000000000000000000000000000BD59
          06FD912501FFE7F3F9FFFFFFFFFFCBCBCBFFD1D1D1FFCFCFCFFFCECECEFFEAEA
          EBFFCAD3DEFF915010FFAE5313FD000000000000000000000000000000008244
          05A0B74701FED7E0E6FFE6E5E4FFE9E8E5FFE5E3E2FFDDDAD9FFD3D2D0FFCCCB
          C9FFB7C0C8FFC25901FE723406A0000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F8010000F8010000C00100008001000080010000800700008007
          000080070000800700008007000080070000800700008007000080070000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000002020207131313541C1C1C95232323C32A2A2AE2262626F1252525F41C1C
          1CEA121212D0080808A004040457010101070000000000000000000000000000
          00003A3A3ADB4A4A4AFF616161FF666666FF5C5E5EFF494242FF515252FF4646
          46FF3A3A3AFF222222FF1A1A1AFF080808D50000000000000000000000000000
          0000444444E36F6F6FFF747474FF676767FF5D5757FF956565FF3B3E3EFF4E4E
          4EFF404040FF303030FF252525FF0C0C0CD10000000000000000000000000000
          00002E2E2E9E777777FF828282FF6A6B6BFF837979FFAF7878FF707373FF3C3C
          3CFF434343FF383838FF262626FF0C0C0C840000000000000000000000000000
          00000D0D0D24575757F3929292FF7C7C7CFFCCCDCDFFB88787FFCFD1D1FF5353
          53FF3A3A3AFF3A3A3AFF212121E7030303130000000000000000000000000000
          000000000000191919464F4F4FEF838383FFA7A9A9FF7C6563FFB8B8B8FF5150
          4FFF343534FF262828E408080832000000000000000000000000000000000000
          000000000000000000000A090919272521C41E1E20FF25384FFF101316FF2531
          51FE493939C63727275900000000000000000000000000000000000000000000
          000000000000000000000D1B2E2D344F87F52E3034FF364F71FF1D1C1DFF3C63
          AFFF3B60C2C2865A53C702010102000000000000000000000000000000000000
          000000000000000000003163ACAC61B8FFFF5092CCFF5395D4FF4B8DCEFF5095
          E3FF4988FEFE816786E60806060B000000000000000000000000000000000000
          000000000000000000002F68C1EE0D0201FF010101FF478BC3FF150A01FF0101
          01FF396BBBFF313860BC00000000000000000000000000000000000000000000
          000000000000010102012E6CAFF65198CCFF519CD2FF529ED6FF4E93CFFF3A6D
          9DFF04060AFF010101BB00000000000000000000000000000000000000000000
          00000000000000000000070F19D013110DFF080B0CFF1D384BFF4582B7FF080C
          12FF050403FF0101019500000000000000000000000000000000000000000000
          000000000000000000000101014820201FFA80807FFF757473FF484644FF1918
          18FF060606F00101012400000000000000000000000000000000000000000000
          00000000000000000000000000000101013A020202BB1F1F1FF2181818F10101
          01B40101012D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000E0070000C0030000C0030000C0030000E0070000F00F0000F80F
          0000F80F0000F00F0000F00F0000F00F0000F00F0000F81F0000FC3F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010101
          031C1B1B14E62C2B28FF324D74FF4C8AE3FF4A85DEFF355992FF000000000000
          0000000000000000000000000000000000000000000000000000000000092143
          76922B303DFF2D2A25FF3D6599FF477BC2FF4678BFFF2C4262FF000000000000
          00000000000000000000000000000000000000000000000000010C16263B4382
          F8FA446FA1FF2A2219FF2F2821FF2B231AFF241C12FF1B130AFF000000000000
          0000000000000000000000000000000000000000000000000003285297A6519A
          FFFF5DB2FFFF4F8ECDFF4572A0FF4370A0FF477DB8FF549BEDFF000000000000
          000000000000000000000000000000000000000000000103070F3D7EEEF161B8
          FFFF63BDFFFF63BEFFFF5BB2FFFF61B7FFFF55A3F1FF3A6DA7FF000000000000
          000000000000000000000000000000000000000000000E1E323C428AF6FF2543
          5AFF101E2AFF000000FF0A141CFF66C6FFFF356895FF191108FF000000000000
          00000000000000000000000000000000000000000000163256603C81DEFF2720
          17FF11100FFF000000FF000000FF62BFFFFF2D5271FF3F3C39FF000000000000
          000000000000000000000000000000000000000000001A3C636C3A81D9FF2D23
          19FF1E1814FF000000FF000000FF193041FF121D23FF242525FF000000000000
          000000000000000000000000000000000000000000000F2747651B3A5FFF4991
          C1FF54AADDFF5FBFF4FF6AD0FFFF67CBFFFF64C7FFFF62C0FFFF000000000000
          000000000000000000000000000000000000000000000B1B31403983D8FF4A92
          C1FF3A7497FF67CFFFFF3D7A9DFF5FBCF9FF66C7FFFF63BEFFFF000000000000
          000000000000000000000000000000000000000000000102051A000000FE150F
          0CFF130E0AFF000205FF060000FF000305FF000508FF02060BFF000000000000
          0000000000000000000000000000000000000000000000000000000000B83636
          36FF656564FF4C4B4BFF646464FF545453FF4C4B4AFF363433FF000000000000
          0000000000000000000000000000000000000000000000000000000000230000
          00F05C5C5CFF7A7A7AFF757575FF6D6D6DFF616161FF555555FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0043000000F1585858FF919191FF808080FF6F6F6FFF5D5D5DFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000002A000000BE0E0E0EFE393939FF424242FF363636FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000002C0000006C0000008B00000088}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001111116D1C1C1CE21C1C1C5F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010101020D0D0D721111118B02020203424242D6737373FF606060BD0000
          0000000000000101010801010110000000000000000000000000000000000000
          00000808081A3F3F3FFF8C8C8CFF858585F7AFAFAFFFAEAEAEFF898989EE3232
          325C0909090E1F1F1FE0484848FA060606130000000000000000000000000000
          00002B2B2B5BBDBDBDFFB2B2B2FF7B7B7BB1929292A94F4F4FCF9C9C9CFFB0B0
          B0FDACACACF77A7A7AFD868686FF1D1D1D300000000000000000000000000000
          000011121226959595DCB5A6A6FFA68483F2977171CF7C6161E9767474FF797B
          7B957E7F7F9F7B7D7DBE9F9F9FFD4A4A4A8D0000000000000000000000000000
          00002D222233C89797F1D5A2A2FCE4B3B1FADFAEAFFBC59394FEBC898BFFAB7B
          7BF1876060C73E2D2D6EA3A6A6F64A4A4A880000000000000000000000000000
          0000A67E7ECCE8C1C1EEEAC2C0EDBDBAE5FDCABEDFFFC9AEC9FFB7A1BAFFBEAA
          97FDD4A2A2FBB68383FE756969CD040404070000000000000000000000000000
          0000C09393E9E8C7C5ECD3C1D4F3CCD1EDFFDAD6E7FFD1B8CCFFB7A7C2FFBDBD
          A0FFD6B3AAFBC89898FF7B5F5FC6000000000000000000000000000000000000
          0000CFA0A0F6E9CFCEEBD0DCF2FCE5ECF9FFEFF1F7FFCBB8D9FFBBBAB2FFC0C1
          A3FFCDB5A7FBD6A6A7FE896969D8000000000000000000000000000000000000
          0000D6A8A8FCF4F0F1F8B4D4F9FCBEDCFEFFBCD5FFFFBCC2F1FFEEE9DBFFDAD9
          C6FFC6B9A2FCF9CACCFD9D7979F3000000000000000000000000000000000000
          00005E4B4B5DBB9190CCD2ADB6F899A3D6F5279FBEFE30B4AFFF4EEC80FF96E6
          A6FFB4CFABFEFDE5E8FDA78080FE181919310000000000000000000000000000
          00000000000000000000181212178661629642E472F86CFF95FF6BEF90FF5BCD
          76F50E731FBE7E5456A79D8B8BF16F7070CC0000000000000000000000000000
          0000000000000000000000000000000000001356295F488B5BC97FA389ED9797
          97B9A9A6A8C9B6B7B8DEBDBFBFF8808080E00000000000000000000000000000
          000000000000000000000000000000000000000000000E0E0E1E7C797AD29695
          96F6888888DA6D6D6DAD494949830F0F0F210000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FC7F0000E47F0000E0270000C0070000E0030000E0030000C007
          0000C0070000C0070000C0070000C0070000F8030000FC030000FF070000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000001043D4F020EAAED0411AFF00210ADF0030FAAF0040D
          AAF002079EED0101374D00000000000000000000000000000000000000000000
          0000000000000106464F061AC6F91D52FFFF1A50FFFF194DFFFF164DFFFF144B
          FFFF1249FFFF0412B7F801023C4D000000000000000000000000000000000000
          00000107494F061ECBF92055FFFF1C50FFFF1B4CFFFF194BFFFF1749FFFF1547
          FFFF1246FFFF1148FFFF0412B9F801023C4D0000000000000000000000000107
          464F0822D2F9265AFFFF2254FFFF2051FFFF1E4FFFFF1C4EFFFF1A4CFFFF1849
          FFFF1547FFFF1447FFFF1148FFFF0413BCF80102394D0000000000000000021C
          C6F0295EFFFF1D4FFFFF1449FFFF194CFFFF1549FFFF174BFFFF0E42FFFF0A3F
          FFFF0C41FFFF0C40FFFF1044FFFF1248FFFF010DA8EF00000000000000000622
          D1F4255BFFFFC1D0FFFFE8EEFFFF3E6AFFFFD2DCFFFF2455FFFFC0CEFFFFC8D5
          FFFF5B7FFFFFB3C3FFFF083CFFFF144AFFFF0413B5F400000000000000000624
          D4F4255BFFFF91AAFFFFE1E8FFFF7291FFFFFFFFFFFF2052FFFFFFFFFFFFE1E8
          FFFFA5B9FFFFD3DDFFFF1548FFFF1147FFFF0215B8F400000000000000000728
          D7F4265CFFFFDDE5FFFFD1DBFFFF0F47FFFFFCFDFFFF184DFFFFFFFFFFFFDCE5
          FFFFA2B7FFFFF5F6FFFFD2DCFFFF0840FFFF0217BDF40000000000000000072C
          DCF42E62FFFFCBD6FFFFF9FAFFFFA9BCFFFFFFFFFFFFBFCEFFFFD8E2FFFFECF0
          FFFF6E8EFFFFFEFFFFFFA9BBFFFF0C45FFFF041AC0F400000000000000000428
          DBF03E6FFFFF2C5EFFFF2559FFFF3162FFFF3060FFFF2457FFFF164BFFFF1148
          FFFF1348FFFF0C42FFFF0F45FFFF194EFFFF0118BFF00000000000000000010E
          53520C36EFF94271FFFF3F6FFFFF4873FFFF4A75FFFF3A68FFFF2D5DFFFF2859
          FFFF2455FFFF2254FFFF2054FFFF0523D7F90108464F00000000000000000000
          0000010F53520D37EFF94372FFFF4270FFFF3F6CFFFF3564FFFF2D5DFFFF295A
          FFFF2758FFFF2658FFFF0628DEF901094F4F0000000000000000000000000000
          000000000000010F53520C37F2F94072FFFF3C6CFFFF3768FFFF3364FFFF2F61
          FFFF2B5EFFFF082BE4F9010C504F000000000000000000000000000000000000
          00000000000000000000010F5352042EE5F00630E6F4072FE5F4062DE3F4062C
          E1F40227D9F0010C4E4F00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F81F0000F00F0000E0070000C003000080010000800100008001
          000080010000800100008001000080030000C0070000E00F0000F01F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A37A66CCC9967FFFC5927DFFC18E7BFFBD8A79FFB98677FFB58275FFB17F
          73FFAD7B71FFA9776FFFA5736DFF845C57CC000000000000000000000000A57D
          67CCEEDACEFFF9F9F9FFF9F9F9FFFAFAFAFFFCFCFCFFFEFEFEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFDBC7C4FF845C57CC0000000000000000D29F
          82FFFFFFFFFFFFF0E1FFFFF6ECFFFFFBF7FFFFFFFFFFFFFFFFFFFFFFFFFF8B8B
          8BFF8A8A8AFF898989FFFFFFFFFFFFFFFFFFA5736DFF0000000000000000D7A4
          84FFFFFFFFFFFFF0E1FFFFF6ECFFD97401FFD87301FFD77201FFFFFFFFFF8D8D
          8DFF8B8B8BFF8A8A8AFFFFFFFFFFFFFFFFFFA9776FFF0000000000000000DCA9
          87FFFFFFFFFFFFF0E1FFFFF6ECFFDA7501FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAE7C71FF0000000000000000E1AE
          8AFFFFFFFFFFFFEEDDFFFFF4E8FFDB7601FFFFFDFBFFFFFFFFFFFFFFFFFF8F8F
          8FFF8E8E8EFF8D8D8DFF8C8C8CFFFFFFFFFFB38174FF0000000000000000E5B2
          8CFFFFFFFFFFDE7901FFDD7801FFDD7801FFDC7701FFDB7601FFFFFFFFFF9191
          91FF909090FF8F8F8FFF8D8D8DFFFFFFFFFFB78476FF0000000000000000E9B6
          8EFFFFFFFFFFE07B01FFFFEEDDFFFFF4E8FFFFF7F0FFFFFBF7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFBA8778FF0000000000000000EDBA
          90FFFFFFFFFFE17C01FFFFEBD6FFFFF0E1FFFFF4E8FF979797FF959595FF9494
          94FF939393FFFFFFFFFFFFFFFFFFFCFCFCFFBE8B7AFF0000000000000000F1BE
          92FFFFFFFFFFE27D01FFE17C01FFE07B01FFFFF0E1FF989898FF979797FF9595
          95FF949494FFFFFBF7FFFFFBF7FFFAFAFAFFC28F7CFF0000000000000000F5C2
          94FFFFFFFFFFE37E01FFFFE5CBFFFFE9D2FFFFEBD6FFFFEEDDFFFFF2E5FFFFF4
          E8FFFFF6ECFFFFF6ECFFFFF6ECFFF9F9F9FFC6937EFF0000000000000000F9C6
          96FFFFFFFFFFFFDCB8FFFFE0C0FFFFE4C7FFFFE7CEFFFFEBD6FFFFEDDAFFFFEE
          DDFFFFF0E1FFFFF0E1FFFFF0E1FFF9F9F9FFC9967FFF0000000000000000C39A
          76CCFDE8D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEEDACEFF886059CC00000000000000000000
          0000BF9674CCF9C696FFF5C294FFF1BE92FFEDBA90FFE9B68EFFE5B28CFFE1AE
          8AFFDDAA88FFD9A686FFD5A284FF886059CC0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C003000080010000800100008001000080010000800100008001
          0000800100008001000080010000800100008001000080010000C0030000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000128
          39562684A9E5298FB6F21F8DB5F21B8CB4F2148BB3F20D8BB3F20688AFEF0188
          AEEF0187ADEF0184AAED0183A7EC01678ED7011B2A4500000000000000002F8B
          B0E8A6EBF7FF84E1F1FF6DE2F1FF57E3F1FF41EBF6FF1AF1FAFF2BF7F7FF02FF
          FFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01759CE700000000000000001A7B
          A1D9A5E7F3FF7FDBEDFF6ADBECFF56DEEDFF33E3FDFF1F3841FF463D39FF0AFF
          FFFF01FBFCFF01FFFEFF01FFFFFF01FFFFFF0187ABF100000000000000000130
          455B6AC3DDFF89DEF0FF6FDBECFF5BDCEDFF45E3F4FF0E93B6FF139DB8FF09F5
          FCFF01F4F9FF01FCFEFF01FFFFFF01E4EEFF01314D8200000000000000000101
          01010A75A0C694E1F1FF77DAECFF62DBECFF51E0F0FF33E6EBFF47DCDDFF17EC
          F7FF07EEF6FF01F5F9FF01FFFFFF0179A0E40106080B00000000000000000000
          000001212F3649B4D6FD87DFEFFF6DDAECFF59E4F6FF1B888EFF417573FF27F2
          FEFF18E9F3FF0CF2F8FF03C6DAFF012E3E5F0000000000000000000000000000
          000000000000016184A187D9EBFF7CDCECFF48E8FFFF253B3FFF403736FF1EFB
          FFFF2CE5F2FF22EEF6FF036A87CA010102010000000000000000000000000000
          0000000000000112181A31A4C9F394E3F0FF39D2F6FF221311FF392827FF21CE
          DAFF42E8F5FF20AED0FE01202E3C000000000000000000000000000000000000
          00000000000000000000014A677B82D3E8FF46CFF1FF120703FF2C1511FF24C9
          E5FF56DFEFFF02597DA900000000000000000000000000000000000000000000
          00000000000000000000010708081C8CB4DF9DE6F6FF32B0D3FF28ADCFFF68E5
          FAFF2C9EC5F601121A2000000000000000000000000000000000000000000000
          000000000000000000000000000001364B5570C7E3FFA2E5F2FF92E1F2FF78D4
          E7FF014663850000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B77A0C1B8E8F2FFB1E9F6FF278D
          B2E601080B0C0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000011720271A7CA1D12788AEE20126
          3747000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF000080030000800100008001000080010000C0030000E003
          0000E0070000F00F0000F00F0000F81F0000F81F0000FC3F0000FE7F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001C1C
          1C41676767DC7B7B7BFB4C4C4CA7000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000C0C0C1B8484
          84F5BFBFBFFF8A8A8AFE2A2A2A670505050C0E0E0E1F00000000000000000000
          00000000000000000000000000000000000000000000000000002626265A9090
          90FF9E9E9EFF4A4A4AC6141414325B5B5BC9606060CF00000000000000000000
          0000000000000000000000000000000000000000000002020203595959C5A3A3
          A3FEACACACFF909090FF707070FDCECECEFE696969E800000000000000000000
          00000000000000000000000000000000000002020203656565D3B4B4B4FEB5B5
          B5FEADADADFFADADADFFACACACFEA6A6A6FE5B5B5BCF00000000000000000000
          000000000000000000000000000002020202595959BEBCBCBCFEB9B9B9FEC2C2
          C2FFC1C1C1FF8D8D8DFF8C8C8CFF5C5C5CD30F0F0F2000000000000000000000
          00000000000000000000020202025B5B5BBDC3C3C3FEBABABAFEBBBBBBFFC0C0
          C0FF777777EF262626591E1E1E43020202040000000000000000000000000000
          00004343437E5F5F5FBA5B5B5BBACECECEFEB7B7B7FEB2B2B2FFBCBCBCFF7C7C
          7CF01C1C1C370000000000000000000000000000000000000000000000003232
          325BA1A1A1FECCCCCCFFBEBEBEFEB7B7B7FEA1A1A1FFB0B0B0FF818181F51D1D
          1D38000000000000000000000000000000000000000000000000000000007B7B
          7BDFD9D9D9FFCECECEFFCBCBCBFFB5B5B5FFA8A8A8FF858585F51F1F1F3A0000
          0000000000000000000000000000000000000000000000000000000000009393
          93F9A2A2A2FE7F7F7FE89C9C9CFACFCFCFFFA7A7A7FF42424282000000000000
          0000000000000000000000000000000000000000000000000000000000008989
          89E94141417104040405676767C5CBCBCBFE969696FF2121213D000000000000
          0000000000000000000000000000000000000000000000000000000000001313
          131D000000005353538DCFCFCFFED1D1D1FE7C7C7CE604040406000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000005A5A5A938F8F8FE66E6E6ED61717172900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFC70000FF870000FF090000FF010000FE010000FC030000F80F
          0000C03F0000807F000080FF000080FF000091FF0000E1FF0000E3FF0000FFFF
          0000}
      end>
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 872
    Top = 1040
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = 16774642
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clCream
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBackground
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaptionText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor]
      Color = clCream
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor]
      Color = 16644338
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor]
      Color = 15924208
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor]
      Color = 16113353
    end
  end
  object dsSetupGrupaArtikalPlan: TDataSource
    DataSet = SetupGrupaArtikalPlan
    Left = 512
    Top = 32
  end
  object IzbraniGrupiZaPlan: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mr.id, mr.opis'
      'from mtr_artvid mr'
      
        'where(  '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 ' +
        '= '#39'GRUPI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mr.id || '#39','#39'  || '#39'%'#39
      '     ) and (     MR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mr.id, mr.opis'
      'from mtr_artvid mr'
      
        'where '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 = ' +
        #39'GRUPI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mr.id || '#39','#39'  || '#39'%'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 624
    Top = 88
    object IzbraniGrupiZaPlanID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object IzbraniGrupiZaPlanOPIS: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzbraniGrupiZaPlan: TDataSource
    DataSet = IzbraniGrupiZaPlan
    Left = 624
    Top = 32
  end
  object tblListaArtikliFiltrirani: TpFIBDataSet
    RefreshSQL.Strings = (
      'select m.artvid,'
      '       m.id,'
      '       m.naziv,'
      '       m.merka,'
      '       cast(m.cena as numeric(15, 4)) as cena,'
      '       m.brpak,'
      '       m.katalog,'
      '       m.barkod,'
      '       m.prozvoditel,'
      '       m.grupa,'
      '       m.klasifikacija,'
      '       cast(m.kolicina as numeric(15,4)) as kolicina,'
      '       m.uninaziv,'
      '       m.unifont,'
      '       m.oblik,'
      '       m.jacina,'
      '       m.tarifa,'
      '       ma.opis as ArtVidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       mr.naziv as MerkaNaziv,'
      '       mp.naziv as ProizvoditelNaziv'
      'from mtr_artikal m  '
      'inner join mtr_artvid ma on m.artvid= ma.id'
      
        'left outer join mtr_artgrupa mg on m.grupa= mg.id and ma.id=mg.m' +
        'tr_artvid_id'
      'left outer join mat_merka mr on mr.id = m.merka'
      'left outer join mat_proizvoditel mp on mp.id = m.prozvoditel'
      
        'where(  '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 ' +
        '= '#39'GRUPI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || m.artvid || '#39','#39'  || '#39'%' +
        #39
      '     ) and (     M.ARTVID = :OLD_ARTVID'
      '    and M.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select m.artvid,'
      '       m.id,'
      '       m.naziv,'
      '       m.merka,'
      '       cast(m.cena as numeric(15, 4)) as cena,'
      '       m.brpak,'
      '       m.katalog,'
      '       m.barkod,'
      '       m.prozvoditel,'
      '       m.grupa,'
      '       m.klasifikacija,'
      '       cast(m.kolicina as numeric(15,4)) as kolicina,'
      '       m.uninaziv,'
      '       m.unifont,'
      '       m.oblik,'
      '       m.jacina,'
      '       m.tarifa,'
      '       ma.opis as ArtVidNaziv,'
      '       mg.naziv as GrupaNaziv,'
      '       mr.naziv as MerkaNaziv,'
      '       mp.naziv as ProizvoditelNaziv'
      'from mtr_artikal m  '
      'inner join mtr_artvid ma on m.artvid= ma.id'
      
        'left outer join mtr_artgrupa mg on m.grupa= mg.id and ma.id=mg.m' +
        'tr_artvid_id'
      'left outer join mat_merka mr on mr.id = m.merka'
      'left outer join mat_proizvoditel mp on mp.id = m.prozvoditel'
      
        'where '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 = ' +
        #39'GRUPI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || m.artvid || '#39','#39'  || '#39'%'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 408
    Top = 576
    object tblListaArtikliFiltriraniARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072'(ID)'
      FieldName = 'ARTVID'
    end
    object tblListaArtikliFiltriraniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblListaArtikliFiltriraniNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' (ID)'
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblListaArtikliFiltriraniBRPAK: TFIBIntegerField
      DisplayLabel = #1041#1088'.'#1087#1072#1082#1091#1074#1072#1114#1077
      FieldName = 'BRPAK'
    end
    object tblListaArtikliFiltriraniKATALOG: TFIBStringField
      DisplayLabel = #1050#1072#1090#1072#1083#1086#1075
      FieldName = 'KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniBARKOD: TFIBStringField
      DisplayLabel = #1041#1072#1088#1082#1086#1076
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083'(ID)'
      FieldName = 'PROZVODITEL'
    end
    object tblListaArtikliFiltriraniGRUPA: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072'(ID)'
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniKLASIFIKACIJA: TFIBStringField
      DisplayLabel = #1050#1083#1072#1089#1080#1092#1080#1082#1072#1094#1080#1112#1072
      FieldName = 'KLASIFIKACIJA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object tblListaArtikliFiltriraniUNINAZIV: TFIBStringField
      FieldName = 'UNINAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniUNIFONT: TFIBStringField
      FieldName = 'UNIFONT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniOBLIK: TFIBIntegerField
      FieldName = 'OBLIK'
    end
    object tblListaArtikliFiltriraniJACINA: TFIBFloatField
      FieldName = 'JACINA'
    end
    object tblListaArtikliFiltriraniTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
    object tblListaArtikliFiltriraniARTVIDNAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniGRUPANAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblListaArtikliFiltriraniPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dstblListaArtikliFiltrirani: TDataSource
    DataSet = tblListaArtikliFiltrirani
    Left = 400
    Top = 512
  end
  object tblPonudeniArtikli: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct pps.artvid, pps.artsif, ma.naziv'
      'from po_ponuda_stavki pps'
      
        'inner join mtr_artikal ma on ma.artvid = pps.artvid and ma.id = ' +
        'pps.artsif'
      
        'inner join po_ponuda pp on pp.broj = pps.broj and pp.godina = pp' +
        's.godina'
      'where pps.godina = :godina  and pp.re = :re')
    SelectSQL.Strings = (
      'select distinct pps.artvid, pps.artsif, ma.naziv'
      'from po_ponuda_stavki pps'
      
        'inner join mtr_artikal ma on ma.artvid = pps.artvid and ma.id = ' +
        'pps.artsif'
      
        'inner join po_ponuda pp on pp.broj = pps.broj and pp.godina = pp' +
        's.godina'
      'where pps.godina = :godina  and pp.re = :re')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 744
    Top = 88
    object tblPonudeniArtikliARTVID: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'ARTVID'
    end
    object tblPonudeniArtikliARTSIF: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblPonudeniArtikliNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudeniArtikli: TDataSource
    DataSet = tblPonudeniArtikli
    Left = 744
    Top = 32
  end
  object frxDBListaPonudiArtikli: TfrxDBDataset
    UserName = 'frxDBListaPonudiArtikli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DATUM=DATUM'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'BROJ1=BROJ1'
      'GODINA1=GODINA1'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'NAZIV=NAZIV'
      'NAZIV_ARTIKAL=NAZIV_ARTIKAL'
      'ARTVID_NAZIV=ARTVID_NAZIV'
      'KRAJNA_CENA=KRAJNA_CENA'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'RABAT=RABAT')
    DataSource = dsListaNaponudiZaArtikli
    BCDToCurrency = False
    Left = 48
    Top = 1032
  end
  object ListaNaponudiZaArtikli: TpFIBDataSet
    RefreshSQL.Strings = (
      'select pp.datum,'
      '       pp.broj,'
      '       pp.godina,'
      '       pps.broj,'
      '       pps.godina,'
      '       pps.artvid,'
      '       pps.artsif,'
      '       pp.tip_partner,'
      '       pp.partner,'
      '       mp.naziv,'
      '       pps.artsif ||'#39'. '#39'|| ma.naziv as naziv_artikal,'
      '       pps.artvid ||'#39'. '#39'|| mg.opis as artvid_naziv,'
      '       pps.krajna_cena,'
      '       pps.cena,'
      '       pps.ddv,'
      '       pps.cenaddv,'
      '       pps.rabat'
      'from po_ponuda pp'
      
        'inner join po_ponuda_stavki pps on pps.broj = pp.broj and pps.go' +
        'dina = pp.godina'
      
        'inner join mat_partner mp on pp.tip_partner = mp.tip_partner and' +
        ' pp.partner = mp.id'
      
        'inner join mtr_artikal ma on ma.id = pps.artsif and pps.artvid=m' +
        'a.artvid'
      
        'inner join mtr_artvid mg on ma.artvid = mg.id and ma.id = pps.ar' +
        'tsif'
      
        'where(   '#39','#39' || :vid  || '#39','#39' like '#39'%'#39' || '#39','#39' || cast (pps.artvid' +
        ' as varchar(1000))|| '#39','#39'  || '#39'%'#39
      
        '       and  '#39','#39' || :sif  || '#39','#39' like '#39'%'#39' || '#39','#39' || cast (pps.art' +
        'sif as varchar(1000))|| '#39','#39'  || '#39'%'#39
      '       and pps.godina = :godina'
      '       and pp.re = :re'
      '     ) and (     PP.BROJ = :OLD_BROJ'
      '    and PP.GODINA = :OLD_GODINA'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pp.datum,'
      '       pp.broj,'
      '       pp.godina,'
      '       pps.broj,'
      '       pps.godina,'
      '       pps.artvid,'
      '       pps.artsif,'
      '       pp.tip_partner,'
      '       pp.partner,'
      '       mp.naziv,'
      '       pps.artsif ||'#39'. '#39'|| ma.naziv as naziv_artikal,'
      '       pps.artvid ||'#39'. '#39'|| mg.opis as artvid_naziv,'
      '       pps.krajna_cena,'
      '       pps.cena,'
      '       pps.ddv,'
      '       pps.cenaddv,'
      '       pps.rabat'
      'from po_ponuda pp'
      
        'inner join po_ponuda_stavki pps on pps.broj = pp.broj and pps.go' +
        'dina = pp.godina'
      
        'inner join mat_partner mp on pp.tip_partner = mp.tip_partner and' +
        ' pp.partner = mp.id'
      
        'inner join mtr_artikal ma on ma.id = pps.artsif and pps.artvid=m' +
        'a.artvid'
      
        'inner join mtr_artvid mg on ma.artvid = mg.id and ma.id = pps.ar' +
        'tsif'
      
        'where  '#39','#39' || :vid  || '#39','#39' like '#39'%'#39' || '#39','#39' || cast (pps.artvid a' +
        's varchar(1000))|| '#39','#39'  || '#39'%'#39
      
        '       and  '#39','#39' || :sif  || '#39','#39' like '#39'%'#39' || '#39','#39' || cast (pps.art' +
        'sif as varchar(1000))|| '#39','#39'  || '#39'%'#39
      '       and pps.godina = :godina'
      '       and pp.re = :re'
      'order by pps.artvid, pps.artsif, pps.krajna_cena')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 184
    Top = 1032
    object ListaNaponudiZaArtikliDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object ListaNaponudiZaArtikliBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object ListaNaponudiZaArtikliGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object ListaNaponudiZaArtikliBROJ1: TFIBIntegerField
      FieldName = 'BROJ1'
    end
    object ListaNaponudiZaArtikliGODINA1: TFIBIntegerField
      FieldName = 'GODINA1'
    end
    object ListaNaponudiZaArtikliARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object ListaNaponudiZaArtikliARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object ListaNaponudiZaArtikliTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object ListaNaponudiZaArtikliPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object ListaNaponudiZaArtikliNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ListaNaponudiZaArtikliNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object ListaNaponudiZaArtikliARTVID_NAZIV: TFIBStringField
      FieldName = 'ARTVID_NAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object ListaNaponudiZaArtikliKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
    object ListaNaponudiZaArtikliCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object ListaNaponudiZaArtikliDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object ListaNaponudiZaArtikliCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object ListaNaponudiZaArtikliRABAT: TFIBFloatField
      FieldName = 'RABAT'
    end
  end
  object dsListaNaponudiZaArtikli: TDataSource
    DataSet = ListaNaponudiZaArtikli
    Left = 360
    Top = 1032
  end
  object frxDBPonudiZaOdbranaNabavka: TfrxDBDataset
    UserName = 'frxDBPonudiZaOdbranaNabavka'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NABAVKA_ID=NABAVKA_ID'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'DATUM=DATUM'
      'DATUM_VAZENJE=DATUM_VAZENJE'
      'KONTAKT=KONTAKT'
      'ZABELESKA=ZABELESKA'
      'USLOVI=USLOVI'
      'STATUS=STATUS'
      'NACIN_PLAKJANJE=NACIN_PLAKJANJE'
      'KREIRAL=KREIRAL'
      'VREME=VREME'
      'NBROJ=NBROJ'
      'NRE=NRE'
      'NGODINA=NGODINA'
      'PNAZIV=PNAZIV'
      'KNAZIV=KNAZIV'
      'SNAZIV=SNAZIV'
      'PLAKANJENAZIV=PLAKANJENAZIV'
      'BROJNABAVKA=BROJNABAVKA'
      'GODINA=GODINA'
      'BROJ=BROJ'
      'PONUDABROJ=PONUDABROJ'
      'PREDMETNABAVKA=PREDMETNABAVKA'
      'PREDMETPONUDA=PREDMETPONUDA')
    DataSource = dsPonudi
    BCDToCurrency = False
    Left = 72
    Top = 1112
  end
  object frxDBSPonudiDetail: TfrxDBDataset
    UserName = 'frxDBSPonudiDetail'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'KOLICINA=KOLICINA'
      'STATUS=STATUS'
      'KOLICINA_PRIFATENA=KOLICINA_PRIFATENA'
      'OPIS=OPIS'
      'CENA=CENA'
      'DDV=DDV'
      'CENADDV=CENADDV'
      'RABAT=RABAT'
      'VALUTA=VALUTA'
      'KURS=KURS'
      'KREIRAL=KREIRAL'
      'VREME=VREME'
      'ARTNAZIV=ARTNAZIV'
      'MERKA=MERKA'
      'ARTGRUPA=ARTGRUPA'
      'ARTVIDNAZIV=ARTVIDNAZIV'
      'ARTGRUPANAZIV=ARTGRUPANAZIV'
      'MERKANAZIV=MERKANAZIV'
      'VALUTANAZIV=VALUTANAZIV'
      'KRAJNA_CENA=KRAJNA_CENA'
      'IIZNOS=IIZNOS'
      'STATUSNAZIV=STATUSNAZIV')
    DataSource = dsPonudiDetail
    BCDToCurrency = False
    Left = 232
    Top = 1112
  end
  object PonudiDetail: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ps.id,'
      '       ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.status,'
      
        '       cast (ps.kolicina_prifatena as numeric(15,4)) as kolicina' +
        '_prifatena,'
      '       ps.opis,'
      '       ps.cena,'
      '       ps.ddv,'
      '       ps.cenaddv,'
      '       cast(ps.rabat as numeric(15,4)) as rabat,'
      '       ps.valuta,'
      '       ps.kurs,'
      '       ps.kreiral,'
      '       ps.vreme,'
      '       ma.naziv as artnaziv,'
      '       ma.merka as merka,'
      '       ma.grupa as artgrupa,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as artgrupanaziv,'
      '       mm.naziv as MerkaNaziv,'
      '       mval.naziv as valutanaziv,'
      '       ps.krajna_cena,'
      
        '       cast((ps.krajna_cena* ps.kolicina_prifatena) as numeric(1' +
        '5, 4)) as iiznos,'
      '       ms.naziv as statusnaziv'
      'from po_ponuda_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      
        'inner join mtr_artvid mv on mv.id = ma.artvid and ma.id = ps.art' +
        'sif and ma.artvid = ps.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'ps.artsif and ma.artvid = ps.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = ps.artsi' +
        'f and ma.artvid = ps.artvid'
      'inner join mat_valuta mval on mval.id=ps.valuta'
      'inner join mat_status ms on ms.id=ps.status'
      'where(  ps.godina = :mat_pp.godina and ps.broj = :mat_pp.broj'
      '     ) and (     PS.ID = :OLD_ID'
      '    and PS.BROJ = :OLD_BROJ'
      '    and PS.GODINA = :OLD_GODINA'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       ps.artsif,'
      '       cast(ps.kolicina as numeric(15,4)) as kolicina,'
      '       ps.status,'
      
        '       cast (ps.kolicina_prifatena as numeric(15,4)) as kolicina' +
        '_prifatena,'
      '       ps.opis,'
      '       ps.cena,'
      '       ps.ddv,'
      '       ps.cenaddv,'
      '       cast(ps.rabat as numeric(15,4)) as rabat,'
      '       ps.valuta,'
      '       ps.kurs,'
      '       ps.kreiral,'
      '       ps.vreme,'
      '       ma.naziv as artnaziv,'
      '       ma.merka as merka,'
      '       ma.grupa as artgrupa,'
      '       mv.opis as artvidnaziv,'
      '       mg.naziv as artgrupanaziv,'
      '       mm.naziv as MerkaNaziv,'
      '       mval.naziv as valutanaziv,'
      '       ps.krajna_cena,'
      
        '       cast((ps.krajna_cena* ps.kolicina_prifatena) as numeric(1' +
        '5, 4)) as iiznos,'
      '       ms.naziv as statusnaziv'
      'from po_ponuda_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      
        'inner join mtr_artvid mv on mv.id = ma.artvid and ma.id = ps.art' +
        'sif and ma.artvid = ps.artvid'
      
        'left outer join mtr_artgrupa mg on mg.id = ma.grupa and ma.id = ' +
        'ps.artsif and ma.artvid = ps.artvid'
      
        'inner join mat_merka mm on mm.id = ma.merka and ma.id = ps.artsi' +
        'f and ma.artvid = ps.artvid'
      'inner join mat_valuta mval on mval.id=ps.valuta'
      'inner join mat_status ms on ms.id=ps.status'
      'where ps.godina = :mas_godina and ps.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudi
    Left = 544
    Top = 1112
    object PonudiDetailID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PonudiDetailBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object PonudiDetailGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PonudiDetailARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object PonudiDetailARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object PonudiDetailKOLICINA: TFIBBCDField
      FieldName = 'KOLICINA'
    end
    object PonudiDetailSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object PonudiDetailKOLICINA_PRIFATENA: TFIBBCDField
      FieldName = 'KOLICINA_PRIFATENA'
    end
    object PonudiDetailOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object PonudiDetailDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object PonudiDetailCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object PonudiDetailRABAT: TFIBBCDField
      FieldName = 'RABAT'
    end
    object PonudiDetailVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailKURS: TFIBFloatField
      FieldName = 'KURS'
    end
    object PonudiDetailKREIRAL: TFIBStringField
      FieldName = 'KREIRAL'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailVREME: TFIBDateTimeField
      FieldName = 'VREME'
    end
    object PonudiDetailARTNAZIV: TFIBStringField
      FieldName = 'ARTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailARTGRUPA: TFIBStringField
      FieldName = 'ARTGRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailARTVIDNAZIV: TFIBStringField
      FieldName = 'ARTVIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailARTGRUPANAZIV: TFIBStringField
      FieldName = 'ARTGRUPANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailMERKANAZIV: TFIBStringField
      FieldName = 'MERKANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailVALUTANAZIV: TFIBStringField
      FieldName = 'VALUTANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiDetailKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
    object PonudiDetailIIZNOS: TFIBBCDField
      FieldName = 'IIZNOS'
      DisplayFormat = '0.00 , .'
    end
    object PonudiDetailSTATUSNAZIV: TFIBStringField
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudiDetail: TDataSource
    DataSet = PonudiDetail
    Left = 616
    Top = 1112
  end
  object frxDBNabavkaInfo: TfrxDBDataset
    UserName = 'frxDBNabavkaInfo'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'RE=RE'
      'GODINA=GODINA'
      'PREDMET=PREDMET'
      'DATUM_KREIRANA=DATUM_KREIRANA'
      'DATUM_VAZENJE=DATUM_VAZENJE'
      'DATUM_PONUDA_OD=DATUM_PONUDA_OD'
      'DATUM_PONUDA_DO=DATUM_PONUDA_DO'
      'TIP_NABAVKA=TIP_NABAVKA'
      'OPIS=OPIS'
      'TIP_KONTAKT=TIP_KONTAKT'
      'KONTAKT=KONTAKT'
      'STATUS=STATUS'
      'KREIRAL=KREIRAL'
      'IZNOS=IZNOS'
      'VALUTA=VALUTA'
      'KURS=KURS'
      'PRIORITET=PRIORITET'
      'USLOVI=USLOVI'
      'ZABELESKA=ZABELESKA'
      'VREME=VREME'
      'TIP_NABAVKA_NAZIV=TIP_NABAVKA_NAZIV'
      'PERIOD_VAZENJE=PERIOD_VAZENJE'
      'NAZIVSTATUS=NAZIVSTATUS'
      'REIDNAZIV=REIDNAZIV'
      'PARTNERNAZIV=PARTNERNAZIV'
      'VALUTANAZIV=VALUTANAZIV'
      'PRIORITETNAZIV=PRIORITETNAZIV'
      'BROJ=BROJ'
      'RENAZIV=RENAZIV'
      'BROJNABAVKA=BROJNABAVKA'
      'POTEKLO=POTEKLO'
      'IZNOSCENA=IZNOSCENA'
      'IZNOS_DENARI=IZNOS_DENARI'
      'ODOBRIL=ODOBRIL'
      'DATUM_ODOBRUVANJE=DATUM_ODOBRUVANJE'
      'BROJ_DOGOVOR=BROJ_DOGOVOR'
      'MESEC=MESEC'
      'mesecNaziv=mesecNaziv')
    DataSource = dsNabavki
    BCDToCurrency = False
    Left = 344
    Top = 1112
  end
  object frxDBPonudiPoArtikli: TfrxDBDataset
    UserName = 'frxDBPonudiPoArtikli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DATUM=DATUM'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'BROJ1=BROJ1'
      'GODINA1=GODINA1'
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'NAZIV=NAZIV'
      'NAZIV_ARTIKAL=NAZIV_ARTIKAL'
      'ARTVID_NAZIV=ARTVID_NAZIV'
      'KRAJNA_CENA=KRAJNA_CENA'
      'BROJPONUDA=BROJPONUDA'
      'KOLICINA_PRIFATENA=KOLICINA_PRIFATENA'
      'KOLICINA=KOLICINA')
    DataSource = dsPonudiPoArtikli
    BCDToCurrency = False
    Left = 448
    Top = 1112
  end
  object PonudiPoArtikli: TpFIBDataSet
    SelectSQL.Strings = (
      'select pp.datum,'
      '       pp.broj,'
      '       pp.godina,'
      '       pps.broj,'
      '       pps.godina,'
      '       pps.artvid,'
      '       pps.artsif,'
      '       pps.kolicina_prifatena,'
      '       pps.kolicina,'
      '       pp.tip_partner,'
      '       pp.partner,'
      '       mp.naziv,'
      '       pps.artsif ||'#39'. '#39'|| ma.naziv as naziv_artikal,'
      '       pps.artvid ||'#39'. '#39'|| mg.opis as artvid_naziv,'
      '       pps.krajna_cena,'
      '       pp.broj ||'#39'/'#39'|| pp.godina as brojPonuda'
      'from po_ponuda pp'
      
        'inner join po_ponuda_stavki pps on pps.broj = pp.broj and pps.go' +
        'dina = pp.godina'
      
        'inner join mat_partner mp on pp.tip_partner = mp.tip_partner and' +
        ' pp.partner = mp.id'
      
        'inner join mtr_artikal ma on ma.id = pps.artsif and pps.artvid=m' +
        'a.artvid'
      
        'inner join mtr_artvid mg on ma.artvid = mg.id and ma.id = pps.ar' +
        'tsif'
      'where  pp.nabavka_id = :nabavkaID'
      'order by pps.artvid, pps.artsif')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 1112
    object PonudiPoArtikliDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object PonudiPoArtikliBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object PonudiPoArtikliGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PonudiPoArtikliBROJ1: TFIBIntegerField
      FieldName = 'BROJ1'
    end
    object PonudiPoArtikliGODINA1: TFIBIntegerField
      FieldName = 'GODINA1'
    end
    object PonudiPoArtikliARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object PonudiPoArtikliARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object PonudiPoArtikliTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object PonudiPoArtikliPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object PonudiPoArtikliNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPoArtikliNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPoArtikliARTVID_NAZIV: TFIBStringField
      FieldName = 'ARTVID_NAZIV'
      Size = 113
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPoArtikliKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
    object PonudiPoArtikliBROJPONUDA: TFIBStringField
      FieldName = 'BROJPONUDA'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPoArtikliKOLICINA_PRIFATENA: TFIBFloatField
      FieldName = 'KOLICINA_PRIFATENA'
    end
    object PonudiPoArtikliKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
  end
  object dsPonudiPoArtikli: TDataSource
    DataSet = PonudiPoArtikli
    Left = 768
    Top = 1104
  end
  object frxDBNajdobriPonudi: TfrxDBDataset
    UserName = 'frxDBNajdobriPonudi'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTVID_OUT=ARTVID_OUT'
      'ARTSIF_OUT=ARTSIF_OUT'
      'KOLICINA_OUT=KOLICINA_OUT'
      'KOLICINAPRIFATENA_OUT=KOLICINAPRIFATENA_OUT'
      'DDV_OUT=DDV_OUT'
      'CENA_OUT=CENA_OUT'
      'GODINA_OUT=GODINA_OUT'
      'BROJ_OUT=BROJ_OUT'
      'CENADDV_OUT=CENADDV_OUT'
      'ARTNAZIV_OUT=ARTNAZIV_OUT'
      'VIDNAZIV_OUT=VIDNAZIV_OUT'
      'PARTNERNAZIV_OUT=PARTNERNAZIV_OUT'
      'KRAJNACENA_OUT=KRAJNACENA_OUT')
    DataSource = dsNajdobriPonudi
    BCDToCurrency = False
    Left = 48
    Top = 1192
  end
  object NajdobriPonudi: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select distinct * from proc_po_najdobra_ponuda(:id, :godina, :re' +
        ')'
      
        'order by proc_po_najdobra_ponuda.artvid_out, proc_po_najdobra_po' +
        'nuda.artsif_out')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 144
    Top = 1192
    object NajdobriPonudiARTVID_OUT: TFIBIntegerField
      FieldName = 'ARTVID_OUT'
    end
    object NajdobriPonudiARTSIF_OUT: TFIBIntegerField
      FieldName = 'ARTSIF_OUT'
    end
    object NajdobriPonudiKOLICINA_OUT: TFIBFloatField
      FieldName = 'KOLICINA_OUT'
    end
    object NajdobriPonudiKOLICINAPRIFATENA_OUT: TFIBFloatField
      FieldName = 'KOLICINAPRIFATENA_OUT'
    end
    object NajdobriPonudiDDV_OUT: TFIBIntegerField
      FieldName = 'DDV_OUT'
    end
    object NajdobriPonudiCENA_OUT: TFIBFloatField
      FieldName = 'CENA_OUT'
      DisplayFormat = '0.00 , .'
    end
    object NajdobriPonudiGODINA_OUT: TFIBIntegerField
      FieldName = 'GODINA_OUT'
    end
    object NajdobriPonudiBROJ_OUT: TFIBIntegerField
      FieldName = 'BROJ_OUT'
    end
    object NajdobriPonudiCENADDV_OUT: TFIBFloatField
      FieldName = 'CENADDV_OUT'
      DisplayFormat = '0.00 , .'
    end
    object NajdobriPonudiARTNAZIV_OUT: TFIBStringField
      FieldName = 'ARTNAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object NajdobriPonudiVIDNAZIV_OUT: TFIBStringField
      FieldName = 'VIDNAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object NajdobriPonudiPARTNERNAZIV_OUT: TFIBStringField
      FieldName = 'PARTNERNAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object NajdobriPonudiKRAJNACENA_OUT: TFIBFloatField
      FieldName = 'KRAJNACENA_OUT'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsNajdobriPonudi: TDataSource
    DataSet = NajdobriPonudi
    Left = 232
    Top = 1192
  end
  object dsDrvoNaIzvestai: TDataSource
    DataSet = drvoNaIzvestai
    Left = 848
    Top = 32
  end
  object drvoNaIzvestai: TpFIBDataSet
    RefreshSQL.Strings = (
      'select sr.id,sr.naslov, sr.koren'
      'from sys_report sr'
      'where sr.koren = 4000'
      'union'
      'select sr.id,sr.naslov, sr.koren'
      'from sys_report sr'
      'where sr.id = 4000')
    SelectSQL.Strings = (
      'select sr.id,sr.naslov, sr.koren'
      'from sys_report sr'
      'where sr.koren = 4000'
      'union'
      'select sr.id,sr.naslov, sr.koren'
      'from sys_report sr'
      'where sr.id = 4000')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 848
    Top = 88
    object drvoNaIzvestaiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object drvoNaIzvestaiNASLOV: TFIBStringField
      FieldName = 'NASLOV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object drvoNaIzvestaiKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
  end
  object PartneriPonuduvaci: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct pp.tip_partner, pp.partner, mp.naziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      'where pp.godina = :godina')
    SelectSQL.Strings = (
      'select distinct pp.tip_partner, pp.partner, mp.naziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      'where pp.godina = :godina')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 960
    Top = 88
    object PartneriPonuduvaciTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object PartneriPonuduvaciPARTNER: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object PartneriPonuduvaciNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPartneriPonuduvaci: TDataSource
    DataSet = PartneriPonuduvaci
    Left = 960
    Top = 32
  end
  object frxDBPonudiOdOdbranPartner: TfrxDBDataset
    UserName = 'frxDBPonudiOdOdbranPartner'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'PREDMET=PREDMET'
      'NAZIV=NAZIV'
      'STATUSNAZIV=STATUSNAZIV')
    DataSource = dsPonudiOdOdbranPartner
    BCDToCurrency = False
    Left = 48
    Top = 1272
  end
  object PonudiOdOdbranPartner: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      'select pp.broj,'
      '       pp.godina,'
      '       pp.predmet,'
      '       mp.naziv,'
      '       ms.naziv statusnaziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      'inner join mat_status ms on ms.id = pp.status'
      
        'where(  pp.partner = :partner and pp.tip_partner = :tipPartner a' +
        'nd pp.godina = :godina'
      '     ) and (     PP.BROJ = :OLD_BROJ'
      '    and PP.GODINA = :OLD_GODINA'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pp.broj,'
      '       pp.godina,'
      '       pp.predmet,'
      '       mp.naziv,'
      '       ms.naziv statusnaziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      'inner join mat_status ms on ms.id = pp.status'
      
        'where pp.partner = :partner and pp.tip_partner = :tipPartner and' +
        ' pp.godina = :godina'
      'order by mp.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 192
    Top = 1272
    object PonudiOdOdbranPartnerBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object PonudiOdOdbranPartnerGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PonudiOdOdbranPartnerPREDMET: TFIBStringField
      FieldName = 'PREDMET'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiOdOdbranPartnerNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiOdOdbranPartnerSTATUSNAZIV: TFIBStringField
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudiOdOdbranPartner: TDataSource
    DataSet = PonudiOdOdbranPartner
    Left = 328
    Top = 1272
  end
  object PonudiPartnerDetail: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ps.artvid,'
      '       ps.artsif,'
      '       pp.broj,'
      '       pp.godina,'
      '       ma.naziv as artnaziv,'
      '       mv.opis as vidnaziv,'
      '       ps.kolicina,'
      '       ps.kolicina_prifatena,'
      '       ps.cena,'
      '       ps.cenaddv,'
      '       ps.ddv,'
      '       ps.krajna_cena'
      'from po_ponuda_stavki ps'
      
        'inner join po_ponuda pp on pp.broj = ps.broj and pp.godina = ps.' +
        'godina'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      'inner join mtr_artvid mv on mv.id = ma.artvid'
      'where ps.godina = :mas_godina and ps.broj = :mas_broj')
    SelectSQL.Strings = (
      'select ps.artvid,'
      '       ps.artsif,'
      '       pp.broj,'
      '       pp.godina,'
      '       ma.naziv as artnaziv,'
      '       mv.opis as vidnaziv,'
      '       ps.kolicina,'
      '       ps.kolicina_prifatena,'
      '       ps.cena,'
      '       ps.cenaddv,'
      '       ps.ddv,'
      '       ps.krajna_cena'
      'from po_ponuda_stavki ps'
      
        'inner join po_ponuda pp on pp.broj = ps.broj and pp.godina = ps.' +
        'godina'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      'inner join mtr_artvid mv on mv.id = ma.artvid'
      'where ps.godina = :mas_godina and ps.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudiOdOdbranPartner
    Left = 576
    Top = 1272
    object PonudiPartnerDetailARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object PonudiPartnerDetailARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object PonudiPartnerDetailBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object PonudiPartnerDetailGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PonudiPartnerDetailARTNAZIV: TFIBStringField
      FieldName = 'ARTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPartnerDetailVIDNAZIV: TFIBStringField
      FieldName = 'VIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPartnerDetailKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object PonudiPartnerDetailKOLICINA_PRIFATENA: TFIBFloatField
      FieldName = 'KOLICINA_PRIFATENA'
    end
    object PonudiPartnerDetailCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object PonudiPartnerDetailCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object PonudiPartnerDetailDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object PonudiPartnerDetailKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsPonudiPartnerDetail: TDataSource
    DataSet = PonudiPartnerDetail
    Left = 680
    Top = 1272
  end
  object frxDBPonudiPartnerDetail: TfrxDBDataset
    UserName = 'frxDBPonudiPartnerDetail'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ARTNAZIV=ARTNAZIV'
      'VIDNAZIV=VIDNAZIV'
      'KOLICINA=KOLICINA'
      'KOLICINA_PRIFATENA=KOLICINA_PRIFATENA'
      'CENA=CENA'
      'CENADDV=CENADDV'
      'DDV=DDV'
      'KRAJNA_CENA=KRAJNA_CENA')
    DataSource = dsPonudiPartnerDetail
    BCDToCurrency = False
    Left = 464
    Top = 1272
  end
  object PrifateniPonudiOdOdbranPartner: TpFIBDataSet
    RefreshSQL.Strings = (
      'select pp.broj,'
      '       pp.godina,'
      '       pp.predmet,'
      '       mp.naziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      
        'where(  pp.partner = :partner and pp.tip_partner = :tipPartner a' +
        'nd pp.status = :status and pp.godina = :godina'
      '     ) and (     PP.BROJ = :OLD_BROJ'
      '    and PP.GODINA = :OLD_GODINA'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pp.broj,'
      '       pp.godina,'
      '       pp.predmet,'
      '       mp.naziv'
      'from po_ponuda pp'
      
        'inner join mat_partner mp on mp.tip_partner = pp.tip_partner and' +
        ' mp.id = pp.partner'
      
        'where pp.partner = :partner and pp.tip_partner = :tipPartner and' +
        ' pp.status = :status and pp.godina = :godina')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 232
    Top = 1344
  end
  object dsPrifateniPonudiOdOdbranPartner: TDataSource
    DataSet = PrifateniPonudiOdOdbranPartner
    Left = 400
    Top = 1344
  end
  object frxDBPrifateniPonudiOdOdbranPartner: TfrxDBDataset
    UserName = 'frxDBPrifateniPonudiOdOdbranPartner'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'PREDMET=PREDMET'
      'NAZIV=NAZIV')
    DataSource = dsPrifateniPonudiOdOdbranPartner
    BCDToCurrency = False
    Left = 56
    Top = 1344
  end
  object frxDBPrifateniPonudiPartnerDetail: TfrxDBDataset
    UserName = 'frxDBPrifateniPonudiPartnerDetail'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTVID=ARTVID'
      'ARTSIF=ARTSIF'
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ARTNAZIV=ARTNAZIV'
      'VIDNAZIV=VIDNAZIV'
      'KOLICINA=KOLICINA'
      'KOLICINA_PRIFATENA=KOLICINA_PRIFATENA'
      'CENA=CENA'
      'CENADDV=CENADDV'
      'DDV=DDV'
      'KRAJNA_CENA=KRAJNA_CENA')
    DataSource = dsPrifateniPonudiPartnerDetail
    BCDToCurrency = False
    Left = 576
    Top = 1344
  end
  object PrifateniPonudiPartnerDetail: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ps.artvid,'
      '       ps.artsif,'
      '       pp.broj,'
      '       pp.godina,'
      '       ma.naziv as artnaziv,'
      '       mv.opis as vidnaziv,'
      '       ps.kolicina,'
      '       ps.kolicina_prifatena,'
      '       ps.cena,'
      '       ps.cenaddv,'
      '       ps.ddv,'
      '       ps.krajna_cena'
      'from po_ponuda_stavki ps'
      
        'inner join po_ponuda pp on pp.broj = ps.broj and pp.godina = ps.' +
        'godina'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      'inner join mtr_artvid mv on mv.id = ma.artvid'
      'where ps.godina = :mas_godina and ps.broj = :mas_broj')
    SelectSQL.Strings = (
      'select ps.artvid,'
      '       ps.artsif,'
      '       pp.broj,'
      '       pp.godina,'
      '       ma.naziv as artnaziv,'
      '       mv.opis as vidnaziv,'
      '       ps.kolicina,'
      '       ps.kolicina_prifatena,'
      '       ps.cena,'
      '       ps.cenaddv,'
      '       ps.ddv,'
      '       ps.krajna_cena'
      'from po_ponuda_stavki ps'
      
        'inner join po_ponuda pp on pp.broj = ps.broj and pp.godina = ps.' +
        'godina'
      
        'inner join mtr_artikal ma on ma.id = ps.artsif and ma.artvid = p' +
        's.artvid'
      'inner join mtr_artvid mv on mv.id = ma.artvid'
      'where ps.godina = :mas_godina and ps.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPrifateniPonudiOdOdbranPartner
    Left = 720
    Top = 1344
    object PrifateniPonudiPartnerDetailARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object PrifateniPonudiPartnerDetailARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object PrifateniPonudiPartnerDetailBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
    object PrifateniPonudiPartnerDetailGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PrifateniPonudiPartnerDetailARTNAZIV: TFIBStringField
      FieldName = 'ARTNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrifateniPonudiPartnerDetailVIDNAZIV: TFIBStringField
      FieldName = 'VIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrifateniPonudiPartnerDetailKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object PrifateniPonudiPartnerDetailKOLICINA_PRIFATENA: TFIBFloatField
      FieldName = 'KOLICINA_PRIFATENA'
    end
    object PrifateniPonudiPartnerDetailCENA: TFIBBCDField
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object PrifateniPonudiPartnerDetailCENADDV: TFIBBCDField
      FieldName = 'CENADDV'
      DisplayFormat = '0.00 , .'
    end
    object PrifateniPonudiPartnerDetailDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object PrifateniPonudiPartnerDetailKRAJNA_CENA: TFIBFloatField
      FieldName = 'KRAJNA_CENA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsPrifateniPonudiPartnerDetail: TDataSource
    DataSet = PrifateniPonudiPartnerDetail
    Left = 792
    Top = 1344
  end
  object MaxSifraPartner: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_SIFRA_PARTNER(?TIPPARTNER)')
    Left = 1040
    Top = 560
  end
  object MaxSifraKontakt: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_PO_SIFRA_KONTAKT')
    Left = 1136
    Top = 560
  end
  object tblSETUP: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_SETUP'
      'SET '
      '    PARAMETAR = :PARAMETAR,'
      '    PARAM_VREDNOST = :PARAM_VREDNOST,'
      '    VREDNOST = :VREDNOST,'
      '    P1 = :P1,'
      '    P2 = :P2,'
      '    V1 = :V1,'
      '    V2 = :V2'
      'WHERE p1= :old_p1 and p2 = :old_p2'
      ''
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_SETUP'
      'WHERE p1= :old_p1 and p2 = :old_p2'
      '        ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_SETUP('
      '    PARAMETAR,'
      '    PARAM_VREDNOST,'
      '    VREDNOST,'
      '    P1,'
      '    P2,'
      '    V1,'
      '    V2'
      ')'
      'VALUES('
      '    :PARAMETAR,'
      '    :PARAM_VREDNOST,'
      '    :VREDNOST,'
      '    :P1,'
      '    :P2,'
      '    :V1,'
      '    :V2'
      ')')
    RefreshSQL.Strings = (
      'select ss.parametar,'
      '       ss.param_vrednost,'
      '       ss.vrednost,'
      '       ss.p1,'
      '       ss.p2,'
      '       ss.v1,'
      '       ss.v2'
      'from sys_setup ss'
      'WHERE p1= :old_p1 and p2 = :old_p2')
    SelectSQL.Strings = (
      'select ss.parametar,'
      '       ss.param_vrednost,'
      '       ss.vrednost,'
      '       ss.p1,'
      '       ss.p2,'
      '       ss.v1,'
      '       ss.v2'
      'from sys_setup ss'
      'where ss.p1 = '#39'PO'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 776
    Top = 192
    object tblSETUPPARAMETAR: TFIBStringField
      DisplayLabel = #1055#1072#1088#1072#1084#1077#1090#1072#1088
      FieldName = 'PARAMETAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSETUPPARAM_VREDNOST: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090' '#1085#1072' '#1087#1072#1088#1072#1084#1077#1090#1072#1088
      FieldName = 'PARAM_VREDNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSETUPVREDNOST: TFIBIntegerField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'VREDNOST'
    end
    object tblSETUPP1: TFIBStringField
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSETUPP2: TFIBStringField
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSETUPV1: TFIBStringField
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSETUPV2: TFIBStringField
      FieldName = 'V2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSETUP: TDataSource
    DataSet = tblSETUP
    Left = 776
    Top = 136
  end
  object tblSys_Setup: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ss.parametar,'
      '       ss.param_vrednost,'
      '       ss.vrednost,'
      '       ss.p1,'
      '       ss.p2,'
      '       ss.v1,'
      '       ss.v2'
      'from sys_setup ss')
    SelectSQL.Strings = (
      'select ss.parametar,'
      '       ss.param_vrednost,'
      '       ss.vrednost,'
      '       ss.p1,'
      '       ss.p2,'
      '       ss.v1,'
      '       ss.v2'
      'from sys_setup ss')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 848
    Top = 192
    object tblSys_SetupPARAMETAR: TFIBStringField
      FieldName = 'PARAMETAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSys_SetupPARAM_VREDNOST: TFIBStringField
      FieldName = 'PARAM_VREDNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSys_SetupVREDNOST: TFIBIntegerField
      FieldName = 'VREDNOST'
    end
    object tblSys_SetupP1: TFIBStringField
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSys_SetupP2: TFIBStringField
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSys_SetupV1: TFIBStringField
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSys_SetupV2: TFIBStringField
      FieldName = 'V2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSys_Setup: TDataSource
    DataSet = tblSys_Setup
    Left = 848
    Top = 136
  end
  object frxMailExport2: TfrxMailExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ShowExportDialog = False
    Signature.Strings = (
      '')
    SmtpPort = 25
    UseIniFile = True
    TimeOut = 30
    ConfurmReading = False
    UseMAPI = SMTP
    Left = 32
    Top = 1472
  end
  object tblNabavkaMail: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PO_NABAVKA_MAIL'
      'SET '
      '    NABAVKA_ID = :NABAVKA_ID,'
      '    DATUM = :DATUM,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    KONTAKT = :KONTAKT,'
      '    NASLOV = :NASLOV,'
      '    TEXT = :TEXT,'
      '    MAIL_SENDER = :MAIL_SENDER,'
      '    MAIL_RECIPIENT = :MAIL_RECIPIENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PO_NABAVKA_MAIL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PO_NABAVKA_MAIL('
      '    ID,'
      '    NABAVKA_ID,'
      '    DATUM,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    KONTAKT,'
      '    NASLOV,'
      '    TEXT,'
      '    MAIL_SENDER,'
      '    MAIL_RECIPIENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :NABAVKA_ID,'
      '    :DATUM,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :KONTAKT,'
      '    :NASLOV,'
      '    :TEXT,'
      '    :MAIL_SENDER,'
      '    :MAIL_RECIPIENT'
      ')')
    RefreshSQL.Strings = (
      'select pnm.id,'
      '       pnm.nabavka_id,'
      '       pnm.datum,'
      '       pnm.tip_partner,'
      '       pnm.partner,'
      '       pnm.kontakt,'
      '       pnm.naslov,'
      '       pnm.text,'
      '       pnm.mail_sender,'
      '       pnm.mail_recipient,'
      '       mp.naziv,'
      
        '       (select m.naziv from mat_kontakt m where m.tip_partner = ' +
        'pnm.tip_partner and m.partner = pnm.partner and m.id = pnm.konta' +
        'kt)as kontaktNaziv'
      'from po_nabavka_mail pnm'
      
        'inner join mat_partner mp on mp.tip_partner = pnm.tip_partner an' +
        'd mp.id = pnm.partner'
      'where(  pnm.nabavka_id = :nabavkaID'
      '     ) and (     PNM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pnm.id,'
      '       pnm.nabavka_id,'
      '       pnm.datum,'
      '       pnm.tip_partner,'
      '       pnm.partner,'
      '       pnm.kontakt,'
      '       pnm.naslov,'
      '       pnm.text,'
      '       pnm.mail_sender,'
      '       pnm.mail_recipient,'
      '       mp.naziv,'
      
        '       (select m.naziv from mat_kontakt m where m.tip_partner = ' +
        'pnm.tip_partner and m.partner = pnm.partner and m.id = pnm.konta' +
        'kt)as kontaktNaziv'
      'from po_nabavka_mail pnm'
      
        'inner join mat_partner mp on mp.tip_partner = pnm.tip_partner an' +
        'd mp.id = pnm.partner'
      'where pnm.nabavka_id = :nabavkaID')
    AfterInsert = tblNabavkaMailAfterInsert
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 944
    Top = 192
    object tblNabavkaMailID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblNabavkaMailNABAVKA_ID: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'NABAVKA_ID'
    end
    object tblNabavkaMailDATUM: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
      FieldName = 'DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tblNabavkaMailTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'TIP_PARTNER'
    end
    object tblNabavkaMailPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER'
    end
    object tblNabavkaMailKONTAKT: TFIBIntegerField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090
      FieldName = 'KONTAKT'
    end
    object tblNabavkaMailNASLOV: TFIBStringField
      DisplayLabel = #1053#1072#1089#1083#1086#1074
      FieldName = 'NASLOV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabavkaMailTEXT: TFIBBlobField
      DisplayLabel = #1058#1077#1082#1089#1090
      FieldName = 'TEXT'
      Size = 8
    end
    object tblNabavkaMailNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabavkaMailKONTAKTNAZIV: TFIBStringField
      FieldName = 'KONTAKTNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabavkaMailMAIL_SENDER: TFIBStringField
      DisplayLabel = #1048#1089#1087#1088#1072#1116#1072#1095' '#1085#1072' mail'
      FieldName = 'MAIL_SENDER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNabavkaMailMAIL_RECIPIENT: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1072#1095' '#1085#1072' mail'
      FieldName = 'MAIL_RECIPIENT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNabavkaMail: TDataSource
    DataSet = tblNabavkaMail
    Left = 936
    Top = 136
  end
  object NabavkaMailID: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_NABAVKAMAIL_ID ')
    Left = 1128
    Top = 256
  end
  object CountStavkiPlan: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'execute procedure PROC_PO_COUNTSTAVKIPLAN (?BROJ, ?GODINA, ?ARTS' +
        'IF, ?ARTVID)')
    Left = 1136
    Top = 312
  end
  object MKolicinaRealizacija: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select p.artvid_out,mv.opis, p.artsif_out,ma.naziv, p.kolicinana' +
        'bavka_out, p.kolicinaponuda_out, p.kolicinanalog_out, p.kolicina' +
        'priemnica_out'
      'from proc_po_mkolicinarealizacija(:godina, :nabavkaID) p'
      
        'inner join mtr_artikal ma on ma.artvid = p.artvid_out and ma.id ' +
        '= p.artsif_out'
      'inner join mtr_artvid mv on mv.id = ma.artvid')
    SelectSQL.Strings = (
      
        'select p.artvid_out,mv.opis, p.artsif_out,ma.naziv, p.kolicinana' +
        'bavka_out, p.kolicinaponuda_out, p.kolicinanalog_out, p.kolicina' +
        'priemnica_out'
      'from proc_po_mkolicinarealizacija(:godina, :nabavkaID) p'
      
        'inner join mtr_artikal ma on ma.artvid = p.artvid_out and ma.id ' +
        '= p.artsif_out'
      'inner join mtr_artvid mv on mv.id = ma.artvid')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1056
    Top = 88
    object MKolicinaRealizacijaARTVID_OUT: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072'(ID)'
      FieldName = 'ARTVID_OUT'
    end
    object MKolicinaRealizacijaARTSIF_OUT: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF_OUT'
    end
    object MKolicinaRealizacijaKOLICINANABAVKA_OUT: TFIBIntegerField
      DisplayLabel = #1055#1088#1077#1076#1074#1080#1076#1077#1085#1086
      FieldName = 'KOLICINANABAVKA_OUT'
    end
    object MKolicinaRealizacijaKOLICINAPONUDA_OUT: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1092#1072#1090#1077#1085#1086
      FieldName = 'KOLICINAPONUDA_OUT'
    end
    object MKolicinaRealizacijaOPIS: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object MKolicinaRealizacijaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object MKolicinaRealizacijaKOLICINANALOG_OUT: TFIBIntegerField
      DisplayLabel = #1055#1086#1088#1072#1095#1072#1085#1086
      FieldName = 'KOLICINANALOG_OUT'
    end
    object MKolicinaRealizacijaKOLICINAPRIEMNICA_OUT: TFIBIntegerField
      DisplayLabel = #1048#1089#1087#1086#1088#1072#1095#1072#1085#1086
      FieldName = 'KOLICINAPRIEMNICA_OUT'
    end
  end
  object dsMKolicinaRealizacija: TDataSource
    DataSet = MKolicinaRealizacija
    Left = 1072
    Top = 32
  end
  object MKolicinaRealizacijaDetail: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select p.vidartikalgen_out, p.sifraartikalgen_out, p.artvid_out,' +
        ' mv.opis,p.artsif_out,m.naziv, p.kolicinaponuda_out, p.kolicinan' +
        'alog_out, p.kolicinapriemnica_out'
      'from PROC_PO_MGENREALIZACIJAKOLICINA (:godina, :nabavkaID)p'
      
        'inner join mtr_artikal m on m.id = p.artsif_out and m.artvid = p' +
        '.artvid_out'
      'inner join mtr_artvid mv on mv.id = m.artvid'
      '')
    SelectSQL.Strings = (
      
        'select p.vidartikalgen_out, p.sifraartikalgen_out, p.artvid_out,' +
        ' mv.opis,p.artsif_out,m.naziv, p.kolicinaponuda_out, p.kolicinan' +
        'alog_out, p.kolicinapriemnica_out'
      'from PROC_PO_MGENREALIZACIJAKOLICINA (:godina, :nabavkaID)p'
      
        'inner join mtr_artikal m on m.id = p.artsif_out and m.artvid = p' +
        '.artvid_out'
      'inner join mtr_artvid mv on mv.id = m.artvid'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1176
    Top = 88
    object MKolicinaRealizacijaDetailVIDARTIKALGEN_OUT: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '#1075#1088#1091#1087#1072' (ID)'
      FieldName = 'VIDARTIKALGEN_OUT'
    end
    object MKolicinaRealizacijaDetailSIFRAARTIKALGEN_OUT: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '#1096#1080#1092#1088#1072
      FieldName = 'SIFRAARTIKALGEN_OUT'
    end
    object MKolicinaRealizacijaDetailARTVID_OUT: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072'(ID)'
      FieldName = 'ARTVID_OUT'
    end
    object MKolicinaRealizacijaDetailOPIS: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object MKolicinaRealizacijaDetailARTSIF_OUT: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF_OUT'
    end
    object MKolicinaRealizacijaDetailNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object MKolicinaRealizacijaDetailKOLICINAPONUDA_OUT: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1092#1072#1090#1077#1085#1072
      FieldName = 'KOLICINAPONUDA_OUT'
    end
    object MKolicinaRealizacijaDetailKOLICINANALOG_OUT: TFIBIntegerField
      DisplayLabel = #1055#1086#1088#1072#1095#1072#1085#1072
      FieldName = 'KOLICINANALOG_OUT'
    end
    object MKolicinaRealizacijaDetailKOLICINAPRIEMNICA_OUT: TFIBIntegerField
      DisplayLabel = #1048#1089#1087#1086#1088#1072#1095#1072#1085#1072
      FieldName = 'KOLICINAPRIEMNICA_OUT'
    end
  end
  object dsMKolicinaRealizacijaDetail: TDataSource
    DataSet = MKolicinaRealizacijaDetail
    Left = 1184
    Top = 32
  end
  object frxDBRealizacijaKolicina: TfrxDBDataset
    UserName = 'frxDBRealizacijaKolicina'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTVID_OUT=ARTVID_OUT'
      'ARTSIF_OUT=ARTSIF_OUT'
      'KOLICINANABAVKA_OUT=KOLICINANABAVKA_OUT'
      'KOLICINAPONUDA_OUT=KOLICINAPONUDA_OUT'
      'OPIS=OPIS'
      'NAZIV=NAZIV'
      'KOLICINANALOG_OUT=KOLICINANALOG_OUT'
      'KOLICINAPRIEMNICA_OUT=KOLICINAPRIEMNICA_OUT')
    DataSource = dsMKolicinaRealizacija
    BCDToCurrency = False
    Left = 48
    Top = 1400
  end
  object frxDBRealizacijaKolicinaDetail: TfrxDBDataset
    UserName = 'frxDBRealizacijaKolicinaDetail'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VIDARTIKALGEN_OUT=VIDARTIKALGEN_OUT'
      'SIFRAARTIKALGEN_OUT=SIFRAARTIKALGEN_OUT'
      'ARTVID_OUT=ARTVID_OUT'
      'OPIS=OPIS'
      'ARTSIF_OUT=ARTSIF_OUT'
      'NAZIV=NAZIV'
      'KOLICINAPONUDA_OUT=KOLICINAPONUDA_OUT'
      'KOLICINANALOG_OUT=KOLICINANALOG_OUT'
      'KOLICINAPRIEMNICA_OUT=KOLICINAPRIEMNICA_OUT')
    DataSource = dsFrxRealizacijaKolicina
    BCDToCurrency = False
    Left = 200
    Top = 1400
  end
  object frxRealizacijaKolicinaDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.vidartikalgen_out, p.sifraartikalgen_out, p.artvid_out,' +
        ' mv.opis,p.artsif_out,m.naziv, p.kolicinaponuda_out, p.kolicinan' +
        'alog_out, p.kolicinapriemnica_out'
      'from PROC_PO_MGENREALIZACIJAKOLICINA (:godina, :nabavkaID)p'
      
        'inner join mtr_artikal m on m.id = p.artsif_out and m.artvid = p' +
        '.artvid_out'
      'inner join mtr_artvid mv on mv.id = m.artvid'
      
        'where p.vidartikalgen_out = :mas_artvid_out and p.sifraartikalge' +
        'n_out = :mas_artsif_out')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsMKolicinaRealizacija
    Left = 344
    Top = 1400
    object frxRealizacijaKolicinaDetailVIDARTIKALGEN_OUT: TFIBIntegerField
      FieldName = 'VIDARTIKALGEN_OUT'
    end
    object frxRealizacijaKolicinaDetailSIFRAARTIKALGEN_OUT: TFIBIntegerField
      FieldName = 'SIFRAARTIKALGEN_OUT'
    end
    object frxRealizacijaKolicinaDetailARTVID_OUT: TFIBIntegerField
      FieldName = 'ARTVID_OUT'
    end
    object frxRealizacijaKolicinaDetailOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object frxRealizacijaKolicinaDetailARTSIF_OUT: TFIBIntegerField
      FieldName = 'ARTSIF_OUT'
    end
    object frxRealizacijaKolicinaDetailNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object frxRealizacijaKolicinaDetailKOLICINAPONUDA_OUT: TFIBIntegerField
      FieldName = 'KOLICINAPONUDA_OUT'
    end
    object frxRealizacijaKolicinaDetailKOLICINANALOG_OUT: TFIBIntegerField
      FieldName = 'KOLICINANALOG_OUT'
    end
    object frxRealizacijaKolicinaDetailKOLICINAPRIEMNICA_OUT: TFIBIntegerField
      FieldName = 'KOLICINAPRIEMNICA_OUT'
    end
  end
  object dsFrxRealizacijaKolicina: TDataSource
    DataSet = frxRealizacijaKolicinaDetail
    Left = 472
    Top = 1400
  end
  object SumaKolicinaRealizacija: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select sum(p.kolicinaponuda_out), sum(p.kolicinanalog_out), sum(' +
        'p.kolicinapriemnica_out)'
      'from PROC_PO_MGENREALIZACIJAKOLICINA (:godina, :nabavkaID)p'
      
        'inner join mtr_artikal m on m.id = p.artsif_out and m.artvid = p' +
        '.artvid_out'
      'inner join mtr_artvid mv on mv.id = m.artvid')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1048
    Top = 192
    object SumaKolicinaRealizacijaSUM: TFIBBCDField
      FieldName = 'SUM'
      Size = 0
    end
    object SumaKolicinaRealizacijaSUM1: TFIBBCDField
      FieldName = 'SUM1'
      Size = 0
    end
    object SumaKolicinaRealizacijaSUM2: TFIBBCDField
      FieldName = 'SUM2'
      Size = 0
    end
  end
  object dsSumaKolicinaRealizacija: TDataSource
    DataSet = SumaKolicinaRealizacija
    Left = 1048
    Top = 136
  end
  object tblSysFrmConf: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_FRM_CONF'
      'SET '
      '    USERID = :USERID,'
      '    APPS = :APPS,'
      '    FRM = :FRM,'
      '    OBJECT = :OBJECT,'
      '    PARAM = :PARAM,'
      '    VAL = :VAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_FRM_CONF'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_FRM_CONF('
      '    ID,'
      '    USERID,'
      '    APPS,'
      '    FRM,'
      '    OBJECT,'
      '    PARAM,'
      '    VAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :USERID,'
      '    :APPS,'
      '    :FRM,'
      '    :OBJECT,'
      '    :PARAM,'
      '    :VAL'
      ')')
    RefreshSQL.Strings = (
      'select sfc.id,'
      '       sfc.userid,'
      '       sfc.apps,'
      '       sfc.frm,'
      '       sfc.object,'
      '       sfc.param,'
      '       sfc.val'
      'from sys_frm_conf sfc'
      'where(  sfc.apps = :apps'
      '     ) and (     SFC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select sfc.id,'
      '       sfc.userid,'
      '       sfc.apps,'
      '       sfc.frm,'
      '       sfc.object,'
      '       sfc.param,'
      '       sfc.val'
      'from sys_frm_conf sfc'
      'where sfc.apps = :apps')
    AutoUpdateOptions.UpdateTableName = 'SYS_FRM_CONF'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_SYS_FRM_CONF_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1176
    Top = 192
    object tblSysFrmConfID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblSysFrmConfUSERID: TFIBStringField
      FieldName = 'USERID'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSysFrmConfAPPS: TFIBStringField
      FieldName = 'APPS'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSysFrmConfFRM: TFIBStringField
      FieldName = 'FRM'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSysFrmConfOBJECT: TFIBStringField
      FieldName = 'OBJECT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSysFrmConfPARAM: TFIBStringField
      FieldName = 'PARAM'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSysFrmConfVAL: TFIBStringField
      FieldName = 'VAL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dstblSysFrmConf: TDataSource
    DataSet = tblSysFrmConf
    Left = 1184
    Top = 136
  end
  object frxJPEGExport1: TfrxJPEGExport
    FileName = 'BaranjeZaPonuda.jpg'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Left = 144
    Top = 1472
  end
  object RealizPlanSektori: TpFIBDataSet
    RefreshSQL.Strings = (
      ' select p.nabavkacena_out,'
      '       p.vidnaziv_out,'
      '       p.artnaziv_out,'
      '       p.re_out,'
      '       p.artsif_out,'
      '       p.artvid_out,'
      '       p.renaziv_out,'
      '       p.plancena_out'
      
        'from proc_po_realizacijaplanposektor(:godina, :spisok, :poteklo)' +
        ' p')
    SelectSQL.Strings = (
      ' select p.nabavkacena_out,'
      '       p.vidnaziv_out,'
      '       p.artnaziv_out,'
      '       p.re_out,'
      '       p.artsif_out,'
      '       p.artvid_out,'
      '       p.renaziv_out,'
      '       p.plancena_out'
      
        'from proc_po_realizacijaplanposektor(:godina, :spisok, :poteklo)' +
        ' p')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1296
    Top = 136
    object RealizPlanSektoriNABAVKACENA_OUT: TFIBIntegerField
      DisplayLabel = #1053#1072#1073#1072#1074#1082#1072'/'#1062#1077#1085#1072
      FieldName = 'NABAVKACENA_OUT'
      DisplayFormat = '0.00 , .'
    end
    object RealizPlanSektoriVIDNAZIV_OUT: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'VIDNAZIV_OUT'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object RealizPlanSektoriARTNAZIV_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTNAZIV_OUT'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object RealizPlanSektoriRE_OUT: TFIBIntegerField
      DisplayLabel = #1057#1077#1082#1090#1086#1088' (ID)'
      FieldName = 'RE_OUT'
    end
    object RealizPlanSektoriARTSIF_OUT: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTSIF_OUT'
    end
    object RealizPlanSektoriARTVID_OUT: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'ARTVID_OUT'
    end
    object RealizPlanSektoriRENAZIV_OUT: TFIBStringField
      DisplayLabel = #1057#1077#1082#1090#1086#1088
      FieldName = 'RENAZIV_OUT'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object RealizPlanSektoriPLANCENA_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1085'/'#1062#1077#1085#1072
      FieldName = 'PLANCENA_OUT'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsRealizPlanSektori: TDataSource
    DataSet = RealizPlanSektori
    Left = 1296
    Top = 88
  end
  object InsertStavkiNalog: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_PO_INSERTSTAVKINALOG (?STATUS_IN,?KREIRAL' +
        '_IN,?GODINA_NALOG,?BROJ_NALOG,?BROJ_IN,?GODINA_IN)')
    Left = 728
    Top = 560
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object CountNalogPonudaNab: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_PO_COUNTNALOZIPONUDI(?NABAVKAID)')
    Left = 1232
    Top = 264
  end
  object tblSysSektorPlan: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mr.id, mr.naziv'
      'from mat_re mr'
      
        'where(  '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 ' +
        '= '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mr.id || '#39','#39'  || '#39'%'#39
      '     ) and (     MR.ID = :OLD_ID'
      '     )'
      '    '
      '      ')
    SelectSQL.Strings = (
      'select mr.id, mr.naziv'
      'from mat_re mr'
      
        'where '#39','#39' || (SELECT V1 FROM sys_setup WHERE P1 = '#39'PO'#39' and P2 = ' +
        #39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || '#39','#39' || mr.id || '#39','#39'  || '#39'%'#39
      '      ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1408
    Top = 136
    object tblSysSektorPlanID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSysSektorPlanNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSysSektorPlan: TDataSource
    DataSet = tblSysSektorPlan
    Left = 1400
    Top = 88
  end
  object tblStatus: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.id, m.grupa, m.naziv, m.boja'
      'from mat_status m'
      'left outer join mat_status_grupa g on g.id = m.grupa'
      'where g.app = :app and g.naziv = :naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 240
    Top = 1472
    object tblStatusID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblStatusGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object tblStatusNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStatusBOJA: TFIBIntegerField
      FieldName = 'BOJA'
    end
  end
  object dsStatus: TDataSource
    DataSet = tblStatus
    Left = 296
    Top = 1472
  end
  object tblGodisenPlan: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.broj,'
      '       ps.godina,'
      '       ps.artvid,'
      '       mv.opis as vidNaziv,'
      '       --ps.artsif,'
      '      -- ma.naziv as artikalNaziv,'
      '       p.datum_od,'
      '       p.datum_do,'
      '       p.re,'
      '       p.opis,'
      '       case when extractmonth(p.datum_od) = 1 then '#39#1032#1072#1085#1091#1072#1088#1080#39
      '              when extractmonth(p.datum_od) = 2 then '#39#1092#1077#1074#1088#1091#1072#1088#1080#39
      '              when extractmonth(p.datum_od) = 3 then '#39#1052#1072#1088#1090#39
      '              when extractmonth(p.datum_od) = 4 then '#39#1040#1087#1088#1080#1083#39
      '              when extractmonth(p.datum_od) = 5 then '#39#1052#1072#1112#39
      '              when extractmonth(p.datum_od) = 6 then '#39#1032#1091#1085#1080#39
      '              when extractmonth(p.datum_od) = 7 then '#39#1032#1091#1083#1080#39
      '              when extractmonth(p.datum_od) = 8 then '#39#1040#1074#1075#1091#1089#1090#39
      '              when extractmonth(p.datum_od) = 9 then '#39#1057#1077#1087#1090#1077#1084#1074#1088#1080#39
      '              when extractmonth(p.datum_od) = 10 then '#39#1054#1082#1090#1086#1084#1074#1088#1080#39
      '              when extractmonth(p.datum_od) = 11 then '#39#1053#1086#1077#1084#1074#1088#1080#39
      '              when extractmonth(p.datum_od) = 12 then '#39#1044#1077#1082#1077#1084#1074#1088#1080#39
      '         end "mesecNaziv",'
      '       sum(ps.cenaddv * ps.kolicina)as iznos'
      'from po_plan_stavki ps'
      
        'inner join po_plan p on p.broj = ps.broj and p.godina = ps.godin' +
        'a'
      'inner join mat_status ms on ms.id = p.status'
      
        'inner join mtr_artikal ma on ma.artvid = ps.artvid and ps.artsif' +
        ' = ma.id'
      'inner join mtr_artvid mv on mv.id = ma.artvid'
      'where ps.godina = :godina and p.status = :status'
      '      and ( p.re in (select MAT_RE.ID'
      '                      from mat_re'
      '                      where  MAT_RE.SPISOK like :spisok'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39'))'
      '           or p.re in (select MAT_RE.ID'
      '                      from mat_re'
      
        '                      where ( (MAT_RE.POTEKLO like :poteklo) or(' +
        ' mat_re.id like :poteklo))'
      
        '                              and ('#39','#39' || (SELECT V1 FROM sys_se' +
        'tup WHERE P1 = '#39'PO'#39' and P2 = '#39'SEKTORI_PLAN'#39') || '#39','#39' like '#39'%'#39' || ' +
        #39','#39' || mat_re.id || '#39','#39'  || '#39'%'#39')))'
      'group by  ps.broj,'
      '          ps.godina,'
      '          ps.artvid,'
      '          vidNaziv,'
      '          --ps.artsif,'
      '          --artikalNaziv,'
      '          p.datum_od,'
      '          p.datum_do,'
      '          p.re,'
      '          p.opis,'
      '          "mesecNaziv"'
      ''
      'order by p.datum_od, vidNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 32
    Top = 1560
    object tblGodisenPlanBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblGodisenPlanGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblGodisenPlanARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' (ID)'
      FieldName = 'ARTVID'
    end
    object tblGodisenPlanVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'VIDNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodisenPlanDATUM_OD: TFIBDateField
      DisplayLabel = #1055#1086#1095#1077#1090#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1085
      FieldName = 'DATUM_OD'
    end
    object tblGodisenPlanDATUM_DO: TFIBDateField
      DisplayLabel = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1085
      FieldName = 'DATUM_DO'
    end
    object tblGodisenPlanRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' (ID)'
      FieldName = 'RE'
    end
    object tblGodisenPlanOPIS: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodisenPlanmesecNaziv: TFIBStringField
      DisplayLabel = #1052#1077#1089#1077#1094
      FieldName = 'mesecNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodisenPlanIZNOS: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1087#1083#1072#1085
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsGodisenPlan: TDataSource
    DataSet = tblGodisenPlan
    Left = 120
    Top = 1560
  end
  object frxDBGodisenPlan: TfrxDBDataset
    UserName = 'frxGodisenPlan'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'ARTVID=ARTVID'
      'VIDNAZIV=VIDNAZIV'
      'DATUM_OD=DATUM_OD'
      'DATUM_DO=DATUM_DO'
      'RE=RE'
      'OPIS=OPIS'
      'mesecNaziv=mesecNaziv'
      'IZNOS=IZNOS')
    DataSet = tblGodisenPlan
    BCDToCurrency = False
    Left = 200
    Top = 1560
  end
end
