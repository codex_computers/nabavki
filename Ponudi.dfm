﻿object frmEvidencijaZaPonudi: TfrmEvidencijaZaPonudi
  Left = 221
  Top = 186
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1080
  ClientHeight = 763
  ClientWidth = 1020
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1020
    Height = 32
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 0
    object DBText1: TDBText
      Left = 174
      Top = 8
      Width = 47
      Height = 16
      AutoSize = True
      DataField = 'BROJNABAVKA'
      DataSource = dm.dsNabavki
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object DBText2: TDBText
      Left = 81
      Top = 8
      Width = 47
      Height = 16
      AutoSize = True
      DataField = 'GODINA'
      DataSource = dm.dsPonudi
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object cxLabel1: TcxLabel
      Left = 5
      Top = 5
      Caption = #1055#1086#1085#1091#1076#1080' '#1079#1072' '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clHighlight
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clNavy
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      AnchorY = 15
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 158
    Width = 1020
    Height = 238
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 1
    object cxPageControl1: TcxPageControl
      Left = 2
      Top = 2
      Width = 1016
      Height = 234
      Align = alClient
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      Properties.NavigatorPosition = npLeftTop
      OnPageChanging = cxPageControl1PageChanging
      ClientRectBottom = 234
      ClientRectRight = 1016
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        Color = clBtnFace
        ImageIndex = 0
        ParentColor = False
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1016
          Height = 210
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid1DBTableView1KeyDown
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPonudi
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1SNAZIV: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'SNAZIV'
              Width = 118
            end
            object cxGrid1DBTableView1BROJPONUDA: TcxGridDBColumn
              Caption = ' '#1041#1088'. '#1087#1086#1085#1091#1076#1072
              DataBinding.FieldName = 'PONUDABROJ'
              Width = 88
            end
            object cxGrid1DBTableView1BrojNabavka: TcxGridDBColumn
              Caption = #1041#1088'. '#1085#1072#1073#1072#1074#1082#1072
              DataBinding.FieldName = 'BROJNABAVKA'
              Width = 104
            end
            object cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_DOGOVOR'
              Width = 90
            end
            object cxGrid1DBTableView1PredmetPonuda: TcxGridDBColumn
              DataBinding.FieldName = 'PREDMETPONUDA'
              Width = 201
            end
            object cxGrid1DBTableView1PNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PNAZIV'
              Width = 246
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 127
            end
            object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_VAZENJE'
              Width = 120
            end
            object cxGrid1DBTableView1NABAVKA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'NABAVKA_ID'
              Visible = False
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Visible = False
            end
            object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER'
              Visible = False
            end
            object cxGrid1DBTableView1KONTAKT: TcxGridDBColumn
              DataBinding.FieldName = 'KONTAKT'
              Visible = False
            end
            object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
              DataBinding.FieldName = 'ZABELESKA'
              Visible = False
            end
            object cxGrid1DBTableView1USLOVI: TcxGridDBColumn
              DataBinding.FieldName = 'USLOVI'
              Visible = False
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1NACIN_PLAKJANJE: TcxGridDBColumn
              DataBinding.FieldName = 'NACIN_PLAKJANJE'
              Visible = False
            end
            object cxGrid1DBTableView1KREIRAL: TcxGridDBColumn
              DataBinding.FieldName = 'KREIRAL'
              Visible = False
            end
            object cxGrid1DBTableView1VREME: TcxGridDBColumn
              DataBinding.FieldName = 'VREME'
              Visible = False
            end
            object cxGrid1DBTableView1NRE: TcxGridDBColumn
              DataBinding.FieldName = 'NRE'
              Visible = False
            end
            object cxGrid1DBTableView1NBROJ: TcxGridDBColumn
              DataBinding.FieldName = 'NBROJ'
              Visible = False
            end
            object cxGrid1DBTableView1NGODINA: TcxGridDBColumn
              DataBinding.FieldName = 'NGODINA'
              Visible = False
            end
            object cxGrid1DBTableView1KNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'KNAZIV'
              Visible = False
            end
            object cxGrid1DBTableView1PLAKANJENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PLAKANJENAZIV'
              Visible = False
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA'
              Visible = False
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Visible = False
            end
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
            end
            object cxGrid1DBTableView1PREDMETNABAVKA: TcxGridDBColumn
              DataBinding.FieldName = 'PREDMETNABAVKA'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        Color = clBtnFace
        ImageIndex = 1
        ParentColor = False
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1016
          Height = 210
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          DesignSize = (
            1016
            210)
          object Label3: TLabel
            Left = 737
            Top = 12
            Width = 65
            Height = 13
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cxDBMemo1: TcxDBMemo
            Left = 737
            Top = 31
            DataBinding.DataField = 'ZABELESKA'
            DataBinding.DataSource = dm.dsPonudi
            StyleFocused.BorderStyle = ebsNone
            StyleFocused.Color = 16113353
            TabOrder = 2
            OnKeyDown = EnterKakoTab
            Height = 114
            Width = 248
          end
          object GroupBox1: TGroupBox
            Left = 7
            Top = -3
            Width = 330
            Height = 200
            Caption = #1055#1086#1085#1091#1076#1072
            TabOrder = 0
            object Label6: TLabel
              Left = 96
              Top = 15
              Width = 25
              Height = 13
              Caption = #1041#1088#1086#1112
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label11: TLabel
              Left = 14
              Top = 15
              Width = 42
              Height = 13
              Caption = #1043#1086#1076#1080#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label10: TLabel
              Left = 179
              Top = 15
              Width = 41
              Height = 13
              Caption = #1057#1090#1072#1090#1091#1089
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label1: TLabel
              Left = 14
              Top = 91
              Width = 50
              Height = 13
              Caption = #1053#1072#1073#1072#1074#1082#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label9: TLabel
              Left = 14
              Top = 135
              Width = 103
              Height = 13
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1085#1091#1076#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label2: TLabel
              Left = 170
              Top = 135
              Width = 109
              Height = 13
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label5: TLabel
              Left = 14
              Top = 58
              Width = 53
              Height = 13
              Caption = #1055#1088#1077#1076#1084#1077#1090
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object PBroj: TcxDBTextEdit
              Tag = 1
              Left = 96
              Top = 31
              BeepOnEnter = False
              DataBinding.DataField = 'BROJ'
              DataBinding.DataSource = dm.dsPonudi
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 1
              OnKeyDown = EnterKakoTab
              Width = 81
            end
            object PGodina: TcxDBComboBox
              Tag = 1
              Left = 14
              Top = 31
              BeepOnEnter = False
              DataBinding.DataField = 'GODINA'
              DataBinding.DataSource = dm.dsPonudi
              Enabled = False
              Properties.Items.Strings = (
                '2000'
                '2001'
                '2002'
                '2003'
                '2004'
                '2005'
                '2006'
                '2007'
                '2008'
                '2009'
                '2010'
                '2011'
                '2012'
                '2013'
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023'
                '2024'
                '2025'
                '2026'
                '2027'
                '2028'
                '2029'
                '2030'
                '2031'
                '2032'
                '2033'
                '2034'
                '2035'
                '2036'
                '2037'
                '2038'
                '2039'
                '2040'
                '2041'
                '2042'
                '2043'
                '2044'
                '2045'
                '2046'
                '2047'
                '2048'
                '2049'
                '2050')
              Properties.OnEditValueChanged = PGodinaPropertiesEditValueChanged
              Style.Color = 16775924
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 0
              OnKeyDown = EnterKakoTab
              Width = 81
            end
            object PStatus: TcxDBLookupComboBox
              Tag = 1
              Left = 178
              Top = 31
              BeepOnEnter = False
              DataBinding.DataField = 'STATUS'
              DataBinding.DataSource = dm.dsPonudi
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'NAZIV'
                end>
              Properties.ListSource = dm.dsStatusPonuda
              Style.Color = 16775924
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 2
              OnKeyDown = EnterKakoTab
              Width = 135
            end
            object PNabavka: TcxDBLookupComboBox
              Left = 70
              Top = 91
              BeepOnEnter = False
              DataBinding.DataField = 'NABAVKA_ID'
              DataBinding.DataSource = dm.dsPonudi
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.DropDownWidth = 50
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  Caption = #1057#1090#1072#1090#1091#1089
                  Width = 150
                  FieldName = 'NAZIVSTATUS'
                end
                item
                  Width = 150
                  FieldName = 'BROJNABAVKA'
                end
                item
                  Width = 150
                  FieldName = 'PREDMET'
                end
                item
                  Width = 150
                  FieldName = 'BROJ_DOGOVOR'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsNabavki
              Properties.OnEditValueChanged = PNabavkaPropertiesEditValueChanged
              Style.Color = 16775924
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 4
              OnKeyDown = EnterKakoTab
              Width = 243
            end
            object PredmetNabavka: TcxDBTextEdit
              Left = 14
              Top = 115
              BeepOnEnter = False
              DataBinding.DataField = 'PREDMETNABAVKA'
              DataBinding.DataSource = dm.dsPonudi
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 5
              OnKeyDown = EnterKakoTab
              Width = 299
            end
            object PDatumVazenje: TcxDBDateEdit
              Left = 170
              Top = 151
              BeepOnEnter = False
              DataBinding.DataField = 'DATUM_VAZENJE'
              DataBinding.DataSource = dm.dsPonudi
              Style.Color = 16775924
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 7
              OnKeyDown = EnterKakoTab
              Width = 143
            end
            object PDatumKreiran: TcxDBDateEdit
              Tag = 1
              Left = 14
              Top = 151
              BeepOnEnter = False
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dm.dsPonudi
              Style.Color = 16775924
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 6
              OnKeyDown = EnterKakoTab
              Width = 143
            end
            object PKreiral: TcxDBTextEdit
              Left = 14
              Top = 176
              BeepOnEnter = False
              DataBinding.DataField = 'KREIRAL'
              DataBinding.DataSource = dm.dsPonudi
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 8
              OnKeyDown = EnterKakoTab
              Width = 143
            end
            object PVreme: TcxDBTextEdit
              Left = 170
              Top = 176
              BeepOnEnter = False
              DataBinding.DataField = 'VREME'
              DataBinding.DataSource = dm.dsPonudi
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 9
              OnKeyDown = EnterKakoTab
              Width = 143
            end
            object PredmetPonuda: TcxDBMemo
              Left = 70
              Top = 58
              DataBinding.DataField = 'PREDMETPONUDA'
              DataBinding.DataSource = dm.dsPonudi
              Style.BorderColor = clHotLight
              Style.BorderStyle = ebsUltraFlat
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 3
              OnKeyDown = EnterKakoTab
              Height = 30
              Width = 243
            end
          end
          object GroupBox2: TGroupBox
            Left = 343
            Top = -3
            Width = 374
            Height = 200
            Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095
            TabOrder = 1
            object Label16: TLabel
              Left = 11
              Top = 15
              Width = 50
              Height = 13
              Caption = #1055#1072#1088#1090#1085#1077#1088
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Контакт: TLabel
              Left = 11
              Top = 60
              Width = 80
              Height = 13
              Caption = #1050#1086#1085#1090#1072#1082#1090' '#1083#1080#1094#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label8: TLabel
              Left = 11
              Top = 104
              Width = 108
              Height = 13
              Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 11
              Top = 147
              Width = 41
              Height = 13
              Caption = #1059#1089#1083#1086#1074#1080
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object PPartner: TcxExtLookupComboBox
              Tag = 1
              Left = 93
              Top = 31
              BeepOnEnter = False
              Properties.DropDownAutoSize = True
              Properties.View = cxGridViewRepository1DBTableView1
              Properties.KeyFieldNames = 'TIP_PARTNER;ID'
              Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
              Style.Color = 16775924
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 2
              OnExit = PPartnerExit
              OnKeyDown = EnterKakoTab
              Width = 268
            end
            object PPartnerID: TcxDBTextEdit
              Tag = 1
              Left = 52
              Top = 31
              BeepOnEnter = False
              DataBinding.DataField = 'PARTNER'
              DataBinding.DataSource = dm.dsPonudi
              Properties.OnEditValueChanged = PPartnerIDPropertiesEditValueChanged
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 1
              OnKeyDown = EnterKakoTab
              Width = 41
            end
            object PTipPartner: TcxDBTextEdit
              Tag = 1
              Left = 11
              Top = 31
              BeepOnEnter = False
              DataBinding.DataField = 'TIP_PARTNER'
              DataBinding.DataSource = dm.dsPonudi
              Properties.OnEditValueChanged = PTipPartnerPropertiesEditValueChanged
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 0
              OnKeyDown = EnterKakoTab
              Width = 41
            end
            object PKontaktNaziv: TcxExtLookupComboBox
              Left = 52
              Top = 77
              BeepOnEnter = False
              Properties.DropDownListStyle = lsFixedList
              Properties.DropDownSizeable = True
              Properties.View = cxGridViewRepository1DBTableView2
              Properties.KeyFieldNames = 'ID'
              Properties.ListFieldItem = cxGridViewRepository1DBTableView2NAZIV
              Style.Color = 16775924
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 4
              OnExit = PKontaktNazivExit
              OnKeyDown = EnterKakoTab
              Width = 309
            end
            object PKontaktID: TcxDBTextEdit
              Left = 11
              Top = 77
              BeepOnEnter = False
              DataBinding.DataField = 'KONTAKT'
              DataBinding.DataSource = dm.dsPonudi
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 3
              OnExit = PKontaktIDExit
              OnKeyDown = EnterKakoTab
              Width = 41
            end
            object PPlakanje: TcxDBLookupComboBox
              Left = 11
              Top = 123
              BeepOnEnter = False
              DataBinding.DataField = 'NACIN_PLAKJANJE'
              DataBinding.DataSource = dm.dsPonudi
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'NAZIV'
                end>
              Properties.ListSource = dm.dsPlakanje
              Style.Color = 16775924
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 5
              OnKeyDown = EnterKakoTab
              Width = 350
            end
            object PUslovi: TcxDBTextEdit
              Left = 11
              Top = 166
              BeepOnEnter = False
              DataBinding.DataField = 'USLOVI'
              DataBinding.DataSource = dm.dsPonudi
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 6
              OnKeyDown = EnterKakoTab
              Width = 350
            end
          end
          object Otkazi: TcxButton
            Left = 910
            Top = 167
            Width = 75
            Height = 25
            Action = OtkaziClick
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 4
          end
          object Zacuvaj: TcxButton
            Left = 829
            Top = 167
            Width = 75
            Height = 25
            Action = ZacuvajClick
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 3
          end
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 396
    Width = 1020
    Height = 32
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 2
    object DBText3: TDBText
      Left = 219
      Top = 9
      Width = 47
      Height = 16
      AutoSize = True
      DataField = 'PONUDABROJ'
      DataSource = dm.dsPonudi
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxLabel3: TcxLabel
      Left = 2
      Top = 2
      Align = alLeft
      Caption = ' '#1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1072' '#1073#1088#1086#1112' '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clHighlight
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clNavy
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      AnchorY = 16
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 428
    Width = 1020
    Height = 310
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 3
    object cxPageControl2: TcxPageControl
      Left = 2
      Top = 2
      Width = 1016
      Height = 306
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheet3
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 306
      ClientRectRight = 1016
      ClientRectTop = 24
      object cxTabSheet3: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 2
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1016
          Height = 282
          Align = alClient
          Caption = 'Panel5'
          TabOrder = 0
          object cxGrid3: TcxGrid
            Left = 1
            Top = 1
            Width = 1014
            Height = 280
            Align = alClient
            TabOrder = 0
            object cxGrid3DBTableView1: TcxGridDBTableView
              OnKeyDown = cxGrid3DBTableView1KeyDown
              OnKeyPress = cxGrid3DBTableView1KeyPress
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dm.dsPoPonudaStavki
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '0.00 , .'
                  Kind = skSum
                  Column = cxGrid3DBTableView1IIZNOS
                end>
              DataController.Summary.SummaryGroups = <>
              FilterRow.Visible = True
              OptionsBehavior.IncSearch = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              object cxGrid3DBTableView1STATUSNAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'STATUSNAZIV'
                Width = 103
              end
              object cxGrid3DBTableView1ARTVID: TcxGridDBColumn
                DataBinding.FieldName = 'ARTVID'
                Width = 71
              end
              object cxGrid3DBTableView1ARTVIDNAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'ARTVIDNAZIV'
                Width = 143
              end
              object cxGrid3DBTableView1ARTSIF: TcxGridDBColumn
                DataBinding.FieldName = 'ARTSIF'
              end
              object cxGrid3DBTableView1ARTNAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'ARTNAZIV'
                Width = 227
              end
              object cxGrid3DBTableView1KOLICINA: TcxGridDBColumn
                Caption = #1050#1086#1083#1080#1095#1080#1085#1072
                DataBinding.FieldName = 'KOLICINA'
              end
              object cxGrid3DBTableView1KOLICINA_PRIFATENA: TcxGridDBColumn
                DataBinding.FieldName = 'KOLICINA_PRIFATENA'
                Width = 134
              end
              object cxGrid3DBTableView1CENA: TcxGridDBColumn
                DataBinding.FieldName = 'CENA'
              end
              object cxGrid3DBTableView1DDV: TcxGridDBColumn
                DataBinding.FieldName = 'DDV'
                Width = 46
              end
              object cxGrid3DBTableView1CENADDV: TcxGridDBColumn
                DataBinding.FieldName = 'CENADDV'
                Width = 81
              end
              object cxGrid3DBTableView1RABAT: TcxGridDBColumn
                DataBinding.FieldName = 'RABAT'
              end
              object cxGrid3DBTableView1KRAJNA_CENA: TcxGridDBColumn
                DataBinding.FieldName = 'KRAJNA_CENA'
                Width = 76
              end
              object cxGrid3DBTableView1IIZNOS: TcxGridDBColumn
                Caption = #1048#1079#1085#1086#1089
                DataBinding.FieldName = 'IIZNOS'
              end
              object cxGrid3DBTableView1OPIS: TcxGridDBColumn
                DataBinding.FieldName = 'OPIS'
                Width = 83
              end
              object cxGrid3DBTableView1VALUTA: TcxGridDBColumn
                DataBinding.FieldName = 'VALUTA'
                Width = 73
              end
              object cxGrid3DBTableView1VALUTANAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'VALUTANAZIV'
                Width = 66
              end
              object cxGrid3DBTableView1KURS: TcxGridDBColumn
                DataBinding.FieldName = 'KURS'
              end
              object cxGrid3DBTableView1MERKA: TcxGridDBColumn
                DataBinding.FieldName = 'MERKA'
                Width = 71
              end
              object cxGrid3DBTableView1MERKANAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'MERKANAZIV'
                Width = 118
              end
              object cxGrid3DBTableView1ARTGRUPA: TcxGridDBColumn
                DataBinding.FieldName = 'ARTGRUPA'
                Width = 80
              end
              object cxGrid3DBTableView1ARTGRUPANAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'ARTGRUPANAZIV'
                Width = 128
              end
            end
            object cxGrid3Level1: TcxGridLevel
              GridView = cxGrid3DBTableView1
            end
          end
        end
      end
      object cxTabSheet4: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 3
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1016
          Height = 282
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          DesignSize = (
            1016
            282)
          object sLabel: TLabel
            Left = 50
            Top = 148
            Width = 27
            Height = 13
            Caption = #1054#1087#1080#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 218
            Top = 210
            Width = 42
            Height = 13
            Caption = #1042#1072#1083#1091#1090#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 398
            Top = 210
            Width = 27
            Height = 13
            Caption = #1050#1091#1088#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label17: TLabel
            Left = 36
            Top = 210
            Width = 41
            Height = 13
            Caption = #1057#1090#1072#1090#1091#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 12
            Top = 167
            Width = 65
            Height = 13
            Caption = #1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SValuta: TcxDBLookupComboBox
            Tag = 1
            Left = 266
            Top = 207
            BeepOnEnter = False
            DataBinding.DataField = 'VALUTA'
            DataBinding.DataSource = dm.dsPoPonudaStavki
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsValuta
            Style.Color = 16775924
            StyleFocused.BorderStyle = ebsNone
            StyleFocused.Color = 16113353
            TabOrder = 3
            OnEnter = Iznos
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object DStatus: TcxDBLookupComboBox
            Tag = 1
            Left = 83
            Top = 207
            BeepOnEnter = False
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsPoPonudaStavki
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Caption = #1053#1072#1079#1080#1074
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dm.dsPoPonudaStavkiStatus
            Properties.OnEditValueChanged = DStatusPropertiesEditValueChanged
            Style.Color = 16775924
            StyleFocused.BorderStyle = ebsNone
            StyleFocused.Color = 16113353
            TabOrder = 2
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object SKurs: TcxDBTextEdit
            Tag = 1
            Left = 431
            Top = 207
            BeepOnEnter = False
            DataBinding.DataField = 'KURS'
            DataBinding.DataSource = dm.dsPoPonudaStavki
            StyleFocused.BorderStyle = ebsNone
            StyleFocused.Color = 16113353
            TabOrder = 4
            OnEnter = Iznos
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object SOpis: TcxDBMemo
            Left = 83
            Top = 145
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsPoPonudaStavki
            StyleFocused.BorderStyle = ebsNone
            StyleFocused.Color = 16113353
            TabOrder = 1
            OnKeyDown = EnterKakoTab
            Height = 56
            Width = 469
          end
          object GroupBox3: TGroupBox
            Left = 7
            Top = 13
            Width = 557
            Height = 126
            Caption = #1054#1089#1085#1086#1074#1085#1080' '#1082#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1079#1072' '#1072#1088#1090#1080#1082#1072#1083
            TabOrder = 0
            object Label29: TLabel
              Left = 40
              Top = 99
              Width = 50
              Height = 13
              Caption = #1045#1076'. '#1084#1077#1088#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label28: TLabel
              Left = 32
              Top = 74
              Width = 58
              Height = 13
              Caption = #1055#1086#1076#1075#1088#1091#1087#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label18: TLabel
              Left = 10
              Top = 49
              Width = 80
              Height = 13
              Caption = #1064#1080#1092#1088#1072'/'#1053#1072#1079#1080#1074
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label25: TLabel
              Left = 53
              Top = 24
              Width = 37
              Height = 13
              Caption = #1043#1088#1091#1087#1072' '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SPodgrupaNaziv: TcxDBTextEdit
              Left = 141
              Top = 71
              DataBinding.DataField = 'ARTGRUPANAZIV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              TabOrder = 0
              Width = 404
            end
            object SMerkaNaziv: TcxDBTextEdit
              Left = 141
              Top = 96
              DataBinding.DataField = 'MERKANAZIV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              TabOrder = 1
              Width = 112
            end
            object SMerkaId: TcxDBTextEdit
              Left = 96
              Top = 96
              DataBinding.DataField = 'MERKA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              TabOrder = 2
              Width = 42
            end
            object SPodgrupaId: TcxDBTextEdit
              Left = 96
              Top = 71
              DataBinding.DataField = 'ARTGRUPA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              TabOrder = 3
              Width = 42
            end
            object SNaziv: TcxDBTextEdit
              Left = 177
              Top = 46
              DataBinding.DataField = 'ARTNAZIV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              TabOrder = 4
              Width = 368
            end
            object SSifra: TcxDBTextEdit
              Left = 96
              Top = 46
              DataBinding.DataField = 'ARTSIF'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.IsFontAssigned = True
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              TabOrder = 5
              Width = 78
            end
            object SArtvid: TcxDBTextEdit
              Left = 96
              Top = 21
              DataBinding.DataField = 'ARTVID'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              TabOrder = 6
              Width = 42
            end
            object SArtvidNaziv: TcxDBTextEdit
              Left = 141
              Top = 21
              DataBinding.DataField = 'ARTVIDNAZIV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clBackground
              TabOrder = 7
              Width = 404
            end
          end
          object GroupBox4: TGroupBox
            Left = 570
            Top = 4
            Width = 243
            Height = 237
            Caption = #1055#1086#1085#1091#1076#1077#1085#1072' '#1094#1077#1085#1072' '#1080' '#1082#1086#1083#1080#1095#1080#1085#1072
            TabOrder = 5
            object Label22: TLabel
              Left = 106
              Top = 208
              Width = 34
              Height = 13
              Caption = #1048#1079#1085#1086#1089
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label19: TLabel
              Left = 69
              Top = 181
              Width = 71
              Height = 13
              Caption = #1050#1088#1072#1112#1085#1072' '#1094#1077#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label13: TLabel
              Left = 82
              Top = 153
              Width = 58
              Height = 13
              Caption = #1056#1072#1073#1072#1090'(%)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label30: TLabel
              Left = 66
              Top = 129
              Width = 74
              Height = 13
              Caption = #1062#1077#1085#1072' '#1089#1086' '#1076#1076#1074
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label31: TLabel
              Left = 90
              Top = 102
              Width = 50
              Height = 13
              Caption = #1044#1044#1042'(%)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label20: TLabel
              Left = 110
              Top = 77
              Width = 30
              Height = 13
              Caption = #1062#1077#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label12: TLabel
              Left = 18
              Top = 50
              Width = 122
              Height = 13
              Caption = #1055#1088#1080#1092#1072#1090#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label21: TLabel
              Left = 24
              Top = 23
              Width = 116
              Height = 13
              Caption = #1055#1086#1085#1091#1076#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SKrajnaCena: TcxDBTextEdit
              Left = 146
              Top = 178
              TabStop = False
              BeepOnEnter = False
              DataBinding.DataField = 'KRAJNA_CENA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = clInactiveCaptionText
              TabOrder = 6
              OnEnter = Iznos
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SRabat: TcxDBTextEdit
              Tag = 1
              Left = 146
              Top = 151
              BeepOnEnter = False
              DataBinding.DataField = 'RABAT'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 5
              OnExit = SRabatExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SCenaSoDDV: TcxDBTextEdit
              Tag = 1
              Left = 146
              Top = 126
              BeepOnEnter = False
              DataBinding.DataField = 'CENADDV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 4
              OnExit = SCenaSoDDVExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SDDV: TcxDBLookupComboBox
              Tag = 1
              Left = 146
              Top = 99
              BeepOnEnter = False
              DataBinding.DataField = 'DDV'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Properties.DropDownListStyle = lsFixedList
              Properties.KeyFieldNames = 'TARIFA'
              Properties.ListColumns = <
                item
                  FieldName = 'TARIFA'
                end>
              Properties.ListSource = dm.dsDDV
              Properties.OnEditValueChanged = SDDVPropertiesEditValueChanged
              Style.Color = 16775924
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 3
              OnKeyDown = EnterKakoTab
              Width = 67
            end
            object SCena: TcxDBTextEdit
              Tag = 1
              Left = 146
              Top = 74
              BeepOnEnter = False
              DataBinding.DataField = 'CENA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 2
              OnExit = SCenaExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SPrifatenaKolicina: TcxDBTextEdit
              Tag = 1
              Left = 146
              Top = 47
              BeepOnEnter = False
              DataBinding.DataField = 'KOLICINA_PRIFATENA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 1
              OnExit = SPrifatenaKolicinaExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SKolicina: TcxDBTextEdit
              Tag = 1
              Left = 146
              Top = 20
              BeepOnEnter = False
              DataBinding.DataField = 'KOLICINA'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = 16113353
              TabOrder = 0
              OnExit = SKolicinaExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SIznos: TcxDBTextEdit
              Left = 146
              Top = 205
              TabStop = False
              BeepOnEnter = False
              DataBinding.DataField = 'IIZNOS'
              DataBinding.DataSource = dm.dsPoPonudaStavki
              Enabled = False
              StyleDisabled.Color = 16113353
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderStyle = ebsNone
              StyleFocused.Color = clInactiveCaptionText
              TabOrder = 7
              OnEnter = Iznos
              OnKeyDown = EnterKakoTab
              Width = 86
            end
          end
          object ButtonOtkazi: TcxButton
            Left = 910
            Top = 234
            Width = 75
            Height = 25
            Action = ButtonOtkaziClick
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 7
          end
          object ButtonZacuvaj: TcxButton
            Left = 829
            Top = 234
            Width = 75
            Height = 25
            Action = ButtonZacuvajјClickk
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 6
          end
        end
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 738
    Width = 1020
    Height = 25
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1044#1086#1076#1072#1076#1080', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080',  F8 - '#1041#1088#1080#1096#1080
        Width = 276
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072', Ctrl+F8 - '#1041#1088#1080 +
          #1096#1080' '#1089#1090#1072#1074#1082#1072
        Width = 387
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F1 - '#1055#1086#1084#1086#1096', '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079#1080' Esc'
        Width = 250
      end>
    LookAndFeel.SkinName = 'Office2007Blue'
    Color = clMenuHighlight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1020
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 648
    Top = 104
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsTblPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      Styles.ContentEven = dm.cxStyle8
      Styles.ContentOdd = dm.cxStyle8
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 43
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 34
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
        DataBinding.FieldName = 'NAZIV'
        Width = 256
      end
    end
    object cxGridViewRepository1DBTableView2: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsKontaktPartner
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      Styles.ContentEven = dm.cxStyle8
      Styles.ContentOdd = dm.cxStyle8
      object cxGridViewRepository1DBTableView2ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 38
      end
      object cxGridViewRepository1DBTableView2NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 218
      end
      object cxGridViewRepository1DBTableView2TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 82
      end
      object cxGridViewRepository1DBTableView2PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER'
        Width = 84
      end
      object cxGridViewRepository1DBTableView2MAIL: TcxGridDBColumn
        DataBinding.FieldName = 'MAIL'
        Width = 135
      end
      object cxGridViewRepository1DBTableView2MOBILEN: TcxGridDBColumn
        DataBinding.FieldName = 'MOBILEN'
        Width = 108
      end
      object cxGridViewRepository1DBTableView2TEL: TcxGridDBColumn
        DataBinding.FieldName = 'TEL'
        Width = 118
      end
      object cxGridViewRepository1DBTableView2OPIS: TcxGridDBColumn
        DataBinding.FieldName = 'OPIS'
        Width = 189
      end
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Caption = #1057#1090#1072#1090#1091#1089
          end
          item
            Caption = #1043#1086#1076#1080#1085#1072
          end
          item
            Action = Action1
            ImageIndex = 23
          end>
      end
      item
      end
      item
      end
      item
      end
      item
        Items = <
          item
            ChangesAllowed = [caModify]
            Items = <
              item
                Caption = '&ActionClientItem0'
              end>
            Caption = '&ActionClientItem0'
            KeyTip = 'F'
          end>
        AutoSize = False
      end
      item
      end
      item
        AutoSize = False
      end>
    LargeImages = dm.cxLargeImages
    Images = dm.cxSmallImages
    Left = 552
    Top = 320
    StyleName = 'Platform Default'
    object aPoNabavkaStavki: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 27
    end
    object Action1: TAction
      ImageIndex = 23
      OnExecute = Action1Execute
    end
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    Left = 360
    Top = 80
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 720
    Top = 368
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 712
    Top = 104
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 920
    Top = 128
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1086#1085#1091#1076#1072
      CaptionButtons = <>
      DockedLeft = 156
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 697
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 120
      FloatClientHeight = 130
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 931
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 141
      FloatClientHeight = 216
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      CaptionButtons = <>
      DockedLeft = 506
      DockedTop = 0
      FloatLeft = 1054
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1048#1079#1074#1077#1096#1090#1072#1080
      CaptionButtons = <>
      DockedLeft = 572
      DockedTop = 0
      FloatLeft = 1054
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 331
      DockedTop = 0
      FloatLeft = 1054
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1054
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 102
          Visible = True
          ItemName = 'cxStatus'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 99
          Visible = True
          ItemName = 'cxGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      ShortCut = 117
      OnClick = aAzurirajExecute
      LargeImageIndex = 12
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1055#1086#1084#1086#1096
      Category = 0
      Enabled = False
      Visible = ivAlways
      LargeImageIndex = 24
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1053#1086#1074
      Category = 0
      Visible = ivAlways
      ShortCut = 116
      OnClick = aNovExecute
      LargeImageIndex = 10
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 39
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 38
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 37
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 38
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aZacuvajStavkiExcel
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabelaStavki
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aNalozii
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = aSpecifikacijaZaPonuda
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aPonudeniArtikli
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aNovaStavka
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aAzurirajStavkaa
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiiStavka
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxStatus: TcxBarEditItem
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      OnChange = cxStatusChange
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dm.dsStatusPonuda
    end
    object cxGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cxGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 608
    Top = 304
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1087#1086#1085#1091#1076#1080' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aZacuvajStavkiExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1089#1090#1072#1074#1082#1080' '#1074#1086' Excel'
      ImageIndex = 9
      OnExecute = aZacuvajStavkiExcelExecute
    end
    object aPecatiTabelaStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaStavkiExecute
    end
    object aNalozii: TAction
      Caption = #1053#1072#1083#1086#1079#1080
      ImageIndex = 8
      OnExecute = aNaloziiExecute
    end
    object aSpecifikacijaZaPonuda: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = aSpecifikacijaZaPonudaExecute
    end
    object aPonudeniArtikli: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080' '#1072#1088#1090#1080#1082#1083#1080
      ImageIndex = 19
      OnExecute = aPonudeniArtikliExecute
    end
    object aNovaStavka: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovaStavkaExecute
    end
    object aAzurirajStavkaa: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 16501
      OnExecute = aAzurirajStavkaaExecute
    end
    object aBrisiiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 16503
      OnExecute = aBrisiiStavkaExecute
    end
    object ButtonZacuvajјClickk: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = ButtonZacuvajјClickkExecute
    end
    object ButtonOtkaziClick: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = ButtonOtkaziClickExecute
    end
    object ZacuvajClick: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = ZacuvajClickExecute
    end
    object OtkaziClick: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = OtkaziClickExecute
    end
    object aDizajn: TAction
      Caption = 'aDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDizajnExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 368
    Top = 344
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44228.561679479160000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid3
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44228.561679525460000000
      ShrinkToPageWidth = True
      BuiltInReportLink = True
    end
  end
  object cxImageList1: TcxImageList
    SourceDPI = 96
    FormatVersion = 1
    DesignInfo = 8913216
  end
end
