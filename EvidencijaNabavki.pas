unit EvidencijaNabavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxNavigator, dxCore, cxDateUtils,
  System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmNabavki = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1NazivStatus: TcxGridDBColumn;
    cxGrid1DBTableView1BrNabavka: TcxGridDBColumn;
    cxGrid1DBTableView1RabEdinica: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1PREDMET: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1Iznos: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Broj: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PONUDA_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PONUDA_DO: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKA1: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid1DBTableView1KURS: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITET: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1VREME: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITETNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POTEKLO: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_DENARI: TcxGridDBColumn;
    cxGrid1DBTableView1ODOBRIL: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ODOBRUVANJE: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    RabEdinica: TcxLookupComboBox;
    TipNabavka: TcxLookupComboBox;
    PredmetNabavka: TcxTextEdit;
    BrojNabavka: TcxTextEdit;
    DatumKreiran: TcxDateEdit;
    ButtonClear: TButton;
    cxTabSheet2: TcxTabSheet;
    Panel2: TPanel;
    Label15: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    DPredmet: TcxDBTextEdit;
    DBroj: TcxDBTextEdit;
    DRabEdinID: TcxDBTextEdit;
    DRabEdinica: TcxDBLookupComboBox;
    DGodina: TcxDBComboBox;
    DTipNabavkaID: TcxDBTextEdit;
    DTipNabavka: TcxDBLookupComboBox;
    DStatus: TcxDBLookupComboBox;
    DPrioritet: TcxDBLookupComboBox;
    DTipKontakt: TcxDBTextEdit;
    DKontakt: TcxDBTextEdit;
    DPartner: TcxExtLookupComboBox;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label10: TLabel;
    DDatumPonudaDo: TcxDBDateEdit;
    DDatumPonudaOd: TcxDBDateEdit;
    GroupBox2: TGroupBox;
    Label26: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label11: TLabel;
    DIznosDenari: TcxDBTextEdit;
    DIznosPotrosen: TcxDBTextEdit;
    DIznos: TcxDBTextEdit;
    DValuta: TcxDBLookupComboBox;
    DKurs: TcxDBTextEdit;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    Label33: TLabel;
    Label14: TLabel;
    Label23: TLabel;
    Label22: TLabel;
    Label18: TLabel;
    DDatumVazenje: TcxDBDateEdit;
    DDatumKreirana: TcxDBTextEdit;
    DOpis: TcxDBMemo;
    DZabeleska: TcxDBMemo;
    DUslovi: TcxDBMemo;
    DKreiral: TcxDBTextEdit;
    DATUM_ODOBRUVANJE: TcxDBDateEdit;
    dxBarManager1Bar5: TdxBar;
    cxGodina: TcxBarEditItem;
    cxStatus: TcxBarEditItem;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aBaranjeZaPonuda: TAction;
    aIsprazniStatus: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    aNajdobriPonudi: TAction;
    dxBarManager1Bar8: TdxBar;
    aNalozi: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    aOdobriOtvori: TAction;
    aPoNabavkaStavki: TAction;
    aPonudi: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aPonudiZaNabavka: TAction;
    dxBarButton2: TdxBarButton;
    PonudiZaNabavkaPoArtikli: TAction;
    dxBarButton3: TdxBarButton;
    aRealizacijaKolicina: TAction;
    dxBarManager1Bar10: TdxBar;
    aStavki: TAction;
    aIscistiButton: TAction;
    OtkaziStavka: TcxButton;
    ZapisiButton: TcxButton;
    �SpecifikacijaZaOdbranaNabavka: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    dxBarButton4: TdxBarButton;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    BrojDogovor: TcxDBTextEdit;
    Label27: TLabel;
    cxGrid1DBTableView1DBColumn: TcxGridDBColumn;
    aDizajnPonudiZaNabavka: TAction;
    aDizajnPonudiZaNabavkaPoArtikli: TAction;
    aDizajnNajdobriPonudi: TAction;
    aDizajn: TAction;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    dxBarButton5: TdxBarButton;
    aDizajnSpecifikacija: TAction;
    N5: TMenuItem;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli();
    procedure frmNabavkiaDodadiExecute(Sender: TObject);
    procedure aBaranjeZaPonudaExecute(Sender: TObject);
    procedure aNajdobriPonudiExecute(Sender: TObject);
    procedure aNaloziExecute(Sender: TObject);
    procedure aOdobriOtvoriExecute(Sender: TObject);
    procedure dxBarLargeButton7Click(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aPoNabavkaStavkiExecute(Sender: TObject);
    procedure aPonudiExecute(Sender: TObject);
    procedure aPonudiZaNabavkaExecute(Sender: TObject);
    procedure PonudiZaNabavkaPoArtikliExecute(Sender: TObject);
    procedure aPrebarajExecute(Sender: TObject);
    procedure aRealizacijaKolicinaExecute(Sender: TObject);
    procedure aStavkiExecute(Sender: TObject);
    procedure aIscistiButtonExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxStatusChange(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure DGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure DRabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure DRabEdinIDPropertiesEditValueChanged(Sender: TObject);
    procedure DTipNabavkaIDPropertiesEditValueChanged(Sender: TObject);
    procedure DTipNabavkaPropertiesEditValueChanged(Sender: TObject);
    procedure Prebaraj(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGodinaChange(Sender: TObject);
    procedure �SpecifikacijaZaOdbranaNabavkaExecute(Sender: TObject);
    procedure aDizajnPonudiZaNabavkaExecute(Sender: TObject);
    procedure aDizajnPonudiZaNabavkaPoArtikliExecute(Sender: TObject);
    procedure aDizajnNajdobriPonudiExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
    procedure aDizajnSpecifikacijaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmNabavki: TfrmNabavki;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmMaticni,
  BaranjeZaPonuda, MKolicinaRealizacija, TipNabavka, Partner, RabotniEdinici,
  Valuta, SpecifikacijaNabavki, PoNabavkaStavki, Ponudi, NaloziZaIsporaka;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmNabavki.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmNabavki.aNajdobriPonudiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4010;
     dm.tblReportDizajn.FullRefresh;

     dm.NajdobriPonudi.Close;
     dm.NajdobriPonudi.ParamByName('re').Value:=dmKon.re;
     dm.NajdobriPonudi.ParamByName('id').Value:=dm.tblNabaviID.Value;
     dm.NajdobriPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
     dm.NajdobriPonudi.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.aNaloziExecute(Sender: TObject);
begin
    if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
       begin
         frmEvidencijaNaNaloziZaIsporaka:=TfrmEvidencijaNaNaloziZaIsporaka.Create(Application);
         frmEvidencijaNaNaloziZaIsporaka.Tag:=1;
         frmEvidencijaNaNaloziZaIsporaka.ShowModal;
         frmEvidencijaNaNaloziZaIsporaka.Free;
       end
      else  ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin

     cxPageControl1.ActivePage:=cxTabSheet2;
     Panel2.Enabled:= True;


     dxRibbon1.Enabled:=False;

     DTipNabavkaID.SetFocus;
     cxTabSheet1.Enabled:=false;

     DPartner.Text:='';
     DRabEdinica.EditValue:=dmKon.re;
     DRabEdinID.Text:=IntToStr(dmKon.re);

     dm.tblNabavi.Insert;
     StateActive:=dsInsert;

     dm.tblNabaviGODINA.Value:=cxGodina.EditValue;
     dm.tblNabaviID.Value:=dm.zemiMaxIDNabavka(dm.MaxIDNabavka, Null, Null, Null, Null, Null,Null, 'MAXBR');
     dm.tblNabaviBROJ.Value:=dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR');
     dm.tblNabaviRE.Value:= dmKon.re;
     if avtomatsko_odobruvanje = '0' then
        dm.tblNabaviSTATUS.Value:= otvorena_nab
     else if avtomatsko_odobruvanje = '1' then
        begin
            dm.tblNabaviSTATUS.Value:= odobrena_nab;
            dm.tblNabaviODOBRIL.Value:=dmKon.user;
            dm.tblNabaviDATUM_ODOBRUVANJE.Value:=Now;
        end;
     dm.tblNabaviVALUTA.Value:='DEN';
     dm.tblNabaviPRIORITET.Value:=1;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmNabavki.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
   if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false)  then
              ShowMessage('������ ����� �� ��������� �������� ������� !!!')
           else
              begin
                 //cxStatus.EditValue:=0;
                 cxPageControl1.ActivePage:=cxTabSheet2;
                 Panel2.Enabled:= True;
                 dxRibbon1.Enabled:=False;
                 DStatus.Enabled:=True;
                 DStatus.SetFocus;
                 cxTabSheet1.Enabled:=false;

                 DPartner.Text:=dm.tblNabaviPARTNERNAZIV.Value;
                 cxGrid1DBTableView1.DataController.DataSet.Edit;
                 StateActive:=dsEdit;
              end;
        end
     else ShowMessage('����������� ������� !!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmNabavki.aBaranjeZaPonudaExecute(Sender: TObject);
begin
     frmBaranjeZaPonuda:=TfrmBaranjeZaPonuda.Create(Application);
     frmBaranjeZaPonuda.ShowModal;
     frmBaranjeZaPonuda.Free;
end;

procedure TfrmNabavki.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)) then
     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
        begin
           if (odobriNabavka = false ) and (dm.tblNabaviSTATUS.Value = odobrena_nab) then
              ShowMessage('������ ����� �� ������� �������� ������� !!!')
           else
              begin
                 frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                 if frmDaNe.ShowModal =mrYes then
                   dm.tblNabavi.Delete;
              end;
        end
     else ShowMessage('����������� ������� !!!');

end;

//	����� �� ���������� �� ����������
procedure TfrmNabavki.aRealizacijaKolicinaExecute(Sender: TObject);
begin
     frmRealizacijaKolicina:=TfrmRealizacijaKolicina.Create(Application);
     frmRealizacijaKolicina.ShowModal;
     frmRealizacijaKolicina.free;
end;

procedure TfrmNabavki.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmNabavki.aIscistiButtonExecute(Sender: TObject);
begin
     RabEdinica.Text:= '';
     TipNabavka.Text:= '';
     PredmetNabavka.Text:= '';
     DatumKreiran.Text:= '';
     BrojNabavka.Text:= '';
     cxGrid1DBTableView1.DataController.Filter.Clear;
     cxGrid1.SetFocus;
end;

procedure TfrmNabavki.aIzlezExecute(Sender: TObject);
begin
     if (StateActive in [dsBrowse]) and (cxPageControl1.ActivePage = cxTabSheet2) then
         cxPageControl1.ActivePage:=cxTabSheet1
     else if (StateActive in [dsBrowse]) and (cxPageControl1.ActivePage = cxTabSheet1) then
          close
     else if StateActive in [dsedit,dsInsert] then
          begin
             frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
             if frmDaNe.ShowModal =mrYes then
                begin
                   dm.tblNabavi.Cancel;
                   dm.RestoreControls(panel2);
                   StateActive:=dsBrowse;
                   cxTabSheet1.Enabled:=True;
                   Panel2.Enabled:=false;
                   DStatus.Enabled:=False;
                   cxPageControl1.ActivePage:=cxTabSheet1;
                   dxRibbon1.Enabled:=true;
                   cxGrid1.SetFocus;
                end;
          end;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmNabavki.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmNabavki.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
//procedure TfrmNabavki.EnterKakoTab(Sender: TObject; var Key: Word;
//  Shift: TShiftState);
//begin
//    case Key of
//        VK_DOWN:
//        begin
//          PostMessage(Handle,WM_NextDlgCtl,0,0);
//          key:=0;
//        end;
//        VK_UP:
//        begin
//          PostMessage(Handle,WM_NextDlgCtl,1,0);
//          key:=0;
//        end;
//        VK_RETURN:
//        begin
//          PostMessage(Handle,WM_NextDlgCtl,0,0);
//        end;
//    end;
//end;


procedure TfrmNabavki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin     //DA
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
          begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
            key:=0;
          end;
        VK_UP:
         begin
           PostMessage(Handle,WM_NextDlgCtl,1,0);
           key:=0;
         end;
        VK_RETURN:
         begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
         end ;
        VK_INSERT:
          begin
             if (Sender = DPartner) or (Sender = DKontakt) then
               begin
                    frmPartner:=TfrmPartner.Create(self,false);
                    frmPartner.ShowModal;
                    if (frmPartner.ModalResult = mrOK) then
                     begin
                       dm.PartnerVraboten.FullRefresh;
                       dm.tblNabaviKONTAKT.Value := StrToInt(frmPartner.GetSifra(1));
                     end;
                    frmPartner.Free;
                    DDatumPonudaOd.SetFocus;
               end;
             if (sender = DValuta) then
               begin
                  frmValuta:=TfrmValuta.Create(self,false);
                  frmValuta.ShowModal;
                  if (frmValuta.ModalResult = mrOK) then
                     begin
                       dm.tblNabaviVALUTA.Value := frmValuta.GetSifra(0);
                     end;
                  frmValuta.Free;
                  DKurs.SetFocus;
               end;
             If (Sender = DTipNabavkaID) or(Sender = DTipNabavka) then
               begin
                  frmTipNabavka:=TfrmTipNabavka.Create(Application);
                  frmTipNabavka.Tag:=1;
                  frmTipNabavka.ShowModal;
                  frmTipNabavka.Free;
                  dm.tblNabaviTIP_NABAVKA.Value:=dm.TipNabavkiID.Value;
                  DPredmet.SetFocus;
              end;
        end;
    end;
end;


//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmNabavki.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmNabavki.cxDBTextEditAllExit(Sender: TObject);
var pom:integer;
begin
    TEdit(Sender).Color:=clWhite;
    if sender = DKontakt then
       begin
        if (StateActive in [dsInsert,dsEdit]) and (DKontakt.Text <> '') then
         begin
           pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, vraboten,strtoInt(DKontakt.text), Null, 'BROJ');
           if (pom = 1)then
               begin
                  dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, strtoInt(DKontakt.Text)]) , []);
                  DPartner.Text:= dm.PartnerVrabotenNAZIV.Value;
               end;
         end;
       end;
    if sender = DPartner then
       begin
         if (StateActive in [dsInsert,dsEdit]) and (DPartner.Text <> '') then
          begin
           dm.tblNabaviTIP_KONTAKT.Value:=dm.PartnerVrabotenTIP_PARTNER.Value;
           dm.tblNabaviKONTAKT.Value:=dm.PartnerVrabotenID.Value;
          end;
       end;

end;

procedure TfrmNabavki.cxGodinaChange(Sender: TObject);
begin
     if cxGodina.EditValue <> Null then
        dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue
     else
        dm.tblNabavi.ParamByName('godina').Value:='%';

     if  cxStatus.EditValue <> Null then
         dm.tblNabavi.ParamByName('status').Value:=cxStatus.EditValue
     else
         dm.tblNabavi.ParamByName('status').Value:='%';
     dm.tblNabavi.FullRefresh;

     cxGrid1.SetFocus;
end;

procedure TfrmNabavki.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmNabavki.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmNabavki.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmNabavki.PonudiZaNabavkaPoArtikliExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4009;
     dm.tblReportDizajn.FullRefresh;

     dm.PonudiPoArtikli.Close;
     dm.PonudiPoArtikli.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
     dm.PonudiPoArtikli.Open;

     dm.tblStavkiZaOdbranaPonuda.Close;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
     dm.tblStavkiZaOdbranaPonuda.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.aPrebarajExecute(Sender: TObject);
begin
     cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1RabEdinica, foLike, '%' + RabEdinica.EditText + '%' , RabEdinica.EditText + '%') ;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1TIP_NABAVKA, foLike, '%' + TipNabavka.EditingText + '%', TipNabavka.EditingText + '%');
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_KREIRAN, foLike, '%'+ DatumKreiran.Text+ '%',DatumKreiran.Text + '%' );
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1PREDMET, foLike, '%'+ PredmetNabavka.Text+ '%',PredmetNabavka.Text + '%' );
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1BrNabavka, foLike,  BrojNabavka.Text + '%', BrojNabavka.Text + '%');
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
end;

procedure TfrmNabavki.Prebaraj(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
        VK_UP:begin
              PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
          VK_RETURN:begin
             aPrebarajExecute(Sender);
             if Sender = BrojNabavka then
                cxGrid1.SetFocus
             else
                PostMessage(Handle,WM_NextDlgCtl,0,0);
          end;
      end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmNabavki.prefrli;
begin
end;

procedure TfrmNabavki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
       dm.tblNabavi.Close;
       dm.TipNabavki.Close;
       dmMat.tblRE.Close;

       dm.StatusNabavka.Close;
       dm.Prioritet.Close;
       dmMat.tblValuta.Close;
       dm.PartnerVraboten.Close;
end;
procedure TfrmNabavki.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  OtvoriTabeli();
  cxPageControl1.ActivePage:=cxTabSheet1;

  dm.tblSysFrmConf.Close;
  dm.tblSysFrmConf.Open;

  dm.tblSysFrmConf.Locate('USERID; APPS; FRM; OBJECT; PARAM', VarArrayOf([dmKon.user, 'PO', 'frmNabavki', 'aOdobriNabavka', 'Enabled']), []);
  odobriNabavka:= dm.tblSysFrmConfVAL.AsBoolean;
end;

//------------------------------------------------------------------------------

procedure TfrmNabavki.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

       dm.frxReport.Script.Clear;
       dm.frxReport.Clear;
       StateActive:=dsBrowse;
       procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
       cxGodina.EditValue:=dmKon.godina;
       cxGrid1.SetFocus;

end;
procedure TfrmNabavki.frmNabavkiaDodadiExecute(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TfrmNabavki.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmNabavki.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case Key of
             VK_INSERT:begin
                           if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
                             begin
                                if dm.tblNabaviSTATUS.Value = stornirana_nab then
                                   begin
                                      ShowMessage('��������� � ���������� !!!');
                                   end
                                else if dm.tblNabaviSTATUS.Value = zatvorena_nab then
                                   begin
                                      ShowMessage('��������� � ��������� !!!');
                                   end
                                else if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false) then
                                   begin
                                      ShowMessage('������ ����� �� ��������/��������� ������ �� �������� ������� !!!');
                                   end
                                else
                                   begin
                                      dm.tblPoNabavkaStavki.Close;
                                      dm.tblPoNabavkaStavki.ParamByName('NabavkaID').Value:=dm.tblNabaviID.Value;
                                      dm.tblPoNabavkaStavki.Open;
                                      FrmSpecifikacijaNabavka:=TFrmSpecifikacijaNabavka.Create(Application);
                                      FrmSpecifikacijaNabavka.ShowModal;
                                      FrmSpecifikacijaNabavka.Free;
                                   end;
                             end
                          else ShowMessage('����������� ������� !!!');
                      end;
            end;
end;

procedure TfrmNabavki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmNabavki.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if ( StateActive in [dsBrowse] ) and (cxPageControl1.ActivePage = cxTabSheet1)then
         begin
            DPartner.Text:=dm.tblNabaviPARTNERNAZIV.Value;
            cxGodina.Enabled:=false;
            cxStatus.Enabled:=False;
           // Action1.Enabled:=False;
         end;
      if (cxPageControl1.ActivePage = cxTabSheet2)then
         begin
            cxGodina.Enabled:=true;
            cxStatus.Enabled:=true;
           // Action1.Enabled:=true;
         end;
end;

procedure TfrmNabavki.cxStatusChange(Sender: TObject);
begin  //da
     if cxGodina.EditValue <> Null then
        dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue
     else
        dm.tblNabavi.ParamByName('godina').Value:='%';

     if  cxStatus.EditValue <> Null then
         dm.tblNabavi.ParamByName('status').Value:=cxStatus.EditValue
     else
         dm.tblNabavi.ParamByName('status').Value:='%';
     dm.tblNabavi.FullRefresh;

     cxGrid1.SetFocus;
end;

procedure TfrmNabavki.DGodinaPropertiesEditValueChanged(Sender: TObject);
begin   // da
     if (StateActive in [dsEdit]) and (DGodina.Text <> '') then
        begin
           if dm.tblNabaviGODINA.Value <> StrToInt(DGodina.text) then
              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'))
           else
              DBroj.Text:=IntToStr(dm.tblNabaviBROJ.Value);
         end;

       if (StateActive in [dsInsert])and (DGodina.Text <> '') then
           begin
              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'));
           end;
end;

procedure TfrmNabavki.DRabEdinicaPropertiesEditValueChanged(Sender: TObject);
var pom :integer;
begin
     if (StateActive in [dsEdit]) and (DRabEdinica.Text <>'') then
        begin
           if dm.tblNabaviRE.Value <> DRabEdinica.EditValue then
              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'))
           else
              DBroj.Text:=IntToStr(dm.tblNabaviBROJ.Value);
              DRabEdinID.Text:=IntToStr(DRabEdinica.EditValue);
          end;
     if (StateActive in [dsInsert]) and (DRabEdinica.Text <>'') then
          begin
             DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'));
             DRabEdinID.Text:=IntToStr(DRabEdinica.EditValue);
          end;
end;

procedure TfrmNabavki.DRabEdinIDPropertiesEditValueChanged(Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) and (DRabEdinID.Text <> '') then
        begin
           DRabEdinica.EditValue:=StrToInt(DRabEdinID.Text);
        end;
end;

procedure TfrmNabavki.DTipNabavkaIDPropertiesEditValueChanged(Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) and (DTipNabavkaID.Text <> '') then
        begin
           DTipNabavka.EditValue:=StrToInt(DTipNabavkaID.Text);
        end;
end;

procedure TfrmNabavki.DTipNabavkaPropertiesEditValueChanged(Sender: TObject);
begin
      if (StateActive in [dsInsert,dsEdit]) and (DTipNabavka.Text <> '') then
        begin
           if DTipNabavka.Text <> '' then
              DTipNabavkaID.Text:= IntToStr(DTipNabavka.EditValue);
        end;
end;

procedure TfrmNabavki.dxBarLargeButton7Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmNabavki.aZapisiExecute(Sender: TObject);
var pom:string;
begin
      ZapisiButton.SetFocus;
      if dm.Validacija(Panel2) = false then
        begin
           if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = False) then
              begin
                 ShowMessage('������ ����� �� ���������� ������� !!!');
                 DStatus.SetFocus;
              end
           else
              begin
                 pom:=DGodina.text;
                 if dm.tblNabaviSTATUS.Value = odobrena_nab then
                    dm.tblNabaviodobril.value:=dmKon.user;
                 cxGrid1DBTableView1.DataController.DataSet.Post;
                 StateActive:=dsBrowse;
                 dm.RestoreControls(panel2);
                 dm.tblNabavi.ParamByName('status').Value:='%';
                 dm.tblNabavi.FullRefresh;
                 cxTabSheet1.Enabled:=True;
                 Panel2.Enabled:=false;
                 DStatus.Enabled:=False;
                 cxPageControl1.ActivePage:=cxTabSheet1;

                 cxGrid1.SetFocus;
                 dxRibbon1.Enabled:=true;
                 cxGodina.Caption:=pom;
              end;
        end;
end;

//	����� �� ���������� �� �������
procedure TfrmNabavki.aOdobriOtvoriExecute(Sender: TObject);
var pom, pom2:integer;
begin
     pom2:=dm.zemiBr4(dm.CountNalogPonudaNab, 'NABAVKAID', Null, Null,Null, dm.tblnabaviID.Value, Null, Null, Null, 'COUNTT');
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           if odobriNabavka = true then
              begin
                 pom:=dm.tblNabaviSTATUS.Value;
                 if pom <> odobrena_nab then
                    begin
                       frmDaNe:=TfrmDaNe.Create(self,'������� !', '���� ��� ������� ���� ������ �� �� �������� ���������?',1);
                       if frmDaNe.ShowModal =mrYes then
                          begin
                             dm.tblNabavi.Edit;
                             dm.tblNabaviSTATUS.Value:=odobrena_nab;
                             dm.tblNabaviodobril.value:=dmKon.user;
                             dm.tblNabaviDATUM_ODOBRUVANJE.value:=Now;
                             dm.tblNabavi.Post;
                          end;
                    end
                  else if (pom = odobrena_nab) and (pom2 = 0)then
                    begin
                       frmDaNe:=TfrmDaNe.Create(self,'������� !','���� ��� ������� ���� ������ �� �� �������� ���������?',1);
                       if frmDaNe.ShowModal =mrYes then
                          begin
                             dm.tblNabavi.Edit;
                             dm.tblNabaviSTATUS.Value:=otvorena_nab;
                             dm.tblNabaviodobril.value:=dmKon.user;
                             dm.tblNabaviDATUM_ODOBRUVANJE.value:=Now;
                             dm.tblNabavi.Post;
                          end;
                    end
                 else ShowMessage('�������� �� �������� �������� ������� �� ��� ��� ��� ������������ ������ ��� ����� !!!');
                 dm.tblNabavi.Refresh;
              end
           else ShowMessage('������ ����� �� ���������� ������� !!!');
        end
     else ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aOtkaziExecute(Sender: TObject);
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
           if frmDaNe.ShowModal =mrYes then
              begin
                 dm.tblNabavi.Cancel;
                 StateActive:=dsBrowse;
                 dm.RestoreControls(panel2);

                 cxTabSheet1.Enabled:=True;
                 Panel2.Enabled:=false;
                 DStatus.Enabled:=False;
                 cxPageControl1.ActivePage:=cxTabSheet1;
                 dxRibbon1.Enabled:=true;
                 cxGrid1.SetFocus;
              end;
          end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmNabavki.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmNabavki.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmNabavki.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmNabavki.aPoNabavkaStavkiExecute(Sender: TObject);
begin
     dm.tblPoNabavkaStavki.Close;
     dm.tblPoNabavkaStavki.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblPoNabavkaStavki.Open;
     frmPoNabavkaStavki:=TfrmPoNabavkaStavki.Create(Application);
     frmPoNabavkaStavki.ShowModal;
     frmPoNabavkaStavki.Free;
end;

procedure TfrmNabavki.aPonudiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
        begin
           frmEvidencijaZaPonudi:=TfrmEvidencijaZaPonudi.Create(Application);
           frmEvidencijaZaPonudi.Tag:=1;
           frmEvidencijaZaPonudi.ShowModal;
           frmEvidencijaZaPonudi.Free;
        end
     else
        begin
           ShowMessage('����������� ������� !!!');
        end;
end;

procedure TfrmNabavki.aPonudiZaNabavkaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4008;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPonudi.Close;
     dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
     dm.tblPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
     dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
     dm.tblPonudi.ParamByName('status').Value:='%';
     dm.tblPonudi.Open;

     dm.PonudiDetail.Close;
     dm.PonudiDetail.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmNabavki.aStavkiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
        begin
           if dm.tblNabaviSTATUS.Value = stornirana_nab then
              begin
                 ShowMessage('��������� � ���������� !!!');
              end
            else if dm.tblNabaviSTATUS.Value = zatvorena_nab then
               begin
                  ShowMessage('��������� � ��������� !!!');
               end
            else if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false) then
               begin
                  ShowMessage('������ ����� �� ��������/��������� ������ �� �������� ������� !!!');
               end
            else
              begin
                 dm.tblPoNabavkaStavki.Close;
                 dm.tblPoNabavkaStavki.ParamByName('NabavkaID').Value:=dm.tblNabaviID.Value;
                 dm.tblPoNabavkaStavki.Open;
                 FrmSpecifikacijaNabavka:=TFrmSpecifikacijaNabavka.Create(Application);
                 FrmSpecifikacijaNabavka.ShowModal;
                 FrmSpecifikacijaNabavka.Free;
              end;
         end
       else ShowMessage('����������� ������� !!!');
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmNabavki.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmNabavki.aDizajnExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500);
end;

procedure TfrmNabavki.aDizajnNajdobriPonudiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4010;
     dm.tblReportDizajn.FullRefresh;

     dm.NajdobriPonudi.Close;
     dm.NajdobriPonudi.ParamByName('re').Value:=dmKon.re;
     dm.NajdobriPonudi.ParamByName('id').Value:=dm.tblNabaviID.Value;
     dm.NajdobriPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
     dm.NajdobriPonudi.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmNabavki.aDizajnPonudiZaNabavkaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4008;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPonudi.Close;
     dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
     dm.tblPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
     dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
     dm.tblPonudi.ParamByName('status').Value:='%';
     dm.tblPonudi.Open;

     dm.PonudiDetail.Close;
     dm.PonudiDetail.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmNabavki.aDizajnPonudiZaNabavkaPoArtikliExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4009;
     dm.tblReportDizajn.FullRefresh;

     dm.PonudiPoArtikli.Close;
     dm.PonudiPoArtikli.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
     dm.PonudiPoArtikli.Open;

     dm.tblStavkiZaOdbranaPonuda.Close;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
     dm.tblStavkiZaOdbranaPonuda.Open;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmNabavki.aDizajnSpecifikacijaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.frxDesignerPlan.Tag:=4;
     dm.tblReportDizajn.ParamByName('id').Value:=4004;
     dm.tblReportDizajn.FullRefresh;

     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblSpecifikacijaNaNabavka.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmNabavki.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmNabavki.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(105);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmNabavki.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

procedure TfrmNabavki.�SpecifikacijaZaOdbranaNabavkaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.frxDesignerPlan.Tag:=4;
     dm.tblReportDizajn.ParamByName('id').Value:=4004;
     dm.tblReportDizajn.FullRefresh;

     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblSpecifikacijaNaNabavka.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmNabavki.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmNabavki.OtvoriTabeli();
begin
       dm.tblNabavi.ParamByName('godina').Value:=dmKon.godina;
       dm.tblNabavi.ParamByName('status').Value:='%';
       dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
       dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
       dm.tblNabavi.Open;
       dm.TipNabavki.Open;
       dmMat.tblRE.Open;
       dm.StatusNabavka.Open;
       dm.Prioritet.Open;
       dmMat.tblValuta.Open;
       dm.PartnerVraboten.ParamByName('vraboten').Value:=vraboten;
       dm.PartnerVraboten.Open;
       dm.IzbraniSektoriZaPlan.close;
       dm.IzbraniSektoriZaPlan.ParamByName('poteklo').Value:=intToStr(dmKon.re) + ',';
       dm.IzbraniSektoriZaPlan.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
      // dm.IzbraniSektoriZaPlan.ParamByName('user').Value:=dmKon.user;
       dm.IzbraniSektoriZaPlan.Open;

       dm.Firmi.close;
       dm.Firmi.ParamByName('re').Value:=dmKon.re;
       dm.Firmi.Open;
       dm.tblReportDizajn.Open;
       dm.tblSpecifikacijaNaNabavka.Open;
end;


end.
