unit ArtikliP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, MasterRibon, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, Menus, cxLookAndFeelPainters, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxSkinsdxBarPainter, cxGridCustomPopupMenu,
  cxGridPopupMenu, ActnList, dxBar, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxButtons, ExtCtrls,
  dxRibbon;

type
  TfrmArtikliP = class(TfrmMasterRibon)
    cxGrid1DBTableView1GRUPA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1SIFRA: TcxGridDBColumn;
    cxGrid1DBTableView1NNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KKATALOG: TcxGridDBColumn;
    cxGrid1DBTableView1PODGRUPA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1PODGRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1CCENA: TcxGridDBColumn;
    cxGrid1DBTableView1BRPAKK: TcxGridDBColumn;
    cxGrid1DBTableView1BBARKOD: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELID: TcxGridDBColumn;
    cxGrid1DBTableView1KKOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1KLASIFIKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1UNINAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1UNIFONT: TcxGridDBColumn;
    cxGrid1DBTableView1OBLIK: TcxGridDBColumn;
    cxGrid1DBTableView1JACINA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKAID: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArtikliP: TfrmArtikliP;

implementation

uses dmUnit, dmKonekcija,  dmMaticni ;

{$R *.dfm}

end.
