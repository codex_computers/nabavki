unit PregledPlanZaIzbranSektor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxStatusBar, dxRibbonStatusBar, cxLookupEdit,cxGridExportLink, cxExport,
  cxDBLookupEdit, cxDBLookupComboBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls, cxGridCustomView,
  cxGrid, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, StdCtrls,
  ExtCtrls, ActnList, cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, cxNavigator, System.Actions;

type
  TfrmPlanZaizbranSektor = class(TForm)
    Panel1: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    PGodina: TcxComboBox;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1SUMAKOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DDV: TcxGridDBColumn;
    cxGrid1DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid1DBTableView1PODGRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    RabEdinica: TcxLookupComboBox;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Label2: TLabel;
    cxGrid1DBTableView1Grupa: TcxGridDBColumn;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aZacuvajVoExcel: TAction;
    aPecati: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aZacuvajIzgled: TAction;
    aDizajn: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure RabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure PGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanZaizbranSektor: TfrmPlanZaizbranSektor;

implementation

uses dmKonekcija, dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmPlanZaizbranSektor.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin

     dm.tblReportDizajn.ParamByName('id').Value:=4002;
     dm.tblReportDizajn.FullRefresh;

//     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=RabEdinica.EditValue;
//     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=PGodina.EditValue;
//     dm.tblPlanZaIzbraniotSektor.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan2.fr3');

     dm.frxReport.DesignReport();
end;

procedure TfrmPlanZaizbranSektor.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPlanZaizbranSektor.aPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin

     dm.tblReportDizajn.ParamByName('id').Value:=4002;
     dm.tblReportDizajn.FullRefresh;

//     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=RabEdinica.EditValue;
//     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=PGodina.EditValue;
//     dm.tblPlanZaIzbraniotSektor.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan2.fr3');

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmPlanZaizbranSektor.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmPlanZaizbranSektor.aZacuvajVoExcelExecute(Sender: TObject);
begin
       zacuvajVoExcel(cxGrid1, Caption);
//     dmMat.SaveToExcelDialog.FileName := '���� �� ������ ������' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmPlanZaizbranSektor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.tblPlanZaIzbraniotSektor.Close;
end;

procedure TfrmPlanZaizbranSektor.FormCreate(Sender: TObject);
begin
      dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=PGodina.EditValue;
      dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=RabEdinica.EditValue;
      dm.tblPlanZaIzbraniotSektor.Open;
      dm.IzbraniSektoriZaPlan.close;
      dm.IzbraniSektoriZaPlan.ParamByName('poteklo').Value:=intToStr(dmKon.re) + ',';
      dm.IzbraniSektoriZaPlan.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
      //dm.IzbraniSektoriZaPlan.ParamByName('user').Value:=dmKon.user;
      dm.IzbraniSektoriZaPlan.Open;
end;

procedure TfrmPlanZaizbranSektor.FormShow(Sender: TObject);
begin
      procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
      RabEdinica.EditValue:=dmKon.re;
      PGodina.EditValue:=dmKon.godina;
      cxGrid1.SetFocus;
      procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
end;

procedure TfrmPlanZaizbranSektor.PGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=PGodina.EditValue;
     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=RabEdinica.EditValue;
     dm.tblPlanZaIzbraniotSektor.FullRefresh;
end;

procedure TfrmPlanZaizbranSektor.RabEdinicaPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPlanZaIzbraniotSektor.ParamByName('godina').Value:=PGodina.EditValue;
     dm.tblPlanZaIzbraniotSektor.ParamByName('re').Value:=RabEdinica.EditValue;
     dm.tblPlanZaIzbraniotSektor.FullRefresh;
end;

end.
