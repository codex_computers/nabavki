unit Ponudi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxControls, dxStatusBar,
  dxRibbonStatusBar, RibbonLunaStyleActnCtrls, Ribbon, cxContainer, cxEdit,
  cxLabel, ExtCtrls, dxSkinscxPCPainter, cxPC, StdCtrls, RibbonActnCtrls,
  ToolWin, ActnMan, ActnCtrls, ActnList, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxGridExportLink, cxExport,
  cxDBLookupComboBox, cxTextEdit, cxDBEdit, cxDBExtLookupComboBox, cxCalendar,
  DBCtrls, cxSpinEdit, cxMemo, ActnMenus, RibbonActnMenus, cxPropertiesStore,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinSeven, dxSkinSharp, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter, dxRibbon, dxBar, cxBarEditItem, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPSCore, dxPScxCommon, PlatformDefaultStyleActnCtrls, Menus,
  cxButtons, dxSkinStardust, cxPCdxBarPopupMenu, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, ImgList, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxBarBuiltInMenu,
  cxNavigator, dxRibbonCustomizationForm, System.ImageList, cxImageList,
  System.Actions;

type
  TfrmEvidencijaZaPonudi = class(TForm)
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Panel8: TPanel;
    Panel3: TPanel;
    cxLabel3: TcxLabel;
    Panel4: TPanel;
    cxPageControl2: TcxPageControl;
    cxGridViewRepository1: TcxGridViewRepository;
    Label3: TLabel;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    ActionManager1: TActionManager;
    aPoNabavkaStavki: TAction;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGridViewRepository1DBTableView2: TcxGridDBTableView;
    cxGridViewRepository1DBTableView2ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MAIL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MOBILEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TEL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1SNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJPONUDA: TcxGridDBColumn;
    DBText3: TDBText;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxTabSheet3: TcxTabSheet;
    Panel5: TPanel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid3DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA_PRIFATENA: TcxGridDBColumn;
    cxGrid3DBTableView1OPIS: TcxGridDBColumn;
    cxGrid3DBTableView1CENA: TcxGridDBColumn;
    cxGrid3DBTableView1DDV: TcxGridDBColumn;
    cxGrid3DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid3DBTableView1RABAT: TcxGridDBColumn;
    cxGrid3DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid3DBTableView1KURS: TcxGridDBColumn;
    cxGrid3DBTableView1ARTNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1ARTGRUPA: TcxGridDBColumn;
    cxGrid3DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ARTGRUPANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1VALUTANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1IIZNOS: TcxGridDBColumn;
    cxGrid3DBTableView1STATUSNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1KRAJNA_CENA: TcxGridDBColumn;
    cxTabSheet4: TcxTabSheet;
    Panel9: TPanel;
    sLabel: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    SValuta: TcxDBLookupComboBox;
    DStatus: TcxDBLookupComboBox;
    cxGrid1DBTableView1BrojNabavka: TcxGridDBColumn;
    SKurs: TcxDBTextEdit;
    DBText1: TDBText;
    DBText2: TDBText;
    Action1: TAction;
    cxDBMemo1: TcxDBMemo;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label1: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    PBroj: TcxDBTextEdit;
    PGodina: TcxDBComboBox;
    PStatus: TcxDBLookupComboBox;
    PNabavka: TcxDBLookupComboBox;
    PredmetNabavka: TcxDBTextEdit;
    PDatumVazenje: TcxDBDateEdit;
    PDatumKreiran: TcxDBDateEdit;
    PKreiral: TcxDBTextEdit;
    PVreme: TcxDBTextEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    �������: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    PPartner: TcxExtLookupComboBox;
    PPartnerID: TcxDBTextEdit;
    PTipPartner: TcxDBTextEdit;
    PKontaktNaziv: TcxExtLookupComboBox;
    PKontaktID: TcxDBTextEdit;
    PPlakanje: TcxDBLookupComboBox;
    PUslovi: TcxDBTextEdit;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    Label5: TLabel;
    PredmetPonuda: TcxDBMemo;
    cxGrid1DBTableView1PredmetPonuda: TcxGridDBColumn;
    SOpis: TcxDBMemo;
    GroupBox3: TGroupBox;
    Label29: TLabel;
    Label28: TLabel;
    Label18: TLabel;
    Label25: TLabel;
    SPodgrupaNaziv: TcxDBTextEdit;
    SMerkaNaziv: TcxDBTextEdit;
    SMerkaId: TcxDBTextEdit;
    SPodgrupaId: TcxDBTextEdit;
    SNaziv: TcxDBTextEdit;
    SSifra: TcxDBTextEdit;
    SArtvid: TcxDBTextEdit;
    SArtvidNaziv: TcxDBTextEdit;
    GroupBox4: TGroupBox;
    Label22: TLabel;
    Label19: TLabel;
    Label13: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label20: TLabel;
    Label12: TLabel;
    Label21: TLabel;
    SKrajnaCena: TcxDBTextEdit;
    SRabat: TcxDBTextEdit;
    SCenaSoDDV: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCena: TcxDBTextEdit;
    SPrifatenaKolicina: TcxDBTextEdit;
    SKolicina: TcxDBTextEdit;
    Label7: TLabel;
    SIznos: TcxDBTextEdit;
    cxPropertiesStore1: TcxPropertiesStore;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    aZacuvajStavkiExcel: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    aPecatiTabelaStavki: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarButton1: TdxBarButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    aNalozii: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    aSpecifikacijaZaPonuda: TAction;
    dxBarButton2: TdxBarButton;
    aPonudeniArtikli: TAction;
    dxBarButton3: TdxBarButton;
    aNovaStavka: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton16: TdxBarLargeButton;
    aAzurirajStavkaa: TAction;
    aBrisiiStavka: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    cxStatus: TcxBarEditItem;
    cxGodina: TcxBarEditItem;
    ButtonOtkazi: TcxButton;
    ButtonZacuvaj: TcxButton;
    Otkazi: TcxButton;
    Zacuvaj: TcxButton;
    ButtonZacuvaj�Clickk: TAction;
    ButtonOtkaziClick: TAction;
    ZacuvajClick: TAction;
    OtkaziClick: TAction;
    cxGrid1DBTableView1NABAVKA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1NACIN_PLAKJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1VREME: TcxGridDBColumn;
    cxGrid1DBTableView1NBROJ: TcxGridDBColumn;
    cxGrid1DBTableView1NRE: TcxGridDBColumn;
    cxGrid1DBTableView1NGODINA: TcxGridDBColumn;
    cxGrid1DBTableView1KNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PLAKANJENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1PREDMETNABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxImageList1: TcxImageList;
    aDizajn: TAction;
    procedure aDodadiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure Iznos (Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PNabavkaPropertiesEditValueChanged(Sender: TObject);
    procedure PPartnerIDPropertiesEditValueChanged(Sender: TObject);
    procedure PTipPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure PGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid3DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SCenaExit(Sender: TObject);
    procedure SDDVPropertiesEditValueChanged(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure SRabatPropertiesEditValueChanged(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure aZacuvajEcelPlanExecute(Sender: TObject);
    procedure aZacuvajExelStavkaExecute(Sender: TObject);
    procedure DStatusPropertiesEditValueChanged(Sender: TObject);
    procedure SKolicinaExit(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
    procedure SRabatExit(Sender: TObject);
    procedure SPrifatenaKolicinaExit(Sender: TObject);
    procedure PPartnerExit(Sender: TObject);
    procedure PKontaktNazivExit(Sender: TObject);
    procedure PKontaktIDExit(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aZacuvajStavkiExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPecatiTabelaStavkiExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aNaloziiExecute(Sender: TObject);
    procedure aSpecifikacijaZaPonudaExecute(Sender: TObject);
    procedure aPonudeniArtikliExecute(Sender: TObject);
    procedure aNovaStavkaExecute(Sender: TObject);
    procedure aAzurirajStavkaaExecute(Sender: TObject);
    procedure aBrisiiStavkaExecute(Sender: TObject);
    procedure cxStatusChange(Sender: TObject);
    procedure cxGodinaChange(Sender: TObject);
    procedure ButtonZacuvaj�ClickkExecute(Sender: TObject);
    procedure ButtonOtkaziClickExecute(Sender: TObject);
    procedure ZacuvajClickExecute(Sender: TObject);
    procedure OtkaziClickExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    protected
    prva, posledna :TWinControl;
  end;

var
  frmEvidencijaZaPonudi: TfrmEvidencijaZaPonudi;
  StateActive:TDataSetState;

implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit,
    Partner, TipPartner, Kontakt, Valuta,  Utils, AboutBox, IzborNaStavki,
  PonudeniArtikli, NaloziZaIsporaka, FormConfig, dmResources;

{$R *.dfm}


procedure TfrmEvidencijaZaPonudi.Iznos (Sender: TObject);
var  pom2 :extended;
begin
     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
         end;
end;
procedure TfrmEvidencijaZaPonudi.aAzurirajExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else
       begin
          cxPageControl1.ActivePage:=cxTabSheet2;
          cxTabSheet1.Enabled:=False;
          panel8.Enabled:=True;
          dxribbon1.Enabled:=False;
          cxPageControl2.Enabled:=False;
          PStatus.SetFocus;
          dm.tblPonudi.Edit;
          StateActive:=dsEdit;

          if Tag = 1 then
             begin
                PNabavka.EditValue:=dm.tblNabaviID.Value;
                PNabavka.Enabled:=False;
             end;
          if tag = 0 then
             begin
                dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
                dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
                dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
                dm.tblNabavi.ParamByName('status').Value:='%';
                dm.tblNabavi.FullRefresh;
             end;
       end
end;

procedure TfrmEvidencijaZaPonudi.aAzurirajStavkaaExecute(Sender: TObject);
begin
     if cxGrid3DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else
       begin
          cxPageControl2.ActivePage:=cxTabSheet4;
          cxTabSheet3.Enabled:=False;
          panel9.Enabled:=True;
          dxRibbon1.Enabled:=False;
          cxPageControl1.Enabled:=False;
          SOpis.SetFocus;

          dm.tblPoPonudaStavki.Edit;
          StateActive:=dsEdit;
       end
end;

procedure TfrmEvidencijaZaPonudi.aBrisiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
     else
         begin
            frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
            if frmDaNe.ShowModal =mrYes then
               cxGrid1DBTableView1.DataController.DataSet.Delete();
         end
end;

procedure TfrmEvidencijaZaPonudi.aBrisiiStavkaExecute(Sender: TObject);
begin
     if cxGrid3DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!!')
     else
       begin
        frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
        if frmDaNe.ShowModal =mrYes then
          dm.tblPoPonudaStavki.Delete;
       end
end;

procedure TfrmEvidencijaZaPonudi.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaZaPonudi.Action1Execute(Sender: TObject);
begin
//     if tag = 0 then
//        begin
//           dm.tblPonudi.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
//           dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
//           dm.tblPonudi.ParamByName('status').Value:='%';
//           dm.tblPonudi.FullRefresh;
//           cxLookupComboBox1.Text:='';
//           cxgrid1.SetFocus;
//        end;
//     if tag = 1 then
//         begin
//            dm.tblPonudi.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
//            dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
//            dm.tblPonudi.ParamByName('status').Value:='%';
//            dm.tblPonudi.FullRefresh;
//            cxLookupComboBox1.Text:='';
//            cxgrid1.SetFocus;
//         end;
end;

procedure TfrmEvidencijaZaPonudi.OtkaziClickExecute(Sender: TObject);
begin
     frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           dm.tblPonudi.Cancel;
           StateActive:=dsBrowse;
           dm.RestoreControls(panel8);
           PStatus.Enabled:=true;
           Panel8.Enabled:=False;
           PGodina.Enabled:= false;
           PBroj.Enabled:=false;
           PNabavka.Enabled:=true;
           dxRibbon1.Enabled:=true;
           cxPageControl2.Enabled:=true;
           cxTabSheet1.Enabled:= true;
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid1.SetFocus;
        end;
end;

procedure TfrmEvidencijaZaPonudi.ZacuvajClickExecute(Sender: TObject);
var pom1, pom2,pom3, i, statusNabavka :integer;
begin
     if (dm.Validacija(panel8) = false) then
        begin
           if PKontaktNaziv.Text = '' then
              PKontaktID.Text:='';
           if dm.tblPonudiNABAVKA_ID.AsVariant <> Null then
              begin
                 dm.tblNabavi.Locate('ID', dm.tblPonudiNABAVKA_ID.Value, []);
                 statusNabavka:=dm.tblNabaviSTATUS.Value;
              end;
           if statusNabavka = zatvorena_nab then
              begin
                 ShowMessage('�������� �� ������������ ������ �� ��������� ������� !!!');
                 PNabavka.SetFocus;
              end
           else if statusNabavka = stornirana_nab then
              begin
                 ShowMessage('�������� �� ������������ ������ �� ���������� ������� !!!');
                 PNabavka.SetFocus;
              end
          else if (statusNabavka <> odobrena_nab) and (dm.tblPonudiSTATUS.Value = prifatena_pon) and (dm.tblPonudiNABAVKA_ID.AsVariant <> Null)then
              begin
                 ShowMessage('�������� �� �� ��������� �������� �� ������ �� �� ������ ��������� !!!');
                 PStatus.SetFocus;
              end
          else
              begin
                 dm.tblPonudi.Post;
                 dm.RestoreControls(panel8);
                 if PStatus.EditValue = prifatena_pon then
                    begin
                       dm.promeniStatusStavka(dm.PromenaStatusStavka, 'STATUSSTAVKA', 'STATUS', 'BROJ', 'GODINA', 'PRIFATENA' , prifatena_pon_stavka, prifatena_pon, dm.tblPonudiBROJ.Value, dm.tblPonudiGODINA.Value, prifatena_pon);
                    end;
                 if PStatus.EditValue = odbiena_pon then
                    begin
                       dm.promeniStatusStavka(dm.PromenaStatusStavka, 'STATUSSTAVKA', 'STATUS', 'BROJ', 'GODINA', 'PRIFATENA' , neprifatena_pon_stavka, odbiena_pon, dm.tblPonudiBROJ.Value, dm.tblPonudiGODINA.Value, prifatena_pon);
                    end;
                 dm.tblPoPonudaStavki.Refresh;
                 PBroj.Enabled:=true;
                 pom1:=dm.tblPonudiGODINA.Value;
                 pom2:=dm.tblPonudiBROJ.Value;
                 PBroj.Enabled:=false;
                 if Tag = 1 then
                    begin
                       dm.tblPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
                       dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
                       dm.tblPonudi.FullRefresh;
                    end
                 else
                    begin
                       dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                       dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
                       dm.tblPonudi.ParamByName('status').Value:='%';
                       dm.tblPonudi.FullRefresh;
                    end;
                 dxRibbon1.Enabled:=true;
                 cxGodina.EditValue:=PGodina.EditValue;
                // cxStatus.EditValue:=0;
                 PStatus.Enabled:=true;
                 Panel8.Enabled:=False;
                 PGodina.Enabled:= false;
                 PNabavka.Enabled:=true;
                 cxPageControl2.Enabled:=true;
                 cxTabSheet1.Enabled:= true;
                 StateActive:=dsBrowse;
                 cxPageControl1.ActivePage:=cxTabSheet1;
                 cxGrid1.SetFocus;
                 dm.tblPonudi.Locate('GODINA; BROJ', VarArrayOf([pom1, pom2]) , []);
              end;
        end;
end;

procedure TfrmEvidencijaZaPonudi.ButtonOtkaziClickExecute(Sender: TObject);
begin
 frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           StateActive:=dsBrowse;
           dm.tblPoPonudaStavki.Cancel;
           Panel9.Enabled:=False;
           dxRibbon1.Enabled:=true;
           cxPageControl1.Enabled:=true;
           cxTabSheet3.Enabled:= true;
           cxPageControl2.ActivePage:=cxTabSheet3;
           cxGrid3.SetFocus;
        end;
end;

procedure TfrmEvidencijaZaPonudi.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4005;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranaPonuda.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
     dm.tblStavkiZaOdbranaPonuda.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmEvidencijaZaPonudi.aDodadiExecute(Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else
         begin
            cxPageControl1.ActivePage:=cxTabSheet2;
            cxTabSheet1.Enabled:=False;
            panel8.Enabled:=True;
            PStatus.Enabled:=False;
            if Tag = 0 then
               PGodina.Enabled:= true;
            dxRibbon1.Enabled:=False;
            cxPageControl2.Enabled:=False;
            PredmetPonuda.SetFocus;

            dm.tblPonudi.Insert;
            StateActive:=dsInsert;

            PPartner.Text:='';
            PKontaktNaziv.Text:= '';

            if Tag = 1 then
               begin
                  PNabavka.EditValue:=dm.tblNabaviID.Value;
                  PNabavka.Enabled:=False;
               end;
            if tag = 0 then
               begin
                  dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
                  dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
                  dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
                  dm.tblNabavi.ParamByName('status').Value:='%';
                  dm.tblNabavi.FullRefresh;
               end;
            dm.tblPonudiDATUM.Value:=Date;
            dm.tblPonudire.value:=dmKon.re;
            dm.tblPonudiGODINA.Value:=cxGodina.EditValue;
            dm.tblPonudiBROJ.Value:=dm.zemiBrPonuda(dm.MaxPonudaID, 'GODINA', Null, Null, dm.tblPonudiGODINA.Value, Null, Null, 'MAXBR');
            dm.tblPonudiSTATUS.Value:=otvorena_pon;
            dm.KontaktPartner.Close;
         end;
end;

procedure TfrmEvidencijaZaPonudi.aFormConfigExecute(Sender: TObject);
begin
     frmFormConfig := TfrmFormConfig.Create(Application);
     frmFormConfig.formPtr := Addr(Self);
     frmFormConfig.ShowModal;
     frmFormConfig.Free;
end;

procedure TfrmEvidencijaZaPonudi.aHelpExecute(Sender: TObject);
begin
     Application.HelpContext(107);
end;

procedure TfrmEvidencijaZaPonudi.aIzlezExecute(Sender: TObject);
begin

     if (cxPageControl1.ActivePage = cxTabSheet2) and (StateActive in [dsBrowse]) and (cxPageControl1.Enabled = true) then
         begin
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid1.SetFocus;
         end
     else
     if (cxPageControl1.ActivePage = cxTabSheet2) and (StateActive in [dsEdit,dsInsert]) and (cxPageControl1.Enabled = true)then
         begin
            frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
            if frmDaNe.ShowModal =mrYes then
              begin
                 dm.tblPonudi.Cancel;
                 dm.RestoreControls(panel8);
                 PStatus.Enabled:=true;
                 Panel8.Enabled:=False;
                 PGodina.Enabled:= false;
                 PBroj.Enabled:=false;

                 PNabavka.Enabled:=true;
                 dxRibbon1.Enabled:=true;
                 cxPageControl2.Enabled:=true;
                 cxTabSheet1.Enabled:= true;
                 cxPageControl1.ActivePage:=cxTabSheet1;
                 StateActive:=dsBrowse;
                 cxGrid1.SetFocus;
              end;
         end
     else
     if (cxPageControl2.ActivePage = cxTabSheet4) and (StateActive in [dsBrowse]) then
         begin
            cxPageControl2.ActivePage:=cxTabSheet3;
            cxGrid3.SetFocus;
         end
     else
     if (cxPageControl2.ActivePage = cxTabSheet4) and (StateActive in [dsEdit,dsInsert]) then
         begin
            frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
            if frmDaNe.ShowModal =mrYes then
               begin
                  StateActive:=dsBrowse;
                  dm.tblPoPonudaStavki.Cancel;
                  Panel9.Enabled:=False;
                  dxRibbon1.Enabled:=true;
                  cxPageControl1.Enabled:=true;
                  cxTabSheet3.Enabled:= true;
                  cxPageControl2.ActivePage:=cxTabSheet3;
                  cxGrid3.SetFocus;
              end;
         end
     else Close;
end;

procedure TfrmEvidencijaZaPonudi.aOsveziExecute(Sender: TObject);
begin
     cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmEvidencijaZaPonudi.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmEvidencijaZaPonudi.aPecatiExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmEvidencijaZaPonudi.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaZaPonudi.aPecatiTabelaStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmEvidencijaZaPonudi.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaZaPonudi.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(107);
end;

procedure TfrmEvidencijaZaPonudi.aPonudeniArtikliExecute(Sender: TObject);
begin
     frmPonudeniArtikli:=TfrmPonudeniArtikli.Create(Application);
     frmPonudeniArtikli.ShowModal;
     frmPonudeniArtikli.Free;
end;

procedure TfrmEvidencijaZaPonudi.aNaloziiExecute(Sender: TObject);
begin
      if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
        begin
           frmEvidencijaNaNaloziZaIsporaka:=TfrmEvidencijaNaNaloziZaIsporaka.Create(Application);
           frmEvidencijaNaNaloziZaIsporaka.Tag:=2;
           frmEvidencijaNaNaloziZaIsporaka.ShowModal;
           frmEvidencijaNaNaloziZaIsporaka.Free;
        end
      else
        begin
           ShowMessage('����������� ������ !!!');
        end;
end;

procedure TfrmEvidencijaZaPonudi.aNovaStavkaExecute(Sender: TObject);
begin
      if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
      else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
      else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
      else
         begin
            frmIzborStavki:=TfrmIzborStavki.Create(Application);
            frmIzborStavki.ShowModal;
            frmIzborStavki.Free;
            dm.DetailStavkiNabavka.Close;
            cxPageControl2.ActivePage:=cxTabSheet3;
            cxgrid3.SetFocus;
         end;
end;

procedure TfrmEvidencijaZaPonudi.aNovExecute(Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else
         begin
            cxPageControl1.ActivePage:=cxTabSheet2;
            cxTabSheet1.Enabled:=False;
            panel8.Enabled:=True;
            PStatus.Enabled:=False;
            if Tag = 0 then
               PGodina.Enabled:= true;
            dxRibbon1.Enabled:=False;
            cxPageControl2.Enabled:=False;
            PredmetPonuda.SetFocus;

            dm.tblPonudi.Insert;
            StateActive:=dsInsert;

            PPartner.Text:='';
            PKontaktNaziv.Text:= '';

            if Tag = 1 then
               begin
                  PNabavka.EditValue:=dm.tblNabaviID.Value;
                  PNabavka.Enabled:=False;
               end;
            if tag = 0 then
               begin
                  dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
                  dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
                  dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
                  dm.tblNabavi.ParamByName('status').Value:='%';
                  dm.tblNabavi.FullRefresh;
               end;
            dm.tblPonudiDATUM.Value:=Date;
            dm.tblPonudire.value:=dmKon.re;
            dm.tblPonudiGODINA.Value:=cxGodina.EditValue;
            dm.tblPonudiBROJ.Value:=dm.zemiBrPonuda(dm.MaxPonudaID, 'GODINA', Null, Null, dm.tblPonudiGODINA.Value, Null, Null, 'MAXBR');
            dm.tblPonudiSTATUS.Value:=otvorena_pon;
            dm.KontaktPartner.Close;
         end;
end;

procedure TfrmEvidencijaZaPonudi.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+cxPageControl1.ActivePage.Name,true);
     zacuvajGridVoIni(cxGrid3DBTableView1,dmKon.aplikacija+Name+cxPageControl2.ActivePage.Name,true);
end;

procedure TfrmEvidencijaZaPonudi.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaZaPonudi.aSpecifikacijaZaPonudaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4005;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranaPonuda.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
     dm.tblStavkiZaOdbranaPonuda.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
     dm.tblStavkiZaOdbranaPonuda.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//       dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmEvidencijaZaPonudi.aZacuvajEcelPlanExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := '������' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, true);
end;

procedure TfrmEvidencijaZaPonudi.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, '������' + '-' + dmKon.user);
end;

procedure TfrmEvidencijaZaPonudi.aZacuvajExelStavkaExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := '������ �� ������' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid3,true, true, true);
end;

procedure TfrmEvidencijaZaPonudi.aZacuvajStavkiExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid3, '������ �� ������' + '-' + dmKon.user);
end;



procedure TfrmEvidencijaZaPonudi.ButtonZacuvaj�ClickkExecute(Sender: TObject);
var pom1, pom2, pom3, pom4, statusNabavka :integer;
begin
     if dm.Validacija(Panel9) = false then
        begin
           if dm.tblPonudiNABAVKA_ID.AsVariant <> Null then
             begin
                dm.tblNabavi.Locate('ID', dm.tblPonudiNABAVKA_ID.Value, []);
                statusNabavka:=dm.tblNabaviSTATUS.Value;
             end;
           if (dm.tblPonudiNABAVKA_ID.AsVariant <> Null)and (statusNabavka <> odobrena_nab) and (dm.tblPoPonudaStavkiSTATUS.Value = prifatena_pon_stavka) then
             begin
                ShowMessage('�������� �� �� ��������� �������� �� ������ �� �� ������ ��������� !!!');
                DStatus.SetFocus;
             end
           else
             begin
                dm.tblPoPonudaStavki.Post;
                StateActive:=dsBrowse;

                dm.tblPoPonudaStavki.FullRefresh;
                pom1:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaStatus,'GODINA', 'BROJ', Null, Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, Null, Null, 'BR');
                pom2:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaPrifatena,'GODINA', 'BROJ', 'PRIFATENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, prifatena_pon_stavka, Null, 'PRIFATENA');
                pom3:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtfrlena,'GODINA', 'BROJ','OTFRLENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, neprifatena_pon_stavka, Null, 'OTFRLENA');
                pom4:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtvorena,'GODINA', 'BROJ','OTVORENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, otvorena_pon_stavka, Null, 'OTVORENA');

                if pom3 = pom1 then
                begin
                  dm.tblPonudi.Edit;
                  dm.tblPonudiSTATUS.Value:=odbiena_pon;
                  dm.tblPonudi.Post;
                  dm.tblPonudi.Refresh;
                end;
               if pom2 = pom1 then
                begin
                  dm.tblPonudi.Edit;
                  dm.tblPonudiSTATUS.Value:=prifatena_pon;
                  dm.tblPonudi.Post;
                  dm.tblPonudi.Refresh;
                end;
                if pom4 = pom1 then
                begin
                  dm.tblPonudi.Edit;
                  dm.tblPonudiSTATUS.Value:=otvorena_pon;
                  dm.tblPonudi.Post;
                  dm.tblPonudi.Refresh;
                end;
                if (pom4 <> 0) and (pom3 <> 0) and (pom2 = 0)then
                begin
                  dm.tblPonudi.Edit;
                  dm.tblPonudiSTATUS.Value:=otvorena_pon;
                  dm.tblPonudi.Post;
                  dm.tblPonudi.Refresh;
                end
                else if (pom2 <> pom1) and (pom1 <> pom3)and (pom1 <> pom4)then
                begin
                  dm.tblPonudi.Edit;
                  dm.tblPonudiSTATUS.Value:=delumnoprifatena_pon;
                  dm.tblPonudi.Post;
                  dm.tblPonudi.Refresh;
                end;
                Panel9.Enabled:=False;
                dxRibbon1.Enabled:=true;
                cxPageControl1.Enabled:=true;
                cxTabSheet3.Enabled:= true;
                cxPageControl2.ActivePage:=cxTabSheet3;

                cxGrid3.SetFocus;
             end;
          end;
end;

procedure TfrmEvidencijaZaPonudi.cxStatusChange(Sender: TObject);
begin
     if (cxStatus.CurEditValue <> null) and (Tag = 0)then
         begin
            dm.tblPonudi.ParamByName('godina').Value:=cxGodina.EditValue;
            dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
            dm.tblPonudi.ParamByName('status').Value:=cxStatus.EditValue;
            dm.tblPonudi.FullRefresh;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
            dm.tblPoPonudaStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
         end;
     if (cxStatus.CurEditValue <> null) and (Tag = 1)then
         begin
            dm.tblPonudi.ParamByName('godina').Value:=cxGodina.editValue;
            dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
            dm.tblPonudi.ParamByName('status').Value:=cxStatus.EditValue;
            dm.tblPonudi.FullRefresh;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
            dm.tblPoPonudaStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
         end;
     if (cxStatus.CurEditValue = null) and (Tag = 0)then
         begin
            dm.tblPonudi.ParamByName('godina').Value:=cxGodina.EditValue;
            dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
            dm.tblPonudi.ParamByName('status').Value:='%';
            dm.tblPonudi.FullRefresh;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
            dm.tblPoPonudaStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
         end;
     if (cxStatus.CurEditValue = null) and (Tag = 1)then
         begin
            dm.tblPonudi.ParamByName('godina').Value:=cxGodina.editValue;
            dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
            dm.tblPonudi.ParamByName('status').Value:='%';
            dm.tblPonudi.FullRefresh;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
            dm.tblPoPonudaStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
         end;
end;

procedure TfrmEvidencijaZaPonudi.cxGodinaChange(Sender: TObject);
begin
     if  (Tag = 0)then
         begin
            dm.tblPonudi.ParamByName('godina').Value:=cxGodina.EditValue;
            dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
            dm.tblPonudi.ParamByName('status').Value:='%';
            dm.tblPonudi.FullRefresh;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
            dm.tblPoPonudaStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
         end;
end;

procedure TfrmEvidencijaZaPonudi.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
           dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
           dm.tblPoPonudaStavki.FullRefresh;
        end;
end;

procedure TfrmEvidencijaZaPonudi.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      case Key of
         VK_F5:begin
            if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
               else
                 begin
                    cxPageControl1.ActivePage:=cxTabSheet2;
                    cxTabSheet1.Enabled:=False;
                    panel8.Enabled:=True;
                    PStatus.Enabled:=False;
                    if Tag = 0 then
                      PGodina.Enabled:= true;
                    dxRibbon1.Enabled:=False;
                    cxPageControl2.Enabled:=False;
                    PredmetPonuda.SetFocus;

                    dm.tblPonudi.Insert;
                    StateActive:=dsInsert;

                    PPartner.Text:='';
                    PKontaktNaziv.Text:= '';

                    if Tag = 1 then
                     begin
                        PNabavka.EditValue:=dm.tblNabaviID.Value;
                        PNabavka.Enabled:=False;
                     end;
                    if tag = 0 then
                     begin
                        dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
                        dm.tblNabavi.ParamByName('status').Value:='%';
                        dm.tblNabavi.FullRefresh;
                     end;
                   dm.tblPonudiDATUM.Value:=Date;
                   dm.tblPonudire.value:=dmKon.re;
                   dm.tblPonudiGODINA.Value:=cxGodina.EditValue;
                   dm.tblPonudiBROJ.Value:=dm.zemiBrPonuda(dm.MaxPonudaID, 'GODINA', Null, Null, dm.tblPonudiGODINA.Value, Null, Null, 'MAXBR');
                   dm.tblPonudiSTATUS.Value:=otvorena_pon;
                   dm.KontaktPartner.Close;
                  end;
          end;
          VK_F6:begin
                   if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
                   else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
                   else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
                   else
                          begin
                             cxPageControl1.ActivePage:=cxTabSheet2;
                             cxTabSheet1.Enabled:=False;
                             panel8.Enabled:=True;
                             
                             dxRibbon1.Enabled:=False;
                             cxPageControl2.Enabled:=False;

                             PStatus.SetFocus;

                             dm.tblPonudi.Edit;
                             StateActive:=dsEdit;

                             if Tag = 1 then
                                begin
                                   PNabavka.EditValue:=dm.tblNabaviID.Value;
                                   PNabavka.Enabled:=False;
                                end;
                             if tag = 0 then
                                begin
                                   dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
                                   dm.tblNabavi.ParamByName('status').Value:='%';
                                   dm.tblNabavi.FullRefresh;
                                end;
                           end
          end;
          VK_F8:begin
                   if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
                   else
                      begin
                         frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                         if frmDaNe.ShowModal =mrYes then
                            cxGrid1DBTableView1.DataController.DataSet.Delete();
                      end;
          end;
          VK_F7: cxGrid1DBTableView1.DataController.DataSet.Refresh;
          VK_INSERT:begin
                   if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then ShowMessage('����������� ������ !!!')
                   else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
                   else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
                   else if (tag = 0) and (dm.tblPonudiSTATUS.Value = odbiena_pon)then ShowMessage('�������� � ������� !!!')
                   else
                      begin
                         frmIzborStavki:=TfrmIzborStavki.Create(Application);
                         frmIzborStavki.ShowModal;
                         frmIzborStavki.Free;
                         cxPageControl2.ActivePage:=cxTabSheet3;
                         cxgrid3.SetFocus;
                      end
          end;
      end;
end;

procedure TfrmEvidencijaZaPonudi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaZaPonudi.cxGrid3DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      case key of
         VK_INSERT:begin
           if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then  ShowMessage('����������� ������ !!!')
           else if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
           else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
           else if (tag = 0) and (dm.tblPonudiSTATUS.Value = odbiena_pon)then ShowMessage('�������� � ������� !!!')
           else
              begin
                 frmIzborStavki:=TfrmIzborStavki.Create(Application);
                 frmIzborStavki.ShowModal;
                 frmIzborStavki.Free;
                 cxPageControl2.ActivePage:=cxTabSheet3;
                 cxgrid3.SetFocus;
              end
         end;
      end;
end;

procedure TfrmEvidencijaZaPonudi.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmEvidencijaZaPonudi.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if ( StateActive in [dsBrowse] ) and (cxPageControl1.ActivePage = cxTabSheet1)then
         begin
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
               begin
                  dm.KontaktPartner.ParamByName('tippartner').Value:=StrToInt(ptipPartner.text);
                  dm.KontaktPartner.ParamByName('partner').Value:=StrToInt(PPartnerID.Text);
                  dm.KontaktPartner.FullRefresh;
                  PPartner.Text:=dm.tblPonudiPNAZIV.Value;
                  PKontaktNaziv.Text:=dm.tblPonudiKNAZIV.Value;
               end
            else
               begin
                  PPartner.Text:='';
                  PKontaktNaziv.Text:='';
               end;
            cxGodina.Enabled:=false;
            cxStatus.Enabled:=False;
            Action1.Enabled:=False;
        end;
     if cxPageControl1.ActivePage = cxTabSheet2 then
       begin
          if Tag = 0 then
           begin
              cxGodina.Enabled:= true;
           end;
          cxStatus.Enabled:=True;
          Action1.Enabled:=True;
       end;

end;

procedure TfrmEvidencijaZaPonudi.DStatusPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsEdit]) and (Panel9.Enabled = true)and (SKolicina.Text <> '')then
        begin
           if DStatus.EditValue = prifatena_pon_stavka then
              dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:=dm.tblPoPonudaStavkiKOLICINA.Value;
           if DStatus.EditValue = otvorena_pon_stavka then
              dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:=0;
        end;
end;

procedure TfrmEvidencijaZaPonudi.FormCreate(Sender: TObject);
begin
     ProcitajFormaIzgled(self);
     if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

     dm.Plakanje.Open;

     dm.StatusPonuda.Open;
     dmMat.tblValuta.Open;
     dm.KontaktPartner.Open;
     dm.PoPonudaStavkIStatus.Open;
     dm.DDV.Open;

     dm.Firmi.close;
     dm.Firmi.ParamByName('re').Value:=dmKon.re;
     dm.Firmi.Open;
     dm.tblReportDizajn.Open;
     dm.tblStavkiZaOdbranaPonuda.Open;
     dm.tblPartner.Close;
     dm.tblPartner.ParamByName('tipPartner').Value:='%';
     dm.tblPartner.Open;
end;

procedure TfrmEvidencijaZaPonudi.FormShow(Sender: TObject);
begin
     dm.frxReport.Script.Clear;
     dm.frxReport.Clear;
     if Tag = 1 then
       begin
          dm.tblPonudi.Close;
          dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
          dm.tblPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
          dm.tblPonudi.ParamByName('status').Value:='%';
          dm.tblPonudi.Open;
          cxGodina.EditValue:=dm.tblNabaviGODINA.Value;
          Caption:='��������� �� ������ �� �������' + ' '+dm.tblNabaviBROJNABAVKA.Value;
          cxGodina.Enabled:=false;
          cxLabel1.Caption:='������ �� ������� ���';
          DBText1.Visible:=true;
          DBText2.Visible :=false;
       end
     else
       begin
          dm.tblPonudi.Close;
          dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
          dm.tblPonudi.ParamByName('godina').Value:=dmKon.godina;
          dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
          dm.tblPonudi.ParamByName('status').Value:='%';
          dm.tblPonudi.Open;
          cxGodina.EditValue:=dmKon.godina;
          Caption:='��������� �� ������';
          cxGodina.Enabled:=true;
          cxLabel1.Caption:='������ ��';
          DBText1.Visible:=false;
          DBText2.Visible:=true;
       end;
     dm.tblNabavi.ParamByName('godina').Value:=cxGodina.EditValue;
     dm.tblNabavi.ParamByName('status').Value:='%';
     dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ '%';
     dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
     dm.tblNabavi.Open;
     cxPageControl1.ActivePage:=cxTabSheet1;
     cxPageControl2.ActivePage:=cxTabSheet3;

     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+cxPageControl1.ActivePage.Name,true,true);
     procitajGridOdIni(cxGrid3DBTableView1,dmKon.aplikacija+Name+cxPageControl2.ActivePage.Name,true,true);

     cxGrid1.SetFocus;
     StateActive:=dsBrowse;

     dm.tblPoPonudaStavki.Close;
     dm.tblPoPonudaStavki.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
     dm.tblPoPonudaStavki.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
     dm.tblPoPonudaStavki.Open;

end;

procedure TfrmEvidencijaZaPonudi.PGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsEdit]) and (PGodina.Text <> '') then
      begin
         if PGodina.Text <> IntToStr(dm.tblPonudiGODINA.Value) then
           begin
              pbroj.Enabled:=true;
              PBroj.Text:=IntToStr(dm.zemiBrPonuda(dm.MaxPonudaID, 'GODINA', Null, Null, StrToInt(PGodina.Text) , Null, Null, 'MAXBR'));
              PBroj.Enabled:=false;
           end;
         dm.tblNabavi.ParamByName('godina').Value:=strToInt(PGodina.Text);
         dm.tblNabavi.FullRefresh;
     end;

     if StateActive in [dsInsert] then
        if PGodina.Text <> '' then
           begin
              pbroj.Enabled:=true;
              PBroj.Text:=IntToStr(dm.zemiBrPonuda(dm.MaxPonudaID, 'GODINA', Null, Null, StrToInt(PGodina.Text) , Null, Null, 'MAXBR'));
              PBroj.Enabled:=false;
              dm.tblNabavi.ParamByName('godina').Value:=strToInt(PGodina.Text);
              dm.tblNabavi.FullRefresh;
           end;
end;

procedure TfrmEvidencijaZaPonudi.PKontaktIDExit(Sender: TObject);
begin
     if StateActive in [dsInsert,dsEdit] then
        if PKontaktID.Text <> '' then
           PKontaktNaziv.EditValue:=dm.tblPonudiKONTAKT.Value;
end;

procedure TfrmEvidencijaZaPonudi.PKontaktNazivExit(Sender: TObject);
begin
     if StateActive in [dsInsert,dsEdit] then
       if PKontaktNaziv.Text <> '' then
          dm.tblPonudiKONTAKT.Value:=dm.KontaktPartnerID.Value;
end;

procedure TfrmEvidencijaZaPonudi.PNabavkaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if StateActive in [dsInsert,dsEdit] then
        if PNabavka.Text <> '' then
          begin
             dm.tblNabavi.Locate('ID', VarArrayOf([PNabavka.EditValue]) , []);
             PredmetNabavka.Text:=dm.tblNabaviPREDMET.Value;
          end;
end;

procedure TfrmEvidencijaZaPonudi.PPartnerExit(Sender: TObject);
begin
     if (StateActive in [dsEdit,dsInsert]) and (PPartner.text <> '') and (cxPageControl1.ActivePage = cxTabSheet2)  then
         begin
            dm.tblPonudiTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
            dm.tblPonudiPARTNER.Value:=dm.tblPartnerID.Value;

            PKontaktNaziv.Text:='';
            PKontaktID.Text:='';
            dm.KontaktPartner.ParamByName('tippartner').Value:=dm.tblPartnerTIP_PARTNER.Value;
            dm.KontaktPartner.ParamByName('partner').Value:=dm.tblPartnerID.Value;
            dm.KontaktPartner.FullRefresh;
         end;
end;

procedure TfrmEvidencijaZaPonudi.PPartnerIDPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
     if (StateActive in [dsInsert,dsEdit]) and (cxPageControl1.ActivePage = cxTabSheet2)then
         begin
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
                begin
                   pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
                   if pom = 1  then
                      begin
                         dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                         PPartner.Text:= dm.tblPartnerNAZIV.Value;
                      end
                   else PPartner.Text:= '';
                end;
         end;
end;

procedure TfrmEvidencijaZaPonudi.PTipPartnerPropertiesEditValueChanged(
  Sender: TObject);
  var pom :integer;
begin
     if (StateActive in [dsInsert,dsEdit]) and (cxPageControl1.ActivePage = cxTabSheet2)then
         begin
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
                begin
                   pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
                   if pom = 1  then
                      begin
                         dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                         PPartner.Text:= dm.tblPartnerNAZIV.Value;
                      end
                   else PPartner.Text:= '';
                end;
         end;
end;

procedure TfrmEvidencijaZaPonudi.SCenaExit(Sender: TObject);
var pom2 :extended;
begin
     if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '') then
         begin
            dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (SDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
            dm.tblPoPonudaStavkiKRAJNA_CENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value-(dm.tblPoPonudaStavkiCENADDV.Value * dm.tblPoPonudaStavkiRABAT.Value / 100);
         end;

     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
         end;
end;

procedure TfrmEvidencijaZaPonudi.SCenaSoDDVExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if (StateActive in [dsEdit]) and (SCenaSoDDV.Text <> '') then
       begin
          dm.tblPoPonudaStavkiCENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value*100/(100 + SDDV.EditValue);
          dm.tblPoPonudaStavkiKRAJNA_CENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value-(dm.tblPoPonudaStavkiCENADDV.Value * dm.tblPoPonudaStavkiRABAT.Value / 100);
       end;

     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
       begin
          dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
       end;
end;

procedure TfrmEvidencijaZaPonudi.SDDVPropertiesEditValueChanged(
  Sender: TObject);
var pom1, pom2 :extended;
begin
      if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '') then
        begin
           dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (SDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
           dm.tblPoPonudaStavkiKRAJNA_CENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value-(dm.tblPoPonudaStavkiCENADDV.Value * dm.tblPoPonudaStavkiRABAT.Value / 100);
        end;

      if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
         end;
end;

procedure TfrmEvidencijaZaPonudi.SKolicinaExit(Sender: TObject);
var  pom2 :extended;
begin
     if (StateActive in [dsEdit])and (SKolicina.Text <> '')then
        begin
           if (DStatus.EditValue = prifatena_pon_stavka) then
              dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:= dm.tblPoPonudaStavkiKOLICINA.Value;
           if (DStatus.EditValue = otvorena_pon_stavka) then
              dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:=0;
        end;

     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
        begin
           dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
        end;
end;

procedure TfrmEvidencijaZaPonudi.SPrifatenaKolicinaExit(Sender: TObject);
var  pom2 :extended;
begin
     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
         end;
end;

procedure TfrmEvidencijaZaPonudi.SRabatExit(Sender: TObject);
var  pom2 :extended;
begin
     if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '') then
        dm.tblPoPonudaStavkiKRAJNA_CENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value-(dm.tblPoPonudaStavkiCENADDV.Value * dm.tblPoPonudaStavkiRABAT.Value / 100);
     if (SKrajnaCena.Text <> '') and (SPrifatenaKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoPonudaStavkiIIZNOS.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value * dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value;
         end;

end;

procedure TfrmEvidencijaZaPonudi.SRabatPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (SDDV.Text <> '') and (SCenaSoDDV.Text <> '')and (StateActive in [dsEdit])  then
        begin
           dm.tblPoPonudaStavkiKRAJNA_CENA.Value:=strtofloat(SCenaSoDDV.Text)-(StrToFloat(SCenaSoDDV.Text) * strtofloat(SRabat.Text) / 100);
        end;
end;

procedure TfrmEvidencijaZaPonudi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
          begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
          end;
        VK_UP:
          begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
          end;
        VK_RETURN:
          begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
          end;
      VK_INSERT:
          begin
             if (sender = PPartner) or (Sender = PPartnerID )then
               begin
                  frmPartner:=TfrmPartner.Create(Application);
                  frmPartner.Tag:=2;
                  frmPartner.ShowModal;
                  frmPartner.Free;
                  PPartner.Text:=dm.tblPartnerNAZIV.Value;
                  dm.tblPonudiPARTNER.Value:=dm.tblPartnerID.Value;
                  dm.tblPonudiTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
                  PKontaktID.SetFocus;
                  dm.tblPartner.ParamByName('tipPartner').Value:='%';
                  dm.tblPartner.FullRefresh;
               end;
            if ( sender = PTipPartner) then
               begin
                  frmTipPartner:=TfrmTipPartner.Create(Application);
                  frmTipPartner.Tag:=1;
                  frmTipPartner.ShowModal;
                  frmTipPartner.Free;
                  dm.tblPonudiTIP_PARTNER.Value:=dmMat.tblTipPartnerID.Value;
                  PPartnerID.SetFocus;
               end;
            if (Sender = PKontaktID) or (Sender =PKontaktNaziv) then
               begin
                  if (PPartner.Text <> '') and (PPartnerID.Text <> '') and (PTipPartner.Text <> '') then
                     begin
                        dm.tblKontakt.close;
                        dm.tblKontakt.ParamByName('tipPartner').Value:=StrToInt(PTipPartner.Text);
                        dm.tblKontakt.ParamByName('Partner').Value:=StrToInt(PPartnerID.Text);
                        dm.tblKontakt.Open;
                        frmKontakt:=TfrmKontakt.Create(Application);
                        dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                        frmKontakt.Tag:= 1;
                        frmKontakt.ShowModal;
                        frmKontakt.Free;
                        dm.KontaktPartner.ParamByName('tippartner').Value:=StrToInt(PTipPartner.Text);
                        dm.KontaktPartner.ParamByName('partner').Value:=StrToInt(PPartnerID.Text);
                        dm.KontaktPartner.FullRefresh;
                        dm.tblPonudiKONTAKT.Value:=dm.tblKontaktID.Value;
                        PKontaktNaziv.EditValue:=dm.tblKontaktID.Value;
                        PPlakanje.SetFocus;
                      end;
               end;
            if (sender = SValuta) then
               begin
                  frmValuta:=TfrmValuta.Create(Application);
                  frmValuta.Tag:=1;
                  frmValuta.ShowModal;
                  frmValuta.Free;
                  dm.tblPoPonudaStavkiVALUTA.Value:=dmMat.tblValutaID.Value;
                  SKurs.SetFocus;
               end;
       end;
    end;
end;

end.
