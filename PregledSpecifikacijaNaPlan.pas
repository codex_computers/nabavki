unit PregledSpecifikacijaNaPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxStatusBar, dxRibbonStatusBar, cxLookupEdit,cxGridExportLink, cxExport,
  cxDBLookupEdit, cxDBLookupComboBox, cxContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, StdCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, ExtCtrls,
  ActnList, cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions;

type
  TfrmSpecifikacijaZaSiteSektori = class(TForm)
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    PGodina: TcxComboBox;
    RabEdinica: TcxLookupComboBox;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1VREME: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BrojGodina: TcxGridDBColumn;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aZacuvajVoExcel: TAction;
    aPecati: TAction;
    Label4: TLabel;
    aDizajn: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure RabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure PGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSpecifikacijaZaSiteSektori: TfrmSpecifikacijaZaSiteSektori;

implementation

uses dmKonekcija, dmMaticni, dmUnit, PregledPlanZaIzbranSektor,
  PregledPlanZaSiteSektori, Utils;

{$R *.dfm}

procedure TfrmSpecifikacijaZaSiteSektori.aDizajnExecute(Sender: TObject);
var RptStream:TStream;
begin
     dm.frxDesignerPlan.Tag:=3;

     dm.tblReportDizajn.ParamByName('id').Value:=4003;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblStavkiZaOdbranPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblStavkiZaOdbranPlan.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan2.fr3');

     dm.frxReport.DesignReport();
end;

procedure TfrmSpecifikacijaZaSiteSektori.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmSpecifikacijaZaSiteSektori.aPecatiExecute(Sender: TObject);
var RptStream:TStream;
begin
     dm.frxDesignerPlan.Tag:=3;

     dm.tblReportDizajn.ParamByName('id').Value:=4003;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranPlan.ParamByName('godina').Value:=dm.tblPlanSektorGODINA.Value;
     dm.tblStavkiZaOdbranPlan.ParamByName('broj').Value:=dm.tblPlanSektorBROJ.Value;
     dm.tblStavkiZaOdbranPlan.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan2.fr3');

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmSpecifikacijaZaSiteSektori.aZacuvajVoExcelExecute(
  Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
//     dmMat.SaveToExcelDialog.FileName := '������������ �� ������ ����' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmSpecifikacijaZaSiteSektori.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dm.tblPlanSektor.Close;
    // dm.IzbraniSektoriZaPlan.Close;
end;

procedure TfrmSpecifikacijaZaSiteSektori.FormCreate(Sender: TObject);
begin
     dm.tblPlanSektor.ParamByName('sektor').Value:=dmKon.re;
     dm.tblPlanSektor.ParamByName('godina').Value:=dmKon.godina ;
     dm.tblPlanSektor.Open;
     dm.IzbraniSektoriZaPlan.close;
     dm.IzbraniSektoriZaPlan.ParamByName('poteklo').Value:=intToStr(dmKon.re) + ',';
     dm.IzbraniSektoriZaPlan.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
     //dm.IzbraniSektoriZaPlan.ParamByName('user').Value:=dmKon.user;
     dm.IzbraniSektoriZaPlan.Open;
end;

procedure TfrmSpecifikacijaZaSiteSektori.FormShow(Sender: TObject);
begin
     RabEdinica.EditValue:=dmKon.re;
     PGodina.EditValue:=dmKon.godina;
     cxGrid1.SetFocus;
end;

procedure TfrmSpecifikacijaZaSiteSektori.PGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPlanSektor.ParamByName('sektor').Value:=RabEdinica.EditValue;
     dm.tblPlanSektor.ParamByName('godina').Value:=PGodina.EditValue ;
     dm.tblPlanSektor.FullRefresh;
end;

procedure TfrmSpecifikacijaZaSiteSektori.RabEdinicaPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPlanSektor.ParamByName('sektor').Value:=RabEdinica.EditValue;
     dm.tblPlanSektor.ParamByName('godina').Value:=PGodina.EditValue ;
     dm.tblPlanSektor.FullRefresh;
end;

end.
