unit Proba;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, StdCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar,
  dxRibbonStatusBar, DBCtrls, ActnList, ActnMan, ToolWin, ActnCtrls, ComCtrls,
  cxPC, RibbonActnCtrls, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, cxDBEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxImageComboBox, cxLabel,SqlExpr, DBClient,cxDBLabel,FMTBcd,
  cxDBExtLookupComboBox;

type
  TFormProba = class(TForm)
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView1ARTNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA_PRIFATENA: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1DDV: TcxGridDBColumn;
    cxGrid2DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid2DBTableView1RABAT: TcxGridDBColumn;
    cxGrid2DBTableView1KrajnaCena: TcxGridDBColumn;
    cxGrid2DBTableView1IIZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1STATUS: TcxGridDBColumn;
    cxGrid2DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid2DBTableView1VALUTANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KURS: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTGRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1ARTGRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid2DBTableView1VREME: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;

    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  FormProba: TFormProba;

implementation

uses dmKonekcija, dmLook, dmMaticni, dmUnit;

{$R *.dfm}



procedure TFormProba.FormCreate(Sender: TObject);
begin
       dm.tblPoPonudaStavki.Close;
       dm.tblPoPonudaStavki.ParamByName('godina').Value:=2009;
       dm.tblPoPonudaStavki.ParamByName('broj').Value:=3;
       dm.tblPoPonudaStavki.Open;
end;


end.
