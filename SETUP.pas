unit SETUP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar,
  dxRibbonStatusBar, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls,
  ToolWin, ActnMan, ActnCtrls, ActnMenus, RibbonActnMenus, ActnList,
  cxContainer, cxLabel, StdCtrls, cxTextEdit, cxDBEdit, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmSetup = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Ribbon1: TRibbon;
    RibbonPage1: TRibbonPage;
    ActionManager1: TActionManager;
    RibbonApplicationMenuBar1: TRibbonApplicationMenuBar;
    cxGrid1DBTableView1PARAMETAR: TcxGridDBColumn;
    cxGrid1DBTableView1PARAM_VREDNOST: TcxGridDBColumn;
    cxGrid1DBTableView1VREDNOST: TcxGridDBColumn;
    cxGrid1DBTableView1P1: TcxGridDBColumn;
    cxGrid1DBTableView1P2: TcxGridDBColumn;
    cxGrid1DBTableView1V1: TcxGridDBColumn;
    cxGrid1DBTableView1V2: TcxGridDBColumn;
    P1: TcxDBTextEdit;
    P2: TcxDBTextEdit;
    V1: TcxDBTextEdit;
    V2: TcxDBTextEdit;
    Parametar: TcxDBTextEdit;
    VrednostParametar: TcxDBTextEdit;
    Vrednost: TcxDBTextEdit;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Zacuvaj: TButton;
    Otkazi: TButton;
    aDodadi: TAction;
    RibbonGroup1: TRibbonGroup;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    RibbonGroup2: TRibbonGroup;
    aIzlez: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aDodadiExecute(Sender: TObject);
    procedure ZacuvajClick(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure OtkaziClick(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSetup: TfrmSetup;

implementation

uses dmKonekcija, dmMaticni, dmUnit, DaNe;

{$R *.dfm}

procedure TfrmSetup.aAzurirajExecute(Sender: TObject);
begin
     Panel1.Enabled:=True;
     Ribbon1.Enabled:=False;
     Panel2.Enabled:=False;
     dm.tblSETUP.Edit;
     p2.SetFocus;
end;

procedure TfrmSetup.aBrisiExecute(Sender: TObject);
begin
      if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
              begin
                 frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                 if frmDaNe.ShowModal =mrYes then
                   cxGrid1DBTableView1.DataController.DataSet.Delete();
              end
           else ShowMessage('����������� ������� !!!');
end;

procedure TfrmSetup.aDodadiExecute(Sender: TObject);
begin
      Panel1.Enabled:=True;
      Ribbon1.Enabled:=False;
      Panel2.Enabled:=False;
      dm.tblSETUP.Insert;
      dm.tblSETUPP1.Value:='PO';
      p2.SetFocus;
end;

procedure TfrmSetup.aIzlezExecute(Sender: TObject);
begin
     if Panel1.Enabled = true then
       begin
          dm.tblSETUP.Cancel;
          Panel1.Enabled:=False;
          Panel2.Enabled:=True;
          Ribbon1.Enabled:=true;
          cxGrid1.SetFocus;
       end
     else
       begin
          Close;
       end;
end;

procedure TfrmSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dm.tblSETUP.Close;
end;

procedure TfrmSetup.FormCreate(Sender: TObject);
begin
     dm.tblSETUP.Open;
end;

procedure TfrmSetup.OtkaziClick(Sender: TObject);
begin
      dm.tblSETUP.Cancel;
      Panel1.Enabled:=False;
      Panel2.Enabled:=True;
      Ribbon1.Enabled:=true;
      cxGrid1.SetFocus;
end;

procedure TfrmSetup.ZacuvajClick(Sender: TObject);
begin
      if p2.Text = '' then
         P2.SetFocus
      else
         begin
            dm.tblSETUP.Post;
            Panel1.Enabled:=False;
            Panel2.Enabled:=True;
            Ribbon1.Enabled:=true;
            dm.tblSETUP.Refresh;
            cxGrid1.SetFocus;
         end;
end;

procedure TfrmSetup.aOsveziExecute(Sender: TObject);
begin
     dm.tblSETUP.Refresh;
end;

procedure TfrmSetup.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;

begin
      case Key of
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:begin

        end;

  end;
end;

end.
