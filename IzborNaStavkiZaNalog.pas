unit IzborNaStavkiZaNalog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, StdCtrls,
  ExtCtrls, dxStatusBar, dxRibbonStatusBar, cxMaskEdit, cxMemo, cxDBEdit,
  cxContainer, cxTextEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, ActnList, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions;

type
  TfrmIzborNaStavkiNalog = class(TForm)
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    RadioButton1: TRadioButton;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView3ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView3KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView3KOLICINA_PRIFATENA: TcxGridDBColumn;
    cxGrid1DBTableView3OPIS: TcxGridDBColumn;
    cxGrid1DBTableView3CENA: TcxGridDBColumn;
    cxGrid1DBTableView3DDV: TcxGridDBColumn;
    cxGrid1DBTableView3CENADDV: TcxGridDBColumn;
    cxGrid1DBTableView3RABAT: TcxGridDBColumn;
    cxGrid1DBTableView3KURS: TcxGridDBColumn;
    cxGrid1DBTableView3ARTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3MERKA: TcxGridDBColumn;
    cxGrid1DBTableView3ARTGRUPA: TcxGridDBColumn;
    cxGrid1DBTableView3ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3ARTGRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3VALUTANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3IIZNOS: TcxGridDBColumn;
    cxGrid1DBTableView3STATUSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView3KRAJNA_CENA: TcxGridDBColumn;
    Panel6: TPanel;
    Bevel2: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    sLabel: TLabel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    aZacuvajGoIzgledot: TAction;
    Panel1: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel3: TPanel;
    Bevel3: TBevel;
    Label6: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    cxGrid1DBTableView2ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView2ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView2SUMAKOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView2SUMACENA: TcxGridDBColumn;
    cxGrid1DBTableView2DDV: TcxGridDBColumn;
    cxGrid1DBTableView2SUMACENADDV: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2GRUPAIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2Nabavka_Id: TcxGridDBColumn;
    cxGrid1DBTableView2MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2MERKA: TcxGridDBColumn;
    Label15: TLabel;
    PrifatenaKolicina: TcxDBTextEdit;
    Label16: TLabel;
    SumaKolicina: TcxMaskEdit;
    SMerkaId: TcxDBTextEdit;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1Level3: TcxGridLevel;
    cxGrid1Level4: TcxGridLevel;
    cxGrid1DBTableView4: TcxGridDBTableView;
    cxGrid1DBTableView4VIDARTIKAL_GEN: TcxGridDBColumn;
    cxGrid1DBTableView4SIFRAARTIKAL_GEN: TcxGridDBColumn;
    cxGrid1DBTableView4OPSTNAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView4VIDARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView4SIFRAARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView4NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1BRPAK: TcxGridDBColumn;
    cxGrid1DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    aPomos: TAction;
    ACena: TcxDBTextEdit;
    ADDV: TcxDBLookupComboBox;
    ACenaSoDDV: TcxDBTextEdit;
    AKolicina: TcxDBTextEdit;
    cxDBTextEdit5: TcxDBTextEdit;
    SOpis: TcxDBMemo;
    SCena: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCenaSoDDV: TcxDBTextEdit;
    SKolicina: TcxDBTextEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    AOpis: TcxDBMemo;
    Opis: TcxDBMemo;
    Kolicina: TcxDBTextEdit;
    CenaSoDDV: TcxDBTextEdit;
    DDV: TcxDBLookupComboBox;
    Cena: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel4: TPanel;
    aInsertPrifateniStavki: TAction;
    Label17: TLabel;
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterZaPanel6(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
    procedure ACenaExit(Sender: TObject);
    procedure ACenaSoDDVExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aZacuvajGoIzgledotExecute(Sender: TObject);
    procedure EnterZaPanel1(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure SCenaExit(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure cxGrid1DBTableView2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterZaPanel3(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxGrid1DBTableView3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CenaExit(Sender: TObject);
    procedure CenaSoDDVExit(Sender: TObject);
    procedure cxGrid1DBTableView4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KolicinaExit(Sender: TObject);
    procedure AKolicinaExit(Sender: TObject);
    procedure SKolicinaExit(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView4KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
    procedure ADDVExit(Sender: TObject);
    procedure DDVExit(Sender: TObject);
    procedure SDDVExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Action1Execute(Sender: TObject);
    procedure aInsertPrifateniStavkiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    prva, posledna, prva3, posledna3 :TWinControl;
  end;

var
  frmIzborNaStavkiNalog: TfrmIzborNaStavkiNalog;
   StateActive:TDataSetState;
implementation

uses dmUnit, dmKonekcija, dmMaticni, Artikal, Utils, DaNe;

{$R *.dfm}


procedure TfrmIzborNaStavkiNalog.EnterZaPanel6(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom1, pom2:integer;
begin
     case Key of
        VK_UP:
        begin
            if Sender = ACena then SOpis.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
            if Sender = SOpis then  Panel6.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if sender = SOpis then
             begin
                 if dm.Validacija(Panel6) = false then
                   begin
                       pom1:=dm.tblNalogGODINA.Value;
                       pom2:=dm.tblNalogBROJ.Value;
                       dm.tblPoNalogStavkiID.Value:=dm.zemiNalogStavkaID(dm.nalogStavkiID, 'BROJ', 'GODINA', Null, pom2, pom1, Null, 'MAXBR');
                       dm.tblPoNalogStavkiBROJ.Value:=pom2;
                       dm.tblPoNalogStavkiGODINA.Value:= pom1;
                       dm.tblPoNalogStavkiARTVID.Value:=dm.tblListaArtikliFiltriraniARTVID.Value;
                       dm.tblPoNalogStavkiARTSIF.Value:=dm.tblListaArtikliFiltriraniID.Value;
                       dm.tblPoNalogStavki.Post;
                       StateActive:=dsBrowse;
                       dm.RestoreControls(panel6);
                       dm.tblPoNalogStavki.Refresh;
                       panel6.Visible:= false;
                       RadioButton4.Visible:=True;
                       RadioButton1.Visible:=true;
                       cxGrid1.SetFocus;
                   end;
             end
          else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
  end;
end;

procedure TfrmIzborNaStavkiNalog.EnterZaPanel3(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom1, pom2:integer;
begin
     case Key of
        VK_UP:
        begin
            if Sender = Cena then Opis.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
            if Sender = Opis then  Panel3.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if sender = Opis then
             begin
                 if dm.Validacija(Panel3) = false then
                   begin
                       pom1:=dm.tblNalogGODINA.Value;
                       pom2:=dm.tblNalogBROJ.Value;
                       dm.tblPoNalogStavkiID.Value:=dm.zemiNalogStavkaID(dm.nalogStavkiID, 'BROJ', 'GODINA', Null, pom2, pom1, Null, 'MAXBR');
                       dm.tblPoNalogStavkiBROJ.Value:=pom2;
                       dm.tblPoNalogStavkiGODINA.Value:= pom1;
                       //dm.tblPoNalogStavkiDDV.Value:=dm.tblPoPonudaStavkiDDV.Value;
                       ddv.EditValue:= dm.tblPoPonudaStavkiDDV.Value;
                       dm.tblPoNalogStavkiARTVID.Value:=dm.tblPoPonudaStavkiARTVID.Value;
                       dm.tblPoNalogStavkiARTSIF.Value:=dm.tblPoPonudaStavkiARTSIF.Value;

                       dm.tblPoNalogStavki.Post;
                       StateActive:=dsBrowse;
                       dm.RestoreControls(panel3);
                       dm.tblPoNalogStavki.Refresh;

                       panel3.Visible:= false;
                       RadioButton4.Visible:=True;
                       RadioButton3.Visible:=true;
                       cxGrid1.SetFocus;
                   end;
             end
          else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;


  end;
end;

procedure TfrmIzborNaStavkiNalog.EnterZaPanel1(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom1, pom2:integer;
begin
     case Key of
        VK_UP:
        begin
            if Sender = SCena then AOpis.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
            if Sender = AOpis then Panel1.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if sender = AOpis then
             begin
                 if dm.Validacija(panel1) = false then
                   begin
                       pom1:=dm.tblNalogGODINA.Value;
                       pom2:=dm.tblNalogBROJ.Value;
                       dm.tblPoNalogStavkiID.Value:=dm.zemiNalogStavkaID(dm.nalogStavkiID, 'BROJ', 'GODINA', Null, pom2, pom1, Null, 'MAXBR');
                       dm.tblPoNalogStavkiBROJ.Value:=pom2;
                       dm.tblPoNalogStavkiGODINA.Value:= pom1;
                       if panel1.Tag = 1 then
                          begin
                             dm.tblPoNalogStavkiARTVID.Value:=dm.DetailStavkiNabavkaVIDARTIKAL.Value;
                             dm.tblPoNalogStavkiARTSIF.Value:=dm.DetailStavkiNabavkaSIFRAARTIKAL.Value;
                          end
                       else
                          begin
                             dm.tblPoNalogStavkiARTVID.Value:=dm.IzborStavkiOdNabavkaARTVID.Value;
                             dm.tblPoNalogStavkiARTSIF.Value:=dm.IzborStavkiOdNabavkaARTSIF.Value;
                          end;
                       dm.tblPoNalogStavki.Post;
                       dm.RestoreControls(panel1);
                       StateActive:=dsBrowse;
                       dm.tblPoNalogStavki.FullRefresh;

                       panel1.Visible:= false;
                       RadioButton3.Visible:=True;
                       RadioButton1.Visible:=true;
                       cxGrid1.SetFocus;
                   end;
             end
          else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;


  end;
end;

procedure TfrmIzborNaStavkiNalog.ACenaExit(Sender: TObject);
begin
      if (StateActive in [dsInsert]) and (ACena.Text <> '') and (ADDV.Text <> '') then
         begin
            dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (ADDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
         end;
end;

procedure TfrmIzborNaStavkiNalog.ACenaSoDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (ACenaSoDDV.Text <> '')  then
       begin
          dm.tblPoNalogStavkiCENA.Value:=dm.tblPoNalogStavkiCENADDV.Value*100/(100 + ADDV.EditValue);
       end;
end;

procedure TfrmIzborNaStavkiNalog.Action1Execute(Sender: TObject);
begin
     dm.promeniStatusStavka(dm.InsertStavkiNalog, 'GODINA_IN', 'BROJ_IN', 'BROJ_NALOG', 'GODINA_NALOG', 'KREIRAL_IN', dm.tblPoPonudaStavkiGODINA.Value, dm.tblPoPonudaStavkiBROJ.Value, dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, 'MIRJANA');
     dm.tblPoNalogStavki.FullRefresh;
end;

procedure TfrmIzborNaStavkiNalog.ADDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (ACena.Text <> '') and (ADDV.Text <> '') then
        begin
           dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (ADDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
        end;
end;

procedure TfrmIzborNaStavkiNalog.aInsertPrifateniStavkiExecute(Sender: TObject);
begin
     if (RadioButton1.Checked) and (cxGrid1DBTableView3.Controller.SelectedRecordCount > 0) then
        begin
           dm.InsertPrifateniStavkiNalog(dm.InsertStavkiNalog, 'GODINA_IN', 'BROJ_IN', 'BROJ_NALOG', 'GODINA_NALOG', 'KREIRAL_IN','STATUS_IN', dm.tblPoPonudaStavkiGODINA.Value, dm.tblPoPonudaStavkiBROJ.Value, dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, dmKon.user, prifatena_pon_stavka);
           dm.tblPoNalogStavki.FullRefresh;
        end;
end;

procedure TfrmIzborNaStavkiNalog.aIzlezExecute(Sender: TObject);
begin
     if (Panel6.Visible = true) then
        begin
            panel6.Visible:= false;
            RadioButton4.Visible:=True;
            RadioButton1.Visible:=true;
            dm.RestoreControls(panel6);
            dm.tblPoNalogStavki.Cancel;
            StateActive:=dsBrowse;
            cxGrid1.SetFocus;
        end
     else if Panel1.Visible = true then
        begin
            dm.tblPoNalogStavki.Cancel;
            StateActive:=dsBrowse;
            panel1.Visible:= false;
            RadioButton3.Visible:=True;
            RadioButton1.Visible:=true;
            dm.RestoreControls(panel1);
            cxGrid1.SetFocus;
        end
     else if Panel3.Visible = true then
        begin
            dm.tblPoNalogStavki.Cancel;
            StateActive:=dsBrowse;
            panel3.Visible:= false;
            RadioButton3.Visible:=True;
            RadioButton4.visible:=true;
            dm.RestoreControls(panel3);
            cxGrid1.SetFocus;
        end
    else if (panel6.Visible = false) and (panel1.Visible = false) and (panel3.Visible = false) then Close;

end;

procedure TfrmIzborNaStavkiNalog.AKolicinaExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if AKolicina.Text <> '' then
        begin
           pom2:=StrToFloat(AKolicina.Text);
           AKolicina.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborNaStavkiNalog.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(102);
end;

procedure TfrmIzborNaStavkiNalog.aZacuvajGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true);
     zacuvajGridVoIni(cxGrid1DBTableView2,dmKon.aplikacija+Name+'2',true);
     zacuvajGridVoIni(cxGrid1DBTableView3,dmKon.aplikacija+Name+'3',true);
end;

procedure TfrmIzborNaStavkiNalog.CenaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (Cena.Text <> '') and (DDV.Text <> '') then
         begin
            dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (DDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
         end;
end;

procedure TfrmIzborNaStavkiNalog.CenaSoDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (CenaSoDDV.Text <> '') then
       begin
          dm.tblPoNalogStavkiCENA.Value:=dm.tblPoNalogStavkiCENADDV.Value*100/(100 + DDV.EditValue);
       end;
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom:integer;
begin
     case key of
         VK_RETURN:begin
             if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               begin
                    pom:=dm.zemiCountStavkiNalog(dm.CountStavkiNalog, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, dm.tblListaArtikliFiltriraniID.Value, dm.tblListaArtikliFiltriraniARTVID.Value, 'COUNTBR');
                    if pom  <> 0 then
                      ShowMessage('���� ������ � ������� �� ������� !!!')
                    else
                      begin
                           Panel6.Visible:=true;
                           Panel6.left:=184;
                           Panel6.top:=172;
                           RadioButton4.Visible:=false;
                           RadioButton1.Visible:=False;
                           AKolicina.SetFocus;
                           dm.tblPoNalogStavki.Insert;
                           StateActive:=dsInsert;
                           //ADDV.EditValue:=dm.tblListaArtikliFiltriraniTARIFA.Value;
                           dm.tblPoNalogStavkiCENA.Value:=0;
                           dm.tblPoNalogStavkiCENADDV.Value:=0;
                           dm.tblPoNalogStavkikolicina.Value:=0;
                           dm.tblPoNalogStavkiDDV.Value:=0;
                      end;
               end;
         end;
         VK_INSERT:begin
            frmArtikal:=TfrmArtikal.Create(Application);
            frmArtikal.Tag:=1;
            frmArtikal.ShowModal;
            frmArtikal.Free;
            dm.tblListaArtikliFiltrirani.fullRefresh;
            dm.tblListaArtikliFiltrirani.Locate('ID; ARTVID', VarArrayOf([dm.tblListaArtikliID.Value, dm.tblListaArtikliARTVID.Value]) , []);
         end;
      end;
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom:integer;
begin
     case key of
         VK_RETURN:begin
             if cxGrid1DBTableView2.Controller.SelectedRecordCount = 1 then
               begin
                    pom:=dm.zemiCountStavkiNalog(dm.CountStavkiNalog, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, dm.IzborStavkiOdNabavkaARTSIF.Value, dm.IzborStavkiOdNabavkaARTVID.Value, 'COUNTBR');
                    if pom <> 0 then
                      ShowMessage('���� ������ � ������� �� ������� !!!')
                    else
                      begin
                           Panel1.Visible:=true;
                           panel1.Tag:=0;
                           Panel1.left:=184;
                           Panel1.top:=172;
                           RadioButton1.Visible:=false;
                           RadioButton3.Visible:=False;
                           SKolicina.SetFocus;
                           dm.tblPoNalogStavki.Insert;
                           StateActive:=dsInsert;
                           //SDDV.EditValue:=dm.tblPoNabavkaStavkiDDV.Value;
                           dm.tblPoNalogStavkiCENA.Value:=0;
                           dm.tblPoNalogStavkiCENADDV.Value:=0;
                           dm.tblPoNalogStavkikolicina.Value:=0;
                           dm.tblPoNalogStavkiDDV.Value:=0;
                      end;
               end;
         end;
      end;
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView2);
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom, pom1:integer;
      pom2:Double;
begin
      case key of
         VK_RETURN:begin
             if cxGrid1DBTableView3.Controller.SelectedRecordCount = 1 then
               begin
                    pom:=dm.zemiCountStavkiNalog(dm.CountStavkiNalog, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, dm.tblPoPonudaStavkiARTSIF.Value, dm.tblPoPonudaStavkiARTVID.Value, 'COUNTBR');
                    if pom <> 0 then
                      ShowMessage('���� ������ � ������� �� ������� !!!')
                    else
                      begin
                           Panel3.Visible:=true;
                           Panel3.left:=184;
                           Panel3.top:=172;
                           RadioButton4.Visible:=false;
                           RadioButton3.Visible:=False;
                           Kolicina.SetFocus;
                           dm.tblPoNalogStavki.Insert;
                           StateActive:=dsInsert;
                           //DDV.EditValue:=dm.tblPoPonudaStavkiDDV.Value;
                           dm.tblPoNalogStavkiCENADDV.Value:=dm.tblPoPonudaStavkiKRAJNA_CENA.Value;
                           dm.tblPoNalogStavkiDDV.Value:=dm.tblPoPonudaStavkiDDV.Value;
                           dm.tblPoNalogStavkiCENA.Value:=dm.tblPoNalogStavkiCENADDV.Value*100/(100 + DDV.EditValue);
                           pom2:=dm.zemiSumaKolicina(dm.SumaPrifatenaKolicina,'BROJ','GODINA', 'ARTSIF','ARTVID', dm.tblPoPonudaStavkiBROJ.Value, dm.tblPoPonudaStavkiGODINA.Value, dm.tblPoPonudaStavkiARTSIF.Value, dm.tblPoPonudaStavkiARTVID.Value, 'KOLICINA');
                           SumaKolicina.Text:= FloatToStr(pom2);
                           if (dm.tblPoPonudaStavkiKOLICINA.Value - pom2) < 0 then
                              dm.tblPoNalogStavkiKOLICINA.Value:='0'
                           else
                              dm.tblPoNalogStavkiKOLICINA.Value:= dm.tblPoPonudaStavkiKOLICINA.Value - pom2;
                      end;
               end;
         end;
      end;
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView3KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView3);
end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
 var pom:integer;
begin
     case key of
         VK_RETURN:begin
                 pom:=dm.zemiCountStavkiNalog(dm.CountStavkiNalog, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblNalogBROJ.Value, dm.tblNalogGODINA.Value, dm.DetailStavkiNabavkaSIFRAARTIKAL.Value, dm.DetailStavkiNabavkaVIDARTIKAL.Value, 'COUNTBR');
                    if pom <> 0 then
                      ShowMessage('���� ������ � ������� �� ������� !!!')
                    else
                      begin
                           Panel1.tag:=1;
                           Panel1.Visible:=true;
                           Panel1.left:=184;
                           Panel1.top:=172;
                           RadioButton1.Visible:=false;
                           RadioButton3.Visible:=False;
                           SKolicina.SetFocus;
                           dm.tblPoNalogStavki.Insert;
                           StateActive:=dsInsert;
                           //SDDV.EditValue:=dm.tblPoNabavkaStavkiDDV.Value;
                           dm.tblPoNalogStavkiCENA.Value:=0;
                           dm.tblPoNalogStavkiCENADDV.Value:=0;
                           dm.tblPoNalogStavkikolicina.Value:=0;
                           dm.tblPoNalogStavkiDDV.Value:=0;
                           AOpis.Text:='';
                      end;
         end;
      end;

end;

procedure TfrmIzborNaStavkiNalog.cxGrid1DBTableView4KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView4);
end;

procedure TfrmIzborNaStavkiNalog.DDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (Cena.Text <> '') and (DDV.Text <> '') then
        begin
           dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (DDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
        end;
end;

procedure TfrmIzborNaStavkiNalog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      if StateActive in [dsInsert] then
        dm.tblPoNalogStavki.Cancel;
end;

procedure TfrmIzborNaStavkiNalog.FormCreate(Sender: TObject);
begin
      dm.tblListaArtikliFiltrirani.Close;
      dm.tblListaArtikliFiltrirani.Open;
      dm.IzborStavkiOdNabavka.Close;
      dm.DetailStavkiNabavka.Close;
      dm.IzborStavkiOdNabavka.ParamByName('NabavkaId').Value:= dm.tblNalogNABAVKA_ID.Value;
      dm.DetailStavkiNabavka.ParamByName('NabavkaId').Value:= dm.tblNalogNABAVKA_ID.Value;
      dm.IzborStavkiOdNabavka.Open;
      dm.DetailStavkiNabavka.Open;
end;

procedure TfrmIzborNaStavkiNalog.FormShow(Sender: TObject);
begin
     dm.PrvPosledenTab(Panel6,posledna,prva);
     dm.PrvPosledenTab(Panel3,posledna3,prva3);
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true,true);
     procitajGridOdIni(cxGrid1DBTableView2,dmKon.aplikacija+Name+'2',true,true);
     procitajGridOdIni(cxGrid1DBTableView3,dmKon.aplikacija+Name+'3',true,true);

     if (dm.tblNalogBROJNANABAVKA.Value = '') then
        RadioButton4.Enabled:=false;
     if  (dm.tblNalogBROJNAPONUDA.Value = '' )then
         begin
            RadioButton1.Enabled:=false;
            if dm.tblNalogBROJNANABAVKA.Value <> '' then
               RadioButton4.Checked:=True
         end
     else RadioButton1.Checked:=true;
     cxGrid1.SetFocus;
end;

procedure TfrmIzborNaStavkiNalog.KolicinaExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if Kolicina.Text <> '' then
        begin
           pom2:=StrToFloat(Kolicina.Text);
           Kolicina.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborNaStavkiNalog.RadioButton1Click(Sender: TObject);
begin
     if (dm.tblNalogBROJNAPONUDA.Value <> '') then
       begin
            dm.tblPoPonudaStavki.Close;
            dm.tblPoPonudaStavki.ParamByName('godina').Value:= dm.tblNalogPONUDA_GODINA.Value;
            dm.tblPoPonudaStavki.ParamByName('broj').Value:= dm.tblNalogPONUDA_BROJ.Value;
            dm.tblPoPonudaStavki.Open;
            cxGrid1Level4.Active:=True;
            cxGrid1Level4.GridView:=cxGrid1DBTableView3;
            cxGrid1.SetFocus;
       end
end;

procedure TfrmIzborNaStavkiNalog.RadioButton3Click(Sender: TObject);
begin
     cxGrid1Level1.Active:=True;
     cxGrid1Level1.GridView:=cxGrid1DBTableView1;
     cxGrid1.SetFocus;
end;

procedure TfrmIzborNaStavkiNalog.RadioButton4Click(Sender: TObject);
begin
     if dm.tblNalogBROJNANABAVKA.Value <> '' then
       begin
            cxGrid1Level2.Active:=True;
            cxGrid1Level2.GridView:=cxGrid1DBTableView2;
            cxGrid1.SetFocus;
       end
end;

procedure TfrmIzborNaStavkiNalog.SCenaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (SCena.Text <> '') and (SDDV.Text <> '') then
         begin
            dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (SDDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
         end;
end;

procedure TfrmIzborNaStavkiNalog.SCenaSoDDVExit(Sender: TObject);
var pom1, pom2 :extended;
begin
      if (SDDV.Text <> '') and (SCenaSoDDV.Text <> '')and (Panel1.Visible = true)  then
         begin
            pom2:=StrToFloat(SCenaSoDDV.text)*100/(100 + StrToFloat(SDDV.EditText));
            SCena.Text:=FormatFloat('0.0000',pom2);
            pom1:=StrToFloat(SCenaSoDDV.Text);
            SCenaSoDDV.Text:=FormatFloat('0.0000',pom1);
         end;

end;

procedure TfrmIzborNaStavkiNalog.SDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (SCena.Text <> '') and (SDDV.Text <> '') then
        begin
           dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (SDDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
        end;
end;

procedure TfrmIzborNaStavkiNalog.SKolicinaExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if SKolicina.Text <> '' then
        begin
           pom2:=StrToFloat(SKolicina.Text);
           SKolicina.Text:=FormatFloat('0.0000',pom2);
        end;
end;

end.
