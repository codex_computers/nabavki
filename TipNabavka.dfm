object frmTipNabavka: TfrmTipNabavka
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1058#1080#1087' '#1085#1072' '#1053#1072#1073#1072#1074#1082#1072
  ClientHeight = 372
  ClientWidth = 656
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dPanel: TPanel
    Left = 359
    Top = 0
    Width = 297
    Height = 353
    Align = alRight
    Anchors = [akRight, akBottom]
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    Enabled = False
    ParentBackground = False
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 30
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1064#1080#1092#1088#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 5
      Top = 57
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 5
      Top = 84
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Sifra: TcxDBTextEdit
      Left = 61
      Top = 27
      TabStop = False
      BeepOnEnter = False
      Constraints.MinHeight = 21
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTipNabavki
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.LookAndFeel.SkinName = 'GlassOceans'
      StyleDisabled.BorderColor = clHotLight
      StyleDisabled.Color = clSkyBlue
      StyleDisabled.LookAndFeel.SkinName = 'GlassOceans'
      StyleDisabled.TextColor = clBackground
      StyleFocused.Color = 16113353
      StyleFocused.LookAndFeel.SkinName = 'GlassOceans'
      StyleHot.LookAndFeel.SkinName = 'GlassOceans'
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object Naziv: TcxDBTextEdit
      Left = 61
      Top = 54
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipNabavki
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.LookAndFeel.SkinName = 'GlassOceans'
      StyleDisabled.LookAndFeel.SkinName = 'GlassOceans'
      StyleFocused.Color = 16113353
      StyleFocused.LookAndFeel.SkinName = 'GlassOceans'
      StyleHot.LookAndFeel.SkinName = 'GlassOceans'
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 220
    end
    object Opis: TcxDBTextEdit
      Left = 61
      Top = 81
      BeepOnEnter = False
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsTipNabavki
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.LookAndFeel.SkinName = 'GlassOceans'
      StyleDisabled.LookAndFeel.SkinName = 'GlassOceans'
      StyleFocused.Color = 16113353
      StyleFocused.LookAndFeel.SkinName = 'GlassOceans'
      StyleHot.LookAndFeel.SkinName = 'GlassOceans'
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 220
    end
    object ButtonOtkazi: TButton
      Left = 195
      Top = 300
      Width = 86
      Height = 25
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      Images = dm.cxSmallImages
      TabOrder = 4
      Visible = False
      OnClick = ButtonOtkaziClick
      OnKeyDown = EnterKakoTab
    end
    object ButtonZacuvaj: TButton
      Left = 103
      Top = 300
      Width = 86
      Height = 25
      Caption = #1047#1072#1095#1091#1074#1072#1112
      ImageIndex = 5
      Images = dm.cxSmallImages
      TabOrder = 3
      Visible = False
      OnClick = ButtonZacuvajClick
      OnKeyDown = EnterKakoTab
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 0
    Width = 359
    Height = 353
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 355
      Height = 349
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnKeyPress = cxGrid1DBTableView1KeyPress
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dm.dsTipNabavki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Background = dm.cxStyle1
        Styles.ContentEven = dm.cxStyle2
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 267
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          Caption = #1054#1087#1080#1089
          DataBinding.FieldName = 'OPIS'
          Width = 356
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 353
    Width = 656
    Height = 19
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F1- '#1055#1086#1084#1086#1096', Esc' +
          ' - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
  end
  object ActionList1: TActionList
    Left = 464
    Top = 152
    object aOtkaziIzlez: TAction
      Caption = 'aOtkaziIzlez'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aOtkaziIzlezExecute
    end
    object aPomos: TAction
      Caption = 'aPomos'
      ShortCut = 112
      OnExecute = aPomosExecute
    end
  end
end
