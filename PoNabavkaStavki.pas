unit PoNabavkaStavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls,
  dxStatusBar, dxRibbonStatusBar, RibbonLunaStyleActnCtrls, Ribbon, StdCtrls,
  cxContainer, cxTextEdit, cxDBEdit, ActnList, cxGridExportLink, cxExport,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmPoNabavkaStavki = class(TForm)
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    BrojNabavka: TcxDBTextEdit;
    Label2: TLabel;
    NabavkaId: TcxDBTextEdit;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Kreiral: TcxDBTextEdit;
    Vreme: TcxDBTextEdit;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    Grupa: TcxDBTextEdit;
    Podgrupa: TcxDBTextEdit;
    Kolicina: TcxDBTextEdit;
    RabEdinica: TcxDBTextEdit;
    cxGrid1DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVIDIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDINIDNAZIV: TcxGridDBColumn;
    ActionList1: TActionList;
    aESC: TAction;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    Cena: TcxDBTextEdit;
    DDV: TcxDBTextEdit;
    CenaSoDDV: TcxDBTextEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    Label9: TLabel;
    ID: TcxDBTextEdit;
    Merka: TcxDBTextEdit;
    aZacuvajVoExcel: TAction;
    cxGrid1DBTableView1Column7: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aZacuvajIzgled: TAction;
    procedure aESCExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPoNabavkaStavki: TfrmPoNabavkaStavki;

implementation

uses DaNe, dmKonekcija,  dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmPoNabavkaStavki.aESCExecute(Sender: TObject);
begin
            Close;
end;

procedure TfrmPoNabavkaStavki.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmPoNabavkaStavki.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPoNabavkaStavki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPoNabavkaStavki.FormShow(Sender: TObject);
begin
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
end;

end.
