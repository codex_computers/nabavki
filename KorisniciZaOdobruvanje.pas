unit KorisniciZaOdobruvanje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxStatusBar, dxRibbonStatusBar, ExtCtrls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxContainer, StdCtrls, cxTextEdit, cxDBEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ActnList, cxGroupBox, cxRadioGroup;

type
  TfrmKorisniciZaOdobruvanje = class(TForm)
    Panel1: TPanel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dPanel: TPanel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    ButtonZacuvaj: TButton;
    ButtonOtkazi: TButton;
    ActionList1: TActionList;
    aIzlez: TAction;
    aDodadi: TAction;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1USERID: TcxGridDBColumn;
    cxGrid1DBTableView1APPS: TcxGridDBColumn;
    cxGrid1DBTableView1FRM: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    aAzuriraj: TAction;
    ABrisi: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    procedure aIzlezExecute(Sender: TObject);
    procedure aDodadiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure ABrisiExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmKorisniciZaOdobruvanje: TfrmKorisniciZaOdobruvanje;

implementation

uses dmUnit, dmKonekcija, Utils;

{$R *.dfm}

procedure TfrmKorisniciZaOdobruvanje.aAzurirajExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          SIFRA.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Edit;
        end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmKorisniciZaOdobruvanje.ABrisiExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmKorisniciZaOdobruvanje.aDodadiExecute(Sender: TObject);
begin
      if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
         begin
           dPanel.Enabled:=True;
           lPanel.Enabled:=False;
           sifra.SetFocus;
           cxGrid1DBTableView1.DataController.DataSet.Insert;
           dm.tblSysFrmConfAPPS.Value:=dmKon.aplikacija;
           dm.tblSysFrmConfFRM.Value:='frmNabavki';
           dm.tblSysFrmConfOBJECT.Value:='aOdobriNabavka';
           dm.tblSysFrmConfPARAM.Value:='Enabled';
         end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmKorisniciZaOdobruvanje.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmKorisniciZaOdobruvanje.aOtkaziExecute(Sender: TObject);
begin
      if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
         begin
           ModalResult := mrCancel;
           Close();
          end
      else
          begin
             cxGrid1DBTableView1.DataController.DataSet.Cancel;
             dPanel.Enabled := false;
             lPanel.Enabled := true;
             cxGrid1.SetFocus;
           end;
end;

procedure TfrmKorisniciZaOdobruvanje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
     begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
     end;
end;

procedure TfrmKorisniciZaOdobruvanje.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmKorisniciZaOdobruvanje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
