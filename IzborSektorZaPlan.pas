unit IzborSektorZaPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, dxtree, dxdbtree, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxGraphics, cxCheckBox, cxDBEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxControls, cxContainer, cxEdit,
  cxTextEdit, StdCtrls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxStatusBar, dxRibbonStatusBar, ActnList, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmIzborSektorZaPlan = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    dxDBTreeView1: TdxDBTreeView;
    Label3: TLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel4: TPanel;
    Label4: TLabel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel5: TPanel;
    Rakovoditel: TcxDBTextEdit;
    Label2: TLabel;
    Label5: TLabel;
    Label15: TLabel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    Koren: TcxDBTextEdit;
    cbRE: TcxDBCheckBox;
    cbMagacin: TcxDBCheckBox;
    cbTroskovnoMesto: TcxDBCheckBox;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aPomos: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBTreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzborSektorZaPlan: TfrmIzborSektorZaPlan;

implementation

uses dmMaticni, dmUnit, dmKonekcija, DaNe, Utils;

{$R *.dfm}

procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
begin
     Assert(Assigned(Strings)) ;
     Strings.Clear;
     Strings.Delimiter := Delimiter;
     Strings.DelimitedText := Input;
end;
procedure TfrmIzborSektorZaPlan.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmIzborSektorZaPlan.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(111);
end;

procedure TfrmIzborSektorZaPlan.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:string;
    i, pom1, dolzina:integer;
    A: TStringList;
begin
     case key of
          vk_f8:begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
           begin
              pom:=dm.SetupPlanSektorV1.Value;
              A := TStringList.Create;
              try
                Split(',', pom, A) ;
                dolzina:=a.Count ;
                for I := 0 to dolzina - 2 do
                    begin
                      if IntToStr(dm.tblSysSektorPlanID.Value) = a[i] then
                         pom1:= i;
                    end;
                for i := pom1 to dolzina - 3 do
                 begin
                  a[i]:=a[i + 1];
                 end;
                pom:='';
                for i := 0 to dolzina - 3 do
                begin
                    pom:= pom + a[i] + ',';
                end;
              finally
                A.Free;
              end;
              dm.SetupPlanSektor.Edit;
              dm.SetupPlanSektorV1.Value:=pom;
              dm.SetupPlanSektor.Post;
              dm.tblSysSektorPlan.FullRefresh;
           end;
          end;
     end;
end;

procedure TfrmIzborSektorZaPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborSektorZaPlan.dxDBTreeView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:string;
    i, pom1, dolzina:integer;
    A: TStringList;

begin
     case Key of
       VK_RETURN:begin
         pom1:=0;
         pom:=dm.SetupPlanSektorV1.Value;
         A := TStringList.Create;
         try
            Split(',', pom, A) ;
            dolzina:= a.Count;
            for i := 0 to dolzina - 2 do
               if IntToStr(dmMat.tblREID.Value) = a[i] then
                  begin
                     pom1:=1;
                  end
         finally
            A.Free;
         end;
         if pom1 = 0 then
            begin
               pom:=pom  + IntToStr(dmMat.tblREID.Value)+ ',';
               dm.SetupPlanSektor.Edit;
               dm.SetupPlanSektorV1.Value:=pom;
               dm.SetupPlanSektor.Post;       dm.tblSysSektorPlan.FullRefresh;
            end
         else ShowMessage('���� ������� ������� � ��� �������');
       end;
     end;
end;

procedure TfrmIzborSektorZaPlan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmMat.tblRE.Close;
     dm.SetupPlanSektor.Close;
     dm.tblSysSektorPlan.close;
end;

procedure TfrmIzborSektorZaPlan.FormCreate(Sender: TObject);
begin
     dmMat.tblRE.Open;
     dm.SetupPlanSektor.Open;
     dm.tblSysSektorPlan.Open;
end;

end.
