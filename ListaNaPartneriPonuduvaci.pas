unit ListaNaPartneriPonuduvaci;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxContainer, cxLabel, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls, ActnList, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions;

type
  TfrmPartneriPonuduvaci = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxLabel1: TcxLabel;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    aPomos: TAction;
    RibbonComboBox1: TcxComboBox;
    cxLabel2: TcxLabel;
    aPecati: TAction;
    aDizajn: TAction;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RibbonComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPartneriPonuduvaci: TfrmPartneriPonuduvaci;

implementation

uses ArtGrupa, dmKonekcija, dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmPartneriPonuduvaci.aPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin

            if tag = 1 then
             begin
              dm.tblReportDizajn.ParamByName('id').Value:=4011;
              dm.tblReportDizajn.FullRefresh;

              dm.PonudiOdOdbranPartner.CLose;
              dm.PonudiOdOdbranPartner.ParamByName('partner').Value:=dm.PartneriPonuduvaciPARTNER.Value;
              dm.PonudiOdOdbranPartner.ParamByName('tipPartner').Value:=dm.PartneriPonuduvaciTIP_PARTNER.Value;
              dm.PonudiOdOdbranPartner.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
              dm.PonudiOdOdbranPartner.Open;

              dm.PonudiPartnerDetail.close;
              dm.PonudiPartnerDetail.Open;

              RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
              dm.frxReport.LoadFromStream(RptStream) ;

//              dm.frxReport.DesignReport();
                   dm.frxReport.ShowReport;
             end;
            if tag = 2 then
             begin
              dm.tblReportDizajn.ParamByName('id').Value:=4012;
              dm.tblReportDizajn.FullRefresh;

              dm.PrifateniPonudiOdOdbranPartner.CLose;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('partner').Value:=dm.PartneriPonuduvaciPARTNER.Value;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('tipPartner').Value:=dm.PartneriPonuduvaciTIP_PARTNER.Value;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('status').Value:=prifatena_pon;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('godina').Value:= RibbonComboBox1.EditValue;
              dm.PrifateniPonudiOdOdbranPartner.Open;

              dm.PonudiPartnerDetail.close;
              dm.PonudiPartnerDetail.Open;

              RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
              dm.frxReport.LoadFromStream(RptStream) ;

//              dm.frxReport.DesignReport();
                   dm.frxReport.ShowReport;
             end;
end;

procedure TfrmPartneriPonuduvaci.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin

            if tag = 1 then
             begin
              dm.tblReportDizajn.ParamByName('id').Value:=4011;
              dm.tblReportDizajn.FullRefresh;

              dm.PonudiOdOdbranPartner.CLose;
              dm.PonudiOdOdbranPartner.ParamByName('partner').Value:=dm.PartneriPonuduvaciPARTNER.Value;
              dm.PonudiOdOdbranPartner.ParamByName('tipPartner').Value:=dm.PartneriPonuduvaciTIP_PARTNER.Value;
              dm.PonudiOdOdbranPartner.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
              dm.PonudiOdOdbranPartner.Open;

              dm.PonudiPartnerDetail.close;
              dm.PonudiPartnerDetail.Open;

              RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
              dm.frxReport.LoadFromStream(RptStream) ;

//              dm.frxReport.DesignReport();
                   dm.frxReport.ShowReport;
             end;
            if tag = 2 then
             begin
              dm.tblReportDizajn.ParamByName('id').Value:=4012;
              dm.tblReportDizajn.FullRefresh;

              dm.PrifateniPonudiOdOdbranPartner.CLose;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('partner').Value:=dm.PartneriPonuduvaciPARTNER.Value;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('tipPartner').Value:=dm.PartneriPonuduvaciTIP_PARTNER.Value;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('status').Value:=prifatena_pon;
              dm.PrifateniPonudiOdOdbranPartner.ParamByName('godina').Value:= RibbonComboBox1.EditValue;
              dm.PrifateniPonudiOdOdbranPartner.Open;

              dm.PonudiPartnerDetail.close;
              dm.PonudiPartnerDetail.Open;

              RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
              dm.frxReport.LoadFromStream(RptStream) ;

              dm.frxReport.DesignReport();
             end;
end;

procedure TfrmPartneriPonuduvaci.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPartneriPonuduvaci.aPomosExecute(Sender: TObject);
begin
      Application.HelpContext(130);
end;

procedure TfrmPartneriPonuduvaci.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPartneriPonuduvaci.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.PartneriPonuduvaci.Close;
end;

procedure TfrmPartneriPonuduvaci.FormCreate(Sender: TObject);
begin
     dm.PartneriPonuduvaci.ParamByName('godina').Value:=dmKon.godina;
     dm.PartneriPonuduvaci.Open;
end;

procedure TfrmPartneriPonuduvaci.FormShow(Sender: TObject);
begin
     RibbonComboBox1.EditValue:=dmKon.godina;
end;

procedure TfrmPartneriPonuduvaci.RibbonComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.PartneriPonuduvaci.ParamByName('godina').Value:= RibbonComboBox1.EditValue;
     dm.PartneriPonuduvaci.FullRefresh;
end;

end.
