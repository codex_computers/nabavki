unit DrvoNaReporti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, dxtree, dxdbtree, StdCtrls, ActnList, cxGraphics,
  cxCustomData, cxStyles, cxTL, cxTLdxBarBuiltInMenu, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxControls, cxInplaceContainer, cxTLData, cxDBTL;

type
  TfrmDrvoNaReporti = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dxDBTreeView1: TdxDBTreeView;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    aPomos: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBTreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDrvoNaReporti: TfrmDrvoNaReporti;

implementation

uses dmKonekcija, dmMaticni, dmUnit, ArtGrupa, Artikli, ArtVid, cxConstantsMak,
  DaNe, dmLook, Drzava, EvidencijaNaNabavki, Firma, IzborGrupaZaPlan,
  IzborNaArtikli, IzborNaStavki, IzborNaStavkiZaNalog, IzborSektorZaPlan,
  ListaNaPartneriPonuduvaci, PregledPlanZaSiteSektori,
  PregledPlanZaIzbranSektor, PregledSpecifikacijaNaPlan, PonudeniArtikli;

{$R *.dfm}

procedure TfrmDrvoNaReporti.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmDrvoNaReporti.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(130);
end;

procedure TfrmDrvoNaReporti.dxDBTreeView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case key of
        VK_RETURN:begin
           if dm.drvoNaIzvestaiID.Value = 4011 then
            begin
             frmPartneriPonuduvaci:=TfrmPartneriPonuduvaci.Create(Application);
             frmPartneriPonuduvaci.Tag:=1;
             frmPartneriPonuduvaci.ShowModal;
             frmPartneriPonuduvaci.Free;
            end;
           if dm.drvoNaIzvestaiID.Value = 4012 then
            begin
             frmPartneriPonuduvaci:=TfrmPartneriPonuduvaci.Create(Application);
             frmPartneriPonuduvaci.Tag:=2;
             frmPartneriPonuduvaci.ShowModal;
             frmPartneriPonuduvaci.Free;
            end;
           if dm.drvoNaIzvestaiID.Value = 4001 then
            begin
             frmPlanZaSiteSektori:=TfrmPlanZaSiteSektori.Create(Application);
             frmPlanZaSiteSektori.ShowModal;
             frmPlanZaSiteSektori.Free;
            end;
           if dm.drvoNaIzvestaiID.Value = 4002 then
            begin
             frmPlanZaizbranSektor:=TfrmPlanZaizbranSektor.Create(Application);
             frmPlanZaizbranSektor.ShowModal;
             frmPlanZaizbranSektor.Free;
            end;
           if dm.drvoNaIzvestaiID.Value = 4003 then
            begin
             frmSpecifikacijaZaSiteSektori:=TfrmSpecifikacijaZaSiteSektori.Create(Application);
             frmSpecifikacijaZaSiteSektori.ShowModal;
             frmSpecifikacijaZaSiteSektori.Free;
            end;
          if dm.drvoNaIzvestaiID.Value = 4007 then
            begin
             frmPonudeniArtikli:=TfrmPonudeniArtikli.Create(Application);
             frmPonudeniArtikli.ShowModal;
             frmPonudeniArtikli.Free;
            end;
        end;
     end;
end;

procedure TfrmDrvoNaReporti.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.drvoNaIzvestai.Close;
end;

procedure TfrmDrvoNaReporti.FormCreate(Sender: TObject);
begin
     dm.drvoNaIzvestai.Open;
end;

end.
