unit RealizacijaPlanSektor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,cxGridExportLink, cxExport,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls,
  cxGridChartView, cxGridDBChartView, cxGridCustomPopupMenu, cxGridPopupMenu,
  ActnList, dxStatusBar, dxRibbonStatusBar, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinSeven, dxSkinSharp,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinsdxRibbonPainter, dxPSCore,
  dxPScxCommon,  dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxPScxGridLnk, dxPScxGridLayoutViewLnk;

type
  TfrmGrafikRealPlan = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBChartView1: TcxGridDBChartView;
    cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid2DBChartView1DataGroup3: TcxGridDBChartDataGroup;
    cxGrid1DBTableView1NABAVKACENA_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1ARTNAZIV_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1ARTSIF_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1PLANCENA_OUT: TcxGridDBColumn;
    cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aZacuvajIzgled: TAction;
    aZacuvajVoExcel: TAction;
    cxGrid2DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series2: TcxGridDBChartSeries;
    aPecati: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxCompositionReportLink;
    procedure FormCreate(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrafikRealPlan: TfrmGrafikRealPlan;

implementation

uses dmKonekcija, dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmGrafikRealPlan.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmGrafikRealPlan.aPecatiExecute(Sender: TObject);
begin
      dxComponentPrinter1.CurrentLink.Preview();
end;

procedure TfrmGrafikRealPlan.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmGrafikRealPlan.aZacuvajVoExcelExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := '����������������������' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, true);
end;

procedure TfrmGrafikRealPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGrafikRealPlan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.RealizPlanSektori.Close;
end;

procedure TfrmGrafikRealPlan.FormCreate(Sender: TObject);
begin
      dm.RealizPlanSektori.paramByName('poteklo').value:=inttostr(dmKon.re)+ ',';
      dm.RealizPlanSektori.paramByName('spisok').value:=inttostr(dmKon.re)+ ','+'%';
      dm.RealizPlanSektori.paramByName('godina').value:=dm.tblPlanSektorGODINA.Value;
      dm.RealizPlanSektori.open;
end;

procedure TfrmGrafikRealPlan.FormShow(Sender: TObject);
begin
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
end;

end.
