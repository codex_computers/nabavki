inherited frmArtikliP: TfrmArtikliP
  Caption = 'frmArtikliP'
  ExplicitWidth = 680
  ExplicitHeight = 577
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelD: TPanel
    inherited cxGrid1: TcxGrid
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsListaArtikli
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxGrid1DBTableView1GRUPA_BR: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA_BR'
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
        end
        object cxGrid1DBTableView1SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
        end
        object cxGrid1DBTableView1NNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NNAZIV'
        end
        object cxGrid1DBTableView1KKATALOG: TcxGridDBColumn
          DataBinding.FieldName = 'KKATALOG'
        end
        object cxGrid1DBTableView1PODGRUPA_BR: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPA_BR'
        end
        object cxGrid1DBTableView1PODGRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'PODGRUPA'
        end
        object cxGrid1DBTableView1PROIZVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'PROIZVODITEL'
        end
        object cxGrid1DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
        end
        object cxGrid1DBTableView1CCENA: TcxGridDBColumn
          DataBinding.FieldName = 'CCENA'
        end
        object cxGrid1DBTableView1BRPAKK: TcxGridDBColumn
          DataBinding.FieldName = 'BRPAKK'
        end
        object cxGrid1DBTableView1BBARKOD: TcxGridDBColumn
          DataBinding.FieldName = 'BBARKOD'
        end
        object cxGrid1DBTableView1PROIZVODITELID: TcxGridDBColumn
          DataBinding.FieldName = 'PROIZVODITELID'
        end
        object cxGrid1DBTableView1KKOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KKOLICINA'
        end
        object cxGrid1DBTableView1KLASIFIKACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'KLASIFIKACIJA'
        end
        object cxGrid1DBTableView1UNINAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'UNINAZIV'
        end
        object cxGrid1DBTableView1UNIFONT: TcxGridDBColumn
          DataBinding.FieldName = 'UNIFONT'
        end
        object cxGrid1DBTableView1OBLIK: TcxGridDBColumn
          DataBinding.FieldName = 'OBLIK'
        end
        object cxGrid1DBTableView1JACINA: TcxGridDBColumn
          DataBinding.FieldName = 'JACINA'
        end
        object cxGrid1DBTableView1MERKAID: TcxGridDBColumn
          DataBinding.FieldName = 'MERKAID'
        end
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 64
      FloatClientHeight = 120
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 111
      FloatClientHeight = 120
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 51
      FloatClientHeight = 80
    end
  end
end
