unit BaranjeZaPonuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxPC, cxControls,
  RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar, dxRibbonStatusBar, ExtCtrls,
  cxGraphics, StdCtrls, cxDBEdit, cxDropDownEdit, cxCalendar, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ActnList, ToolWin, ActnMan,
  ActnCtrls, RibbonActnCtrls, ActnMenus, RibbonActnMenus, cxGridExportLink, cxExport,
  RibbonSilverStyleActnCtrls, RibbonObsidianStyleActnCtrls, ComCtrls,
  cxDBExtLookupComboBox, DBCtrls, cxMemo, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  dxRibbon, dxBar, cxBarEditItem, Menus, cxButtons, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  PlatformDefaultStyleActnCtrls;

type
  TfrmBaranjeZaPonuda = class(TForm)
    Panel2: TPanel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2: TcxGridDBTableView;
    cxGridViewRepository1DBTableView2ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MAIL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2MOBILEN: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2TEL: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2OPIS: TcxGridDBColumn;
    ActionManager1: TActionManager;
    aIspratiPoMail: TAction;
    aPregledajGoBaraweto: TAction;
    aIzlez: TAction;
    GroupBox1: TGroupBox;
    Tekst: TcxDBMemo;
    Label5: TLabel;
    Naslov: TcxDBMemo;
    Label4: TLabel;
    Label6: TLabel;
    PKontaktNaziv: TcxExtLookupComboBox;
    PKontaktID: TcxDBTextEdit;
    Label3: TLabel;
    PPartner: TcxExtLookupComboBox;
    PPartnerID: TcxDBTextEdit;
    PTipPartner: TcxDBTextEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Firma: TcxTextEdit;
    ImePrezime: TcxTextEdit;
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1NASLOV: TcxGridDBColumn;
    cxGrid1DBTableView1TEXT: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    aPomos: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    aPreprati: TAction;
    aBrisi: TAction;
    aEdit: TAction;
    MailSender: TcxDBTextEdit;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    MailRecipient: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aZacuvajGoIzgledot: TAction;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    Action1: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action2: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    Zacuvaj: TcxButton;
    Otkazi: TcxButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aIspratiPoMailExecute(Sender: TObject);
    procedure aPregledajGoBarawetoExecute(Sender: TObject);
    procedure PTipPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure PPartnerIDPropertiesEditValueChanged(Sender: TObject);
    procedure PKontaktIDPropertiesEditValueChanged(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure aPrepratiExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aEditExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aZacuvajGoIzgledotExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    protected
    prva, posledna :TWinControl;
  end;

var
  frmBaranjeZaPonuda: TfrmBaranjeZaPonuda;
  StateActive:TDataSetState;

implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, TipPartner,
  Utils, Partner, Kontakt;

{$R *.dfm}

procedure TfrmBaranjeZaPonuda.aPrepratiExecute(Sender: TObject);
var RptStream :TStream;
    pid, ptip, kid: integer;  nas, par, psender, precipient:string; tek:Extended;
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           Panel2.Enabled:=True;
           Panel1.Enabled:=False;
           dxRibbon1.Enabled:=False;

           pid:=dm.tblNabavkaMailPARTNER.Value;
           ptip:=dm.tblNabavkaMailTIP_PARTNER.Value;
           psender:=dm.tblNabavkaMailMAIL_SENDER.Value;
           precipient:=dm.tblNabavkaMailMAIL_RECIPIENT.Value;

           if dm.tblNabavkaMailKONTAKT.AsVariant <> Null then
              kid:=dm.tblNabavkaMailKONTAKT.Value
           else
              kid:=0;
           nas:=dm.tblNabavkaMailNASLOV.Value;
           par:=dm.tblNabavkaMailNAZIV.Value;

           Firma.Text:=dm.FirmiNAZIV.Value;
           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
           ImePrezime.Text:=dm.tblPartnerNAZIV.Value;

           dm.tblNabavkaMail.Insert;
           StateActive:=dsInsert;

           dm.tblNabavkaMailTIP_PARTNER.Value:=ptip;
           dm.tblNabavkaMailPARTNER.Value:=pid;
           dm.tblNabavkaMailMAIL_SENDER.Value:= psender;
           dm.tblNabavkaMailMAIL_RECIPIENT.Value:= precipient;
           if kid <> 0 then
             dm.tblNabavkaMailKONTAKT.Value:=kid;
           dm.tblNabavkaMailNASLOV.Value:=nas;
           PPartner.Text:=par;
           MailRecipient.SetFocus;
        end
     else ShowMessage('����������� ������');
end;

procedure TfrmBaranjeZaPonuda.aSnimiIzgledExecute(Sender: TObject);
begin
        zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
        ZacuvajFormaIzgled(self);
end;

procedure TfrmBaranjeZaPonuda.aSnimiPecatenjeExecute(Sender: TObject);
begin
      zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmBaranjeZaPonuda.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmBaranjeZaPonuda.aZacuvajGoIzgledotExecute(Sender: TObject);
begin
      zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmBaranjeZaPonuda.aZapisiExecute(Sender: TObject);
var RptStream :TStream;
begin
      if StateActive in [dsEdit] then
        begin
           if (dm.Validacija(panel2) = false) then
              begin
                 dm.tblNabavkaMail.post;
                 StateActive:=dsBrowse;
                 dm.tblNabavkaMail.FullRefresh;
                 Panel2.Enabled:=False;
                 dxRibbon1.Enabled:=True;
                 Panel1.Enabled:=True;
                 cxGrid1.SetFocus;
                end;
        end;

      if (dm.Validacija(panel2) = false) and (StateActive in [dsInsert]) then
         begin
            dm.tblReportDizajn.ParamByName('id').Value:=4013;
            dm.tblReportDizajn.FullRefresh;

            dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
            dm.tblSpecifikacijaNaNabavka.FullRefresh;

            RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
            dm.frxReport.LoadFromStream(RptStream) ;

            dm.frxReport.PrepareReport();

            dm.frxMailExport2.Login:=MailSender.Text;
            dm.frxMailExport2.Password:=smpt_pass;
            dm.frxMailExport2.SmtpHost:=smpt_mail;
            dm.frxMailExport2.FromMail:=MailSender.Text; //from
            dm.frxMailExport2.FromName:=ImePrezime.Text;
            dm.frxMailExport2.FromCompany:=Firma.Text;
            dm.frxMailExport2.Address:=MailRecipient.Text; //to
            dm.frxMailExport2.Subject:=Naslov.Text;
            dm.frxMailExport2.Lines.Text:=Tekst.Text;
            dm.frxMailExport2.ExportFilter:=dm.frxJPEGExport1;

            dm.frxReport.Export(dm.frxMailExport2);

            dm.tblNabavkaMailID.Value:=dm.zemiPonudaStavkaID(dm.NabavkaMailID,Null, Null, Null, Null, Null, Null, 'MAXBR');
            dm.tblNabavkaMailNABAVKA_ID.Value:=dm.tblNabaviID.Value;
            dm.tblNabavkaMail.post;
            StateActive:=dsBrowse;
            dm.tblNabavkaMail.FullRefresh;
            dm.RestoreControls(panel2);

            Panel2.Enabled:=False;
            dxRibbon1.Enabled:=True;
            Panel1.Enabled:=True;
            cxGrid1.SetFocus;
         end;
end;

procedure TfrmBaranjeZaPonuda.aBrisiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
             cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
     else ShowMessage('����������� ������ !!!');
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 0 then
       begin
          panel2.Enabled:=true;
          Firma.Text:=dm.FirmiNAZIV.Value;
          dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
          PPartner.Text:= '';
          PKontaktNaziv.Text:= '';
          panel2.Enabled:=false;
       end;
end;

procedure TfrmBaranjeZaPonuda.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmBaranjeZaPonuda.aEditExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           Panel1.Enabled:=False;
           Panel2.Enabled:=True;
           dxRibbon1.Enabled:=False;
           //GroupBox2.Enabled:=False;
           PTipPartner.SetFocus;

           Firma.Text:=dm.FirmiNAZIV.Value;
           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
           ImePrezime.Text:=dm.tblPartnerNAZIV.Value;

           dm.tblNabavkaMail.Edit;
           StateActive:=dsEdit;
        end
     else ShowMessage('����������� ������ !!!');
end;

procedure TfrmBaranjeZaPonuda.aIspratiPoMailExecute(Sender: TObject);
begin
     Panel2.Enabled:=True;
     Panel1.Enabled:=False;
     dxRibbon1.Enabled:=False;

     Firma.Text:='';
     ImePrezime.Text:='';

     dm.tblNabavkaMail.Insert;
     StateActive:=dsInsert;
     PTipPartner.SetFocus;

     Firma.Text:=dm.FirmiNAZIV.Value;
     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
     ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
     dm.tblNabavkaMailMAIL_SENDER.Value:=dm.tblPartnerMAIL.Value;

     PPartner.Text:='';
     PKontaktNaziv.Text:= '';
end;

procedure TfrmBaranjeZaPonuda.aIzlezExecute(Sender: TObject);
begin
     if Panel2.Enabled = true then
        begin
           dm.tblNabavkaMail.Cancel;
           dm.RestoreControls(panel2);
           StateActive:=dsBrowse;

           //GroupBox2.Enabled:=true;
           Panel2.Enabled:=False;
           Panel1.Enabled:=True;
           dxRibbon1.Enabled:=True;

           if (PTipPartner.Text = '') and (PPartnerID.text = '') then
            begin
               PPartner.Text := '';
               PKontaktNaziv.Text := '';
            end;

           if PKontaktNaziv.Text = '' then
            begin
               PKontaktNaziv.Text := '';
            end;

           Firma.Text:=dm.FirmiNAZIV.Value;
           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
           cxGrid1.SetFocus;
        end
      else
        Close;
end;

procedure TfrmBaranjeZaPonuda.aOtkaziExecute(Sender: TObject);
begin
     dm.tblNabavkaMail.Cancel;
     StateActive:=dsBrowse;
     dm.RestoreControls(panel2);

     //GroupBox2.Enabled:=true;
     Panel2.Enabled:=False;
     Panel1.Enabled:=True;
     dxRibbon1.Enabled:=True;

     if (PTipPartner.Text = '') and (PPartnerID.text = '') then
      begin
         PPartner.Text := '';
         PKontaktNaziv.Text := '';
      end;

      if PKontaktNaziv.Text = '' then
      begin
         PKontaktNaziv.Text := '';
      end;

     Firma.Text:=dm.FirmiNAZIV.Value;
     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
     cxGrid1.SetFocus;
end;

procedure TfrmBaranjeZaPonuda.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmBaranjeZaPonuda.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmBaranjeZaPonuda.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmBaranjeZaPonuda.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(129);
end;

procedure TfrmBaranjeZaPonuda.aPregledajGoBarawetoExecute(Sender: TObject);
var RptStream :TStream;
begin
     PPartner.Text:= '';
     PKontaktNaziv.Text:= '';

     dm.tblReportDizajn.ParamByName('id').Value:=4013;
     dm.tblReportDizajn.FullRefresh;

     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblSpecifikacijaNaNabavka.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport();
end;

procedure TfrmBaranjeZaPonuda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.tblNabavkaMail.Close;
end;

procedure TfrmBaranjeZaPonuda.FormCreate(Sender: TObject);
begin
     dm.KontaktPartner.Open;
     dm.tblPartner.Close;
     dm.tblPartner.ParamByName('tipPartner').Value:='%';
     dm.tblPartner.Open;
     dm.tblNabavkaMail.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
     dm.tblNabavkaMail.Open;
end;

procedure TfrmBaranjeZaPonuda.FormShow(Sender: TObject);
begin
     dm.frxReport.Script.Clear;
     dm.frxReport.Clear;
     dm.PrvPosledenTab(panel2,posledna,prva);
     Firma.Text:=dm.FirmiNAZIV.Value;
     dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, dm.tblNabaviKONTAKT.Value]) , []);
     ImePrezime.Text:=dm.tblPartnerNAZIV.Value;
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
end;

procedure TfrmBaranjeZaPonuda.PKontaktIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if PKontaktID.Text <> '' then
        begin
           PKontaktNaziv.EditValue:=StrToInt(PKontaktID.Text);
        end;
end;

procedure TfrmBaranjeZaPonuda.PPartnerIDPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
    if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
       begin
          pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
          if pom = 1  then
            begin
               dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
               PPartner.Text:= dm.tblPartnerNAZIV.Value;
            end
         else PPartner.Text:= '';
    end;
end;

procedure TfrmBaranjeZaPonuda.PPartnerPropertiesEditValueChanged(
  Sender: TObject);
begin
     if PPartner.Text <> '' then
        begin
           PTipPartner.Text :=IntToStr(dm.tblPartnerTIP_PARTNER.Value);
           PPartnerID.Text:=IntToStr(dm.tblPartnerID.Value);
        end;
     PKontaktNaziv.Text:='';
     PKontaktID.Text:='';
     dm.KontaktPartner.ParamByName('tippartner').Value:=dm.tblPartnerTIP_PARTNER.Value;
     dm.KontaktPartner.ParamByName('partner').Value:=dm.tblPartnerID.Value;
     dm.KontaktPartner.FullRefresh;
end;

procedure TfrmBaranjeZaPonuda.PTipPartnerPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
     if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
        begin
           pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
           if pom = 1  then
             begin
                dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                PPartner.Text:= dm.tblPartnerNAZIV.Value;
             end
           else PPartner.Text:= '';
        end;
end;

procedure TfrmBaranjeZaPonuda.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
          begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
          end;
        VK_UP:
          begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
          end;
        VK_RETURN:
          begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
          end;
        VK_INSERT:begin
                   if (sender = PPartner) or (Sender = PPartnerID )then
                      begin
                         frmPartner:=TfrmPartner.Create(Application);
                         frmPartner.Tag:=4;
                         frmPartner.ShowModal;
                         frmPartner.Free;
                         PPartner.Text:=dm.tblPartnerNAZIV.Value;
                         dm.tblNabavkaMailPARTNER.Value:=dm.tblPartnerID.Value;
                         dm.tblNabavkaMailTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
                         PKontaktID.SetFocus;
                         dm.tblPartner.ParamByName('tipPartner').Value:='%';
                         dm.tblPartner.FullRefresh;
                      end;
                   if (sender = PTipPartner) then
                      begin
                         frmTipPartner:=TfrmTipPartner.Create(Application);
                         frmTipPartner.Tag:=1;
                         frmTipPartner.ShowModal;
                         frmTipPartner.Free;
                         dm.tblNabavkaMailTIP_PARTNER.Value:=dmMat.tblTipPartnerID.Value;
                         PPartnerID.SetFocus;
                      end;
                   if (Sender = PKontaktID) or (Sender =PKontaktNaziv) then
                      begin
                         if (PPartner.Text <> '') and (PPartnerID.Text <> '') and (PTipPartner.Text <> '') then
                            begin
                               dm.tblKontakt.close;
                               dm.tblKontakt.ParamByName('tipPartner').Value:=StrToInt(PTipPartner.Text);
                               dm.tblKontakt.ParamByName('Partner').Value:=StrToInt(PPartnerID.Text);
                               dm.tblKontakt.Open;
                               frmKontakt:=TfrmKontakt.Create(Application);
                               dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                               frmKontakt.Tag:= 1;
                               frmKontakt.ShowModal;
                               frmKontakt.Free;
                               dm.KontaktPartner.ParamByName('tippartner').Value:=StrToInt(PTipPartner.Text);
                               dm.KontaktPartner.ParamByName('partner').Value:=StrToInt(PPartnerID.Text);
                               dm.KontaktPartner.FullRefresh;
                               dm.tblNabavkaMailKONTAKT.Value:=dm.tblKontaktID.Value;
                               PKontaktNaziv.EditValue:=dm.tblKontaktID.Value;
                            end;
                      end;
          end;

    end;
end;

procedure TfrmBaranjeZaPonuda.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmBaranjeZaPonuda.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;

    if Sender = PKontaktNaziv then
      begin
        if (PKontaktNaziv.Text <> '') and (StateActive in [dsInsert, dsEdit]) and (panel2.Enabled = true)then
        begin
           PKontaktID.Text:=IntToStr(dm.KontaktPartnerID.Value);
           if dm.KontaktPartnerMAIL.AsVariant <> Null then
              dm.tblNabavkaMailMAIL_RECIPIENT.Value:= dm.KontaktPartnerMAIL.Value;
        end;
      end;
end;

end.
