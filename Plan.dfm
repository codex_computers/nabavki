object frmPlan: TfrmPlan
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1083#1072#1085
  ClientHeight = 768
  ClientWidth = 1201
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1201
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 745
    Width = 1201
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', I' +
          'nsert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072', Ctrl+F8 - '#1041#1088#1080#1096 +
          #1080' '#1089#1090#1072#1074#1082#1072', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel2: TPanel
    Left = 0
    Top = 126
    Width = 1201
    Height = 284
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 2
    object cxPageControl1: TcxPageControl
      Left = 2
      Top = 2
      Width = 1197
      Height = 280
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      Properties.NavigatorPosition = npLeftTop
      Properties.Style = 11
      OnPageChanging = cxPageControl1PageChanging
      ClientRectBottom = 278
      ClientRectLeft = 2
      ClientRectRight = 1195
      ClientRectTop = 22
      object cxTabSheet1: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1193
          Height = 256
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid1DBTableView1KeyDown
            Navigator.Buttons.CustomButtons = <>
            FilterBox.Visible = fvNever
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPlanSektor
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'STATUSNAZIV'
              Width = 92
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1083#1072#1085
              DataBinding.FieldName = 'BROJGODINA'
              Width = 100
            end
            object cxGrid1DBTableView1BROJ1: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1083#1072#1085
              DataBinding.FieldName = 'BROJ'
              Visible = False
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_KREIRAN'
              Width = 130
            end
            object cxGrid1DBTableView1DatumOd: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1086#1076
              DataBinding.FieldName = 'DATUM_OD'
              Width = 82
            end
            object cxGrid1DBTableView1DatumDo: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1076#1086
              DataBinding.FieldName = 'DATUM_DO'
              Width = 77
            end
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
            end
            object cxGrid1DBTableView1RabEdinica: TcxGridDBColumn
              DataBinding.FieldName = 'RABEDIDNAZIV'
              Width = 384
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Width = 240
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM_ODOBRUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_ODOBRUVANJE'
              Width = 100
            end
            object cxGrid1DBTableView1ODOBRIL: TcxGridDBColumn
              DataBinding.FieldName = 'ODOBRIL'
              Width = 206
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'KREIRAL'
              Visible = False
              Width = 113
            end
            object cxGrid1DBTableView1Column2: TcxGridDBColumn
              Caption = #1042#1088#1077#1084#1077
              DataBinding.FieldName = 'VREME'
              Visible = False
              Width = 118
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1193
          Height = 256
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          DesignSize = (
            1193
            256)
          object Label12: TLabel
            Left = 53
            Top = 27
            Width = 42
            Height = 13
            Caption = #1043#1086#1076#1080#1085#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 70
            Top = 54
            Width = 25
            Height = 13
            Caption = #1041#1088#1086#1112
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label16: TLabel
            Left = 323
            Top = 121
            Width = 70
            Height = 13
            Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 320
            Top = 27
            Width = 76
            Height = 13
            Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 344
            Top = 133
            Width = 49
            Height = 13
            Caption = #1085#1072#1073#1072#1074#1082#1072
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 54
            Top = 81
            Width = 41
            Height = 13
            Caption = #1057#1090#1072#1090#1091#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 19
            Top = 102
            Width = 76
            Height = 26
            Alignment = taRightJustify
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1114#1077
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object POpis: TcxDBMemo
            Left = 402
            Top = 118
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsPlanSektor
            Properties.WantReturns = False
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Height = 74
            Width = 721
          end
          object PGodina: TcxDBComboBox
            Tag = 1
            Left = 101
            Top = 24
            BeepOnEnter = False
            DataBinding.DataField = 'GODINA'
            DataBinding.DataSource = dm.dsPlanSektor
            Properties.Items.Strings = (
              '2000'
              '2001'
              '2002'
              '2003'
              '2004'
              '2005'
              '2006'
              '2007'
              '2008'
              '2009'
              '2010'
              '2011'
              '2012'
              '2013'
              '2014'
              '2015'
              '2016'
              '2017'
              '2018'
              '2019'
              '2020'
              '2021'
              '2022'
              '2023'
              '2024'
              '2025'
              '2026'
              '2027'
              '2028'
              '2029'
              '2030'
              '2031'
              '2032'
              '2033'
              '2034'
              '2035'
              '2036'
              '2037'
              '2038'
              '2039'
              '2040'
              '2041'
              '2042'
              '2043'
              '2044'
              '2045'
              '2046'
              '2047'
              '2048'
              '2049'
              '2050')
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 165
          end
          object PBroj: TcxDBTextEdit
            Tag = 1
            Left = 102
            Top = 51
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsPlanSektor
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Width = 165
          end
          object PRabEdinica: TcxDBLookupComboBox
            Tag = 1
            Left = 455
            Top = 24
            Anchors = [akLeft, akTop, akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'RE'
            DataBinding.DataSource = dm.dsPlanSektor
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                Caption = #1053#1072#1079#1080#1074
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsIzbraniSektoriZaPlan
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Width = 668
          end
          object PRabEdinicaID: TcxDBTextEdit
            Tag = 1
            Left = 402
            Top = 24
            BeepOnEnter = False
            DataBinding.DataField = 'RE'
            DataBinding.DataSource = dm.dsPlanSektor
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Width = 53
          end
          object Status: TcxDBLookupComboBox
            Tag = 1
            Left = 101
            Top = 78
            BeepOnEnter = False
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsPlanSektor
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dm.dsStatus
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.Color = 16113353
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Width = 166
          end
          object DatumOdobruvanje: TcxDBDateEdit
            Left = 101
            Top = 105
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_ODOBRUVANJE'
            DataBinding.DataSource = dm.dsPlanSektor
            Style.Color = 16775924
            Style.LookAndFeel.SkinName = ''
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clBackground
            StyleFocused.BorderStyle = ebsUltraFlat
            StyleFocused.Color = 16113353
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnKeyDown = EnterKakoTab
            Width = 166
          end
          object cxGroupBox1: TcxGroupBox
            Left = 402
            Top = 51
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1087#1083#1072#1085
            TabOrder = 6
            Height = 69
            Width = 721
            object Label13: TLabel
              Left = 31
              Top = 28
              Width = 58
              Height = 13
              Caption = #1044#1072#1090#1091#1084' '#1086#1076
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label14: TLabel
              Left = 263
              Top = 28
              Width = 58
              Height = 13
              Caption = #1044#1072#1090#1091#1084' '#1076#1086
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object PDatumOd: TcxDBDateEdit
              Tag = 1
              Left = 95
              Top = 25
              BeepOnEnter = False
              DataBinding.DataField = 'DATUM_OD'
              DataBinding.DataSource = dm.dsPlanSektor
              Style.Color = 16775924
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnKeyDown = EnterKakoTab
              Width = 138
            end
            object PDatumDo: TcxDBDateEdit
              Tag = 1
              Left = 327
              Top = 25
              BeepOnEnter = False
              DataBinding.DataField = 'DATUM_DO'
              DataBinding.DataSource = dm.dsPlanSektor
              Style.Color = 16775924
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnKeyDown = EnterKakoTab
              Width = 138
            end
          end
          object OtkaziButton: TcxButton
            Left = 1048
            Top = 210
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 9
          end
          object ZapisiButton: TcxButton
            Left = 967
            Top = 210
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 8
          end
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 410
    Width = 1201
    Height = 335
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 3
    object cxPageControl2: TcxPageControl
      Left = 2
      Top = 2
      Width = 1197
      Height = 331
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheet3
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 331
      ClientRectRight = 1197
      ClientRectTop = 24
      object cxTabSheet3: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        Color = clCream
        ImageIndex = 0
        ParentColor = False
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1197
          Height = 307
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid2DBTableView1KeyDown
            OnKeyPress = cxGrid2DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsSpecifikacijaPlan
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00 , .'
                Kind = skSum
                Column = cxGrid2DBTableView1IIZNOS
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGrid2DBTableView1Artvid: TcxGridDBColumn
              Caption = #1043#1088#1091#1087#1072'(ID)'
              DataBinding.FieldName = 'ARTVID'
              HeaderAlignmentHorz = taRightJustify
              Width = 70
            end
            object cxGrid2DBTableView1ArtvidNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'ARTVIDNAZIV'
              Width = 158
            end
            object cxGrid2DBTableView1ARTSIF: TcxGridDBColumn
              DataBinding.FieldName = 'ARTSIF'
              HeaderAlignmentHorz = taRightJustify
              Width = 72
            end
            object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV'
              Width = 279
            end
            object cxGrid2DBTableView1Cena: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              HeaderAlignmentHorz = taRightJustify
              Width = 92
            end
            object cxGrid2DBTableView1DDV: TcxGridDBColumn
              DataBinding.FieldName = 'DDV'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid2DBTableView1CENADDV: TcxGridDBColumn
              DataBinding.FieldName = 'CENADDV'
              HeaderAlignmentHorz = taRightJustify
              Width = 93
            end
            object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
              DataBinding.FieldName = 'KOLICINA'
              HeaderAlignmentHorz = taRightJustify
              Width = 76
            end
            object cxGrid2DBTableView1IIZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'IIZNOS'
              HeaderAlignmentHorz = taRightJustify
              Width = 87
            end
            object cxGrid2DBTableView1MerkaID: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072'(ID)'
              DataBinding.FieldName = 'MERKAID'
              Width = 76
            end
            object cxGrid2DBTableView1MerkaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'MERKANAZIV'
              Width = 82
            end
            object cxGrid2DBTableView1PodgrupaID: TcxGridDBColumn
              DataBinding.FieldName = 'PODGRUPAID'
              Width = 90
            end
            object cxGrid2DBTableView1PodgrupaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'PODGRUPANAZIV'
              Width = 152
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
      object cxTabSheet4: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1197
          Height = 307
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          DesignSize = (
            1197
            307)
          object GroupBox2: TGroupBox
            Left = 583
            Top = 14
            Width = 189
            Height = 161
            Caption = #1055#1083#1072#1085#1080#1088#1072#1085#1072' '#1094#1077#1085#1072' '#1080' '#1082#1086#1083#1080#1095#1080#1085#1072
            TabOrder = 1
            object Label21: TLabel
              Left = 31
              Top = 27
              Width = 54
              Height = 13
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label20: TLabel
              Left = 55
              Top = 54
              Width = 30
              Height = 13
              Caption = #1062#1077#1085#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label31: TLabel
              Left = 35
              Top = 81
              Width = 50
              Height = 13
              Caption = #1044#1044#1042'(%)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label30: TLabel
              Left = 11
              Top = 108
              Width = 74
              Height = 13
              Caption = #1062#1077#1085#1072' '#1089#1086' '#1076#1076#1074
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label22: TLabel
              Left = 51
              Top = 135
              Width = 34
              Height = 13
              Caption = #1048#1079#1085#1086#1089
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SKolicina: TcxDBTextEdit
              Tag = 1
              Left = 91
              Top = 24
              BeepOnEnter = False
              DataBinding.DataField = 'KOLICINA'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderColor = clBtnShadow
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SCena: TcxDBTextEdit
              Tag = 1
              Left = 91
              Top = 51
              BeepOnEnter = False
              DataBinding.DataField = 'CENA'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderColor = clBtnShadow
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SDDV: TcxDBLookupComboBox
              Tag = 1
              Left = 91
              Top = 78
              BeepOnEnter = False
              DataBinding.DataField = 'DDV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Properties.DropDownListStyle = lsFixedList
              Properties.KeyFieldNames = 'TARIFA'
              Properties.ListColumns = <
                item
                  FieldName = 'TARIFA'
                end>
              Properties.ListSource = dm.dsDDV
              Style.Color = 16775924
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleFocused.BorderColor = clBtnShadow
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 67
            end
            object SCenaSoDDV: TcxDBTextEdit
              Tag = 1
              Left = 91
              Top = 105
              BeepOnEnter = False
              DataBinding.DataField = 'CENADDV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderColor = clBtnShadow
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.Color = 16113353
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 86
            end
            object SIznos: TcxDBTextEdit
              Left = 91
              Top = 132
              BeepOnEnter = False
              DataBinding.DataField = 'IIZNOS'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.BorderColor = clBtnShadow
              StyleFocused.BorderStyle = ebsUltraFlat
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 4
              Width = 86
            end
          end
          object GroupBox1: TGroupBox
            Left = 10
            Top = 14
            Width = 567
            Height = 141
            Caption = #1054#1089#1085#1086#1074#1085#1080' '#1082#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1079#1072' '#1072#1088#1090#1080#1082#1072#1083
            TabOrder = 0
            object Label25: TLabel
              Left = 56
              Top = 33
              Width = 37
              Height = 13
              Caption = #1043#1088#1091#1087#1072' '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label18: TLabel
              Left = 13
              Top = 58
              Width = 80
              Height = 13
              Caption = #1064#1080#1092#1088#1072'/'#1053#1072#1079#1080#1074
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label28: TLabel
              Left = 35
              Top = 83
              Width = 58
              Height = 13
              Caption = #1055#1086#1076#1075#1088#1091#1087#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label29: TLabel
              Left = 43
              Top = 108
              Width = 50
              Height = 13
              Caption = #1045#1076'. '#1084#1077#1088#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object SArtvid: TcxDBTextEdit
              Left = 99
              Top = 30
              DataBinding.DataField = 'ARTVID'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clBackground
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 0
              Width = 42
            end
            object SArtvidNaziv: TcxDBTextEdit
              Left = 144
              Top = 30
              DataBinding.DataField = 'ARTVIDNAZIV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clBackground
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 1
              Width = 409
            end
            object SSifra: TcxDBTextEdit
              Left = 99
              Top = 55
              DataBinding.DataField = 'ARTSIF'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.SkinName = ''
              Style.IsFontAssigned = True
              StyleDisabled.BorderColor = cl3DLight
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clBackground
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 2
              Width = 78
            end
            object SNaziv: TcxDBTextEdit
              Left = 180
              Top = 55
              DataBinding.DataField = 'NAZIV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = [fsBold]
              Style.LookAndFeel.SkinName = ''
              Style.IsFontAssigned = True
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 3
              Width = 373
            end
            object SPodgrupaId: TcxDBTextEdit
              Left = 99
              Top = 80
              DataBinding.DataField = 'PODGRUPAID'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 4
              Width = 42
            end
            object SPodgrupaNaziv: TcxDBTextEdit
              Left = 144
              Top = 80
              DataBinding.DataField = 'PODGRUPANAZIV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 5
              Width = 409
            end
            object SMerkaId: TcxDBTextEdit
              Left = 99
              Top = 105
              DataBinding.DataField = 'MERKAID'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 6
              Width = 42
            end
            object SMerkaNaziv: TcxDBTextEdit
              Left = 144
              Top = 105
              DataBinding.DataField = 'MERKANAZIV'
              DataBinding.DataSource = dm.dsSpecifikacijaPlan
              Enabled = False
              Style.LookAndFeel.SkinName = ''
              StyleDisabled.Color = 16113353
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleDisabled.TextColor = clWindowText
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 7
              Width = 112
            end
          end
          object OtkaziStavka: TcxButton
            Left = 1052
            Top = 203
            Width = 75
            Height = 25
            Action = aOtkaziStavka
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 3
          end
          object ZapisiStavka: TcxButton
            Left = 971
            Top = 203
            Width = 75
            Height = 25
            Action = aZapisiStavki
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 2
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 376
    Top = 296
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1083#1072#1085
      CaptionButtons = <>
      DockedLeft = 382
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 885
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1115
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxGodina'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 295
          Visible = True
          ItemName = 'cxRabEdinica'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 671
      DockedTop = 0
      FloatLeft = 1235
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1048#1079#1074#1077#1096#1090#1072#1080
      CaptionButtons = <>
      DockedLeft = 760
      DockedTop = 0
      FloatLeft = 1235
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      CaptionButtons = <>
      DockedLeft = 557
      DockedTop = 0
      FloatLeft = 1235
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' - '#1055#1083#1072#1085
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object cxGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cxGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
    end
    object cxRabEdinica: TcxBarEditItem
      Caption = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      Category = 0
      Hint = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      Visible = ivAlways
      OnChange = cxRabEdinicaChange
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          Width = 100
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          Width = 1000
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsIzbraniSektoriZaPlan
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aNovaStavka
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aAzurirajStavka
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiStavka
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aZacuvajStavkiExcel
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = aPlanZaIzbraniotSektor
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080#1086#1090' '#1089#1077#1082#1090#1086#1088
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aOtvoriOdobri
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aPlanZaSiteSektori
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1089#1080#1090#1077' '#1089#1077#1082#1090#1086#1088#1080
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aRealizacijaKolSektor
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1087#1083#1072#1085' '#1087#1086' '#1082#1086#1083#1080#1095#1080#1085#1072
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiStavki
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aGodisenPlan
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1087#1083#1072#1085' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080'  - '#1055#1083#1072#1085
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aNovaStavka: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovaStavkaExecute
    end
    object aAzurirajStavka: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      SecondaryShortCuts.Strings = (
        'Ctrl+F6')
      OnExecute = aAzurirajStavkaExecute
    end
    object aBrisiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      SecondaryShortCuts.Strings = (
        'Ctrl+F8')
      OnExecute = aBrisiStavkaExecute
    end
    object aZacuvajStavkiExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1089#1090#1072#1074#1082#1080' '#1074#1086' Excel'
      ImageIndex = 9
      OnExecute = aZacuvajStavkiExcelExecute
    end
    object aZapisiStavki: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiStavkiExecute
    end
    object aOtkaziStavka: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziStavkaExecute
    end
    object aStavkiZaOdbranPlan: TAction
      Caption = 'aStavkiZaOdbranPlan'
      ImageIndex = 19
      OnExecute = aStavkiZaOdbranPlanExecute
    end
    object aRealizacijaKolSektor: TAction
      Caption = 'aRealizacijaKolSektor'
      ImageIndex = 19
      OnExecute = aRealizacijaKolSektorExecute
    end
    object aOtvoriOdobri: TAction
      Caption = #1054#1076#1086#1073#1088#1080'/'#1054#1090#1074#1086#1088#1080' '#1087#1083#1072#1085
      ImageIndex = 6
      OnExecute = aOtvoriOdobriExecute
    end
    object aPlanZaIzbraniotSektor: TAction
      Caption = 'aPlanZaIzbraniotSektor'
      ImageIndex = 19
      OnExecute = aPlanZaIzbraniotSektorExecute
    end
    object aPlanZaSiteSektori: TAction
      Caption = 'aPlanZaSiteSektori'
      ImageIndex = 19
      OnExecute = aPlanZaSiteSektoriExecute
    end
    object aPecatiStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' - '#1057#1090#1072#1074#1082#1080
      ImageIndex = 30
      OnExecute = aPecatiStavkiExecute
    end
    object aGodisenPlan: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aGodisenPlanExecute
    end
    object aDizajnPlanZaIzbraniotSektor: TAction
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080#1086#1090' '#1089#1077#1082#1090#1086#1088
      ImageIndex = 19
      OnExecute = aDizajnPlanZaIzbraniotSektorExecute
    end
    object aDizajnPlanZaSiteSektori: TAction
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1089#1080#1090#1077' '#1089#1077#1082#1090#1086#1088#1080
      ImageIndex = 19
      OnExecute = aDizajnPlanZaSiteSektoriExecute
    end
    object aPopupDizajn: TAction
      Caption = 'aPopupDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aPopupDizajnExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]'
        '')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44228.567409479170000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44228.567409513890000000
      ShrinkToPageWidth = True
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 664
    Top = 65520
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 224
    Top = 304
  end
  object PopupMenu2: TPopupMenu
    Left = 456
    Top = 240
    object N2: TMenuItem
      Action = aDizajnPlanZaIzbraniotSektor
    end
    object N3: TMenuItem
      Action = aDizajnPlanZaSiteSektori
    end
  end
end
