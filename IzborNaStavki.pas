unit IzborNaStavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, StdCtrls, ExtCtrls, cxControls,
  dxStatusBar, dxRibbonStatusBar, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxDBExtLookupComboBox, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxContainer,
  cxTextEdit, ActnList, cxDBEdit, cxMemo, DBCtrls, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom,
  dxSkinFoggy, dxSkinSeven, dxSkinSharp, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver;

type
  TfrmIzborStavki = class(TForm)
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    Panel6: TPanel;
    Bevel2: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    ActionList1: TActionList;
    aIzlez: TAction;
    Label14: TLabel;
    Label15: TLabel;
    sLabel: TLabel;
    SOpis: TcxDBMemo;
    Panel3: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    NValuta: TcxDBLookupComboBox;
    NOpis: TcxDBMemo;
    aZacuvajGoIzgledot: TAction;
    cxGrid1DBTableView2ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView2ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView2SUMAKOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView2SUMACENA: TcxGridDBColumn;
    cxGrid1DBTableView2DDV: TcxGridDBColumn;
    cxGrid1DBTableView2SUMACENADDV: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2GRUPAIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2Column1: TcxGridDBColumn;
    cxGrid1DBTableView2Column2: TcxGridDBColumn;
    SMerkaId: TcxDBTextEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1Level3: TcxGridLevel;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3VIDARTIKAL_GEN: TcxGridDBColumn;
    cxGrid1DBTableView3SIFRAARTIKAL_GEN: TcxGridDBColumn;
    cxGrid1DBTableView3OPSTNAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView3VIDARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView3SIFRAARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView3NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1BRPAK: TcxGridDBColumn;
    cxGrid1DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    �Pomos: TAction;
    SValuta: TcxDBLookupComboBox;
    SKurs: TcxDBTextEdit;
    SCena: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCenaSoDDV: TcxDBTextEdit;
    SRabat: TcxDBTextEdit;
    SKolicina: TcxDBTextEdit;
    NKurs: TcxDBTextEdit;
    NCena: TcxDBTextEdit;
    NDDV: TcxDBLookupComboBox;
    NCenaSoDDV: TcxDBTextEdit;
    NRabat: TcxDBTextEdit;
    NKolicina: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    procedure RadioButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
    procedure cxGrid1DBTableView2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure EnterKakoTabPanel3(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aZacuvajGoIzgledotExecute(Sender: TObject);
    procedure NCenaExit(Sender: TObject);
    procedure NCenaSoDDVExit(Sender: TObject);
    procedure NDDVPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1DBTableView3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NKolicinaExit(Sender: TObject);
    procedure SKursExit(Sender: TObject);
    procedure NKursExit(Sender: TObject);
    procedure NRabatExit(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure �PomosExecute(Sender: TObject);
    procedure SCenaExit(Sender: TObject);
    procedure SDDVPropertiesEditValueChanged(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzborStavki: TfrmIzborStavki;

implementation

uses dmKonekcija, dmMaticni, dmUnit, IzborNaArtikli, Utils, Artikal;

{$R *.dfm}

procedure TfrmIzborStavki.aIzlezExecute(Sender: TObject);
begin
     //and (StateActive in [dsInsert])
     if Panel6.Visible = true then
        begin
            panel6.Visible:= false;
            dm.RestoreControls(panel6);
            RadioButton4.Visible:=True;
            dm.tblPoPonudaStavki.Cancel;
            StateActive:=dsBrowse;
            cxGrid1.SetFocus;
        end
      else if Panel3.Visible = true then
        begin
            panel3.Visible:= false;
            dm.RestoreControls(panel3);
            RadioButton3.Visible:=True;
            dm.tblPoPonudaStavki.Cancel;
            StateActive:=dsBrowse;
            cxGrid1.SetFocus;
        end
      else if(panel3.Visible = false) then
           begin
              Close;
           end;
end;

procedure TfrmIzborStavki.aZacuvajGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true);
     zacuvajGridVoIni(cxGrid1DBTableView2,dmKon.aplikacija+Name+'2',true);
end;

procedure TfrmIzborStavki.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom:integer;
begin
      case key of
         VK_RETURN:begin
             if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               begin
                    pom:=dm.zemiCountStavkiPonuda(dm.CountStavkiPonuda, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblPoPonudaStavkiBROJ.Value, dm.tblPoPonudaStavkiGODINA.Value, dm.tblListaArtikliFiltriraniID.Value, dm.tblListaArtikliFiltriraniARTVID.Value, 'COUNTBR');
                    if pom = 1 then
                      ShowMessage('���� ������ � ������� �� �������� !!!')
                    else
                      begin
                           Panel6.Top:= 84;
                           Panel6.Left:=135;
                           Panel6.Visible:=true;

                           RadioButton4.Visible:=false;
                           SCena.SetFocus;
                           dm.tblPoPonudaStavki.Insert;
                           StateActive:=dsInsert;
                           //sDDV.EditValue:=dm.tblListaArtikliFiltriraniTARIFA.Value;
                           SValuta.EditValue:='DEN';
                           dm.tblPoPonudaStavkiCENA.Value:=0;
                           dm.tblPoPonudaStavkiCENADDV.Value:=0;
                           dm.tblPoPonudaStavkiRabat.Value:=0;
                           dm.tblPoPonudaStavkiKolicina.Value:=0;
                           dm.tblPoPonudaStavkiDDV.Value:=0;
                      end;

               end;
         end;
         VK_INSERT:begin
            frmArtikal:=TfrmArtikal.Create(Application);
            frmArtikal.Tag:=1;
            frmArtikal.ShowModal;
            frmArtikal.Free;
            dm.tblListaArtikliFiltrirani.fullRefresh;
            dm.tblListaArtikliFiltrirani.Locate('ID; ARTVID', VarArrayOf([dm.tblListaArtikliID.Value, dm.tblListaArtikliARTVID.Value]) , []);

         end;
      end;
end;

procedure TfrmIzborStavki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborStavki.cxGrid1DBTableView2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom:integer;
begin
     case Key of
      VK_RETURN:begin
             if cxGrid1DBTableView2.Controller.SelectedRecordCount = 1 then
               begin
                    pom:=dm.zemiCountStavkiPonuda(dm.CountStavkiPonuda, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblPoPonudaStavkiBROJ.Value, dm.tblPoPonudaStavkiGODINA.Value, dm.IzborStavkiOdNabavkaARTSIF.Value, dm.IzborStavkiOdNabavkaARTVID.Value, 'COUNTBR');
                    if pom = 1 then
                      ShowMessage('���� ������ � ������� �� �������� !!!')
                    else
                      begin
                           Panel3.Top:= 84;
                           Panel3.Left:=135;
                           Panel3.Visible:=true;
                           Panel3.Tag:=0;
                           RadioButton3.visible:=False;
                           NCena.SetFocus;
                           dm.tblPoPonudaStavki.Insert;
                           StateActive:=dsInsert;
                           //NDDV.EditValue:=dm.tblPoNabavkaStavkiDDV.Value;
                           NValuta.EditValue:='DEN';
                           dm.tblPoPonudaStavkiCENA.Value:=0;
                           dm.tblPoPonudaStavkiCENADDV.Value:=0;
                           dm.tblPoPonudaStavkiRabat.Value:=0;
                           dm.tblPoPonudaStavkiKolicina.Value:=0;
                           dm.tblPoPonudaStavkiDDV.Value:=0;
                      end;

               end;
         end;
      end;
end;

procedure TfrmIzborStavki.cxGrid1DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView2);
end;

procedure TfrmIzborStavki.cxGrid1DBTableView3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:integer;
begin
     case Key of
      VK_RETURN:begin
                 pom:=dm.zemiCountStavkiPonuda(dm.CountStavkiPonuda, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblPoPonudaStavkiBROJ.Value, dm.tblPoPonudaStavkiGODINA.Value, dm.DetailStavkiNabavkaSIFRAARTIKAL.Value, dm.DetailStavkiNabavkaVIDARTIKAL.Value, 'COUNTBR');
                    if pom = 1 then
                      ShowMessage('���� ������ � ������� �� �������� !!!')
                    else
                      begin
                           panel3.Tag:=1;
                           Panel3.Top:= 84;
                           Panel3.Left:=135;
                           Panel3.Visible:=true;
                           RadioButton3.visible:=False;
                           NCena.SetFocus;
                           dm.tblPoPonudaStavki.Insert;
                           StateActive:=dsInsert;
                           //NDDV.EditValue:=dm.tblPoNabavkaStavkiDDV.Value;
                           NValuta.EditValue:='DEN';
                           dm.tblPoPonudaStavkiCENA.Value:=0;
                           dm.tblPoPonudaStavkiCENADDV.Value:=0;
                           dm.tblPoPonudaStavkiRabat.Value:=0;
                           dm.tblPoPonudaStavkiKolicina.Value:=0;
                           dm.tblPoPonudaStavkiDDV.Value:=0;
                      end;

               end;
      end;
end;

procedure TfrmIzborStavki.cxGrid1DBTableView3KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView3);
end;

procedure TfrmIzborStavki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (StateActive in [dsInsert]) then
        dm.tblPoPonudaStavki.Cancel;
end;

procedure TfrmIzborStavki.FormCreate(Sender: TObject);
begin
       dm.IzborStavkiOdNabavka.Close;
       dm.DetailStavkiNabavka.Close;
       dm.tblListaArtikliFiltrirani.Close;
       dm.tblListaArtikliFiltrirani.Open;
       dm.IzborStavkiOdNabavka.ParamByName('nabavkaID').Value:=dm.tblPonudiNABAVKA_ID.Value;
       dm.IzborStavkiOdNabavka.Open;
       dm.DetailStavkiNabavka.ParamByName('nabavkaID').Value:= dm.tblPonudiNABAVKA_ID.Value;
       dm.DetailStavkiNabavka.open;
end;

procedure TfrmIzborStavki.FormShow(Sender: TObject);
begin
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true,true);
     procitajGridOdIni(cxGrid1DBTableView2,dmKon.aplikacija+Name+'2',true,true);
     if dm.tblPonudiBROJNABAVKA.Value = '' then
       RadioButton4.Enabled:=false
     else RadioButton4.Checked:=True;
       cxGrid1.SetFocus;
end;

procedure TfrmIzborStavki.NCenaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel3.Visible = true) and (NCena.Text <> '') and (NDDV.Text <> '') then
         begin
            dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (NDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
         end;
end;

procedure TfrmIzborStavki.NCenaSoDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel3.Visible = true) and (NCenaSoDDV.Text <> '') then
       begin
          dm.tblPoPonudaStavkiCENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value*100/(100 + NDDV.EditValue);
       end;
end;

procedure TfrmIzborStavki.NDDVPropertiesEditValueChanged(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel3.Visible = true) and (NCena.Text <> '') and (NDDV.Text <> '') then
        begin
           dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (NDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
        end;
end;

procedure TfrmIzborStavki.NKolicinaExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if NKolicina.Text <> '' then
        begin
           pom2:=StrToFloat(NKolicina.Text);
           NKolicina.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborStavki.NKursExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if NKurs.Text <> '' then
        begin
           pom2:=StrToFloat(NKurs.Text);
           NKurs.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborStavki.NRabatExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if NRabat.Text <> '' then
        begin
           pom2:=StrToFloat(NRabat.Text);
           NRabat.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborStavki.RadioButton3Click(Sender: TObject);
begin
         cxGrid1Level1.Active:=true;
         cxGrid1Level1.GridView:=cxGrid1DBTableView1;
         cxGrid1.SetFocus;
end;

procedure TfrmIzborStavki.RadioButton4Click(Sender: TObject);
begin
     if dm.tblPonudiNABAVKA_ID.Value <> Null then
       begin
            dm.IzborStavkiOdNabavka.ParamByName('NabavkaId').Value:= dm.tblPonudiNABAVKA_ID.Value;
            dm.IzborStavkiOdNabavka.FullRefresh;
            cxGrid1Level2.Active:=true;
            cxGrid1Level2.GridView:=cxGrid1DBTableView2;
            cxGrid1Level3.GridView:=cxGrid1DBTableView3;
            cxGrid1.SetFocus;
       end
end;

procedure TfrmIzborStavki.SCenaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel6.Visible = true) and (SCena.Text <> '') and (SDDV.Text <> '') then
         begin
            dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (SDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
         end;
end;

procedure TfrmIzborStavki.SCenaSoDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel6.Visible = true) and (SCenaSoDDV.Text <> '') then
       begin
          dm.tblPoPonudaStavkiCENA.Value:=dm.tblPoPonudaStavkiCENADDV.Value*100/(100 + SDDV.EditValue);
       end;
end;

procedure TfrmIzborStavki.SDDVPropertiesEditValueChanged(Sender: TObject);
begin
     if (StateActive in [dsInsert])and (Panel6.Visible = true) and (SCena.Text <> '') and (SDDV.Text <> '') then
        begin
           dm.tblPoPonudaStavkiCENADDV.Value:= dm.tblPoPonudaStavkiCENA.Value + (SDDV.EditValue*dm.tblPoPonudaStavkiCENA.Value/100);
        end;
end;

procedure TfrmIzborStavki.SKursExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if SKurs.Text <> '' then
        begin
           pom2:=StrToFloat(SKurs.Text);
           SKurs.Text:=FormatFloat('0.0000',pom2);
        end;
end;

procedure TfrmIzborStavki.�PomosExecute(Sender: TObject);
begin
     Application.HelpContext(103);
end;

procedure TfrmIzborStavki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;
  pom1, pom2, pom3, pom4, pom5, pom6, pom7:Integer;
begin
      case Key of
        VK_UP:
        begin
            if Sender = SValuta then SOpis.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
            if Sender = SOpis then Panel6.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if sender = SOpis then
             begin
                 if dm.Validacija(panel6) = false then
                    begin
                       pom1:=dm.tblPonudiGODINA.Value;
                       pom2:=dm.tblPonudiBROJ.Value;
                       dm.tblPoPonudaStavkiID.Value:=dm.zemiPonudaStavkaID(dm.ponudaStavkaID, 'BROJ', 'GODINA', Null, pom2, pom1, Null, 'MAXBR');
                       dm.tblPoPonudaStavkiBROJ.Value:=pom2;
                       dm.tblPoPonudaStavkiGODINA.Value:= pom1;
                       dm.tblPoPonudaStavkiARTVID.Value:=dm.tblListaArtikliFiltriraniARTVID.Value;
                       dm.tblPoPonudaStavkiARTSIF.Value:=dm.tblListaArtikliFiltriraniID.Value;
                       dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:=0;
                       dm.tblPoPonudaStavkiSTATUS.Value:=otvorena_pon_stavka;
                       dm.tblPoPonudaStavki.Post;

                       StateActive:=dsBrowse;
                       dm.tblPoPonudaStavki.Refresh;

                       pom4:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaStatus,'GODINA', 'BROJ', Null, Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, Null, Null, 'BR');
                       pom5:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaPrifatena,'GODINA', 'BROJ', 'PRIFATENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, prifatena_pon_stavka, Null, 'PRIFATENA');
                       pom6:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtfrlena,'GODINA', 'BROJ', 'OTFRLENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, neprifatena_pon_stavka, Null, 'OTFRLENA');
                       pom7:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtvorena,'GODINA', 'BROJ', 'OTVORENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, otvorena_pon_stavka, Null, 'OTVORENA');

                       if pom6 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=odbiena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if pom5 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=prifatena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if pom7 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=otvorena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if (pom7 <> 0) and (pom6 <> 0) and (pom5 = 0) then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=otvorena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end
                       else if (pom5 <> pom4) and (pom4 <> pom6)and (pom7 <> pom4)then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=delumnoprifatena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;

                       panel6.Visible:= false;
                       RadioButton4.Visible:=True;

                       dm.RestoreControls(panel6);

                       cxGrid1.SetFocus;
                   end;
             end
          else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
  end;
end;

procedure TfrmIzborStavki.EnterKakoTabPanel3(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;
  pom1, pom2, pom3, pom4, pom5, pom6, pom7:Integer;
begin
      case Key of
        VK_UP:
        begin
            if Sender = NValuta then NOpis.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
            if Sender = NOpis then Panel3.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if sender = NOpis then
             begin
                 if dm.Validacija(panel3) = false then
                   begin
                       pom1:=dm.tblPonudiGODINA.Value;
                       pom2:=dm.tblPonudiBROJ.Value;
                       dm.tblPoPonudaStavkiID.Value:=dm.zemiPonudaStavkaID(dm.ponudaStavkaID, 'BROJ', 'GODINA', Null, pom2, pom1, Null, 'MAXBR');
                       dm.tblPoPonudaStavkiBROJ.Value:=pom2;
                       dm.tblPoPonudaStavkiGODINA.Value:= pom1;
                       if panel3.Tag = 1 then
                          begin
                             dm.tblPoPonudaStavkiARTVID.Value:=dm.DetailStavkiNabavkaVIDARTIKAL.Value;
                             dm.tblPoPonudaStavkiARTSIF.Value:=dm.DetailStavkiNabavkaSIFRAARTIKAL.Value;
                          end
                       else
                          begin
                             dm.tblPoPonudaStavkiARTVID.Value:=dm.IzborStavkiOdNabavkaARTVID.Value;
                             dm.tblPoPonudaStavkiARTSIF.Value:=dm.IzborStavkiOdNabavkaARTSIF.Value;
                          end;

                       dm.tblPoPonudaStavkiKOLICINA.Value:=StrToFloat(NKolicina.Text);
                       dm.tblPoPonudaStavkiKOLICINA_PRIFATENA.Value:=0;

                       dm.tblPoPonudaStavkiSTATUS.Value:=otvorena_pon_stavka;
                       dm.tblPoPonudaStavki.Post;
                       StateActive:=dsBrowse;
                       dm.tblPoPonudaStavki.Refresh;

                       pom4:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaStatus,'GODINA', 'BROJ', Null, Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, Null, Null, 'BR');
                       pom5:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaPrifatena,'GODINA', 'BROJ', 'PRIFATENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, prifatena_pon_stavka, Null, 'PRIFATENA');
                       pom6:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtfrlena,'GODINA', 'BROJ', 'OTFRLENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, neprifatena_pon_stavka, Null, 'OTFRLENA');
                       pom7:=dm.zemiCountStavkiPonuda(dm.countPonudaStavkaOtvorena,'GODINA', 'BROJ', 'OTVORENA_PONUDA', Null, dm.tblPonudiGODINA.Value, dm.tblPonudiBROJ.Value, otvorena_pon_stavka, Null, 'OTVORENA');


                       if pom6 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=odbiena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if pom5 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=prifatena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if pom7 = pom4 then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=otvorena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;
                       if (pom7 <> 0) and (pom6 <> 0) and (pom5 = 0) then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=otvorena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end
                       else if (pom5 <> pom4) and (pom4 <> pom6)and (pom7 <> pom4)then
                         begin
                            dm.tblPonudi.Edit;
                            dm.tblPonudiSTATUS.Value:=delumnoprifatena_pon;
                            dm.tblPonudi.Post;
                            dm.tblPonudi.Refresh;
                         end;

                       panel3.Visible:= false;
                       dm.RestoreControls(panel3);
                       RadioButton3.visible:=True;
                       cxGrid1.SetFocus;
                   end;
             end
          else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;


  end;
end;
end.
