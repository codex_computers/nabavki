unit IzborNaArtikli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, StdCtrls, cxTextEdit, cxContainer,
  cxDBEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, ExtCtrls,
  ActnList, cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp, Menus,
  cxButtons;

type
  TfrmIzborArtikli = class(TForm)
    Panel5: TPanel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    aZacuvajIzgled: TAction;
    cxGrid3DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1CENA: TcxGridDBColumn;
    cxGrid3DBTableView1BRPAK: TcxGridDBColumn;
    cxGrid3DBTableView1KATALOG: TcxGridDBColumn;
    cxGrid3DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid3DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid3DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    aPomos: TAction;
    Panel6: TPanel;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Godina: TcxDBTextEdit;
    Broj: TcxDBTextEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    SKolicina: TcxDBTextEdit;
    Merka: TcxDBTextEdit;
    SCena: TcxDBTextEdit;
    SCenaSoDDV: TcxDBTextEdit;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    Artvid: TcxDBTextEdit;
    ArtSif: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    SDDV: TcxDBLookupComboBox;
    OtkaziButton: TcxButton;
    ButtonZapisi: TcxButton;
    aZapisi: TAction;
    aOtkazi: TAction;
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ButtonVnesiVoPlanClick(Sender: TObject);
    procedure ButtonOtkaziClick(Sender: TObject);
    procedure cxGrid3DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid3DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
    procedure SCenaExit(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure SDDVvExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SDDVvEnter(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
  private
    { Private declarations }
  public
  protected
    prva, posledna :TWinControl;
    { Public declarations }
  end;

var
  frmIzborArtikli: TfrmIzborArtikli;
  StateActive: TDataSetState;

implementation

uses dmKonekcija, dmMaticni, dmUnit,  Utils, Artikal;

{$R *.dfm}

procedure TfrmIzborArtikli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Panel5.Enabled:=False;
     dm.tblSpecifikacijaZaPlan.Cancel;
end;

procedure TfrmIzborArtikli.FormCreate(Sender: TObject);
begin
     dm.DDV.Open;
     dm.tblListaArtikliFiltrirani.close;
     dm.tblListaArtikliFiltrirani.open;
end;

procedure TfrmIzborArtikli.FormShow(Sender: TObject);
begin
     dm.PrvPosledenTab(panel6,posledna,prva);
     procitajGridOdIni(cxGrid3DBTableView1,dmKon.aplikacija+Name,true,true);
     if (Artvid.Text <> '') and (ArtSif.Text <> '') then
        begin
           dm.tblSpecifikacijaZaPlan.Insert;
           StateActive:=dsInsert;
           dm.tblSpecifikacijaZaPlanKOLICINA.Value:= 0;
           dm.tblSpecifikacijaZaPlanDDV.Value:=dm.zemiDDVStavka(dm.DDVStavka,'ARTVID','ARTSIF',Null,StrToInt(Artvid.Text),StrToInt(ArtSif.Text),Null, 'DDV');
           dm.tblSpecifikacijaZaPlanCena.Value:= dm.zemiCenaStavka(dm.CenaStavki,'ARTVID','ARTSIF',Null,StrToInt(Artvid.Text),StrToInt(ArtSif.Text),Null, 'CENA');
           if (SDDV.Text <> '') and (SCena.Text <> '')   then
              begin
                 dm.tblSpecifikacijaZaPlanCENADDV.Value:= dm.tblSpecifikacijaZaPlanCena.Value + (dm.tblSpecifikacijaZaPlanDDV.Value*dm.tblSpecifikacijaZaPlanCena.Value/100);
              end;
       end;
     Panel5.Enabled:=True;
end;

procedure TfrmIzborArtikli.SCenaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (SCena.Text <> '') and (SDDV.Text <> '')and (Panel6.Enabled = true) then
         begin
            dm.tblSpecifikacijaZaPlanCENADDV.Value:= dm.tblSpecifikacijaZaPlanCENA.Value + (SDDV.EditValue*dm.tblSpecifikacijaZaPlanCENA.Value/100);
         end;
end;

procedure TfrmIzborArtikli.SCenaSoDDVExit(Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (SCenaSoDDV.Text <> '')and (Panel6.Enabled = true) then
       begin
          dm.tblSpecifikacijaZaPlanCENA.Value:=dm.tblSpecifikacijaZaPlanCENADDV.Value*100/(100 + SDDV.EditValue);
       end;
end;

procedure TfrmIzborArtikli.SDDVvEnter(Sender: TObject);
begin
      TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmIzborArtikli.SDDVvExit(Sender: TObject);
begin
     if ((SKolicina.Text <> '') and (SCena.Text <> '')and (Panel6.Enabled = true) and (StateActive in [dsInsert])) then
         begin
            dm.tblSpecifikacijaZaPlanCENADDV.Value:=dm.tblSpecifikacijaZaPlanCENA.Value  + (SDDV.EditValue*dm.tblSpecifikacijaZaPlanCENA.Value/100);
         end;
end;

procedure TfrmIzborArtikli.aIzlezExecute(Sender: TObject);
begin
     if Panel6.Enabled = true then
        begin
            Panel6.Enabled:=False;
            Panel5.Enabled:=true;
            cxGrid3.SetFocus;
        end
     else
        begin
           dm.tblSpecifikacijaZaPlan.Cancel;
           close;
        end;
end;

procedure TfrmIzborArtikli.aOtkaziExecute(Sender: TObject);
begin
     Panel6.Enabled:=False;
     Panel5.Enabled:=true;
     cxGrid3.SetFocus;
end;

procedure TfrmIzborArtikli.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(118);
end;

procedure TfrmIzborArtikli.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid3DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmIzborArtikli.aZapisiExecute(Sender: TObject);
begin
     if (dm.Validacija(Panel6) = false) and (StateActive in [dsinsert]) then
         begin
              dm.tblSpecifikacijaZaPlanBROJ.Value:= StrToInt(Broj.Text);
              dm.tblSpecifikacijaZaPlanGODINA.Value:= StrToInt(Godina.Text);
              dm.tblSpecifikacijaZaPlanARTVID.Value:= StrToInt(Artvid.Text);
              dm.tblSpecifikacijaZaPlanARTSIF.Value:= StrToInt(ArtSif.Text);
              dm.tblSpecifikacijaZaPlanMERKAID.Value:=Merka.Text;

              dm.tblSpecifikacijaZaPlan.Post;
              dm.tblSpecifikacijaZaPlan.fullRefresh;
              dm.tblSpecifikacijaZaPlan.Locate('GODINA; BROJ; ARTSIF; ARTVID', VarArrayOf([StrToInt(Godina.Text), StrToInt(Broj.Text), StrToInt(ArtSif.Text), StrToInt(Artvid.Text)]) , []);

              dm.RestoreControls(Panel6);

              Panel6.Enabled:=False;
              Panel5.Enabled:=true;

              dm.tblSpecifikacijaZaPlan.Insert;
              StateActive:=dsInsert;

              cxgrid3.SetFocus;
         end;
end;

procedure TfrmIzborArtikli.ButtonOtkaziClick(Sender: TObject);
begin
     Panel6.Enabled:=False;
     Panel5.Enabled:=true;
     cxGrid3.SetFocus;
end;

procedure TfrmIzborArtikli.ButtonVnesiVoPlanClick(Sender: TObject);
begin
      if (dm.Validacija(Panel6) = false) and (StateActive in [dsinsert]) then
         begin
              dm.tblSpecifikacijaZaPlanBROJ.Value:= StrToInt(Broj.Text);
              dm.tblSpecifikacijaZaPlanGODINA.Value:= StrToInt(Godina.Text);
              dm.tblSpecifikacijaZaPlanARTVID.Value:= StrToInt(Artvid.Text);
              dm.tblSpecifikacijaZaPlanARTSIF.Value:= StrToInt(ArtSif.Text);
              dm.tblSpecifikacijaZaPlanMERKAID.Value:=Merka.Text;

              dm.tblSpecifikacijaZaPlan.Post;
              dm.tblSpecifikacijaZaPlan.fullRefresh;
              dm.tblSpecifikacijaZaPlan.Locate('GODINA; BROJ; ARTSIF; ARTVID', VarArrayOf([StrToInt(Godina.Text), StrToInt(Broj.Text), StrToInt(ArtSif.Text), StrToInt(Artvid.Text)]) , []);

              dm.RestoreControls(Panel6);

              Panel6.Enabled:=False;
              Panel5.Enabled:=true;

              dm.tblSpecifikacijaZaPlan.Insert;
              StateActive:=dsInsert;

              cxgrid3.SetFocus;
         end;
end;

procedure TfrmIzborArtikli.cxGrid3DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if (Artvid.Text <> '') and (ArtSif.Text <> '') and (StateActive in [dsInsert]) and (Panel5.Enabled = true) then
         begin
              dm.tblSpecifikacijaZaPlanKOLICINA.Value:=0;
              dm.tblSpecifikacijaZaPlanCena.Value:= dm.zemiCenaStavka(dm.CenaStavki,'ARTVID','ARTSIF',Null,StrToInt(Artvid.Text),StrToInt(ArtSif.Text),Null, 'CENA');
              dm.tblSpecifikacijaZaPlanDDV.Value:=dm.zemiCenaStavka(dm.DDVStavka, 'ARTVID','ARTSIF',Null,StrToInt(Artvid.Text),StrToInt(ArtSif.Text),Null, 'DDV');
              dm.tblSpecifikacijaZaPlanCENADDV.Value:= dm.zemiCenaStavka(dm.CenaDDVStavka,'ARTVID','ARTSIF',Null,StrToInt(Artvid.Text),StrToInt(ArtSif.Text),Null, 'CENADDV');
         end;
end;

procedure TfrmIzborArtikli.cxGrid3DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom :integer;
begin
     case Key of
            VK_RETURN:begin
               pom:=dm.zemiCountStavkiNalog(dm.CountStavkiPlan, 'BROJ', 'GODINA', 'ARTSIF', 'ARTVID', dm.tblPlanSektorBROJ.Value, dm.tblPlanSektorGODINA.Value, dm.tblListaArtikliFiltriraniID.Value, dm.tblListaArtikliFiltriraniARTVID.Value, 'COUNTBR');
               if pom <> 0 then
                  ShowMessage('���� ������ � ������� �� ������ !!!')
               else
                 begin
                    Panel6.Enabled:=True;
                    Panel5.Enabled:=False;
                    dm.tblSpecifikacijaZaPlanKOLICINA.Value:= '0';
                    SKolicina.SetFocus;
                 end;
            end;
            VK_INSERT:begin
             frmArtikal:=TfrmArtikal.Create(Application);
             frmArtikal.Tag:=1;
             frmArtikal.ShowModal;
             frmArtikal.Free;
             StateActive:=dsInsert;
             dm.tblListaArtikliFiltrirani.fullRefresh;
             dm.tblListaArtikliFiltrirani.Locate('ID; ARTVID', VarArrayOf([dm.tblListaArtikliID.Value, dm.tblListaArtikliARTVID.Value]) , []);
            end;
            VK_F7 :begin
                   dm.tblListaArtikliFiltrirani.FullRefresh;
            end;
        end;
end;

procedure TfrmIzborArtikli.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmIzborArtikli.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
          begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
           // Key := 0;
          end;
        VK_UP:
          begin
            if Sender = SKolicina then OtkaziButton.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
           // Key := 0;
          end;
        VK_RETURN:
          begin
           // Key := 0;
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
          end;
    end;
end;

end.
