unit NovaGenerika;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxDBEdit, ExtCtrls, cxGraphics, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, StdCtrls, ActnList,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp;

type
  TfrmNovaGenerika = class(TForm)
    Panel1: TPanel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    MerkaId: TcxDBTextEdit;
    Cena: TcxDBTextEdit;
    Label16: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Zacuvaj: TButton;
    Otkazi: TButton;
    DDV: TcxDBLookupComboBox;
    Merka: TcxDBLookupComboBox;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    procedure FormCreate(Sender: TObject);
    procedure ZacuvajClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OtkaziClick(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovaGenerika: TfrmNovaGenerika;

implementation

uses dmUnit, dmMaticni, Merka, DaNe, ArtGrupa;

{$R *.dfm}

procedure TfrmNovaGenerika.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//     dm.DDV.Close;
//     dmMat.tblMerka.Close;
      //dm.tblGenerika.Close;
//     dm.Podgrupa.Close;
end;

procedure TfrmNovaGenerika.FormCreate(Sender: TObject);
begin
      dm.DDV.Open;
//      dmMat.tblMerka.Open;
      //dm.tblGenerika.Open;
end;

procedure TfrmNovaGenerika.FormShow(Sender: TObject);
var pom:integer;
begin
    pom:= dm.zemiGenArtVid(dm.GenArtVid, Null, Null, Null, Null, Null, Null, 'GARTVID');
    dm.Podgrupa.ParamByName('artvidID').Value:= pom;
    dm.Podgrupa.Open;
      if tag = 1  then
        begin
           Sifra.Enabled:=False;
           dm.tblGenerika.Edit;
        end
      else
        begin
           dm.tblGenerika.Insert;
           dm.tblGenerikaID.Value:= dm.zemiGenID(dm.GenerikaID, 'ARTVID', Null, Null, pom, Null, Null, 'ID');
           Cena.Text:='0';
        end;
      Naziv.SetFocus;

end;

procedure TfrmNovaGenerika.OtkaziClick(Sender: TObject);
begin
     frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           dm.tblGenerika.Cancel;
           dm.RestoreControls(Panel1);
           Close;
        end;
end;

procedure TfrmNovaGenerika.ZacuvajClick(Sender: TObject);
var pom :integer;
begin
      if dm.Validacija(Panel1) = false then
          begin
             pom:= dm.zemiGenArtVid(dm.GenArtVid, Null, Null, Null, Null, Null, Null, 'GARTVID');
             dm.tblGenerikaARTVID.Value:= pom;
             dm.tblGenerika.Post;
             dm.RestoreControls(Panel1);
             dm.tblGenerika.FullRefresh;
             dm.tblListaArtikli.FullRefresh;
             Close;
          end;

end;
procedure TfrmNovaGenerika.aOtkaziIzlezExecute(Sender: TObject);
begin
     frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           dm.tblGenerika.Cancel;
           dm.RestoreControls(Panel1);
           Close;
        end;
end;

procedure TfrmNovaGenerika.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;

begin
      case Key of
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:begin
          if (sender = MerkaId) or ( Sender = Merka)then
             begin
               frmMerka:=TfrmMerka.Create(Application);
               frmMerka.Tag:=1;
               frmMerka.ShowModal;
               frmMerka.Free;
               dm.tblGenerikaMERKA.Value:=dmMat.tblMerkaID.Value;
               Cena.SetFocus;
             end;
        end;

  end;
end;
end.
