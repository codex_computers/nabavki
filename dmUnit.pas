unit dmUnit;

interface

uses
  SysUtils, Classes, Messages,DB,Dialogs, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  ImgList, Controls, cxGraphics, cxStyles, FIBQuery, pFIBQuery, pFIBStoredProc,
  DBClient, Variants, frxDesgn, frxClass, frxDBSet, frxRich, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit,  cxDBData, StdCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar,
  dxRibbonStatusBar, DBCtrls, ActnList, ActnMan, ToolWin, ActnCtrls, ComCtrls,
  cxPC, RibbonActnCtrls, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, cxDBEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,cxGridExportLink, cxExport,
  cxImageComboBox, cxLabel,SqlExpr, cxDBLabel,FMTBcd, cxCalendar,
  ActnMenus, RibbonActnMenus, cxMemo, Graphics, frxExportMail, frxExportImage,
  System.ImageList, cxImageList;


type
  Tdm = class(TDataModule)
    tblListaArtikli: TpFIBDataSet;
    dsListaArtikli: TDataSource;
    tblListaArtikliARTVID: TFIBIntegerField;
    tblListaArtikliID: TFIBIntegerField;
    tblListaArtikliNAZIV: TFIBStringField;
    tblListaArtikliMERKA: TFIBStringField;
    tblListaArtikliBRPAK: TFIBIntegerField;
    tblListaArtikliKATALOG: TFIBStringField;
    tblListaArtikliBARKOD: TFIBStringField;
    tblListaArtikliPROZVODITEL: TFIBIntegerField;
    tblListaArtikliGRUPA: TFIBStringField;
    tblListaArtikliKLASIFIKACIJA: TFIBStringField;
    tblListaArtikliUNINAZIV: TFIBStringField;
    tblListaArtikliUNIFONT: TFIBStringField;
    tblListaArtikliOBLIK: TFIBIntegerField;
    tblListaArtikliJACINA: TFIBFloatField;
    tblPlanSektor: TpFIBDataSet;
    dsPlanSektor: TDataSource;
    tblPlanSektorBROJ: TFIBIntegerField;
    tblPlanSektorRE: TFIBIntegerField;
    tblPlanSektorOPIS: TFIBStringField;
    tblPlanSektorDATUM_KREIRAN: TFIBDateField;
    tblPlanSektorKREIRAL: TFIBStringField;
    tblPlanSektorGODINA: TFIBSmallIntField;
    tblSpecifikacijaZaPlan: TpFIBDataSet;
    dsSpecifikacijaPlan: TDataSource;
    tblPlanSektorNAZIV: TFIBStringField;
    CenaStavki: TpFIBStoredProc;
    DDVStavka: TpFIBStoredProc;
    tblSpecifikacijaZaPlanBROJ: TFIBIntegerField;
    tblSpecifikacijaZaPlanGODINA: TFIBSmallIntField;
    tblSpecifikacijaZaPlanARTVID: TFIBIntegerField;
    tblSpecifikacijaZaPlanARTSIF: TFIBIntegerField;
    tblSpecifikacijaZaPlanDDV: TFIBFloatField;
    tblSpecifikacijaZaPlanCENADDV: TFIBBCDField;
    tblSpecifikacijaZaPlanNAZIV: TFIBStringField;
    tblSpecifikacijaZaPlanVREME: TFIBDateTimeField;
    tblSpecifikacijaZaPlanKREIRAL: TFIBStringField;
    tblSpecifikacijaZaPlanIIZNOS: TFIBBCDField;
    MaxBrPlan: TpFIBStoredProc;
    tblPlanSektorVREME: TFIBDateTimeField;
    tblPlanSektorDATUM_OD: TFIBDateField;
    tblPlanSektorDATUM_DO: TFIBDateField;
    tblNabavi: TpFIBDataSet;
    dsNabavki: TDataSource;
    tblNabaviID: TFIBIntegerField;
    tblNabaviRE: TFIBIntegerField;
    tblNabaviGODINA: TFIBSmallIntField;
    tblNabaviPREDMET: TFIBStringField;
    tblNabaviDATUM_KREIRANA: TFIBDateTimeField;
    tblNabaviDATUM_VAZENJE: TFIBDateField;
    tblNabaviDATUM_PONUDA_OD: TFIBDateField;
    tblNabaviDATUM_PONUDA_DO: TFIBDateField;
    tblNabaviTIP_NABAVKA: TFIBSmallIntField;
    tblNabaviOPIS: TFIBStringField;
    tblNabaviTIP_KONTAKT: TFIBIntegerField;
    tblNabaviKONTAKT: TFIBIntegerField;
    tblNabaviSTATUS: TFIBIntegerField;
    tblNabaviKREIRAL: TFIBStringField;
    tblNabaviIZNOS: TFIBBCDField;
    tblNabaviVALUTA: TFIBStringField;
    tblNabaviKURS: TFIBFloatField;
    tblNabaviPRIORITET: TFIBSmallIntField;
    tblNabaviUSLOVI: TFIBStringField;
    tblNabaviZABELESKA: TFIBStringField;
    tblNabaviVREME: TFIBDateTimeField;
    TipNabavki: TpFIBDataSet;
    dsTipNabavki: TDataSource;
    TipNabavkiID: TFIBIntegerField;
    TipNabavkiNAZIV: TFIBStringField;
    TipNabavkiOPIS: TFIBStringField;
    tblNabaviTIP_NABAVKA_NAZIV: TFIBStringField;
    tblNabaviPERIOD_VAZENJE: TFIBStringField;
    tblNabaviNAZIVSTATUS: TFIBStringField;
    tblNabaviREIDNAZIV: TFIBStringField;
    DDV: TpFIBDataSet;
    dsDDV: TDataSource;
    DDVTARIFA: TFIBFloatField;
    tblListaArtikliTARIFA: TFIBFloatField;
    tblSpecifikacijaZaPlanMERKAID: TFIBStringField;
    tblSpecifikacijaZaPlanMERKANAZIV: TFIBStringField;
    tblSpecifikacijaZaPlanARTVIDNAZIV: TFIBStringField;
    tblSpecifikacijaZaPlanPODGRUPAID: TFIBStringField;
    tblSpecifikacijaZaPlanPODGRUPANAZIV: TFIBStringField;
    Podgrupa: TpFIBDataSet;
    dsPodgrupa: TDataSource;
    PodgrupaID: TFIBStringField;
    PodgrupaNAZIV: TFIBStringField;
    PodgrupaMTR_ARTVID_ID: TFIBIntegerField;
    PodgrupaKOREN: TFIBStringField;
    PodgrupaPOTEKLO: TFIBStringField;
    tblListaArtikliARTVIDNAZIV: TFIBStringField;
    tblListaArtikliGRUPANAZIV: TFIBStringField;
    tblListaArtikliMERKANAZIV: TFIBStringField;
    tblListaArtikliPROIZVODITELNAZIV: TFIBStringField;
    tblNabaviPARTNERNAZIV: TFIBStringField;
    tblNabaviVALUTANAZIV: TFIBStringField;
    Prioritet: TpFIBDataSet;
    dsPrioritet: TDataSource;
    PrioritetID: TFIBIntegerField;
    PrioritetNAZIV: TFIBStringField;
    PrioritetVREME: TFIBIntegerField;
    tblNabaviPRIORITETNAZIV: TFIBStringField;
    StatusNabavka: TpFIBDataSet;
    dsStatusNabavka: TDataSource;
    MaxIDNabavka: TpFIBStoredProc;
    tblPoNabavkaStavki: TpFIBDataSet;
    dsPoNabavkaStavki: TDataSource;
    tblNabaviBROJ: TFIBIntegerField;
    MaxBrojNabavka: TpFIBStoredProc;
    tblNabaviRENAZIV: TFIBStringField;
    PartnerVraboten: TpFIBDataSet;
    dsPartner: TDataSource;
    tblPoPlanStavki: TpFIBDataSet;
    dsPoPlanStavki: TDataSource;
    MaxNabavkaStavkaID: TpFIBStoredProc;
    ViewListaArtikli: TpFIBDataSet;
    dsViewListaArtikli: TDataSource;
    tblPonudi: TpFIBDataSet;
    dsPonudi: TDataSource;
    tblPonudiNABAVKA_ID: TFIBIntegerField;
    tblPonudiTIP_PARTNER: TFIBIntegerField;
    tblPonudiPARTNER: TFIBIntegerField;
    tblPonudiDATUM: TFIBDateField;
    tblPonudiDATUM_VAZENJE: TFIBDateField;
    tblPonudiKONTAKT: TFIBIntegerField;
    tblPonudiZABELESKA: TFIBStringField;
    tblPonudiUSLOVI: TFIBStringField;
    tblPonudiSTATUS: TFIBIntegerField;
    tblPonudiNACIN_PLAKJANJE: TFIBIntegerField;
    tblPonudiKREIRAL: TFIBStringField;
    tblPonudiVREME: TFIBDateTimeField;
    tblPonudiNBROJ: TFIBIntegerField;
    tblPonudiNRE: TFIBIntegerField;
    tblPonudiNGODINA: TFIBSmallIntField;
    tblPonudiPNAZIV: TFIBStringField;
    tblPonudiKNAZIV: TFIBStringField;
    tblPonudiSNAZIV: TFIBStringField;
    tblPonudiPLAKANJENAZIV: TFIBStringField;
    tblPonudiBROJNABAVKA: TFIBStringField;
    tblNabaviBROJNABAVKA: TFIBStringField;
    MaxPonudaID: TpFIBStoredProc;
    Plakanje: TpFIBDataSet;
    dsPlakanje: TDataSource;
    PlakanjeID: TFIBIntegerField;
    PlakanjeNAZIV: TFIBStringField;
    StatusPonuda: TpFIBDataSet;
    dsStatusPonuda: TDataSource;
    StatusPonudaID: TFIBIntegerField;
    StatusPonudaGRUPA: TFIBIntegerField;
    StatusPonudaNAZIV: TFIBStringField;
    ViewListaArtikliARTVID: TFIBIntegerField;
    ViewListaArtikliID: TFIBIntegerField;
    ViewListaArtikliNAZIV: TFIBStringField;
    ViewListaArtikliMERKA: TFIBStringField;
    ViewListaArtikliCENA: TFIBBCDField;
    ViewListaArtikliBRPAK: TFIBIntegerField;
    ViewListaArtikliKATALOG: TFIBStringField;
    ViewListaArtikliBARKOD: TFIBStringField;
    ViewListaArtikliPROZVODITEL: TFIBIntegerField;
    ViewListaArtikliGRUPA: TFIBStringField;
    ViewListaArtikliKLASIFIKACIJA: TFIBStringField;
    ViewListaArtikliKOLICINA: TFIBBCDField;
    ViewListaArtikliUNINAZIV: TFIBStringField;
    ViewListaArtikliUNIFONT: TFIBStringField;
    ViewListaArtikliOBLIK: TFIBIntegerField;
    ViewListaArtikliJACINA: TFIBFloatField;
    ViewListaArtikliTARIFA: TFIBFloatField;
    ViewListaArtikliARTVIDNAZIV: TFIBStringField;
    ViewListaArtikliGRUPANAZIV: TFIBStringField;
    ViewListaArtikliMERKANAZIV: TFIBStringField;
    ViewListaArtikliPROIZVODITELNAZIV: TFIBStringField;
    tblPlanSektorBROJGODINA: TFIBStringField;
    PartnerVrabotenTIP_PARTNER: TFIBIntegerField;
    PartnerVrabotenID: TFIBIntegerField;
    PartnerVrabotenNAZIV: TFIBStringField;
    PartnerVrabotenADRESA: TFIBStringField;
    PartnerVrabotenTEL: TFIBStringField;
    PartnerVrabotenFAX: TFIBStringField;
    PartnerVrabotenDANOCEN: TFIBStringField;
    PartnerVrabotenMESTO: TFIBIntegerField;
    PartnerVrabotenIME: TFIBStringField;
    PartnerVrabotenPREZIME: TFIBStringField;
    PartnerVrabotenTATKO: TFIBStringField;
    PartnerVrabotenRE: TFIBIntegerField;
    PartnerVrabotenTIPPARTNER_NAZIV: TFIBStringField;
    PartnerVrabotenMESTONAZIV: TFIBStringField;
    PartnerVrabotenNAZIVSOMESTO: TFIBStringField;
    BrStavkiNabavka: TpFIBStoredProc;
    RabEdinica: TpFIBDataSet;
    dsRabEdinica: TDataSource;
    RabEdinicaID: TFIBIntegerField;
    RabEdinicaNAZIV: TFIBStringField;
    RabEdinicaTIP_PARTNER: TFIBIntegerField;
    RabEdinicaPARTNER: TFIBIntegerField;
    RabEdinicaKOREN: TFIBIntegerField;
    RabEdinicaSPISOK: TFIBStringField;
    RabEdinicaRE: TFIBIntegerField;
    RabEdinicaPOTEKLO: TFIBStringField;
    RabEdinicaRAKOVODITEL: TFIBStringField;
    RabEdinicaR: TFIBSmallIntField;
    RabEdinicaM: TFIBSmallIntField;
    RabEdinicaT: TFIBSmallIntField;
    tblNabaviPOTEKLO: TFIBStringField;
    PlanStavkiRabEd: TpFIBDataSet;
    dsPlanStavkiRabEd: TDataSource;
    tblPoPlanStavkiARTVID: TFIBIntegerField;
    tblPoPlanStavkiARTSIF: TFIBIntegerField;
    tblPoPlanStavkiDDV: TFIBFloatField;
    tblPoPlanStavkiNAZIV: TFIBStringField;
    tblPoPlanStavkiMERKAID: TFIBStringField;
    tblPoPlanStavkiMERKANAZIV: TFIBStringField;
    tblPoPlanStavkiARTVIDNAZIV: TFIBStringField;
    tblPoPlanStavkiPODGRUPAID: TFIBStringField;
    tblPoPlanStavkiPODGRUPANAZIV: TFIBStringField;
    PlanStavkiRabEdARTVID: TFIBIntegerField;
    PlanStavkiRabEdARTSIF: TFIBIntegerField;
    PlanStavkiRabEdDDV: TFIBFloatField;
    PlanStavkiRabEdNAZIV: TFIBStringField;
    PlanStavkiRabEdMERKAID: TFIBStringField;
    PlanStavkiRabEdMERKANAZIV: TFIBStringField;
    PlanStavkiRabEdARTVIDNAZIV: TFIBStringField;
    PlanStavkiRabEdPODGRUPAID: TFIBStringField;
    PlanStavkiRabEdPODGRUPANAZIV: TFIBStringField;
    KontaktPartner: TpFIBDataSet;
    dsKontaktPartner: TDataSource;
    KontaktPartnerID: TFIBIntegerField;
    KontaktPartnerTIP_PARTNER: TFIBIntegerField;
    KontaktPartnerPARTNER: TFIBIntegerField;
    KontaktPartnerNAZIV: TFIBStringField;
    KontaktPartnerMAIL: TFIBStringField;
    KontaktPartnerMOBILEN: TFIBStringField;
    KontaktPartnerTEL: TFIBStringField;
    KontaktPartnerOPIS: TFIBStringField;
    tblPonudiGODINA: TFIBIntegerField;
    tblPonudiBROJ: TFIBIntegerField;
    tblPonudiPONUDABROJ: TFIBStringField;
    tblPoPonudaStavki: TpFIBDataSet;
    dsPoPonudaStavki: TDataSource;
    tblPoPonudaStavkiID: TFIBIntegerField;
    tblPoPonudaStavkiBROJ: TFIBIntegerField;
    tblPoPonudaStavkiGODINA: TFIBIntegerField;
    tblPoPonudaStavkiARTVID: TFIBIntegerField;
    tblPoPonudaStavkiARTSIF: TFIBIntegerField;
    tblPoPonudaStavkiSTATUS: TFIBIntegerField;
    tblPoPonudaStavkiOPIS: TFIBStringField;
    tblPoPonudaStavkiCENA: TFIBBCDField;
    tblPoPonudaStavkiDDV: TFIBFloatField;
    tblPoPonudaStavkiCENADDV: TFIBBCDField;
    tblPoPonudaStavkiVALUTA: TFIBStringField;
    tblPoPonudaStavkiKURS: TFIBFloatField;
    tblPoPonudaStavkiKREIRAL: TFIBStringField;
    tblPoPonudaStavkiVREME: TFIBDateTimeField;
    tblPoPonudaStavkiARTNAZIV: TFIBStringField;
    tblPoPonudaStavkiMERKA: TFIBStringField;
    tblPoPonudaStavkiARTGRUPA: TFIBStringField;
    tblPoPonudaStavkiARTVIDNAZIV: TFIBStringField;
    tblPoPonudaStavkiARTGRUPANAZIV: TFIBStringField;
    tblPoPonudaStavkiMERKANAZIV: TFIBStringField;
    tblPoPonudaStavkiVALUTANAZIV: TFIBStringField;
    tblPoPonudaStavkiSTATUSNAZIV: TFIBStringField;
    CountPartner: TpFIBStoredProc;
    ponudaStavkaID: TpFIBStoredProc;
    CountStavkiPonuda: TpFIBStoredProc;
    tblPlanSektorRABEDIDNAZIV: TFIBStringField;
    tblNalog: TpFIBDataSet;
    dsNalog: TDataSource;
    PoPonudaStavkIStatus: TpFIBDataSet;
    dsPoPonudaStavkiStatus: TDataSource;
    PoPonudaStavkIStatusID: TFIBIntegerField;
    PoPonudaStavkIStatusGRUPA: TFIBIntegerField;
    PoPonudaStavkIStatusNAZIV: TFIBStringField;
    NalogStatus: TpFIBDataSet;
    dsNalogStatus: TDataSource;
    NalogStatusID: TFIBIntegerField;
    NalogStatusGRUPA: TFIBIntegerField;
    NalogStatusNAZIV: TFIBStringField;
    Prevoz: TpFIBDataSet;
    dsPrevoz: TDataSource;
    PrevozID: TFIBSmallIntField;
    PrevozNAZIV: TFIBStringField;
    MaxNalogID: TpFIBStoredProc;
    tblPoNalogStavki: TpFIBDataSet;
    dsPoNalogStavki: TDataSource;
    CountStavkiNalog: TpFIBStoredProc;
    NalogStavkiID: TpFIBStoredProc;
    StatusNabavkaID: TFIBIntegerField;
    StatusNabavkaGRUPA: TFIBIntegerField;
    StatusNabavkaNAZIV: TFIBStringField;
    IzborStavkiOdNabavka: TpFIBDataSet;
    dsIzborStavkiOdNabavka: TDataSource;
    IzborStavkiOdNabavkaARTVID: TFIBIntegerField;
    IzborStavkiOdNabavkaARTSIF: TFIBIntegerField;
    IzborStavkiOdNabavkaSUMAKOLICINA: TFIBFloatField;
    IzborStavkiOdNabavkaSUMACENA: TFIBBCDField;
    IzborStavkiOdNabavkaDDV: TFIBFloatField;
    IzborStavkiOdNabavkaSUMACENADDV: TFIBBCDField;
    IzborStavkiOdNabavkaNAZIVARTIKAL: TFIBStringField;
    IzborStavkiOdNabavkaARTVIDNAZIV: TFIBStringField;
    IzborStavkiOdNabavkaGRUPAIDNAZIV: TFIBStringField;
    IzborStavkiOdNabavkaNABAVKA_ID: TFIBIntegerField;
    IzborStavkiOdNabavkaMERKANAZIV: TFIBStringField;
    IzborStavkiOdNabavkaMERKA: TFIBStringField;
    MaxIDTipNabavka: TpFIBStoredProc;
    countPonudaStavkaPrifatena: TpFIBStoredProc;
    countPonudaStavkaStatus: TpFIBStoredProc;
    countPonudaStavkaOtfrlena: TpFIBStoredProc;
    countPonudaStavkaOtvorena: TpFIBStoredProc;
    MaxVidPrevozID: TpFIBStoredProc;
    SumaPrifatenaKolicina: TpFIBStoredProc;
    tblKontakt: TpFIBDataSet;
    dsKontakt: TDataSource;
    tblKontaktID: TFIBIntegerField;
    tblKontaktTIP_PARTNER: TFIBIntegerField;
    tblKontaktPARTNER: TFIBIntegerField;
    tblKontaktNAZIV: TFIBStringField;
    tblKontaktMAIL: TFIBStringField;
    tblKontaktMOBILEN: TFIBStringField;
    tblKontaktTEL: TFIBStringField;
    tblKontaktOPIS: TFIBStringField;
    tblKontaktPARTNERNAZIV: TFIBStringField;
    tblPartner: TpFIBDataSet;
    dsTblPartner: TDataSource;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerADRESA: TFIBStringField;
    tblPartnerTEL: TFIBStringField;
    tblPartnerFAX: TFIBStringField;
    tblPartnerDANOCEN: TFIBStringField;
    tblPartnerMESTO: TFIBIntegerField;
    tblPartnerIME: TFIBStringField;
    tblPartnerPREZIME: TFIBStringField;
    tblPartnerTATKO: TFIBStringField;
    tblPartnerLOGO: TFIBBlobField;
    tblPartnerRE: TFIBIntegerField;
    tblPartnerLOGOTEXT: TFIBBlobField;
    tblPartnerSTATUS: TFIBIntegerField;
    tblPartnerLIMIT: TFIBBCDField;
    tblPartnerTIP_PARTNER_NAZIV: TFIBStringField;
    tblPartnerMESTONAZIV: TFIBStringField;
    tblPartnerRAB_EDIN_NAZIV: TFIBStringField;
    frxDBPlan1: TfrxDBDataset;
    frxDesignerPlan: TfrxDesigner;
    tblPlanZaSiteSektori: TpFIBDataSet;
    dsPlanZaSiteSektori: TDataSource;
    frxReport: TfrxReport;
    tblReportDizajn: TpFIBDataSet;
    dsReportDizajn: TDataSource;
    tblReportDizajnID: TFIBIntegerField;
    tblReportDizajnKOREN: TFIBIntegerField;
    tblReportDizajnNASLOV: TFIBStringField;
    tblReportDizajnSQL: TFIBBlobField;
    tblReportDizajnDATA: TFIBBlobField;
    tblReportDizajnXML: TFIBBlobField;
    tblReportDizajnUSLOVI: TFIBBlobField;
    tblReportDizajnNASLOVI: TFIBBlobField;
    tblReportDizajnVIDLIVOST: TFIBStringField;
    tblReportDizajnPOJAVIUSLOVI: TFIBIntegerField;
    tblReportDizajnUSLOVITEXT: TFIBBlobField;
    tblReportDizajnDINAMICKIUSLOVI: TFIBBlobField;
    tblReportDizajnTABELA_GRUPA: TFIBStringField;
    tblReportDizajnBR: TFIBIntegerField;
    tblReportDizajnREDOSLED: TFIBIntegerField;
    tblPlanZaIzbraniotSektor: TpFIBDataSet;
    dsPlanZaIzbraniotSektor: TDataSource;
    frxDBPlan2: TfrxDBDataset;
    frxDBFirma: TfrxDBDataset;
    Firmi: TpFIBDataSet;
    dsFirmi: TDataSource;
    tblStavkiZaOdbranPlan: TpFIBDataSet;
    dsStavkiZaOdbranPlan: TDataSource;
    frxDBPlan3: TfrxDBDataset;
    frxDBNabavka1: TfrxDBDataset;
    dsStavkiZaOdbranaPonuda: TDataSource;
    tblStavkiZaOdbranaPonuda: TpFIBDataSet;
    frxDBPonuda1: TfrxDBDataset;
    tblStavkiZaOdbranaPonudaID: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaBROJ: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaGODINA: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaARTVID: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaARTSIF: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaKOLICINA: TFIBBCDField;
    tblStavkiZaOdbranaPonudaSTATUS: TFIBIntegerField;
    tblStavkiZaOdbranaPonudaKOLICINA_PRIFATENA: TFIBFloatField;
    tblStavkiZaOdbranaPonudaOPIS: TFIBStringField;
    tblStavkiZaOdbranaPonudaCENA: TFIBBCDField;
    tblStavkiZaOdbranaPonudaDDV: TFIBFloatField;
    tblStavkiZaOdbranaPonudaCENADDV: TFIBBCDField;
    tblStavkiZaOdbranaPonudaRABAT: TFIBFloatField;
    tblStavkiZaOdbranaPonudaVALUTA: TFIBStringField;
    tblStavkiZaOdbranaPonudaKURS: TFIBFloatField;
    tblStavkiZaOdbranaPonudaARTNAZIV: TFIBStringField;
    tblStavkiZaOdbranaPonudaMERKA: TFIBStringField;
    tblStavkiZaOdbranaPonudaARTGRUPA: TFIBStringField;
    tblStavkiZaOdbranaPonudaARTVIDNAZIV: TFIBStringField;
    tblStavkiZaOdbranaPonudaARTGRUPANAZIV: TFIBStringField;
    tblStavkiZaOdbranaPonudaVALUTANAZIV: TFIBStringField;
    tblStavkiZaOdbranaPonudaKRAJNA_CENA: TFIBFloatField;
    tblStavkiZaOdbranaPonudaIIZNOS: TFIBBCDField;
    tblStavkiZaOdbranaPonudaSTATUSNAZIV: TFIBStringField;
    tblStavkiZaOdbranaPonudaPONUDABROJ: TFIBStringField;
    tblSpecifikacijaNaNabavka: TpFIBDataSet;
    dsSpecifikacijaZaNabavka: TDataSource;
    tblSpecifikacijaNaNabavkaNABAVKA_ID: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaID: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaARTVID: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaARTSIF: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaKOLICINA: TFIBBCDField;
    tblSpecifikacijaNaNabavkaRE: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaVREME: TFIBDateTimeField;
    tblSpecifikacijaNaNabavkaKREIRAL: TFIBStringField;
    tblSpecifikacijaNaNabavkaCENA: TFIBBCDField;
    tblSpecifikacijaNaNabavkaDDV: TFIBFloatField;
    tblSpecifikacijaNaNabavkaCENADDV: TFIBBCDField;
    tblSpecifikacijaNaNabavkaBROJNABAVKA: TFIBIntegerField;
    tblSpecifikacijaNaNabavkaNAZIVRE: TFIBStringField;
    tblSpecifikacijaNaNabavkaNAZIVARTIKAL: TFIBStringField;
    tblSpecifikacijaNaNabavkaARTVIDNAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaGRUPANAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaARTVIDIDNAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaGRUPAIDNAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaRABEDINIDNAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaIIZNOS: TFIBBCDField;
    tblSpecifikacijaNaNabavkaPOTEKLO: TFIBStringField;
    tblSpecifikacijaNaNabavkaBROJNANABAVKA: TFIBStringField;
    tblSpecifikacijaNaNabavkaMERKANAZIV: TFIBStringField;
    tblSpecifikacijaNaNabavkaMERKA: TFIBStringField;
    tblSpecifikacijaNaNabavkaGRUPA: TFIBStringField;
    frxDBNabavka2: TfrxDBDataset;
    tblPoNabavkaStavkiNABAVKA_ID: TFIBIntegerField;
    tblPoNabavkaStavkiID: TFIBIntegerField;
    tblPoNabavkaStavkiARTVID: TFIBIntegerField;
    tblPoNabavkaStavkiARTSIF: TFIBIntegerField;
    tblPoNabavkaStavkiRE: TFIBIntegerField;
    tblPoNabavkaStavkiVREME: TFIBDateTimeField;
    tblPoNabavkaStavkiKREIRAL: TFIBStringField;
    tblPoNabavkaStavkiDDV: TFIBFloatField;
    tblPoNabavkaStavkiCENADDV: TFIBBCDField;
    tblPoNabavkaStavkiBROJNABAVKA: TFIBIntegerField;
    tblPoNabavkaStavkiNAZIVRE: TFIBStringField;
    tblPoNabavkaStavkiNAZIVARTIKAL: TFIBStringField;
    tblPoNabavkaStavkiARTVIDNAZIV: TFIBStringField;
    tblPoNabavkaStavkiGRUPANAZIV: TFIBStringField;
    tblPoNabavkaStavkiARTVIDIDNAZIV: TFIBStringField;
    tblPoNabavkaStavkiGRUPAIDNAZIV: TFIBStringField;
    tblPoNabavkaStavkiRABEDINIDNAZIV: TFIBStringField;
    tblPoNabavkaStavkiIIZNOS: TFIBBCDField;
    tblPoNabavkaStavkiPOTEKLO: TFIBStringField;
    tblPoNabavkaStavkiBROJNANABAVKA: TFIBStringField;
    tblPoNabavkaStavkiMERKANAZIV: TFIBStringField;
    tblPoNabavkaStavkiMERKA: TFIBStringField;
    frxDBPonuda2: TfrxDBDataset;
    frxDBNAlog1: TfrxDBDataset;
    tblStavkiZaOdbranNalog: TpFIBDataSet;
    dsStavkiZaOdbranNalog: TDataSource;
    tblStavkiZaOdbranNalogBROJ: TFIBIntegerField;
    tblStavkiZaOdbranNalogGODINA: TFIBSmallIntField;
    tblStavkiZaOdbranNalogID: TFIBIntegerField;
    tblStavkiZaOdbranNalogARTVID: TFIBIntegerField;
    tblStavkiZaOdbranNalogARTSIF: TFIBIntegerField;
    tblStavkiZaOdbranNalogKOLICINA: TFIBFloatField;
    tblStavkiZaOdbranNalogCENA: TFIBBCDField;
    tblStavkiZaOdbranNalogDDV: TFIBFloatField;
    tblStavkiZaOdbranNalogCENADDV: TFIBBCDField;
    tblStavkiZaOdbranNalogOPIS: TFIBStringField;
    tblStavkiZaOdbranNalogKREIRAL: TFIBStringField;
    tblStavkiZaOdbranNalogVREME: TFIBDateTimeField;
    tblStavkiZaOdbranNalogNAZIVARTIKAL: TFIBStringField;
    tblStavkiZaOdbranNalogARTVIDNAZIV: TFIBStringField;
    tblStavkiZaOdbranNalogGRUPANAZIV: TFIBStringField;
    tblStavkiZaOdbranNalogGRUPA: TFIBStringField;
    tblStavkiZaOdbranNalogMERKA: TFIBStringField;
    tblStavkiZaOdbranNalogMERKANAZIV: TFIBStringField;
    tblStavkiZaOdbranNalogIIZNOS: TFIBBCDField;
    frxDBNalog2: TfrxDBDataset;
    frxRichObject1: TfrxRichObject;
    FirmiID: TFIBIntegerField;
    FirmiNAZIV: TFIBStringField;
    FirmiTIP_PARTNER: TFIBIntegerField;
    FirmiPARTNER: TFIBIntegerField;
    FirmiKOREN: TFIBIntegerField;
    FirmiSPISOK: TFIBStringField;
    FirmiPOTEKLO: TFIBStringField;
    FirmiRE: TFIBIntegerField;
    FirmiLOGO: TFIBBlobField;
    FirmiPARTNERNAZIV: TFIBStringField;
    FirmiMESTONAZIV: TFIBStringField;
    FirmiADRESA: TFIBStringField;
    FirmiTEL: TFIBStringField;
    FirmiFAX: TFIBStringField;
    FirmiDANOCEN: TFIBStringField;
    FirmiLOGOTEXT: TFIBBlobField;
    FirmiADRESAMESTO: TFIBStringField;
    tblGenerika: TpFIBDataSet;
    dsGenerika: TDataSource;
    tblGenerikaARTVID: TFIBIntegerField;
    tblGenerikaID: TFIBIntegerField;
    tblGenerikaNAZIV: TFIBStringField;
    tblGenerikaMERKA: TFIBStringField;
    tblGenerikaCENA: TFIBBCDField;
    tblGenerikaKOLICINA: TFIBBCDField;
    tblGenerikaTARIFA: TFIBFloatField;
    GenArtVid: TpFIBStoredProc;
    GenerikaID: TpFIBStoredProc;
    tblMapiranje: TpFIBDataSet;
    dsMapiranje: TDataSource;
    tblMapiranjeVID_GENERIKA: TFIBIntegerField;
    tblMapiranjeGENERIKA: TFIBIntegerField;
    tblMapiranjeOPST_NAZIV: TFIBStringField;
    tblMapiranjeVID_ARTIKAL: TFIBIntegerField;
    tblMapiranjeARTIKAL: TFIBIntegerField;
    tblMapiranjeNAZIV: TFIBStringField;
    tblGenerikaGRUPA: TFIBStringField;
    tblGenerikaPODGRUPANAZIV: TFIBStringField;
    DetailStavkiNabavka: TpFIBDataSet;
    dsDetailStavkiNabavka: TDataSource;
    DetailStavkiNabavkaVIDARTIKAL_GEN: TFIBIntegerField;
    DetailStavkiNabavkaSIFRAARTIKAL_GEN: TFIBIntegerField;
    DetailStavkiNabavkaOPSTNAZIVARTIKAL: TFIBStringField;
    DetailStavkiNabavkaVIDARTIKAL: TFIBIntegerField;
    DetailStavkiNabavkaSIFRAARTIKAL: TFIBIntegerField;
    DetailStavkiNabavkaNAZIV: TFIBStringField;
    PromenaStatusStavka: TpFIBStoredProc;
    CenaDDVStavka: TpFIBStoredProc;
    tblSpecifikacijaZaPlanKOLICINA: TFIBBCDField;
    tblSpecifikacijaZaPlanCENA: TFIBBCDField;
    tblPoPlanStavkiKOLICINA: TFIBBCDField;
    PlanStavkiRabEdKOLICINA: TFIBBCDField;
    tblPoNabavkaStavkiCENA: TFIBBCDField;
    tblPoNabavkaStavkiKOLICINA: TFIBBCDField;
    tblNabaviIZNOSCENA: TFIBFloatField;
    tblNabaviIZNOS_DENARI: TFIBBCDField;
    tblPoNalogStavkiBROJ: TFIBIntegerField;
    tblPoNalogStavkiGODINA: TFIBSmallIntField;
    tblPoNalogStavkiID: TFIBIntegerField;
    tblPoNalogStavkiARTVID: TFIBIntegerField;
    tblPoNalogStavkiARTSIF: TFIBIntegerField;
    tblPoNalogStavkiKOLICINA: TFIBBCDField;
    tblPoNalogStavkiCENA: TFIBBCDField;
    tblPoNalogStavkiDDV: TFIBFloatField;
    tblPoNalogStavkiCENADDV: TFIBBCDField;
    tblPoNalogStavkiOPIS: TFIBStringField;
    tblPoNalogStavkiKREIRAL: TFIBStringField;
    tblPoNalogStavkiVREME: TFIBDateTimeField;
    tblPoNalogStavkiNAZIVARTIKAL: TFIBStringField;
    tblPoNalogStavkiARTVIDNAZIV: TFIBStringField;
    tblPoNalogStavkiGRUPANAZIV: TFIBStringField;
    tblPoNalogStavkiGRUPA: TFIBStringField;
    tblPoNalogStavkiMERKA: TFIBStringField;
    tblPoNalogStavkiMERKANAZIV: TFIBStringField;
    tblPoNalogStavkiIIZNOS: TFIBBCDField;
    tblNalogBROJ: TFIBIntegerField;
    tblNalogGODINA: TFIBSmallIntField;
    tblNalogPONUDA_BROJ: TFIBIntegerField;
    tblNalogPONUDA_GODINA: TFIBIntegerField;
    tblNalogNABAVKA_ID: TFIBIntegerField;
    tblNalogDATUM: TFIBDateField;
    tblNalogDATUM_ISPORAKA: TFIBDateField;
    tblNalogSTATUS: TFIBIntegerField;
    tblNalogKREIRAL: TFIBStringField;
    tblNalogTIP_KONTAKT: TFIBIntegerField;
    tblNalogKONTAKT: TFIBIntegerField;
    tblNalogPRIMAC: TFIBStringField;
    tblNalogPRIMAC_ADRESA: TFIBStringField;
    tblNalogPRIMAC_KONTAKT: TFIBStringField;
    tblNalogTIP_PLAKJANJE: TFIBSmallIntField;
    tblNalogVID_PREVOZ: TFIBSmallIntField;
    tblNalogPREDMET_NABAVKA: TFIBStringField;
    tblNalogZABELESKA: TFIBStringField;
    tblNalogTEZINA: TFIBStringField;
    tblNalogVREME: TFIBDateTimeField;
    tblNalogBROJNAPONUDA: TFIBStringField;
    tblNalogBROJNANABAVKA: TFIBStringField;
    tblNalogSTATUSNAZIV: TFIBStringField;
    tblNalogKONTAKTNAZIV: TFIBStringField;
    tblNalogPLAKANJENAZIV: TFIBStringField;
    tblNalogPREVOZNAZIV: TFIBStringField;
    tblNalogBROJNANALOG: TFIBStringField;
    tblNalogTIP_NABAVKANAZIV: TFIBStringField;
    tblNalogTIP_PARTNER: TFIBIntegerField;
    tblNalogPARTNER: TFIBIntegerField;
    tblNalogPARTNERNAZIV: TFIBStringField;
    tblNalogPARTNERADRESA: TFIBStringField;
    tblNalogPARTNERMESTO: TFIBStringField;
    tblListaArtikliCENA: TFIBBCDField;
    tblListaArtikliKOLICINA: TFIBBCDField;
    tblPoPonudaStavkiKOLICINA: TFIBBCDField;
    tblPoPonudaStavkiKOLICINA_PRIFATENA: TFIBBCDField;
    tblPoPonudaStavkiRABAT: TFIBBCDField;
    GrupaArtikal: TpFIBDataSet;
    dsGrupaArtikal: TDataSource;
    GrupaArtikalID: TFIBIntegerField;
    GrupaArtikalOPIS: TFIBStringField;
    IzbraniSektoriZaPlan: TpFIBDataSet;
    dsIzbraniSektoriZaPlan: TDataSource;
    IzbraniSektoriZaPlanID: TFIBIntegerField;
    IzbraniSektoriZaPlanNAZIV: TFIBStringField;
    SetupPlanSektor: TpFIBDataSet;
    dsSetupPlanSektor: TDataSource;
    SetupPlanSektorP1: TFIBStringField;
    SetupPlanSektorP2: TFIBStringField;
    SetupPlanSektorV1: TFIBStringField;
    SetupGrupaArtikalPlan: TpFIBDataSet;
    cxLargeImages: TcxImageList;
    cxSmallImages: TcxImageList;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    dsSetupGrupaArtikalPlan: TDataSource;
    SetupGrupaArtikalPlanP1: TFIBStringField;
    SetupGrupaArtikalPlanP2: TFIBStringField;
    SetupGrupaArtikalPlanV1: TFIBStringField;
    IzbraniGrupiZaPlan: TpFIBDataSet;
    IzbraniGrupiZaPlanID: TFIBIntegerField;
    IzbraniGrupiZaPlanOPIS: TFIBStringField;
    dsIzbraniGrupiZaPlan: TDataSource;
    tblListaArtikliFiltrirani: TpFIBDataSet;
    dstblListaArtikliFiltrirani: TDataSource;
    tblListaArtikliFiltriraniARTVID: TFIBIntegerField;
    tblListaArtikliFiltriraniID: TFIBIntegerField;
    tblListaArtikliFiltriraniNAZIV: TFIBStringField;
    tblListaArtikliFiltriraniMERKA: TFIBStringField;
    tblListaArtikliFiltriraniCENA: TFIBBCDField;
    tblListaArtikliFiltriraniBRPAK: TFIBIntegerField;
    tblListaArtikliFiltriraniKATALOG: TFIBStringField;
    tblListaArtikliFiltriraniBARKOD: TFIBStringField;
    tblListaArtikliFiltriraniPROZVODITEL: TFIBIntegerField;
    tblListaArtikliFiltriraniGRUPA: TFIBStringField;
    tblListaArtikliFiltriraniKLASIFIKACIJA: TFIBStringField;
    tblListaArtikliFiltriraniKOLICINA: TFIBBCDField;
    tblListaArtikliFiltriraniUNINAZIV: TFIBStringField;
    tblListaArtikliFiltriraniUNIFONT: TFIBStringField;
    tblListaArtikliFiltriraniOBLIK: TFIBIntegerField;
    tblListaArtikliFiltriraniJACINA: TFIBFloatField;
    tblListaArtikliFiltriraniTARIFA: TFIBFloatField;
    tblListaArtikliFiltriraniARTVIDNAZIV: TFIBStringField;
    tblListaArtikliFiltriraniGRUPANAZIV: TFIBStringField;
    tblListaArtikliFiltriraniMERKANAZIV: TFIBStringField;
    tblListaArtikliFiltriraniPROIZVODITELNAZIV: TFIBStringField;
    tblPoNabavkaStavkiOPIS: TFIBStringField;
    tblPonudeniArtikli: TpFIBDataSet;
    dsPonudeniArtikli: TDataSource;
    tblPonudeniArtikliARTVID: TFIBIntegerField;
    tblPonudeniArtikliARTSIF: TFIBIntegerField;
    tblPonudeniArtikliNAZIV: TFIBStringField;
    frxDBListaPonudiArtikli: TfrxDBDataset;
    ListaNaponudiZaArtikli: TpFIBDataSet;
    dsListaNaponudiZaArtikli: TDataSource;
    ListaNaponudiZaArtikliDATUM: TFIBDateField;
    ListaNaponudiZaArtikliBROJ: TFIBIntegerField;
    ListaNaponudiZaArtikliGODINA: TFIBIntegerField;
    ListaNaponudiZaArtikliBROJ1: TFIBIntegerField;
    ListaNaponudiZaArtikliGODINA1: TFIBIntegerField;
    ListaNaponudiZaArtikliARTVID: TFIBIntegerField;
    ListaNaponudiZaArtikliARTSIF: TFIBIntegerField;
    ListaNaponudiZaArtikliTIP_PARTNER: TFIBIntegerField;
    ListaNaponudiZaArtikliPARTNER: TFIBIntegerField;
    ListaNaponudiZaArtikliNAZIV: TFIBStringField;
    ListaNaponudiZaArtikliNAZIV_ARTIKAL: TFIBStringField;
    ListaNaponudiZaArtikliARTVID_NAZIV: TFIBStringField;
    ListaNaponudiZaArtikliKRAJNA_CENA: TFIBFloatField;
    tblStavkiZaOdbranPlanGODINA: TFIBSmallIntField;
    tblStavkiZaOdbranPlanGRUPA: TFIBStringField;
    tblStavkiZaOdbranPlanARTSIF: TFIBIntegerField;
    tblStavkiZaOdbranPlanNAZIV: TFIBStringField;
    tblStavkiZaOdbranPlanPODGRUPA: TFIBStringField;
    tblStavkiZaOdbranPlanMERKA: TFIBStringField;
    tblStavkiZaOdbranPlanSUMAKOLICINA: TFIBBCDField;
    tblStavkiZaOdbranPlanSUMACENA: TFIBBCDField;
    tblStavkiZaOdbranPlanDDV: TFIBFloatField;
    tblStavkiZaOdbranPlanSUMASENADDV: TFIBBCDField;
    tblStavkiZaOdbranPlanRE: TFIBIntegerField;
    tblStavkiZaOdbranPlanRENAZIV: TFIBStringField;
    tblStavkiZaOdbranPlanPLANBROJ: TFIBStringField;
    tblStavkiZaOdbranPlanDATUM_OD: TFIBDateField;
    tblStavkiZaOdbranPlanDATUM_DO: TFIBDateField;
    tblStavkiZaOdbranPlanIZNOS: TFIBBCDField;
    tblStavkiZaOdbranPlanOPIS: TFIBStringField;
    frxDBPonudiZaOdbranaNabavka: TfrxDBDataset;
    frxDBSPonudiDetail: TfrxDBDataset;
    PonudiDetail: TpFIBDataSet;
    PonudiDetailID: TFIBIntegerField;
    PonudiDetailBROJ: TFIBIntegerField;
    PonudiDetailGODINA: TFIBIntegerField;
    PonudiDetailARTVID: TFIBIntegerField;
    PonudiDetailARTSIF: TFIBIntegerField;
    PonudiDetailKOLICINA: TFIBBCDField;
    PonudiDetailSTATUS: TFIBIntegerField;
    PonudiDetailKOLICINA_PRIFATENA: TFIBBCDField;
    PonudiDetailOPIS: TFIBStringField;
    PonudiDetailCENA: TFIBBCDField;
    PonudiDetailDDV: TFIBFloatField;
    PonudiDetailCENADDV: TFIBBCDField;
    PonudiDetailRABAT: TFIBBCDField;
    PonudiDetailVALUTA: TFIBStringField;
    PonudiDetailKURS: TFIBFloatField;
    PonudiDetailKREIRAL: TFIBStringField;
    PonudiDetailVREME: TFIBDateTimeField;
    PonudiDetailARTNAZIV: TFIBStringField;
    PonudiDetailMERKA: TFIBStringField;
    PonudiDetailARTGRUPA: TFIBStringField;
    PonudiDetailARTVIDNAZIV: TFIBStringField;
    PonudiDetailARTGRUPANAZIV: TFIBStringField;
    PonudiDetailMERKANAZIV: TFIBStringField;
    PonudiDetailVALUTANAZIV: TFIBStringField;
    PonudiDetailKRAJNA_CENA: TFIBFloatField;
    PonudiDetailIIZNOS: TFIBBCDField;
    PonudiDetailSTATUSNAZIV: TFIBStringField;
    dsPonudiDetail: TDataSource;
    frxDBNabavkaInfo: TfrxDBDataset;
    frxDBPonudiPoArtikli: TfrxDBDataset;
    PonudiPoArtikli: TpFIBDataSet;
    dsPonudiPoArtikli: TDataSource;
    PonudiPoArtikliDATUM: TFIBDateField;
    PonudiPoArtikliBROJ: TFIBIntegerField;
    PonudiPoArtikliGODINA: TFIBIntegerField;
    PonudiPoArtikliBROJ1: TFIBIntegerField;
    PonudiPoArtikliGODINA1: TFIBIntegerField;
    PonudiPoArtikliARTVID: TFIBIntegerField;
    PonudiPoArtikliARTSIF: TFIBIntegerField;
    PonudiPoArtikliTIP_PARTNER: TFIBIntegerField;
    PonudiPoArtikliPARTNER: TFIBIntegerField;
    PonudiPoArtikliNAZIV: TFIBStringField;
    PonudiPoArtikliNAZIV_ARTIKAL: TFIBStringField;
    PonudiPoArtikliARTVID_NAZIV: TFIBStringField;
    PonudiPoArtikliKRAJNA_CENA: TFIBFloatField;
    PonudiPoArtikliBROJPONUDA: TFIBStringField;
    PonudiPoArtikliKOLICINA_PRIFATENA: TFIBFloatField;
    PonudiPoArtikliKOLICINA: TFIBFloatField;
    tblPoPlanStavkiCENA: TFIBBCDField;
    PlanStavkiRabEdCENA: TFIBBCDField;
    frxDBNajdobriPonudi: TfrxDBDataset;
    NajdobriPonudi: TpFIBDataSet;
    dsNajdobriPonudi: TDataSource;
    NajdobriPonudiARTVID_OUT: TFIBIntegerField;
    NajdobriPonudiARTSIF_OUT: TFIBIntegerField;
    NajdobriPonudiKOLICINA_OUT: TFIBFloatField;
    NajdobriPonudiKOLICINAPRIFATENA_OUT: TFIBFloatField;
    NajdobriPonudiDDV_OUT: TFIBIntegerField;
    NajdobriPonudiCENA_OUT: TFIBFloatField;
    NajdobriPonudiGODINA_OUT: TFIBIntegerField;
    NajdobriPonudiBROJ_OUT: TFIBIntegerField;
    NajdobriPonudiCENADDV_OUT: TFIBFloatField;
    NajdobriPonudiARTNAZIV_OUT: TFIBStringField;
    NajdobriPonudiVIDNAZIV_OUT: TFIBStringField;
    NajdobriPonudiPARTNERNAZIV_OUT: TFIBStringField;
    dsDrvoNaIzvestai: TDataSource;
    drvoNaIzvestai: TpFIBDataSet;
    drvoNaIzvestaiID: TFIBIntegerField;
    drvoNaIzvestaiNASLOV: TFIBStringField;
    drvoNaIzvestaiKOREN: TFIBIntegerField;
    PartneriPonuduvaci: TpFIBDataSet;
    dsPartneriPonuduvaci: TDataSource;
    PartneriPonuduvaciTIP_PARTNER: TFIBIntegerField;
    PartneriPonuduvaciPARTNER: TFIBIntegerField;
    PartneriPonuduvaciNAZIV: TFIBStringField;
    frxDBPonudiOdOdbranPartner: TfrxDBDataset;
    PonudiOdOdbranPartner: TpFIBDataSet;
    dsPonudiOdOdbranPartner: TDataSource;
    PonudiOdOdbranPartnerBROJ: TFIBIntegerField;
    PonudiOdOdbranPartnerGODINA: TFIBIntegerField;
    PonudiOdOdbranPartnerPREDMET: TFIBStringField;
    PonudiOdOdbranPartnerNAZIV: TFIBStringField;
    PonudiPartnerDetail: TpFIBDataSet;
    dsPonudiPartnerDetail: TDataSource;
    PonudiPartnerDetailARTVID: TFIBIntegerField;
    PonudiPartnerDetailARTSIF: TFIBIntegerField;
    PonudiPartnerDetailBROJ: TFIBIntegerField;
    PonudiPartnerDetailGODINA: TFIBIntegerField;
    PonudiPartnerDetailARTNAZIV: TFIBStringField;
    PonudiPartnerDetailVIDNAZIV: TFIBStringField;
    PonudiPartnerDetailKOLICINA: TFIBFloatField;
    PonudiPartnerDetailKOLICINA_PRIFATENA: TFIBFloatField;
    PonudiPartnerDetailCENA: TFIBBCDField;
    PonudiPartnerDetailCENADDV: TFIBBCDField;
    PonudiPartnerDetailDDV: TFIBFloatField;
    frxDBPonudiPartnerDetail: TfrxDBDataset;
    PrifateniPonudiOdOdbranPartner: TpFIBDataSet;
    dsPrifateniPonudiOdOdbranPartner: TDataSource;
    frxDBPrifateniPonudiOdOdbranPartner: TfrxDBDataset;
    frxDBPrifateniPonudiPartnerDetail: TfrxDBDataset;
    PrifateniPonudiPartnerDetail: TpFIBDataSet;
    dsPrifateniPonudiPartnerDetail: TDataSource;
    PrifateniPonudiPartnerDetailARTVID: TFIBIntegerField;
    PrifateniPonudiPartnerDetailARTSIF: TFIBIntegerField;
    PrifateniPonudiPartnerDetailBROJ: TFIBIntegerField;
    PrifateniPonudiPartnerDetailGODINA: TFIBIntegerField;
    PrifateniPonudiPartnerDetailARTNAZIV: TFIBStringField;
    PrifateniPonudiPartnerDetailVIDNAZIV: TFIBStringField;
    PrifateniPonudiPartnerDetailKOLICINA: TFIBFloatField;
    PrifateniPonudiPartnerDetailKOLICINA_PRIFATENA: TFIBFloatField;
    PrifateniPonudiPartnerDetailCENA: TFIBBCDField;
    PrifateniPonudiPartnerDetailCENADDV: TFIBBCDField;
    PrifateniPonudiPartnerDetailDDV: TFIBFloatField;
    PonudiOdOdbranPartnerSTATUSNAZIV: TFIBStringField;
    tblNalogDATUMPONUDA: TFIBDateField;
    ListaNaponudiZaArtikliCENA: TFIBBCDField;
    ListaNaponudiZaArtikliDDV: TFIBFloatField;
    ListaNaponudiZaArtikliCENADDV: TFIBBCDField;
    ListaNaponudiZaArtikliRABAT: TFIBFloatField;
    tblPlanZaSiteSektoriGODINA: TFIBSmallIntField;
    tblPlanZaSiteSektoriGRUPA: TFIBStringField;
    tblPlanZaSiteSektoriPODGRUPA: TFIBStringField;
    tblPlanZaSiteSektoriARTSIF: TFIBIntegerField;
    tblPlanZaSiteSektoriNAZIV: TFIBStringField;
    tblPlanZaSiteSektoriMERKA: TFIBStringField;
    tblPlanZaSiteSektoriSUMAKOLICINA: TFIBBCDField;
    tblPlanZaSiteSektoriCENA: TFIBBCDField;
    tblPlanZaSiteSektoriDDV: TFIBFloatField;
    tblPlanZaSiteSektoriCENADDV: TFIBBCDField;
    tblPlanZaSiteSektoriARTVIDNAZIV: TFIBStringField;
    tblPlanZaSiteSektoriGRUPANAZIV: TFIBStringField;
    tblPlanZaIzbraniotSektorGODINA: TFIBSmallIntField;
    tblPlanZaIzbraniotSektorGRUPA: TFIBStringField;
    tblPlanZaIzbraniotSektorPODGRUPA: TFIBStringField;
    tblPlanZaIzbraniotSektorARTSIF: TFIBIntegerField;
    tblPlanZaIzbraniotSektorNAZIV: TFIBStringField;
    tblPlanZaIzbraniotSektorMERKA: TFIBStringField;
    tblPlanZaIzbraniotSektorSUMAKOLICINA: TFIBBCDField;
    tblPlanZaIzbraniotSektorCENA: TFIBBCDField;
    tblPlanZaIzbraniotSektorDDV: TFIBFloatField;
    tblPlanZaIzbraniotSektorCENADDV: TFIBBCDField;
    tblPlanZaIzbraniotSektorARTVIDNAZIV: TFIBStringField;
    tblPlanZaIzbraniotSektorGRUPANAZIV: TFIBStringField;
    tblPlanZaIzbraniotSektorRENAZIV: TFIBStringField;
    MaxSifraPartner: TpFIBStoredProc;
    MaxSifraKontakt: TpFIBStoredProc;
    tblSETUP: TpFIBDataSet;
    dsSETUP: TDataSource;
    tblSETUPPARAMETAR: TFIBStringField;
    tblSETUPPARAM_VREDNOST: TFIBStringField;
    tblSETUPVREDNOST: TFIBIntegerField;
    tblSETUPP1: TFIBStringField;
    tblSETUPP2: TFIBStringField;
    tblSETUPV1: TFIBStringField;
    tblSETUPV2: TFIBStringField;
    NajdobriPonudiKRAJNACENA_OUT: TFIBFloatField;
    PrifateniPonudiPartnerDetailKRAJNA_CENA: TFIBFloatField;
    PonudiPartnerDetailKRAJNA_CENA: TFIBFloatField;
    tblSys_Setup: TpFIBDataSet;
    dsSys_Setup: TDataSource;
    tblSys_SetupPARAMETAR: TFIBStringField;
    tblSys_SetupPARAM_VREDNOST: TFIBStringField;
    tblSys_SetupVREDNOST: TFIBIntegerField;
    tblSys_SetupP1: TFIBStringField;
    tblSys_SetupP2: TFIBStringField;
    tblSys_SetupV1: TFIBStringField;
    tblSys_SetupV2: TFIBStringField;
    tblPonudiPREDMETNABAVKA: TFIBStringField;
    tblPonudiPREDMETPONUDA: TFIBStringField;
    frxMailExport2: TfrxMailExport;
    tblNabavkaMail: TpFIBDataSet;
    dsNabavkaMail: TDataSource;
    tblNabavkaMailID: TFIBIntegerField;
    tblNabavkaMailNABAVKA_ID: TFIBIntegerField;
    tblNabavkaMailDATUM: TFIBDateTimeField;
    tblNabavkaMailTIP_PARTNER: TFIBIntegerField;
    tblNabavkaMailPARTNER: TFIBIntegerField;
    tblNabavkaMailKONTAKT: TFIBIntegerField;
    tblNabavkaMailNASLOV: TFIBStringField;
    tblNabavkaMailTEXT: TFIBBlobField;
    tblPartnerMAIL: TFIBStringField;
    tblPartnerURL: TFIBStringField;
    NabavkaMailID: TpFIBStoredProc;
    tblNabavkaMailNAZIV: TFIBStringField;
    tblNabavkaMailKONTAKTNAZIV: TFIBStringField;
    CountStavkiPlan: TpFIBStoredProc;
    MKolicinaRealizacija: TpFIBDataSet;
    dsMKolicinaRealizacija: TDataSource;
    MKolicinaRealizacijaARTVID_OUT: TFIBIntegerField;
    MKolicinaRealizacijaARTSIF_OUT: TFIBIntegerField;
    MKolicinaRealizacijaKOLICINANABAVKA_OUT: TFIBIntegerField;
    MKolicinaRealizacijaKOLICINAPONUDA_OUT: TFIBIntegerField;
    MKolicinaRealizacijaOPIS: TFIBStringField;
    MKolicinaRealizacijaNAZIV: TFIBStringField;
    MKolicinaRealizacijaKOLICINANALOG_OUT: TFIBIntegerField;
    MKolicinaRealizacijaKOLICINAPRIEMNICA_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetail: TpFIBDataSet;
    dsMKolicinaRealizacijaDetail: TDataSource;
    MKolicinaRealizacijaDetailVIDARTIKALGEN_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailSIFRAARTIKALGEN_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailARTVID_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailOPIS: TFIBStringField;
    MKolicinaRealizacijaDetailARTSIF_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailNAZIV: TFIBStringField;
    MKolicinaRealizacijaDetailKOLICINAPONUDA_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailKOLICINANALOG_OUT: TFIBIntegerField;
    MKolicinaRealizacijaDetailKOLICINAPRIEMNICA_OUT: TFIBIntegerField;
    frxDBRealizacijaKolicina: TfrxDBDataset;
    frxDBRealizacijaKolicinaDetail: TfrxDBDataset;
    frxRealizacijaKolicinaDetail: TpFIBDataSet;
    dsFrxRealizacijaKolicina: TDataSource;
    SumaKolicinaRealizacija: TpFIBDataSet;
    dsSumaKolicinaRealizacija: TDataSource;
    SumaKolicinaRealizacijaSUM: TFIBBCDField;
    SumaKolicinaRealizacijaSUM1: TFIBBCDField;
    SumaKolicinaRealizacijaSUM2: TFIBBCDField;
    tblSysFrmConf: TpFIBDataSet;
    dstblSysFrmConf: TDataSource;
    tblSysFrmConfID: TFIBIntegerField;
    tblSysFrmConfUSERID: TFIBStringField;
    tblSysFrmConfAPPS: TFIBStringField;
    tblSysFrmConfFRM: TFIBStringField;
    tblSysFrmConfOBJECT: TFIBStringField;
    tblSysFrmConfPARAM: TFIBStringField;
    tblSysFrmConfVAL: TFIBStringField;
    tblNabaviODOBRIL: TFIBStringField;
    tblNabavkaMailMAIL_SENDER: TFIBStringField;
    tblNabavkaMailMAIL_RECIPIENT: TFIBStringField;
    frxJPEGExport1: TfrxJPEGExport;
    cxStyle8: TcxStyle;
    RealizPlanSektori: TpFIBDataSet;
    dsRealizPlanSektori: TDataSource;
    RealizPlanSektoriNABAVKACENA_OUT: TFIBIntegerField;
    RealizPlanSektoriVIDNAZIV_OUT: TFIBStringField;
    RealizPlanSektoriARTNAZIV_OUT: TFIBStringField;
    RealizPlanSektoriRE_OUT: TFIBIntegerField;
    RealizPlanSektoriARTSIF_OUT: TFIBIntegerField;
    RealizPlanSektoriARTVID_OUT: TFIBIntegerField;
    RealizPlanSektoriRENAZIV_OUT: TFIBStringField;
    RealizPlanSektoriPLANCENA_OUT: TFIBIntegerField;
    InsertStavkiNalog: TpFIBStoredProc;
    CountNalogPonudaNab: TpFIBStoredProc;
    tblSysSektorPlan: TpFIBDataSet;
    dsSysSektorPlan: TDataSource;
    tblSysSektorPlanID: TFIBIntegerField;
    tblSysSektorPlanNAZIV: TFIBStringField;
    tblPonudiRE: TFIBIntegerField;
    tblNalogRE: TFIBIntegerField;
    tblPlanSektorSTATUS: TFIBIntegerField;
    tblPlanSektorODOBRIL: TFIBStringField;
    tblPlanSektorSTATUSNAZIV: TFIBStringField;
    tblStatus: TpFIBDataSet;
    tblStatusID: TFIBIntegerField;
    tblStatusGRUPA: TFIBIntegerField;
    tblStatusNAZIV: TFIBStringField;
    tblStatusBOJA: TFIBIntegerField;
    dsStatus: TDataSource;
    tblPlanSektorDATUM_ODOBRUVANJE: TFIBDateField;
    tblNabaviDATUM_ODOBRUVANJE: TFIBDateField;
    tblNabaviBROJ_DOGOVOR: TFIBStringField;
    tblNabaviMESEC: TFIBIntegerField;
    tblNabavimesecNaziv: TFIBStringField;
    tblPonudiBROJ_DOGOVOR: TFIBStringField;
    frxRealizacijaKolicinaDetailVIDARTIKALGEN_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailSIFRAARTIKALGEN_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailARTVID_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailOPIS: TFIBStringField;
    frxRealizacijaKolicinaDetailARTSIF_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailNAZIV: TFIBStringField;
    frxRealizacijaKolicinaDetailKOLICINAPONUDA_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailKOLICINANALOG_OUT: TFIBIntegerField;
    frxRealizacijaKolicinaDetailKOLICINAPRIEMNICA_OUT: TFIBIntegerField;
    tblPoPonudaStavkiKRAJNA_CENA: TFIBBCDField;
    tblPoPonudaStavkiIIZNOS: TFIBBCDField;
    tblGodisenPlan: TpFIBDataSet;
    tblGodisenPlanBROJ: TFIBIntegerField;
    tblGodisenPlanGODINA: TFIBSmallIntField;
    tblGodisenPlanARTVID: TFIBIntegerField;
    tblGodisenPlanVIDNAZIV: TFIBStringField;
    tblGodisenPlanDATUM_OD: TFIBDateField;
    tblGodisenPlanDATUM_DO: TFIBDateField;
    tblGodisenPlanRE: TFIBIntegerField;
    tblGodisenPlanOPIS: TFIBStringField;
    dsGodisenPlan: TDataSource;
    tblGodisenPlanmesecNaziv: TFIBStringField;
    tblGodisenPlanIZNOS: TFIBFloatField;
    frxDBGodisenPlan: TfrxDBDataset;
    tblNalogBROJ_DOGOVOR: TFIBStringField;
    tblSpecifikacijaNaNabavkaBROJ_DOGOVOR: TFIBStringField;
    procedure tblSpecifikacijaZaPlanBeforePost(DataSet: TDataSet);
    procedure tblPoNabavkaStavkiBeforePost(DataSet: TDataSet);
    procedure tblPoPonudaStavkiBeforePost(DataSet: TDataSet);
    procedure tblPoNalogStavkiBeforePost(DataSet: TDataSet);
    procedure tblNalogAfterInsert(DataSet: TDataSet);
    procedure tblPonudiAfterInsert(DataSet: TDataSet);
    procedure tblNabaviAfterInsert(DataSet: TDataSet);
    procedure tblPlanSektorAfterInsert(DataSet: TDataSet);
    function frxDesignerPlanSaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    procedure tblNabavkaMailAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public

       function zemiCountStavkiPonuda(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
       function zemiCountStavkiNalog(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
       function zemiCenaStavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
       function zemiDDVStavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, ddv : Variant):Variant;
       function zemiMaxBrPlan(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiMaxIDNabavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiMaxBrojNabavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiMaxNabavkaStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiBrPonuda(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiBrStavkiNabavka(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr: Variant):Variant;
       function zemiCountPartner(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr: Variant):Variant;
       function zemiPonudaStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
       function zemiNalogStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
       function zemiBrNalog(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
       function zemiTipNabavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
       function zemiSumaKolicina(Proc : TpFIBStoredProc; p1, p2, p3,p4, v1, v2, v3, v4, ddv : Variant):Variant;
       function zemiGenArtVid(Proc: TpFIBStoredProc; p1: Variant; p2: Variant; p3: Variant; v1: Variant; v2: Variant; v3: Variant; gvid: Variant):Variant;
       function zemiGenID(Proc: TpFIBStoredProc; p1: Variant; p2: Variant; p3: Variant; v1: Variant; v2: Variant; v3: Variant; gid: Variant):Variant;
       function promeniStatusStavka(Proc : TpFIBStoredProc; p1, p2, p3, p4,p5, v1, v2, v3, v4, v5: Variant):Variant;
       function Validacija(cxp:Tpanel):Boolean;
       procedure RestoreControls(cxp:Tpanel);
       function zemiMaxPlusEden3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
       function zemiBr4(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
       procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
       function InsertPrifateniStavkiNalog(Proc : TpFIBStoredProc; p1, p2, p3, p4, p5, p6, v1, v2, v3, v4, v5, v6: Variant):Variant;
                           { Public declarations }


  end;

var
  dm: Tdm;
  otvorena_nab, stornirana_nab, zatvorena_nab, otvorena_pon,
  delumnoprifatena_pon, prifatena_pon, odbiena_pon,
  otvorena_pon_stavka, prifatena_pon_stavka, neprifatena_pon_stavka,
  otvoren_nal, delumnoisporacan_nal, isporacan_nal, vraboten, odobrena_nab, otvoren_plan, odobren_plan:integer;
  smpt_mail, smpt_pass, avtomatsko_odobruvanje:string;
  odobriNabavka, odobriPlan:Boolean;
implementation

uses dmKonekcija, dmMaticni;

{$R *.dfm}

function Tdm.Validacija(cxp:Tpanel):Boolean;
var k,i:integer;
    neuspesno:boolean;
    list:TList;
    ClassRef:TClass;
begin
    neuspesno := false;
    k := -1;
    list:=TList.Create;
    cxp.GetTabOrderList(list);
    for I := 0 to List.Count - 1 do
    begin
        ClassRef := TcxCustomEdit(List.items[i]).ClassType;
        while ClassRef<>nil  do
        begin
          if ((ClassRef.ClassName='TcxCustomEdit') and (TcxCustomEdit(List.items[i]).Tag=1 ) and (TcxCustomEdit(List.items[i]).Enabled=true ) ) then
          begin
            if TcxCustomEdit(List.items[i]).GetTextLen = 0 then
            begin
              if k=-1 then k:=i ;
              neuspesno := true;
              TcxCustomEdit(List.items[i]).Style.Color := clRed;
            end
            else
            begin
              TcxCustomEdit(List.items[i]).Style.Color := clWhite;
              break;
            end;

          end;
           ClassRef := ClassRef.ClassParent;
        end;
    end;
    if k<>-1 then
    begin
        TcxCustomEdit(List.items[k]).SetFocus;
        ShowMessage('������������ ��������� �� ������� ��������� �� ������!!!');
    end;
    Validacija:=neuspesno;
end;

procedure Tdm.RestoreControls(cxp:Tpanel);
var i, br:integer;
    list:TList;
    ClassRef:TClass;
begin
	 list:=TList.Create;
   cxp.GetTabOrderList(list);
   for i := 0 to List.Count - 1 do
	    begin
         ClassRef := TcxCustomEdit(List.items[i]).ClassType;
         while ClassRef<>nil  do
		       begin
              if ((ClassRef.ClassName='TcxCustomEdit')and (TcxCustomEdit(List.items[i]).Tag=1 ))  then
               begin

                  if (TcxCustomEdit(List.items[i]).ToString() = 'TcxComboBox') or (TcxCustomEdit(List.items[i]).ToString() = 'TcxLookupComboBox') or (TcxCustomEdit(List.items[i]).ToString() = 'TcxDBLookupComboBox') or (TcxCustomEdit(List.items[i]).ToString() = 'TcxDBDateEdit') or (TcxCustomEdit(List.items[i]).ToString() = 'TcxDBComboBox') or (TcxCustomEdit(List.items[i]).ToString() = 'TcxExtLookupComboBox') then
                     TcxCustomEdit(List.items[i]).Style.Color := $00FFFAF4
                  else
                     TcxCustomEdit(List.items[i]).Style.Color := clWhite;
				          break ;
               end
			    	else
				      begin
                 ClassRef := ClassRef.ClassParent;
              end;
           end;
      end;
end;


function Tdm.frxDesignerPlanSaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var template :TStream;
begin
     if dm.tblReportDizajnID.Value = 4001 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4001;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

     if dm.tblReportDizajnID.Value = 4002 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4002;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

     if dm.tblReportDizajnID.Value = 4003 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4003;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

      if dm.tblReportDizajnID.Value = 4004 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4004;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

      if dm.tblReportDizajnID.Value = 4005 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4005;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

     if dm.tblReportDizajnID.Value = 4006 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4006;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

      if dm.tblReportDizajnID.Value = 4007 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4007;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

      if dm.tblReportDizajnID.Value = 4008 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4008;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4009 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4009;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4010 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4010;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4011 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4011;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4012 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4012;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4013 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4013;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;

        if dm.tblReportDizajnID.Value = 4014 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4014;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
      if dm.tblReportDizajnID.Value = 4015 then
        begin
           template := TMemoryStream.Create;
           template.Position := 0;
           frxReport.SaveToStream(template);
           dm.tblReportDizajn.ParamByName('id').Value:=4015;
           tblReportDizajn.Open;
           tblReportDizajn.edit;
           (tblReportDizajnDATA as TBlobField).LoadFromStream(template);
           tblReportDizajn.Post;
        end;
end;

procedure Tdm.tblNabaviAfterInsert(DataSet: TDataSet);
begin
     tblNabaviKREIRAL.Value:=dmKon.user;
     tblNabaviDATUM_KREIRANA.Value:=Date;
end;

procedure Tdm.tblNabavkaMailAfterInsert(DataSet: TDataSet);
begin
      dm.tblNabavkaMailDATUM.Value:= Date;
end;

procedure Tdm.tblNalogAfterInsert(DataSet: TDataSet);
begin
     tblNalogKREIRAL.Value:=dmKon.user;
     tblNalogDATUM.Value:=Date;
end;

procedure Tdm.tblPlanSektorAfterInsert(DataSet: TDataSet);
begin
     tblPlanSektorKREIRAL.Value:=dmKon.user;
     tblPlanSektorDATUM_KREIRAN.Value:=Date;
end;

procedure Tdm.tblPoNabavkaStavkiBeforePost(DataSet: TDataSet);
begin
     tblPoNabavkaStavkiKREIRAL.Value:=dmKon.user;
end;

procedure Tdm.tblPoNalogStavkiBeforePost(DataSet: TDataSet);
begin
     tblPoNalogStavkiKREIRAL.Value:=dmKon.user;
end;

procedure Tdm.tblPonudiAfterInsert(DataSet: TDataSet);
begin
     tblPonudiKREIRAL.Value:= dmKon.user;
end;

procedure Tdm.tblPoPonudaStavkiBeforePost(DataSet: TDataSet);
begin
      tblPoPonudaStavkiKREIRAL.Value:=dmKon.user;
end;

procedure Tdm.tblSpecifikacijaZaPlanBeforePost(DataSet: TDataSet);
begin
             tblSpecifikacijaZaPlanKREIRAL.Value:= dmKon.user;
end;

function Tdm.promeniStatusStavka(Proc : TpFIBStoredProc; p1, p2, p3, p4, p5, v1, v2, v3, v4, v5: Variant):Variant;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         Proc.ExecProc;
//         result := ret;
    end;

function Tdm.InsertPrifateniStavkiNalog(Proc : TpFIBStoredProc; p1, p2, p3, p4, p5, p6, v1, v2, v3, v4, v5, v6: Variant):Variant;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
//         result := ret;
    end;



function Tdm.zemiCenaStavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(cena).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(cena).Value;
         result := ret;
    end;


 function Tdm.zemiPonudaStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
    var
      ret : integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(cena).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(cena).Value +1;
         result := ret;
    end;

  function tdm.zemiGenID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, gid : Variant):Variant;
    var
      ret : integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(gid).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(gid).Value + 1;
         result := ret;
    end;
  function Tdm.zemiTipNabavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
    var
      ret : integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(cena).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(cena).Value +1;
         result := ret;
    end;

function Tdm.zemiNalogStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, cena : Variant):Variant;
    var
      ret : integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(cena).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(cena).Value +1;
         result := ret;
    end;
////ok
function tdm.zemiGenArtVid(Proc: TpFIBStoredProc; p1: Variant; p2: Variant; p3: Variant; v1: Variant; v2: Variant; v3: Variant; gvid: Variant):Variant;
var
      ret :integer;
begin
       Proc.ExecProc;
       ret := Proc.ParamByName(gvid).Value;
       result :=ret;
end;
function Tdm.zemiDDVStavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, ddv : Variant):Variant;
    var
      ret :integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(ddv).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(ddv).Value;
         result :=ret;
    end;

    function Tdm.zemiSumaKolicina(Proc : TpFIBStoredProc; p1, p2, p3,p4, v1, v2, v3, v4, ddv : Variant):Variant;
    var
      ret :integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(ddv).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(ddv).Value;
         result :=ret;
    end;

    function Tdm.zemiMaxBrPlan(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
    var
      ret :integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
    end;

function Tdm.zemiMaxIDNabavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
end;


function Tdm.zemiMaxBrojNabavka(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
end;

function Tdm.zemiMaxNabavkaStavkaID(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
end;



function Tdm.zemiBrStavkiNabavka(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(maxbr).Value ;
         result :=ret;
end;

function Tdm.zemiCountStavkiPonuda(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(maxbr).Value ;
         result :=ret;
end;

function Tdm.zemiCountStavkiNalog(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(maxbr).Value ;
         result :=ret;
end;

function Tdm.zemiBrPonuda(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
end;

function Tdm.zemiBrNalog(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
end;

function Tdm.zemiCountPartner(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(maxbr).Value;
         result :=ret;
end;

function Tdm.zemiMaxPlusEden3(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, maxbr : Variant):Variant;
    var
      ret :integer;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 1
         else
           ret := Proc.ParamByName(maxbr).Value + 1;
         result :=ret;
    end;


function Tdm.zemiBr4(Proc : TpFIBStoredProc; p1, p2, p3, p4, v1, v2, v3, v4, maxbr : Variant):Variant;
var
      ret :integer;
begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
        if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(maxbr).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(maxbr).Value ;
         result :=ret;
end;


procedure Tdm.PrvPosledenTab(panel : TPanel; var posledna , prva : TWinControl);
var n:Smallint;
    list : TList;
begin
    list := TList.Create;
    panel.GetTabOrderList(list);
    n:=List.Count;
    prva := TWinControl(list.First);
    posledna := TWinControl(list.Items[n-1]);
end;

end.
