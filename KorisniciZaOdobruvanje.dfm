object frmKorisniciZaOdobruvanje: TfrmKorisniciZaOdobruvanje
  Left = 0
  Top = 0
  Caption = #1050#1086#1088#1080#1089#1085#1080#1094#1080' '#1082#1086#1080' '#1084#1086#1078#1077' '#1076#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1072#1090' '#1085#1072#1073#1072#1074#1082#1072
  ClientHeight = 368
  ClientWidth = 638
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 638
    Height = 345
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dPanel: TPanel
      Left = 0
      Top = 240
      Width = 638
      Height = 105
      Align = alBottom
      BevelInner = bvLowered
      BevelOuter = bvSpace
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 22
        Width = 64
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1086#1088#1080#1089#1085#1080#1082' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Sifra: TcxDBTextEdit
        Left = 86
        Top = 19
        TabStop = False
        BeepOnEnter = False
        Constraints.MinHeight = 21
        DataBinding.DataField = 'USERID'
        DataBinding.DataSource = dm.dstblSysFrmConf
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.LookAndFeel.SkinName = 'GlassOceans'
        StyleDisabled.LookAndFeel.SkinName = 'GlassOceans'
        StyleFocused.Color = 16113353
        StyleFocused.LookAndFeel.SkinName = 'GlassOceans'
        StyleHot.LookAndFeel.SkinName = 'GlassOceans'
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 243
      end
      object ButtonZacuvaj: TButton
        Left = 425
        Top = 63
        Width = 86
        Height = 25
        Action = aZapisi
        Images = dm.cxSmallImages
        TabOrder = 2
        OnKeyDown = EnterKakoTab
      end
      object ButtonOtkazi: TButton
        Left = 517
        Top = 63
        Width = 86
        Height = 25
        Action = aOtkazi
        Images = dm.cxSmallImages
        TabOrder = 3
        OnKeyDown = EnterKakoTab
      end
      object cxDBRadioGroup1: TcxDBRadioGroup
        Left = 86
        Top = 41
        Caption = 'Enabled'
        DataBinding.DataField = 'VAL'
        DataBinding.DataSource = dm.dstblSysFrmConf
        Properties.Columns = 2
        Properties.DefaultValue = 'True'
        Properties.Items = <
          item
            Caption = 'True'
            Value = 'True'
          end
          item
            Caption = 'False'
            Value = 'False'
          end>
        TabOrder = 1
        Height = 48
        Width = 131
      end
    end
    object lPanel: TPanel
      Left = 0
      Top = 0
      Width = 638
      Height = 240
      Align = alClient
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 636
        Height = 238
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dm.dstblSysFrmConf
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid1DBTableView1USERID: TcxGridDBColumn
            Caption = #1050#1086#1088#1080#1089#1085#1080#1082
            DataBinding.FieldName = 'USERID'
          end
          object cxGrid1DBTableView1APPS: TcxGridDBColumn
            Caption = #1040#1087#1083#1080#1082#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'APPS'
          end
          object cxGrid1DBTableView1FRM: TcxGridDBColumn
            Caption = #1060#1086#1088#1084#1072
            DataBinding.FieldName = 'FRM'
            Width = 133
          end
          object cxGrid1DBTableView1Column3: TcxGridDBColumn
            Caption = #1054#1073#1112#1077#1082#1090
            DataBinding.FieldName = 'OBJECT'
            Width = 87
          end
          object cxGrid1DBTableView1Column2: TcxGridDBColumn
            Caption = #1055#1072#1088#1072#1084#1077#1090#1072#1088
            DataBinding.FieldName = 'PARAM'
            Width = 67
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            Caption = #1042#1088#1077#1076#1085#1086#1089#1090
            DataBinding.FieldName = 'VAL'
            Width = 74
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 345
    Width = 638
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1044#1086#1076#1072#1076#1080' , F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072 +
          #1078#1080
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object ActionList1: TActionList
    Images = dm.cxSmallImages
    Left = 440
    Top = 88
    object aIzlez: TAction
      Caption = 'aIzlez'
      OnExecute = aIzlezExecute
    end
    object aDodadi: TAction
      Caption = 'aDodadi'
      ShortCut = 116
      OnExecute = aDodadiExecute
    end
    object aAzuriraj: TAction
      Caption = 'aAzuriraj'
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object ABrisi: TAction
      Caption = 'ABrisi'
      ShortCut = 119
      OnExecute = ABrisiExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 5
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aOtkaziExecute
    end
  end
end
