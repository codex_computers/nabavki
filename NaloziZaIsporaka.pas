unit NaloziZaIsporaka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, StdCtrls, RibbonActnCtrls, ToolWin, ActnMan, ActnCtrls, Ribbon,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxPC, cxContainer, cxLabel,
  ExtCtrls, dxStatusBar, dxRibbonStatusBar, ActnList, RibbonLunaStyleActnCtrls, cxGridExportLink, cxExport,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBExtLookupComboBox, cxCalendar,
  cxDBEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, DBCtrls, cxMemo,
  ActnMenus, RibbonActnMenus, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinSeven, dxSkinSharp, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinsdxBarPainter, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxSkinsdxRibbonPainter,
  dxRibbon, dxPSCore, dxPScxCommon, dxBar, cxBarEditItem, Menus,
  cxButtons, cxGroupBox, PlatformDefaultStyleActnCtrls, cxPCdxBarPopupMenu,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxBarBuiltInMenu,
  cxNavigator, dxRibbonCustomizationForm, System.Actions;

type
  TfrmEvidencijaNaNaloziZaIsporaka = class(TForm)
    ActionManager1: TActionManager;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Panel8: TPanel;
    Panel3: TPanel;
    cxLabel3: TcxLabel;
    Panel4: TPanel;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView1PREDMET_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJNAPONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJNANABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableViewBrojNalog: TcxGridDBColumn;
    Label3: TLabel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2: TcxGridDBTableView;
    cxGridViewRepository1DBTableView2BROJNABAVKA: TcxGridDBColumn;
    cxGridViewRepository1DBTableView2PONUDABROJ: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1DDV: TcxGridDBColumn;
    cxGrid2DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    Panel9: TPanel;
    sLabel: TLabel;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    DBText4: TDBText;
    GroupBox2: TGroupBox;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label12: TLabel;
    Label8: TLabel;
    PGodina: TcxDBComboBox;
    PBroj: TcxDBTextEdit;
    PDatumKreiran: TcxDBDateEdit;
    PStatus: TcxDBLookupComboBox;
    PNabavka: TcxDBLookupComboBox;
    PPonuda: TcxExtLookupComboBox;
    PPlakanje: TcxDBLookupComboBox;
    PredmetNabavka: TcxDBTextEdit;
    Label13: TLabel;
    PPrimac: TcxDBTextEdit;
    Label15: TLabel;
    Label14: TLabel;
    PPrimacKontakt: TcxDBTextEdit;
    PPrimacAdresa: TcxDBTextEdit;
    Label2: TLabel;
    PDatumIsporaka: TcxDBDateEdit;
    Label18: TLabel;
    PPrevoz: TcxDBLookupComboBox;
    Label4: TLabel;
    PTezina: TcxDBTextEdit;
    cxDBMemo1: TcxDBMemo;
    cxGridViewRepository1DBTableView2Column1: TcxGridDBColumn;
    Label16: TLabel;
    Label5: TLabel;
    PKontaktNaziv: TcxExtLookupComboBox;
    DKontakt: TcxDBTextEdit;
    cxGridViewRepository1DBTableView3: TcxGridDBTableView;
    cxGridViewRepository1DBTableView3TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView3NAZIV: TcxGridDBColumn;
    PTipPartner: TcxDBTextEdit;
    PPartnerID: TcxDBTextEdit;
    PPartner: TcxExtLookupComboBox;
    Label17: TLabel;
    Label23: TLabel;
    SOpis: TcxDBMemo;
    Label24: TLabel;
    GroupBox3: TGroupBox;
    Label25: TLabel;
    Label19: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    SArtvidNaziv: TcxDBTextEdit;
    SNaziv: TcxDBTextEdit;
    SPodgrupaNaziv: TcxDBTextEdit;
    SMerkaNaziv: TcxDBTextEdit;
    SArtvid: TcxDBTextEdit;
    SSifra: TcxDBTextEdit;
    SPodgrupaId: TcxDBTextEdit;
    SMerkaId: TcxDBTextEdit;
    GroupBox4: TGroupBox;
    Label21: TLabel;
    Label20: TLabel;
    Label31: TLabel;
    Label30: TLabel;
    Label22: TLabel;
    SCenaSoDDV: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCena: TcxDBTextEdit;
    SKolicina: TcxDBTextEdit;
    SIznoss: TcxDBTextEdit;
    cxGridViewRepository1DBTableView2Column2: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    aZacuvajStavkiExcel: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aPecatiStavki: TAction;
    dxBarButton1: TdxBarButton;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    aSpecifikacijaNalog: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    cxGroupBox1: TcxGroupBox;
    cxLookupComboBox1: TcxLookupComboBox;
    aIscisti: TAction;
    Label26: TLabel;
    Label27: TLabel;
    RibbonComboBox1: TcxComboBox;
    cxButton1: TcxButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavka: TAction;
    aAzurirajStavka: TAction;
    aBrisiStavka: TAction;
    ZacuvajClickk: TAction;
    OtkaziClick: TAction;
    Otkazi: TcxButton;
    Zacuvaj: TcxButton;
    ButtonZacuvaj: TcxButton;
    ButtonOtkazi: TcxButton;
    ButtonZacuvaj�Click�: TAction;
    ButtonOtkazi�Click: TAction;
    aDizajn: TAction;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1PONUDA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1PONUDA_GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1NABAVKA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1PRIMAC: TcxGridDBColumn;
    cxGrid1DBTableView1PRIMAC_ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1PRIMAC_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PLAKJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1VID_PREVOZ: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1TEZINA: TcxGridDBColumn;
    cxGrid1DBTableView1VREME: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PLAKANJENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PREVOZNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERMESTO: TcxGridDBColumn;
    cxGrid1DBTableView1DATUMPONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PNabavkaPropertiesEditValueChanged(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure DKontaktPropertiesEditValueChanged(Sender: TObject);
    procedure RibbonComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure PGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
   procedure Iznos (Sender: TObject);
    procedure SCenaExit(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure SDDVPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure PTipPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure PPartnerIDPropertiesEditValueChanged(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aSpecifikacijaNaNalogExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure Action2Execute(Sender: TObject);
    procedure PKontaktNazivExit(Sender: TObject);
    procedure PPartnerExit(Sender: TObject);
    procedure PPonudaExit(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aZacuvajStavkiExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPecatiStavkiExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aSpecifikacijaNalogExecute(Sender: TObject);
    procedure aIscistiExecute(Sender: TObject);
    procedure aDodadiStavkaExecute(Sender: TObject);
    procedure aAzurirajStavkaExecute(Sender: TObject);
    procedure aBrisiStavkaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure ZacuvajClickkExecute(Sender: TObject);
    procedure OtkaziClickExecute(Sender: TObject);
    procedure ButtonZacuvaj�Click�Execute(Sender: TObject);
    procedure ButtonOtkazi�ClickExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    prva, posledna :TWinControl;
  end;

var
  frmEvidencijaNaNaloziZaIsporaka: TfrmEvidencijaNaNaloziZaIsporaka;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, IzborNaStavkiZaNalog,
  VidPrevoz, Kontakt, Partner, TipPartner, Utils, dmResources;

{$R *.dfm}

procedure TfrmEvidencijaNaNaloziZaIsporaka.Iznos (Sender: TObject);
begin
     if (SCenaSoDDV.Text <> '') and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
        dm.tblPoNalogStavkiIIZNOS.Value:= dm.tblPoNalogStavkiCENADDV.Value * dm.tblPoNalogStavkiKOLICINA.Value;

end;
procedure TfrmEvidencijaNaNaloziZaIsporaka.OtkaziClickExecute(Sender: TObject);
begin
     frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           dm.tblNalog.Cancel;
           StateActive:=dsBrowse;
           dm.RestoreControls(panel8);
           PGodina.Enabled:=True;
           PBroj.Enabled:=True;
           Panel8.Enabled:=False;
           PNabavka.Enabled:=true;
           dxRibbon1.Enabled:=true;
           cxPageControl2.Enabled:=true;
           cxTabSheet1.Enabled:= true;
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid1.SetFocus;
        end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aAzurirajStavkaExecute(
  Sender: TObject);
begin
      if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if cxGrid2DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           cxPageControl2.ActivePage:=cxTabSheet4;
           cxTabSheet3.Enabled:=False;
           panel9.Enabled:=True;
           dxRibbon1.Enabled:=False;
           cxPageControl1.Enabled:=False;

           SOpis.SetFocus;

           dm.tblPoNalogStavki.Edit;
           StateActive:=dsEdit;

           if (SCenaSoDDV.Text <> '') and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
             begin
                dm.tblPoNalogStavkiIIZNOS.Value:= dm.tblPoNalogStavkiCENADDV.Value * dm.tblPoNalogStavkiKOLICINA.Value;
             end;
        end
     else ShowMessage('����������� ������ !!!!');
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aBrisiExecute(Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
     else ShowMessage('����������� ����� !!!');
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aBrisiStavkaExecute(Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if cxGrid2DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              dm.tblPoNalogStavki.Delete;
        end
     else ShowMessage('����������� ������ !!!!');
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.ButtonOtkazi�ClickExecute(Sender: TObject);
begin
     frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
     if frmDaNe.ShowModal =mrYes then
        begin
           StateActive:=dsBrowse;
           dm.tblPoNalogStavki.Cancel;
           dm.RestoreControls(Panel9);
           Panel9.Enabled:=False;
           dxRibbon1.Enabled:=true;
           cxPageControl1.Enabled:=true;
           cxTabSheet3.Enabled:= true;
           cxPageControl2.ActivePage:=cxTabSheet3;
           cxGrid2.SetFocus;
        end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.Action2Execute(Sender: TObject);
begin
     Application.HelpContext(106);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aAzurirajExecute(Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
          begin
             cxPageControl1.ActivePage:=cxTabSheet2;
             cxTabSheet1.Enabled:=False;
             PStatus.Enabled:=true;
             panel8.Enabled:=True;

             PGodina.Enabled:=False;
             PBroj.Enabled:=False;

             cxLookupComboBox1.Text:='';
             dxRibbon1.Enabled:=False;
             cxPageControl2.Enabled:=False;

             if (PNabavka.Text <> '') and (tag = 0)then
                begin
                   dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                   dm.tblPonudi.ParamByName('nabavkaID').Value:=PNabavka.EditValue;
                   dm.tblPonudi.ParamByName('status').Value:='%';
                   dm.tblPonudi.FullRefresh;
                end
             else
                if (PNabavka.Text = '') and (tag = 0)then
                   begin
                      dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                      dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
                      dm.tblPonudi.ParamByName('status').Value:='%';
                      dm.tblPonudi.FullRefresh;
                   end;

             PStatus.SetFocus;

             dm.tblNalog.Edit;
             StateActive:=dsEdit;

             PPonuda.text:=dm.tblNalogBROJNAPONUDA.Value;
             if DKontakt.Text = '' then PKontaktNaziv.Text:= '';
             if Tag = 1 then
                begin
                   PNabavka.EditValue:=dm.tblNabaviID.Value;
                   PNabavka.Enabled:=False;
                   PPonuda.Enabled:=false;
                end;
             if tag = 2 then
                begin
                   PNabavka.EditValue:=dm.tblPonudiNABAVKA_ID.Value;
                   PNabavka.Enabled:=False;
                   PPonuda.Enabled:=False;
                end;
          end
     else ShowMessage('����������� �����!!!');
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4006;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranNalog.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
     dm.tblStavkiZaOdbranNalog.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
     dm.tblStavkiZaOdbranNalog.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

       dm.frxReport.DesignReport();
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aDodadiStavkaExecute(
  Sender: TObject);
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmIzborNaStavkiNalog:=TfrmIzborNaStavkiNalog.Create(Application);
           frmIzborNaStavkiNalog.ShowModal;
           frmIzborNaStavkiNalog.Free;
           cxPageControl2.ActivePage:=cxTabSheet3;
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxgrid1.SetFocus;
        end
     else ShowMessage('����������� ����� !!!');
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aHelpExecute(Sender: TObject);
begin
     Application.HelpContext(106);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aIscistiExecute(Sender: TObject);
begin
     if tag = 0 then
        begin
           dm.tblNalog.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
           dm.tblNalog.ParamByName('nabavkaID').Value:='%';
           dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
           dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
           dm.tblNalog.ParamByName('status').Value:='%';
           dm.tblNalog.FullRefresh;
           cxGrid1.SetFocus;
           cxLookupComboBox1.Text:='';
        end;
     if tag =1  then
       begin
          dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
          dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
          dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
          dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.FullRefresh;
          cxGrid1.SetFocus;
          cxLookupComboBox1.Text:='';
       end;
      if tag = 2  then
       begin
          dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:='%';
          dm.tblNalog.ParamByName('ponudaBroj').Value:=dm.tblPonudiBROJ.Value;
          dm.tblNalog.ParamByName('ponudaGodina').Value:=dm.tblPonudiGODINA.Value;
          dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.FullRefresh;
          cxGrid1.SetFocus;
          cxLookupComboBox1.Text:='';
       end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aIzlezExecute(Sender: TObject);
begin
     if (cxPageControl1.ActivePage = cxTabSheet2) and (StateActive in [dsBrowse]) and (cxPageControl1.Enabled = true) then
         begin
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid1.SetFocus;
         end
     else
     if (cxPageControl1.ActivePage = cxTabSheet2) and (StateActive in [dsEdit,dsInsert]) and (cxPageControl1.Enabled = true)then
         begin
            frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
            if frmDaNe.ShowModal =mrYes then
               begin
                 dm.tblNalog.Cancel;
                 StateActive:=dsBrowse;
                 dm.RestoreControls(panel8);
                 PGodina.Enabled:=True;
                 PBroj.Enabled:=True;
                 Panel8.Enabled:=False;
                 PNabavka.Enabled:=true;
                 dxRibbon1.Enabled:=true;
                 cxPageControl2.Enabled:=true;
                 cxTabSheet1.Enabled:= true;
                 cxPageControl1.ActivePage:=cxTabSheet1;
                 cxGrid1.SetFocus;
               end;
         end
     else
     if (cxPageControl2.ActivePage = cxTabSheet4) and (StateActive in [dsBrowse]) then
         begin
            cxPageControl2.ActivePage:=cxTabSheet3;
            cxGrid2.SetFocus;
         end
     else
     if (cxPageControl2.ActivePage = cxTabSheet4) and (StateActive in [dsEdit,dsInsert]) then
         begin
            frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
            if frmDaNe.ShowModal =mrYes then
              begin
                 StateActive:=dsBrowse;
                 dm.tblPoNalogStavki.Cancel;
                 dm.RestoreControls(Panel9);
                 Panel9.Enabled:=False;
                 dxRibbon1.Enabled:=true;
                 cxPageControl1.Enabled:=true;
                 cxTabSheet3.Enabled:= true;
                 cxPageControl2.ActivePage:=cxTabSheet3;
                 cxGrid2.SetFocus;
              end;
         end
     else Close;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aNovExecute(Sender: TObject);
var pom1, pom2, pom3, pom4 :integer;
begin
     if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = otvorena_nab) then ShowMessage('��������� �� � �������� !!!')
     else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
     else if (tag = 2) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
     else if (tag = 2) and (dm.tblNabaviSTATUS.Value = otvorena_nab) then ShowMessage('��������� �� � �������� !!!')
     else if (tag = 2) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
     else
       begin
          cxPageControl1.ActivePage:=cxTabSheet2;
          cxTabSheet1.Enabled:=False;
          cxPageControl2.Enabled:=False;
          cxLookupComboBox1.Text:='';
          dxRibbon1.Enabled:=False;
          Panel8.Enabled:=True;

          if Tag = 1 then
             begin
                PGodina.Enabled:=false;
                PredmetNabavka.SetFocus;
                PPartner.Text:='';
             end
          else if tag = 2 then
             begin
                PGodina.Enabled:=false;
                PredmetNabavka.SetFocus;
             end
          else
             begin
                PredmetNabavka.SetFocus;
             end;

          StateActive:=dsInsert;
          dm.tblNalog.Insert;

          if Tag = 1 then
            begin
               PNabavka.EditValue:=dm.tblNabaviID.Value;
               PNabavka.Enabled:=False;
               PPonuda.Text:='';
               PPonuda.Enabled:=False;
            end;

          if tag = 2 then
            begin
               pom1:=dm.tblPonudiBROJ.Value;
               pom2:=dm.tblPonudiGODINA.Value;
               PNabavka.EditValue:=dm.tblPonudiNABAVKA_ID.Value;
               PNabavka.Enabled:=False;
               dm.tblPonudi.Locate('BROJ; GODINA', VarArrayOf([pom1, pom2]) , []);
               PPonuda.Text:=dm.tblPonudiPONUDABROJ.Value;
               pom3:=dm.tblPonudiTIP_PARTNER.Value;
               pom4:=dm.tblPonudiPARTNER.Value;
               dm.tblNalogTIP_PARTNER.Value:=pom3;
               dm.tblNalogPARTNER.Value:=pom4;
               dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([pom3, pom4]) , []);
               PPartner.Text:=dm.tblPartnerNAZIV.Value;
               PPonuda.Enabled:=False;
            end;

          if (tag = 0) then
            begin
               PPonuda.Text:='';
               PPartner.Text:='';
               PPartnerID.Text:='';
               PTipPartner.Text:='';
               PredmetNabavka.SetFocus;
            end;

          dm.tblNalogGODINA.Value:=RibbonComboBox1.EditValue;
          dm.tblNalogBROJ.Value:=dm.zemiBrNalog(dm.MaxNalogID, 'GODINA', Null, Null, RibbonComboBox1.EditValue, Null, Null, 'MAXBR');
          dm.tblNalogSTATUS.Value:=otvoren_nal;
          dm.tblNalogPRIMAC.Value:= dm.FirmiPARTNERNAZIV.Value;
          dm.tblNalogPRIMAC_ADRESA.Value:=dm.FirmiADRESAMESTO.Value;
          dm.tblNalogRE.Value:=dmKon.re;
       end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aOsveziExecute(Sender: TObject);
begin
     if tag = 0 then
        begin
           dm.tblNalog.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
           dm.tblNalog.ParamByName('nabavkaID').Value:='%';
           dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
           dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
           if cxLookupComboBox1.Text <> '' then
              dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
           else
              dm.tblNalog.ParamByName('status').Value:='%';
           dm.tblNalog.FullRefresh;
           cxGrid1.SetFocus;
        end;
     if tag =1  then
       begin
          dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
          dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
          dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
          if cxLookupComboBox1.Text <> '' then
             dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
          else
             dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.FullRefresh;
          cxGrid1.SetFocus;
       end;
     if tag = 2  then
       begin
          dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:='%';
          dm.tblNalog.ParamByName('ponudaBroj').Value:=dm.tblPonudiBROJ.Value;
          dm.tblNalog.ParamByName('ponudaGodina').Value:=dm.tblPonudiGODINA.Value;
          if cxLookupComboBox1.Text <> '' then
             dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
          else
             dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.FullRefresh;
          cxGrid1.SetFocus;
       end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aPecatiExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aPecatiStavkiExecute(
  Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aPecatiTabelaExecute(
  Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+cxPageControl1.ActivePage.Name,true);
     zacuvajGridVoIni(cxGrid2DBTableView1,dmKon.aplikacija+Name+cxPageControl2.ActivePage.Name,true);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aSnimiPecatenjeExecute(
  Sender: TObject);
begin
      zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aSpecifikacijaNalogExecute(
  Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4006;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranNalog.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
     dm.tblStavkiZaOdbranNalog.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
     dm.tblStavkiZaOdbranNalog.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.Variables.AddVariable('VAR', 'BROJ_DOGOVOR', QuotedStr(dm.tblNalogBROJ_DOGOVOR.Value));

//       dm.frxReport.DesignReport();
       dm.frxReport.ShowReport;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aSpecifikacijaNaNalogExecute(
  Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4006;
     dm.tblReportDizajn.FullRefresh;

     dm.tblStavkiZaOdbranNalog.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
     dm.tblStavkiZaOdbranNalog.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
     dm.tblStavkiZaOdbranNalog.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.Variables.AddVariable('VAR', 'BROJ_DOGOVOR', QuotedStr(dm.tblNalogBROJ_DOGOVOR.Value));

//     dm.frxReport.DesignReport();
       dm.frxReport.ShowReport;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aZacuvajExcelExecute(
  Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.aZacuvajStavkiExcelExecute(
  Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, Caption);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.ButtonZacuvaj�Click�Execute(
  Sender: TObject);
begin
     if dm.Validacija(Panel9) = false then
          begin
             dm.tblPoNalogStavki.Post;
             dm.RestoreControls(Panel9);
             StateActive:=dsBrowse;
             dm.tblPoNalogStavki.FullRefresh;
             Panel9.Enabled:=False;
             dxRibbon1.Enabled:=true;
             cxPageControl1.Enabled:=true;
             cxTabSheet3.Enabled:= true;
             cxPageControl2.ActivePage:=cxTabSheet3;
             cxGrid2.SetFocus;
          end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
           dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
           dm.tblPoNalogStavki.FullRefresh;
        end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var pom1, pom2, pom3, pom4 :integer;
  begin
     case key of
        VK_F5:begin
            if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = otvorena_nab) then ShowMessage('��������� �� � �������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
            else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
            else if (tag = 2) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 2) and (dm.tblNabaviSTATUS.Value = otvorena_nab) then ShowMessage('��������� �� � �������� !!!')
            else if (tag = 2) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
            else
              begin
                 cxPageControl1.ActivePage:=cxTabSheet2;
                 cxTabSheet1.Enabled:=False;
                 cxPageControl2.Enabled:=False;
                 cxLookupComboBox1.Text:='';
                 dxRibbon1.Enabled:=False;
                 Panel8.Enabled:=True;
                 Zacuvaj.Visible:=True;
                 Otkazi.Visible:=True;
                 if Tag = 1 then
                   begin
                      PGodina.Enabled:=false;
                      PredmetNabavka.SetFocus;
                      PPartner.Text:='';
                   end
                 else if tag = 2 then
                   begin
                      PGodina.Enabled:=false;
                      PredmetNabavka.SetFocus;
                   end
                 else
                   PredmetNabavka.SetFocus;

                 StateActive:=dsInsert;
                 dm.tblNalog.Insert;

                 if Tag = 1 then
                   begin
                      PNabavka.EditValue:=dm.tblNabaviID.Value;
                      PPonuda.Text:='';
                      PNabavka.Enabled:=False;
                      PPonuda.Enabled:=False;
                   end;

                 if tag = 2 then
                   begin
                      pom1:=dm.tblPonudiBROJ.Value;
                      pom2:=dm.tblPonudiGODINA.Value;
                      PNabavka.EditValue:=dm.tblPonudiNABAVKA_ID.Value;
                      PNabavka.Enabled:=False;
                      dm.tblPonudi.Locate('BROJ; GODINA', VarArrayOf([pom1, pom2]) , []);
                      PPonuda.Text:=dm.tblPonudiPONUDABROJ.Value;
                      pom3:=dm.tblPonudiTIP_PARTNER.Value;
                      pom4:=dm.tblPonudiPARTNER.Value;
                      dm.tblNalogTIP_PARTNER.Value:=pom3;
                      dm.tblNalogPARTNER.Value:=pom4;
                      dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([pom3, pom4]) , []);
                      PPartner.Text:=dm.tblPartnerNAZIV.Value;
                      PPonuda.Enabled:=False;
                   end;

                 if (tag = 0) then
                   begin
                      PPonuda.Text:='';
                      PPartner.Text:='';
                      PPartnerID.Text:='';
                      PTipPartner.Text:='';
                      PredmetNabavka.SetFocus;
                   end;

                 dm.tblNalogGODINA.Value:=RibbonComboBox1.EditValue;
                 dm.tblNalogBROJ.Value:=dm.zemiBrNalog(dm.MaxNalogID, 'GODINA', Null, Null, RibbonComboBox1.EditValue, Null, Null, 'MAXBR');
                 dm.tblNalogSTATUS.Value:=otvoren_nal;
                 dm.tblNalogPRIMAC.Value:= dm.FirmiPARTNERNAZIV.Value;
                 dm.tblNalogPRIMAC_ADRESA.Value:=dm.FirmiADRESAMESTO.Value;
                 dm.tblNalogRE.Value:=dmKon.re;
              end;
        end;
        VK_F6:begin
            if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
            else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
            else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
              begin
                 cxPageControl1.ActivePage:=cxTabSheet2;
                 cxTabSheet1.Enabled:=False;
                 PStatus.Enabled:=true;
                 panel8.Enabled:=True;

                 PGodina.Enabled:=False;
                 PBroj.Enabled:=False;

                 cxLookupComboBox1.Text:='';
                 dxRibbon1.Enabled:=False;
                 cxPageControl2.Enabled:=False;

                 if (PNabavka.Text <> '') and (tag = 0)then
                  begin
                     dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                     dm.tblPonudi.ParamByName('nabavkaID').Value:=PNabavka.EditValue;
                     dm.tblPonudi.ParamByName('status').Value:='%';
                     dm.tblPonudi.FullRefresh;
                  end
                 else
                    if (PNabavka.Text = '') and (tag = 0)then
                      begin
                         dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                         dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
                         dm.tblPonudi.ParamByName('status').Value:='%';
                         dm.tblPonudi.FullRefresh;
                      end;

                 PStatus.SetFocus;

                 dm.tblNalog.Edit;
                 StateActive:=dsEdit;

                 PPonuda.text:=dm.tblNalogBROJNAPONUDA.Value;
                 if DKontakt.Text = '' then PKontaktNaziv.Text:= '';
                 if Tag = 1 then
                  begin
                     PNabavka.EditValue:=dm.tblNabaviID.Value;
                     PNabavka.Enabled:=False;
                     PPonuda.Enabled:=false;
                  end;
                 if tag = 2 then
                  begin
                     PNabavka.EditValue:=dm.tblPonudiNABAVKA_ID.Value;
                     PNabavka.Enabled:=False;
                     PPonuda.Enabled:=False;
                  end;
              end
            else ShowMessage('����������� �����!!!');
        end;
        VK_F7:begin
              if tag = 0 then
                 begin
                    dm.tblNalog.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
                    dm.tblNalog.ParamByName('nabavkaID').Value:='%';
                    dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
                    dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
                    if cxLookupComboBox1.Text <> '' then
                       dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
                    else
                       dm.tblNalog.ParamByName('status').Value:='%';
                    dm.tblNalog.FullRefresh;
                    cxGrid1.SetFocus;
                 end;
              if tag =1  then
                 begin
                    dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
                    dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
                    dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
                    dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
                    if cxLookupComboBox1.Text <> '' then
                       dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
                    else
                       dm.tblNalog.ParamByName('status').Value:='%';
                    dm.tblNalog.FullRefresh;
                    cxGrid1.SetFocus;
                 end;
              if tag = 2  then
                 begin
                    dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
                    dm.tblNalog.ParamByName('nabavkaID').Value:='%';
                    dm.tblNalog.ParamByName('ponudaBroj').Value:=dm.tblPonudiBROJ.Value;
                    dm.tblNalog.ParamByName('ponudaGodina').Value:=dm.tblPonudiGODINA.Value;
                    if cxLookupComboBox1.Text <> '' then
                       dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue
              else
                    dm.tblNalog.ParamByName('status').Value:='%';
              dm.tblNalog.FullRefresh;
              cxGrid1.SetFocus;
          end;
        end;
        VK_F8:begin
           if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
           else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
           else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
           else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
              begin
                 frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                 if frmDaNe.ShowModal =mrYes then
                    cxGrid1DBTableView1.DataController.DataSet.Delete();
              end
           else ShowMessage('����������� ����� !!!');
        end;
       VK_INSERT:begin
            if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
            else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
            else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               begin
                  frmIzborNaStavkiNalog:=TfrmIzborNaStavkiNalog.Create(Application);
                  frmIzborNaStavkiNalog.ShowModal;
                  frmIzborNaStavkiNalog.Free;
                  cxPageControl2.ActivePage:=cxTabSheet3;
                  cxPageControl1.ActivePage:=cxTabSheet1;
                  cxgrid1.SetFocus;
               end
            else ShowMessage('����������� �����');
          end;
     end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxGrid2DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     case Key of
           VK_INSERT:begin
            if (tag = 1) and (dm.tblNabaviSTATUS.Value = zatvorena_nab) then ShowMessage('��������� � ��������� !!!')
            else if (tag = 1) and (dm.tblNabaviSTATUS.Value = stornirana_nab) then ShowMessage('��������� � ���������� !!!')
            else if (tag = 2) and (dm.tblPonudiSTATUS.Value = odbiena_pon) then ShowMessage('�������� � ������� !!!')
            else if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               begin
                  frmIzborNaStavkiNalog:=TfrmIzborNaStavkiNalog.Create(Application);
                  frmIzborNaStavkiNalog.ShowModal;
                  frmIzborNaStavkiNalog.Free;
                  cxPageControl2.ActivePage:=cxTabSheet3;
                  cxPageControl1.ActivePage:=cxTabSheet1;
                  cxgrid2.SetFocus;
               end
            else ShowMessage('����������� �����');
          end;
     end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxGrid2DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
     if (cxLookupComboBox1.Text <> '') and (tag = 0) then
       begin
           dm.tblNalog.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
           dm.tblNalog.ParamByName('nabavkaID').Value:='%';
           dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue;
           dm.tblNalog.FullRefresh;
           dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
           dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
           dm.tblPoNalogStavki.FullRefresh;
           cxGrid1.SetFocus;
       end;

     if (cxLookupComboBox1.Text <> '') and (tag = 1) then
       begin
           dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
           dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
           dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue;
           dm.tblNalog.FullRefresh;
           dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
           dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
           dm.tblPoNalogStavki.FullRefresh;
           cxGrid1.SetFocus;
       end;

      if (cxLookupComboBox1.Text <> '') and (tag = 2) then
       begin
           dm.tblNalog.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
           dm.tblNalog.ParamByName('nabavkaID').Value:='%';
           dm.tblNalog.ParamByName('status').Value:=cxLookupComboBox1.EditValue;
           dm.tblNalog.FullRefresh;
           dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
           dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
           dm.tblPoNalogStavki.FullRefresh;
           cxGrid1.SetFocus;
       end;

end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.cxPageControl1PageChanging(
  Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
       if ( StateActive in [dsBrowse] ) and (cxPageControl1.ActivePage = cxTabSheet1)then
         begin
//            if PGodina.Text <> '' then
//               begin
//                  dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
//                  dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
//                  dm.tblPonudi.ParamByName('status').Value:='%';
//                  dm.tblPonudi.Refresh;
//               end;
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
               begin
                   dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                   PPartner.Text:= dm.tblPartnerNAZIV.Value;
               end
            else
              PPartner.Text:= '';
            PPonuda.Text:=dm.tblNalogBROJNAPONUDA.Value;
            PNabavka.EditValue:=dm.tblNalogNABAVKA_ID.Value;
            if DKontakt.Text <> '' then
               begin
                  dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, strtoInt(DKontakt.Text)]) , []);
                  PKontaktNaziv.Text:= dm.PartnerVrabotenNAZIV.Value;
               end
            else
               PKontaktNaziv.Text:= '';
            RibbonComboBox1.Enabled:=false;
            cxLookupComboBox1.Enabled:=false;
            aIscisti.Enabled:=False;
         end;
       if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
        if Tag = 0 then
           begin
              RibbonComboBox1.Enabled:= true;
              cxLookupComboBox1.Enabled:=True;
              aIscisti.Enabled:=True;
           end;
        if tag = 1 then
           begin
              aIscisti.Enabled:=True;
              cxLookupComboBox1.Enabled:=True;
           end;
        if tag = 2 then
           begin
              aIscisti.Enabled:=True;
              cxLookupComboBox1.Enabled:=True;
           end;
        end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.DKontaktPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
      if StateActive in [dsInsert,dsEdit] then
          begin
              if (DKontakt.Text <> '') then
                 pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, vraboten,strtoInt(DKontakt.text), Null, 'BROJ');
               if (pom = 1)then
                  begin
                      dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, strtoInt(DKontakt.Text)]) , []);
                      PKontaktNaziv.Text:= dm.PartnerVrabotenNAZIV.Value;
                  end
               else PKontaktNaziv.Text:= '';
          end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.FormCreate(Sender: TObject);
begin
    ProcitajFormaIzgled(self);
    if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
     if tag = 0 then
        begin
           dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
           dm.tblPonudi.ParamByName('godina').Value:=dmKon.godina;
           dm.tblPonudi.ParamByName('nabavkaID').Value:= '%';
           dm.tblPonudi.ParamByName('status').Value:='%';
           dm.tblPonudi.Open;
        end;
     dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ '%';
     dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
     dm.tblNabavi.ParamByName('godina').Value:=dmKon.godina;
     dm.tblNabavi.ParamByName('status').Value:='%';
     dm.tblNabavi.Open;
     dm.NalogStatus.Open;
     dm.PartnerVraboten.ParamByName('vraboten').Value:=vraboten;
     dm.PartnerVraboten.Open;
     dm.Plakanje.Open;
     dm.prevoz.Open;
     dm.DDV.Open;
     dm.tblPartner.ParamByName('tipPartner').Value:='%';
     dm.tblPartner.Open;
     dm.Firmi.close;
     dm.Firmi.ParamByName('re').Value:=dmKon.re;
     dm.Firmi.Open;
     dm.tblReportDizajn.Open;
     dm.tblStavkiZaOdbranNalog.Open;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.FormShow(Sender: TObject);
begin
     SpremiForma(self);
     if Tag = 1 then
       begin
          dm.tblNalog.Close;
          dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
          dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
          dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
          dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.ParamByName('re').Value:=dmKon.re;
          dm.tblNalog.Open;

          RibbonComboBox1.Text:=IntToStr(dm.tblNabaviGODINA.Value);
          Caption:='��������� �� ������ �� �������' + ' '+dm.tblNabaviBROJNABAVKA.Value;
          RibbonComboBox1.Enabled:=false;
       end
     else if Tag = 2 then
       begin
          dm.tblNalog.Close;
          dm.tblNalog.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
          dm.tblNalog.ParamByName('nabavkaID').Value:='%';
          dm.tblNalog.ParamByName('ponudaBroj').Value:=dm.tblPonudiBROJ.Value;
          dm.tblNalog.ParamByName('ponudaGodina').Value:=dm.tblPonudiGODINA.Value;
          dm.tblNalog.ParamByName('status').Value:='%';
          dm.tblNalog.ParamByName('re').Value:=dmKon.re;
          dm.tblNalog.Open;

          RibbonComboBox1.Text:=IntToStr(dm.tblPonudiGODINA.Value);
          Caption:='��������� �� ������ �� ������' + ' '+dm.tblPonudiPONUDABROJ.Value;
          RibbonComboBox1.Enabled:=false;

       end
     else
        begin
           dm.tblNalog.Close;
           dm.tblNalog.ParamByName('re').Value:=dmKon.re;
           dm.tblNalog.ParamByName('godina').Value:=dmKon.godina;
           dm.tblNalog.ParamByName('status').Value:='%';
           dm.tblNalog.ParamByName('nabavkaID').Value:='%';
           dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
           dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
           dm.tblNalog.Open;
           RibbonComboBox1.Text:=IntToStr(dmKon.godina);
           Caption:='��������� �� ������ �� ��������';
           RibbonComboBox1.Enabled:=true;
        end;

     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+cxPageControl1.ActivePage.Name,true,true);
     procitajGridOdIni(cxGrid2DBTableView1,dmKon.aplikacija+Name+cxPageControl2.ActivePage.Name,true,true);

     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);

     cxPageControl1.ActivePage:=cxTabSheet1;
     cxPageControl2.ActivePage:=cxTabSheet3;

     cxGrid1.SetFocus;
     StateActive:=dsBrowse;

     dm.tblPoNalogStavki.Close;
     dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
     dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
     dm.tblPoNalogStavki.Open;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PGodinaPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
     if StateActive in [dsInsert] then
        if (PGodina.Text <> '') and (tag = 0)then
            begin
               PBroj.Text:=IntToStr(dm.zemiBrNalog(dm.MaxNalogID, 'GODINA', Null, Null, StrToInt(PGodina.Text) , Null, Null, 'MAXBR'));
               dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
               dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
               dm.tblPonudi.ParamByName('status').Value:='%';
               dm.tblPonudi.FullRefresh;
               dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
               dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
               dm.tblNabavi.ParamByName('godina').Value:=strToInt(PGodina.Text);
               dm.tblNabavi.FullRefresh;
            end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PKontaktNazivExit(Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit])and (PKontaktNaziv.Text <> '') then
        dm.tblNalogKONTAKT.Value:=dm.PartnerVrabotenID.Value;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PNabavkaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) and (tag = 0) then
        if (PNabavka.Text <> '') and (PGodina.Text <> '') then
           begin
               if tag = 0 then
                 begin
                    dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                    dm.tblPonudi.ParamByName('nabavkaID').Value:=PNabavka.EditValue;
                    dm.tblPonudi.ParamByName('status').Value:='%';
                    dm.tblPonudi.FullRefresh;
                 end
            end
        else  if (PNabavka.Text = '') and (PGodina.Text <> '') then
            begin
               dm.tblPonudi.ParamByName('godina').Value:=StrToInt(PGodina.Text);
               dm.tblPonudi.ParamByName('nabavkaID').Value:='%';
               dm.tblPonudi.ParamByName('status').Value:='%';
               dm.tblPonudi.FullRefresh;
            end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PPartnerExit(Sender: TObject);
begin
      if (StateActive in [dsEdit,dsInsert]) and (PPartner.text <> '') and (cxPageControl1.ActivePage = cxTabSheet2)  then
         begin
            dm.tblNalogTIP_PARTNER.Value :=dm.tblPartnerTIP_PARTNER.Value;
            dm.tblNalogPARTNER.Value:=dm.tblPartnerID.Value;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PPartnerIDPropertiesEditValueChanged(
  Sender: TObject);
var pom:integer;
begin
     if (StateActive in [dsInsert,dsEdit]) and (cxPageControl1.ActivePage = cxTabSheet2)then
         begin
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
                begin
                   pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
                   if pom = 1  then
                      begin
                         dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                         PPartner.Text:= dm.tblPartnerNAZIV.Value;
                      end
                   else PPartner.Text:= '';
                end;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PPonudaExit(Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) and (cxPageControl1.ActivePage = cxTabSheet2)and (PPonuda.Text <> '')then
       begin
          dm.tblNalogTIP_PARTNER.Value:=dm.tblPonudiTIP_PARTNER.Value;
          dm.tblNalogPARTNER.Value:=dm.tblPonudiPARTNER.Value;
       end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.PTipPartnerPropertiesEditValueChanged(
  Sender: TObject);
  var pom:integer;
begin
     if (StateActive in [dsInsert,dsEdit]) and (cxPageControl1.ActivePage = cxTabSheet2)then
         begin
            if (PTipPartner.Text <> '') and (PPartnerID.Text <> '') then
                begin
                   pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(PtipPartner.text),strtoInt(PPartnerID.text), Null, 'BROJ');
                   if pom = 1  then
                      begin
                         dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(PTipPartner.Text), StrToInt(PPartnerID.Text)]) , []);
                         PPartner.Text:= dm.tblPartnerNAZIV.Value;
                      end
                   else PPartner.Text:= '';
                end;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.RibbonComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
     if (RibbonComboBox1.Text <> '') and (Tag = 0)then
         begin
            dm.tblNalog.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
            dm.tblNalog.ParamByName('nabavkaID').Value:='%';
            dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
            dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
            dm.tblNalog.ParamByName('status').Value:='%';
            dm.tblNalog.FullRefresh;
            dm.tblPoNalogStavki.ParamByName('broj').Value:=dm.tblNalogBROJ.Value;
            dm.tblPoNalogStavki.ParamByName('godina').Value:=dm.tblNalogGODINA.Value;
            dm.tblPoNalogStavki.FullRefresh;
            if cxPageControl1.ActivePage = cxTabSheet1 then
               cxgrid1.SetFocus;
            cxLookupComboBox1.Text:='';
        end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.SCenaExit(Sender: TObject);
var pom1, pom2 :extended;
begin
     if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '')and (Panel9.Enabled = true) then
         begin
            dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (SDDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
         end;
     if (SCenaSoDDV.Text <> '') and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoNalogStavkiIIZNOS.Value:= dm.tblPoNalogStavkiCENADDV.Value * dm.tblPoNalogStavkiKOLICINA.Value;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.SCenaSoDDVExit(Sender: TObject);
var pom1, pom2 :extended;
begin
      if (StateActive in [dsEdit]) and (SCenaSoDDV.Text <> '') then
       begin
          dm.tblPoNalogStavkiCENA.Value:=dm.tblPoNalogStavkiCENADDV.Value*100/(100 + SDDV.EditValue);
       end;
      if (SCenaSoDDV.Text <> '') and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoNalogStavkiIIZNOS.Value:= dm.tblPoNalogStavkiCENADDV.Value * dm.tblPoNalogStavkiKOLICINA.Value;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.SDDVPropertiesEditValueChanged(
  Sender: TObject);
var pom1, pom2 :extended;
begin
     if (StateActive in [dsEdit]) and (SCena.Text <> '') and (SDDV.Text <> '') then
        begin
           dm.tblPoNalogStavkiCENADDV.Value:= dm.tblPoNalogStavkiCENA.Value + (SDDV.EditValue * dm.tblPoNalogStavkiCENA.Value/100);
        end;
     if (SCenaSoDDV.Text <> '') and (SKolicina.Text <> '') and (StateActive in [dsEdit]) then
         begin
            dm.tblPoNalogStavkiIIZNOS.Value:= dm.tblPoNalogStavkiCENADDV.Value * dm.tblPoNalogStavkiKOLICINA.Value;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.ZacuvajClickkExecute(
  Sender: TObject);
var pom, pom1, pom2,statusNabavka :integer;
begin
      if dm.Validacija(Panel8) = false then
         begin
            dm.tblNalogTIP_KONTAKT.Value:=vraboten;
            if dm.tblNalogNABAVKA_ID.AsVariant <> Null then
               begin
                  dm.tblNabavi.Locate('ID', dm.tblNalogNABAVKA_ID.Value, []);
                  statusNabavka:=dm.tblNabaviSTATUS.Value;
               end;
            if PPonuda.Text <> '' then
               begin
                  dm.tblNalogPONUDA_BROJ.Value:=dm.tblPonudiBROJ.Value;
                  dm.tblNalogPONUDA_GODINA.Value:=dm.tblPonudiGODINA.Value;
               end
             else
               begin
                  dm.tblNalogPONUDA_BROJ.AsVariant:=Null;
                  dm.tblNalogPONUDA_GODINA.AsVariant:=Null;
               end;
            if PNabavka.Text = '' then
               dm.tblNalogNABAVKA_ID.AsVariant:=Null;

            if (statusNabavka <> odobrena_nab) and (dm.tblNalogNABAVKA_ID.AsVariant <> Null)and (tag = 0) then
               begin
                  ShowMessage('��������� �� � �������� !!!');
                  PNabavka.SetFocus;
               end
            else
               begin
                  dm.tblNalog.Post;
                  dm.RestoreControls(panel8);
                  pom1:=dm.tblNalogGODINA.Value;
                  pom2:=dm.tblNalogBROJ.Value;
                  if Tag = 1 then
                    begin
                       dm.tblNalog.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
                       dm.tblNalog.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
                       dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
                       dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
                       dm.tblNalog.ParamByName('status').Value:='%';
                       dm.tblNalog.FullRefresh;
                     end
                  else if Tag = 2 then
                    begin
                       dm.tblNalog.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
                       dm.tblNalog.ParamByName('nabavkaID').Value:='%';
                       dm.tblNalog.ParamByName('ponudaBroj').Value:=dm.tblPonudiBROJ.Value;
                       dm.tblNalog.ParamByName('ponudaGodina').Value:=dm.tblPonudiGODINA.Value;
                       dm.tblNalog.ParamByName('status').Value:='%';
                       dm.tblNalog.FullRefresh;
                     end
                  else
                    begin
                       dm.tblNalog.ParamByName('godina').Value:=StrToInt(PGodina.Text);
                       dm.tblNalog.ParamByName('nabavkaID').Value:='%';
                       dm.tblNalog.ParamByName('ponudaBroj').Value:='%';
                       dm.tblNalog.ParamByName('ponudaGodina').Value:='%';
                       dm.tblNalog.ParamByName('status').Value:='%';
                       dm.tblNalog.FullRefresh;
                    end;
                  StateActive:=dsBrowse;
                  PStatus.Enabled:=true;
                  PGodina.Enabled:=True;
                  PBroj.Enabled:=True;
                  Panel8.Enabled:=False;
                  PNabavka.Enabled:=true;
                  dxRibbon1.Enabled:=true;
                  RibbonComboBox1.Text:=PGodina.Text;
                  cxPageControl2.Enabled:=true;
                  cxTabSheet1.Enabled:= true;
                  cxPageControl1.ActivePage:=cxTabSheet1;
                  cxGrid1.SetFocus;
                  dm.tblNalog.Locate('GODINA; BROJ', VarArrayOf([pom1, pom2]) , []);
               end;
         end;
end;

procedure TfrmEvidencijaNaNaloziZaIsporaka.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
          begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
          end;
        VK_UP:
          begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
          end;
        VK_RETURN:
          begin
             PostMessage(Handle, WM_NEXTDLGCTL,0,0);
          end;
       VK_INSERT:begin
          if sender = PPrevoz then
             begin
                frmVidPrevoz:=TfrmVidPrevoz.Create(Application);
                frmVidPrevoz.Tag:=1;
                frmVidPrevoz.ShowModal;
                frmVidPrevoz.Free;
                dm.tblNalogVID_PREVOZ.Value:=dm.PrevozID.Value;
                PTezina.SetFocus;
             end;
          if (sender = PKontaktNaziv) or (sender = DKontakt) then
             begin
                 frmPartner:=TfrmPartner.Create(Application);
                 frmPartner.Tag:=1;
                 frmPartner.ShowModal;
                 frmPartner.Free;
                 dm.PartnerVraboten.ParamByName('vraboten').Value:=vraboten;
                 dm.PartnerVraboten.FullRefresh;
                 PKontaktNaziv.Text:=dm.tblPartnerNAZIV.Value;
                 dm.tblNalogKONTAKT.Value:=dm.tblPartnerID.Value;
                 cxDBMemo1.SetFocus;
                 dm.tblPartner.ParamByName('tipPartner').Value:='%';
                 dm.tblPartner.FullRefresh;
             end;
          if (sender = PPartner) or (Sender = PPartnerID )then
             begin
                 frmPartner:=TfrmPartner.Create(Application);
                 frmPartner.Tag := 3;
                 frmPartner.ShowModal;
                 frmPartner.Free;
                 PPartner.Text:=dm.tblPartnerNAZIV.Value;
                 dm.tblNalogTIP_PARTNER.Value:= dm.tblPartnerTIP_PARTNER.Value;
                 dm.tblNalogPARTNER.Value:=dm.tblPartnerID.Value;
                 PPlakanje.SetFocus;
                 dm.tblPartner.ParamByName('tipPartner').Value:='%';
                 dm.tblPartner.FullRefresh;
             end;
          if ( sender = PTipPartner) then
             begin
                frmTipPartner:=TfrmTipPartner.Create(Application);
                frmTipPartner.Tag:=1;
                frmTipPartner.ShowModal;
                frmTipPartner.Free;
                dm.tblNalogTIP_PARTNER.Value:=dmMat.tblTipPartnerID.Value;
                PPartnerID.SetFocus;
             end;
        end;

    end;
end;

end.
