object frmDrvoNaReporti: TfrmDrvoNaReporti
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1080
  ClientHeight = 446
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 449
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 14
      Width = 280
      Height = 16
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1080#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1087#1088#1077#1075#1083#1077#1076'/'#1087#1077#1095#1072#1090#1077#1114#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 449
    Height = 405
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 1
    object dxDBTreeView1: TdxDBTreeView
      Left = 2
      Top = 2
      Width = 445
      Height = 382
      ShowNodeHint = True
      AutoExpand = True
      DataSource = dm.dsDrvoNaIzvestai
      DisplayField = 'NASLOV'
      KeyField = 'ID'
      ListField = 'NASLOV'
      ParentField = 'KOREN'
      RootValue = Null
      SeparatedSt = ' - '
      RaiseOnError = True
      ReadOnly = True
      Indent = 19
      Align = alClient
      ParentColor = False
      Options = [trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SelectedIndex = -1
      TabOrder = 0
      OnKeyDown = dxDBTreeView1KeyDown
    end
    object StatusBar1: TStatusBar
      Left = 2
      Top = 384
      Width = 445
      Height = 19
      Panels = <
        item
          Text = 'Enter - '#1048#1079#1073#1086#1088' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
          Width = 210
        end
        item
          Text = 'F1 - '#1055#1086#1084#1086#1096', Esc - '#1048#1079#1083#1077#1079
          Width = 50
        end>
    end
  end
  object ActionList1: TActionList
    Left = 400
    Top = 368
    object aIzlez: TAction
      Caption = 'aIzlez'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aPomos: TAction
      Caption = 'aPomos'
      ShortCut = 112
      OnExecute = aPomosExecute
    end
  end
end
