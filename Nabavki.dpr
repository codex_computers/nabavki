
program Nabavki;

uses
  Forms,
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  Utils in '..\Share2010\Utils.pas',
  AboutBox in '..\Share2010\AboutBox.pas' {frmAboutBox},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  Master in '..\Share2010\Master.pas' {frmMaster},
  ArtVid in '..\Share2010\ArtVid.pas' {frmArtVid},
  ArtGrupa in '..\Share2010\ArtGrupa.pas' {frmArtGrupa},
  Merka in '..\Share2010\Merka.pas' {frmMerka},
  Region in '..\Share2010\Region.pas' {frmRegion},
  Opstina in '..\Share2010\Opstina.pas' {frmOpstina},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  Drzava in '..\Share2010\Drzava.pas' {frmDrzava},
  Proizvoditel in '..\Share2010\Proizvoditel.pas' {frmProizvoditel},
  Artikal in '..\Share2010\Artikal.pas' {frmArtikal},
  Kontakt in '..\Share2010\Kontakt.pas' {frmKontakt},
  TipPartner in '..\Share2010\TipPartner.pas' {frmTipPartner},
  Partner in '..\Share2010\Partner.pas' {frmPartner},
  MainForm in 'MainForm.pas' {frmMain},
  ZabeleskaKontakt in '..\Share2010\ZabeleskaKontakt.pas' {frmZabeleskaKontakt},
  Login in '..\Share2010\Login.pas' {frmLogin},
  Ponudi in 'Ponudi.pas' {frmEvidencijaZaPonudi},
  IzborNaStavki in 'IzborNaStavki.pas' {frmIzborStavki},
  IzborNaArtikli in 'IzborNaArtikli.pas' {frmIzborArtikli},
  IzborNaStavkiZaNalog in 'IzborNaStavkiZaNalog.pas' {frmIzborNaStavkiNalog},
  VidPrevoz in 'VidPrevoz.pas' {frmVidPrevoz},
  Plan in 'Plan.pas' {frmPlan},
  RealizacijaPlanSektor in 'RealizacijaPlanSektor.pas' {frmGrafikRealPlan},
  EvidencijaNabavki in 'EvidencijaNabavki.pas' {frmNabavki},
  BaranjeZaPonuda in 'BaranjeZaPonuda.pas' {frmBaranjeZaPonuda},
  MKolicinaRealizacija in 'MKolicinaRealizacija.pas' {frmRealizacijaKolicina},
  TipNabavka in 'TipNabavka.pas' {frmTipNabavka},
  RabotniEdinici in '..\Share2010\RabotniEdinici.pas' {frmRE},
  Valuta in '..\Share2010\Valuta.pas' {frmValuta},
  SpecifikacijaNabavki in 'SpecifikacijaNabavki.pas' {FrmSpecifikacijaNabavka},
  PoNabavkaStavki in 'PoNabavkaStavki.pas' {frmPoNabavkaStavki},
  NaloziZaIsporaka in 'NaloziZaIsporaka.pas' {frmEvidencijaNaNaloziZaIsporaka},
  FormConfig in '..\Share2010\FormConfig.pas' {frmFormConfig},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  ListaNaPartneriPonuduvaci in 'ListaNaPartneriPonuduvaci.pas' {frmPartneriPonuduvaci},
  PregledPlanZaIzbranSektor in 'PregledPlanZaIzbranSektor.pas' {frmPlanZaizbranSektor},
  PregledSpecifikacijaNaPlan in 'PregledSpecifikacijaNaPlan.pas' {frmSpecifikacijaZaSiteSektori},
  PregledPlanZaSiteSektori in 'PregledPlanZaSiteSektori.pas' {frmPlanZaSiteSektori},
  PonudeniArtikli in 'PonudeniArtikli.pas' {frmPonudeniArtikli},
  Mapiranje in 'Mapiranje.pas' {frmGeneriki},
  PregledPoTipNabavka in 'PregledPoTipNabavka.pas' {frmPregledPoTipNabavka},
  SETUP in 'SETUP.pas' {frmSetup},
  KorisniciZaOdobruvanje in 'KorisniciZaOdobruvanje.pas' {frmKorisniciZaOdobruvanje},
  MK in '..\Share2010\MK.pas' {frmMK},
  PregledGodisenPlan in 'PregledGodisenPlan.pas' {frmGodisenPlan};

{$R *.res}

begin

//  Application.Run;

Application.Initialize;
  //Application.HelpFile := 'D:\Codis\HRM\HRM_Help.hlp';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='PO';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(Tdm, dm);
  Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;

end.
