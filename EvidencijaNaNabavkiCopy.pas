unit EvidencijaNaNabavkiCopy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxPC, cxControls,
  RibbonLunaStyleActnCtrls, Ribbon, dxStatusBar, dxRibbonStatusBar, ExtCtrls,
  cxGraphics, StdCtrls, cxDBEdit, cxDropDownEdit, cxCalendar, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ActnList, ToolWin, ActnMan,
  ActnCtrls, RibbonActnCtrls, ActnMenus, RibbonActnMenus, cxGridExportLink, cxExport,
  RibbonSilverStyleActnCtrls, RibbonObsidianStyleActnCtrls, ComCtrls,
  cxDBExtLookupComboBox, DBCtrls, cxMemo, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinSeven, dxSkinSharp, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver;

type
  TfrmNabavki = class(TForm)
    Ribbon1: TRibbon;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ActionList1: TActionList;
    RibbonPage1: TRibbonPage;
    ActionManager1: TActionManager;
    aDodadi: TAction;
    aAzuriraj: TAction;
    aStavki: TAction;
    aPonudi: TAction;
    aNalozi: TAction;
    cxGrid1DBTableView1PREDMET: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KREIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1NazivStatus: TcxGridDBColumn;
    cxGrid1DBTableView1Iznos: TcxGridDBColumn;
    aPrebaraj: TAction;
    cxGrid1DBTableView1RabEdinica: TcxGridDBColumn;
    aBrisi: TAction;
    Panel2: TPanel;
    Label15: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DPredmet: TcxDBTextEdit;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label25: TLabel;
    Otkazi: TButton;
    Zacuvaj: TButton;
    aOsvezi: TAction;
    aPoNabavkaStavki: TAction;
    DBroj: TcxDBTextEdit;
    DRabEdinID: TcxDBTextEdit;
    DRabEdinica: TcxDBLookupComboBox;
    DGodina: TcxDBComboBox;
    DTipNabavkaID: TcxDBTextEdit;
    DTipNabavka: TcxDBLookupComboBox;
    DStatus: TcxDBLookupComboBox;
    DPrioritet: TcxDBLookupComboBox;
    DTipKontakt: TcxDBTextEdit;
    DKontakt: TcxDBTextEdit;
    DPartner: TcxExtLookupComboBox;
    aSnimiIzgled: TAction;
    aIzlez: TAction;
    RibbonGroup2: TRibbonGroup;
    RibbonGroup3: TRibbonGroup;
    RibbonGroup4: TRibbonGroup;
    RibbonGroup5: TRibbonGroup;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1BrNabavka: TcxGridDBColumn;
    cxGrid1DBTableView1Broj: TcxGridDBColumn;
    aZacuvajVoExcel: TAction;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    RibbonGroup1: TRibbonGroup;
    Action1: TAction;
    RibbonApplicationMenuBar1: TRibbonApplicationMenuBar;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label10: TLabel;
    DDatumPonudaDo: TcxDBDateEdit;
    DDatumPonudaOd: TcxDBDateEdit;
    GroupBox2: TGroupBox;
    Label26: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label11: TLabel;
    DIznosDenari: TcxDBTextEdit;
    DIznosPotrosen: TcxDBTextEdit;
    DIznos: TcxDBTextEdit;
    DValuta: TcxDBLookupComboBox;
    DKurs: TcxDBTextEdit;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    DDatumVazenje: TcxDBDateEdit;
    Label33: TLabel;
    DDatumKreirana: TcxDBTextEdit;
    Label14: TLabel;
    DOpis: TcxDBMemo;
    Label23: TLabel;
    DZabeleska: TcxDBMemo;
    Label22: TLabel;
    DUslovi: TcxDBMemo;
    DKreiral: TcxDBTextEdit;
    Label18: TLabel;
    RibbonGroup6: TRibbonGroup;
    aPecati: TAction;
    �SpecifikacijaZaOdbranaNabavka: TAction;
    aPonudiZaNabavka: TAction;
    aPonudiZaNabavkaPoArtikli: TAction;
    aNajdobriPonudi: TAction;
    aBaranjeZaPonuda: TAction;
    RibbonGroup7: TRibbonGroup;
    aPomos: TAction;
    aRealizacijaKolicina: TAction;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    RabEdinica: TcxLookupComboBox;
    TipNabavka: TcxLookupComboBox;
    PredmetNabavka: TcxTextEdit;
    BrojNabavka: TcxTextEdit;
    DatumKreiran: TcxDateEdit;
    ButtonClear: TButton;
    Action2: TAction;
    RibbonGroup8: TRibbonGroup;
    aOdobriNabavka: TAction;
    cxLookupComboBox1: TcxLookupComboBox;
    RibbonComboBox1: TcxComboBox;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PONUDA_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PONUDA_DO: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKA1: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1KREIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid1DBTableView1KURS: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITET: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1VREME: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITETNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POTEKLO: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_DENARI: TcxGridDBColumn;
    cxGrid1DBTableView1ODOBRIL: TcxGridDBColumn;
    Label24: TLabel;
    DATUM_ODOBRUVANJE: TcxDBDateEdit;
    cxGrid1DBTableView1DATUM_ODOBRUVANJE: TcxGridDBColumn;
    procedure aDodadiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aStavkiExecute(Sender: TObject);
    procedure aPonudiExecute(Sender: TObject);
    procedure aNaloziExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPrebarajExecute(Sender: TObject);
    procedure Prebaraj(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OtvoriTabeli();
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OtkaziClick(Sender: TObject);
    procedure ZacuvajClick(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aPoNabavkaStavkiExecute(Sender: TObject);
    procedure DGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure DRabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure DRabEdinIDPropertiesEditValueChanged(Sender: TObject);
    procedure DTipNabavkaPropertiesEditValueChanged(Sender: TObject);
    procedure DTipNabavkaIDPropertiesEditValueChanged(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure DPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure DTipKontaktPropertiesEditValueChanged(Sender: TObject);
    procedure DKontaktPropertiesEditValueChanged(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure ButtonClearClick(Sender: TObject);
    procedure DGodinaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure �SpecifikacijaZaOdbranaNabavkaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPonudiZaNabavkaExecute(Sender: TObject);
    procedure aPonudiZaNabavkaPoArtikliExecute(Sender: TObject);
    procedure aNajdobriPonudiExecute(Sender: TObject);
    procedure aBaranjeZaPonudaExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure aRealizacijaKolicinaExecute(Sender: TObject);
    procedure aOdobriNabavkaExecute(Sender: TObject);
    procedure RibbonComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure DKontaktExit(Sender: TObject);
    procedure DPartnerExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    protected
    prva, posledna :TWinControl;
  end;

var
  frmNabavki: TfrmNabavki;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija,  dmMaticni, dmUnit, PoNabavkaStavki,
  SpecifikacijaNabavki, Ponudi, NaloziZaIsporaka, RabotniEdinici, Kontakt,
  Valuta, TipNabavka, Partner, SifrarnikPartner, Utils, BaranjeZaPonuda,
  MKolicinaRealizacija;

{$R *.dfm}

procedure TfrmNabavki.aAzurirajExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//        begin
//           if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false)  then
//              ShowMessage('������ ����� �� ��������� �������� ������� !!!')
//           else
//              begin
//                 cxPageControl1.ActivePage:=cxTabSheet2;
//                 Panel2.Enabled:= True;
//                 cxLookupComboBox1.Text:='';
//                 Ribbon1.Enabled:=False;
//                 DStatus.Enabled:=True;
//                 DStatus.SetFocus;
//                 cxTabSheet1.Enabled:=false;
//
//                 DPartner.Text:=dm.tblNabaviPARTNERNAZIV.Value;
//                 cxGrid1DBTableView1.DataController.DataSet.Edit;
//                 StateActive:=dsEdit;
//              end;
//        end
//     else ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aBaranjeZaPonudaExecute(Sender: TObject);
begin
//     frmBaranjeZaPonuda:=TfrmBaranjeZaPonuda.Create(Application);
//     frmBaranjeZaPonuda.ShowModal;
//     frmBaranjeZaPonuda.Free;
end;

procedure TfrmNabavki.aBrisiExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//        begin
//           if (odobriNabavka = false ) and (dm.tblNabaviSTATUS.Value = odobrena_nab) then
//              ShowMessage('������ ����� �� ������� �������� ������� !!!')
//           else
//              begin
//                 frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
//                 if frmDaNe.ShowModal =mrYes then
//                   dm.tblNabavi.Delete;
//              end;
//        end
//     else ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.Action1Execute(Sender: TObject);
begin
//     dm.tblNabavi.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
//     dm.tblNabavi.ParamByName('status').Value:='%';
//     dm.tblNabavi.FullRefresh;
//     cxLookupComboBox1.Text:='';
//     cxGrid1.SetFocus
end;

procedure TfrmNabavki.aDodadiExecute(Sender: TObject);
begin
//     cxPageControl1.ActivePage:=cxTabSheet2;
//     Panel2.Enabled:= True;
//
//     cxLookupComboBox1.Text:='';
//     Ribbon1.Enabled:=False;
//
//     DTipNabavkaID.SetFocus;
//     cxTabSheet1.Enabled:=false;
//
//     DPartner.Text:='';
//     DRabEdinica.EditValue:=dmKon.re;
//     DRabEdinID.Text:=IntToStr(dmKon.re);
//
//     dm.tblNabavi.Insert;
//     StateActive:=dsInsert;
//
//     dm.tblNabaviGODINA.Value:=StrToInt(RibbonComboBox1.Text);
//     dm.tblNabaviID.Value:=dm.zemiMaxIDNabavka(dm.MaxIDNabavka, Null, Null, Null, Null, Null,Null, 'MAXBR');
//     dm.tblNabaviBROJ.Value:=dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR');
//     dm.tblNabaviRE.Value:= dmKon.re;
//     if avtomatsko_odobruvanje = '0' then
//        dm.tblNabaviSTATUS.Value:= otvorena_nab
//     else if avtomatsko_odobruvanje = '1' then
//        begin
//            dm.tblNabaviSTATUS.Value:= odobrena_nab;
//            dm.tblNabaviODOBRIL.Value:=dmKon.user;
//            dm.tblNabaviDATUM_ODOBRUVANJE.Value:=Now;
//        end;
//     dm.tblNabaviVALUTA.Value:='DEN';
//     dm.tblNabaviPRIORITET.Value:=1;
end;

procedure TfrmNabavki.aIzlezExecute(Sender: TObject);
begin
//     if (StateActive in [dsBrowse]) and (cxPageControl1.ActivePage = cxTabSheet2) then
//         cxPageControl1.ActivePage:=cxTabSheet1
//     else if (StateActive in [dsBrowse]) and (cxPageControl1.ActivePage = cxTabSheet1) then
//          close
//     else if StateActive in [dsedit,dsInsert] then
//          begin
//             frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
//             if frmDaNe.ShowModal =mrYes then
//                begin
//                   dm.tblNabavi.Cancel;
//                   dm.RestoreControls(panel2);
//                   StateActive:=dsBrowse;
//                   cxTabSheet1.Enabled:=True;
//                   Panel2.Enabled:=false;
//                   DStatus.Enabled:=False;
//                   cxPageControl1.ActivePage:=cxTabSheet1;
//                   Ribbon1.Enabled:=true;
//                   cxGrid1.SetFocus;
//                end;
//          end;
end;

procedure TfrmNabavki.aNajdobriPonudiExecute(Sender: TObject);
var RptStream :TStream;
begin
//     dm.tblReportDizajn.ParamByName('id').Value:=4010;
//     dm.tblReportDizajn.FullRefresh;
//
//     dm.NajdobriPonudi.Close;
//     dm.NajdobriPonudi.ParamByName('re').Value:=dmKon.re;
//     dm.NajdobriPonudi.ParamByName('id').Value:=dm.tblNabaviID.Value;
//     dm.NajdobriPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
//     dm.NajdobriPonudi.Open;
//
//     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
//     dm.frxReport.LoadFromStream(RptStream) ;
//
////     dm.frxReport.DesignReport();
//     dm.frxReport.ShowReport;

end;

procedure TfrmNabavki.aNaloziExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//        begin
//                   frmEvidencijaNaNaloziZaIsporaka:=TfrmEvidencijaNaNaloziZaIsporaka.Create(Application);
//                   frmEvidencijaNaNaloziZaIsporaka.Tag:=1;
//                   frmEvidencijaNaNaloziZaIsporaka.ShowModal;
//                   frmEvidencijaNaNaloziZaIsporaka.Free;
//        end
//      else  ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aOdobriNabavkaExecute(Sender: TObject);
var pom, pom2:integer;
begin
//     pom2:=dm.zemiBr4(dm.CountNalogPonudaNab, 'NABAVKAID', Null, Null,Null, dm.tblnabaviID.Value, Null, Null, Null, 'COUNTT');
//     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//        begin
//           if odobriNabavka = true then
//              begin
//                 pom:=dm.tblNabaviSTATUS.Value;
//                 if pom <> odobrena_nab then
//                    begin
//                       frmDaNe:=TfrmDaNe.Create(self,'������� !', '���� ��� ������� ���� ������ �� �� �������� ���������?',1);
//                       if frmDaNe.ShowModal =mrYes then
//                          begin
//                             dm.tblNabavi.Edit;
//                             dm.tblNabaviSTATUS.Value:=odobrena_nab;
//                             dm.tblNabaviodobril.value:=dmKon.user;
//                             dm.tblNabaviDATUM_ODOBRUVANJE.value:=Now;
//                             dm.tblNabavi.Post;
//                          end;
//                    end
//                  else if (pom = odobrena_nab) and (pom2 = 0)then
//                    begin
//                       frmDaNe:=TfrmDaNe.Create(self,'������� !','���� ��� ������� ���� ������ �� �� �������� ���������?',1);
//                       if frmDaNe.ShowModal =mrYes then
//                          begin
//                             dm.tblNabavi.Edit;
//                             dm.tblNabaviSTATUS.Value:=otvorena_nab;
//                             dm.tblNabaviodobril.value:=dmKon.user;
//                             dm.tblNabaviDATUM_ODOBRUVANJE.value:=Now;
//                             dm.tblNabavi.Post;
//                          end;
//                    end
//                 else ShowMessage('�������� �� �������� �������� ������� �� ��� ��� ��� ������������ ������ ��� ����� !!!');
//                 dm.tblNabavi.Refresh;
//              end
//           else ShowMessage('������ ����� �� ���������� ������� !!!');
//        end
//     else ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aOsveziExecute(Sender: TObject);
begin
//     dm.tblNabavi.Refresh;
end;

procedure TfrmNabavki.aPecatiExecute(Sender: TObject);
begin
//     Close;
end;

procedure TfrmNabavki.aPomosExecute(Sender: TObject);
begin
//     Application.HelpContext(105);
end;

procedure TfrmNabavki.aPoNabavkaStavkiExecute(Sender: TObject);
begin
//     dm.tblPoNabavkaStavki.Close;
//     dm.tblPoNabavkaStavki.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
//     dm.tblPoNabavkaStavki.Open;
//     frmPoNabavkaStavki:=TfrmPoNabavkaStavki.Create(Application);
//     frmPoNabavkaStavki.ShowModal;
//     frmPoNabavkaStavki.Free;
end;

procedure TfrmNabavki.aPonudiExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//        begin
//           frmEvidencijaZaPonudi:=TfrmEvidencijaZaPonudi.Create(Application);
//           frmEvidencijaZaPonudi.Tag:=1;
//           frmEvidencijaZaPonudi.ShowModal;
//           frmEvidencijaZaPonudi.Free;
//        end
//     else
//        begin
//           ShowMessage('����������� ������� !!!');
//        end;
end;

procedure TfrmNabavki.aPonudiZaNabavkaExecute(Sender: TObject);
var RptStream :TStream;
begin
//     dm.tblReportDizajn.ParamByName('id').Value:=4008;
//     dm.tblReportDizajn.FullRefresh;
//
//     dm.tblPonudi.Close;
//     dm.tblPonudi.ParamByName('re').Value:=dmKon.re;
//     dm.tblPonudi.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
//     dm.tblPonudi.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
//     dm.tblPonudi.ParamByName('status').Value:='%';
//     dm.tblPonudi.Open;
//
//     dm.PonudiDetail.Close;
//     dm.PonudiDetail.Open;
//
//     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
//     dm.frxReport.LoadFromStream(RptStream) ;
//
////     dm.frxReport.DesignReport();
//     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.aPonudiZaNabavkaPoArtikliExecute(Sender: TObject);
var RptStream :TStream;
begin
//     dm.tblReportDizajn.ParamByName('id').Value:=4009;
//     dm.tblReportDizajn.FullRefresh;
//
//     dm.PonudiPoArtikli.Close;
//     dm.PonudiPoArtikli.ParamByName('nabavkaID').Value:=dm.tblNabaviID.Value;
//     dm.PonudiPoArtikli.Open;
//
//     dm.tblStavkiZaOdbranaPonuda.Close;
//     dm.tblStavkiZaOdbranaPonuda.ParamByName('godina').Value:=dm.tblPonudiGODINA.Value;
//     dm.tblStavkiZaOdbranaPonuda.ParamByName('broj').Value:=dm.tblPonudiBROJ.Value;
//     dm.tblStavkiZaOdbranaPonuda.Open;
//
//     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
//     dm.frxReport.LoadFromStream(RptStream) ;
//
////     dm.frxReport.DesignReport();
//     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.aPrebarajExecute(Sender: TObject);
begin
//     cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
//     try
//        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1RabEdinica, foLike, '%' + RabEdinica.EditText + '%' , RabEdinica.EditText + '%') ;
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1TIP_NABAVKA, foLike, '%' + TipNabavka.EditingText + '%', TipNabavka.EditingText + '%');
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_KREIRAN, foLike, '%'+ DatumKreiran.Text+ '%',DatumKreiran.Text + '%' );
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1PREDMET, foLike, '%'+ PredmetNabavka.Text+ '%',PredmetNabavka.Text + '%' );
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1BrNabavka, foLike,  BrojNabavka.Text + '%', BrojNabavka.Text + '%');
//        cxGrid1DBTableView1.DataController.Filter.Active:=True;
//     finally
//        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
//     end;
end;

procedure TfrmNabavki.aRealizacijaKolicinaExecute(Sender: TObject);
begin
//     frmRealizacijaKolicina:=TfrmRealizacijaKolicina.Create(Application);
//     frmRealizacijaKolicina.ShowModal;
//     frmRealizacijaKolicina.free;
end;

procedure TfrmNabavki.aSnimiIzgledExecute(Sender: TObject);
begin
//     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmNabavki.aStavkiExecute(Sender: TObject);
begin
//     if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//        begin
//           if dm.tblNabaviSTATUS.Value = stornirana_nab then
//              begin
//                 ShowMessage('��������� � ���������� !!!');
//              end
//            else if dm.tblNabaviSTATUS.Value = zatvorena_nab then
//               begin
//                  ShowMessage('��������� � ��������� !!!');
//               end
//            else if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false) then
//               begin
//                  ShowMessage('������ ����� �� ��������/��������� ������ �� �������� ������� !!!');
//               end
//            else
//              begin
//                 dm.tblPoNabavkaStavki.Close;
//                 dm.tblPoNabavkaStavki.ParamByName('NabavkaID').Value:=dm.tblNabaviID.Value;
//                 dm.tblPoNabavkaStavki.Open;
//                 FrmSpecifikacijaNabavka:=TFrmSpecifikacijaNabavka.Create(Application);
//                 FrmSpecifikacijaNabavka.ShowModal;
//                 FrmSpecifikacijaNabavka.Free;
//              end;
//         end
//       else ShowMessage('����������� ������� !!!');
end;

procedure TfrmNabavki.aZacuvajVoExcelExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmNabavki.ButtonClearClick(Sender: TObject);
begin
//     RabEdinica.Text:= '';
//     TipNabavka.Text:= '';
//     PredmetNabavka.Text:= '';
//     DatumKreiran.Text:= '';
//     BrojNabavka.Text:= '';
//     cxGrid1DBTableView1.DataController.Filter.Clear;
//     cxGrid1.SetFocus;
end;
procedure TfrmNabavki.OtkaziClick(Sender: TObject);
begin
//     if StateActive in [dsEdit,dsInsert] then
//        begin
//           frmDaNe:=TfrmDaNe.Create(self,'������','���� ��� ������� ���� ������ �� �� �������� ������� �� ���������� ?',1);
//           if frmDaNe.ShowModal =mrYes then
//              begin
//                 dm.tblNabavi.Cancel;
//                 StateActive:=dsBrowse;
//                 dm.RestoreControls(panel2);
//
//                 cxTabSheet1.Enabled:=True;
//                 Panel2.Enabled:=false;
//                 DStatus.Enabled:=False;
//                 cxPageControl1.ActivePage:=cxTabSheet1;
//                 Ribbon1.Enabled:=true;
//                 cxGrid1.SetFocus;
//              end;
//          end;
end;

procedure TfrmNabavki.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//     case Key of
//          VK_F5:begin
//                   cxPageControl1.ActivePage:=cxTabSheet2;
//                   Panel2.Enabled:= True;
//                   cxLookupComboBox1.Text:='';
//                   Ribbon1.Enabled:=False;
//
//                   DTipNabavkaID.SetFocus;
//                   cxTabSheet1.Enabled:=false;
//
//                   DPartner.Text:='';
//                   DRabEdinica.EditValue:=dmKon.re;
//                   DRabEdinID.Text:=IntToStr(dmKon.re);
//
//                   dm.tblNabavi.Insert;
//                   StateActive:=dsInsert;
//
//                   dm.tblNabaviGODINA.Value:=StrToInt(RibbonComboBox1.Text);
//                   dm.tblNabaviID.Value:=dm.zemiMaxIDNabavka(dm.MaxIDNabavka, Null, Null, Null, Null, Null,Null, 'MAXBR');
//                   dm.tblNabaviBROJ.Value:=dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR');
//                   dm.tblNabaviRE.Value:= dmKon.re;
//                   if avtomatsko_odobruvanje = '0' then
//                      dm.tblNabaviSTATUS.Value:= otvorena_nab
//                   else if avtomatsko_odobruvanje = '1' then
//                     begin
//                      dm.tblNabaviSTATUS.Value:= odobrena_nab;
//                      dm.tblNabaviODOBRIL.Value:=dmKon.user;
//                      dm.tblNabaviDATUM_ODOBRUVANJE.Value:=Now;
//                     end;
//                   dm.tblNabaviVALUTA.Value:='DEN';
//                   dm.tblNabaviPRIORITET.Value:=1;
//                end;
//
//          VK_F6:begin
//                   if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
//                     begin
//                        if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false)  then
//                           ShowMessage('������ ����� �� ��������� �������� ������� !!!')
//                        else
//                          begin
//                             cxPageControl1.ActivePage:=cxTabSheet2;
//                             Panel2.Enabled:= True;
//                             cxLookupComboBox1.Text:='';
//                             Ribbon1.Enabled:=False;
//                             DStatus.Enabled:=True;
//                             DStatus.SetFocus;
//                             cxTabSheet1.Enabled:=false;
//
//                             DPartner.Text:=dm.tblNabaviPARTNERNAZIV.Value;
//                             cxGrid1DBTableView1.DataController.DataSet.Edit;
//                             StateActive:=dsEdit;
//                          end;
//                      end
//                    else ShowMessage('����������� ������� !!!');
//                end;
//
//            VK_F7: dm.tblNabavi.Refresh;
//
//            VK_F8 : begin
//                       if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//                         begin
//                            if (odobriNabavka = false ) and (dm.tblNabaviSTATUS.Value = odobrena_nab) then
//                                ShowMessage('������ ����� �� ������� �������� ������� !!!')
//                            else
//                                 begin
//                                    frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
//                                    if frmDaNe.ShowModal =mrYes then
//                                      dm.tblNabavi.Delete;
//                                 end;
//                          end
//                       else ShowMessage('����������� ������� !!!');
//                    end;
//
//            VK_INSERT:begin
//                           if cxGrid1DBTableView1.Controller.SelectedRowCount = 1 then
//                             begin
//                                if dm.tblNabaviSTATUS.Value = stornirana_nab then
//                                   begin
//                                      ShowMessage('��������� � ���������� !!!');
//                                   end
//                                else if dm.tblNabaviSTATUS.Value = zatvorena_nab then
//                                   begin
//                                      ShowMessage('��������� � ��������� !!!');
//                                   end
//                                else if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = false) then
//                                   begin
//                                      ShowMessage('������ ����� �� ��������/��������� ������ �� �������� ������� !!!');
//                                   end
//                                else
//                                   begin
//                                      dm.tblPoNabavkaStavki.Close;
//                                      dm.tblPoNabavkaStavki.ParamByName('NabavkaID').Value:=dm.tblNabaviID.Value;
//                                      dm.tblPoNabavkaStavki.Open;
//                                      FrmSpecifikacijaNabavka:=TFrmSpecifikacijaNabavka.Create(Application);
//                                      FrmSpecifikacijaNabavka.ShowModal;
//                                      FrmSpecifikacijaNabavka.Free;
//                                   end;
//                             end
//                          else ShowMessage('����������� ������� !!!');
//                      end;
//            end;
end;

procedure TfrmNabavki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmNabavki.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
//     if cxLookupComboBox1.Text <> '' then
//        begin
//           dm.tblNabavi.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
//           dm.tblNabavi.ParamByName('status').Value:=cxLookupComboBox1.EditValue;
//           dm.tblNabavi.FullRefresh;
//           cxGrid1.SetFocus
//        end;
end;

procedure TfrmNabavki.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
//      if ( StateActive in [dsBrowse] ) and (cxPageControl1.ActivePage = cxTabSheet1)then
//         begin
//            DPartner.Text:=dm.tblNabaviPARTNERNAZIV.Value;
//            RibbonComboBox1.Enabled:=false;
//            cxLookupComboBox1.Enabled:=False;
//            Action1.Enabled:=False;
//         end;
//      if (cxPageControl1.ActivePage = cxTabSheet2)then
//         begin
//            RibbonComboBox1.Enabled:=true;
//            cxLookupComboBox1.Enabled:=true;
//            Action1.Enabled:=true;
//         end;
end;

procedure TfrmNabavki.DGodinaExit(Sender: TObject);
begin
     DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'));
end;

procedure TfrmNabavki.DGodinaPropertiesEditValueChanged(Sender: TObject);
begin
//     if (StateActive in [dsEdit]) and (DGodina.Text <> '') then
//        begin
//           if dm.tblNabaviGODINA.Value <> StrToInt(DGodina.text) then
//              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'))
//           else
//              DBroj.Text:=IntToStr(dm.tblNabaviBROJ.Value);
//         end;
//
//       if (StateActive in [dsInsert])and (DGodina.Text <> '') then
//           begin
//              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'));
//           end;
end;

procedure TfrmNabavki.DKontaktExit(Sender: TObject);
var pom:integer;
begin
//     if (StateActive in [dsInsert,dsEdit]) and (DKontakt.Text <> '') then
//         begin
//            if (DKontakt.Text <> '') then
//               pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, vraboten,strtoInt(DKontakt.text), Null, 'BROJ');
//            if (pom = 1)then
//               begin
//                  dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, strtoInt(DKontakt.Text)]) , []);
//                  DPartner.Text:= dm.PartnerVrabotenNAZIV.Value;
//               end;
//         end;
end;

procedure TfrmNabavki.DKontaktPropertiesEditValueChanged(Sender: TObject);
var pom:integer;
begin
     if (StateActive in [dsInsert,dsEdit]) and (DKontakt.Text <> '') then
         begin
            if (DKontakt.Text <> '') then
               pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, vraboten,strtoInt(DKontakt.text), Null, 'BROJ');
            if (pom = 1)then
               begin
                  dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([vraboten, strtoInt(DKontakt.Text)]) , []);
                  DPartner.Text:= dm.PartnerVrabotenNAZIV.Value;
               end;
         end;
end;

procedure TfrmNabavki.DPartnerExit(Sender: TObject);
begin
//     if (StateActive in [dsInsert,dsEdit]) and (DPartner.Text <> '') then
//        begin
//           if DPartner.Text <> '' then
//              begin
//                 dm.tblNabaviTIP_KONTAKT.Value:=dm.PartnerVrabotenTIP_PARTNER.Value;
//                 dm.tblNabaviKONTAKT.Value:=dm.PartnerVrabotenID.Value;
//              end;
//        end;
end;

procedure TfrmNabavki.DPartnerPropertiesEditValueChanged(Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) and (DPartner.Text <> '') then
        begin
           //ShowMessage('sssss');
           if DPartner.Text <> '' then
              begin
                 dm.tblNabaviTIP_KONTAKT.Value:=dm.PartnerVrabotenTIP_PARTNER.Value;
                 dm.tblNabaviKONTAKT.Value:=dm.PartnerVrabotenID.Value;
              end;
        end;
end;

procedure TfrmNabavki.DRabEdinicaPropertiesEditValueChanged(Sender: TObject);
var pom :integer;
begin
//     if (StateActive in [dsEdit]) and (DRabEdinica.Text <>'') then
//        begin
//           if dm.tblNabaviRE.Value <> DRabEdinica.EditValue then
//              DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'))
//           else
//              DBroj.Text:=IntToStr(dm.tblNabaviBROJ.Value);
//              DRabEdinID.Text:=IntToStr(DRabEdinica.EditValue);
//          end;
//     if (StateActive in [dsInsert]) and (DRabEdinica.Text <>'') then
//          begin
//             DBroj.Text:=IntToStr(dm.zemiMaxBrojNabavka(dm.MaxBrojNabavka,'GODINA','RE', Null, StrToInt(DGodina.Text), DRabEdinica.EditValue, Null, 'MAXBR'));
//             DRabEdinID.Text:=IntToStr(DRabEdinica.EditValue);
//          end;
end;

procedure TfrmNabavki.DRabEdinIDPropertiesEditValueChanged(Sender: TObject);
begin
//     if (StateActive in [dsInsert,dsEdit]) and (DRabEdinID.Text <> '') then
//        begin
//           DRabEdinica.EditValue:=StrToInt(DRabEdinID.Text);
//        end;
end;

procedure TfrmNabavki.DTipKontaktPropertiesEditValueChanged(Sender: TObject);
begin
     if StateActive in [dsInsert,dsEdit] then
        begin
           if (DTipKontakt.Text <> '') and (DKontakt.Text <> '')then
              begin
                 dm.PartnerVraboten.Locate('TIP_PARTNER; ID', VarArrayOf([strtoInt(DTipKontakt.Text), strtoInt(DKontakt.Text)]) , []);
                 DPartner.Text:= dm.PartnerVrabotenNAZIV.Value;
              end;
        end;
end;

procedure TfrmNabavki.DTipNabavkaIDPropertiesEditValueChanged(Sender: TObject);
begin
//     if (StateActive in [dsInsert,dsEdit]) and (DTipNabavkaID.Text <> '') then
//        begin
//           DTipNabavka.EditValue:=StrToInt(DTipNabavkaID.Text);
//        end;
end;

procedure TfrmNabavki.DTipNabavkaPropertiesEditValueChanged(Sender: TObject);
begin
//     if (StateActive in [dsInsert,dsEdit]) and (DTipNabavka.Text <> '') then
//        begin
//           if DTipNabavka.Text <> '' then
//              DTipNabavkaID.Text:= IntToStr(DTipNabavka.EditValue);
//        end;
end;


procedure TfrmNabavki.OtvoriTabeli();
begin
//       dm.tblNabavi.ParamByName('godina').Value:=dmKon.godina;
//       dm.tblNabavi.ParamByName('status').Value:='%';
//       dm.tblNabavi.ParamByName('poteklo').Value:=inttostr(dmKon.re)+ ',';
//       dm.tblNabavi.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
//       dm.tblNabavi.Open;
//       dm.TipNabavki.Open;
//       dmMat.tblRE.Open;
//       dm.StatusNabavka.Open;
//       dm.Prioritet.Open;
//       dmMat.tblValuta.Open;
//       dm.PartnerVraboten.ParamByName('vraboten').Value:=vraboten;
//       dm.PartnerVraboten.Open;
//       dm.IzbraniSektoriZaPlan.close;
//       dm.IzbraniSektoriZaPlan.ParamByName('poteklo').Value:=intToStr(dmKon.re) + ',';
//       dm.IzbraniSektoriZaPlan.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';
//      // dm.IzbraniSektoriZaPlan.ParamByName('user').Value:=dmKon.user;
//       dm.IzbraniSektoriZaPlan.Open;
//
//       dm.Firmi.close;
//       dm.Firmi.ParamByName('re').Value:=dmKon.re;
//       dm.Firmi.Open;
//       dm.tblReportDizajn.Open;
//       dm.tblSpecifikacijaNaNabavka.Open;
end;

procedure TfrmNabavki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//       dm.tblNabavi.Close;
//       dm.TipNabavki.Close;
//       dmMat.tblRE.Close;
//
//       dm.StatusNabavka.Close;
//       dm.Prioritet.Close;
//       dmMat.tblValuta.Close;
//       dm.PartnerVraboten.Close;
end;

procedure TfrmNabavki.FormCreate(Sender: TObject);
begin
//       OtvoriTabeli();
//       cxPageControl1.ActivePage:=cxTabSheet1;
//
//       dm.tblSysFrmConf.Close;
//       dm.tblSysFrmConf.Open;
//
//       dm.tblSysFrmConf.Locate('USERID; APPS; FRM; OBJECT; PARAM', VarArrayOf([dmKon.user, 'PO', 'frmNabavki', 'aOdobriNabavka', 'Enabled']), []);
//       odobriNabavka:= dm.tblSysFrmConfVAL.AsBoolean;
end;

procedure TfrmNabavki.FormShow(Sender: TObject);
begin
//       dm.frxReport.Script.Clear;
//       dm.frxReport.Clear;
//       StateActive:=dsBrowse;
//       procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
//       RibbonComboBox1.Text:=IntToStr(dmKon.godina);
//       cxGrid1.SetFocus;
end;

procedure TfrmNabavki.Prebaraj(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//      case Key of
//        VK_UP:begin
//              PostMessage(Handle,WM_NextDlgCtl,1,0);
//        end;
//        VK_DOWN:begin
//             PostMessage(Handle,WM_NextDlgCtl,0,0);
//        end;
//          VK_RETURN:begin
//             aPrebarajExecute(Sender);
//             if Sender = BrojNabavka then
//                cxGrid1.SetFocus
//             else
//                PostMessage(Handle,WM_NextDlgCtl,0,0);
//          end;
//      end;
end;

procedure TfrmNabavki.RibbonComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
//     dm.tblNabavi.ParamByName('godina').Value:=StrToInt(RibbonComboBox1.Text);
//     dm.tblNabavi.ParamByName('status').Value:='%';
//     dm.tblNabavi.FullRefresh;
//
//     cxGrid1.SetFocus;
//     cxLookupComboBox1.Text:='';
end;

procedure TfrmNabavki.ZacuvajClick(Sender: TObject);
var pom:string;
begin
//      if dm.Validacija(Panel2) = false then
//        begin
//           if (dm.tblNabaviSTATUS.Value = odobrena_nab) and (odobriNabavka = False) then
//              begin
//                 ShowMessage('������ ����� �� ���������� ������� !!!');
//                 DStatus.SetFocus;
//              end
//           else
//              begin
//                 pom:=DGodina.text;
//                 if dm.tblNabaviSTATUS.Value = odobrena_nab then
//                    dm.tblNabaviodobril.value:=dmKon.user;
//                 cxGrid1DBTableView1.DataController.DataSet.Post;
//                 StateActive:=dsBrowse;
//                 dm.RestoreControls(panel2);
//                 dm.tblNabavi.ParamByName('status').Value:='%';
//                 dm.tblNabavi.FullRefresh;
//                 cxTabSheet1.Enabled:=True;
//                 Panel2.Enabled:=false;
//                 DStatus.Enabled:=False;
//                 cxPageControl1.ActivePage:=cxTabSheet1;
//
//                 cxGrid1.SetFocus;
//                 Ribbon1.Enabled:=true;
//                 RibbonComboBox1.Text:=pom;
//              end;
//        end;
end;

procedure TfrmNabavki.�SpecifikacijaZaOdbranaNabavkaExecute(Sender: TObject);
var RptStream :TStream;
begin
//     dm.frxDesignerPlan.Tag:=4;
//     dm.tblReportDizajn.ParamByName('id').Value:=4004;
//     dm.tblReportDizajn.FullRefresh;
//
//     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
//     dm.tblSpecifikacijaNaNabavka.FullRefresh;
//
//     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
//     dm.frxReport.LoadFromStream(RptStream) ;
//
////     dm.frxReport.DesignReport();
//     dm.frxReport.ShowReport;
end;

procedure TfrmNabavki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
//    kom := Sender as TWinControl;
//    case Key of
//        VK_DOWN:
//          begin
//            PostMessage(Handle,WM_NextDlgCtl,0,0);
//          end;
//        VK_UP:
//         begin
//           PostMessage(Handle,WM_NextDlgCtl,1,0);
//         end;
//        VK_RETURN:
//         begin
//            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
//         end ;
//        VK_INSERT:
//          begin
//             if (Sender = DRabEdinID) or (Sender = DRabEdinica) then
//               begin
//                  frmRE:=TfrmRE.Create(Application);
//                  frmRE.Tag:=1;
//                  frmRE.ShowModal;
//                  frmRE.Free;
//                  dm.tblNabaviRE.Value:=dmMat.tblREID.Value;
//                  DGodina.SetFocus;
//               end;
////             if (Sender = DPartner) or (Sender = DKontakt) then
////               begin
////                  frmSifrarnikPartner:=TfrmSifrarnikPartner.Create(Application);
////                  frmSifrarnikPartner.Tag:=1;
////                  frmSifrarnikPartner.ShowModal;
////                  frmSifrarnikPartner.Free;
////                  dm.PartnerVraboten.FullRefresh;
////                  DPartner.Text:=dm.tblPartnerNAZIV.Value;
////                  dm.tblNabaviKONTAKT.Value:=dm.tblPartnerID.Value;
////                  DDatumPonudaOd.SetFocus;
////               end;
//             if (sender = DValuta) then
//               begin
//                  frmValuta:=TfrmValuta.Create(Application);
//                  frmValuta.Tag:=1;
//                  frmValuta.ShowModal;
//                  frmValuta.Free;
//                  dm.tblNabaviVALUTA.Value:=dmMat.tblValutaID.Value;
//                  DKurs.SetFocus;
//               end;
//             If (Sender = DTipNabavkaID) or(Sender = DTipNabavka) then
//               begin
//                  frmTipNabavka:=TfrmTipNabavka.Create(Application);
//                  frmTipNabavka.Tag:=1;
//                  frmTipNabavka.ShowModal;
//                  frmTipNabavka.Free;
//                  dm.tblNabaviTIP_NABAVKA.Value:=dm.TipNabavkiID.Value;
//                  DPredmet.SetFocus;
//              end;
//        end;
//    end;
end;

end.
