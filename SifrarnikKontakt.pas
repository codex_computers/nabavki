unit SifrarnikKontakt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, Menus, cxLookAndFeelPainters, cxGraphics,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,cxGridExportLink, cxExport,
  cxDBExtLookupComboBox, StdCtrls, cxButtons, cxContainer, cxTextEdit, cxDBEdit,
  ExtCtrls, ComCtrls, ActnList, ActnMan, ToolWin, ActnCtrls, Ribbon,
  RibbonLunaStyleActnCtrls, dxStatusBar, dxRibbonStatusBar, ActnMenus,
  RibbonActnMenus, cxLookAndFeels, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp;

type
  TfrmSifrarnikKontakt = class(TForm)
    dPanel: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    TipPartner: TcxDBTextEdit;
    Partner: TcxDBTextEdit;
    Email: TcxDBTextEdit;
    Telefon: TcxDBTextEdit;
    Mobilen: TcxDBTextEdit;
    Opis: TcxDBTextEdit;
    PPartner: TcxExtLookupComboBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PartnerNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1MAIL: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Ribbon1: TRibbon;
    RibbonPage1: TRibbonPage;
    RibbonGroup1: TRibbonGroup;
    ActionManager1: TActionManager;
    aDodadi: TAction;
    ButtonZacuvaj: TButton;
    ButtonOtkazi: TButton;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    aAzuriraj: TAction;
    RibbonGroup2: TRibbonGroup;
    aIzlez: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aZacuvajVoExcel: TAction;
    RibbonApplicationMenuBar1: TRibbonApplicationMenuBar;
    LabelPartnerTip: TLabel;
    LabelPartner: TLabel;
    aPomos: TAction;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure PartnerExit(Sender: TObject);
    procedure aDodadiExecute(Sender: TObject);
    procedure ButtonZacuvajClick(Sender: TObject);
    procedure TipPartnerExit(Sender: TObject);
    procedure PPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure ButtonOtkaziClick(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSifrarnikKontakt: TfrmSifrarnikKontakt;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, Utils, SifrarnikPartner,
  TipPartner;

{$R *.dfm}

procedure TfrmSifrarnikKontakt.aAzurirajExecute(Sender: TObject);
begin
     cxGrid1.Enabled:=False;
     Ribbon1.Enabled:=false;
     dPanel.Enabled:=true;
     ButtonZacuvaj.Visible:=True;
     ButtonOtkazi.Visible:=true;
     dm.tblKontakt.Edit;
     StateActive:=dsEdit;
     Naziv.SetFocus;
end;

procedure TfrmSifrarnikKontakt.aBrisiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
end;

procedure TfrmSifrarnikKontakt.aOsveziExecute(Sender: TObject);
begin
     dm.tblKontakt.FullRefresh;
end;

procedure TfrmSifrarnikKontakt.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(112);
end;

procedure TfrmSifrarnikKontakt.aZacuvajVoExcelExecute(Sender: TObject);
begin
     dmMat.SaveToExcelDialog.FileName := '��������' + '-' + dmKon.user;
      if(dmMat.SaveToExcelDialog.Execute)then
        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmSifrarnikKontakt.aDodadiExecute(Sender: TObject);
begin
     cxGrid1.Enabled:=False;
     Ribbon1.Enabled:=false;
     dPanel.Enabled:=true;
     ButtonZacuvaj.Visible:=True;
     ButtonOtkazi.Visible:=true;
     dm.tblKontakt.Insert;
     StateActive:=dsInsert;
     if tag = 1  then
        begin
           dm.tblKontaktTIP_PARTNER.Value:=StrToInt(LabelPartnerTip.Caption);
           dm.tblKontaktPARTNER.Value:=StrToInt(LabelPartner.Caption);
           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(LabelPartnerTip.Caption), StrToInt(LabelPartner.Caption)]) , []);
           PPartner.Text:=dm.tblPartnerNAZIV.Value;
           PPartner.Enabled:=false;
           TipPartner.Enabled:=False;
           Partner.Enabled:=False;
        end;
     dm.tblKontaktID.Value:=dm.zemiMaxIDNabavka(dm.MaxSifraKontakt, Null, Null, Null, Null, Null, Null, 'SIFRA');
     Naziv.SetFocus;
end;

procedure TfrmSifrarnikKontakt.aIzlezExecute(Sender: TObject);
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
           StateActive:=dsBrowse;
           dm.tblKontakt.Cancel;
           dm.RestoreControls(dPanel);
           PPartner.Enabled:=true;
           TipPartner.Enabled:=true;
           Partner.Enabled:=true;
           cxGrid1.Enabled:=True;
           dPanel.Enabled:=False;
           ButtonZacuvaj.Visible:=False;
           ButtonOtkazi.Visible:=False;
           Ribbon1.Enabled:=True;
           cxGrid1.SetFocus;
        end
     else close;
end;

procedure TfrmSifrarnikKontakt.ButtonOtkaziClick(Sender: TObject);
begin
     StateActive:=dsBrowse;
     dm.tblKontakt.Cancel;
     dm.RestoreControls(dPanel);
     cxGrid1.Enabled:=True;
     dPanel.Enabled:=False;
     ButtonZacuvaj.Visible:=False;
     ButtonOtkazi.Visible:=False;
     Ribbon1.Enabled:=True;
     cxGrid1.SetFocus;
end;

procedure TfrmSifrarnikKontakt.ButtonZacuvajClick(Sender: TObject);
begin
     if dm.Validacija(dPanel) = False then
         begin
            dm.tblKontakt.Post;
            dm.RestoreControls(dPanel);
            StateActive:=dsBrowse;
            PPartner.Enabled:=true;
            TipPartner.Enabled:=true;
            Partner.Enabled:=true;
            PPartner.Enabled:=true;
            TipPartner.Enabled:=true;
            Partner.Enabled:=true;
            cxGrid1.Enabled:=True;
            dPanel.Enabled:=False;
            ButtonZacuvaj.Visible:=False;
            ButtonOtkazi.Visible:=False;
            Ribbon1.Enabled:=True;
            dm.tblKontakt.FullRefresh;
            cxGrid1.SetFocus;
         end;
end;

procedure TfrmSifrarnikKontakt.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
      if (TipPartner.Text <> '') and (Partner.Text <> '') then
          begin
             PPartner.Text:= dm.tblKontaktPARTNERNAZIV.Value;
          end;
end;

procedure TfrmSifrarnikKontakt.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_RETURN:begin
            if tag = 1 then
               close;
          end;
          VK_F5:begin
            cxGrid1.Enabled:=False;
            Ribbon1.Enabled:=false;
            dPanel.Enabled:=true;
            ButtonZacuvaj.Visible:=True;
            ButtonOtkazi.Visible:=true;
            dm.tblKontakt.Insert;
            StateActive:=dsInsert;
            if tag = 1  then
               begin
                  dm.tblKontaktTIP_PARTNER.Value:=StrToInt(LabelPartnerTip.Caption);
                  dm.tblKontaktPARTNER.Value:=StrToInt(LabelPartner.Caption);
                  dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(LabelPartnerTip.Caption), StrToInt(LabelPartner.Caption)]) , []);
                  PPartner.Text:=dm.tblPartnerNAZIV.Value;
                  PPartner.Enabled:=false;
                  TipPartner.Enabled:=False;
                  Partner.Enabled:=False;
                end;
            dm.tblKontaktID.Value:=dm.zemiMaxIDNabavka(dm.MaxSifraKontakt, Null, Null, Null, Null, Null, Null, 'SIFRA');
            Naziv.SetFocus;
          end;
          VK_F6:begin
            cxGrid1.Enabled:=False;
            Ribbon1.Enabled:=false;
            dPanel.Enabled:=true;
            ButtonZacuvaj.Visible:=True;
            ButtonOtkazi.Visible:=true;
            dm.tblKontakt.Edit;
            StateActive:=dsEdit;
            Naziv.SetFocus;
          end;
          VK_F8:begin
            if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               begin
                  frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                  if frmDaNe.ShowModal =mrYes then
                    cxGrid1DBTableView1.DataController.DataSet.Delete();
              end
          end;
          vk_f7:begin
            dm.tblKontakt.FullRefresh;
          end;
     end;
end;

procedure TfrmSifrarnikKontakt.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmSifrarnikKontakt.FormShow(Sender: TObject);
var pom:integer;
begin
    LabelPartner.Caption:= IntToStr(dm.tblPartnerID.Value);
    LabelPartnerTip.Caption:=IntToStr(dm.tblPartnerTIP_PARTNER.Value);
    cxGrid1DBTableView1.Controller.FocusedColumn := cxGrid1DBTableView1NAZIV;
    if tag = 0 then
        begin
          dm.tblKontakt.Close;
          dm.tblKontakt.ParamByName('tipPartner').Value:='%';
          dm.tblKontakt.ParamByName('Partner').Value:='%';
          dm.tblKontakt.Open;
          dm.tblPartner.ParamByName('tipPartner').Value:='%';
          dm.tblPartner.Open;
       end;

     if (TipPartner.Text <> '') and (Partner.Text <> '') then
        begin
           dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TipPartner.Text), strtoInt(Partner.Text)]) , []);
           PPartner.Text:=dm.tblPartnerNAZIV.Value;
        end
     else PPartner.Text:= '';
     StateActive:=dsBrowse;
end;

procedure TfrmSifrarnikKontakt.PartnerExit(Sender: TObject);
var pom:integer;
begin
     if (TipPartner.Text <> '') and (Partner.Text <> '') then
        begin
           pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(tipPartner.text),strtoInt(Partner.text), Null, 'BROJ');
           if pom = 1  then
              begin
                 dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TipPartner.Text), StrToInt(Partner.Text)]) , []);
                 PPartner.Text:= dm.tblPartnerNAZIV.Value;
              end
           else PPartner.Text:= '';
        end;
end;

procedure TfrmSifrarnikKontakt.PPartnerPropertiesEditValueChanged(
  Sender: TObject);
begin
     if StateActive in [dsInsert,dsEdit] then
        begin
           dm.tblKontaktPARTNER.Value:=dm.tblPartnerID.Value;
           dm.tblKontaktTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
        end;
end;

procedure TfrmSifrarnikKontakt.TipPartnerExit(Sender: TObject);
var pom:integer;
begin
     if (TipPartner.Text <> '') and (Partner.Text <> '') then
        begin
           pom:=dm.zemicountPartner(dm.countPartner,'TIP_PARTNER','ID', Null, strToInt(tipPartner.text),strtoInt(Partner.text), Null, 'BROJ');
           if pom = 1  then
              begin
                 dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TipPartner.Text), StrToInt(Partner.Text)]) , []);
                 PPartner.Text:= dm.tblPartnerNAZIV.Value;
              end
           else PPartner.Text:= '';
        end;
end;

procedure TfrmSifrarnikKontakt.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;

begin
      case Key of
        VK_UP:
        begin
            if Sender = Sifra then ButtonOtkazi.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             if Sender = ButtonOtkazi then Sifra.SetFocus
             else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
           if (Sender = Opis) and (StateActive in [dsInsert])  then
              begin
                 if Sifra.Text = '' then Sifra.SetFocus
                 else if Naziv.Text = '' then Naziv.SetFocus
                 else if TipPartner.Text = ''  then TipPartner.SetFocus
                 else if Partner.Text = ''then  Partner.SetFocus
                 else if PPartner.Text = '' then PPartner.SetFocus
                 else
                    begin
                       dm.tblKontakt.Post;
                       dm.tblKontakt.Insert;
                       if tag = 1  then
                          begin
                             dm.tblKontaktTIP_PARTNER.Value:=StrToInt(LabelPartnerTip.Caption);
                             dm.tblKontaktPARTNER.Value:=StrToInt(LabelPartner.Caption);
                             dm.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(LabelPartnerTip.Caption), StrToInt(LabelPartner.Caption)]) , []);
                             PPartner.Text:=dm.tblPartnerNAZIV.Value;
                             dm.tblKontaktID.Value:=dm.zemiMaxIDNabavka(dm.MaxSifraKontakt, Null, Null, Null, Null, Null, Null, 'SIFRA');
                             Naziv.SetFocus;
                          end
                       else
                          begin
                             dm.tblKontaktID.Value:=dm.zemiMaxIDNabavka(dm.MaxSifraKontakt, Null, Null, Null, Null, Null, Null, 'SIFRA');
                             Naziv.SetFocus;
                             Partner.Text:= '';
                             TipPartner.Text:= '';
                          end;
                    end;
              end
           else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:begin
              if (Sender = PPartner) or (Sender = Partner) then
                 begin
                      frmSifrarnikPartner:=TfrmSifrarnikPartner.Create(Application);
                      frmSifrarnikPartner.Tag:= 10;
                      frmSifrarnikPartner.ShowModal;
                      frmSifrarnikPartner.Free;
                      dm.tblKontaktTIP_PARTNER.Value:=dm.tblPartnerTIP_PARTNER.Value;
                      dm.tblKontaktPARTNER.Value:=dm.tblPartnerID.Value;
                      Email.SetFocus;
                      dm.tblPartner.ParamByName('tipPartner').Value:='%';
                      dm.tblPartner.FullRefresh;
                 end;
              if (Sender = TipPartner) then
                begin
                   frmTipPartner:=TfrmTipPartner.Create(Application);
                   frmTipPartner.Tag:= 1;
                   frmTipPartner.ShowModal;
                   frmTipPartner.Free;
                   dm.tblKontaktTIP_PARTNER.Value:=dmMat.tblTipPartnerID.Value;
                   Partner.SetFocus;
                end;

        end;

  end;
end;

end.
