unit IzborGrupaZaPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxContainer, cxTextEdit, cxDBEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, StdCtrls, ExtCtrls, dxStatusBar,
  dxRibbonStatusBar, ActnList;

type
  TfrmIzborGrupaZaPlan = class(TForm)
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Label3: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel4: TPanel;
    Label4: TLabel;
    Panel5: TPanel;
    Label15: TLabel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    aPomos: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzborGrupaZaPlan: TfrmIzborGrupaZaPlan;

implementation

uses dmUnit, dmMaticni, DaNe, Utils;

{$R *.dfm}
procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
begin
     Assert(Assigned(Strings)) ;
     Strings.Clear;
     Strings.Delimiter := Delimiter;
     Strings.DelimitedText := Input;
end;
procedure TfrmIzborGrupaZaPlan.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmIzborGrupaZaPlan.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(110);
end;

procedure TfrmIzborGrupaZaPlan.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:string;
    i, pom1, dolzina:integer;
    A: TStringList;
begin
     case key of
          vk_f8:begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
           begin
              pom:=dm.SetupGrupaArtikalPlanV1.Value;
              A := TStringList.Create;
              try
                Split(',', pom, A) ;
                dolzina:=a.Count ;
                for I := 0 to dolzina - 2 do
                    begin
                      if IntToStr(dm.IzbraniGrupiZaPlanID.Value) = a[i] then
                         pom1:= i;
                    end;
                for i := pom1 to dolzina - 3 do
                 begin
                  a[i]:=a[i + 1];
                 end;
                pom:='';
                for i := 0 to dolzina - 3 do
                begin
                    pom:= pom + a[i] + ',';
                end;
              finally
                A.Free;
              end;
              dm.SetupGrupaArtikalPlan.Edit;
              dm.SetupGrupaArtikalPlanV1.Value:=pom;
              dm.SetupGrupaArtikalPlan.Post;
              dm.IzbraniGrupiZaPlan.FullRefresh;
           end;
          end;
     end;
end;

procedure TfrmIzborGrupaZaPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborGrupaZaPlan.cxGridDBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:string;
    i, pom1, dolzina:integer;
    A: TStringList;

begin
     case Key of
       VK_RETURN:begin
         pom1:=0;
         pom:=dm.SetupGrupaArtikalPlanV1.Value;
         A := TStringList.Create;
         try
            Split(',', pom, A) ;
            dolzina:= a.Count;
            for i := 0 to dolzina - 2 do
               if IntToStr(dmMat.tblArtVidID.Value) = a[i] then
                  begin
                     pom1:=1;
                  end
         finally
            A.Free;
         end;
         if pom1 = 0 then
            begin
               pom:=pom  + IntToStr(dmMat.tblArtVidID.Value)+ ',';
               dm.SetupGrupaArtikalPlan.Edit;
               dm.SetupGrupaArtikalPlanV1.Value:=pom;
               dm.SetupGrupaArtikalPlan.Post;
               dm.IzbraniGrupiZaPlan.FullRefresh;
            end
         else ShowMessage('���� ����� � ��� ������� !!!');
       end;
     end;
end;

procedure TfrmIzborGrupaZaPlan.cxGridDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView1);
end;

procedure TfrmIzborGrupaZaPlan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmMat.tblArtVid.close;
     dm.SetupGrupaArtikalPlan.Close;
     dm.IzbraniGrupiZaPlan.Close;
end;

procedure TfrmIzborGrupaZaPlan.FormCreate(Sender: TObject);
begin
      dmMat.tblArtVid.Open;
      dm.SetupGrupaArtikalPlan.Open;
      dm.IzbraniGrupiZaPlan.Open;
end;

end.
