unit SpecifikacijaNabavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RibbonLunaStyleActnCtrls, Ribbon, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxContainer, cxLabel, cxDBLabel,
  StdCtrls, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxStatusBar, dxRibbonStatusBar, ExtCtrls, dxBar, dxRibbon, dxRibbonGallery,
  cxTextEdit, cxDBEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,cxGridExportLink, cxExport,
  cxDBLookupEdit, cxDBLookupComboBox, ActnList, ToolWin, ActnMan, ActnCtrls,
  cxDBExtLookupComboBox, cxMemo, ActnMenus, RibbonActnMenus,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinSeven, dxSkinSharp, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter, cxBarEditItem, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  cxCheckBox, PlatformDefaultStyleActnCtrls, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk;

type
  TFrmSpecifikacijaNabavka = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    Panel4: TPanel;
    Panel3: TPanel;
    Label6: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDINIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CenaDDV: TcxGridDBColumn;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView2: TcxGridDBTableView;
    CNabavka_ID: TcxDBLabel;
    cxGrid1DBTableView1ArtSifra: TcxGridDBColumn;
    ActionManager1: TActionManager;
    aAzuriraj: TAction;
    aBrisi: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1Iznos: TcxGridDBColumn;
    Panel7: TPanel;
    Bevel3: TBevel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    cxGrid1DBTableView1Cena: TcxGridDBColumn;
    cxGrid1DBTableView1DDV: TcxGridDBColumn;
    CBrojNabavka: TcxDBLabel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1SPISOK: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1POTEKLO: TcxGridDBColumn;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    cxGrid2DBTableView3: TcxGridDBTableView;
    RabEdinica: TcxExtLookupComboBox;
    Label20: TLabel;
    aIzlez: TAction;
    aZacuvajIzgled: TAction;
    cxGrid2DBTableView2ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView2ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView2KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView2CENA: TcxGridDBColumn;
    cxGrid2DBTableView2DDV: TcxGridDBColumn;
    cxGrid2DBTableView2NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView2MERKAID: TcxGridDBColumn;
    cxGrid2DBTableView2MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView2ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView2PODGRUPAID: TcxGridDBColumn;
    cxGrid2DBTableView2PODGRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView3ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView3ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView3KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView3CENA: TcxGridDBColumn;
    cxGrid2DBTableView3DDV: TcxGridDBColumn;
    cxGrid2DBTableView3NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView3MERKAID: TcxGridDBColumn;
    cxGrid2DBTableView3MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView3ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView3PODGRUPAID: TcxGridDBColumn;
    cxGrid2DBTableView3PODGRUPANAZIV: TcxGridDBColumn;
    aZacuvajVoExcel: TAction;
    cxDBTextEdit4: TcxDBTextEdit;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1BRPAK: TcxGridDBColumn;
    cxGrid2DBTableView1KATALOG: TcxGridDBColumn;
    cxGrid2DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1KLASIFIKACIJA: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1Opis: TcxGridDBColumn;
    Label24: TLabel;
    aPomos: TAction;
    Kolicina: TcxDBTextEdit;
    Cena: TcxDBTextEdit;
    DDV: TcxDBLookupComboBox;
    CenaSoDDV: TcxDBTextEdit;
    Opis: TcxDBMemo;
    RabEdinicaID: TcxDBTextEdit;
    Panel6: TPanel;
    Bevel2: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label21: TLabel;
    ARabEdinica: TcxExtLookupComboBox;
    cxDBTextEdit1: TcxDBTextEdit;
    AKolicina: TcxDBTextEdit;
    ACena: TcxDBTextEdit;
    ADDV: TcxDBLookupComboBox;
    ACenaSoDDV: TcxDBTextEdit;
    AOpis: TcxDBMemo;
    ARabEdinicaID: TcxDBTextEdit;
    Panel5: TPanel;
    Bevel1: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label23: TLabel;
    PRabEdinica: TcxExtLookupComboBox;
    cxDBTextEdit2: TcxDBTextEdit;
    PKolicina: TcxDBTextEdit;
    PCena: TcxDBTextEdit;
    PDDV: TcxDBLookupComboBox;
    PCenaSoDDV: TcxDBTextEdit;
    POpis: TcxDBMemo;
    PRabEdinicaID: TcxDBTextEdit;
    Panel8: TPanel;
    Bevel4: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label17: TLabel;
    Label25: TLabel;
    cxDBTextEdit3: TcxDBTextEdit;
    SKolicina: TcxDBTextEdit;
    SCena: TcxDBTextEdit;
    SDDV: TcxDBLookupComboBox;
    SCenaSoDDV: TcxDBTextEdit;
    SOpis: TcxDBMemo;
    Label29: TLabel;
    Label22: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    ActionList1: TActionList;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid2DBTableView2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aESCExecute(Sender: TObject);
    procedure EnterPanel5(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterPanel6(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterPanel8(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterPanel7(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PDDVPropertiesEditValueChanged(Sender: TObject);
    procedure ADDVPropertiesEditValueChanged(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure RabEdinicaIDPropertiesEditValueChanged(Sender: TObject);
    procedure ARabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure PRabEdinicaIDPropertiesEditValueChanged(Sender: TObject);
    procedure PRabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PCenaExit(Sender: TObject);
    procedure PCenaSoDDVExit(Sender: TObject);
    procedure ACenaExit(Sender: TObject);
    procedure ACenaSoDDVExit(Sender: TObject);
    procedure cxGrid2DBTableView3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RadioButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SDDVPropertiesEditValueChanged(Sender: TObject);
    procedure SCenaExit(Sender: TObject);
    procedure SCenaSoDDVExit(Sender: TObject);
    procedure ARabEdinicaIDPropertiesEditValueChanged(Sender: TObject);
    procedure RabEdinicaPropertiesEditValueChanged(Sender: TObject);
    procedure CenaExit(Sender: TObject);
    procedure CenaSoDDVExit(Sender: TObject);
    procedure DDVPropertiesEditValueChanged(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSpecifikacijaNabavka: TFrmSpecifikacijaNabavka;

implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, Plan, Utils, FormConfig, Artikal;

{$R *.dfm}

procedure TFrmSpecifikacijaNabavka.ARabEdinicaIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (ARabEdinicaID.Text <> '') then
         ARabEdinica.EditValue:=dm.tblPoNabavkaStavkiRE.Value;
end;

procedure TFrmSpecifikacijaNabavka.ARabEdinicaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (ARabEdinica.Text <> '') then
        dm.tblPoNabavkaStavkiRE.Value:=ARabEdinica.EditValue;
end;

procedure TFrmSpecifikacijaNabavka.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
    ZacuvajFormaIzgled(self);
end;

procedure TFrmSpecifikacijaNabavka.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TFrmSpecifikacijaNabavka.aZacuvajExcelExecute(Sender: TObject);
begin
      zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TFrmSpecifikacijaNabavka.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true);
     zacuvajGridVoIni(cxGrid2DBTableView1,dmKon.aplikacija+Name+'2',true);
     zacuvajGridVoIni(cxGrid2DBTableView2,dmKon.aplikacija+Name+'3',true);
     zacuvajGridVoIni(cxGrid2DBTableView3,dmKon.aplikacija+Name+'4',true);
end;

procedure TFrmSpecifikacijaNabavka.aZacuvajVoExcelExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TFrmSpecifikacijaNabavka.CenaExit(Sender: TObject);
begin
     if (Cena.Text <> '') and (DDV.Text <> '') and (StateActive in [dsEdit])then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:= dm.tblPoNabavkaStavkiCENA.Value + (DDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.CenaSoDDVExit(Sender: TObject);
begin
     if (DDV.Text <> '') and (CenaSoDDV.Text <> '') and (StateActive in [dsEdit])  then
        begin
           dm.tblPoNabavkaStavkiCENA.Value:=dm.tblPoNabavkaStavkiCENADDV.Value*100/(100 + DDV.EditValue);
        end;
end;

procedure TFrmSpecifikacijaNabavka.aAzurirajExecute(Sender: TObject);
var pom:string;
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           pom := dm.tblNabaviPOTEKLO.Value;
           if pom = '' then
              begin
                 dm.RabEdinica.ParamByName('spisok').Value:= '%';
                 dm.RabEdinica.ParamByName('poteklo').Value:= '%';
              end
           else
              begin
                 dm.RabEdinica.ParamByName('spisok').Value:= dm.tblNabaviPOTEKLO.Value + '%';
                 dm.RabEdinica.ParamByName('poteklo').Value:=dm.tblNabaviPOTEKLO.Value;
              end;
           dm.RabEdinica.FullRefresh;
           dm.DDV.Open;
           Panel7.Visible:=true;

           dm.RabEdinica.Locate('RE', dm.tblPoNabavkaStavkiRE.Value, []);
           RabEdinica.Text:=dm.RabEdinicaNAZIV.Value;
           dm.tblPoNabavkaStavki.Edit;
           StateActive:=dsEdit;
           Cena.SetFocus;
        end
     else
        ShowMessage('����������� ������ !!!!');
end;

procedure TFrmSpecifikacijaNabavka.aBrisiExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
           if frmDaNe.ShowModal =mrYes then
              dm.tblPoNabavkaStavki.Delete
        end
     else
        ShowMessage('����������� ������ !!!!');
end;

procedure TFrmSpecifikacijaNabavka.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TFrmSpecifikacijaNabavka.ACenaExit(Sender: TObject);
begin
     if (ACena.Text <> '') and (ADDV.Text <> '') and (StateActive in [dsInsert])then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:= dm.tblPoNabavkaStavkiCENA.Value + (ADDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.ACenaSoDDVExit(Sender: TObject);
begin
     if (ADDV.Text <> '') and (ACenaSoDDV.Text <> '') and (StateActive in [dsInsert])  then
        begin
           dm.tblPoNabavkaStavkiCENA.Value:=dm.tblPoNabavkaStavkiCENADDV.Value*100/(100 + ADDV.EditValue);
        end;
end;

procedure TFrmSpecifikacijaNabavka.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TFrmSpecifikacijaNabavka.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TFrmSpecifikacijaNabavka.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TFrmSpecifikacijaNabavka.aPomosExecute(Sender: TObject);
begin
      Application.HelpContext(101);
end;

procedure TFrmSpecifikacijaNabavka.ADDVPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (Cena.Text <> '') and (ADDV.Text <> '') then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:=dm.tblPoNabavkaStavkiCENA.Value  + (ADDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.aESCExecute(Sender: TObject);
begin
     if (Panel6.Visible = true) then
         begin
            dm.DDV.Close;
            dm.RabEdinica.Close;
            dm.tblPoNabavkaStavki.Cancel;
            StateActive:=dsBrowse;
            Panel6.Visible:= False;
            dm.RestoreControls(Panel6);
            cxGrid2.SetFocus;
          end
      else
      if (Panel5.Visible = true) then
          begin
             dm.DDV.Close;
             dm.RabEdinica.Close;
             Panel5.Visible:= False;
             dm.tblPoNabavkaStavki.Cancel;
             StateActive:=dsBrowse;
             dm.RestoreControls(Panel5);
             cxGrid2.SetFocus;
          end
      else
      if (Panel8.Visible = true) then
          begin
             dm.DDV.Close;
             dm.RabEdinica.Close;
             dm.tblPoNabavkaStavki.Cancel;
             StateActive:=dsBrowse;
             Panel8.Visible:= False;
             dm.RestoreControls(Panel8);
             cxGrid2.SetFocus;
          end
      else
      if (Panel7.Visible = true)then
          begin
             dm.DDV.Close;
             dm.RabEdinica.Close;
             panel7.Visible:=False;
             dm.tblPoNabavkaStavki.Cancel;
             StateActive:=dsBrowse;
             dm.RestoreControls(Panel7);
             cxGrid1.SetFocus;
          end
      else
         begin
            dm.tblPoPlanStavki.Close;
            Close;
         end;
end;

procedure TFrmSpecifikacijaNabavka.aFormConfigExecute(Sender: TObject);
begin
   frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TFrmSpecifikacijaNabavka.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom :string;
begin
      case Key of
         VK_F6: begin
           if (Panel8.Visible = false)and (Panel5.Visible = false) and (Panel6.Visible = false) then
            begin
             if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
                 begin
                    pom := dm.tblNabaviPOTEKLO.Value;
                    if pom = '' then
                       begin
                          dm.RabEdinica.ParamByName('spisok').Value:= '%';
                          dm.RabEdinica.ParamByName('poteklo').Value:= '%';
                       end
                    else
                       begin
                          dm.RabEdinica.ParamByName('spisok').Value:= dm.tblNabaviPOTEKLO.Value + '%';
                          dm.RabEdinica.ParamByName('poteklo').Value:=dm.tblNabaviPOTEKLO.Value;
                       end;
                    dm.RabEdinica.FullRefresh;
                    dm.DDV.Open;
                    Panel7.Visible:=true;

                    dm.RabEdinica.Locate('RE', dm.tblPoNabavkaStavkiRE.Value, []);
                    RabEdinica.Text:=dm.RabEdinicaNAZIV.Value;
                    dm.tblPoNabavkaStavki.Edit;
                    StateActive:=dsEdit;
                    Cena.SetFocus;
                 end
              else
                 ShowMessage('����������� ������ !!!!');
           end;
         end;
         VK_F8:begin
             if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
                begin
                    frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                    if frmDaNe.ShowModal =mrYes then
                       dm.tblPoNabavkaStavki.Delete
                end
             else
                ShowMessage('����������� ������ !!!!');
        end;
      end;

end;

procedure TFrmSpecifikacijaNabavka.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom: string;
      pom1: integer;
begin
      if cxGrid2DBTableView1.Controller.SelectedRecordCount = 1 then
        case Key of
             VK_RETURN:begin
                    if (Panel8.Visible = false)and (Panel5.Visible = false) and (Panel7.Visible = false) then
                        begin
                           POM1:=dm.zemiBr4(dm.BrStavkiNabavka,'ARTSIF', 'ARTVID','ID','RE', dm.tblListaArtikliFiltriraniID.Value, dm.tblListaArtikliFiltriraniARTVID.Value, StrToInt(CNabavka_ID.Caption), ARabEdinica.EditValue, 'BROJ');
                           if pom1 = 0 then
                              begin
                                 pom := dm.tblNabaviPOTEKLO.Value;
                                 if pom = '' then
                                    begin
                                       dm.RabEdinica.ParamByName('spisok').Value:= '%';
                                       dm.RabEdinica.ParamByName('poteklo').Value:= '%';
                                    end
                                 else
                                    begin
                                       dm.RabEdinica.ParamByName('spisok').Value:= dm.tblNabaviPOTEKLO.Value + '%';
                                       dm.RabEdinica.ParamByName('poteklo').Value:=dm.tblNabaviPOTEKLO.Value;
                                    end;
                                 dm.RabEdinica.FullRefresh;
                                 dm.DDV.Open;
                                 Panel6.Top:=140;
                                 Panel6.Left:=128;
                                 panel6.Visible:=true;
                                 dm.tblPoNabavkaStavki.Insert;
                                 StateActive:=dsInsert;
                                 ACena.SetFocus;

                                 dm.tblPoNabavkaStavkiRE.Value:=dm.tblNabaviRE.Value;
                                 ARabEdinica.EditValue:=dm.tblNabaviRE.Value;

                                 dm.tblPoNabavkaStavkiKOLICINA.Value:=0;
                                 dm.tblPoNabavkaStavkiCENA.Value:=dm.tblListaArtikliFiltriraniCENA.Value;
                                 dm.tblPoNabavkaStavkiCENADDV.Value:=0;
                                 dm.tblPoNabavkaStavkiDDV.Value:=dm.tblListaArtikliFiltriraniTARIFA.Value;
                              end
                            else ShowMessage('���� ������ � ��� ������� !!!')
                        end;
             end;
             VK_INSERT:begin
//               frmArtikli:=TfrmArtikli.Create(Application);
//               frmArtikli.Tag:=1;
//               frmArtikli.ShowModal;
//               frmArtikli.Free;

               frmArtikal:=TfrmArtikal.Create(self,true);
               frmArtikal.ShowModal;
               frmArtikal.Free;

               dm.tblListaArtikliFiltrirani.fullRefresh;
               dm.tblListaArtikliFiltrirani.Locate('ID; ARTVID', VarArrayOf([dm.tblListaArtikliID.Value, dm.tblListaArtikliARTVID.Value]) , []);
             end;
        end;
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var pom:string;
    pom1 :integer;
begin
       if cxGrid2DBTableView2.Controller.SelectedRecordCount = 1 then
        case Key of
             VK_RETURN:begin
                      if (Panel8.Visible = false)and (Panel7.Visible = false) and (Panel6.Visible = false) then
                          begin
                              POM1:=dm.zemiBr4(dm.BrStavkiNabavka,'ARTSIF', 'ARTVID','ID','RE', dm.tblPoPlanStavkiARTSIF.Value, dm.tblPoPlanStavkiARTVID.Value, StrToInt(CNabavka_ID.Caption), PRabEdinica.EditValue, 'BROJ');
                              if pom1 = 0 then
                                 begin
                                    pom := dm.tblNabaviPOTEKLO.Value;
                                    if pom = '' then
                                      begin
                                         dm.RabEdinica.ParamByName('spisok').Value:= '%';
                                         dm.RabEdinica.ParamByName('poteklo').Value:= '%';
                                      end
                                    else
                                      begin
                                         dm.RabEdinica.ParamByName('spisok').Value:= dm.tblNabaviPOTEKLO.Value + '%';
                                         dm.RabEdinica.ParamByName('poteklo').Value:=dm.tblNabaviPOTEKLO.Value;
                                      end;
                                   dm.RabEdinica.FullRefresh;
                                   dm.DDV.Open;
                                   Panel5.Top:=140;
                                   Panel5.Left:=128;
                                   panel5.Visible:=true;
                                   dm.tblPoNabavkaStavki.Insert;
                                   StateActive:=dsInsert;
                                   PCena.SetFocus;

                                   PRabEdinica.EditValue:=dm.tblNabaviRE.Value;
                                   dm.tblPoNabavkaStavkiRE.Value:= dm.tblNabaviRE.Value;

                                   dm.tblPoNabavkaStavkiKOLICINA.Value:=dm.tblPoPlanStavkiKOLICINA.Value;
                                   dm.tblPoNabavkaStavkiCENA.Value:=dm.tblPoPlanStavkiCENA.Value;
                                   dm.tblPoNabavkaStavkiCENADDV.Value:=0;
                                   dm.tblPoNabavkaStavkiDDV.Value:=dm.tblPoPlanStavkiDDV.Value;
                                 end
                              else ShowMessage('���� ������ � ��� ������� !!!')
                      end;
             end;
        end;
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView2);
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var pom:integer;
begin
     if cxGrid2DBTableView3.Controller.SelectedRecordCount = 1 then
        case Key of
             VK_RETURN:begin
                 if (Panel7.Visible = false)and (Panel5.Visible = false) and (Panel6.Visible = false) then
                     begin
                        POM:=dm.zemiBr4(dm.BrStavkiNabavka,'ARTSIF', 'ARTVID','ID','RE', dm.PlanStavkiRabEdARTSIF.Value, dm.PlanStavkiRabEdARTVID.Value, StrToInt(CNabavka_ID.Caption), dm.tblNabaviRE.Value, 'BROJ');
                        if pom = 0 then
                           begin
                              dm.DDV.Open;
                              Panel8.Top:=140;
                              Panel8.Left:=128;
                              panel8.Visible:=true;
                              dm.tblPoNabavkaStavki.Insert;
                              StateActive:=dsInsert;
                              SCena.SetFocus;
                              //SDDV.EditValue:=dm.PlanStavkiRabEdDDV.Value;
                              dm.tblPoNabavkaStavkiRE.Value:=dm.tblNabaviRE.Value;

                              dm.tblPoNabavkaStavkiKOLICINA.Value:=dm.PlanStavkiRabEdKOLICINA.Value;
                              dm.tblPoNabavkaStavkiCENA.Value:=dm.PlanStavkiRabEdCENA.Value;
                              dm.tblPoNabavkaStavkiCENADDV.Value:=dm.PlanStavkiRabEdCENA.Value * dm.PlanStavkiRabEdDDV.Value / 100;
                              dm.tblPoNabavkaStavkiDDV.Value:=dm.PlanStavkiRabEdDDV.Value;
                           end
                        else  ShowMessage('���� ������ � ��� ������� !!!');
                     end;
             end;
        end;
end;

procedure TFrmSpecifikacijaNabavka.cxGrid2DBTableView3KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView3);
end;

procedure TFrmSpecifikacijaNabavka.DDVPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsEdit]) and (Cena.Text <> '') and (DDV.Text <> '') then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:=dm.tblPoNabavkaStavkiCENA.Value  + (DDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.tblListaArtikliFiltrirani.Close;
     dm.tblPoPlanStavki.Close;
     dm.PlanStavkiRabEd.Close;
     dm.tblNabavi.FullRefresh;
end;

procedure TFrmSpecifikacijaNabavka.FormCreate(Sender: TObject);
begin
       dm.tblListaArtikliFiltrirani.Close;
       dm.tblListaArtikliFiltrirani.Open;
       dm.tblPoPlanStavki.ParamByName('spisok').Value:=inttostr(dmKon.re) + ','+'%';
       dm.tblPoPlanStavki.ParamByName('poteklo').Value:=inttostr(dmKon.re) + ',';
       dm.tblPoPlanStavki.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
       dm.tblPoPlanStavki.ParamByName('odobren').Value:=odobren_plan;
       dm.tblPoPlanStavki.Open;
       dm.PlanStavkiRabEd.ParamByName('godina').Value:=dm.tblNabaviGODINA.Value;
       dm.PlanStavkiRabEd.ParamByName('RE').Value:=dm.tblNabaviRE.Value;
       dm.PlanStavkiRabEd.ParamByName('odobren').Value:=odobren_plan;
       dm.PlanStavkiRabEd.Open;
       dmMat.tblRE.Open;
end;

procedure TFrmSpecifikacijaNabavka.FormShow(Sender: TObject);
begin
       procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name+'1',true,true);
       procitajGridOdIni(cxGrid2DBTableView1,dmKon.aplikacija+Name+'2',true,true);
       procitajGridOdIni(cxGrid2DBTableView2,dmKon.aplikacija+Name+'3',true,true);
       procitajGridOdIni(cxGrid2DBTableView3,dmKon.aplikacija+Name+'4',true,true);
       cxGrid2.SetFocus;
       cxGrid2Level1.GridView:=cxGrid2DBTableView3;
       StateActive:=dsBrowse;
end;

procedure TFrmSpecifikacijaNabavka.PCenaExit(Sender: TObject);
begin
     if (PCena.Text <> '') and (PDDV.Text <> '') and (StateActive in [dsInsert])then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:= dm.tblPoNabavkaStavkiCENA.Value + (PDDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.PCenaSoDDVExit(Sender: TObject);
begin
     if (PCenaSoDDV.Text <> '') and (PDDV.Text <> '') and (StateActive in [dsInsert])  then
        begin
           dm.tblPoNabavkaStavkiCENA.Value:=dm.tblPoNabavkaStavkiCENADDV.Value*100/(100 + PDDV.EditValue);
        end;
end;

procedure TFrmSpecifikacijaNabavka.PDDVPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (PCena.Text <> '')and (PDDV.Text <> '') then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:=dm.tblPoNabavkaStavkiCENA.Value  + (PDDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.PRabEdinicaIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (PRabEdinicaID.Text <> '') then
         PRabEdinica.EditValue:=dm.tblPoNabavkaStavkiRE.Value;
end;

procedure TFrmSpecifikacijaNabavka.PRabEdinicaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (PRabEdinica.Text <> '') then
        dm.tblPoNabavkaStavkiRE.Value:=PRabEdinica.EditValue;
end;

procedure TFrmSpecifikacijaNabavka.RabEdinicaIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsEdit]) and (RabEdinicaID.Text <> '') then
         RabEdinica.EditValue:=dm.tblPoNabavkaStavkiRE.Value;
end;

procedure TFrmSpecifikacijaNabavka.RabEdinicaPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsEdit]) and (RabEdinica.Text <> '') then
        dm.tblPoNabavkaStavkiRE.Value:=RabEdinica.EditValue;
end;

procedure TFrmSpecifikacijaNabavka.RadioButton1Click(Sender: TObject);
begin
     cxGrid2Level1.GridView:=cxGrid2DBTableView3;
     cxGrid2.SetFocus;
end;

procedure TFrmSpecifikacijaNabavka.RadioButton2Click(Sender: TObject);
begin
     cxGrid2.SetFocus;
     cxGrid2Level1.GridView:=cxGrid2DBTableView1;
end;

procedure TFrmSpecifikacijaNabavka.RadioButton3Click(Sender: TObject);
begin
     cxGrid2.SetFocus;
     cxGrid2Level1.GridView:=cxGrid2DBTableView2;
end;


procedure TFrmSpecifikacijaNabavka.SCenaExit(Sender: TObject);
begin
     if (SCena.Text <> '') and (StateActive in [dsInsert])and (SDDV.Text <> '')then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:= dm.tblPoNabavkaStavkiCENA.Value + (SDDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;

procedure TFrmSpecifikacijaNabavka.SCenaSoDDVExit(Sender: TObject);
begin
     if (SCenaSoDDV.Text <> '') and (StateActive in [dsInsert])and (SDDV.Text <> '')  then
        begin
           dm.tblPoNabavkaStavkiCENA.Value:=dm.tblPoNabavkaStavkiCENADDV.Value*100/(100 + SDDV.EditValue);
        end;
end;

procedure TFrmSpecifikacijaNabavka.SDDVPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert]) and (SCena.Text <> '')and (SDDV.Text <> '') then
        begin
           dm.tblPoNabavkaStavkiCENADDV.Value:=dm.tblPoNabavkaStavkiCENA.Value  + (SDDV.EditValue*dm.tblPoNabavkaStavkiCENA.Value/100);
        end;
end;
procedure TFrmSpecifikacijaNabavka.EnterPanel7(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case Key of
            VK_UP: begin
              if Sender = RabEdinicaID then  Kolicina.SetFocus
              else PostMessage(Handle,WM_NextDlgCtl,1,0);
            end;
            VK_DOWN:begin
              if sender = Kolicina then RabEdinicaID.SetFocus
              else PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;
            VK_RETURN:begin
              if (Sender = Kolicina) then
                  begin
                     if (dm.Validacija(Panel7) = false) and (Kolicina.Text <> '')then
                        begin
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           dm.RestoreControls(Panel7);
                           cxGrid1DBTableView1.DataController.DataSet.Refresh;
                           StateActive:=dsBrowse;
                           dm.RestoreControls(Panel7);
                           cxGrid1.SetFocus;
                           Panel7.Visible:=False;
                        end
                  end
              else  PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;
      end;
end;

procedure TFrmSpecifikacijaNabavka.EnterPanel5(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom: Integer;
begin

      case Key of
            VK_UP: begin
              if Sender = PRabEdinicaID then
                 PKolicina.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,1,0);
            end;
            VK_DOWN:begin
              if sender = PKolicina  then
                 PRabEdinicaID.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;
            VK_RETURN:begin
              if (Sender = PKolicina) then
                  begin
                     if (dm.Validacija(Panel5) = false) and (PKolicina.Text <> '')  then
                        begin
                           dm.tblPoNabavkaStavkiNABAVKA_ID.Value:= StrToInt(CNabavka_ID.Caption);
                           CNabavka_ID.Visible:=true;
                           dm.tblPoNabavkaStavkiID.Value:= dm.zemiMaxPlusEden3(dm.MaxNabavkaStavkaID,'NABAVKAID',Null,Null,StrToInt(CNabavka_ID.Caption),Null,Null, 'MAXBR');
                           CNabavka_ID.Visible:=false;
                           dm.tblPoNabavkaStavkiARTVID.Value:= dm.tblPoPlanStavkiARTVID.Value;
                           dm.tblPoNabavkaStavkiARTSIF.Value:= dm.tblPoPlanStavkiARTSIF.Value;
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           StateActive:=dsBrowse;
                           dm.RestoreControls(Panel5);
                           dm.tblPoNabavkaStavki.Close;
                           dm.tblPoNabavkaStavki.ParamByName('NabavkaId').Value:=StrToInt(CNabavka_ID.Caption);
                           dm.tblPoNabavkaStavki.Open;
                           dm.RabEdinica.Close;
                           Panel5.Visible:=False;
                           cxGrid2.SetFocus;
                        end;
                  end
              else PostMessage(Handle,WM_NextDlgCtl,0,0)
            end;
      end;
end;

procedure TFrmSpecifikacijaNabavka.EnterPanel6(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom: Integer;
begin

      case Key of
            VK_UP: begin
              if Sender = ARabEdinicaID then
                 AKolicina.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,1,0);
            end;
            VK_DOWN:begin
              if sender = AKolicina  then
                 ARabEdinicaID.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;
            VK_RETURN:begin
              if (Sender = AKolicina)then
                  begin
                     if (dm.Validacija(Panel6) = false) and (AKolicina.Text <> '')then
                        begin
                           CNabavka_ID.Visible:=true;
                           dm.tblPoNabavkaStavkiNABAVKA_ID.Value:= StrToInt(CNabavka_ID.Caption);
                           CNabavka_ID.Visible:=false;
                           dm.tblPoNabavkaStavkiID.Value:= dm.zemiMaxPlusEden3(dm.MaxNabavkaStavkaID,'NABAVKAID',Null,Null,StrToInt(CNabavka_ID.Caption),Null,Null, 'MAXBR');
                           dm.tblPoNabavkaStavkiARTVID.Value:= dm.tblListaArtikliFiltriraniARTVID.Value;
                           dm.tblPoNabavkaStavkiARTSIF.Value:= dm.tblListaArtikliFiltriraniID.Value;
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           StateActive:=dsBrowse;
                           dm.RestoreControls(Panel6);
                           dm.tblPoNabavkaStavki.Close;
                           dm.tblPoNabavkaStavki.ParamByName('NabavkaId').Value:=StrToInt(CNabavka_ID.Caption);
                           dm.tblPoNabavkaStavki.Open;
                           dm.RabEdinica.Close;
                           Panel6.Visible:=False;
                           cxGrid2.SetFocus;
                        end;
                  end
              else  PostMessage(Handle,WM_NextDlgCtl,0,0)
            end;
       end;
end;

procedure TFrmSpecifikacijaNabavka.EnterPanel8(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var pom: Integer;
begin

      case Key of
            VK_UP: begin
              if Sender = SOpis then
                 SKolicina.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,1,0);
            end;
            VK_DOWN:begin
              if sender = SKolicina  then
                 SOpis.SetFocus
              else
                 PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;
            VK_RETURN:begin
              if (Sender = SKolicina) then
                  begin
                     if (dm.Validacija(Panel8) = false) and (SKolicina.Text <> '')  then
                        begin
                           CNabavka_ID.Visible:=true;
                           dm.tblPoNabavkaStavkiNABAVKA_ID.Value:= StrToInt(CNabavka_ID.Caption);
                           CNabavka_ID.Visible:=false;
                           dm.tblPoNabavkaStavkiID.Value:= dm.zemiMaxPlusEden3(dm.MaxNabavkaStavkaID,'NABAVKAID',Null,Null,StrToInt(CNabavka_ID.Caption),Null,Null, 'MAXBR');
                           dm.tblPoNabavkaStavkiARTVID.Value:= dm.PlanStavkiRabEdARTVID.Value;
                           dm.tblPoNabavkaStavkiARTSIF.Value:= dm.PlanStavkiRabEdARTSIF.Value;
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           StateActive:=dsBrowse;
                           dm.RestoreControls(Panel8);
                           dm.tblPoNabavkaStavki.Close;
                           dm.tblPoNabavkaStavki.ParamByName('NabavkaId').Value:=StrToInt(CNabavka_ID.Caption);
                           dm.tblPoNabavkaStavki.Open;

                           Panel8.Visible:=False;
                           cxGrid2.SetFocus;
                        end;
                  end
              else PostMessage(Handle,WM_NextDlgCtl,0,0)
            end;
       end;
end;


end.
