object FormProba: TFormProba
  Left = 0
  Top = 0
  Caption = 'FormProba'
  ClientHeight = 580
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 617
    Height = 580
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -398
    ExplicitWidth = 1015
    ExplicitHeight = 195
    object cxGrid2DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dm.dsPoPonudaStavki
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = cxGrid2DBTableView1IIZNOS
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      Styles.Background = dm.cxStyle1
      Styles.ContentEven = dm.cxStyle2
      Styles.ContentOdd = dmKon.cxStyle17
      Styles.GroupByBox = dmKon.cxStyle2
      Styles.Header = dmKon.cxStyle17
      object cxGrid2DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 41
      end
      object cxGrid2DBTableView1BROJ: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ'
      end
      object cxGrid2DBTableView1GODINA: TcxGridDBColumn
        DataBinding.FieldName = 'GODINA'
      end
      object cxGrid2DBTableView1ARTVID: TcxGridDBColumn
        DataBinding.FieldName = 'ARTVID'
      end
      object cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTVIDNAZIV'
        Width = 124
      end
      object cxGrid2DBTableView1ARTSIF: TcxGridDBColumn
        DataBinding.FieldName = 'ARTSIF'
      end
      object cxGrid2DBTableView1ARTNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTNAZIV'
        Width = 119
      end
      object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
      end
      object cxGrid2DBTableView1KOLICINA_PRIFATENA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA_PRIFATENA'
        Width = 135
      end
      object cxGrid2DBTableView1CENA: TcxGridDBColumn
        DataBinding.FieldName = 'CENA'
      end
      object cxGrid2DBTableView1DDV: TcxGridDBColumn
        DataBinding.FieldName = 'DDV'
        Width = 54
      end
      object cxGrid2DBTableView1CENADDV: TcxGridDBColumn
        DataBinding.FieldName = 'CENADDV'
        Width = 82
      end
      object cxGrid2DBTableView1RABAT: TcxGridDBColumn
        DataBinding.FieldName = 'RABAT'
        Width = 56
      end
      object cxGrid2DBTableView1KrajnaCena: TcxGridDBColumn
        DataBinding.FieldName = 'KRAJNA_CENA'
        Width = 82
      end
      object cxGrid2DBTableView1IIZNOS: TcxGridDBColumn
        DataBinding.FieldName = 'IIZNOS'
        Width = 51
      end
      object cxGrid2DBTableView1OPIS: TcxGridDBColumn
        DataBinding.FieldName = 'OPIS'
        Width = 133
      end
      object cxGrid2DBTableView1STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'STATUS'
        Width = 60
      end
      object cxGrid2DBTableView1VALUTA: TcxGridDBColumn
        DataBinding.FieldName = 'VALUTA'
        Width = 78
      end
      object cxGrid2DBTableView1VALUTANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VALUTANAZIV'
        Width = 80
      end
      object cxGrid2DBTableView1KURS: TcxGridDBColumn
        DataBinding.FieldName = 'KURS'
      end
      object cxGrid2DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Width = 69
      end
      object cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Width = 76
      end
      object cxGrid2DBTableView1ARTGRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'ARTGRUPA'
        Width = 87
      end
      object cxGrid2DBTableView1ARTGRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTGRUPANAZIV'
        Width = 112
      end
      object cxGrid2DBTableView1KREIRAL: TcxGridDBColumn
        DataBinding.FieldName = 'KREIRAL'
        Width = 82
      end
      object cxGrid2DBTableView1VREME: TcxGridDBColumn
        DataBinding.FieldName = 'VREME'
        Width = 66
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
end
