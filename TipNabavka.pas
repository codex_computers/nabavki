unit TipNabavka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, Menus, cxLookAndFeelPainters, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls, cxGridCustomView,
  cxGrid, StdCtrls, cxButtons, cxContainer, cxTextEdit, cxDBEdit, ExtCtrls,
  ActnList, cxLookAndFeels, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmTipNabavka = class(TForm)
    dPanel: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    StatusBar1: TStatusBar;
    Opis: TcxDBTextEdit;
    Label2: TLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    ButtonOtkazi: TButton;
    ButtonZacuvaj: TButton;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aPomos: TAction;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure ButtonZacuvajClick(Sender: TObject);
    procedure ButtonOtkaziClick(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipNabavka: TfrmTipNabavka;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija,  dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmTipNabavka.aOtkaziIzlezExecute(Sender: TObject);
begin
     if StateActive in [dsEdit,dsInsert] then
         begin
             dm.TipNabavki.Cancel;
             lPanel.Enabled:=true;
             dPanel.Enabled:=false;
             ButtonOtkazi.Visible:=False;
             ButtonZacuvaj.Visible:=False;
             cxgrid1.SetFocus;
             StateActive:=dsBrowse;
         end
     else Close;

end;

procedure TfrmTipNabavka.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(125);
end;

procedure TfrmTipNabavka.ButtonOtkaziClick(Sender: TObject);
begin
      if StateActive in [dsEdit,dsInsert] then
         begin
             dm.TipNabavki.Cancel;
             Sifra.Enabled:=True;
             lPanel.Enabled:=true;
             dPanel.Enabled:=false;
             ButtonOtkazi.Visible:=False;
             ButtonZacuvaj.Visible:=False;
             cxgrid1.SetFocus;
         end;
end;

procedure TfrmTipNabavka.ButtonZacuvajClick(Sender: TObject);
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
             if Sifra.Text = '' then Sifra.SetFocus
             else if Naziv.Text = '' then Naziv.SetFocus
             else if Opis.Text = '' then Opis.SetFocus
             else
                begin
                    dm.TipNabavki.Post;
                    Sifra.Enabled:=True;
                    lPanel.Enabled:=true;
                    dPanel.Enabled:=false;
                    ButtonOtkazi.Visible:=False;
                    ButtonZacuvaj.Visible:=False;
                    cxgrid1.SetFocus;
                end;
        end;
end;

procedure TfrmTipNabavka.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case Key of
          VK_F5:begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               Naziv.SetFocus;
               ButtonOtkazi.Visible:=true;
               ButtonZacuvaj.Visible:=True;
               dm.TipNabavki.Insert;
               StateActive:=dsInsert;
               dm.TipNabavkiID.Value:=dm.zemiTipNabavkaID(dm.MaxIDTipNabavka, Null, Null, Null, Null, Null, Null, 'MAXBR');
          end;
          VK_F6:begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               Naziv.SetFocus;
               ButtonOtkazi.Visible:=true;
               ButtonZacuvaj.Visible:=True;
               Sifra.Enabled:=False;
               dm.TipNabavki.Edit;
               StateActive:=dsEdit;
          end;
          VK_F7:begin
               dm.TipNabavki.Refresh;
          end;
          VK_F8:begin
               frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                 if frmDaNe.ShowModal =mrYes then
                    cxGrid1DBTableView1.DataController.DataSet.Delete();
          end;
          VK_F9:begin
               dm.TipNabavki.Post;
               dPanel.Enabled:=False;
               lPanel.Enabled:=true;
          end;
          VK_RETURN:begin
               if tag = 1 then
                  close;
          end;
      end;
end;

procedure TfrmTipNabavka.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTipNabavka.FormShow(Sender: TObject);
begin
     dm.TipNabavki.Open;
     cxGrid1.SetFocus;
end;

procedure TfrmTipNabavka.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;

begin
      case Key of
        VK_UP:
        begin
            if sender = Sifra then  ButtonOtkazi.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             if Sender = ButtonOtkazi then Sifra.SetFocus
             else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
          if sender = opis then
             begin
                if Sifra.Text = '' then Sifra.SetFocus
                else if Naziv.Text = '' then Naziv.SetFocus
                else if Opis.Text = '' then Opis.SetFocus
                else
                   begin
                      dm.TipNabavki.Post;
                      if StateActive in [dsEdit] then
                         begin
                            Sifra.Enabled:=True;
                            lPanel.Enabled:=true;
                            dPanel.Enabled:=false;
                            ButtonOtkazi.Visible:=False;
                            ButtonZacuvaj.Visible:=False;
                            cxgrid1.SetFocus;
                         end
                       else if StateActive in [dsInsert] then
                       begin
                          dm.TipNabavki.Insert;
                          dm.TipNabavkiID.Value:=dm.zemiTipNabavkaID(dm.MaxIDTipNabavka, Null, Null, Null, Null, Null, Null, 'MAXBR');
                          StateActive:=dsInsert;
                          Naziv.SetFocus;
                       end;
                   end;
             end
         else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;

  end;
end;
end.
