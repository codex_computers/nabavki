unit MKolicinaRealizacija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,cxGridExportLink, cxExport,
  cxGridDBTableView, dxStatusBar, dxRibbonStatusBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ExtCtrls, ActnList, cxCustomPivotGrid,
  cxDBPivotGrid, cxPivotGridChartConnection, cxGridChartView, cxGridDBChartView,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmRealizacijaKolicina = class(TForm)
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1ARTVID_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1ARTSIF_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINANABAVKA_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINAPONUDA_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINANALOG_OUT: TcxGridDBColumn;
    cxGrid1DBTableViewKOLICINAPRIEMNICA_OUT: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2ARTVID_OUT: TcxGridDBColumn;
    cxGrid1DBTableView2OPIS: TcxGridDBColumn;
    cxGrid1DBTableView2ARTSIF_OUT: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINAPONUDA_OUT: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINANALOG_OUT: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINAPRIEMNICA_OUT: TcxGridDBColumn;
    ActionList1: TActionList;
    aIzlez: TAction;
    aPecati: TAction;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1SUM: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1SUM1: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1SUM2: TcxGridDBColumn;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBChartView1: TcxGridDBChartView;
    cxGrid2DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series3: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series4: TcxGridDBChartSeries;
    cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    aZacuvajIzgled: TAction;
    aZacuvajVoExcel: TAction;
    cxGrid2DBChartView2: TcxGridDBChartView;
    cxGrid2DBChartView2Series1: TcxGridDBChartSeries;
    cxGrid2DBChartView2Series2: TcxGridDBChartSeries;
    cxGrid2DBChartView2Series3: TcxGridDBChartSeries;
    cxGrid2DBChartView2DataGroup2: TcxGridDBChartDataGroup;
    cxGrid2DBChartView2DataGroup4: TcxGridDBChartDataGroup;
    cxGrid2DBChartView2DataGroup1: TcxGridDBChartDataGroup;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    aDizajn: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRealizacijaKolicina: TfrmRealizacijaKolicina;

implementation

uses dmKonekcija,  dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmRealizacijaKolicina.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4014;
     dm.tblReportDizajn.FullRefresh;

     dm.frxRealizacijaKolicinaDetail.ParamByName('nabavkaID').Value:= dm.tblNabaviID.Value;
     dm.frxRealizacijaKolicinaDetail.ParamByName('godina').Value:= dm.tblNabaviGODINA.Value;
     dm.frxRealizacijaKolicinaDetail.Open;

     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblSpecifikacijaNaNabavka.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

     dm.frxReport.DesignReport();
end;

procedure TfrmRealizacijaKolicina.aIzlezExecute(Sender: TObject);
begin
     close;
end;

procedure TfrmRealizacijaKolicina.aPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.tblReportDizajn.ParamByName('id').Value:=4014;
     dm.tblReportDizajn.FullRefresh;

     dm.frxRealizacijaKolicinaDetail.ParamByName('nabavkaID').Value:= dm.tblNabaviID.Value;
     dm.frxRealizacijaKolicinaDetail.ParamByName('godina').Value:= dm.tblNabaviGODINA.Value;
     dm.frxRealizacijaKolicinaDetail.Open;

     dm.tblSpecifikacijaNaNabavka.ParamByName('NabavkaId').Value:=dm.tblNabaviID.Value;
     dm.tblSpecifikacijaNaNabavka.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmRealizacijaKolicina.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmRealizacijaKolicina.aZacuvajVoExcelExecute(Sender: TObject);
begin
//     dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmRealizacijaKolicina.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmRealizacijaKolicina.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.MKolicinaRealizacija.close;
     dm.MKolicinaRealizacijaDetail.Close;
     dm.frxRealizacijaKolicinaDetail.Close;
end;

procedure TfrmRealizacijaKolicina.FormShow(Sender: TObject);
begin
     dm.MKolicinaRealizacija.parambyName('godina').value:=dm.tblNabaviGODINA.Value;
     dm.MKolicinaRealizacija.parambyName('nabavkaID').value:=dm.tblNabaviID.Value;
     dm.MKolicinaRealizacija.Open;
     dm.MKolicinaRealizacijaDetail.ParamByName('nabavkaID').Value:= dm.tblNabaviID.Value;
     dm.MKolicinaRealizacijaDetail.ParamByName('godina').Value:= dm.tblNabaviGODINA.Value;
     dm.MKolicinaRealizacijaDetail.Open;

     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);

end;

end.
