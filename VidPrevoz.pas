unit VidPrevoz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, StdCtrls,
  cxContainer, cxTextEdit, cxDBEdit, ExtCtrls, ActnList, ComCtrls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp;

type
  TfrmVidPrevoz = class(TForm)
    dPanel: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    ButtonOtkazi: TButton;
    ButtonZacuvaj: TButton;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    StatusBar1: TStatusBar;
    aPomos: TAction;
    procedure ButtonZacuvajClick(Sender: TObject);
    procedure ButtonOtkaziClick(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVidPrevoz: TfrmVidPrevoz;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmVidPrevoz.aOtkaziIzlezExecute(Sender: TObject);
begin
    if StateActive in [dsEdit,dsInsert] then
         begin
             dm.Prevoz.Cancel;
             lPanel.Enabled:=true;
             dPanel.Enabled:=false;
             ButtonOtkazi.Visible:=False;
             ButtonZacuvaj.Visible:=False;
             cxgrid1.SetFocus;
             StateActive:=dsBrowse;
         end
     else Close;
end;

procedure TfrmVidPrevoz.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(128);
end;

procedure TfrmVidPrevoz.ButtonOtkaziClick(Sender: TObject);
begin
    if StateActive in [dsEdit,dsInsert] then
         begin
             dm.Prevoz.Cancel;
             Sifra.Enabled:=True;
             lPanel.Enabled:=true;
             dPanel.Enabled:=false;
             ButtonOtkazi.Visible:=False;
             ButtonZacuvaj.Visible:=False;
             cxgrid1.SetFocus;
         end;
end;

procedure TfrmVidPrevoz.ButtonZacuvajClick(Sender: TObject);
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
             if Sifra.Text = '' then Sifra.SetFocus
             else if Naziv.Text = '' then Naziv.SetFocus
             else
                begin
                    dm.Prevoz.Post;
                    Sifra.Enabled:=True;
                    lPanel.Enabled:=true;
                    dPanel.Enabled:=false;
                    ButtonOtkazi.Visible:=False;
                    ButtonZacuvaj.Visible:=False;
                    cxgrid1.SetFocus;
                end;
        end;
end;
procedure TfrmVidPrevoz.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
          VK_F5:begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               Naziv.SetFocus;
               ButtonOtkazi.Visible:=true;
               ButtonZacuvaj.Visible:=True;
               dm.Prevoz.Insert;
               StateActive:=dsInsert;
               dm.PrevozID.Value:=dm.zemiTipNabavkaID(dm.MaxVidPrevozID, Null, Null, Null, Null, Null, Null, 'MAXBR');
          end;
          VK_F6:begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               Naziv.SetFocus;
               ButtonOtkazi.Visible:=true;
               ButtonZacuvaj.Visible:=True;
               Sifra.Enabled:=False;
               dm.Prevoz.Edit;
               StateActive:=dsEdit;
          end;
          VK_F7:begin
               dm.Prevoz.Refresh;
          end;
          VK_F8:begin
               frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
                 if frmDaNe.ShowModal =mrYes then
                    cxGrid1DBTableView1.DataController.DataSet.Delete();
          end;
          VK_F9:begin
               dm.Prevoz.Post;
               dPanel.Enabled:=False;
               lPanel.Enabled:=true;
          end;
          VK_RETURN:begin
               if tag = 1 then
                  Close;
          end;
      end;
end;

procedure TfrmVidPrevoz.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmVidPrevoz.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var

  kom : TWinControl;

begin
      case Key of
        VK_UP:
        begin
            if sender = Sifra then  ButtonOtkazi.SetFocus
            else PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_DOWN:begin
             if Sender = ButtonOtkazi then Sifra.SetFocus
             else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_RETURN:
        begin
          if sender = Naziv then
             begin
                if Sifra.Text = '' then Sifra.SetFocus
                else if Naziv.Text = '' then Naziv.SetFocus
                else
                   begin
                      dm.Prevoz.Post;
                      if StateActive in [dsEdit] then
                         begin
                            Sifra.Enabled:=True;
                            lPanel.Enabled:=true;
                            dPanel.Enabled:=false;
                            ButtonOtkazi.Visible:=False;
                            ButtonZacuvaj.Visible:=False;
                            cxgrid1.SetFocus;
                         end
                       else if StateActive in [dsInsert] then
                       begin
                          dm.Prevoz.Insert;
                          dm.PrevozID.Value:=dm.zemiTipNabavkaID(dm.MaxVidPrevozID, Null, Null, Null, Null, Null, Null, 'MAXBR');
                          StateActive:=dsInsert;
                          Naziv.SetFocus;
                       end;
                   end;
             end
         else PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;

  end;
end;
procedure TfrmVidPrevoz.FormShow(Sender: TObject);
begin
     dm.Prevoz.Open;
end;

end.
