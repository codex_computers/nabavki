unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  cxCheckBox, cxDBLookupComboBox, dxRibbonSkins, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxSkinscxPCPainter, System.ImageList, cxImageList,
  System.Actions;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    aPonudi: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    aPlan: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    aNabavki: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aNalozi: TAction;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1Bar3: TdxBar;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aPonudiOdbranPartner: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton12: TdxBarLargeButton;
    aPrifateniPonudiPartner: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    aPlanZaSiteSektori: TAction;
    aPlanZaIzbraniotSektor: TAction;
    aSpecifikacijaNaPlan: TAction;
    aPonudiIzbraniArtikli: TAction;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aArtikli: TAction;
    aMapiranje: TAction;
    aArtVid: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    aArtGeupa: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    aMerka: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aProizvoditel: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    aTipPartner: TAction;
    aPartner: TAction;
    aKontakt: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aRabEdinica: TAction;
    aTipNabavka: TAction;
    aVidPrevoz: TAction;
    aValuta: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    aDrzavi: TAction;
    aMesto: TAction;
    aOpstina: TAction;
    aRegioni: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    aNabavkiPregled: TAction;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton33: TdxBarLargeButton;
    aSetupAdmin: TAction;
    asysFrmPrava: TAction;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    aGodisenPlan: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPonudiExecute(Sender: TObject);
    procedure aPlanExecute(Sender: TObject);
    procedure aNabavkiExecute(Sender: TObject);
    procedure aNaloziExecute(Sender: TObject);
    procedure aPonudiOdbranPartnerExecute(Sender: TObject);
    procedure aPrifateniPonudiPartnerExecute(Sender: TObject);
    procedure aPlanZaSiteSektoriExecute(Sender: TObject);
    procedure aPlanZaIzbraniotSektorExecute(Sender: TObject);
    procedure aSpecifikacijaNaPlanExecute(Sender: TObject);
    procedure aPonudiIzbraniArtikliExecute(Sender: TObject);
    procedure aArtikliExecute(Sender: TObject);
    procedure aMapiranjeExecute(Sender: TObject);
    procedure aArtVidExecute(Sender: TObject);
    procedure aArtGeupaExecute(Sender: TObject);
    procedure aMerkaExecute(Sender: TObject);
    procedure aProizvoditelExecute(Sender: TObject);
    procedure aTipPartnerExecute(Sender: TObject);
    procedure aPartnerExecute(Sender: TObject);
    procedure aKontaktExecute(Sender: TObject);
    procedure aRabEdinicaExecute(Sender: TObject);
    procedure aTipNabavkaExecute(Sender: TObject);
    procedure aVidPrevozExecute(Sender: TObject);
    procedure aValutaExecute(Sender: TObject);
    procedure aDrzaviExecute(Sender: TObject);
    procedure aMestoExecute(Sender: TObject);
    procedure aOpstinaExecute(Sender: TObject);
    procedure aRegioniExecute(Sender: TObject);
    procedure aNabavkiPregledExecute(Sender: TObject);
    procedure aSetupAdminExecute(Sender: TObject);
    procedure asysFrmPravaExecute(Sender: TObject);
    procedure aGodisenPlanExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, Ponudi, Plan, dmUnit, EvidencijaNabavki,
  NaloziZaIsporaka, ListaNaPartneriPonuduvaci, PregledPlanZaIzbranSektor,
  PregledPlanZaSiteSektori, PregledSpecifikacijaNaPlan, PonudeniArtikli,
  Artikal, Mapiranje, ArtGrupa, ArtVid, Merka, Proizvoditel, TipPartner, Drzava,
  Kontakt, Mesto, Opstina, Partner, RabotniEdinici, Region, TipNabavka, Valuta,
  VidPrevoz, PregledPoTipNabavka, SETUP, KorisniciZaOdobruvanje, MK,
  PregledGodisenPlan;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aArtGeupaExecute(Sender: TObject);
begin
      frmArtGrupa:=TfrmArtGrupa.Create(Application);
      frmArtGrupa.ShowModal;
      frmArtGrupa.Free;
end;

procedure TfrmMain.aArtikliExecute(Sender: TObject);
begin
    frmArtikal:=TfrmArtikal.Create(Application);
    frmArtikal.ShowModal;
    frmArtikal.free;
end;

procedure TfrmMain.aArtVidExecute(Sender: TObject);
begin
     frmArtVid:=TfrmArtVid.Create(Application);
     frmArtVid.ShowModal;
     frmArtVid.Free;
end;

procedure TfrmMain.aDrzaviExecute(Sender: TObject);
begin
      frmDrzava:=TfrmDrzava.Create(Application);
      frmDrzava.ShowModal;
      frmDrzava.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aGodisenPlanExecute(Sender: TObject);
begin
     frmGodisenPlan:=TfrmGodisenPlan.Create(Application);
     frmGodisenPlan.ShowModal;
     frmGodisenPlan.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aKontaktExecute(Sender: TObject);
begin
     frmKontakt:=TfrmKontakt.Create(Application);
     frmKontakt.ShowModal;
     frmKontakt.Free;
end;

procedure TfrmMain.aMapiranjeExecute(Sender: TObject);
begin
     frmGeneriki:=TfrmGeneriki.Create(Application);
     frmGeneriki.ShowModal;
     frmGeneriki.Free;
end;

procedure TfrmMain.aMerkaExecute(Sender: TObject);
begin
     frmMerka:=TfrmMerka.Create(Application);
     frmMerka.ShowModal;
     frmMerka.Free;
end;

procedure TfrmMain.aMestoExecute(Sender: TObject);
begin
     frmMesto:=TfrmMesto.Create(Application);
     frmMesto.ShowModal;
     frmMesto.Free;
end;

procedure TfrmMain.aNabavkiExecute(Sender: TObject);
begin
       frmNabavki:=tfrmNabavki.Create(Application);
       frmNabavki.ShowModal;
       frmNabavki.free;
end;

procedure TfrmMain.aNabavkiPregledExecute(Sender: TObject);
begin
     frmPregledPoTipNabavka:=TfrmPregledPoTipNabavka.Create(Application);
     frmPregledPoTipNabavka.ShowModal;
     frmPregledPoTipNabavka.Free;
end;

procedure TfrmMain.aNaloziExecute(Sender: TObject);
begin
     frmEvidencijaNaNaloziZaIsporaka:=TfrmEvidencijaNaNaloziZaIsporaka.Create(Application);
     frmEvidencijaNaNaloziZaIsporaka.ShowModal;
     frmEvidencijaNaNaloziZaIsporaka.Free;
end;

procedure TfrmMain.aOpstinaExecute(Sender: TObject);
begin
     frmOpstina:=TfrmOpstina.Create(Application);
     frmOpstina.ShowModal;
     frmOpstina.Free;
end;

procedure TfrmMain.aPartnerExecute(Sender: TObject);
begin
     frmPartner:=TfrmPartner.Create(Application);
     frmPartner.ShowModal;
     frmPartner.Free;
end;

procedure TfrmMain.aPlanExecute(Sender: TObject);
begin
     frmPlan:=tfrmPlan.create(application);
     frmPlan.showmodal;
     frmPlan.free;
end;

procedure TfrmMain.aPlanZaIzbraniotSektorExecute(Sender: TObject);
begin
     frmPlanZaizbranSektor:=TfrmPlanZaizbranSektor.Create(Application);
     frmPlanZaizbranSektor.ShowModal;
     frmPlanZaizbranSektor.Free;
end;

procedure TfrmMain.aPlanZaSiteSektoriExecute(Sender: TObject);
begin
     frmPlanZaSiteSektori:=TfrmPlanZaSiteSektori.Create(Application);
     frmPlanZaSiteSektori.ShowModal;
     frmPlanZaSiteSektori.Free;
end;

procedure TfrmMain.aPonudiExecute(Sender: TObject);
begin
     frmEvidencijaZaPonudi:=tfrmEvidencijaZaPonudi.create(application);
     frmEvidencijaZaPonudi.showmodal;
     frmEvidencijaZaPonudi.free;
end;

procedure TfrmMain.aPonudiIzbraniArtikliExecute(Sender: TObject);
begin
     frmPonudeniArtikli:=TfrmPonudeniArtikli.Create(Application);
     frmPonudeniArtikli.ShowModal;
     frmPonudeniArtikli.Free;
end;

procedure TfrmMain.aPonudiOdbranPartnerExecute(Sender: TObject);
begin
     frmPartneriPonuduvaci:=TfrmPartneriPonuduvaci.Create(Application);
     frmPartneriPonuduvaci.Tag:=1;
     frmPartneriPonuduvaci.ShowModal;
     frmPartneriPonuduvaci.Free;
end;

procedure TfrmMain.aPrifateniPonudiPartnerExecute(Sender: TObject);
begin
      frmPartneriPonuduvaci:=TfrmPartneriPonuduvaci.Create(Application);
      frmPartneriPonuduvaci.Tag:=2;
      frmPartneriPonuduvaci.ShowModal;
      frmPartneriPonuduvaci.Free;
end;

procedure TfrmMain.aProizvoditelExecute(Sender: TObject);
begin
     frmProizvoditel:=TfrmProizvoditel.Create(Application);
     frmProizvoditel.ShowModal;
     frmProizvoditel.Free;
end;

procedure TfrmMain.aRabEdinicaExecute(Sender: TObject);
begin
     frmRE:=TfrmRE.Create(Application);
     frmRE.ShowModal;
     frmRE.Free;
end;

procedure TfrmMain.aRegioniExecute(Sender: TObject);
begin
     frmRegion:=TfrmRegion.Create(Application);
     frmRegion.ShowModal;
     frmRegion.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSetupAdminExecute(Sender: TObject);
begin
     frmSetup:=TfrmSetup.Create(Application);
     frmSetup.ShowModal;
     frmSetup.Free;
end;

procedure TfrmMain.aSpecifikacijaNaPlanExecute(Sender: TObject);
begin
      frmSpecifikacijaZaSiteSektori:=TfrmSpecifikacijaZaSiteSektori.Create(Application);
      frmSpecifikacijaZaSiteSektori.ShowModal;
      frmSpecifikacijaZaSiteSektori.Free;
end;

procedure TfrmMain.asysFrmPravaExecute(Sender: TObject);
begin
     frmKorisniciZaOdobruvanje:=TfrmKorisniciZaOdobruvanje.Create(Application);
     frmKorisniciZaOdobruvanje.ShowModal;
     frmKorisniciZaOdobruvanje.Free;
end;

procedure TfrmMain.aTipNabavkaExecute(Sender: TObject);
begin
      frmTipNabavka:=TfrmTipNabavka.Create(Application);
      frmTipNabavka.ShowModal;
      frmTipNabavka.Free;
end;

procedure TfrmMain.aTipPartnerExecute(Sender: TObject);
begin
     frmTipPartner:=TfrmTipPartner.Create(Application);
     frmTipPartner.ShowModal;
     frmTipPartner.Free;
end;

procedure TfrmMain.aValutaExecute(Sender: TObject);
begin
     frmValuta:=TfrmValuta.Create(Application);
     frmValuta.ShowModal;
     frmValuta.Free;
end;

procedure TfrmMain.aVidPrevozExecute(Sender: TObject);
begin
     frmVidPrevoz:=TfrmVidPrevoz.Create(Application);
     frmVidPrevoz.ShowModal;
     frmVidPrevoz.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

   //cxSetEureka('danica.stojkova@codex.mk');
  //���� �����, �� ��� �� ������ ����������� �� ��
  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;

    dm.StatusNabavka.Open;
    dm.StatusPonuda.Open;
    dm.PoPonudaStavkIStatus.Open;
    dm.NalogStatus.Open;
    dm.tblSys_Setup.Open;
    dm.tblSysFrmConf.Close;
    dm.tblSysFrmConf.ParamByName('apps').Value:=dmKon.aplikacija;
    dm.tblSysFrmConf.Open;

    dm.Firmi.ParamByName('re').Value:=dmKon.re;
    dm.Firmi.Open;

    dm.tblSysFrmConf.Locate('USERID; APPS; FRM; OBJECT; PARAM', VarArrayOf([dmKon.user, 'PO', 'frmNabavki', 'aOdobriNabavka', 'Enabled']), []);
    odobriNabavka:= dm.tblSysFrmConfVAL.AsBoolean;

    dm.tblSysFrmConf.Locate('USERID; APPS; FRM; OBJECT; PARAM', VarArrayOf([dmKon.user, 'PO', 'frmPlan', 'aOtvoriOdobri', 'Enabled']), []);
    odobriPlan:= dm.tblSysFrmConfVAL.AsBoolean;

    dm.StatusNabavka.Locate('NAZIV', 'O�������',  []);
    otvorena_nab:=dm.StatusNabavkaID.Value;

    dm.StatusNabavka.Locate('NAZIV', '����������',  []);
    stornirana_nab:=dm.StatusNabavkaID.Value;

    dm.StatusNabavka.Locate('NAZIV', '���������',  []);
    zatvorena_nab:=dm.StatusNabavkaID.Value;

    dm.StatusNabavka.Locate('NAZIV', '��������',  []);
    odobrena_nab:=dm.StatusNabavkaID.Value;

    dm.StatusPonuda.Locate('NAZIV', 'O�������',  []);
    otvorena_pon:=dm.StatusPonudaID.Value;

    dm.StatusPonuda.Locate('NAZIV', '������� ���������',  []);
    delumnoprifatena_pon:=dm.StatusPonudaID.Value;

    dm.StatusPonuda.Locate('NAZIV', '���������',  []);
    prifatena_pon:=dm.StatusPonudaID.Value;

    dm.StatusPonuda.Locate('NAZIV', '�������',  []);
    odbiena_pon:=dm.StatusPonudaID.Value;

    dm.PoPonudaStavkIStatus.Locate('NAZIV', '��������',  []);
    otvorena_pon_stavka:=dm.PoPonudaStavkIStatusID.Value;

    dm.PoPonudaStavkIStatus.Locate('NAZIV', '���������',  []);
    prifatena_pon_stavka:=dm.PoPonudaStavkIStatusID.Value;

    dm.PoPonudaStavkIStatus.Locate('NAZIV', '�����������',  []);
    neprifatena_pon_stavka:=dm.PoPonudaStavkIStatusID.Value;

    dm.NalogStatus.Locate('NAZIV', '�������',  []);
    otvoren_nal:=dm.NalogStatusID.Value;

    dm.tblSys_Setup.Locate('P1; P2', VarArrayOf(['TIP_PARTNER', 'VRABOTEN']) , []);
    vraboten :=StrToInt(dm.tblSys_SetupV1.Value);

    dm.tblSys_Setup.Locate('P1; P2', VarArrayOf(['PO', 'SMTP']) , []);
    smpt_mail := dm.tblSys_SetupV1.Value;
    smpt_pass:= dm.tblSys_SetupV2.Value;

    dm.tblSys_Setup.Locate('P1; P2', VarArrayOf(['PO', 'avtomatsko_odobruvanje']) , []);
    avtomatsko_odobruvanje:= dm.tblSys_SetupV1.Value;

    dm.tblstatus.close;
    dm.tblstatus.parambyName('app').value:=dmKon.aplikacija;
    dm.tblstatus.parambyName('naziv').Value:='PO_PLAN';
    dm.tblstatus.Open;

    dm.tblstatus.Locate('NAZIV', '�������',  []);
    otvoren_plan:=dm.tblStatusID.Value;

    dm.tblstatus.Locate('NAZIV', '�������',  []);
    odobren_plan:=dm.tblStatusID.Value;

end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���
  dmMat.tblRE.Open();
  dmMat.tblArtikal.Open;
  dmMat.tblProizvoditel.Open;
  dmMat.tblArtGrupa.Open;
  dmMat.tblMerka.Open;
  dmMat.tblArtVid.Open;
  dmMat.tblDrzava.Open;
  dmMat.tblRegioni.Open;
  dmMat.tblOpstina.Open;
  dmMat.tblMesto.Open;
  dmMat.tblKontakt.Open;
  dmMat.tblTipPartner.Open;
  dmMat.tblPartner.Open;
  dmMat.tblValuta.Open;
end;


end.
