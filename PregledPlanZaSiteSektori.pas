unit PregledPlanZaSiteSektori;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit,
  StdCtrls, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,cxGridExportLink, cxExport,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, dxStatusBar,
  dxRibbonStatusBar, ExtCtrls, ActnList, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinSeven, dxSkinSharp, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions;

type
  TfrmPlanZaSiteSektori = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label3: TLabel;
    Label1: TLabel;
    PGodina: TcxComboBox;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aZacuvajVoExcel: TAction;
    aPecati: TAction;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1PODGRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1SUMAKOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DDV: TcxGridDBColumn;
    cxGrid1DBTableView1CENADDV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    aZacuvaIzgled: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aDizajn: TAction;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aZacuvaIzgledExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanZaSiteSektori: TfrmPlanZaSiteSektori;

implementation

uses dmKonekcija,  dmMaticni, dmUnit, Utils;

{$R *.dfm}

procedure TfrmPlanZaSiteSektori.aDizajnExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.frxDesignerPlan.Tag:=1;
     dm.tblReportDizajn.ParamByName('id').Value:=4001;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaSiteSektori.ParamByName('poteklo').Value:=(IntToStr(dmKon.re))+',';
     dm.tblPlanZaSiteSektori.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';;
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=PGodina.EditValue;
     dm.tblPlanZaSiteSektori.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan1.fr3');

     dm.frxReport.DesignReport();
end;

procedure TfrmPlanZaSiteSektori.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPlanZaSiteSektori.aPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dm.frxDesignerPlan.Tag:=1;
     dm.tblReportDizajn.ParamByName('id').Value:=4001;
     dm.tblReportDizajn.FullRefresh;

     dm.tblPlanZaSiteSektori.ParamByName('poteklo').Value:=(IntToStr(dmKon.re))+',';
     dm.tblPlanZaSiteSektori.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';;
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=PGodina.EditValue;
     dm.tblPlanZaSiteSektori.FullRefresh;

     RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
     dm.frxReport.LoadFromStream(RptStream) ;

//     dm.frxReportPlan1.LoadFromFile('D:\Codis\NABAVKI\reporti\Plan1.fr3');

//     dm.frxReport.DesignReport();
     dm.frxReport.ShowReport;
end;

procedure TfrmPlanZaSiteSektori.aZacuvaIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmPlanZaSiteSektori.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
//     dmMat.SaveToExcelDialog.FileName := '���� �� ���� �������' + '-' + dmKon.user;
//     if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmPlanZaSiteSektori.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPlanZaSiteSektori.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.tblPlanZaSiteSektori.Close;
end;

procedure TfrmPlanZaSiteSektori.FormCreate(Sender: TObject);
begin
     dm.tblPlanZaSiteSektori.ParamByName('poteklo').Value:=(IntToStr(dmKon.re))+',';
     dm.tblPlanZaSiteSektori.ParamByName('spisok').Value:=inttostr(dmKon.re)+ ','+'%';;
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=dmKon.godina;
     dm.tblPlanZaSiteSektori.Open;
end;

procedure TfrmPlanZaSiteSektori.FormShow(Sender: TObject);
begin
      procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
      PGodina.EditValue:=dmKon.godina;
      cxGrid1.SetFocus;
end;

procedure TfrmPlanZaSiteSektori.PGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPlanZaSiteSektori.ParamByName('godina').Value:=PGodina.EditValue;
     dm.tblPlanZaSiteSektori.FullRefresh;
end;

end.
