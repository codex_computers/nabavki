unit PonudeniArtikli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxContainer, cxLabel, dxStatusBar, dxRibbonStatusBar, ExtCtrls, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, ActnList, StdCtrls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, cxNavigator, System.Actions;

type
  TfrmPonudeniArtikli = class(TForm)
    Panel1: TPanel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel2: TPanel;
    cxLabel1: TcxLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    RibbonComboBox1: TcxComboBox;
    cxLabel2: TcxLabel;
    ActionList1: TActionList;
    aIzlez: TAction;
    Label1: TLabel;
    aPecati: TAction;
    aDizajn: TAction;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure RibbonComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPonudeniArtikli: TfrmPonudeniArtikli;

implementation

uses ArtGrupa, dmKonekcija, dmMaticni, dmUnit, Utils, AboutBox;

{$R *.dfm}

procedure TfrmPonudeniArtikli.aDizajnExecute(Sender: TObject);
var pom1, pom2,pom3,pom4:string;
    i:integer;
    var RptStream :TStream;
begin
     pom3:= '';
     pom4:='';

         dm.tblReportDizajn.ParamByName('id').Value:=4007;
         dm.tblReportDizajn.FullRefresh;
         if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
          begin
            pom1:= IntToStr(dm.tblPonudeniArtikliARTVID.Value)+',';
            pom2:=IntToStr(dm.tblPonudeniArtikliARTSIF.Value)+',';

            dm.ListaNaponudiZaArtikli.ParamByName('re').Value:=dmKon.re;
            dm.ListaNaponudiZaArtikli.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
            dm.ListaNaponudiZaArtikli.ParamByName('vid').Value:=pom1;
            dm.ListaNaponudiZaArtikli.ParamByName('sif').Value:=pom2;
            dm.ListaNaponudiZaArtikli.FullRefresh;
          end
         else if cxGrid1DBTableView1.Controller.SelectedRecordCount > 1  then
          begin
            for i := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
               begin
                 pom1:= IntToStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0]);
                 pom2:= IntToStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[1]);
                 pom3:= pom3 + pom1 + ',';
                 pom4:= pom4 + pom2 + ',';
               end;
            dm.ListaNaponudiZaArtikli.ParamByName('re').Value:=dmKon.re;
            dm.ListaNaponudiZaArtikli.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
            dm.ListaNaponudiZaArtikli.ParamByName('vid').Value:=pom3;
            dm.ListaNaponudiZaArtikli.ParamByName('sif').Value:=pom4;
            dm.ListaNaponudiZaArtikli.FullRefresh;
          end;
          RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
          dm.frxReport.LoadFromStream(RptStream) ;

           dm.frxReport.DesignReport();
end;

procedure TfrmPonudeniArtikli.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPonudeniArtikli.aPecatiExecute(Sender: TObject);
var pom1, pom2,pom3,pom4:string;
    i:integer;
    var RptStream :TStream;
begin
     pom3:= '';
     pom4:='';
         dm.tblReportDizajn.ParamByName('id').Value:=4007;
         dm.tblReportDizajn.FullRefresh;
         if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
          begin
            pom1:= IntToStr(dm.tblPonudeniArtikliARTVID.Value)+',';
            pom2:=IntToStr(dm.tblPonudeniArtikliARTSIF.Value)+',';

            dm.ListaNaponudiZaArtikli.ParamByName('re').Value:=dmKon.re;
            dm.ListaNaponudiZaArtikli.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
            dm.ListaNaponudiZaArtikli.ParamByName('vid').Value:=pom1;
            dm.ListaNaponudiZaArtikli.ParamByName('sif').Value:=pom2;
            dm.ListaNaponudiZaArtikli.FullRefresh;
          end
         else if cxGrid1DBTableView1.Controller.SelectedRecordCount > 1  then
          begin
            for i := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
               begin
                 pom1:= IntToStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0]);
                 pom2:= IntToStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[1]);
                 pom3:= pom3 + pom1 + ',';
                 pom4:= pom4 + pom2 + ',';
               end;
            dm.ListaNaponudiZaArtikli.ParamByName('re').Value:=dmKon.re;
            dm.ListaNaponudiZaArtikli.ParamByName('godina').Value:=RibbonComboBox1.EditValue;
            dm.ListaNaponudiZaArtikli.ParamByName('vid').Value:=pom3;
            dm.ListaNaponudiZaArtikli.ParamByName('sif').Value:=pom4;
            dm.ListaNaponudiZaArtikli.FullRefresh;
          end;
          RptStream := dm.tblReportDizajn.CreateBlobStream(dm.tblReportDizajnDATA , bmRead);
          dm.frxReport.LoadFromStream(RptStream) ;

//           dm.frxReport.DesignReport();
          dm.frxReport.ShowReport;
end;

procedure TfrmPonudeniArtikli.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPonudeniArtikli.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.tblPonudeniArtikli.Close;
end;

procedure TfrmPonudeniArtikli.FormCreate(Sender: TObject);
begin
     dm.tblPonudeniArtikli.ParamByName('re').Value:=dmKon.re;
     dm.tblPonudeniArtikli.ParamByName('godina').Value :=dmKon.godina;
     dm.tblPonudeniArtikli.open;
     dm.ListaNaponudiZaArtikli.open;
end;

procedure TfrmPonudeniArtikli.FormShow(Sender: TObject);
begin
     cxGrid1.SetFocus;
     RibbonComboBox1.EditValue:=dmKon.godina;
end;

procedure TfrmPonudeniArtikli.RibbonComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
    dm.tblPonudeniArtikli.ParamByName('godina').Value := RibbonComboBox1.EditValue;
    dm.tblPonudeniArtikli.FullRefresh;
end;

end.
