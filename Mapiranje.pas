unit Mapiranje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, ExtCtrls, cxControls, dxStatusBar,
  dxRibbonStatusBar, RibbonLunaStyleActnCtrls, Ribbon, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ComCtrls, StdCtrls, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ToolWin, ActnMan, ActnCtrls, ActnList, ActnMenus, RibbonActnMenus,
  cxContainer, cxLabel, cxDBLabel, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinDarkRoom, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinSeven, dxSkinSharp;

type
  TfrmGeneriki = class(TForm)
    Ribbon1: TRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel4: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    Panel5: TPanel;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    ActionManager1: TActionManager;
    aDodadi: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aIzlez: TAction;
    aPoNabavkaStavki: TAction;
    aSnimiIzgled: TAction;
    aBrisiStavkaPonuda: TAction;
    RibbonPage1: TRibbonPage;
    RibbonGroup1: TRibbonGroup;
    RibbonGroup2: TRibbonGroup;
    Action1: TAction;
    RibbonApplicationMenuBar1: TRibbonApplicationMenuBar;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1BRPAK: TcxGridDBColumn;
    cxGrid2DBTableView1KATALOG: TcxGridDBColumn;
    cxGrid2DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1TARIFA: TcxGridDBColumn;
    cxGrid3DBTableView1VID_GENERIKA: TcxGridDBColumn;
    cxGrid3DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid3DBTableView1OPST_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV: TcxGridDBColumn;
    RibbonGroup4: TRibbonGroup;
    aArtikli: TAction;
    cxDBLabel1: TcxDBLabel;
    aPomos: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aDodadiExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aBrisiStavkaPonudaExecute(Sender: TObject);
    procedure aArtikliExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPomosExecute(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeneriki: TfrmGeneriki;

implementation

uses dmKonekcija, dmMaticni, dmUnit, NovaGenerika, DaNe, Utils,
  Artikal;

{$R *.dfm}

procedure TfrmGeneriki.aArtikliExecute(Sender: TObject);
begin
    (* ������� frmArtikli:=TfrmArtikli.Create(Application);
     frmArtikli.Tag:=10;
     frmArtikli.ShowModal;
     frmArtikli.Free;       *)
     frmArtikal:=TfrmArtikal.Create(Application);
     frmArtikal.ShowModal;
     frmArtikal.Free;
     dm.tblListaArtikli.Refresh;
     cxGrid2.SetFocus;
end;

procedure TfrmGeneriki.aAzurirajExecute(Sender: TObject);
begin
      if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
           frmNovaGenerika:=TfrmNovaGenerika.Create(Application);
           frmNovaGenerika.Tag:=1;
           frmNovaGenerika.ShowModal;
           frmNovaGenerika.Free;
           cxGrid2.SetFocus;
        end
      else ShowMessage('����������� �������� !!!!');
end;


procedure TfrmGeneriki.aBrisiExecute(Sender: TObject);
begin
      if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
      begin
         frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
         if frmDaNe.ShowModal =mrYes then
            begin
               dm.tblGenerika.Delete;
               dm.tblGenerika.Refresh;
               dm.tblListaArtikli.FullRefresh;
            end;
      end
     else ShowMessage('����������� �������� !!!!');
end;

procedure TfrmGeneriki.aBrisiStavkaPonudaExecute(Sender: TObject);
begin
      if cxGrid3DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
          frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
          if frmDaNe.ShowModal =mrYes then
            cxGrid3DBTableView1.DataController.DataSet.Delete();
        end;
end;

procedure TfrmGeneriki.aDodadiExecute(Sender: TObject);
begin
      frmNovaGenerika:=TfrmNovaGenerika.Create(Application);
      frmNovaGenerika.ShowModal;
      frmNovaGenerika.Free;
      cxGrid2.SetFocus;
end;

procedure TfrmGeneriki.aIzlezExecute(Sender: TObject);
begin
      Close;
end;

procedure TfrmGeneriki.aOsveziExecute(Sender: TObject);
begin
      dm.tblGenerika.ParamByName('artvid').Value:=dm.zemiGenArtVid(dm.GenArtVid, Null, Null, Null, Null, Null, Null, 'GARTVID');
      dm.tblGenerika.FullRefresh;
end;

procedure TfrmGeneriki.aPomosExecute(Sender: TObject);
begin
      Application.HelpContext(108);
end;

procedure TfrmGeneriki.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
         begin
             dm.tblMapiranje.ParamByName('generika').Value:=dm.tblGenerikaID.Value;
             dm.tblMapiranje.FullRefresh;
         end;
end;

procedure TfrmGeneriki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGeneriki.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case key  of
         VK_RETURN:begin
             if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
               if cxGrid2DBTableView1.Controller.SelectedRecordCount = 1 then
                  begin
                     dm.tblMapiranje.Insert;
                     dm.tblMapiranjeVID_GENERIKA.Value:=dm.tblGenerikaARTVID.Value;
                     dm.tblMapiranjeGENERIKA.Value:=dm.tblGenerikaID.Value;
                     dm.tblMapiranjeVID_ARTIKAL.Value:=dm.tblListaArtikliARTVID.Value;
                     dm.tblMapiranjeARTIKAL.Value:=dm.tblListaArtikliID.Value;
                     try
                       dm.tblMapiranje.Post;
                       except on Exception DO dm.tblMapiranje.Cancel;
                     end;
                     dm.tblMapiranje.Refresh;
                  end;
         end;
        VK_INSERT:begin
            if tag <> 1 then
             begin
              (* �������frmArtikli:=TfrmArtikli.Create(Application);
              frmArtikli.Tag:=15;
              frmArtikli.ShowModal;
              frmArtikli.Free;*)
              frmArtikal:=TfrmArtikal.Create(Application);
              frmArtikal.ShowModal;
              frmArtikal.Free;
              dm.tblListaArtikli.Refresh;
              cxGrid2.SetFocus;
             end;
        end;
     end;
end;

procedure TfrmGeneriki.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmGeneriki.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmGeneriki.FormCreate(Sender: TObject);
begin
      dm.tblListaArtikli.Open;
      dm.tblGenerika.ParamByName('artvid').Value:=dm.zemiGenArtVid(dm.GenArtVid, Null, Null, Null, Null, Null, Null, 'GARTVID');
      dm.tblGenerika.Open;

end;

procedure TfrmGeneriki.FormShow(Sender: TObject);
begin
      if tag = 1  then
         aArtikli.Enabled:=false;
      if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
         begin
             dm.tblMapiranje.ParamByName('generika').Value:=dm.tblGenerikaID.Value;
             dm.tblMapiranje.Open;
         end;
      cxGrid2.SetFocus;

end;



end.
